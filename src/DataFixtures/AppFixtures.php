<?php

namespace App\DataFixtures;

use App\Entity\Company;
use App\Entity\Role;
use App\Entity\Skill;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    /**
     * @var Factory
     */
    private $faker;

    private const DRIVER = 'DRIVER';
    private const EMPLOYEE = 'EMPLOYEE';
    private const COMPANY = 'COMPANY';
    private const MANAGER = 'MANAGER';
    private const SUPERVISOR = 'SUPERVISOR';

    private CONST ROLES = [
        'ADMIN',
        'COMPANY',
        'EMPLOYEE',
        'MANAGER',
        'DRIVER',
        'SUPERVISOR',
        'TRAINEE'
    ];

    private const EMPLOYEE_ROLES =
        [
            'DRIVER',
            'MANAGER',
            'SUPERVISOR'
        ];

    private const MANAGER_ROLES =
        [
            'ASSISTANT MANAGER',
            'ASSISTANT MANAGER 2'
        ];

    private const SUPERVISOR_ROLES =
        [
            'ASSISTANT SUPERVISOR',
            'ASSISTANT SUPERVISOR 2'
        ];

    private const MANAGER_HIERARCHY =
        [
            'STATION MANGER',
            'ASSISTANT MANAGER'
        ];

    private const SUPERVISOR_HIERARCHY =
        [
            'STATION SUPERVISOR',
            'ASSISTANT SUPERVISOR'
        ];

    private const DRIVERS_SKILLS =
        [
            'Backup Driver',
            'Lead Driver',
            'Bulk Driver',
            'AM Cosmos',
            'PM Cosmos',
            'Hourly',
            'Rescue Driver',
            'Sweeper'
        ];


    public function __construct()
    {
        $this->faker = Factory::create('en_US');
    }

    public function load(ObjectManager $manager)
    {
        echo 'DOCUMENTATION IS ON THE README ';
        echo 'THIS IS FOR TESTING, YOU SHOULD CALL YOUR FUNCTIONS TO BE EXECUTED FOR DB INTERACTION';
//        $this->loadCompanies($manager);
    }

    public static function getGroups(): array
    {
        return ['NAME_TO_IDENTIFY_FIXTURE_TO RUN'];
    }

    public function getRandom($array)
    {
        $max = count($array) - 1;
        return $array[rand(0, $max)];
    }

    public function loadCompanies(ObjectManager $manager)
    {
        for ($i = 0; $i < 1; $i++) {

            $apiKey = '';
            try {
                $apiKey = bin2hex(random_bytes(16));
            } catch (\Exception $e) {
                return $e;
            }
            $user = new User();
            $user->setEmail($this->faker->unique()->email);
            $user->setFirstname($this->faker->firstName);
            $user->setLastname($this->faker->lastName);
            $user->setPassword($this->faker->password);
            $manager->persist($user);
            $manager->flush();

            $role = new Role();
            $roleName = self::COMPANY;
            $role->setName($roleName);
            $manager->persist($role);
            $manager->flush();


            $company = new Company();
            $company->setName($this->faker->company);
            $company->setApiKey($apiKey);
            $manager->persist($company);
            $manager->flush();

            $user->addUserRole($role);
            $user->setCompany($company);

            $company->addCompanyRole($role);
            $company->addUser($user);

            $role->setCompany($company);
            $role->addUser($user);

            $manager->flush();

//           $this->loadEmployee($manager, $company, $role, self::DRIVER);
//           $this->loadEmployee($manager, $company, $role, self::MANAGER);
//           $this->loadEmployee($manager, $company, $role, self::SUPERVISOR);
        }
    }

    public function loadEmployee(ObjectManager $manager, Company $company, Role $roleParent, string $roleName)
    {
        $user = new User();
        $user->setEmail($this->faker->unique()->email);
        $manager->persist($user);
        $manager->flush();

        $role = new Role();
        $role->setName(self::EMPLOYEE);
        $role->setParent($roleParent);
        $role->setCompany($company);
        $manager->persist($role);
        $manager->flush();

        $roleParent->addChild($role);
        $user->addUserRole($role);
        $manager->flush();

        if ($roleName === self::DRIVER) {
            $roleChild = new Role();
            $roleChild->setName(self::DRIVER);
            $roleChild->setParent($role);
            $roleChild->setCompany($company);
            $manager->persist($roleChild);
            $manager->flush();

            $role->addChild($roleChild);
            $manager->flush();

            for ($k = 0; $k < 3; $k++) {
                $skill = new Skill();
                $skill->setName($this->getRandom(self::DRIVERS_SKILLS));
                $skill->addUser($user);
                $skill->setCompany($company);
                $manager->persist($skill);
                $manager->flush();

                $user->addSkill($skill);
                $manager->flush();
            }
        } elseif ($roleName === self::MANAGER) {
            $roleChild = new Role();
            $roleChild->setName(self::MANAGER);
            $roleChild->setParent($role);
            $roleChild->setCompany($company);
            $manager->persist($roleChild);
            $manager->flush();

            $role->addChild($roleChild);
            $manager->flush();

            if (rand(0, 1) === 1) {
                $roleChild1 = new Role();
                $roleChild1->setName(self::MANAGER_HIERARCHY[0]);
                $roleChild1->setParent($roleChild);
                $roleChild1->setCompany($company);
                $manager->persist($roleChild1);
                $manager->flush();

                $roleChild->addChild($roleChild1);
                $manager->flush();
                foreach (self::MANAGER_ROLES as $name) {
                    $roleChild2 = new Role();
                    $roleChild2->setName($name);
                    $roleChild2->setParent($roleChild1);
                    $roleChild2->setCompany($company);
                    $manager->persist($roleChild2);
                    $manager->flush();

                    $roleChild1->addChild($roleChild2);
                    $manager->flush();
                }
            } else {
                $roleChild1 = new Role();
                $roleChild1->setName(self::MANAGER_HIERARCHY[0]);
                $roleChild1->setParent($roleChild);
                $roleChild1->setCompany($company);
                $manager->persist($roleChild1);
                $manager->flush();

                $roleChild->addChild($roleChild1);
                $manager->flush();
            }

        } elseif ($roleName === self::SUPERVISOR) {
            $roleChild = new Role();
            $roleChild->setName(self::SUPERVISOR);
            $roleChild->setParent($role);
            $roleChild->setCompany($company);
            $manager->persist($roleChild);
            $manager->flush();

            $role->addChild($roleChild);
            $manager->flush();

            if (rand(0, 1) === 1) {
                $roleChild1 = new Role();
                $roleChild1->setName(self::SUPERVISOR_HIERARCHY[0]);
                $roleChild1->setParent($roleChild);
                $roleChild1->setCompany($company);
                $manager->persist($roleChild1);
                $manager->flush();

                $roleChild->addChild($roleChild1);
                $manager->flush();
                foreach (self::SUPERVISOR_ROLES as $name) {
                    $roleChild2 = new Role();
                    $roleChild2->setName($name);
                    $roleChild2->setParent($roleChild1);
                    $roleChild2->setCompany($company);
                    $manager->persist($roleChild2);
                    $manager->flush();

                    $roleChild1->addChild($roleChild2);
                    $manager->flush();
                }
            } else {
                $roleChild1 = new Role();
                $roleChild1->setName(self::SUPERVISOR_HIERARCHY[0]);
                $roleChild1->setParent($roleChild);
                $roleChild1->setCompany($company);
                $manager->persist($roleChild1);
                $manager->flush();

                $roleChild->addChild($roleChild1);
                $manager->flush();
            }
        }
    }
}
