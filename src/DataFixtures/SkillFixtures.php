<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class SkillFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $skill1 = new Skill();
        $skill1->setName('Driver');
        $skill1->setCompany($this->getReference('company_1'));
        $manager->persist($skill1);
        $manager->flush();

        $skill2 = new Skill();
        $skill2->setName('Backup Driver');
        $skill2->setCompany($this->getReference('company_2'));
        $manager->persist($skill2);
        $manager->flush();

        $skill3 = new Skill();
        $skill3->setName('Load Driver');
        $skill3->setCompany($this->getReference('company_3'));
        $manager->persist($skill3);
        $manager->flush();

        $skill4 = new Skill();
        $skill4->setName('On Road Training');
        $skill4->setCompany($this->getReference('company_1'));
        $manager->persist($skill4);
        $manager->flush();

        $skill5 = new Skill();
        $skill5->setName('Bulk Driver');
        $skill5->setCompany($this->getReference('company_2'));
        $manager->persist($skill5);
        $manager->flush();

        $skill6 = new Skill();
        $skill6->setName('AM Cosmos');
        $skill6->setCompany($this->getReference('company_3'));
        $manager->persist($skill6);
        $manager->flush();

        $skill7 = new Skill();
        $skill7->setName('Hourly');
        $skill7->setCompany($this->getReference('company_3'));
        $manager->persist($skill7);
        $manager->flush();

        $skill8 = new Skill();
        $skill8->setName('Night Duty');
        $skill8->setCompany($this->getReference('company_2'));
        $manager->persist($skill8);
        $manager->flush();

        $skill9 = new Skill();
        $skill9->setName('Pace Training');
        $skill9->setCompany($this->getReference('company_3'));
        $manager->persist($skill9);
        $manager->flush();

        $skill10 = new Skill();
        $skill10->setName('Classroom Training');
        $skill10->setCompany($this->getReference('company_1'));
        $manager->persist($skill10);
        $manager->flush();

        $skill11 = new Skill();
        $skill11->setName('Driver Training');
        $skill11->setCompany($this->getReference('company_2'));
        $manager->persist($skill11);
        $manager->flush();

        $skill12 = new Skill();
        $skill12->setName('Station Manager');
        $skill12->setCompany($this->getReference('company_1'));
        $manager->persist($skill12);
        $manager->flush();

        $skill13 = new Skill();
        $skill13->setName('Sweeper');
        $skill13->setCompany($this->getReference('company_3'));
        $manager->persist($skill13);
        $manager->flush();

        $skill14 = new Skill();
        $skill14->setName('Rescue Driver');
        $skill14->setCompany($this->getReference('company_1'));
        $manager->persist($skill14);
        $manager->flush();

        $skill15 = new Skill();
        $skill15->setName('Senior lead Driver');
        $skill15->setCompany($this->getReference('company_1'));
        $manager->persist($skill15);
        $manager->flush();

        $skill16 = new Skill();
        $skill16->setName('MECOB Driver');
        $skill16->setCompany($this->getReference('company_1'));
        $manager->persist($skill16);
        $manager->flush();

        $skill17 = new Skill();
        $skill17->setName('Assistant Manager');
        $skill17->setCompany($this->getReference('company_1'));
        $manager->persist($skill17);
        $manager->flush();

        $skill18 = new Skill();
        $skill18->setName('SWA');
        $skill18->setCompany($this->getReference('company_1'));
        $manager->persist($skill18);
        $manager->flush();

        $skill19 = new Skill();
        $skill19->setName('PM Cosmos');
        $skill19->setCompany($this->getReference('company_1'));
        $manager->persist($skill19);
        $manager->flush();

        $skill20 = new Skill();
        $skill20->setName('Test Position');
        $skill20->setCompany($this->getReference('company_2'));
        $manager->persist($skill20);
        $manager->flush();

        $skill22 = new Skill();
        $skill22->setName('Hourly Rescue');
        $skill22->setCompany($this->getReference('company_3'));
        $manager->persist($skill22);
        $manager->flush();

        $skill23 = new Skill();
        $skill23->setName('PM Cosmos Backup');
        $skill23->setCompany($this->getReference('company_3'));
        $manager->persist($skill23);
        $manager->flush();

        $skill24 = new Skill();
        $skill24->setName('Frozen Goods');
        $skill24->setCompany($this->getReference('company_2'));
        $manager->persist($skill24);
        $manager->flush();

        $skill25 = new Skill();
        $skill25->setName('Van Shuttle - Hourly');
        $skill25->setCompany($this->getReference('company_2'));
        $manager->persist($skill25);
        $manager->flush();

    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}
