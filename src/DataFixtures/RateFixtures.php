<?php

namespace App\DataFixtures;

use App\Entity\Rate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class RateFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $rate1 = new Rate();
        $rate1->setName('Type 1');
        $rate1->setStation($this->getReference('station_1'));
        $rate1->setCriteria('monthly');
        $rate1->setEffectiveRate('10');
        $rate1->setCumulativeRate('15');
        $rate1->setCompany($this->getReference('company_1'));
        $this->addReference('rate_1', $rate1);
        $manager->persist($rate1);
        $manager->flush();

        $rate2 = new Rate();
        $rate2->setName('Type 2');
        $rate2->setStation($this->getReference('station_1'));
        $rate2->setCriteria('monthly');
        $rate2->setEffectiveRate('10');
        $rate2->setCumulativeRate('15');
        $rate2->setCompany($this->getReference('company_1'));
        $this->addReference('rate_2', $rate2);
        $manager->persist($rate2);
        $manager->flush();

        $rate3 = new Rate();
        $rate3->setName('Type 3');
        $rate3->setStation($this->getReference('station_1'));
        $rate3->setCriteria('monthly');
        $rate3->setEffectiveRate('10');
        $rate3->setCumulativeRate('15');
        $rate3->setCompany($this->getReference('company_1'));
        $this->addReference('rate_3', $rate3);
        $manager->persist($rate3);
        $manager->flush();

        $rate4 = new Rate();
        $rate4->setName('Type 4');
        $rate4->setStation($this->getReference('station_1'));
        $rate4->setCriteria('monthly');
        $rate4->setEffectiveRate('10');
        $rate4->setCumulativeRate('15');
        $rate4->setCompany($this->getReference('company_1'));
        $this->addReference('rate_4', $rate4);
        $manager->persist($rate4);
        $manager->flush();

        $rate5 = new Rate();
        $rate5->setName('Type 5');
        $rate5->setStation($this->getReference('station_1'));
        $rate5->setCriteria('monthly');
        $rate5->setEffectiveRate('10');
        $rate5->setCumulativeRate('15');
        $rate5->setCompany($this->getReference('company_1'));
        $this->addReference('rate_5', $rate5);
        $manager->persist($rate5);
        $manager->flush();
    }
    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}
