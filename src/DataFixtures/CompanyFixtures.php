<?php

namespace App\DataFixtures;

use App\Entity\Company;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class CompanyFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface

{

    public function load(ObjectManager $manager)
    {
        $company = new Company();
        $company->setName('Solider Company');
        $company->setApiKey('3F7QRJREmt');
        $company->setNumberOfUser(10);
        $this->addReference('company_1', $company);
        $manager->persist($company);
        $manager->flush();

        $company1 = new Company();
        $company1->setName('DSP Test 1');
        $company1->setApiKey('6PGiPpZO27');
        $company1->setNumberOfUser(10);
        $this->addReference('company_2', $company1);
        $manager->persist($company1);
        $manager->flush();

        $company2 = new Company();
        $company2->setName('DSP Test 2');
        $company2->setApiKey('FizKKDfVg3');
        $company2->setNumberOfUser(10);
        $this->addReference('company_3', $company2);
        $manager->persist($company2);
        $manager->flush();
    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}
