<?php

namespace App\DataFixtures;

use App\Entity\InvoiceType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class InvoiceTypeFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $invoiceType1 = new InvoiceType();
        $invoiceType1->setName('Type 1');
        $invoiceType1->setStation($this->getReference('station_1'));
        $invoiceType1->setRateRule($this->getReference('rate_1'));
        $invoiceType1->getBillableHour(3);
        $this->addReference('invoice_type_1', $invoiceType1);
        $manager->persist($invoiceType1);
        $manager->flush();

        $invoiceType2 = new InvoiceType();
        $invoiceType2->setName('Type 2');
        $invoiceType2->setStation($this->getReference('station_2'));
        $invoiceType2->setRateRule($this->getReference('rate_2'));
        $invoiceType2->getBillableHour(11);
        $this->addReference('invoice_type_2', $invoiceType2);
        $manager->persist($invoiceType2);
        $manager->flush();

        $invoiceType3 = new InvoiceType();
        $invoiceType3->setName('Type 3');
        $invoiceType3->setStation($this->getReference('station_3'));
        $invoiceType3->setRateRule($this->getReference('rate_3'));
        $invoiceType3->getBillableHour(18);
        $this->addReference('invoice_type_3', $invoiceType3);
        $manager->persist($invoiceType3);
        $manager->flush();

        $invoiceType4 = new InvoiceType();
        $invoiceType4->setName('Type 4');
        $invoiceType4->setStation($this->getReference('station_4'));
        $invoiceType4->setRateRule($this->getReference('rate_4'));
        $invoiceType4->getBillableHour(22);
        $this->addReference('invoice_type_4', $invoiceType4);
        $manager->persist($invoiceType4);
        $manager->flush();

        $invoiceType5 = new InvoiceType();
        $invoiceType5->setName('Type 5');
        $invoiceType5->setStation($this->getReference('station_5'));
        $invoiceType5->setRateRule($this->getReference('rate_5'));
        $invoiceType5->getBillableHour(22);
        $this->addReference('invoice_type_5', $invoiceType5);
        $manager->persist($invoiceType5);
        $manager->flush();

    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 4;
    }
}
