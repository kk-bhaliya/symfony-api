<?php

namespace App\DataFixtures;

use App\Entity\Shift;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class ShiftTypeFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $shift1 = new Shift();
        $shift1->setName('Type 1');
        $shift1->setStation($this->getReference('station_1'));
        $shift1->setBalanceGroup($this->getReference('group_1'));
        $shift1->setInvoiceType($this->getReference('invoice_type_1'));
        $shift1->setCompany($this->getReference('company_1'));
        $shift1->setColor('#ffttuu');
        $shift1->setStartTime('07:18');
        $shift1->setEndTime('10:18');
        $shift1->setUnpaidBreak(15);
        $shift1->setNote('lorem ipsum....');
        $shift1->setCategory(1);
        $manager->persist($shift1);
        $manager->flush();

        $shift2 = new Shift();
        $shift2->setName('Type 2');
        $shift2->setStation($this->getReference('station_2'));
        $shift2->setInvoiceType($this->getReference('invoice_type_2'));
        $shift2->setBalanceGroup($this->getReference('group_2'));
        $shift2->setCompany($this->getReference('company_1'));
        $shift2->setColor('#ffttuu');
        $shift2->setStartTime('11:18');
        $shift2->setEndTime('12:18');
        $shift2->setUnpaidBreak(10);
        $shift2->setNote('lorem ipsum....');
        $shift2->setCategory(1);
        $manager->persist($shift2);
        $manager->flush();

        $shift3 = new Shift();
        $shift3->setName('Type 3');
        $shift3->setStation($this->getReference('station_3'));
        $shift3->setInvoiceType($this->getReference('invoice_type_3'));
        $shift3->setBalanceGroup($this->getReference('group_3'));
        $shift3->setCompany($this->getReference('company_1'));
        $shift3->setColor('#fftppp');
        $shift3->setStartTime('06:18');
        $shift3->setEndTime('09:18');
        $shift3->setUnpaidBreak(10);
        $shift3->setNote('lorem ipsum....');
        $shift3->setCategory(2);
        $manager->persist($shift3);
        $manager->flush();

        $shift4 = new Shift();
        $shift4->setName('Type 4');
        $shift4->setStation($this->getReference('station_4'));
        $shift4->setInvoiceType($this->getReference('invoice_type_4'));
        $shift4->setBalanceGroup($this->getReference('group_4'));
        $shift4->setCompany($this->getReference('company_1'));
        $shift4->setCategory(1);
        $shift4->setColor('#fftqqp');
        $shift4->setStartTime('10:18');
        $shift4->setEndTime('11:18');
        $shift4->setUnpaidBreak(10);
        $shift4->setNote('lorem ipsum....');
        $manager->persist($shift4);
        $manager->flush();

    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 5;
    }
}
