<?php

namespace App\DataFixtures;

use App\Entity\Station;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class StationFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $station = new Station();
        $station->setName('DCH4');
        $station->setNote('Hello');
        $station->setCompany($this->getReference('company_1'));
        $station->setCode('101');
        $station->setStationAddress('401 Terrace Dr, Mundelein, IL 60060');
        $station->setStationLatitude('42.253950');
        $station->setStationLongitude('-87.985380');
        $station->setStationGeofence('100');
        $station->setParkingLotAddress('600 US-45, Libertyville, IL 60048');
        $station->setParkingLotLatitude('42.289660');
        $station->setParkingLotLongitude('-87.998710');
        $station->setParkingLotGeofence('100');
        $this->addReference('station_1', $station);
        $manager->persist($station);
        $manager->flush();

        $station1 = new Station();
        $station1->setName('DCH5');
        $station1->setNote('Hello');
        $station1->setCompany($this->getReference('company_2'));
        $station1->setCode('101');
        $station1->setStationAddress('2400 Northwest Parkway, Elgin, IL 60124');
        $station1->setStationLatitude('42.094110');
        $station1->setStationLongitude('-88.339660');
        $station1->setStationGeofence('100');
        $station1->setParkingLotAddress('450 Airport Rd #830, Elgin, IL 60178');
        $station1->setParkingLotLatitude('42.071860');
        $station1->setParkingLotLongitude('-88.287230');
        $station1->setParkingLotGeofence('100');
        $this->addReference('station_2', $station1);
        $manager->persist($station1);
        $manager->flush();

        $station2 = new Station();
        $station2->setName('DGR1');
        $station2->setNote('Hello');
        $station2->setCompany($this->getReference('company_3'));
        $station2->setCode('101');
        $station2->setStationAddress('2935 Walkent Court NW, Walker, MI 49544');
        $station2->setStationLatitude('43.020550');
        $station2->setStationLongitude('-85.720920');
        $station2->setStationGeofence('100');
        $station2->setParkingLotAddress('701 Ann St NW, Grand Rapids, MI 49544');
        $station2->setParkingLotLatitude('42.998650');
        $station2->setParkingLotLongitude('-85.685130');
        $station2->setParkingLotGeofence('100');
        $this->addReference('station_3', $station2);
        $manager->persist($station2);
        $manager->flush();

        $station3 = new Station();
        $station3->setName('DML2');
        $station3->setNote('Hello');
        $station3->setCompany($this->getReference('company_1'));
        $station3->setCode('101');
        $station3->setStationAddress('N53 W24700 Corporate Circle, Sussex, WI 53089');
        $station3->setStationLatitude('43.115260');
        $station3->setStationLongitude('-88.242450');
        $station3->setStationGeofence('100');
        $station3->setParkingLotAddress('N53 W24700 Corporate Circle, Sussex, WI 53089');
        $station3->setParkingLotLatitude('43.115260');
        $station3->setParkingLotLongitude('-88.242450');
        $station3->setParkingLotGeofence('100');
        $this->addReference('station_4', $station3);
        $manager->persist($station3);
        $manager->flush();

        $station4 = new Station();
        $station4->setName('DMS1');
        $station4->setNote('Hello');
        $station4->setCompany($this->getReference('company_2'));
        $station4->setCode('101');
        $station4->setStationAddress('2811 Beverly Rd, Suite 300, Eagan, MN 55121');
        $station4->setStationLatitude('44.854320');
        $station4->setStationLongitude('-93.135930');
        $station4->setStationGeofence('100');
        $station4->setParkingLotAddress('1000 Blue Gentian Rd, Eagan, MN 55121');
        $station4->setParkingLotLatitude('44.859930');
        $station4->setParkingLotLongitude('-93.137370');
        $station4->setParkingLotGeofence('100');
        $this->addReference('station_5', $station4);
        $manager->persist($station4);
        $manager->flush();

        $station5 = new Station();
        $station5->setName('DNA1');
        $station5->setNote('Hello');
        $station5->setCompany($this->getReference('company_3'));
        $station5->setCode('101');
        $station5->setStationAddress('2815 Brick Church Pike, Nashville, TN 37207');
        $station5->setStationLatitude('36.217910');
        $station5->setStationLongitude('-86.781850');
        $station5->setStationGeofence('100');
        $station5->setParkingLotAddress('2815 Brick Church Pike, Nashville, TN 37207');
        $station5->setParkingLotLatitude('36.217910');
        $station5->setParkingLotLongitude('-86.781850');
        $station5->setParkingLotGeofence('100');
        $this->addReference('station_6', $station5);
        $manager->persist($station5);
        $manager->flush();

        $station6 = new Station();
        $station6->setName('DOM1');
        $station6->setNote('Hello');
        $station6->setCompany($this->getReference('company_1'));
        $station6->setCode('101');
        $station6->setStationAddress('11652 South 154th Street, Omaha, NE 68138');
        $station6->setStationLatitude('41.188940');
        $station6->setStationLongitude('-96.154630');
        $station6->setStationGeofence('100');
        $station6->setParkingLotAddress('11652 South 154th Street, Omaha, NE 68138');
        $station6->setParkingLotLatitude('41.188940');
        $station6->setParkingLotLongitude('-96.154630');
        $station6->setParkingLotGeofence('100');
        $this->addReference('station_7', $station6);
        $manager->persist($station6);
        $manager->flush();
    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}
