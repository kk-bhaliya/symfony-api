<?php

namespace App\DataFixtures;

use App\Entity\BalanceGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class BalanceGroupFixtures extends Fixture implements FixtureInterface, OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $balanceGroup1 = new BalanceGroup();
        $balanceGroup1->setName('Group 1');
        $balanceGroup1->setCompany($this->getReference('company_1'));
        $this->addReference('group_1', $balanceGroup1);
        $manager->persist($balanceGroup1);
        $manager->flush();

        $balanceGroup2 = new BalanceGroup();
        $balanceGroup2->setName('Group 2');
        $balanceGroup2->setCompany($this->getReference('company_1'));
        $this->addReference('group_2', $balanceGroup2);
        $manager->persist($balanceGroup2);
        $manager->flush();

        $balanceGroup3 = new BalanceGroup();
        $balanceGroup3->setName('Group 3');
        $balanceGroup3->setCompany($this->getReference('company_1'));
        $this->addReference('group_3', $balanceGroup3);
        $manager->persist($balanceGroup3);
        $manager->flush();

        $balanceGroup4 = new BalanceGroup();
        $balanceGroup4->setName('Group 4');
        $balanceGroup4->setCompany($this->getReference('company_1'));
        $this->addReference('group_4', $balanceGroup4);
        $manager->persist($balanceGroup4);
        $manager->flush();

        $balanceGroup5 = new BalanceGroup();
        $balanceGroup5->setName('Group 5');
        $balanceGroup5->setCompany($this->getReference('company_1'));
        $this->addReference('group_5', $balanceGroup5);
        $manager->persist($balanceGroup5);
        $manager->flush();
    }

    public function getOrder()
    {
        //TODO: the order in which fixtures will be loaded
        //TODO: the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}
