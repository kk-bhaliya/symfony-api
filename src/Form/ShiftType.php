<?php

namespace App\Form;

use App\Entity\Shift;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShiftType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('color')
            ->add('startTime')
            ->add('endTime')
            ->add('unpaidBreak')
            ->add('invoiceType')
            ->add('station')
            ->add('skill')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shift::class,
            'csrf_protection' => false
        ]);
    }
}
