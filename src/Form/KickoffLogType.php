<?php

namespace App\Form;

use App\Entity\KickoffLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class KickoffLogType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('loggedInAt')
            ->add('loggedPunchInTimeIn')
            ->add('loggedPunchInTimeAt')
            ->add('scannedQRCodeAt')
            ->add('loggedMileageAt')
            ->add('loggedIntoFlexAt')
            ->add('stageReleaseAt')
            ->add('stationArrivalAt')
            ->add('stationDepartureAt')
            ->add('driverTask')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => KickoffLog::class,
            'csrf_protection' => false
        ]);
    }
}
