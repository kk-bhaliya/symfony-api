<?php

namespace App\Form;

use App\Entity\Driver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DriverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jobTitle')
            ->add('personalPhone')
            ->add('companyPhone')
            ->add('driversLicenseNumber')
            ->add('driversLicenseNumberExpiration', DateTimeType::class, [
                'format' => 'Y-m-d',
                'widget' => 'single_text'
            ])
            ->add('hireDate', DateTimeType::class, [
                'format' => 'Y-m-d H:i:s',
                'widget' => 'single_text'
            ])
            ->add('birthday', DateTimeType::class, [
                'format' => 'Y-m-d',
                'widget' => 'single_text'
            ])
            ->add('note')
            ->add('shirtSize')
            ->add('shoeSize')
            ->add('pantSize')
            ->add('fireDate')
            ->add('discretionaryBonus')
            ->add('skills')
            ->add('station')
            ->add('score')
            ->add('user')
            ->add('employmentStatus')
            ->add('assignedManager')
            ->add('payRate')
            ->add('schedule')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Driver::class,
            'csrf_protection' => false
        ]);
    }
}
