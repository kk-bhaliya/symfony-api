<?php

namespace App\Form;

use App\Entity\DriverRoute;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DriverRouteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateCreated', DateTimeType::class, [
                'format' => 'Y-m-d',
                'widget' => 'single_text'
            ])
            ->add('punchIn')
            ->add('punchOut')
            ->add('shiftTime')
            ->add('skill')
            ->add('vehicle')
            ->add('device')
            ->add('driver')
            ->add('station')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DriverRoute::class,
            'csrf_protection' => false
        ]);
    }
}
