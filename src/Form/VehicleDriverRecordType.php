<?php

namespace App\Form;

use App\Entity\VehicleDriverRecords;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VehicleDriverRecordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateSignedOut')
            ->add('dateSignedIn')
            ->add('startingMileage')
            ->add('endingMileage')
            ->add('startCondition')
            ->add('endCondition')
            ->add('vehicle')
            ->add('driver')
            ->add('driverTask')
            ->add('station')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => VehicleDriverRecord::class,
            'csrf_protection' => false
        ]);
    }
}
