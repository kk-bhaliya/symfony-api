<?php

namespace App\Form;

use App\Entity\Station;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('location')
            ->add('note')
            ->add('company')
            ->add('code')
            ->add('stationAddress')
            ->add('stationLatitude')
            ->add('stationLogitude')
            ->add('stationGeofence')
            ->add('parkingLotAddress')
            ->add('parkingLotLatitude')
            ->add('parkingLotLogitude')
            ->add('parkingLotGeofence')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Station::class,
            'csrf_protection' => false
        ]);
    }
}
