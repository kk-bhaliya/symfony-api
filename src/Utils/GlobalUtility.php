<?php

namespace App\Utils;


use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\KickoffLog;
use App\Entity\ReturnToStation;
use App\Entity\Shift;
use App\Entity\Event;
use App\Entity\Incident;
use App\Entity\DriverRouteCode;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Entity\Station;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use function Sodium\randombytes_buf;

class GlobalUtility
{


    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var ContainerBagInterface
     */
    private $params;

    private $stopwatch;
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * GlobalUtility constructor.
     * @param ContainerBagInterface $params
     * @param EntityManagerInterface $manager
     * @param Stopwatch $stopwatch
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
     */
    public function __construct(
        ContainerBagInterface $params,
        EntityManagerInterface $manager,
        Stopwatch $stopwatch,
        \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface $tokenStorage
    )
    {
        $this->manager = $manager;
        $this->params = $params;
        $this->stopwatch = $stopwatch;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string $time
     * @return DateTime
     * @throws Exception
     */
    public static function newUTCDateTime($time = 'now')
    {
        return new DateTime($time, new \DateTimeZone('UTC'));
    }

    /**
     * Compute the start and end date of some fixed o relative quarter in a specific year.
     * @param mixed $quarter Integer from 1 to 4 or relative string value:
     *                        'this', 'current', 'previous', 'first' or 'last'.
     *                        'this' is equivalent to 'current'. Any other value
     *                        will be ignored and instead current quarter will be used.
     *                        Default value 'current'. Particulary, 'previous' value
     *                        only make sense with current year so if you use it with
     *                        other year like: get_dates_of_quarter('previous', 1990)
     *                        the year will be ignored and instead the current year
     *                        will be used.
     * @param int $year Year of the quarter. Any wrong value will be ignored and
     *                        instead the current year will be used.
     *                        Default value null (current year).
     * @param string $format String to format returned dates
     * @return array          Array with two elements (keys): start and end date.
     */
    public function getDatesOfQuarter($quarter = 'current', $lastFixDays = [90, 0], $year = null, $format = null)
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }
        $currentQuarter = ceil((new DateTime)->format('n') / 3);
        switch (strtolower($quarter)) {
            case 'this':
            case 'current':
                $quarter = ceil((new DateTime)->format('n') / 3);
                break;

            case 'previous':
                $year = (new DateTime)->format('Y');
                if ($currentQuarter == 1) {
                    $quarter = 4;
                    $year--;
                } else {
                    $quarter = $currentQuarter - 1;
                }
                break;

            case 'first':
                $quarter = 1;
                break;

            case 'last':
                $quarter = 4;
                break;

            case 'byday':
                $quarter = false;
                break;

            default:
                $quarter = (!is_int($quarter) || $quarter < 1 || $quarter > 4) ? $currentQuarter : $quarter;
                break;
        }

        if ($quarter == false) {
            list($startFrom, $endTo) = $lastFixDays;
            $start = (new DateTime($startFrom . ' days ago'));
            $end = (new DateTime($endTo . ' days ago'));
        } else {
            if ($quarter === 'this') {
                $quarter = ceil((new DateTime)->format('n') / 3);
            }
            $start = new DateTime($year . '-' . (3 * $quarter - 2) . '-1 00:00:00');
            $end = new DateTime($year . '-' . (3 * $quarter) . '-' . ($quarter == 1 || $quarter == 4 ? 31 : 30) . ' 23:59:59');
        }

        return array(
            'start' => $format ? $start->format($format) : $start,
            'end' => $format ? $end->format($format) : $end,
        );
    }

    /**
     * Convert datetime object according requested timezone offset.
     * @param datetime|DateTimeInterface $datetime Datetime object.
     * @param string $requestTimeZoneOffset Requested timezone offset.
     * @param bool $isUtcToLocal
     *
     * @return datetime                     Return datetime object.
     */
    public function getTimeZoneConversation($datetime, $requestTimeZoneOffset, $isUtcToLocal = true)
    {
        if (!empty($datetime) && $isUtcToLocal) {
            $datetime->modify("-$requestTimeZoneOffset minutes");
        } else {
            $datetime->modify("+$requestTimeZoneOffset minutes");
        }
        return $datetime;
    }

    /**
     * Compute the start and end date of some fixed week and year.
     * @param int $week week.
     * @param int $year Year of the week. Any wrong value will be ignored and
     *                        instead the current year will be used.
     *                        Default value null (current year).
     * @param string $format String to format returned dates
     * @return array          Array with two elements (keys): start and end date.
     */
    public function getStartAndEndDate($week, $year = null, $format = 'Y-m-d')
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }
        $dto = (new DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dto->setISODate($year, $week);
        $dto->modify('-1 days');
        $result['start'] = $dto->format($format);
        $dto->modify('+6 days');
        $result['end'] = $dto->format($format);

        return $result;
    }

    /**
     * Create shift data for passed driver route object.
     * @param DriverRoute|TempDriverRoute $driverRouteObj Driver route object.
     * @param int $weekNumber Current Driver Route Object week number.
     * @param int $requestTimeZone
     * @param bool $timeline
     *
     * @return array Shift data array.
     * @throws Exception
     */
    public function getShiftData($driverRouteObj, $weekNumber, $requestTimeZone = 0, $timeline = false)
    {
        $today = (new DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today = $this->getTimeZoneConversation($today, $requestTimeZone);
        $today->setTime(0, 0, 0);

        $codeArr = [];
        if (method_exists($driverRouteObj, 'getRouteId')) {
            if (!empty($driverRouteObj->getRouteId()) && !empty($driverRouteObj->getRouteId()->getDriverRouteCodes())) {
                foreach ($driverRouteObj->getRouteId()->getDriverRouteCodes()->filter(function ($routeCode) {
                    return !$routeCode->getIsArchive();
                }) as $code) {
                    $codeArr[] = $code->getCode();
                }
            }
        } else {
            if (!empty($driverRouteObj->getDriverRouteCodes())) {
                foreach ($driverRouteObj->getDriverRouteCodes()->filter(function ($routeCode) {
                    return !$routeCode->getIsArchive();
                }) as $code) {
                    $codeArr[] = $code->getCode();
                }
            }
        }

        $breakPunchTime = '00:00';
        if (!empty($driverRouteObj->getBreakPunchIn()) && !empty($driverRouteObj->getBreakPunchOut())) {
            $interval = date_diff($driverRouteObj->getBreakPunchIn(), $driverRouteObj->getBreakPunchOut());
            $breakPunchTime = $interval->format('%H:%I');
        }

        $startTimeObj = !empty($driverRouteObj->getPunchIn()) ? $driverRouteObj->getPunchIn() : $driverRouteObj->getStartTime();
        $endTimeObj = !empty($driverRouteObj->getPunchOut()) ? $driverRouteObj->getPunchOut() : $driverRouteObj->getEndTime();

        $endTimeOfPriorShiftObj = $priorShiftType = null;
        if (!empty($driverRouteObj->getOldDriverRoute()) && !empty($driverRouteObj->getOldDriverRoute()->getId())) {
            $this->manager->detach($driverRouteObj->getOldDriverRoute());
            $oldDriverRoute = $this->manager->getRepository(DriverRoute::class)->find($driverRouteObj->getOldDriverRoute()->getId());
            $endTimeOfPriorShiftObj = $oldDriverRoute->getDateTimeEnded() ?? $oldDriverRoute->getEndTime();
            //Get shift type
            $priorShiftType = (($oldDriverRoute->getIsBackup()) ? 'Backup' : (($oldDriverRoute->getIsRescuer()) ? 'Rescuer' : (($oldDriverRoute->getIsAddTrain()) ? 'Trainer' : (($oldDriverRoute->getIsLightDuty()) ? 'Duty' : ''))));
        }

        $utcStartTime = $startTimeObj->format('G.i');
        $utcEndTime = $endTimeObj->format('G.i');
        $utcEndTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;

        $startTimeObj = $this->getTimeZoneConversation($startTimeObj, $requestTimeZone);

        $endTimeObj = $this->getTimeZoneConversation($endTimeObj, $requestTimeZone);
        if (!empty($driverRouteObj->getDatetimeEnded()))
            $endTimeObj = $driverRouteObj->getDatetimeEnded();

        $endTimeOfPriorShiftObj = !empty($endTimeOfPriorShiftObj) ? $this->getTimeZoneConversation($endTimeOfPriorShiftObj, $requestTimeZone) : null;

        $startHour = $startTimeObj->format($this->params->get('time_format'));
        $endHour = $endTimeObj->format($this->params->get('time_format'));

        $startTime = $startTimeObj->format('G.i');
        $endTime = $endTimeObj->format('G.i');
        $endTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;

        $dateTimeEndedObj = (method_exists($driverRouteObj, 'getRouteId'))
            ? empty($driverRouteObj->getRouteId())
                ? null
                : $driverRouteObj->getRouteId()->getDatetimeEnded()
            : $driverRouteObj->getDatetimeEnded();
        $dateTimeEnded = empty($dateTimeEndedObj)
            ? null
            : (float)$dateTimeEndedObj->format('G.i');

        $priorShiftRouteId = !empty($driverRouteObj->getOldDriverRoute()) ? $driverRouteObj->getOldDriverRoute()->getId() : null;

        $driverRouteDevice = (method_exists($driverRouteObj, 'getDevice')) ? $driverRouteObj->getDevice() : $driverRouteObj->getRouteId()->getDevice();
        $driverRouteVehicle = (method_exists($driverRouteObj, 'getVehicle')) ? $driverRouteObj->getVehicle() : $driverRouteObj->getRouteId()->getVehicle();

        $deviceId = !empty($driverRouteDevice->last) ? $driverRouteDevice->last->getImei() : '';
        $vehicleId = !empty($driverRouteVehicle) ? $driverRouteVehicle->getId() : '';
        $vehicleUniqueId = !empty($driverRouteVehicle) ? $driverRouteVehicle->getVehicleId() : '';
        $vehicleVin = !empty($driverRouteVehicle) ? $driverRouteVehicle->getVin() : '';
        $hasDriver = !empty($driverRouteObj->getDriver()) ? true : false;

        $timelineArray = [];

        /* @var $driverRouteEventObj DriverRoute */
        $driverRouteEventObj = $driverRouteObj;

        $driver = $driverRouteEventObj->getDriver();
        $driverFullName = 'N/A';
        if ($driver instanceof Driver) {
            $driverFullName = $driver->getFullName();
        }

        $events = $driverRouteEventObj->getEvents()->filter(function (Event $item) {
            return $item->getIsActive() === true;
        });

        $eventsList = $this->eventsList($events->toArray(), $requestTimeZone, $driverFullName, $deviceId, $vehicleId, $vehicleUniqueId, $vehicleVin, false, false, false, false);
        $timelineArray = $eventsList[1];

        if (!empty($eventsList[0])) {
            $eventsList2 = $this->eventsList(array_reverse($eventsList[0]), $requestTimeZone, $driverFullName, $deviceId, $vehicleId, $vehicleUniqueId, $vehicleVin, $eventsList[2], $eventsList[3], $eventsList[4], $eventsList[5]);
            $timelineArray = array_merge($timelineArray, $eventsList2[1]);
        }

        /*
            TODO:
            - Move events loop to a new function: eventsList()
            - Update events loop to contain more detailed information about the devices.
            - Return $events and $timelineArray

            Proposal:

            $events = $driverRouteEventObj->getEvents()->filter(function (Event $item) {
                return $item->getIsActive() === true;
            });

            // Return loadout events
            $events = $this->eventsList($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '' );
            $timelineArray = $events['timelineArray'];

            // Return RTS
            if(!empty($events)) {
                $events = $this->eventsList($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '' );
                $timelineArray = $events['timelineArray'];
            }
         */

        $incident = null;
        if (!empty($driverRouteObj->getIncidents())) {
            $incidentObj = $driverRouteObj->getIncidents()->filter(function (Incident $item) {
                return $item->getIsArchive() === false;
            });
            if (!empty($incidentObj->first())) {
                $incident = [
                    'id' => $incidentObj->first()->getId(),
                    'title' => $incidentObj->first()->getTitle(),
                    'type' => $incidentObj->first()->getIncidentType()->getName(),
                    'dateTime' => $incidentObj->first()->getDateTime()->format('Y-m-d H:i:s')
                ];
            }
        }

        // Sent home punch
        if (!empty($driverRouteObj->getDatetimeEnded()) && $incident) {
            $timelineArray[] = $this->buildTimeline(
                'sentHome',
                substr(($this->getTimeZoneConversation($driverRouteObj->getDatetimeEnded(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                $driverFullName . ' was sent home.',
                $deviceId,
                $vehicleId,
                $vehicleUniqueId,
                $vehicleVin
            );
        }

        if ($timeline && $hasDriver) {
            if (!empty($driverRouteObj->getPunchIn())) {
                $timelineArray[] = $this->buildTimeline(
                    'PunchIn',
                    substr($startHour, 0, -1),
                    $driverFullName . ' has punched in successfully.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($driverRouteObj->getBreakPunchIn())) {
                $timelineArray[] = $this->buildTimeline(
                    'LunchIn',
                    substr(($this->getTimeZoneConversation($driverRouteObj->getBreakPunchIn(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has punched out for break.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($driverRouteObj->getBreakPunchOut())) {
                $timelineArray[] = $this->buildTimeline(
                    'LunchOut',
                    substr(($this->getTimeZoneConversation($driverRouteObj->getBreakPunchOut(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has punched in from break.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($driverRouteObj->getDatetimeEnded())) {
                $endTimeObj = $this->getTimeZoneConversation($driverRouteObj->getDatetimeEnded(), $requestTimeZone);
                $endHour = $endTimeObj->format($this->params->get('time_format'));
                $endTime = $endTimeObj->format('G.i');

                $timelineArray[] = $this->buildTimeline(
                    'CheckSquare',
                    substr(
                        $endHour,
                        0,
                        -1
                    ),
                    'Shift type ended by ' . ((method_exists($driverRouteEventObj->getUpdatedBy(), 'getFriendlyName')) ? $driverRouteEventObj->getUpdatedBy()->getFriendlyName() : $driverRouteEventObj->getDriver()->getMainManager()->getFriendlyName()),
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($driverRouteObj->getPunchOut())) {
                $timelineArray[] = $this->buildTimeline(
                    'PunchOut',
                    substr($endHour, 0, -1),
                    $driverFullName . ' has punched out for the day.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }
        }

        usort($timelineArray, function ($a, $b) {
            return new DateTime($a['eventTime'] . 'm') <=> new DateTime($b['eventTime'] . 'm');
        });

        $isExempt = !empty($driverRouteObj->getDriver()) ? ($driverRouteObj->getDriver()->getEmploymentStatus()->getName() == 'Exempt' ? true : false) : false;
        $currentRequestType = '';
        if ($driverRouteObj->getRequestForRoute()) {
            $routeRequest = $driverRouteObj->getRequestForRoute();
            if ($routeRequest->getIsSwapRequest()) {
                $currentRequestType = 'swap_request';
            } else if ($routeRequest->getNewDriver()) {
                $currentRequestType = 'shift_request';
            } else {
                $currentRequestType = 'drop_request';
            }
        }

        //Get driver routes details
        $driverRoutesDetails = [];
        if (!empty($driverRouteObj->getDriverRoutes())) {
            foreach ($driverRouteObj->getDriverRoutes() as $driverRoutesAll) {
                if (!empty($driverRoutesAll->getDriver())) {
                    $driverRoutesDetails[] = [
                        'id' => $driverRoutesAll->getId(),
                        'driverId' => $driverRoutesAll->getDriver()->getId(),
                        'driverName' => $driverRoutesAll->getDriver()->getFullName(),
                    ];
                }
            }
        }

        return [
            'id' => $driverRouteObj->getId(),
            'hour' => $startHour . '-' . $endHour,
            'startTime' => (float)$startTime,
            'endTime' => (float)$endTime,
            'utcStartTime' => (float)$utcStartTime,
            'utcEndTime' => (float)$utcEndTime,
            'endTimeOfPriorShift' => (float)$endTimeOfPriorShift,
            'utcEndTimeOfPriorShift' => (float)$utcEndTimeOfPriorShift,
            'dateTimeEnded' => $dateTimeEnded,
            'typeId' => $driverRouteObj->getShiftType()->getId(),
            'type' => $driverRouteObj->getShiftName(),
            'category' => $driverRouteObj->getShiftType()->getCategory(),
            'bg' => $driverRouteObj->getShiftColor(),
            'color' => $driverRouteObj->getShiftTextColor(),
            'skill' => !empty($driverRouteObj->getSkill()) ? $driverRouteObj->getSkill()->getId() : '',
            'note' => $driverRouteObj->getShiftNote(),
            'countOfTotalStops' => $driverRouteObj->getCountOfTotalStops(),
            'completedCountOfStops' => $driverRouteObj->getCompletedCountOfStops(),
            'countOfTotalPackages' => $driverRouteObj->getCountOfTotalPackages(),
            'countOfAttemptedPackages' => $driverRouteObj->getCountOfAttemptedPackages(),
            'countOfReturningPackages' => $driverRouteObj->getCountOfReturningPackages(),
            'countOfDeliveredPackages' => $driverRouteObj->getCountOfDeliveredPackages(),
            'countOfMissingPackages' => $driverRouteObj->getCountOfMissingPackages(),
            'percent' => '0%',
            'time' => $breakPunchTime,
            'isNew' => method_exists($driverRouteObj, 'getIsNew'),
            'hasAlert' => method_exists($driverRouteObj, 'getIsNew'),
            'currentWeek' => $weekNumber,
            'originalDay' => (method_exists($driverRouteObj, 'getRouteId') && !empty($driverRouteObj->getRouteId()))
                ? date('w', strtotime($driverRouteObj->getRouteId()->getDateCreated()->format('Y-m-d H:i:s')))
                : date('w', strtotime($driverRouteObj->getDateCreated()->format('Y-m-d H:i:s'))),
            'isOpenShift' => (method_exists($driverRouteObj, 'getIsOpenShift')) ? $driverRouteObj->getIsOpenShift() : false,
            'stationId' => $driverRouteObj->getStation()->getId(),
            'routeId' => (method_exists($driverRouteObj, 'getRouteId'))
                ? empty($driverRouteObj->getRouteId()) ? null : $driverRouteObj->getRouteId()->getId()
                : $driverRouteObj->getId(),
            'priorShiftRouteId' => $priorShiftRouteId,
            'priorShiftType' => $priorShiftType,
            'shiftDate' => $driverRouteObj->getDateCreated()->format('Y-m-d'),
            'driverId' => !empty($driverRouteObj->getDriver()) ? $driverRouteObj->getDriver()->getId() : '0.0.0',
            'driver' => !empty($driverRouteObj->getDriver()) ? [
                "name" => $driverRouteObj->getDriver()->getFullName(),
                "title" => $driverRouteObj->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                "img" => $driverRouteObj->getDriver()->getUser()->getProfileImage(),
                'employeeStatus' => $driverRouteObj->getDriver()->getEmployeeStatus() == Driver::STATUS_ACTIVE ? true : false
            ] : [],
            'routeCode' => $codeArr,
            'timezone' => $driverRouteObj->getStation()->getTimezone(),
            'isTemp' => $driverRouteObj->getIsTemp(),
            'canDelete' => $today <= $driverRouteObj->getDateCreated(),
            'showTimeline' => $today < $driverRouteObj->getDateCreated(),
            'isRescued' => $driverRouteObj->getIsRescued(),
            'isRescuer' => $driverRouteObj->getIsRescuer(),
            'isDuty' => $driverRouteObj->getIsLightDuty(),
            'isTrain' => $driverRouteObj->getIsAddTrain(),
            'numberOfPackage' => $driverRouteObj->getNumberOfPackage(),
            'invoiceTypeId' => !empty($driverRouteObj->getShiftInvoiceType()) ? $driverRouteObj->getShiftInvoiceType()->getId() : null,
            'timeline' => $timelineArray,
            // 'driverStation' => !empty($driverRouteObj->getDriver()) ? $driverRouteObj->getDriver()->getStations()->first()->getId() : null,
            'isExempt' => $isExempt,
            'isBackup' => ($driverRouteObj->getShiftType()->getCategory() == \App\Entity\Shift::BACKUP),
            'isRequested' => $driverRouteObj->getRequestForRoute() ? true : false,
            'currentRequestType' => $currentRequestType,
            'isRequestCompleted' => $driverRouteObj->getIsRequestCompleted(),
            'requestApprovedBy' => (!empty($driverRouteObj->getRequestForRoute()) && $driverRouteObj->getRequestForRoute()->getApprovedBy()) ? true : false,
            'reasonForReqest' => (!empty($driverRouteObj->getRequestForRoute()) && $driverRouteObj->getRequestForRoute()->getReasonForRequest()) ? $driverRouteObj->getRequestForRoute()->getReasonForRequest() : '',
            'isSentHome' => $driverRouteObj->getIsSentHome(),
            'incident' => $incident,
            'newNote' => $driverRouteObj->getNewNote(),
            'currentNote' => $driverRouteObj->getCurrentNote(),
            'mobileVersionUsed' => $driverRouteObj instanceof DriverRoute ? $driverRouteObj->getMobileVersionUsed() : "N/A",
            'trainee' => ($driverRouteObj->getIsAddTrain()) ? $driverRoutesDetails : [],
            'paired' => ($driverRouteObj->getIsLightDuty()) ? $driverRoutesDetails : [],
            'isUnscheduledDriver' => ($driverRouteObj->getShiftType()->getCategory() == \App\Entity\Shift::SYSTEM) ? true : false,
            'sheduleName' => ($driverRouteObj->getSchedule()) ? $driverRouteObj->getSchedule()->getName() : '',
            'isSecondShift' => $driverRouteObj->getIsSecondShift() ? true : false,
            'hasSecondshift' => (!empty($driverRouteObj->getOldDriverRoute()) && $driverRouteObj->getOldDriverRoute()->getIsSecondShift() && !$driverRouteObj->getOldDriverRoute()->getIsArchive()) ? true : false,
            'userId' => !empty($driverRouteObj->getDriver()) ? $driverRouteObj->getDriver()->getUser()->getId() : null,
            'email' => !empty($driverRouteObj->getDriver()) ? $driverRouteObj->getDriver()->getUser()->getEmail() : null,
            'isPunchedIn' => !empty($driverRouteObj->getPunchIn()) ? true : false,
            'isPunchedOut' => !empty($driverRouteObj->getPunchOut()) ? true : false,
            'isBreakPunchIn' => !empty($driverRouteObj->getBreakPunchIn()) ? true : false,
            'isBreakPunchOut' => !empty($driverRouteObj->getBreakPunchOut()) ? true : false,
        ];
    }

    /**
     * Create shift data for passed driver route object.
     * @param mixed $driverRouteObj Driver route object.
     * @param int $weekNumber Current Driver Route Object week number.
     * @param int $requestTimeZone
     * @param bool $timeline
     *
     * @return array Shift data array.
     * @throws Exception
     */
    public function getShiftDataNew($driverRouteObj, $weekNumber, $requestTimeZone = 0, $timeline = false)
    {
        $today = (new DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today = $this->getTimeZoneConversation($today, $requestTimeZone);
        $today->setTime(0, 0, 0);

        #Gaurav : Query optimize if need
        $codeArr = [];
        $driverRouteCodes = $this->manager->getRepository(DriverRouteCode::class)->getRouteCodeByRouteId($driverRouteObj['routeId']);
        if (!empty($driverRouteCodes)) {
            $codeArr = array_column($driverRouteCodes,'code');
        }

        $breakPunchTime = '00:00';
        $breakPunchIn = !empty($driverRouteObj['breakPunchIn']) ? new \DateTime($driverRouteObj['breakPunchIn']) : null;
        $breakPunchOut = !empty($driverRouteObj['breakPunchOut']) ? new \DateTime($driverRouteObj['breakPunchOut']) : null;
        if (!empty($breakPunchIn) && !empty($breakPunchOut)) {
            $interval = date_diff($breakPunchIn, $breakPunchOut);
            $breakPunchTime = $interval->format('%H:%I');
        }

        $punchInObj = !empty($driverRouteObj['punchIn']) ? new \DateTime($driverRouteObj['punchIn']) : null;
        $punchOutObj = !empty($driverRouteObj['punchOut']) ? new \DateTime($this->getPunchOutData($driverRouteObj)) : null;

        $driverRouteStartTimeObj = !empty($driverRouteObj['startTime']) ? new \DateTime($driverRouteObj['startTime']) : new \DateTime($driverRouteObj['shift_startTime']);
        $driverRouteEndTimeObj = !empty($driverRouteObj['endTime']) ? new \DateTime($driverRouteObj['endTime']) : new \DateTime($driverRouteObj['shift_endTime']);

        $startTimeObj = !empty($punchInObj) ? $punchInObj : $driverRouteStartTimeObj;
        $endTimeObj = !empty($punchOutObj) ? $punchOutObj : $driverRouteEndTimeObj;



        $endTimeOfPriorShiftObj = $priorShiftType = null;
        if (!empty($driverRouteObj['oldDriverRoute_id'])) {
            //May be not needed
            // $this->manager->detach($driverRouteObj->getOldDriverRoute());
            $endTimeOfPriorShiftObj = new \DateTime($driverRouteObj['oldDriverRoute_endTime']);
            //Get shift type
            $priorShiftType = (!empty($driverRouteObj['oldDriverRoute_isBackup']) ? 'Backup' : ( !empty($driverRouteObj['oldDriverRoute_isRescuer']) ? 'Rescuer': (!empty($driverRouteObj['oldDriverRoute_isAddTrain']) ? 'Trainer': (!empty($driverRouteObj['oldDriverRoute_isLightDuty']) ? 'Duty' : ''))));
        }

        $utcStartTime = $startTimeObj->format('G.i');
        $utcEndTime = $endTimeObj->format('G.i');
        $utcEndTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;

        $startTimeObj = $this->getTimeZoneConversation($startTimeObj, $requestTimeZone);
        $endTimeObj = $this->getTimeZoneConversation($endTimeObj, $requestTimeZone);
        $endTimeOfPriorShiftObj = !empty($endTimeOfPriorShiftObj) ? $this->getTimeZoneConversation($endTimeOfPriorShiftObj, $requestTimeZone) : null;
        if(!empty($driverRouteObj['datetimeEnded'])){
            $endTimeObj = new \DateTime($driverRouteObj['datetimeEnded']);
        }

        $startHour = $startTimeObj->format($this->params->get('time_format'));
        $endHour = $endTimeObj->format($this->params->get('time_format'));



        $startTime = $startTimeObj->format('G.i');
        $endTime = $endTimeObj->format('G.i');
        $endTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;

        $priorShiftRouteId = $driverRouteObj['oldDriverRoute_id'];

         #Gaurav : Now not needed
        // $driverRouteDevice = (method_exists($driverRouteObj, 'getDevice')) ? $driverRouteObj->getDevice() : $driverRouteObj->getRouteId()->getDevice();
        // $driverRouteVehicle = (method_exists($driverRouteObj, 'getVehicle')) ? $driverRouteObj->getVehicle() : $driverRouteObj->getRouteId()->getVehicle();

        $deviceId = $driverRouteObj['device_imei_latest'];
        $vehicleId = $driverRouteObj['vehicle_id'];
        $vehicleUniqueId = $driverRouteObj['vehicle_vehicleId'];
        $vehicleVin = $driverRouteObj['vehicle_vin'];
        $hasDriver = !empty($driverRouteObj['driver_id']) ? true : false;

        $timelineArray = [];

        /* @var $driverRouteEventObj DriverRoute */
        $driverRouteEventObj = $driverRouteObj;

        $driverFullName = 'N/A';
        if($hasDriver){
            $driverFullName = $driverRouteObj['user_friendlyName'];
        }

        //PENDING : Gaurav
        $events = $this->manager->getRepository(Event::class)->getRouteCodeByRouteId($driverRouteObj['routeId']);
        $eventsList = $this->eventsListNew($events, $requestTimeZone, $driverFullName, $deviceId, $vehicleId, $vehicleUniqueId, $vehicleVin, false,false ,false,false );
        $timelineArray = $eventsList[1];

        if(!empty($eventsList[0])) {
            $eventsList2 = $this->eventsListNew(array_reverse($eventsList[0]), $requestTimeZone, $driverFullName, $deviceId, $vehicleId, $vehicleUniqueId, $vehicleVin, $eventsList[2], $eventsList[3], $eventsList[4], $eventsList[5]);
            $timelineArray = array_merge($timelineArray, $eventsList2[1]);
        }
        /*
            TODO:
            - Move events loop to a new function: eventsList()
            - Update events loop to contain more detailed information about the devices.
            - Return $events and $timelineArray

            Proposal:

            $events = $driverRouteEventObj->getEvents()->filter(function (Event $item) {
                return $item->getIsActive() === true;
            });

            // Return loadout events
            $events = $this->eventsList($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '' );
            $timelineArray = $events['timelineArray'];

            // Return RTS
            if(!empty($events)) {
                $events = $this->eventsList($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '' );
                $timelineArray = $events['timelineArray'];
            }
         */

        //Pending : Gaurav
        $incident = null;
        $incidentResult = $this->manager->getRepository(Incident::class)->getIncidentByRouteId($driverRouteObj['routeId']);
        if (!empty($incidentResult)) {
            $incident =  [
                'id' => $incidentResult[0]['id'],
                'title' => $incidentResult[0]['title'],
                'type' => $incidentResult[0]['name'],
                'dateTime' => $incidentResult[0]['dateTime']
            ];
        }

        // Sent home punch
        $dateTimeEnded = !empty($driverRouteObj['datetimeEnded']) ? new \DateTime($driverRouteObj['datetimeEnded']) : null;
        if (!empty($dateTimeEnded) && !empty($incident)) {
            $timelineArray[] = $this->buildTimeline(
                'sentHome',
                substr(($this->getTimeZoneConversation($dateTimeEnded, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                $driverFullName . ' was sent home.',
                $deviceId,
                $vehicleId,
                $vehicleUniqueId,
                $vehicleVin
            );
        }

        if ($timeline && $hasDriver) {
            if (!empty($punchInObj)) {
                $timelineArray[] = $this->buildTimeline(
                    'PunchIn',
                    substr($startHour, 0, -1),
                    $driverFullName . ' has punched in successfully.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($breakPunchIn)) {
                $timelineArray[] = $this->buildTimeline(
                    'LunchIn',
                    substr(($this->getTimeZoneConversation($breakPunchIn, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has punched out for break.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($breakPunchOut)) {
                $timelineArray[] = $this->buildTimeline(
                    'LunchOut',
                    substr(($this->getTimeZoneConversation($breakPunchOut, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has punched in from break.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($dateTimeEnded)) {
                $endTimeObj = $this->getTimeZoneConversation($dateTimeEnded, $requestTimeZone);
                $endHour = $endTimeObj->format($this->params->get('time_format'));
                $endTime = $endTimeObj->format('G.i');

                $timelineArray[] = $this->buildTimeline(
                    'CheckSquare',
                    substr(
                        $endHour,
                        0,
                        -1
                    ),
                    'Shift type ended by ' . !empty($driverRouteObj['updatedBy_friendly_name']) ? $driverRouteObj['updatedBy_friendly_name'] : '',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }

            if (!empty($punchOutObj)) {
                $timelineArray[] = $this->buildTimeline(
                    'PunchOut',
                    substr($endHour, 0, -1),
                    $driverFullName . ' has punched out for the day.',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );
            }
        }

        usort($timelineArray, function($a, $b) {
            return (new DateTime($a['eventTime'] . 'm')) <=> (new DateTime($b['eventTime'] . 'm'));
        });

        $isExempt = !empty($hasDriver) ? ($driverRouteObj['es_name'] == 'Exempt' ? true : false) : false;
        $currentRequestType = '';
        if(!empty($driverRouteObj['requestForRoute_id'])){
            if(!empty($driverRouteObj['requestForRoute_isSwapRequest'])){
                $currentRequestType = 'swap_request';
            } else if(!empty($driverRouteObj['requestForRoute_driver_id'])){
                $currentRequestType = 'shift_request';
            } else {
                $currentRequestType = 'drop_request';
            }
        }

        //Pending : Gaurav
        //Get driver routes details
        $oldDriverRoutes = $this->manager->getRepository(DriverRoute::class)->getOldDriverRouteByRouteId($driverRouteObj['routeId']);
        $driverRoutesDetails = [];
        if (!empty($oldDriverRoutes)) {
            foreach ($oldDriverRoutes as $driverRoutesData) {
               if (!empty($driverRoutesData['dri_id'])) {
                    $driverRoutesDetails[] = [
                                'id' => $driverRoutesData['id'],
                                'driverId' =>  $driverRoutesData['dri_id'],
                                'driverName' => $driverRoutesData['friendlyName'],
                    ];
                }
            }
        }

        /*if($driverRouteObj['isRescuer'] == 1){
            dd($driverRouteObj);
        }*/
        $isSentHome = $this->manager->getRepository(Event::class)->findIsSentHome($driverRouteObj['id']);
        return [
            'id' => (int)$driverRouteObj['id'],
            'hour' => $startHour . '-' . $endHour,
            'startTime' => (Float) $startTime,
            'endTime' => (Float) $endTime,
            'utcStartTime' => (Float) $utcStartTime,
            'utcEndTime' => (Float) $utcEndTime,
            'endTimeOfPriorShift' => (Float) $endTimeOfPriorShift,
            'utcEndTimeOfPriorShift' => (Float) $utcEndTimeOfPriorShift,
            'dateTimeEnded' => !empty($dateTimeEnded) ? $dateTimeEnded->format('G.i') : null,
            'typeId' => (int)$driverRouteObj['shift_id'],
            'type' => !empty($driverRouteObj['shiftName']) ? $driverRouteObj['shiftName'] : $driverRouteObj['shift_name'],
            'category' => (int)$driverRouteObj['shift_category'],
            'bg' => !empty($driverRouteObj['shiftColor']) ? $driverRouteObj['shiftColor'] : $driverRouteObj['shift_color'],
            'color' => !empty($driverRouteObj['shiftTextColor']) ? $driverRouteObj['shiftTextColor'] : $driverRouteObj['shift_textColor'],
            'skill' => (int)$driverRouteObj['skill_id'],
            'note' => !empty($driverRouteObj['shiftNote']) ? $driverRouteObj['shiftNote'] : $driverRouteObj['note'],
            'countOfTotalStops' => $driverRouteObj['countOfTotalStops'],
            'completedCountOfStops' => $driverRouteObj['completedCountOfStops'],
            'countOfTotalPackages' => $driverRouteObj['countOfTotalPackages'],
            'countOfAttemptedPackages' => $driverRouteObj['countOfAttemptedPackages'],
            'countOfReturningPackages' => $driverRouteObj['countOfReturningPackages'],
            'countOfDeliveredPackages' => $driverRouteObj['countOfDeliveredPackages'],
            'countOfMissingPackages' => $driverRouteObj['countOfMissingPackages'],
            'percent' => '0%',
            'time' => $breakPunchTime,
            'isNew' => !empty($driverRouteObj['isNew']) ? true : false,
            'hasAlert' => !empty($driverRouteObj['isNew']) ? true : false,
            'currentWeek' => $weekNumber,
            'originalDay' => date('w', strtotime($driverRouteObj['dateCreated'])),
            'isOpenShift' => ($driverRouteObj['isOpenShift'] == 1) ? true : false,
            'stationId' => (int)$driverRouteObj['station_id'],
            'routeId' => !empty($driverRouteObj['routeId']) ? (int)$driverRouteObj['routeId'] : null,
            'priorShiftRouteId' => (int)$priorShiftRouteId,
            'priorShiftType' => $priorShiftType,
            'shiftDate' => (new \DateTime($driverRouteObj['dateCreated']))->format('Y-m-d'),
            'driverId' => !empty($driverRouteObj['driver_id']) ? (int)$driverRouteObj['driver_id'] : '0.0.0',
            'driver' => !empty($driverRouteObj['driver_id']) ? [
                "name" => $driverRouteObj['user_friendlyName'],
                "title" => $this->getPosition($driverRouteObj['driver_user_role_name']),
                "img" => $driverRouteObj['user_profileImage'],
                "employeeStatus" => $driverRouteObj['driver_employeeStatus'] == Driver::STATUS_ACTIVE ? true : false,
            ] : [],
            'routeCode' => $codeArr,
            'timezone' => $driverRouteObj['station_timezone'],
            'isTemp' => ($driverRouteObj['isTemp'] == 1) ? true : false,
            'canDelete' => $today <= new \DateTime($driverRouteObj['dateCreated']),
            'showTimeline' => $today < new \DateTime($driverRouteObj['dateCreated']),
            'isRescued' => ($driverRouteObj['isRescued'] == 1) ? true : false,
            'isRescuer' => ($driverRouteObj['isRescuer'] == 1) ? true : false,
            'isDuty' => ($driverRouteObj['isLightDuty'] == 1) ? true : false,
            'isTrain' => ($driverRouteObj['isAddTrain'] == 1) ? true : false,
            'numberOfPackage' => $driverRouteObj['numberOfPackage'],
            'invoiceTypeId' => !empty($driverRouteObj['shiftInvoiceType_id']) ? (int)$driverRouteObj['shiftInvoiceType_id'] : null,
            'timeline' => $timelineArray,
            // 'driverStation' => !empty($driverRouteObj->getDriver()) ? $driverRouteObj->getDriver()->getStations()->first()->getId() : null,
            'isExempt' => $isExempt,
            'isBackup' => ($driverRouteObj['shift_category'] == \App\Entity\Shift::BACKUP),
            'isRequested' => !empty($driverRouteObj['requestForRoute_id']) ? true:false,
            'currentRequestType' => $currentRequestType,
            'isRequestCompleted' => ($driverRouteObj['requestForRoute_isRequestCompleted'] == 1) ? true :false,
            'requestApprovedBy' => (!empty($driverRouteObj['requestForRoute_reasonForRequest']) && !empty($driverRouteObj['requestForRoute_user_id'])) ? true : false,
            'reasonForReqest' => (!empty($driverRouteOb['requestForRoute_id']) && $driverRouteObj['requestForRoute_reasonForRequest']) ? $driverRouteObj['requestForRoute_reasonForRequest'] : '',
            'isSentHome' => $isSentHome,
            'incident' => $incident,
            'newNote' => $driverRouteObj['newNote'],
            'currentNote' => $driverRouteObj['currentNote'],
            'mobileVersionUsed' => ($driverRouteObj['isNew'] == 0) ? $driverRouteObj['mobileVersionUsed'] : "N/A",
            'trainee' => !empty($driverRouteObj['isAddTrain']) ? $driverRoutesDetails : [],
            'paired' => !empty($driverRouteObj['isLightDuty']) ? $driverRoutesDetails : [],
            'isUnscheduledDriver' => ($driverRouteObj['shift_category'] == \App\Entity\Shift::SYSTEM) ? true : false,
            'sheduleName' => !empty($driverRouteObj['schedule_id']) ? $driverRouteObj['schedule_name'] : '',
            'isSecondShift' => !empty($driverRouteObj['isSecondShift']) ? true : false,
            'hasSecondshift'=> (!empty($driverRouteObj['oldDriverRoute_id']) && !empty($driverRouteObj['oldDriverRoute_isSecondShift']) && ($driverRouteObj['oldDriverRoute_isArchive'] == '0')) ? true : false,
            'userId' => !empty($driverRouteObj['driver_id']) ? (int)$driverRouteObj['user_id'] : null,
            'email' => !empty($driverRouteObj['driver_id']) ? $driverRouteObj['user_email'] : null,
            'isPunchedIn' => !empty($punchInObj) ? true : false,
            'isPunchedOut' => !empty($punchOutObj) ? true : false,
            'isBreakPunchIn' => !empty($breakPunchIn) ? true : false,
            'isBreakPunchOut' => !empty($breakPunchOut) ? true : false
        ];
    }

    public function eventsList($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '', $firstInsideParking = false, $firstInsideStation = false, $firstOutsideParking = false, $firstOutsideStation = false)
    {
        $timelineArray = [];
        $eventCount = 0;

        // $firstInsideParking = false;
        // $firstInsideStation = false;

        // $firstOutsideParking = false;
        // $firstOutsideStation = false;

        foreach ($events as $event) {
            //$eventTime = substr(($this->getTimeZoneConversation($event->getCreatedAt(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1);
            // Entering parking lot event
            if (($event->getEventName() === 'Inside Parking Lot !!!' || $event->getName() === 'INSIDE_PARKING_LOT') && $firstInsideParking === false) {
                $timelineArray[] = $this->buildTimeline(
                    'ParkingGeoIn',
                    substr(($this->getTimeZoneConversation($event->getCreatedAt(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has entered the parking lot',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstInsideParking = true;
            }

            // Exiting parking lot event
            if (($event->getEventName() === 'Outside Parking Lot !!!' || $event->getName() === 'OUTSIDE_PARKING_LOT') && $firstInsideParking === true && $firstOutsideParking === false) {
                $timelineArray[] = $this->buildTimeline(
                    'ParkingGeoOut',
                    substr(($this->getTimeZoneConversation($event->getCreatedAt(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has exited the parking lot',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstOutsideParking = true;
            }

            // Enter station event
            if (($event->getEventName() === 'Inside Station !!!' || $event->getName() === 'INSIDE_STATION') && $firstInsideStation === false) {
                $timelineArray[] = $this->buildTimeline(
                    'GeoferenceIn',
                    substr(($this->getTimeZoneConversation($event->getCreatedAt()->add(\DateInterval::createfromdatestring('+1 minute')), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has entered the station',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstInsideStation = true;
            }

            // Exist station event
            if (($event->getEventName() === 'Outside Station !!!' || $event->getName() === 'OUTSIDE_STATION') && $firstInsideStation === true && $firstOutsideStation === false) {
                $timelineArray[] = $this->buildTimeline(
                    'GeoferenceOut',
                    substr(($this->getTimeZoneConversation($event->getCreatedAt(), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has exited the station',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstOutsideStation = true;
            }

            unset($events[$event->getId()]);
            // No more punches for this group
            if ($firstInsideStation && $firstInsideParking && $firstOutsideParking && $firstOutsideStation) {
                break;
            }
        }

        return array(
            $events,
            $timelineArray,
            $firstInsideParking,
            $firstInsideStation,
            $firstOutsideParking,
            $firstOutsideStation
        );
    }

    public function eventsListNew($events, $requestTimeZone, $driverFullName, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '', $firstInsideParking = false, $firstInsideStation = false, $firstOutsideParking = false, $firstOutsideStation = false)
    {
        $timelineArray = [];
        $eventCount = 0;

        // $firstInsideParking = false;
        // $firstInsideStation = false;

        // $firstOutsideParking = false;
        // $firstOutsideStation = false;

        foreach($events as $event)
        {
            //$eventTime = substr(($this->getTimeZoneConversation($event['name'], $requestTimeZone)->format($this->params->get('time_format'))), 0, -1);
            // Entering parking lot event
            $eventCreatedDate = new \DateTime($event['createdAt']);
            if( ( $event['eventName'] === 'Inside Parking Lot !!!' || $event['name'] === 'INSIDE_PARKING_LOT') && $firstInsideParking === false) {
                $timelineArray[] = $this->buildTimeline(
                    'ParkingGeoIn',
                    substr(($this->getTimeZoneConversation($eventCreatedDate, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has entered the parking lot',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstInsideParking = true;
            }

            // Exiting parking lot event
            if( ($event['eventName'] === 'Outside Parking Lot !!!' || $event['name'] === 'OUTSIDE_PARKING_LOT' ) && $firstInsideParking === true && $firstOutsideParking === false) {
                $timelineArray[] = $this->buildTimeline(
                    'ParkingGeoOut',
                    substr(($this->getTimeZoneConversation($eventCreatedDate, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has exited the parking lot',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstOutsideParking = true;
            }

            // Enter station event
            if(($event['eventName'] === 'Inside Station !!!' || $event['name'] === 'INSIDE_STATION') && $firstInsideStation === false) {
                $timelineArray[] = $this->buildTimeline(
                    'GeoferenceIn',
                    substr(($this->getTimeZoneConversation($eventCreatedDate->add(\DateInterval::createfromdatestring('+1 minute')), $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has entered the station',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstInsideStation = true;
            }

            // Exist station event
            if(($event['eventName'] === 'Outside Station !!!' || $event['name'] === 'OUTSIDE_STATION') && $firstInsideStation === true && $firstOutsideStation === false) {
                $timelineArray[] = $this->buildTimeline(
                    'GeoferenceOut',
                    substr(($this->getTimeZoneConversation($eventCreatedDate, $requestTimeZone)->format($this->params->get('time_format'))), 0, -1),
                    $driverFullName . ' has exited the station',
                    $deviceId,
                    $vehicleId,
                    $vehicleUniqueId,
                    $vehicleVin
                );

                $eventCount++;
                $firstOutsideStation = true;
            }

            unset($events[$event['id']]);
            // No more punches for this group
            if($firstInsideStation && $firstInsideParking && $firstOutsideParking && $firstOutsideStation ) {
                break;
            }
        }

        return array(
            $events,
            $timelineArray,
            $firstInsideParking,
            $firstInsideStation,
            $firstOutsideParking,
            $firstOutsideStation
        );
    }

    /**
     * @param $type
     * @param $eventTime
     * @param $message
     * @param string $deviceId
     * @param string $vehicleId
     * @param string $vehicleUniqueId
     * @param string $vehicleVin
     * @return mixed
     */
    public function buildTimeline($type, $eventTime, $message, $deviceId = '', $vehicleId = '', $vehicleUniqueId = '', $vehicleVin = '')
    {
        if (!empty($eventTime)) return
            [
                'type' => $type,
                'eventTime' => $eventTime,
                'description' => $message,
                'device' => [
                    'id' => $deviceId,
                    'uniqueId' => '',
                ],
                'vehicle' => [
                    'id' => $vehicleId,
                    'uniqueId' => $vehicleUniqueId,
                    'vin' => $vehicleVin,
                ],
                'location' => [
                    'lat' => '',
                    'long' => '',
                ],
            ];
    }


    /**
     * Create loadout payload data for passed driver route object.
     * @param mixed $driverRouteObj Driver route object.
     * @param int $nCount sorting order number
     * @param int $requestTimeZone
     *
     * @return array Loadout payload data array.
     * @throws Exception
     */
    public function getLoadoutPayloadData($driverRouteObj, $nCount, $nNotDepartedCount, $nNotPunchInCount, $requestTimeZone = 0)
    {
        $startTime = $this->getTimeZoneConversation($driverRouteObj->getStartTime(), $requestTimeZone)->format($this->params->get('time_format'));
        $endTime = $this->getTimeZoneConversation($driverRouteObj->getEndTime(), $requestTimeZone)->format($this->params->get('time_format'));
        $punchedIn = !empty($driverRouteObj->getPunchIn()) ? $this->getTimeZoneConversation($driverRouteObj->getPunchIn(), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $clockedOut = !empty($driverRouteObj->getPunchOut()) ? $this->getTimeZoneConversation($driverRouteObj->getPunchOut(), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $breakPunchIn = !empty($driverRouteObj->getBreakPunchIn()) ? $this->getTimeZoneConversation($driverRouteObj->getBreakPunchIn(), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $breakPunchOut = !empty($driverRouteObj->getBreakPunchOut()) ? $this->getTimeZoneConversation($driverRouteObj->getBreakPunchOut(), $requestTimeZone)->format($this->params->get('time_format')) : false;

        $mustLeaveUntilTime = $insideStation = false;
        $kickoffLog = (method_exists($driverRouteObj, 'getKickoffLog')) ? $driverRouteObj->getKickoffLog() : $driverRouteObj->getRouteId()->getKickoffLog();
        $stationArrivalAt = (!empty($kickoffLog) && $kickoffLog->getStationArrivalAt()) ? $kickoffLog->getStationArrivalAt() : null;
        $getDateMustLeaveUntil = !empty($stationArrivalAt) ? $this->getTimeZoneConversation($stationArrivalAt, $requestTimeZone)->format('Y-m-d H:i:s') : null;
        if (!empty($getDateMustLeaveUntil)) {
            $mustLeaveUntil = new DateTime($getDateMustLeaveUntil);
            $insideStation = $mustLeaveUntil->format($this->params->get('time_format'));
            $mustLeaveUntil->modify('+20 minutes');
            $mustLeaveUntilTime = $mustLeaveUntil->format($this->params->get('time_format'));
            $nNotDepartedCount++;
        }

        if ($driverRouteObj->getPunchIn() == null) {
            $nNotPunchInCount++;
        }

        //get driver skill
        $driverSkills = [];
        if (!empty($driverRouteObj->getDriver()) && !empty($driverRouteObj->getDriver()->getDriverSkills())) {
            foreach ($driverRouteObj->getDriver()->getDriverSkills() as $skill) {
                if ($skill->getIsArchive() == false && !empty($skill->getSkill()) && $skill->getSkill()->getIsArchive() == false) {
                    $driverSkills[] = $skill->getSkill()->getId();
                }
            }
        }

        $categoryName = Shift::$categories;

        //Load out listings data
        $codeArr = [];
        if (method_exists($driverRouteObj, 'getRouteId')) {
            if (!empty($driverRouteObj->getRouteId())) {
                foreach ($driverRouteObj->getRouteId()->getDriverRouteCodes()->filter(function ($routeCode) {
                    return !$routeCode->getIsArchive();
                }) as $code) {
                    $codeArr[] = $code->getCode();
                }
            }
        } else {
            foreach ($driverRouteObj->getDriverRouteCodes()->filter(function ($routeCode) {
                return !$routeCode->getIsArchive();
            }) as $code) {
                $codeArr[] = $code->getCode();
            }
        }

        $temp = $devicesArray = [];
        if (method_exists($driverRouteObj, 'getDevice')) {
            if (!empty($driverRouteObj->getDevice())) {
                foreach ($driverRouteObj->getDevice() as $device) {
                    $temp['id'] = $device->getId();
                    $temp['name'] = $device->getName();
                    $temp['number'] = $device->getNumber();
                    array_push($devicesArray, $temp);
                }
            }
        } else {
            foreach ($driverRouteObj->getRouteId()->getDevice() as $device) {
                $temp['id'] = $device->getId();
                $temp['name'] = $device->getName();
                $temp['number'] = $device->getNumber();
                array_push($devicesArray, $temp);
            }
        }

        $vehicleObj = (method_exists($driverRouteObj, 'getVehicle')) ? $driverRouteObj->getVehicle() : $driverRouteObj->getRouteId()->getVehicle();
        $vehicle = !empty($vehicleObj) ? ['id' => $vehicleObj->getId(), 'unit' => $vehicleObj->getVehicleId()] : [];

        $rescuedDriverRoutes = null;

        if ($driverRouteObj->getIsRescuer() == true) {
            $rescuedDriverRoutes = $this->manager->getRepository(DriverRoute::class)->getRescuedRouteByRouteCodeAndDate($driverRouteObj->getDateCreated(), $codeArr[0]);
        }

        if (!empty($rescuedDriverRoutes)) {
            $shiftId = $rescuedDriverRoutes->getShiftType()->getId();
            $shiftName = $rescuedDriverRoutes->getShiftName();
            $shiftColor = $rescuedDriverRoutes->getShiftColor();
            $shiftCategory = $rescuedDriverRoutes->getShiftCategory();
            $driverName = $rescuedDriverRoutes->getDriver() ? $driverRouteObj->getDriver()->getFullName() : 'N/A';
        } else {
            $shiftId = $driverRouteObj->getShiftType()->getId();
            $shiftName = $driverRouteObj->getShiftName();
            $shiftColor = $driverRouteObj->getShiftColor();
            $shiftCategory = $driverRouteObj->getShiftCategory();
            $driverName = $driverRouteObj->getDriver() ? $driverRouteObj->getDriver()->getFullName() : 'N/A';
        }

        list($workHours, $estimatedWorkHours) = $this->manager->getRepository(DriverRoute::class)->getWorkHoursEstimatedWorkHours(['id' => $driverRouteObj->getDriver()->getId(), 'date' => $driverRouteObj->getDateCreated()->format('Y-m-d'), 'timezoneOffset' => $requestTimeZone]);

        $startTimeObj = !empty($driverRouteObj->getPunchIn()) ? $driverRouteObj->getPunchIn() : $driverRouteObj->getStartTime();
        $utcStartTime = $startTimeObj->format('G.i');
        $endTimeObj = !empty($driverRouteObj->getPunchOut()) ? $driverRouteObj->getPunchOut() : $driverRouteObj->getEndTime();
        $utcEndTime = $endTimeObj->format('G.i');

        $priorShiftRouteId = !empty($driverRouteObj->getOldDriverRoute()) ? $driverRouteObj->getOldDriverRoute()->getId() : null;

        $incident = null;
        if (!empty($driverRouteObj->getIncidents())) {
            $incidentObj = $driverRouteObj->getIncidents()->filter(function (Incident $item) {
                return $item->getIsArchive() === false;
            });
            if (!empty($incidentObj->first())) {
                $incident = [
                    'id' => $incidentObj->first()->getId(),
                    'title' => $incidentObj->first()->getTitle(),
                    'type' => $incidentObj->first()->getIncidentType()->getName(),
                    'dateTime' => $incidentObj->first()->getDateTime()->format('Y-m-d H:i:s')
                ];
            }
        }

        $payloadData = [
            "id" => (method_exists($driverRouteObj, 'getRouteId')) ? $driverRouteObj->getRouteId()->getId() : $driverRouteObj->getId(),
            "order" => $nCount,
            "name" => $driverRouteObj->getDriver() ? $driverRouteObj->getDriver()->getFullName() : 'N/A',
            "title" => $driverRouteObj->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
            "img" => $driverRouteObj->getDriver()->getUser()->getProfileImage(),
            'userId' => $driverRouteObj->getDriver()->getUser()->getId(),
            "email" => $driverRouteObj->getDriver()->getUser()->getEmail(),
            "schedule" => !empty($driverRouteObj->getSchedule()) ? $driverRouteObj->getSchedule()->getId() : 0,
            "skills" => $driverSkills,
            "shiftType" => [
                "id" => $shiftId,
                "name" => $shiftName,
                "color" => $shiftColor,
                "category" => $shiftCategory,
            ],
            'stationId' => $driverRouteObj->getStation()->getId(),
            "startTime" => $startTime,
            "punchedIn" => $punchedIn,
            "insideStation" => $insideStation,
            "mustLeaveUntil" => $mustLeaveUntilTime,
            "onRoute" => false, // TODO - onRoute = punchedIn + has(route && vehicle && device) + leftStationGeofence
            "onBreak" => $breakPunchIn,
            "breakPunchOut" => $breakPunchOut,
            "returned" => false, //TODO: pull from ReturnToStation entity
            "clockedOut" => $clockedOut,
            "ncns" => false,
            "routes" => $codeArr,
            "vehicle" => $vehicle,
            "devices" => $devicesArray,
            "backup" => (!empty($driverRouteObj->getOldDriver()) && !empty($driverRouteObj->getOldDriverRoute()) && $driverRouteObj->getOldDriverRoute()->getIsBackup()) ?
                [
                    'color' => $driverRouteObj->getOldDriverRoute()->getShiftColor(),
                    'driver' => $driverRouteObj->getOldDriver() ? $driverRouteObj->getOldDriver()->getFullName() : 'N/A',
                ] : false,
            "secondShift" => (!empty($driverRouteObj->getOldDriverRoute()) && $driverRouteObj->getOldDriverRoute()->getIsSecondShift() && !$driverRouteObj->getOldDriverRoute()->getIsArchive()) ?
                [
                    'color' => $driverRouteObj->getOldDriverRoute()->getShiftColor(),
                    'driver' => $driverRouteObj->getOldDriverRoute()->getDriver() ? $driverRouteObj->getOldDriverRoute()->getDriver()->getFullName() : 'N/A',
                ] : false,
            "rescue" => ($driverRouteObj->getIsRescuer() == true) ? ["color" => $driverRouteObj->getShiftColor(), 'driver' => $driverName] : false,
            "isNew" => method_exists($driverRouteObj, 'getIsNew'),
            "driverId" => $driverRouteObj->getDriver()->getId(),
            'workHours' => $workHours,
            'estimatedWorkHours' => $estimatedWorkHours,
            'balanceGroup' => $driverRouteObj->getShiftType()->getBalanceGroup() ? $driverRouteObj->getShiftType()->getBalanceGroup()->getId() : null,
            'isBackup' => $driverRouteObj->getShiftType() ? $driverRouteObj->getShiftType()->getCategory() === 2 ? true : false : false,
            'isSentHome' => $driverRouteObj->getIsSentHome(),
            'dateTimeEnded' => (method_exists($driverRouteObj, 'getRouteId'))
                ? empty($driverRouteObj->getRouteId()) ? null : $driverRouteObj->getRouteId()->getDatetimeEnded()
                : $driverRouteObj->getDatetimeEnded(),
            'dateTimeEndedTimeZoneConversion' => (method_exists($driverRouteObj, 'getRouteId') && !empty($driverRouteObj->getRouteId()->getDatetimeEnded()))
                ? $this->getTimeZoneConversation($driverRouteObj->getRouteId()->getDatetimeEnded(), $requestTimeZone)->format($this->params->get('time_format'))
                : !empty($driverRouteObj->getDatetimeEnded()) ? $this->getTimeZoneConversation($driverRouteObj->getDatetimeEnded(), $requestTimeZone)->format($this->params->get('time_format')) : null,
            'isAddTrain' => ($driverRouteObj->getIsAddTrain() == true) ? true : false,
            'isLightDuty' => ($driverRouteObj->getIsLightDuty() == true) ? true : false,
            'utcStartTime' => $utcStartTime,
            'utcEndTime' => $utcEndTime,
            'priorShiftRouteId' => $priorShiftRouteId,
            'shiftDate' => $driverRouteObj->getDateCreated()->format('Y-m-d'),
            'incident' => $incident,
            'hasBag' => $driverRouteObj->getHasBag(),
            'employeeStatus' => $driverRouteObj->getDriver()->getEmployeeStatus() == Driver::STATUS_ACTIVE ? true : false
        ];

        return [$payloadData, $nNotDepartedCount, $nNotPunchInCount];
    }

    /**
     * Create loadout payload data for passed driver route object.
     * @param mixed $driverRouteObj Driver route object.
     * @param int $nCount sorting order number
     * @param int $requestTimeZone
     *
     * @return array Loadout payload data array.
     * @throws Exception
     */
    public function getLoadoutPayloadDataNew($driverRouteObj, $nCount, $nNotDepartedCount, $nNotPunchInCount, $requestTimeZone = 0)
    {
        $startTime = new \DateTime(!empty($driverRouteObj['startTime']) ? $driverRouteObj['startTime'] : $driverRouteObj['shiftStartTime']);
        $endTime = new \DateTime(!empty($driverRouteObj['endTime']) ? $driverRouteObj['endTime'] : $driverRouteObj['shiftEndTime']);

        $startTime = $this->getTimeZoneConversation($startTime, $requestTimeZone)->format($this->params->get('time_format'));
        $endTime = $this->getTimeZoneConversation($endTime, $requestTimeZone)->format($this->params->get('time_format'));
        $punchedIn = !empty($driverRouteObj['punchIn']) ? $this->getTimeZoneConversation(new \DateTime($driverRouteObj['punchIn']), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $clockedOut = !empty($driverRouteObj['punchOut']) ? $this->getTimeZoneConversation(new \DateTime($this->getPunchOutData($driverRouteObj)), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $breakPunchIn = !empty($driverRouteObj['breakPunchIn']) ? $this->getTimeZoneConversation(new \DateTime($driverRouteObj['breakPunchIn']), $requestTimeZone)->format($this->params->get('time_format')) : false;
        $breakPunchOut = !empty($driverRouteObj['breakPunchOut']) ? $this->getTimeZoneConversation(new \DateTime($driverRouteObj['breakPunchOut']), $requestTimeZone)->format($this->params->get('time_format')) : false;

        $mustLeaveUntilTime = $insideStation = false;


        $stationArrivalAt = !empty($driverRouteObj['kickoffLog']) && !empty($driverRouteObj['stationArrivalAt']) ? new \DateTime(!empty($driverRouteObj['stationArrivalAt'])) : null;
        $getDateMustLeaveUntil = !empty($stationArrivalAt) ? $this->getTimeZoneConversation($stationArrivalAt, $requestTimeZone)->format('Y-m-d H:i:s') : null;
        if (!empty($getDateMustLeaveUntil)) {
            $mustLeaveUntil = new DateTime($getDateMustLeaveUntil);
            $insideStation = $mustLeaveUntil->format($this->params->get('time_format'));
            $mustLeaveUntil->modify('+20 minutes');
            $mustLeaveUntilTime = $mustLeaveUntil->format($this->params->get('time_format'));
            $nNotDepartedCount++;
        }


        if ($driverRouteObj['punchIn'] == null) {
            $nNotPunchInCount++;
        }


        $categoryName = Shift::$categories;
        $codeArr = $driverRouteObj['routeCode'] !='' ? explode(",",$driverRouteObj['routeCode']) : [];

        $devices= $this->manager->getRepository(Device::class)->getDevicesByRouteId($driverRouteObj['id']);
        $temp = $devicesArray = [];
        foreach ($devices as $device) {
            $temp['id'] = (int)$device['id'];
            $temp['name'] = $device['name'];
            $temp['number'] = $device['number'];
            array_push($devicesArray, $temp);
        }

        $vehicle = !empty($driverRouteObj['VehicleId']) ? ['id' => $driverRouteObj['VehicleId'], 'unit' => $driverRouteObj['VehicleUnit']] : [];
        $rescuedDriverRoutes = null;

        if ($driverRouteObj['is_rescuer'] == 1) {
            $rescuedDriverRoutes = $this->manager->getRepository(DriverRoute::class)->getRescuedRouteByRouteCodeAndDate($driverRouteObj['dateCreated'], $codeArr);

        }
        if (!empty($rescuedDriverRoutes)) {
            $shiftId = $rescuedDriverRoutes[0]['shift_id'];
            $shiftName = $rescuedDriverRoutes[0]['shift_name'];
            $shiftColor = $rescuedDriverRoutes[0]['shift_color'];
            $shiftCategory = $rescuedDriverRoutes[0]['shift_category'];
            $driverName = !empty($rescuedDriverRoutes[0]['driver_id']) ? $driverRouteObj['friendly_name'] : 'N/A';
        } else {
            $shiftId = $driverRouteObj['shift_type_id'];
            $shiftName = !empty($driverRouteObj['dr_shift_name']) ? $driverRouteObj['dr_shift_name'] : $driverRouteObj['shift_name'];
            $shiftColor = !empty($driverRouteObj['dr_shift_color']) ? $driverRouteObj['dr_shift_color'] : $driverRouteObj['shift_color'];
            $shiftCategory = !empty($driverRouteObj['dr_shift_category']) ? $driverRouteObj['dr_shift_category'] : $driverRouteObj['shift_category'];
            $driverName = !empty($driverRouteObj['driverId']) ? $driverRouteObj['friendly_name'] : 'N/A';
        }

        list($workHours, $estimatedWorkHours) = $this->manager->getRepository(DriverRoute::class)->getWorkHoursEstimatedWorkHours(['id' => $driverRouteObj['driverId'], 'date' => $driverRouteObj['dateCreated'], 'timezoneOffset' => $requestTimeZone]);

        $startTimeObj = !empty($driverRouteObj['punchIn']) ? new \DateTime($driverRouteObj['punchIn']) : new \DateTime($driverRouteObj['startTime']);
        $utcStartTime = $startTimeObj->format('G.i');
        $endTimeObj = !empty($driverRouteObj['punchOut']) ? new \DateTime($this->getPunchOutData($driverRouteObj)) : new \DateTime($driverRouteObj['endTime']);
        $utcEndTime = $endTimeObj->format('G.i');

        $priorShiftRouteId = !empty($driverRouteObj['oldDriverRoute']) ? $driverRouteObj['oldDriverRoute'] : null;


        $incident = null;
        $incidentResult = $this->manager->getRepository(Incident::class)->getIncidentByRouteId($driverRouteObj['id']);
        if (!empty($incidentResult)) {
            $incident =  [
                'id' => $incidentResult[0]['id'],
                'title' => $incidentResult[0]['title'],
                'type' => $incidentResult[0]['name'],
                'dateTime' => $incidentResult[0]['dateTime']
            ];
        }

        $isSentHome = $this->manager->getRepository(Event::class)->findIsSentHome($driverRouteObj['id']);

        $payloadData = [
            "id" => isset($driverRouteObj['routeId']) ? (int)$driverRouteObj['routeId'] :(int)$driverRouteObj['id'],
            "order" => $nCount,
            "name" => !empty($driverRouteObj['driverId']) ? $driverRouteObj['friendly_name'] : 'N/A',
            "title" => !empty($driverRouteObj['driverId']) ? $this->getPosition($driverRouteObj['driver_user_role_name']): null,
            "img" => !empty($driverRouteObj['driverId']) ? $driverRouteObj['user_profileImage'] : null,
            'userId' => !empty($driverRouteObj['driverId']) ? $driverRouteObj['user_id'] : null,
            "email" => !empty($driverRouteObj['driverId']) ? $driverRouteObj['user_email'] : null,
            "schedule" => !empty($driverRouteObj['scheduleId']) ? $driverRouteObj['scheduleId'] : 0,
            "skills" =>  $driverRouteObj['skillId'] !='' ? explode(",",$driverRouteObj['skillId']) : [],
            "shiftType" => [
                "id" => $shiftId,
                "name" => $shiftName,
                "color" => $shiftColor,
                "category" => (int)$shiftCategory,
            ],
            'stationId' => $driverRouteObj['stationId'],
            "startTime" => $startTime,
            "punchedIn" => $punchedIn,
            "insideStation" => $insideStation,
            "mustLeaveUntil" => $mustLeaveUntilTime,
            "onRoute" => false, // TODO - onRoute = punchedIn + has(route && vehicle && device) + leftStationGeofence
            "onBreak" => $breakPunchIn,
            "breakPunchOut" => $breakPunchOut,
            "returned" => false, //TODO: pull from ReturnToStation entity
            "clockedOut" => $clockedOut,
            "ncns" => false,
            "routes" => $codeArr,
            "vehicle" => $vehicle,
            "devices" => $devicesArray,
            "backup" => !empty($driverRouteObj['oldDriverRoute']) && !empty($driverRouteObj['isBackup']) ?
                [
                    'color' => $driverRouteObj['odrShiftColor'],
                    'driver' => !empty($driverRouteObj['oldDriverId']) ? $driverRouteObj['old_route_user_firstName'].''.$driverRouteObj['old_route_user_lastName'] : 'N/A',
                ] : false,
            "secondShift" => !empty($driverRouteObj['oldDriverRoute']) && !empty($driverRouteObj['isSecondShift']) ?
                [
                    'color' => $driverRouteObj['odrShiftColor'],
                    'driver' => !empty($driverRouteObj['oldDriverId']) ? $driverRouteObj['old_route_user_firstName'].''.$driverRouteObj['old_route_user_lastName'] : 'N/A',
                ] : false,
            "rescue" => ($driverRouteObj['is_rescuer'] == 1) ? ["color" => $driverRouteObj['shift_color'], 'driver' => $driverName] : false,
            "isNew" => !empty($driverRouteObj['isNew']) ? true : false,
            "driverId" => !empty($driverRouteObj['driverId']) ? (int)$driverRouteObj['driverId'] : null,
            'workHours' => $workHours,
            'estimatedWorkHours' => $estimatedWorkHours,
            'balanceGroup' => !empty($driverRouteObj['shift_balance_group']) ? $driverRouteObj['shift_balance_group'] : null,
            'isBackup' => !empty($driverRouteObj['shift_category']) ? $driverRouteObj['shift_category'] == 2 ? true : false: false,
            'isSentHome' => $isSentHome,
            'dateTimeEnded' => $driverRouteObj['datetimeEnded'],
            'dateTimeEndedTimeZoneConversion' => !empty($driverRouteObj['datetimeEnded']) ? $this->getTimeZoneConversation(new \DateTime($driverRouteObj['datetimeEnded']), $requestTimeZone)->format($this->params->get('time_format')) : null ,
            'isAddTrain' => $driverRouteObj['isAddTrain'] == 1 ? true : false,
            'isLightDuty' => $driverRouteObj['isAddTrain'] == 1 ? true : false,
            'utcStartTime' => $utcStartTime,
            'utcEndTime' => $utcEndTime,
            'priorShiftRouteId' => $priorShiftRouteId,
            'shiftDate' => $driverRouteObj['dateCreated'],
            'incident' => $incident,
            'hasBag' => $driverRouteObj['hasBag'] == 1 ? true : false,
            'employeeStatus' => $driverRouteObj['employeeStatus'] == Driver::STATUS_ACTIVE ? true : false

        ];

        return [$payloadData, $nNotDepartedCount, $nNotPunchInCount];
    }

    /**
     * Create incident payload data.
     * @param mixed $incident Incident object.
     * @param int $requestTimeZone
     * @param bool $isList
     *
     * @return array Incident payload data array.
     * @throws Exception
     */
    public function getIncidentloadData($incident, $requestTimeZone = 0, $isList = false)
    {
        //Phase count logic
        $currentPhaseCount = count($incident->getIncidentSubmitedPhases());
        $phaseCount = count($incident->getIncidentType()->getIncidentPhases());
        //Phase Due date
        $allocatedTime = (!empty($incident->getIncidentCurrentPhase()) && !empty($incident->getIncidentCurrentPhase()->getAllocatedTime())) ? $incident->getIncidentCurrentPhase()->getAllocatedTime() : 0;
        if (!empty($incident->getIncidentCompletedPhase())) {
            $dueDateTime = $incident->getIncidentSubmitedPhases()->first()->getSubmittedAt();
        } else {
            $dueDateTime = $incident->getDateTime();
        }
        $dueDateTime->modify("+$allocatedTime minutes");

        $incidentPayload = [
            'id' => $incident->getId(),
            'incidentId' => $incident->getIncidentType()->getPrefix() . '#' . sprintf("%04d", $incident->getId()),
            'title' => $incident->getTitle(),
            'dateTime' => $incident->getDateTime(),
            'station' => [
                'id' => $incident->getStation()->getId(),
                'name' => $incident->getStation()->getName(),
                'code' => $incident->getStation()->getCode()
            ],
            'driver' => [
                'fullName' => $incident->getDriver() ? $incident->getDriver()->getFullName() : 'N/A',
                'id' => $incident->getDriver()->getId(),
                'jobTitle' => $incident->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                'userStatus' => $incident->getDriver()->getUser()->getIsEnabled()
            ],
            'incidentType' => [
                'id' => $incident->getIncidentType()->getId(),
                'name' => $incident->getIncidentType()->getName()
            ],
            'phasesCount' => $phaseCount,
            'currentPhaseCount' => $currentPhaseCount,
            'currentPhaseDescription' => !empty($incident->getIncidentCompletedPhase()) ? $incident->getIncidentCompletedPhase()->getTitle() : '',
            'nextDueDate' => $dueDateTime,
            'dueStatus' => $dueDateTime,
            'isCompleted' => ($phaseCount == $currentPhaseCount)
        ];

        if ($incident->getDriverRoute() && !$isList)
            $incidentPayload['driverRoute'] = $this->getShiftData($incident->getDriverRoute(), $incident->getDriverRoute()->getDateCreated()->format('w'));

        return $incidentPayload;
    }

    /**
     * Create incident payload data.
     * @param mixed $incident Incident object.
     * @param int $requestTimeZone
     * @param bool $isList
     *
     * @return array Incident payload data array.
     * @throws Exception
     */
    public function getAllIncidentloadData($incident, $requestTimeZone = 0, $isList = false)
    {
        $this->stopwatch->start('loadout');
        //Phase count logic
        $currentPhaseCount = $incident['incident_submited_phase_count'];
        $phaseCount = $incident['incident_total_phase_count'];
        //Phase Due date
        $incidentDueDate = (new \DateTime($incident['date_time']))->setTimezone(new \DateTimeZone('UTC'));
        $allocatedTime = !empty($incident['incident_allocated_time']) ? $incident['incident_allocated_time'] : 0;
        if ($incident['incident_completed_phase_id'] > 0) {
            $dueDateTime = !empty($incident['incident_submited_phase_submited_at']) ? (new \DateTime($incident['incident_submited_phase_submited_at']))->setTimezone(new \DateTimeZone('UTC')) : 0;
        } else {
            $dueDateTime = $incidentDueDate;
        }
        $dueDateTime->modify("+$allocatedTime minutes");
        $incidentPayload = [
            'id' => (int)$incident['id'],
            'incidentId' => $incident['type_prefix'] . '#' . sprintf("%04d", $incident['id']),
            'title' => $incident['title'],
            'dateTime' => $incidentDueDate,
            'station' => [
                'id' => (int)$incident['station_id'],
                'name' => $incident['station_name'],
                'code' => $incident['station_code']
            ],
            'driver' => [
                'fullName' => $incident['user_friendlyName'],
                'id' => (int)$incident['driver_id'],
                'jobTitle' => !empty($incident['driver_user_role_name']) ? $this->getPosition($incident['driver_user_role_name']) : 'Owner',
                'userStatus' => (int)$incident['user_isEnabled'],
                'driverIsArchived' => (int)$incident['driver_isArchive']
            ],
            'incidentType' => [
                'id' => (int)$incident['type_id'],
                'name' => $incident['type_name']
            ],
            'phasesCount' => (int)$phaseCount,
            'currentPhaseCount' => (int)$currentPhaseCount,
            'currentPhaseDescription' => $incident['incident_completed_phase_title'],
            'nextDueDate' => $dueDateTime,
            'dueStatus' => $dueDateTime,
            'isCompleted' => ($phaseCount == $currentPhaseCount)
        ];
        if ($incident['driver_route_id'] > 0 && !$isList) {
            $driverRoute = $this->manager->getRepository(DriverRoute::class)->find($incident['driver_route_id']);
            $incidentPayload['driverRoute'] = $this->getShiftData($driverRoute, $driverRoute->getDateCreated()->format('w'));
        }
        $this->stopwatch->stop('loadout');
        return $incidentPayload;
    }

    public function plainPassword()
    {
        $numbers = array_rand(range(0, 9), rand(2, 2));
        $uppercase = array_rand(array_flip(range('A', 'Z')), rand(2, 2));
        $lowercase = array_rand(array_flip(range('a', 'z')), rand(2, 2));
        $special = array_rand(array_flip(['@', '#', '$', '!', '%', '*', '?', '&']), rand(2, 2));
        $password = array_merge(
            $numbers,
            $uppercase,
            $lowercase,
            $special
        );
        shuffle($password);
        return implode($password);
    }

    /**
     * @param array $criteria
     * @param string $entityName
     * @return KickoffLog|ReturnToStation
     * @throws Exception
     */
    public function validateFieldsSetMileage(array $criteria, string $entityName)
    {
        if (empty($criteria['id'])) {
            throw new Exception('id missing.');
        }
        if (empty($criteria['loggedMileageAndGasAt'])) {
            throw new Exception('loggedMileageAndGasAt missing.');
        }
        if (empty($criteria['mileage'])) {
            throw new Exception('mileage missing.');
        }
        if (empty($criteria['gasTankLevel'])) {
            throw new Exception('gasTankLevel missing.');
        }
        $entity = null;
        if ($entityName === 'KickoffLog') {
            $entity = $this->manager->find(KickoffLog::class, $criteria['id']);
        } elseif ($entityName === 'ReturnToStation') {
            $entity = $this->manager->find(ReturnToStation::class, $criteria['id']);
        }
        if (is_null($entity)) {
            throw new Exception($entityName . ' not found.');
        }
        $entity->setGasTankLevel($criteria['gasTankLevel']);
        $entity->setMileage($criteria['mileage']);
        $entity->setLoggedMileageAndGasAt(new DateTime($criteria['loggedMileageAndGasAt']));
        $this->manager->flush();

        return $entity;
    }

    /**
     * @param string $role
     * @return string
     */
    public function getPosition(string $role)
    {
        $roleName = str_replace("ROLE_", "", $role);
        $roleName = str_replace("_", " ", $roleName);
        $roleName = ucwords(strtolower($roleName));
        $roleName = str_replace('Hr ', 'HR ', $roleName);
        return $roleName;
    }

    /**
     * @param string|null $value
     * @return DateTime
     * @throws Exception
     */
    public function getDateTimeObject(?string $value)
    {
        $date = new DateTime('now');
        if ($value) {
            $date = new DateTime($value);
        }
        return $date;
    }

    public function vinCheckLength(string $vin)
    {
        if (strlen($vin) > 17) {
            $vinResult = ltrim($vin, 'Ii');
            if (strlen($vinResult) > 17) {
                return substr($vinResult, 0, 17);
            } else {
                return $vinResult;
            }
        }
        return $vin;
    }

    public function getVinFromInput(string $vin)
    {
        $vinResult = $vin;
        if (strlen($vin) > 17) {
            if (strpos($vin, ',') !== false) {
                // Fix comma delimited issues:
                // 1GCWGAFP1L1145188,2020,CG23405,11,19,08,XBTJS3, AK5 AR7 AXK C6P C60 DE5 DRJ EF7 FE9 FE9 FHO GAZ GU6 G80 I20 JH6 K68 LV1 MAH M5U NTB QB5 U0F V8D WEN WML X88 ZLP ZW9 ZX2 1WT 1WT 6AK 7AK 93I 93W,8624,,,,ZY1
                $vinArray = explode(',', $vin);
                $vinResult = $vinArray[0]; // Get the first item in the array - probably the be a VIN number
                $vinResult = $this->vinCheckLength($vinResult);
            } elseif (strpos($vin, '=') !== false) {
                // Fix url issues:
                //  HTTP://ONRAMP.EHI.COM/RAM/PM15/LRMC/US/?V=3C6TRVAG9LE105585
                $vinArray = explode('=', $vin);
                $vinResult = $vinArray[1]; // Get the second item in the array - probably the be a VIN number
                $vinResult = $this->vinCheckLength($vinResult);
            } else {
                return $this->vinCheckLength($vinResult);
            }
        }
        return $vinResult;
    }

    /**
     * @param array $data
     * @param bool $exception
     * @param string $key
     * @return Device|null
     * @throws Exception
     */
    public function getDevice(array $data, bool $exception = false, string $key = "deviceId")
    {
        $device = null;
        if (!empty($data['deviceId'])) {
            $device = $this->manager->getRepository(Device::class)->findOneBy(['id' => $data['deviceId'], 'isArchive' => false]);
        }
        if (!empty($data['id'])) {
            $device = $this->manager->getRepository(Device::class)->findOneBy(['id' => $data['id'], 'isArchive' => false]);
        }
        if ($exception) {
            if (empty($data[$key])) {
                throw new Exception($key . ' field missing');
            }
            if (!($device instanceof Device)) {
                throw new Exception('Device not found.');
            }
        }
        return $device;
    }

    /**
     * @param array $data
     * @param bool $exception
     * @param string $key
     * @return Vehicle|null
     * @throws Exception
     */
    public function getVehicle(array $data, bool $exception = false, string $key = "vehicleId")
    {
        $vehicle = null;
        if (!empty($data['deviceId'])) {
            $vehicle = $this->manager->getRepository(Vehicle::class)->findOneBy(['id' => $data['deviceId'], 'isArchive' => false]);
        }
        if (!empty($data['id'])) {
            $vehicle = $this->manager->getRepository(Vehicle::class)->findOneBy(['id' => $data['id'], 'isArchive' => false]);
        }
        if ($exception) {
            if (empty($data[$key])) {
                throw new Exception($key . ' field missing');
            }
            if (!($vehicle instanceof Vehicle)) {
                throw new Exception('Device not found.');
            }
        }
        return $vehicle;
    }

    /**
     * @return User
     * @throws Exception
     */
    public function getTokenUser()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof User) {
            return $user;
        }
        throw new Exception('User not found.');
    }

    /**
     * @param string $key
     * @return string
     */
    public function getIconType(string $key)
    {
        $eventsIcons = [
            "DRIVER_ROUTE_CHECK" => 'Play',
            "LOAD_OUT" => 'Play',
            "RTS" => 'Play',
            "UNSCHEDULED_DRIVER_ROUTE" => 'Play',
            "VEHICLE_ATTACHED" => 'Backup',
            "INSIDE_STATION" => 'GeoferenceIn',
            "OUTSIDE_STATION" => 'GeoferenceOut',
            "INSIDE_PARKING_LOT" => 'ParkingGeoIn',
            "OUTSIDE_PARKING_LOT" => 'ParkingGeoOut',
            "GAS" => 'VehicleOil',
            "MILEAGE" => 'VehicleOil',
            "BREAK_PUNCH_OUT" => "LunchOut",
            "BREAK_PUNCH_IN" => "LunchIn",
            "PUNCH_IN" => "PunchIn",
            "PUNCH_OUT" => "PunchOut",
        ];

        if (isset($eventsIcons[$key])) {
            return $eventsIcons[$key];
        }

        return 'Play';
    }

    /**
     * @param int $length
     * @return string
     */
    public function getUniqueIdentifier($length = 8)
    {
        return bin2hex(randombytes_buf($length));
    }


    public function getPunchOutData($route, $dailySummary = null)
    {
        if (is_null($dailySummary)) {
            $now = new \DateTime('now', new \DateTimeZone($this->getTimeZoneName($route['station_timezone'])));
        } else {
            $now = new \DateTime('now');
        }

        $now->setTime(0, 0, 0);
        if (empty($route['punchOut']) && $route['breakPunchOut'] && empty($route['breakPunchOut']) && new \DateTime($route['breakPunchIn']) < $now)
            return $route['breakPunchIn'];
        else
            return $route['punchOut'];
    }

    public function getTimeZoneName($timezone) {
        $timezones['ET'] = 'America/Detroit';
        $timezones['CT'] = 'America/Chicago';
        $timezones['MT'] = 'America/Denver';
        $timezones['PT'] = 'America/Los_Angeles';
        $timezones['AKT'] = 'US/Alaska';
        $timezones['HT'] = 'Pacific/Honolulu';

        if($timezone !== null){
            return $timezones[$timezone];
        }
        return 'America/Detroit';
    }
}
