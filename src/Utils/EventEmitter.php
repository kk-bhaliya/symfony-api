<?php

namespace App\Utils;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;

class EventEmitter {

    /**
     * Mercure topic suffix
     * @var string
     */
    private $topic;

    /**
     * Event emitter identifier
     * @var int|string
     */
    private $emitter;

    /**
     * Event identifier
     * @var string
     */
    private $event;

    /**
     * Event data
     * @var mixed
     */
    private $data;

    private $params;
    private $bus;

    public function __construct(
        ContainerBagInterface $params,
        MessageBusInterface $bus
    )
    {
        $this->params = $params;
        $this->bus = $bus;
    }

    /**
     * @return EventEmitter
     */
    public function create(): EventEmitter
    {
        return new $this($this->params, $this->bus);
    }

    public function dispatch()
    {
        $update = new Update(
            $this->params->get('mercure_prefix') . $this->getTopic(),
            json_encode([
                'emitter' => $this->getEmitter(),
                'event' => $this->getEvent(),
                'data' => $this->getData()
            ])
        );

        $this->bus->dispatch($update);
    }

    /**
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @param string|array $topic topic or [company, station, topic]
     * @return EventEmitter
     * @throws \Exception
     */
    public function setTopic($topic): EventEmitter
    {
        if (is_array($topic)) {
            if (!isset($topic['company']) || !isset($topic['station']) || !isset($topic['topic']))
                throw new \Exception('Wrong topic setup.');

            $topic = "{$topic['company']}/{$topic['station']}/{$topic['topic']}";
        }

        $this->topic = $topic;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getEmitter()
    {
        return $this->emitter;
    }

    /**
     * @param int|string $emitter
     * @return EventEmitter
     */
    public function setEmitter($emitter): EventEmitter
    {
        $this->emitter = $emitter;
        return $this;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     * @return EventEmitter
     */
    public function setEvent(string $event): EventEmitter
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return EventEmitter
     */
    public function setData($data): EventEmitter
    {
        $this->data = $data;
        return $this;
    }
}
