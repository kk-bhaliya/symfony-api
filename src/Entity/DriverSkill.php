<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DriverSkillRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DriverSkill
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="driverSkills")
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="driverSkills")
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $hourlyRate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SkillRate", inversedBy="driverSkills")
     * @MaxDepth(1)
     */
    private $skillRate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(?Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getHourlyRate(): ?float
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(?float $hourlyRate): self
    {
        $this->hourlyRate = $hourlyRate;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getSkillRate(): ?SkillRate
    {
        return $this->skillRate;
    }

    public function setSkillRate(?SkillRate $skillRate): self
    {
        $this->skillRate = $skillRate;

        return $this;
    }
}
