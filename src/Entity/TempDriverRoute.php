<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TempDriverRouteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TempDriverRoute
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateCreated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $shiftTime;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $routeCode;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRescuer = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRescued = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shift", inversedBy="tempDriverRoutes")
     * @ORM\JoinColumn(name="shift_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @MaxDepth(1)
     */
    private $shiftType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Schedule", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $schedule;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNew;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DriverRoute", inversedBy="tempDriverRoutes")
     * @ORM\JoinColumn(nullable=true,onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $routeId;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $createdBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOpenShift = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLoadOut = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="oldTempDriverRoutes")
     * @MaxDepth(1)
     */
    private $oldDriver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DriverRoute", inversedBy="oldTempDriverRoutes")
     * @MaxDepth(1)
     */
    private $oldDriverRoute;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isBackup = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTemp = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftColor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unpaidBreak;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $shiftNote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InvoiceType", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $shiftInvoiceType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="tempDriverRouteShiftStation")
     * @MaxDepth(1)
     */
    private $shiftStation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="tempDriverRouteShiftSkill")
     * @MaxDepth(1)
     */
    private $shiftSkill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $shiftCategory;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BalanceGroup", inversedBy="tempDriverRoutes")
     * @MaxDepth(1)
     */
    private $shiftBalanceGroup;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftTextColor;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $numberOfPackage = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentNote;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $newNote;

    public function __construct()
    {
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(?Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getPunchIn(): ?\DateTimeInterface
    {
        return $this->getRouteId()->getPunchIn();
    }

    public function setPunchIn(?\DateTimeInterface $punchIn): self
    {
        $this->getRouteId()->setPunchIn($punchIn);

        return $this;
    }

    public function getPunchOut(): ?\DateTimeInterface
    {
        return $this->getRouteId()->getPunchOut();
    }

    public function setPunchOut(?\DateTimeInterface $punchOut): self
    {
        $this->getRouteId()->setPunchOut($punchOut);

        return $this;
    }

    public function getShiftTime(): ?\DateTimeInterface
    {
        return $this->shiftTime;
    }

    public function setShiftTime(?\DateTimeInterface $shiftTime): self
    {
        $this->shiftTime = $shiftTime;

        return $this;
    }

    public function getBreakPunchOut(): ?\DateTimeInterface
    {
        return $this->getRouteId()->getBreakPunchOut();
    }

    public function setBreakPunchOut(?\DateTimeInterface $breakPunchOut): self
    {
        $this->getRouteId()->setBreakPunchOut($breakPunchOut);

        return $this;
    }

    public function getBreakPunchIn(): ?\DateTimeInterface
    {
        return $this->getRouteId()->getBreakPunchIn();
    }

    public function setBreakPunchIn(?\DateTimeInterface $breakPunchIn): self
    {
        $this->getRouteId()->setBreakPunchIn($breakPunchIn);

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getRouteCode(): ?string
    {
        return $this->routeCode;
    }

    public function setRouteCode(?string $routeCode): self
    {
        $this->routeCode = $routeCode;

        return $this;
    }

    public function getIsRescuer(): ?bool
    {
        return $this->isRescuer;
    }

    public function setIsRescuer(?bool $isRescuer): self
    {
        $this->isRescuer = $isRescuer;

        return $this;
    }

    public function getIsRescued(): ?bool
    {
        return $this->isRescued;
    }

    public function setIsRescued(?bool $isRescued): self
    {
        $this->isRescued = $isRescued;

        return $this;
    }

    public function getShiftType(): ?Shift
    {
        return $this->shiftType;
    }

    public function setShiftType(?Shift $shiftType): self
    {
        $this->shiftType = $shiftType;

        return $this;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getIsNew(): ?bool
    {
        return $this->isNew;
    }

    public function setIsNew(?bool $isNew): self
    {
        $this->isNew = $isNew;

        return $this;
    }

    public function getRouteId(): ?DriverRoute
    {
        return $this->routeId;
    }

    public function setRouteId(?DriverRoute $routeId): self
    {
        $this->routeId = $routeId;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(?\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getVehicleDriverRecord(): ?VehicleDriverRecord
    {
        return $this->vehicleDriverRecord;
    }

    public function setVehicleDriverRecord(?VehicleDriverRecord $vehicleDriverRecord): self
    {
        $this->vehicleDriverRecord = $vehicleDriverRecord;

        // set (or unset) the owning side of the relation if necessary
        // $newTempDriverRoute = null === $vehicleDriverRecord ? null : $this;
        // if ($vehicleDriverRecord->getTempDriverRoute() !== $newTempDriverRoute) {
        //     $vehicleDriverRecord->setTempDriverRoute($newTempDriverRoute);
        // }

        return $this;
    }

    public function getIsOpenShift(): ?bool
    {
        return $this->isOpenShift;
    }

    public function setIsOpenShift(?bool $isOpenShift): self
    {
        $this->isOpenShift = $isOpenShift;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        if (!$this->startTime)
            return $this->getShiftType()->getStartTime();

        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        if ($this->routeId && $this->routeId->getDatetimeEnded())
            return $this->routeId->getDatetimeEnded();

        if (!$this->endTime)
            return $this->getShiftType()->getEndTime();

        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        if ($this->routeId && $this->routeId->getDatetimeEnded())
            $this->routeId->setDatetimeEnded($endTime);

        $this->endTime = $endTime;

        return $this;
    }

    public function getIsLoadOut(): ?bool
    {
        return $this->isLoadOut;
    }

    public function setIsLoadOut(bool $isLoadOut): self
    {
        $this->isLoadOut = $isLoadOut;

        return $this;
    }

    public function getOldDriver(): ?Driver
    {
        return $this->oldDriver;
    }

    public function setOldDriver(?Driver $oldDriver): self
    {
        $this->oldDriver = $oldDriver;

        return $this;
    }

    public function getOldDriverRoute(): ?DriverRoute
    {
        return $this->oldDriverRoute;
    }

    public function setOldDriverRoute(?DriverRoute $oldDriverRoute): self
    {
        $this->oldDriverRoute = $oldDriverRoute;

        return $this;
    }

    public function getIsBackup(): ?bool
    {
        return $this->isBackup;
    }

    public function setIsBackup(?bool $isBackup): self
    {
        $this->isBackup = $isBackup;

        return $this;
    }

    public function getIsTemp(): ?bool
    {
        return $this->isTemp;
    }

    public function setIsTemp(?bool $isTemp): self
    {
        $this->isTemp = $isTemp;

        return $this;
    }

    public function getShiftName(): ?string
    {
        if (!$this->shiftName) {
            return $this->getShiftType()->getName();
        }
        return $this->shiftName;
    }

    public function setShiftName(?string $shiftName): self
    {
        $this->shiftName = $shiftName;

        return $this;
    }

    public function getShiftColor(): ?string
    {
        if (!$this->shiftColor) {
            return $this->getShiftType()->getColor();
        }
        return $this->shiftColor;
    }

    public function setShiftColor(?string $shiftColor): self
    {
        $this->shiftColor = $shiftColor;

        return $this;
    }

    public function getUnpaidBreak(): ?int
    {
        if (!$this->unpaidBreak) {
            return $this->getShiftType()->getUnpaidBreak();
        }
        return $this->unpaidBreak;
    }

    public function setUnpaidBreak(?int $unpaidBreak): self
    {
        $this->unpaidBreak = $unpaidBreak;

        return $this;
    }

    public function getShiftNote(): ?string
    {
        if (!$this->shiftNote) {
            return $this->getShiftType()->getNote();
        }

        return $this->shiftNote;
    }

    public function setShiftNote(?string $shiftNote): self
    {
        $this->shiftNote = $shiftNote;

        return $this;
    }

    public function getShiftInvoiceType(): ?InvoiceType
    {
       /* if (!$this->shiftInvoiceType) {
            return $this->getShiftType()->getInvoiceType();
        }*/
        return $this->shiftInvoiceType;
    }

    public function setShiftInvoiceType(?InvoiceType $shiftInvoiceType): self
    {
        $this->shiftInvoiceType = $shiftInvoiceType;

        return $this;
    }

    public function getShiftStation(): ?Station
    {
        if (!$this->shiftStation) {
            return $this->getShiftType()->getStation();
        }
        return $this->shiftStation;
    }

    public function setShiftStation(?Station $shiftStation): self
    {
        $this->shiftStation = $shiftStation;

        return $this;
    }

    public function getShiftSkill(): ?Skill
    {
        if (!$this->shiftSkill) {
            return $this->getShiftType()->getSkill();
        }
        return $this->shiftSkill;
    }

    public function setShiftSkill(?Skill $shiftSkill): self
    {
        $this->shiftSkill = $shiftSkill;

        return $this;
    }

    public function getShiftCategory(): ?int
    {
        if (!$this->shiftCategory) {
            return $this->getShiftType()->getCategory();
        }
        return $this->shiftCategory;
    }

    public function setShiftCategory(?int $shiftCategory): self
    {
        $this->shiftCategory = $shiftCategory;

        return $this;
    }

    public function getShiftBalanceGroup(): ?BalanceGroup
    {
        if (!$this->shiftBalanceGroup) {
            return $this->getShiftType()->getBalanceGroup();
        }

        return $this->shiftBalanceGroup;
    }

    public function setShiftBalanceGroup(?BalanceGroup $shiftBalanceGroup): self
    {
        $this->shiftBalanceGroup = $shiftBalanceGroup;

        return $this;
    }

    public function getShiftTextColor(): ?string
    {
        if (!$this->shiftTextColor) {
            return $this->getShiftType()->getTextColor();
        }
        return $this->shiftTextColor;
    }

    public function setShiftTextColor(?string $shiftTextColor): self
    {
        $this->shiftTextColor = $shiftTextColor;

        return $this;
    }

    public function getNumberOfPackage(): ?string
    {
        return $this->numberOfPackage;
    }

    public function setNumberOfPackage(?string $numberOfPackage): self
    {
        $this->route = $numberOfPackage;

        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(?string $action): self
    {
        $this->action = $action;

        return $this;
    }

    // magic call for parent driver route
    public function __call($name, $arguments)
    {
        if ($this->routeId && method_exists($this->routeId, $name))
            return $this->routeId->{$name}($arguments);
        else
            return null;
    }

    public function getCurrentNote(): ?string
    {
        return $this->currentNote;
    }

    public function setCurrentNote(?string $currentNote): self
    {
        $this->currentNote = $currentNote;

        return $this;
    }

    public function getNewNote(): ?string
    {
        return $this->newNote;
    }

    public function setNewNote(?string $newNote): self
    {
        $this->newNote = $newNote;

        return $this;
    }
}
