<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 * @ORM\Entity(repositoryClass="App\Repository\CompanyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Company
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Role", mappedBy="company")
     * @MaxDepth(1)
     */
    private $companyRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Skill", mappedBy="company")
     * @MaxDepth(1)
     */
    private $skills;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Device", mappedBy="company")
     * @MaxDepth(1)
     */
    private $devices;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Status", mappedBy="company", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $statuses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="company")
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $apiKey;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmploymentStatus", mappedBy="companyId")
     * @MaxDepth(1)
     */
    private $employmentStatuses;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownerId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numberOfUser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $stripeData;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Station", mappedBy="company")
     * @MaxDepth(1)
     */
    private $stations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Schedule", mappedBy="company", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $schedules;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="company")
     * @MaxDepth(1)
     */
    private $rates;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $domain;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serviceSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $channelRoleSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $serviceRoleSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accountSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twilioApiKey;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twilioApiKeySecret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $generalChannelSid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Driver", mappedBy="company")
     * @MaxDepth(1)
     */
    private $drivers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $amazonAbbreviation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BalanceGroup", mappedBy="company")
     * @MaxDepth(1)
     */
    private $balanceGroups;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RouteCommitment", mappedBy="company")
     * @MaxDepth(1)
     */
    private $companyRouteCommitments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GradingSettings", mappedBy="company")
     * @MaxDepth(1)
     */
    private $gradingSettings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shift", mappedBy="company")
     * @MaxDepth(1)
     */
    private $shifts;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;



    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $workplaceBotChannelSid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehicle", mappedBy="company")
     * @MaxDepth(1)
     */
    private $vehicles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plivoAuthId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plivoAuthToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plivoMainSrcNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRouteCode", mappedBy="company")
     * @MaxDepth(1)
     */
    private $driverRouteCodes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="company")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentType", mappedBy="company")
     * @MaxDepth(1)
     */
    private $incidentTypes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $areaCode;

    /**
     * @ORM\OneToMany(targetEntity=IncidentQuestion::class, mappedBy="company")
     */
    private $incidentQuestions;

    /**
     * @ORM\OneToMany(targetEntity=DailyDriverSummary::class, mappedBy="company")
     */
    private $dailyDriverSummaries;

    /**
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $overtimeHours = 0;

    /**
     * @ORM\Column(type="integer", options={"default": 10})
     */
    private $rescueHours = 10;

    /**
     * @ORM\Column(type="integer", options={"default": 10})
     */
    private $backupDriverHours = 10;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fcmCredentialSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $apnCredentialSid;

    /**
     * Company constructor.
     */
    public function __construct()
    {
        $this->companyRoles = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->statuses = new ArrayCollection();
        $this->users = new ArrayCollection();
        // $this->packages = new ArrayCollection();
        $this->employmentStatuses = new ArrayCollection();
        $this->stations = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->drivers = new ArrayCollection();
        $this->balanceGroups = new ArrayCollection();
        $this->companyRouteCommitments = new ArrayCollection();
        $this->gradingSettings = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->driverRouteCodes = new ArrayCollection();
        $this->dailyDriverSummaries = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->incidentTypes = new ArrayCollection();
        $this->incidentQuestions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->ownerId;
    }

    public function setOwnerId(?int $ownerId): self
    {
        $this->ownerId = $ownerId;

        return $this;
    }

    public function getNumberOfUser(): ?int
    {
        return $this->numberOfUser;
    }

    public function setNumberOfUser(?int $numberOfUser): self
    {
        $this->numberOfUser = $numberOfUser;

        return $this;
    }

    /**
     * @return Collection|Skill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->setCompany($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
            // set the owning side to null (unless already changed)
            if ($skill->getCompany() === $this) {
                $skill->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setCompany($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getCompany() === $this) {
                $device->setCompany(null);
            }
        }

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Status[]
     */
    public function getStatuses(): Collection
    {
        return $this->statuses;
    }

    public function addStatus(Status $status): self
    {
        if (!$this->statuses->contains($status)) {
            $this->statuses[] = $status;
            $status->setCompany($this);
        }

        return $this;
    }

    public function removeStatus(Status $status): self
    {
        if ($this->statuses->contains($status)) {
            $this->statuses->removeElement($status);
            // set the owning side to null (unless already changed)
            if ($status->getCompany() === $this) {
                $status->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User  $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getCompany() === $this) {
                $user->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|EmploymentStatus[]
     */
    public function getEmploymentStatuses(): Collection
    {
        return $this->employmentStatuses;
    }

    public function addEmploymentStatus(EmploymentStatus $employmentStatus): self
    {
        if (!$this->employmentStatuses->contains($employmentStatus)) {
            $this->employmentStatuses[] = $employmentStatus;
            $employmentStatus->setCompanyId($this);
        }

        return $this;
    }

    public function removeEmploymentStatus(EmploymentStatus $employmentStatus): self
    {
        if ($this->employmentStatuses->contains($employmentStatus)) {
            $this->employmentStatuses->removeElement($employmentStatus);
            // set the owning side to null (unless already changed)
            if ($employmentStatus->getCompanyId() === $this) {
                $employmentStatus->setCompanyId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Station[]
     */
    public function getStations(): Collection
    {
        return $this->stations;
    }

    public function addStation(Station $station): self
    {
        if (!$this->stations->contains($station)) {
            $this->stations[] = $station;
            $station->setCompany($this);
        }

        return $this;
    }

    public function removeStation(Station $station): self
    {
        if ($this->stations->contains($station)) {
            $this->stations->removeElement($station);
            // set the owning side to null (unless already changed)
            if ($station->getCompany() === $this) {
                $station->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->setCompany($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->contains($schedule)) {
            $this->schedules->removeElement($schedule);
            // set the owning side to null (unless already changed)
            if ($schedule->getCompany() === $this) {
                $schedule->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rate[]
     */
    public function getRates(): Collection
    {
        return $this->rates;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates[] = $rate;
            $rate->setCompany($this);
        }

        return $this;
    }

    public function removeRate(Rate $rate): self
    {
        if ($this->rates->contains($rate)) {
            $this->rates->removeElement($rate);
            // set the owning side to null (unless already changed)
            if ($rate->getCompany() === $this) {
                $rate->setCompany(null);
            }
        }

        return $this;
    }

    public function getStripeData(): ?string
    {
        return $this->stripeData;
    }

    public function setStripeData(?string $stripeData): self
    {
        $this->stripeData = $stripeData;

        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(?string $domain): self
    {
        $this->domain = $domain;

        return $this;
    }

    public function getAuthToken(): ?string
    {
        return $this->authToken;
    }

    public function setAuthToken(?string $authToken): self
    {
        $this->authToken = $authToken;

        return $this;
    }

    public function getServiceSid(): ?string
    {
        return $this->serviceSid;
    }

    public function setServiceSid(?string $serviceSid): self
    {
        $this->serviceSid = $serviceSid;

        return $this;
    }

    public function getChannelRoleSid(): ?string
    {
        return $this->channelRoleSid;
    }

    public function setChannelRoleSid(?string $channelRoleSid): self
    {
        $this->channelRoleSid = $channelRoleSid;

        return $this;
    }

    public function getServiceRoleSid(): ?string
    {
        return $this->serviceRoleSid;
    }

    public function setServiceRoleSid(?string $serviceRoleSid): self
    {
        $this->serviceRoleSid = $serviceRoleSid;

        return $this;
    }

    public function getAccountSid(): ?string
    {
        return $this->accountSid;
    }

    public function setAccountSid(?string $accountSid): self
    {
        $this->accountSid = $accountSid;

        return $this;
    }

    public function getCredentialSid(): ?string
    {
        return $this->credentialSid;
    }

    public function setCredentialSid(?string $credentialSid): self
    {
        $this->credentialSid = $credentialSid;

        return $this;
    }

    public function getTwilioApiKey(): ?string
    {
        return $this->twilioApiKey;
    }

    public function setTwilioApiKey(?string $twilioApiKey): self
    {
        $this->twilioApiKey = $twilioApiKey;

        return $this;
    }

    public function getTwilioApiKeySecret(): ?string
    {
        return $this->twilioApiKeySecret;
    }

    public function setTwilioApiKeySecret(?string $twilioApiKeySecret): self
    {
        $this->twilioApiKeySecret = $twilioApiKeySecret;

        return $this;
    }

    public function getGeneralChannelSid(): ?string
    {
        return $this->generalChannelSid;
    }

    public function setGeneralChannelSid(?string $generalChannelSid): self
    {
        $this->generalChannelSid = $generalChannelSid;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getCompanyRoles(): Collection
    {
        return $this->companyRoles;
    }

    public function addCompanyRole(Role $companyRole): self
    {
        if (!$this->companyRoles->contains($companyRole)) {
            $this->companyRoles[] = $companyRole;
            $companyRole->setCompany($this);
        }

        return $this;
    }

    public function removeCompanyRole(Role $companyRole): self
    {
        if ($this->companyRoles->contains($companyRole)) {
            $this->companyRoles->removeElement($companyRole);
            // set the owning side to null (unless already changed)
            if ($companyRole->getCompany() === $this) {
                $companyRole->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Driver[]
     */
    public function getDrivers(): Collection
    {
        return $this->drivers;
    }

    public function addDriver(Driver $driver): self
    {
        if (!$this->drivers->contains($driver)) {
            $this->drivers[] = $driver;
            $driver->setCompany($this);
        }

        return $this;
    }

    public function removeDriver(Driver $driver): self
    {
        if ($this->drivers->contains($driver)) {
            $this->drivers->removeElement($driver);
            // set the owning side to null (unless already changed)
            if ($driver->getCompany() === $this) {
                $driver->setCompany(null);
            }
        }

        return $this;
    }

    public function getAmazonAbbreviation(): ?string
    {
        return $this->amazonAbbreviation;
    }

    public function setAmazonAbbreviation(?string $amazonAbbreviation): self
    {
        $this->amazonAbbreviation = $amazonAbbreviation;

        return $this;
    }

    /**
     * @return Collection|BalanceGroup[]
     */
    public function getBalanceGroups(): Collection
    {
        return $this->balanceGroups;
    }

    public function addBalanceGroup(BalanceGroup $balanceGroup): self
    {
        if (!$this->balanceGroups->contains($balanceGroup)) {
            $this->balanceGroups[] = $balanceGroup;
            $balanceGroup->setCompany($this);
        }

        return $this;
    }

    public function removeBalanceGroup(BalanceGroup $balanceGroup): self
    {
        if ($this->balanceGroups->contains($balanceGroup)) {
            $this->balanceGroups->removeElement($balanceGroup);
            // set the owning side to null (unless already changed)
            if ($balanceGroup->getCompany() === $this) {
                $balanceGroup->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RouteCommitment[]
     */
    public function getCompanyRouteCommitments(): Collection
    {
        return $this->companyRouteCommitments;
    }

    public function addCompanyRouteCommitment(RouteCommitment $companyRouteCommitment): self
    {
        if (!$this->companyRouteCommitments->contains($companyRouteCommitment)) {
            $this->companyRouteCommitments[] = $companyRouteCommitment;
            $companyRouteCommitment->setCompany($this);
        }

        return $this;
    }

    public function removeCompanyRouteCommitment(RouteCommitment $companyRouteCommitment): self
    {
        if ($this->companyRouteCommitments->contains($companyRouteCommitment)) {
            $this->companyRouteCommitments->removeElement($companyRouteCommitment);
            // set the owning side to null (unless already changed)
            if ($companyRouteCommitment->getCompany() === $this) {
                $companyRouteCommitment->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GradingSettings[]
     */
    public function getGradingSettings(): Collection
    {
        return $this->gradingSettings;
    }

    public function addGradingSetting(GradingSettings $gradingSetting): self
    {
        if (!$this->gradingSettings->contains($gradingSetting)) {
            $this->gradingSettings[] = $gradingSetting;
            $gradingSetting->setCompany($this);
        }

        return $this;
    }

    public function removeGradingSetting(GradingSettings $gradingSetting): self
    {
        if ($this->gradingSettings->contains($gradingSetting)) {
            $this->gradingSettings->removeElement($gradingSetting);
            // set the owning side to null (unless already changed)
            if ($gradingSetting->getCompany() === $this) {
                $gradingSetting->setCompany(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setCompany($this);
        }

        return $this;
    }

   /**
    * @return Collection|Incident[]
    */
   public function getIncidents(): Collection
   {
       return $this->incidents;
   }

   public function addIncident(Incident $incident): self
   {
       if (!$this->incidents->contains($incident)) {
           $this->incidents[] = $incident;
           $incident->setCompany($this);
       }

       return $this;
   }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getCompany() === $this) {
                $shift->setCompany(null);
            }
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getCompany() === $this) {
                $incident->setCompany(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;

    }


    /**
     * @return Collection|IncidentType[]
     */
    public function getIncidentTypes(): Collection
    {
        return $this->incidentTypes;
    }

    public function addIncidentType(IncidentType $incidentType): self
    {
        if (!$this->incidentTypes->contains($incidentType)) {
            $this->incidentTypes[] = $incidentType;
            $incidentType->setCompany($this);
        }

        return $this;
    }


    public function removeIncidentType(IncidentType $incidentType): self
    {
        if ($this->incidentTypes->contains($incidentType)) {
            $this->incidentTypes->removeElement($incidentType);
            // set the owning side to null (unless already changed)
            if ($incidentType->getCompany() === $this) {
                $incidentType->setCompany(null);
            }
        }

        return $this;
    }

    public function getWorkplaceBotChannelSid(): ?string
    {
        return $this->workplaceBotChannelSid;
    }

    public function setWorkplaceBotChannelSid(?string $workplaceBotChannelSid): self
    {
        $this->workplaceBotChannelSid = $workplaceBotChannelSid;

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->setCompany($this);
        }

        return $this;
    }

    public function getPlivoAuthId(): ?string
    {
        return $this->plivoAuthId;
    }

    public function setPlivoAuthId(?string $plivoAuthId): self
    {
        $this->plivoAuthId = $plivoAuthId;

        return $this;
    }

    public function getPlivoAuthToken(): ?string
    {
        return $this->plivoAuthToken;
    }

    public function setPlivoAuthToken(?string $plivoAuthToken): self
    {
        $this->plivoAuthToken = $plivoAuthToken;

        return $this;
    }

    public function getPlivoMainSrcNumber(): ?string
    {
        return $this->plivoMainSrcNumber;
    }

    public function setPlivoMainSrcNumber(?string $plivoMainSrcNumber): self
    {
        $this->plivoMainSrcNumber = $plivoMainSrcNumber;

        return $this;
    }

    /**
     * @return Collection|DriverRouteCode[]
     */
    public function getDriverRouteCodes(): Collection
    {
        return $this->driverRouteCodes;
    }

    public function addDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if (!$this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes[] = $driverRouteCode;
            $driverRouteCode->setCompany($this);
        }

        return $this;
    }

    public function removeDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if ($this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes->removeElement($driverRouteCode);
            // set the owning side to null (unless already changed)
            if ($driverRouteCode->getCompany() === $this) {
                $driverRouteCode->setCompany(null);
            }
        }

        return $this;
    }

    public function getAreaCode(): ?string
    {
        return $this->areaCode;
    }

    public function setAreaCode(?string $areaCode): self
    {
        $this->areaCode = $areaCode;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getOwners(): Collection
    {
        $ownerRole = $this->companyRoles->filter(function ($role) {
            return $role->getName() === 'ROLE_OWNER';
        })->first();

        $owners = $this->users->filter(function ($user) use ($ownerRole) {
            return $user->getUserRoles()->contains($ownerRole);
        });

        return $owners;
    }

    /**
     * @return Collection|User[]
     */
    public function getStationManager(): Collection
    {
        $stationManagerRole = $this->companyRoles->filter(function ($role) {
            return $role->getName() === 'ROLE_STATION_MANAGER';
        })->first();

        return $this->users->filter(function ($user) use ($stationManagerRole) {
            return $user->getUserRoles()->contains($stationManagerRole);
        });
    }

    /**
     * @return Collection|User[]
     */
    public function getOperationManager(): Collection
    {
        $operationManagerRole = $this->companyRoles->filter(function ($role) {
            return $role->getName() === 'ROLE_OPERATIONS_MANAGER';
        })->first();

        return $this->users->filter(function ($user) use ($operationManagerRole) {
            return $user->getUserRoles()->contains($operationManagerRole);
        });
    }

    /**
     * @return Collection|User[]
     */
    public function getOperationAccountManager(): Collection
    {
        $operationAccountManagerRole = $this->companyRoles->filter(function ($role) {
            return $role->getName() === 'ROLE_OPERATIONS_ACCOUNT_MANAGER';
        })->first();

        return $this->users->filter(function ($user) use ($operationAccountManagerRole) {
            return $user->getUserRoles()->contains($operationAccountManagerRole);
        });
    }


    /**
     * @return Collection|User[]
     */
    public function getAssistantStationManager(): Collection
    {
        $assistantStationManagerRole = $this->companyRoles->filter(function ($role) {
            return $role->getName() === 'ROLE_ASSISTANT_STATION_MANAGER';
        })->first();

        return $this->users->filter(function ($user) use ($assistantStationManagerRole) {
            return $user->getUserRoles()->contains($assistantStationManagerRole);
        });
    }

    /**
    * @return Collection|DailyDriverSummary[]
    */
   public function getDailyDriverSummaries(): Collection
   {
       return $this->dailyDriverSummaries;
   }

   public function addDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
   {
       if (!$this->dailyDriverSummaries->contains($dailyDriverSummary)) {
           $this->dailyDriverSummaries[] = $dailyDriverSummary;
           $dailyDriverSummary->setCompany($this);
       }

       return $this;
   }

    /**
     * @return Collection|IncidentQuestion[]
     */
    public function getIncidentQuestions(): Collection
    {
        return $this->incidentQuestions;
    }

    public function addIncidentQuestion(IncidentQuestion $incidentQuestion): self
    {
        if (!$this->incidentQuestions->contains($incidentQuestion)) {
            $this->incidentQuestions[] = $incidentQuestion;
            $incidentQuestion->setCompany($this);
        }

        return $this;
    }

    public function removeIncidentQuestion(IncidentQuestion $incidentQuestion): self
    {
        if ($this->incidentQuestions->contains($incidentQuestion)) {
            $this->incidentQuestions->removeElement($incidentQuestion);
            // set the owning side to null (unless already changed)
            if ($incidentQuestion->getCompany() === $this) {
                $incidentQuestion->setCompany(null);
            }
        }

        return $this;
    }

    public function removeDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
    {
        if ($this->dailyDriverSummaries->contains($dailyDriverSummary)) {
            $this->dailyDriverSummaries->removeElement($dailyDriverSummary);
            // set the owning side to null (unless already changed)
            if ($dailyDriverSummary->getCompany() === $this) {
                $dailyDriverSummary->setCompany(null);
            }
        }

        return $this;
    }

    public function getOvertimeHours(): ?int
    {
        return $this->overtimeHours;
    }

    public function setOvertimeHours(?int $overtimeHours): self
    {
        $this->overtimeHours = $overtimeHours;

        return $this;
    }

    public function getRescueHours(): ?int
    {
        return $this->rescueHours;
    }

    public function setRescueHours(int $rescueHours): self
    {
        $this->rescueHours = $rescueHours;

        return $this;
    }

    public function getBackupDriverHours(): ?int
    {
        return $this->backupDriverHours;
    }

    public function setBackupDriverHours(int $backupDriverHours): self
    {
        $this->backupDriverHours = $backupDriverHours;

        return $this;
    }

    public function getFcmCredentialSid(): ?string
    {
        return $this->fcmCredentialSid;
    }

    public function setFcmCredentialSid(?string $fcmCredentialSid): self
    {
        $this->fcmCredentialSid = $fcmCredentialSid;

        return $this;
    }

    public function getApnCredentialSid(): ?string
    {
        return $this->apnCredentialSid;
    }

    public function setApnCredentialSid(?string $apnCredentialSid): self
    {
        $this->apnCredentialSid = $apnCredentialSid;

        return $this;
    }
}
