<?php

namespace App\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CortexSettingsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CortexSettings
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Company", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cortexUsername;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Encrypted
     */
    private $cortexPassword;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cortexDspId;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cortexCookies;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"}, columnDefinition="DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
     */
    private $dateUpdated;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isArchive;

    public function __construct()
    {
        $this->setIsEnabled(1);
        $this->setIsArchive(0);
        $this->setDateCreated((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
        $this->setDateUpdated((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?\DateTimeInterface
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTimeInterface $dateUpdated): self
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getCortexDspId(): ?string
    {
        return $this->cortexDspId;
    }

    public function setCortexDspId(?string $cortexDspId): self
    {
        $this->cortexDspId = $cortexDspId;

        return $this;
    }

    public function getCortexUsername(): ?string
    {
        return $this->cortexUsername;
    }

    public function setCortexUsername(?string $cortexUsername): self
    {
        $this->cortexUsername = $cortexUsername;

        return $this;
    }

    public function getCortexPassword(): ?string
    {
        return $this->cortexPassword;
    }

    public function setCortexPassword(?string $cortexPassword): self
    {
        $this->cortexPassword = $cortexPassword;

        return $this;
    }

    public function getCortexCookies(): ?string
    {
        if ( empty($this->cortexCookies) )
            return '[]';

        return $this->cortexCookies;
    }

    public function setCortexCookies(?string $cortexCookies): self
    {
        $this->cortexCookies = $cortexCookies;

        return $this;
    }
}
