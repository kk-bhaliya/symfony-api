<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RouteCommitmentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RouteCommitment
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $week;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $weekDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="routeCommitments")
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="companyRouteCommitments")
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shift", inversedBy="shiftRouteCommitments")
     * @MaxDepth(1)
     */
    private $shiftType;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeek(): ?string
    {
        return $this->week;
    }

    public function setWeek(?string $week): self
    {
        $this->week = $week;

        return $this;
    }

    public function getWeekDate(): ?\DateTimeInterface
    {
        return $this->weekDate;
    }

    public function setWeekDate(?\DateTimeInterface $weekDate): self
    {
        $this->weekDate = $weekDate;

        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): self
    {
        $this->route = $route;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getShiftType(): ?Shift
    {
        return $this->shiftType;
    }

    public function setShiftType(?Shift $shiftType): self
    {
        $this->shiftType = $shiftType;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
