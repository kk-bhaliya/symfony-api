<?php

namespace App\Entity;

use App\Repository\DailyDriverSummaryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DailyDriverSummaryRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class DailyDriverSummary
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="dailyDriverSummaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="dailyDriverSummariesUsers")
     */
    private $users;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $summary;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $scheduledInterviews;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $interviewsCompleted;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $driversHired = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="date")
     */
    private $summaryDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dailyDriverSummaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="dailyDriverSummariesModifieds")
     * @ORM\JoinColumn(nullable=false)
     */
    private $modifiedBy;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $emailed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $emailedContent;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="dailyDriverSummaries")
     * @ORM\JoinColumn(nullable=false)
     */
    private $station;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $lockUser;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $driversFired = 0;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $driversQuit = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reportUrl;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $this->updatedDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addDailyDriverSummariesUser($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            if ($user->getDailyDriverSummariesUsers() === $this) {
                $user->removeDailyDriverSummariesUser(null);
            }
        }

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getScheduledInterviews(): ?int
    {
        return $this->scheduledInterviews;
    }

    public function setScheduledInterviews(?int $scheduledInterviews): self
    {
        $this->scheduledInterviews = $scheduledInterviews;

        return $this;
    }

    public function getInterviewsCompleted(): ?int
    {
        return $this->interviewsCompleted;
    }

    public function setInterviewsCompleted(?int $interviewsCompleted): self
    {
        $this->interviewsCompleted = $interviewsCompleted;

        return $this;
    }

    public function getDriversHired(): ?int
    {
        return $this->driversHired;
    }

    public function setDriversHired(?int $driversHired): self
    {
        $this->driversHired = $driversHired;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getSummaryDate(): ?\DateTimeInterface
    {
        return $this->summaryDate;
    }

    public function setSummaryDate(\DateTimeInterface $summaryDate): self
    {
        $this->summaryDate = $summaryDate;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getModifiedBy(): ?user
    {
        return $this->modifiedBy;
    }

    public function setModifiedBy(?User $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getEmailed(): ?bool
    {
        return $this->emailed;
    }

    public function setEmailed(?bool $emailed): self
    {
        $this->emailed = $emailed;

        return $this;
    }

    public function getEmailedContent(): ?string
    {
        return $this->emailedContent;
    }

    public function setEmailedContent(?string $emailedContent): self
    {
        $this->emailedContent = $emailedContent;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }


    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(?bool $isArchive): self
    {
        $this->isArchive = $isArchive;
        return $this;
    }

    public function getLockUser(): ?string
    {
        return $this->lockUser;
    }

    public function setLockUser(?string $lockUser): self
    {
        $this->lockUser = $lockUser;
        return $this;
    }

    public function getDriversFired(): ?int
    {
        return $this->driversFired;
    }

    public function setDriversFired(?int $driversFired): self
    {
        $this->driversFired = $driversFired;

        return $this;
    }

    public function getDriversQuit(): ?int
    {
        return $this->driversQuit;
    }

    public function setDriversQuit(?int $driversQuit): self
    {
        $this->driversQuit = $driversQuit;

        return $this;
    }

    public function getReportUrl(): ?string
    {
        return $this->reportUrl;
    }

    public function setReportUrl(?string $reportUrl): self
    {
        $this->reportUrl = $reportUrl;

        return $this;
    }
}
