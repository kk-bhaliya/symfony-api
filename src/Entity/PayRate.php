<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayRateRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PayRate
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $pay_rate;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Driver", inversedBy="payRate", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Skill", inversedBy="payRate", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayRate(): ?int
    {
        return $this->pay_rate;
    }

    public function setPayRate(int $pay_rate): self
    {
        $this->pay_rate = $pay_rate;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
