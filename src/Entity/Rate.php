<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RateRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Rate
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $criteria;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $effectiveRate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $cumulativeRate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="rates")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="rates")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InvoiceType", mappedBy="rateRule", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $invoiceTypes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function __construct()
    {
        $this->invoiceTypes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCriteria(): ?string
    {
        return $this->criteria;
    }

    public function setCriteria(?string $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }

    public function getEffectiveRate(): ?float
    {
        return $this->effectiveRate;
    }

    public function setEffectiveRate(?float $effectiveRate): self
    {
        $this->effectiveRate = $effectiveRate;

        return $this;
    }

    public function getCumulativeRate(): ?float
    {
        return $this->cumulativeRate;
    }

    public function setCumulativeRate(?float $cumulativeRate): self
    {
        $this->cumulativeRate = $cumulativeRate;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    /**
     * @return Collection|InvoiceType[]
     */
    public function getInvoiceTypes(): Collection
    {
        return $this->invoiceTypes;
    }

    public function addInvoiceType(InvoiceType $invoiceType): self
    {
        if (!$this->invoiceTypes->contains($invoiceType)) {
            $this->invoiceTypes[] = $invoiceType;
            $invoiceType->setRateRule($this);
        }

        return $this;
    }

    public function removeInvoiceType(InvoiceType $invoiceType): self
    {
        if ($this->invoiceTypes->contains($invoiceType)) {
            $this->invoiceTypes->removeElement($invoiceType);
            // set the owning side to null (unless already changed)
            if ($invoiceType->getRateRule() === $this) {
                $invoiceType->setRateRule(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

}
