<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkillRateRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SkillRate
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="skillRates")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="skillRates")
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\Column(type="float")
     */
    private $defaultRate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverSkill", mappedBy="skillRate")
     * @MaxDepth(1)
     */
    private $driverSkills;

    public function __construct()
    {
        $this->driverSkills = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDefaultRate(): ?float
    {
        return $this->defaultRate;
    }

    public function setDefaultRate(float $defaultRate): self
    {
        $this->defaultRate = $defaultRate;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(?Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|DriverSkill[]
     */
    public function getDriverSkills(): Collection
    {
        return $this->driverSkills;
    }

    public function addDriverSkill(DriverSkill $driverSkill): self
    {
        if (!$this->driverSkills->contains($driverSkill)) {
            $this->driverSkills[] = $driverSkill;
            $driverSkill->setSkillRate($this);
        }

        return $this;
    }

    public function removeDriverSkill(DriverSkill $driverSkill): self
    {
        if ($this->driverSkills->contains($driverSkill)) {
            $this->driverSkills->removeElement($driverSkill);
            // set the owning side to null (unless already changed)
            if ($driverSkill->getSkillRate() === $this) {
                $driverSkill->setSkillRate(null);
            }
        }

        return $this;
    }

}
