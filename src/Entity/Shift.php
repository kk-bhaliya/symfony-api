<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShiftRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Shift
{
    const NORMAL = 1;
    const BACKUP = 2;
    const TRAINING = 3;
    const RESCUE = 4;
    const TRAIN = 5;
    const DUTY = 6;
    const SYSTEM = 7;

    public static $categories = [
        self::NORMAL => 'Normal',
        self::BACKUP => 'Backup',
        self::TRAINING => 'Training',
        self::RESCUE => 'Rescue',
        self::TRAIN => 'Train',
        self::DUTY => 'Duty',
        self::SYSTEM => 'System',
    ];

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\Column(type="time")
     */
    private $startTime;

    /**
     * @ORM\Column(type="time")
     */
    private $endTime;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unpaidBreak;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InvoiceType", inversedBy="shifts")
     * @MaxDepth(1)
     */
    private $invoiceType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="shifts")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ScheduleDesign", mappedBy="shifts")
     * @MaxDepth(1)
     */
    private $scheduleDesigns;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="shifts")
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ShiftTemplate", inversedBy="shifts")
     * @MaxDepth(1)
     */
    private $shiftTemplate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="shiftType")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="shiftType")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BalanceGroup", inversedBy="shifts")
     * @MaxDepth(1)
     */
    private $balanceGroup;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RouteCommitment", mappedBy="shiftType")
     */
    private $shiftRouteCommitments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="shifts")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textColor;

    public function __construct()
    {
        $this->scheduleDesigns = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->shiftRouteCommitments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getUnpaidBreak(): ?int
    {
        return $this->unpaidBreak;
    }

    public function setUnpaidBreak(?int $unpaidBreak): self
    {
        $this->unpaidBreak = $unpaidBreak;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getInvoiceType(): ?InvoiceType
    {
        return $this->invoiceType;
    }

    public function setInvoiceType(?InvoiceType $invoiceType): self
    {
        $this->invoiceType = $invoiceType;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    /**
     * @return Collection|ScheduleDesign[]
     */
    public function getScheduleDesigns(): Collection
    {
        return $this->scheduleDesigns;
    }

    public function addScheduleDesign(ScheduleDesign $scheduleDesign): self
    {
        if (!$this->scheduleDesigns->contains($scheduleDesign)) {
            $this->scheduleDesigns[] = $scheduleDesign;
            $scheduleDesign->addShift($this);
        }

        return $this;
    }

    public function removeScheduleDesign(ScheduleDesign $scheduleDesign): self
    {
        if ($this->scheduleDesigns->contains($scheduleDesign)) {
            $this->scheduleDesigns->removeElement($scheduleDesign);
            $scheduleDesign->removeShift($this);
        }

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(?Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getShiftTemplate(): ?ShiftTemplate
    {
        return $this->shiftTemplate;
    }

    public function setShiftTemplate(?ShiftTemplate $shiftTemplate): self
    {
        $this->shiftTemplate = $shiftTemplate;

        return $this;
    }

    public function getCategoryArray()
    {
        $categoryInWord = self::$categories;
        return $categoryInWord;
    }

    public function getCategoryName()
    {
        $categoryInWord = self::$categories[$this->category];
        return $categoryInWord;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function setCategory(?int $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setShiftType($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getShiftType() === $this) {
                $driverRoute->setShiftType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setShiftType($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getShiftType() === $this) {
                $tempDriverRoute->setShiftType(null);
            }
        }

        return $this;
    }

    public function getBalanceGroup(): ?BalanceGroup
    {
        return $this->balanceGroup;
    }

    public function setBalanceGroup(?BalanceGroup $balanceGroup): self
    {
        $this->balanceGroup = $balanceGroup;

        return $this;
    }

    /**
     * @return Collection|RouteCommitment[]
     */
    public function getShiftRouteCommitments(): Collection
    {
        return $this->shiftRouteCommitments;
    }

    public function addShiftRouteCommitment(RouteCommitment $shiftRouteCommitment): self
    {
        if (!$this->shiftRouteCommitments->contains($shiftRouteCommitment)) {
            $this->shiftRouteCommitments[] = $shiftRouteCommitment;
            $shiftRouteCommitment->setShiftType($this);
        }

        return $this;
    }

    public function removeShiftRouteCommitment(RouteCommitment $shiftRouteCommitment): self
    {
        if ($this->shiftRouteCommitments->contains($shiftRouteCommitment)) {
            $this->shiftRouteCommitments->removeElement($shiftRouteCommitment);
            // set the owning side to null (unless already changed)
            if ($shiftRouteCommitment->getShiftType() === $this) {
                $shiftRouteCommitment->setShiftType(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getTextColor(): ?string
    {
        return $this->textColor;
    }

    public function setTextColor(?string $textColor): self
    {
        $this->textColor = $textColor;

        return $this;
    }
}
