<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentFileRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentFile
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncidentFormFieldValue", inversedBy="incidentFiles")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $incidentFormFieldValue;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Image", inversedBy="incidentFiles")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $image;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIncidentFormFieldValue(): ?IncidentFormFieldValue
    {
        return $this->incidentFormFieldValue;
    }

    public function setIncidentFormFieldValue(?IncidentFormFieldValue $incidentFormFieldValue): self
    {
        $this->incidentFormFieldValue = $incidentFormFieldValue;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
