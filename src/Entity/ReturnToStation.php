<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReturnToStationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ReturnToStation
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedOutFlexAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedOutPayrollAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $completeMentorPostTripAt;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $stationCheckListResult = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $packagesReturning;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $packagesMissing;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedStationCheckAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedReturnToParkingLotAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedReturnRestItemsAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedMileageAndGasAt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mileage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gasTankLevel;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DriverRoute", cascade={"persist", "remove"})
     */
    private $driverRoute;

    /**
     * @ORM\Column(type="boolean")
     */
    private $processCompleted = false;

    /**
     * @ORM\ManyToOne(targetEntity=Device::class)
     */
    private $device;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $completedAt;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLoggedOutFlexAt(): ?\DateTimeInterface
    {
        return $this->loggedOutFlexAt;
    }

    public function setLoggedOutFlexAt(?\DateTimeInterface $loggedOutFlexAt): self
    {
        $this->loggedOutFlexAt = $loggedOutFlexAt;

        return $this;
    }

    public function getLoggedOutPayrollAt(): ?\DateTimeInterface
    {
        return $this->loggedOutPayrollAt;
    }

    public function setLoggedOutPayrollAt(?\DateTimeInterface $loggedOutPayrollAt): self
    {
        $this->loggedOutPayrollAt = $loggedOutPayrollAt;

        return $this;
    }

    public function getCompleteMentorPostTripAt(): ?\DateTimeInterface
    {
        return $this->completeMentorPostTripAt;
    }

    public function setCompleteMentorPostTripAt(?\DateTimeInterface $completeMentorPostTripAt): self
    {
        $this->completeMentorPostTripAt = $completeMentorPostTripAt;

        return $this;
    }

    public function getStationCheckListResult(): ?array
    {
        return $this->stationCheckListResult;
    }

    public function setStationCheckListResult(?array $stationCheckListResult): self
    {
        $this->stationCheckListResult = $stationCheckListResult;

        return $this;
    }

    public function getPackagesReturning(): ?int
    {
        return $this->packagesReturning;
    }

    public function setPackagesReturning(?int $packagesReturning): self
    {
        $this->packagesReturning = $packagesReturning;

        return $this;
    }

    public function getPackagesMissing(): ?int
    {
        return $this->packagesMissing;
    }

    public function setPackagesMissing(?int $packagesMissing): self
    {
        $this->packagesMissing = $packagesMissing;

        return $this;
    }

    public function getLoggedStationCheckAt(): ?\DateTimeInterface
    {
        return $this->loggedStationCheckAt;
    }

    public function setLoggedStationCheckAt(?\DateTimeInterface $loggedStationCheckAt): self
    {
        $this->loggedStationCheckAt = $loggedStationCheckAt;

        return $this;
    }

    public function getLoggedReturnToParkingLotAt(): ?\DateTimeInterface
    {
        return $this->loggedReturnToParkingLotAt;
    }

    public function setLoggedReturnToParkingLotAt(?\DateTimeInterface $loggedReturnToParkingLotAt): self
    {
        $this->loggedReturnToParkingLotAt = $loggedReturnToParkingLotAt;

        return $this;
    }

    public function getLoggedReturnRestItemsAt(): ?\DateTimeInterface
    {
        return $this->loggedReturnRestItemsAt;
    }

    public function setLoggedReturnRestItemsAt(?\DateTimeInterface $loggedReturnRestItemsAt): self
    {
        $this->loggedReturnRestItemsAt = $loggedReturnRestItemsAt;

        return $this;
    }

    public function getLoggedMileageAndGasAt(): ?\DateTimeInterface
    {
        return $this->loggedMileageAndGasAt;
    }

    public function setLoggedMileageAndGasAt(?\DateTimeInterface $loggedMileageAndGasAt): self
    {
        $this->loggedMileageAndGasAt = $loggedMileageAndGasAt;

        return $this;
    }

    public function getMileage(): ?float
    {
        return $this->mileage;
    }

    public function setMileage(?float $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getGasTankLevel(): ?string
    {
        return $this->gasTankLevel;
    }

    public function setGasTankLevel(?string $gasTankLevel): self
    {
        $this->gasTankLevel = $gasTankLevel;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getDriverRoute(): ?DriverRoute
    {
        return $this->driverRoute;
    }

    public function setDriverRoute(?DriverRoute $driverRoute): self
    {
        $this->driverRoute = $driverRoute;

        return $this;
    }

    public function getProcessCompleted(): ?bool
    {
        return $this->processCompleted;
    }

    public function setProcessCompleted(bool $processCompleted): self
    {
        $this->processCompleted = $processCompleted;

        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $completedAt): self
    {
        $this->completedAt = $completedAt;

        return $this;
    }
}
