<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RoleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Role
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="childrens")
     * @MaxDepth(1)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Role", mappedBy="parent")
     * @MaxDepth(1)
     */
    private $childrens;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="companyRoles")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="userRoles")
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentPhase", mappedBy="role")
     * @MaxDepth(1)
     */
    private $incidentPhases;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->childrens = new ArrayCollection();
        $this->incidentPhases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRoleName()
    {
        if(is_string($this->getName())){
            $role_name = explode('ROLE_', $this->getName());
            $role = str_replace("_"," ",$role_name);
            if(isset($role[1])){
                $role_full_name = ucwords(strtolower($role[1]));
                return str_replace('Hr ','HR ',$role_full_name);
            }
        }
        return 'Delivery Associate';
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(Role $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(Role $children): self
    {
        if ($this->childrens->contains($children)) {
            $this->childrens->removeElement($children);
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addUserRole($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeUserRole($this);
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|IncidentPhase[]
     */
    public function getIncidentPhases(): Collection
    {
        return $this->incidentPhases;
    }

    public function addIncidentPhase(IncidentPhase $incidentPhase): self
    {
        if (!$this->incidentPhases->contains($incidentPhase)) {
            $this->incidentPhases[] = $incidentPhase;
            $incidentPhase->setRole($this);
        }

        return $this;
    }

    public function removeIncidentPhase(IncidentPhase $incidentPhase): self
    {
        if ($this->incidentPhases->contains($incidentPhase)) {
            $this->incidentPhases->removeElement($incidentPhase);
            // set the owning side to null (unless already changed)
            if ($incidentPhase->getRole() === $this) {
                $incidentPhase->setRole(null);
            }
        }

        return $this;
    }


}
