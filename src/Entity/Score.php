<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScoreRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Score
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="scores", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deliveryScore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attendanceScore;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $attendanceRecord;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disciplineRecord;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $coreValueScore;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function __construct()
    {
        $this->createdAt = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeliveryScore(): ?string
    {
        return $this->deliveryScore;
    }

    public function setDeliveryScore(?string $deliveryScore): self
    {
        $this->deliveryScore = $deliveryScore;

        return $this;
    }

    public function getAttendanceScore(): ?string
    {
        return $this->attendanceScore;
    }

    public function setAttendanceScore(?string $attendanceScore): self
    {
        $this->attendanceScore = $attendanceScore;

        return $this;
    }

    public function getAttendanceRecord(): ?string
    {
        return $this->attendanceRecord;
    }

    public function setAttendanceRecord(?string $attendanceRecord): self
    {
        $this->attendanceRecord = $attendanceRecord;

        return $this;
    }

    public function getDisciplineRecord(): ?string
    {
        return $this->disciplineRecord;
    }

    public function setDisciplineRecord(?string $disciplineRecord): self
    {
        $this->disciplineRecord = $disciplineRecord;

        return $this;
    }

    public function getCoreValueScore(): ?string
    {
        return $this->coreValueScore;
    }

    public function setCoreValueScore(?string $coreValueScore): self
    {
        $this->coreValueScore = $coreValueScore;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

}
