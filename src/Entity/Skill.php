<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SkillRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Skill
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="skills")
     * @MaxDepth(1)
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="skills")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="childrens")
     * @MaxDepth(1)
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Skill", mappedBy="parent")
     * @MaxDepth(1)
     */
    private $childrens;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="skill")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PayRate", mappedBy="skill", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $payRate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SkillRate", mappedBy="skill", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $skillRates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shift", mappedBy="skill", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $shifts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverSkill", mappedBy="skill")
     * @MaxDepth(1)
     */
    private $driverSkills;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="skill")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ShiftTemplate", mappedBy="skill", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $shiftTemplate;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="shiftSkill")
     * @MaxDepth(1)
     */
    private $driverShiftSkill;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="shiftSkill")
     * @MaxDepth(1)
     */
    private $tempDriverRouteShiftSkill;


    /**
     * Skill constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->childrens = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->skillRates = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->driverSkills = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->driverShiftSkill = new ArrayCollection();
        $this->tempDriverRouteShiftSkill = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getSkillName(): ?string
    {
        $name = $this->getName();
        if(is_string($name)){
            return str_replace("_", " ", $name);
        }
        return $name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Skill[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(Skill $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(Skill $children): self
    {
        if ($this->childrens->contains($children)) {
            $this->childrens->removeElement($children);
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setSkill($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getSkill() === $this) {
                $driverRoute->setSkill(null);
            }
        }

        return $this;
    }

    public function getPayRate(): ?PayRate
    {
        return $this->payRate;
    }

    public function setPayRate(?PayRate $payRate): self
    {
        $this->payRate = $payRate;

        // set (or unset) the owning side of the relation if necessary
        $newSkill = null === $payRate ? null : $this;
        if ($payRate->getSkill() !== $newSkill) {
            $payRate->setSkill($newSkill);
        }

        return $this;
    }

    /**
     * @return Collection|SkillRate[]
     */
    public function getSkillRates(): Collection
    {
        return $this->skillRates;
    }

    public function addSkillRate(SkillRate $skillRate): self
    {
        if (!$this->skillRates->contains($skillRate)) {
            $this->skillRates[] = $skillRate;
            $skillRate->setSkill($this);
        }

        return $this;
    }

    public function removeSkillRate(SkillRate $skillRate): self
    {
        if ($this->skillRates->contains($skillRate)) {
            $this->skillRates->removeElement($skillRate);
            // set the owning side to null (unless already changed)
            if ($skillRate->getSkill() === $this) {
                $skillRate->setSkill(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setSkill($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getSkill() === $this) {
                $shift->setSkill(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DriverSkill[]
     */
    public function getDriverSkills(): Collection
    {
        return $this->driverSkills;
    }

    public function addDriverSkill(DriverSkill $driverSkill): self
    {
        if (!$this->driverSkills->contains($driverSkill)) {
            $this->driverSkills[] = $driverSkill;
            $driverSkill->setSkill($this);
        }

        return $this;
    }

    public function removeDriverSkill(DriverSkill $driverSkill): self
    {
        if ($this->driverSkills->contains($driverSkill)) {
            $this->driverSkills->removeElement($driverSkill);
            // set the owning side to null (unless already changed)
            if ($driverSkill->getSkill() === $this) {
                $driverSkill->setSkill(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setSkill($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getSkill() === $this) {
                $tempDriverRoute->setSkill(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverShiftSkill(): Collection
    {
        return $this->driverShiftSkill;
    }

    public function addDriverShiftSkill(DriverRoute $driverShiftSkill): self
    {
        if (!$this->driverShiftSkill->contains($driverShiftSkill)) {
            $this->driverShiftSkill[] = $driverShiftSkill;
            $driverShiftSkill->setShiftSkill($this);
        }

        return $this;
    }

    public function removeDriverShiftSkill(DriverRoute $driverShiftSkill): self
    {
        if ($this->driverShiftSkill->contains($driverShiftSkill)) {
            $this->driverShiftSkill->removeElement($driverShiftSkill);
            // set the owning side to null (unless already changed)
            if ($driverShiftSkill->getShiftSkill() === $this) {
                $driverShiftSkill->setShiftSkill(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRouteShiftSkill(): Collection
    {
        return $this->tempDriverRouteShiftSkill;
    }

    public function addTempDriverRouteShiftSkill(TempDriverRoute $tempDriverRouteShiftSkill): self
    {
        if (!$this->tempDriverRouteShiftSkill->contains($tempDriverRouteShiftSkill)) {
            $this->tempDriverRouteShiftSkill[] = $tempDriverRouteShiftSkill;
            $tempDriverRouteShiftSkill->setShiftSkill($this);
        }

        return $this;
    }

    public function removeTempDriverRouteShiftSkill(TempDriverRoute $tempDriverRouteShiftSkill): self
    {
        if ($this->tempDriverRouteShiftSkill->contains($tempDriverRouteShiftSkill)) {
            $this->tempDriverRouteShiftSkill->removeElement($tempDriverRouteShiftSkill);
            // set the owning side to null (unless already changed)
            if ($tempDriverRouteShiftSkill->getShiftSkill() === $this) {
                $tempDriverRouteShiftSkill->setShiftSkill(null);
            }
        }

        return $this;
    }
}
