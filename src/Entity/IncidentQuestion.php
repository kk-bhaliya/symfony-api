<?php

namespace App\Entity;

use App\Repository\IncidentQuestionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IncidentQuestionRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentQuestion
{
    const TYPE_TEXT = 1;
    const TYPE_TEXTAREA = 2;
    const TYPE_PASSWORD = 3;
    const TYPE_INTEGER = 4;
    const TYPE_PERCENT = 5;
    const TYPE_NUMBER = 6;
    const TYPE_RANGE = 7;
    const TYPE_EMAIL_ADDRESS = 8;
    const TYPE_PHONE = 9;
    const TYPE_URL = 10;
    const TYPE_MONEY = 11;
    const TYPE_SELECT = 12;
    const TYPE_MULTI_SELECT = 13;
    const TYPE_CHECKBOX = 14;
    const TYPE_RADIO = 15;
    const TYPE_DATE = 16;
    const TYPE_DATETIME = 17;
    const TYPE_TIME = 18;
    const TYPE_COLOR_PICKER = 19;
    const TYPE_CURRENCY = 20;
    const TYPE_FILE = 21;
    const TYPE_QUESTION = 22;
    const TYPE_ENTITY = 23;
    const TYPE_COLLECTION = 24;
    const TYPE_HIDDEN = 25;

    public static $fieldIdArray = [
        self::TYPE_TEXT => 'Text',
        self::TYPE_TEXTAREA => 'Textarea',
        self::TYPE_PASSWORD => 'Password',
        self::TYPE_INTEGER => 'Integer',
        self::TYPE_PERCENT => 'Percent',
        self::TYPE_NUMBER => 'Number',
        self::TYPE_RANGE => 'Range',
        self::TYPE_EMAIL_ADDRESS => 'Email',
        self::TYPE_PHONE => 'Phone',
        self::TYPE_URL => 'URL',
        self::TYPE_MONEY => 'Money',
        self::TYPE_SELECT => 'Select',
        self::TYPE_MULTI_SELECT => 'Multi Select',
        self::TYPE_CHECKBOX => 'Checkbox',
        self::TYPE_RADIO => 'Radio',
        self::TYPE_DATE => 'Date',
        self::TYPE_DATETIME => 'Datetime',
        self::TYPE_TIME => 'Time',
        self::TYPE_COLOR_PICKER => 'Color Picker',
        self::TYPE_CURRENCY => 'Currency',
        self::TYPE_FILE => 'File',
        self::TYPE_QUESTION => 'Question',
        self::TYPE_ENTITY => 'Entity',
        self::TYPE_COLLECTION => 'Collection',
        self::TYPE_HIDDEN => 'Hidden' 
    ];


    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fieldId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity=IncidentFormFields::class, mappedBy="incidentQuestion", cascade={"persist", "remove"})
     */
    private $incidentFormFields;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="incidentQuestions")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $company;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldData = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldRule = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldStyle = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->incidentFormFields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getFieldId(): ?string
    {
        return $this->fieldId;
    }

    public function setFieldId(?string $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|IncidentFormFields[]
     */
    public function getIncidentFormFields(): Collection
    {
        return $this->incidentFormFields;
    }

    public function addIncidentFormField(IncidentFormFields $incidentFormField): self
    {
        if (!$this->incidentFormFields->contains($incidentFormField)) {
            $this->incidentFormFields[] = $incidentFormField;
            $incidentFormField->setIncidentQuestion($this);
        }

        return $this;
    }

    public function removeIncidentFormField(IncidentFormFields $incidentFormField): self
    {
        if ($this->incidentFormFields->contains($incidentFormField)) {
            $this->incidentFormFields->removeElement($incidentFormField);
            // set the owning side to null (unless already changed)
            if ($incidentFormField->getIncidentQuestion() === $this) {
                $incidentFormField->setIncidentQuestion(null);
            }
        }

        return $this;
    }


    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getFieldData(): ?array
    {
        return $this->fieldData;
    }

    public function setFieldData(?array $fieldData): self
    {
        $this->fieldData = $fieldData;

        return $this;
    }

    public function getFieldRule(): ?array
    {
        return $this->fieldRule;
    }

    public function setFieldRule(?array $fieldRule): self
    {
        $this->fieldRule = $fieldRule;

        return $this;
    }

    public function getFieldStyle(): ?array
    {
        return $this->fieldStyle;
    }

    public function setFieldStyle(?array $fieldStyle): self
    {
        $this->fieldStyle = $fieldStyle;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
