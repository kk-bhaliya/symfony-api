<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentFormFieldsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentFormFields
{
    const TYPE_TEXT = 1;
    const TYPE_TEXTAREA = 2;
    const TYPE_PASSWORD = 3;
    const TYPE_INTEGER = 4;
    const TYPE_PERCENT = 5;
    const TYPE_NUMBER = 6;
    const TYPE_RANGE = 7;
    const TYPE_EMAIL_ADDRESS = 8;
    const TYPE_PHONE = 9;
    const TYPE_URL = 10;
    const TYPE_MONEY = 11;
    const TYPE_SELECT = 12;
    const TYPE_MULTI_SELECT = 13;
    const TYPE_CHECKBOX = 14;
    const TYPE_RADIO = 15;
    const TYPE_DATE = 16;
    const TYPE_DATETIME = 17;
    const TYPE_TIME = 18;
    const TYPE_COLOR_PICKER = 19;
    const TYPE_CURRENCY = 20;
    const TYPE_FILE = 21;
    const TYPE_QUESTION = 22;
    const TYPE_ENTITY = 23;
    const TYPE_COLLECTION = 24;
    const TYPE_HIDDEN = 25;

    public static $fieldIdArray = [
        self::TYPE_TEXT => 'Text',
        self::TYPE_TEXTAREA => 'Textarea',
        self::TYPE_PASSWORD => 'Password',
        self::TYPE_INTEGER => 'Integer',
        self::TYPE_PERCENT => 'Percent',
        self::TYPE_NUMBER => 'Number',
        self::TYPE_RANGE => 'Range',
        self::TYPE_EMAIL_ADDRESS => 'Email',
        self::TYPE_PHONE => 'Phone',
        self::TYPE_URL => 'URL',
        self::TYPE_MONEY => 'Money',
        self::TYPE_SELECT => 'Select',
        self::TYPE_MULTI_SELECT => 'Multi Select',
        self::TYPE_CHECKBOX => 'Checkbox',
        self::TYPE_RADIO => 'Radio',
        self::TYPE_DATE => 'Date',
        self::TYPE_DATETIME => 'Datetime',
        self::TYPE_TIME => 'Time',
        self::TYPE_COLOR_PICKER => 'Color Picker',
        self::TYPE_CURRENCY => 'Currency',
        self::TYPE_FILE => 'File',
        self::TYPE_QUESTION => 'Question',
        self::TYPE_ENTITY => 'Entity',
        self::TYPE_COLLECTION => 'Collection',
        self::TYPE_HIDDEN => 'Hidden' 
    ];

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncidentStep", inversedBy="incidentFormFields")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $step;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordering;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentFormFieldValue", mappedBy="incidentFormField", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentFormFieldValues;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\ManyToOne(targetEntity=IncidentQuestion::class, inversedBy="incidentFormFields")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incidentQuestion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fieldId;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldData = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldRule = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $fieldStyle = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInApp = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInWeb = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->incidentFormFieldValues = new ArrayCollection();
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStep(): ?IncidentStep
    {
        return $this->step;
    }

    public function setStep(?IncidentStep $step): self
    {
        $this->step = $step;

        return $this;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }


    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return Collection|IncidentFormFieldValue[]
     */
    public function getIncidentFormFieldValues(): Collection
    {
        return $this->incidentFormFieldValues;
    }

    public function addIncidentFormFieldValue(IncidentFormFieldValue $incidentFormFieldValue): self
    {
        if (!$this->incidentFormFieldValues->contains($incidentFormFieldValue)) {
            $this->incidentFormFieldValues[] = $incidentFormFieldValue;
            $incidentFormFieldValue->setIncidentFormField($this);
        }

        return $this;
    }

    public function removeIncidentFormFieldValue(IncidentFormFieldValue $incidentFormFieldValue): self
    {
        if ($this->incidentFormFieldValues->contains($incidentFormFieldValue)) {
            $this->incidentFormFieldValues->removeElement($incidentFormFieldValue);
            // set the owning side to null (unless already changed)
            if ($incidentFormFieldValue->getIncidentFormField() === $this) {
                $incidentFormFieldValue->setIncidentFormField(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getIncidentQuestion(): ?IncidentQuestion
    {
        return $this->incidentQuestion;
    }

    public function setIncidentQuestion(?IncidentQuestion $incidentQuestion): self
    {
        $this->incidentQuestion = $incidentQuestion;

        return $this;
    }

    public function getLabel(): ?string
    {
        if (!$this->label) {
            return $this->getIncidentQuestion()->getLabel();
        }
        return $this->label;
    }

    public function setLabel(?string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getFieldId(): ?string
    {
        if (!$this->fieldId) {
            return $this->getIncidentQuestion()->getFieldId();
        }
        return $this->fieldId;
    }

    public function setFieldId(?string $fieldId): self
    {
        $this->fieldId = $fieldId;

        return $this;
    }

    public function getFieldData(): ?array
    {
        if (!$this->fieldData) {
            return $this->getIncidentQuestion()->getFieldData();
        }
        return $this->fieldData;
    }

    public function setFieldData(?array $fieldData): self
    {
        $this->fieldData = $fieldData;

        return $this;
    }

    public function getFieldRule(): ?array
    {
        if (!$this->fieldRule) {
            return $this->getIncidentQuestion()->getFieldRule();
        }
        return $this->fieldRule;
    }

    public function setFieldRule(?array $fieldRule): self
    {
        $this->fieldRule = $fieldRule;

        return $this;
    }

    public function getFieldStyle(): ?array
    {
        if (!$this->fieldStyle) {
            return $this->getIncidentQuestion()->getFieldStyle();
        }
        return $this->fieldStyle;
    }

    public function setFieldStyle(?array $fieldStyle): self
    {
        $this->fieldStyle = $fieldStyle;

        return $this;
    }

    public function getIsInApp(): ?bool
    {
        return $this->isInApp;
    }

    public function setIsInApp(bool $isInApp): self
    {
        $this->isInApp = $isInApp;

        return $this;
    }

    public function getIsInWeb(): ?bool
    {
        return $this->isInWeb;
    }

    public function setIsInWeb(bool $isInWeb): self
    {
        $this->isInWeb = $isInWeb;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
