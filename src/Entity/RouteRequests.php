<?php

namespace App\Entity;

use App\Repository\RouteRequestsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass=RouteRequestsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class RouteRequests
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Driver::class, inversedBy="newDriverRequest")
     * @MaxDepth(1)
     */
    private $newDriver;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reasonForRequest;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $isSwapRequest = true;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="routeRequests")
     * @MaxDepth(1)
     */
    private $approvedBy;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isRequestCompleted = false;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity=DriverRoute::class, mappedBy="requestForRoute")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\ManyToOne(targetEntity=DriverRoute::class, inversedBy="oldRouteRequest")
     */
    private $oldRoute;

    /**
     * @ORM\ManyToOne(targetEntity=DriverRoute::class, inversedBy="newRouteRequest")
     */
    private $newRoute;

    /**
     * @ORM\ManyToOne(targetEntity=Driver::class, inversedBy="oldDriverRequests")
     */
    private $oldDriver;

    public function __construct()
    {
        $this->driverRoutes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNewDriver(): ?Driver
    {
        return $this->newDriver;
    }

    public function setNewDriver(?Driver $newDriver): self
    {
        $this->newDriver = $newDriver;

        return $this;
    }

    public function getReasonForRequest(): ?string
    {
        return $this->reasonForRequest;
    }

    public function setReasonForRequest(?string $reasonForRequest): self
    {
        $this->reasonForRequest = $reasonForRequest;

        return $this;
    }

    public function getIsSwapRequest(): ?bool
    {
        return $this->isSwapRequest;
    }

    public function setIsSwapRequest(bool $isSwapRequest): self
    {
        $this->isSwapRequest = $isSwapRequest;

        return $this;
    }

    public function getApprovedBy(): ?User
    {
        return $this->approvedBy;
    }

    public function setApprovedBy(?User $approvedBy): self
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    public function getIsRequestCompleted(): ?bool
    {
        return $this->isRequestCompleted;
    }

    public function setIsRequestCompleted(bool $isRequestCompleted): self
    {
        $this->isRequestCompleted = $isRequestCompleted;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setRequestForRoute($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getRequestForRoute() === $this) {
                $driverRoute->setRequestForRoute(null);
            }
        }

        return $this;
    }

    public function getOldRoute(): ?DriverRoute
    {
        return $this->oldRoute;
    }

    public function setOldRoute(?DriverRoute $oldRoute): self
    {
        $this->oldRoute = $oldRoute;

        return $this;
    }

    public function getNewRoute(): ?DriverRoute
    {
        return $this->newRoute;
    }

    public function setNewRoute(?DriverRoute $newRoute): self
    {
        $this->newRoute = $newRoute;

        return $this;
    }

    public function getOldDriver(): ?Driver
    {
        return $this->oldDriver;
    }

    public function setOldDriver(?Driver $oldDriver): self
    {
        $this->oldDriver = $oldDriver;

        return $this;
    }
}
