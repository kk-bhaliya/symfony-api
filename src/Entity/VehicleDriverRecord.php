<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehicleDriverRecordRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class VehicleDriverRecord
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle", inversedBy="vehicleDriverRecords")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="vehicleDriverRecords")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateSignedOut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateSignedIn;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DriverRoute", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $driverRoute;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Station", inversedBy="vehicleDriverRecord", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $startingMileage;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $endingMileage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $startCondition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $endCondition;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\TempDriverRoute", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $tempDriverRoute;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateSignedOut(): ?\DateTimeInterface
    {
        return $this->dateSignedOut;
    }

    public function setDateSignedOut(?\DateTimeInterface $dateSignedOut): self
    {
        $this->dateSignedOut = $dateSignedOut;

        return $this;
    }

    public function getDateSignedIn(): ?\DateTimeInterface
    {
        return $this->dateSignedIn;
    }

    public function setDateSignedIn(\DateTimeInterface $dateSignedIn): self
    {
        $this->dateSignedIn = $dateSignedIn;

        return $this;
    }

    public function getStartingMileage(): ?int
    {
        return $this->startingMileage;
    }

    public function setStartingMileage(?int $startingMileage): self
    {
        $this->startingMileage = $startingMileage;

        return $this;
    }

    public function getEndingMileage(): ?int
    {
        return $this->endingMileage;
    }

    public function setEndingMileage(?int $endingMileage): self
    {
        $this->endingMileage = $endingMileage;

        return $this;
    }

    public function getStartCondition(): ?string
    {
        return $this->startCondition;
    }

    public function setStartCondition(?string $startCondition): self
    {
        $this->startCondition = $startCondition;

        return $this;
    }

    public function getEndCondition(): ?string
    {
        return $this->endCondition;
    }

    public function setEndCondition(?string $endCondition): self
    {
        $this->endCondition = $endCondition;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getDriverRoute(): ?DriverRoute
    {
        return $this->driverRoute;
    }

    public function setDriverRoute(?DriverRoute $driverRoute): self
    {
        $this->driverRoute = $driverRoute;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getTempDriverRoute(): ?TempDriverRoute
    {
        return $this->tempDriverRoute;
    }

    public function setTempDriverRoute(?TempDriverRoute $tempDriverRoute): self
    {
        $this->tempDriverRoute = $tempDriverRoute;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
