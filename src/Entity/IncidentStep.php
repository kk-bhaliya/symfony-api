<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentStepRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentStep
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncidentPhase", inversedBy="incidentSteps")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $phase;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordering;

    /**
     * @ORM\Column(type="text")
     */
    private $instructions;

    /**
     * @ORM\Column(type="integer")
     */
    private $stepNumber;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentFormFields", mappedBy="step", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentFormFields;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInWeb = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInApp = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->incidentFormFields = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPhase(): ?IncidentPhase
    {
        return $this->phase;
    }

    public function setPhase(?IncidentPhase $phase): self
    {
        $this->phase = $phase;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getStepNumber(): ?int
    {
        return $this->stepNumber;
    }

    public function setStepNumber(int $stepNumber): self
    {
        $this->stepNumber = $stepNumber;

        return $this;
    }

    /**
     * @return Collection|IncidentFormFields[]
     */
    public function getIncidentFormFields(): Collection
    {
        return $this->incidentFormFields;
    }

    public function addIncidentFormField(IncidentFormFields $incidentFormField): self
    {
        if (!$this->incidentFormFields->contains($incidentFormField)) {
            $this->incidentFormFields[] = $incidentFormField;
            $incidentFormField->setStep($this);
        }

        return $this;
    }

    public function removeIncidentFormField(IncidentFormFields $incidentFormField): self
    {
        if ($this->incidentFormFields->contains($incidentFormField)) {
            $this->incidentFormFields->removeElement($incidentFormField);
            // set the owning side to null (unless already changed)
            if ($incidentFormField->getStep() === $this) {
                $incidentFormField->setStep(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getIsInWeb(): ?bool
    {
        return $this->isInWeb;
    }

    public function setIsInWeb(bool $isInWeb): self
    {
        $this->isInWeb = $isInWeb;

        return $this;
    }

    public function getIsInApp(): ?bool
    {
        return $this->isInApp;
    }

    public function setIsInApp(bool $isInApp): self
    {
        $this->isInApp = $isInApp;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
