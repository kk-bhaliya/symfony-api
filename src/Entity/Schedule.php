<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScheduleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Schedule
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="schedules")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isRecurring;

    /**
     * @ORM\Column(type="date")
     */
    private $startDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $endDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ScheduleDesign", mappedBy="schedule", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $scheduleDesigns;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ShiftTemplate", mappedBy="schedules")
     * @MaxDepth(1)
     */
    private $shiftTemplates;
    

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Driver", inversedBy="schedules")
     * @MaxDepth(1)
     */
    private $drivers;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $previousAssignSchedule = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="schedules")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $startWeek;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $endWeek;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $scheduleLastDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="schedule")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="schedule")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $originalWeekCount;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $weekEndYear;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity=UpdatedSchedule::class, mappedBy="schedule")
     */
    private $updatedSchedules;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateUpdated;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $effectiveDate;

    /**
     * Schedule constructor.
     */
    public function __construct()
    {
        $this->scheduleDesigns = new ArrayCollection();
        $this->shiftTemplates = new ArrayCollection();
        $this->drivers = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->updatedSchedules = new ArrayCollection();
        $this->setCreatedDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
        $this->setDateUpdated((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsRecurring(): ?bool
    {
        return $this->isRecurring;
    }

    public function setIsRecurring(bool $isRecurring): self
    {
        $this->isRecurring = $isRecurring;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getDateUpdated(): ?\DateTimeInterface
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTimeInterface $dateUpdated): self
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|ScheduleDesign[]
     */
    public function getScheduleDesigns(): Collection
    {
        return $this->scheduleDesigns;
    }

    public function addScheduleDesign(ScheduleDesign $scheduleDesign): self
    {
        if (!$this->scheduleDesigns->contains($scheduleDesign)) {
            $this->scheduleDesigns[] = $scheduleDesign;
            $scheduleDesign->setSchedule($this);
        }

        return $this;
    }

    public function removeScheduleDesign(ScheduleDesign $scheduleDesign): self
    {
        if ($this->scheduleDesigns->contains($scheduleDesign)) {
            $this->scheduleDesigns->removeElement($scheduleDesign);
            // set the owning side to null (unless already changed)
            if ($scheduleDesign->getSchedule() === $this) {
                $scheduleDesign->setSchedule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ShiftTemplate[]
     */
    public function getShiftTemplates(): Collection
    {
        return $this->shiftTemplates;
    }

    public function addShiftTemplate(ShiftTemplate $shiftTemplate): self
    {
        if (!$this->shiftTemplates->contains($shiftTemplate)) {
            $this->shiftTemplates[] = $shiftTemplate;
            $shiftTemplate->addSchedule($this);
        }

        return $this;
    }

    public function removeShiftTemplate(ShiftTemplate $shiftTemplate): self
    {
        if ($this->shiftTemplates->contains($shiftTemplate)) {
            $this->shiftTemplates->removeElement($shiftTemplate);
            $shiftTemplate->removeSchedule($this);
        }

        return $this;
    }

    /**
     * @return Collection|driver[]
     */
    public function getDrivers(): Collection
    {
        return $this->drivers;
    }

    public function addDriver(driver $driver): self
    {
        if (!$this->drivers->contains($driver)) {
            $this->drivers[] = $driver;
        }

        return $this;
    }

    public function removeDriver(driver $driver): self
    {
        if ($this->drivers->contains($driver)) {
            $this->drivers->removeElement($driver);
        }

        return $this;
    }

    public function getPreviousAssignSchedule(): ?bool
    {
        return $this->previousAssignSchedule;
    }

    public function setPreviousAssignSchedule(?bool $previousAssignSchedule): self
    {
        $this->previousAssignSchedule = $previousAssignSchedule;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getStartWeek(): ?int
    {
        return $this->startWeek;
    }

    public function setStartWeek(?int $startWeek): self
    {
        $this->startWeek = $startWeek;

        return $this;
    }

    public function getEndWeek(): ?int
    {
        return $this->endWeek;
    }

    public function setEndWeek(?int $endWeek): self
    {
        $this->endWeek = $endWeek;

        return $this;
    }

    public function getScheduleLastDate(): ?\DateTimeInterface
    {
        return $this->scheduleLastDate;
    }

    public function setScheduleLastDate(?\DateTimeInterface $scheduleLastDate): self
    {
        $this->scheduleLastDate = $scheduleLastDate;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setSchedule($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getSchedule() === $this) {
                $driverRoute->setSchedule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setSchedule($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getSchedule() === $this) {
                $tempDriverRoute->setSchedule(null);
            }
        }

        return $this;
    }

    public function getOriginalWeekCount(): ?int
    {
        return $this->originalWeekCount;
    }

    public function setOriginalWeekCount(?int $originalWeekCount): self
    {
        $this->originalWeekCount = $originalWeekCount;

        return $this;
    }

    public function getWeekEndYear(): ?string
    {
        return $this->weekEndYear;
    }

    public function setWeekEndYear(?string $weekEndYear): self
    {
        $this->weekEndYear = $weekEndYear;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|UpdatedSchedule[]
     */
    public function getUpdatedSchedules(): Collection
    {
        return $this->updatedSchedules;
    }

    public function addUpdatedSchedule(UpdatedSchedule $updatedSchedule): self
    {
        if (!$this->updatedSchedules->contains($updatedSchedule)) {
            $this->updatedSchedules[] = $updatedSchedule;
            $updatedSchedule->setSchedule($this);
        }

        return $this;
    }

    public function removeUpdatedSchedule(UpdatedSchedule $updatedSchedule): self
    {
        if ($this->updatedSchedules->contains($updatedSchedule)) {
            $this->updatedSchedules->removeElement($updatedSchedule);
            // set the owning side to null (unless already changed)
            if ($updatedSchedule->getSchedule() === $this) {
                $updatedSchedule->setSchedule(null);
            }
        }

        return $this;
    }

    public function getEffectiveDate(): ?\DateTimeInterface
    {
        return $this->effectiveDate;
    }

    public function setEffectiveDate(?\DateTimeInterface $effectiveDate): self
    {
        $this->effectiveDate = $effectiveDate;

        return $this;
    }

}
