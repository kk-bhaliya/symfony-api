<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Incident
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * Incident constructor.
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $manager;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Device", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $device;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @ORM\Column(type="boolean", options={"default" : 1})
     */
    private $status = 1;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentFormFieldValue", mappedBy="incident", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentFormFieldValues;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle", inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity=IncidentType::class, inversedBy="incidents")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incidentType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=IncidentPhase::class, inversedBy="incidentCompletedPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incidentCompletedPhase;

    /**
     * @ORM\ManyToOne(targetEntity=IncidentPhase::class, inversedBy="incidentCurrentPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incidentCurrentPhase;

    /**
     * @ORM\OneToMany(targetEntity=IncidentSubmitedPhase::class, mappedBy="incident", cascade={"persist", "remove"})
     */
    private $incidentSubmitedPhases;

    /**
     * @ORM\ManyToOne(targetEntity=DriverRoute::class, inversedBy="incidents")
     */
    private $driverRoute;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="createdByIncidents")
     */
    private $createdBy;

    public function __construct()
    {
        $this->incidentFormFieldValues = new ArrayCollection();
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $this->updatedDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $this->incidentSubmitedPhases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getManager(): ?User
    {
        return $this->manager;
    }

    public function setManager(?User $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getDateTime(): ?\DateTimeInterface
    {
        return $this->dateTime;
    }

    public function setDateTime(\DateTimeInterface $dateTime): self
    {
        $this->dateTime = $dateTime;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    public function getUpdatedDate(): ?\DateTimeInterface
    {
        return $this->updatedDate;
    }

    public function setUpdatedDate(\DateTimeInterface $updatedDate): self
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    /**
     * @return Collection|IncidentFormFieldValue[]
     */
    public function getIncidentFormFieldValues(): Collection
    {
        return $this->incidentFormFieldValues;
    }

    public function addIncidentFormFieldValue(IncidentFormFieldValue $incidentFormFieldValue): self
    {
        if (!$this->incidentFormFieldValues->contains($incidentFormFieldValue)) {
            $this->incidentFormFieldValues[] = $incidentFormFieldValue;
            $incidentFormFieldValue->setIncident($this);
        }

        return $this;
    }

    public function removeIncidentFormFieldValue(IncidentFormFieldValue $incidentFormFieldValue): self
    {
        if ($this->incidentFormFieldValues->contains($incidentFormFieldValue)) {
            $this->incidentFormFieldValues->removeElement($incidentFormFieldValue);
            // set the owning side to null (unless already changed)
            if ($incidentFormFieldValue->getIncident() === $this) {
                $incidentFormFieldValue->setIncident(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }


    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getIncidentType(): ?IncidentType
    {
        return $this->incidentType;
    }

    public function setIncidentType(?IncidentType $incidentType): self
    {
        $this->incidentType = $incidentType;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getIncidentCompletedPhase(): ?IncidentPhase
    {
        return $this->incidentCompletedPhase;
    }

    public function setIncidentCompletedPhase(?IncidentPhase $incidentCompletedPhase): self
    {
        $this->incidentCompletedPhase = $incidentCompletedPhase;

        return $this;
    }

    public function getIncidentCurrentPhase(): ?IncidentPhase
    {
        return $this->incidentCurrentPhase;
    }

    public function setIncidentCurrentPhase(?IncidentPhase $incidentCurrentPhase): self
    {
        $this->incidentCurrentPhase = $incidentCurrentPhase;

        return $this;
    }

    /**
     * @return Collection|IncidentSubmitedPhase[]
     */
    public function getIncidentSubmitedPhases(): Collection
    {
        return $this->incidentSubmitedPhases;
    }

    public function addIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if (!$this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases[] = $incidentSubmitedPhase;
            $incidentSubmitedPhase->setIncident($this);
        }

        return $this;
    }

    public function removeIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if ($this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases->removeElement($incidentSubmitedPhase);
            // set the owning side to null (unless already changed)
            if ($incidentSubmitedPhase->getIncident() === $this) {
                $incidentSubmitedPhase->setIncident(null);
            }
        }

        return $this;
    }

    public function getDriverRoute(): ?DriverRoute
    {
        return $this->driverRoute;
    }

    public function setDriverRoute(?DriverRoute $driverRoute): self
    {
        $this->driverRoute = $driverRoute;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

}
