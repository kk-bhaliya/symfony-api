<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TimeOffRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TimeOff
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="timeOffs")
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $timeOff;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getTimeOff(): ?\DateTimeInterface
    {
        return $this->timeOff;
    }

    public function setTimeOff(?\DateTimeInterface $timeOff): self
    {
        $this->timeOff = $timeOff;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
