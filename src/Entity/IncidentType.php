<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentType
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="incidentTypes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentPhase", mappedBy="incidentType", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentPhases;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="incidentType", cascade={"persist", "remove"})
     */
    private $incidents;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInWeb = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInApp = true;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $prefix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->incidentPhases = new ArrayCollection();
        $this->incidents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|IncidentPhase[]
     */
    public function getIncidentPhases(): Collection
    {
        return $this->incidentPhases;
    }

    public function addIncidentPhase(IncidentPhase $incidentPhase): self
    {
        if (!$this->incidentPhases->contains($incidentPhase)) {
            $this->incidentPhases[] = $incidentPhase;
            $incidentPhase->setIncidentType($this);
        }

        return $this;
    }

    public function removeIncidentPhase(IncidentPhase $incidentPhase): self
    {
        if ($this->incidentPhases->contains($incidentPhase)) {
            $this->incidentPhases->removeElement($incidentPhase);
            // set the owning side to null (unless already changed)
            if ($incidentPhase->getIncidentType() === $this) {
                $incidentPhase->setIncidentType(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setIncidentType($this);
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getIncidentType() === $this) {
                $incident->setIncidentType(null);
            }
        }

        return $this;
    }

    public function getIsInWeb(): ?bool
    {
        return $this->isInWeb;
    }

    public function setIsInWeb(bool $isInWeb): self
    {
        $this->isInWeb = $isInWeb;

        return $this;
    }

    public function getIsInApp(): ?bool
    {
        return $this->isInApp;
    }

    public function setIsInApp(bool $isInApp): self
    {
        $this->isInApp = $isInApp;

        return $this;
    }

    public function getPrefix(): ?string
    {
        return $this->prefix;
    }

    public function setPrefix(string $prefix): self
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
