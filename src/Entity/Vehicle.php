<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VehicleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Vehicle
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $vehicleId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $vin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $make;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $modelType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licensePlate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tollCardNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $height;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateStart;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateEnd;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tankCapacity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gasType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fuelCardNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insuranceCompany;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $policyNumber;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRented;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOwned;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rentalCompany;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="vehicles")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehicleDriverRecord", mappedBy="vehicle", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $vehicleDriverRecords;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="vehicle")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEnabled = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Location", inversedBy="vehicles")
     * @MaxDepth(1)
     */
    private $location;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="vehicles")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $overnightParkingLocation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licensePlateExpiration;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insuranceCard;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $leasingCompany;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $leasingAgreementNumber;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $leasingPricePerMonth;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $leasingStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $leasingEndDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leasingDeposit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leasingInitialConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $leasingInitialConditionDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leasingReturnConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $leasingReturnConditionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $rentalAgreementNumber;

    /**
     * @ORM\Column(type="integer", length=255, nullable=true)
     */
    private $rentalPricePerMonth;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentalStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentalEndDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rentalDeposit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rentalInitialConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentalInitialConditionDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rentalReturnConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentalReturnConditionDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownedPurchaseConditionPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownedPurchaseConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ownedPurchaseConditionDate;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownedSoldConditionPrice;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ownedSoldConditionOdometer;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ownedSoldConditionDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLeased;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $tollCardStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $tollCardEndDate;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="vehicle")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="vehicle")
     */
    private $events;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentMileage = "10000";

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentGasTankLevel = "Half";

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $currentUsedBy;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $lastUsedByUser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastUsedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPending = false;

    /**
     * Vehicle constructor.
     */
    public function __construct()
    {
        $this->vehicleDriverRecords = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVin(): ?string
    {
        return $this->vin;
    }

    public function setVin(string $vin): self
    {
        $this->vin = $vin;

        return $this;
    }

    public function getLicensePlate(): ?string
    {
        return $this->licensePlate;
    }

    public function setLicensePlate(?string $licensePlate): self
    {
        $this->licensePlate = $licensePlate;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return Collection|VehicleDriverRecord[]
     */
    public function getVehicleDriverRecords(): Collection
    {
        return $this->vehicleDriverRecords;
    }

    public function addVehicleDriverRecord(VehicleDriverRecord $vehicleDriverRecord): self
    {
        if (!$this->vehicleDriverRecords->contains($vehicleDriverRecord)) {
            $this->vehicleDriverRecords[] = $vehicleDriverRecord;
            $vehicleDriverRecord->setVehicle($this);
        }

        return $this;
    }

    public function removeVehicleDriverRecord(VehicleDriverRecord $vehicleDriverRecord): self
    {
        if ($this->vehicleDriverRecords->contains($vehicleDriverRecord)) {
            $this->vehicleDriverRecords->removeElement($vehicleDriverRecord);
            // set the owning side to null (unless already changed)
            if ($vehicleDriverRecord->getVehicle() === $this) {
                $vehicleDriverRecord->setVehicle(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setVehicle($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getVehicle() === $this) {
                $driverRoute->setVehicle(null);
            }
        }

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setVehicle($this);
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(?bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getVehicleId(): ?string
    {
        return $this->vehicleId;
    }

    public function setVehicleId(?string $vehicleId): self
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function getTollCardNumber(): ?string
    {
        return $this->tollCardNumber;
    }

    public function setTollCardNumber(?string $tollCardNumber): self
    {
        $this->tollCardNumber = $tollCardNumber;

        return $this;
    }

    public function getHeight(): ?string
    {
        return $this->height;
    }

    public function setHeight(?string $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getDateStart(): ?\DateTimeInterface
    {
        return $this->dateStart;
    }

    public function setDateStart(?\DateTimeInterface $dateStart): self
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    public function getDateEnd(): ?\DateTimeInterface
    {
        return $this->dateEnd;
    }

    public function setDateEnd(?\DateTimeInterface $dateEnd): self
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    public function getTankCapacity(): ?string
    {
        return $this->tankCapacity;
    }

    public function setTankCapacity(?string $tankCapacity): self
    {
        $this->tankCapacity = $tankCapacity;

        return $this;
    }

    public function getGasType(): ?string
    {
        return $this->gasType;
    }

    public function setGasType(?string $gasType): self
    {
        $this->gasType = $gasType;

        return $this;
    }

    public function getFuelCardNumber(): ?string
    {
        return $this->fuelCardNumber;
    }

    public function setFuelCardNumber(?string $fuelCardNumber): self
    {
        $this->fuelCardNumber = $fuelCardNumber;

        return $this;
    }

    public function getInsuranceCompany(): ?string
    {
        return $this->insuranceCompany;
    }

    public function setInsuranceCompany(?string $insuranceCompany): self
    {
        $this->insuranceCompany = $insuranceCompany;

        return $this;
    }

    public function getpolicyNumber(): ?string
    {
        return $this->policyNumber;
    }

    public function setPolicyNumber(?string $policyNumber): self
    {
        $this->policyNumber = $policyNumber;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getIsRented(): ?bool
    {
        return $this->isRented;
    }

    public function setIsRented(?bool $isRented): self
    {
        $this->isRented = $isRented;

        return $this;
    }

    public function getIsOwned(): ?bool
    {
        return $this->isOwned;
    }

    public function setIsOwned(?bool $isOwned): self
    {
        $this->isOwned = $isOwned;

        return $this;
    }

    public function getRentalCompany(): ?string
    {
        return $this->rentalCompany;
    }

    public function setRentalCompany(?string $rentalCompany): self
    {
        $this->rentalCompany = $rentalCompany;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(?string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function setMake(?string $make): self
    {
        $this->make = $make;

        return $this;
    }

    public function getModelType(): ?string
    {
        return $this->modelType;
    }

    public function setModelType(?string $modelType): self
    {
        $this->modelType = $modelType;

        return $this;
    }

    public function getLocation(): ?Location
    {
        return $this->location;
    }

    public function setLocation(?Location $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getOvernightParkingLocation(): ?string
    {
        return $this->overnightParkingLocation;
    }

    public function setOvernightParkingLocation(?string $overnightParkingLocation): self
    {
        $this->overnightParkingLocation = $overnightParkingLocation;

        return $this;
    }

    public function getLicensePlateExpiration(): ?string
    {
        return $this->licensePlateExpiration;
    }

    public function setLicensePlateExpiration(?string $licensePlateExpiration): self
    {
        $this->licensePlateExpiration = $licensePlateExpiration;

        return $this;
    }

    public function getInsuranceCard(): ?string
    {
        return $this->insuranceCard;
    }

    public function setInsuranceCard(?string $insuranceCard): self
    {
        $this->insuranceCard = $insuranceCard;

        return $this;
    }

    public function getLeasingCompany(): ?string
    {
        return $this->leasingCompany;
    }

    public function setLeasingCompany(?string $leasingCompany): self
    {
        $this->leasingCompany = $leasingCompany;

        return $this;
    }

    public function getLeasingAgreementNumber(): ?string
    {
        return $this->leasingAgreementNumber;
    }

    public function setLeasingAgreementNumber(?string $leasingAgreementNumber): self
    {
        $this->leasingAgreementNumber = $leasingAgreementNumber;

        return $this;
    }

    public function getLeasingPricePerMonth(): ?string
    {
        return $this->leasingPricePerMonth;
    }

    public function setLeasingPricePerMonth(?string $leasingPricePerMonth): self
    {
        $this->leasingPricePerMonth = $leasingPricePerMonth;

        return $this;
    }

    public function getLeasingStartDate(): ?\DateTimeInterface
    {
        return $this->leasingStartDate;
    }

    public function setLeasingStartDate(?\DateTimeInterface $leasingStartDate): self
    {
        $this->leasingStartDate = $leasingStartDate;

        return $this;
    }

    public function getLeasingEndDate(): ?\DateTimeInterface
    {
        return $this->leasingEndDate;
    }

    public function setLeasingEndDate(?\DateTimeInterface $leasingEndDate): self
    {
        $this->leasingEndDate = $leasingEndDate;

        return $this;
    }

    public function getLeasingDeposit(): ?int
    {
        return $this->leasingDeposit;
    }

    public function setLeasingDeposit(?int $leasingDeposit): self
    {
        $this->leasingDeposit = $leasingDeposit;

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getVehicle() === $this) {
                $incident->setVehicle(null);
            }
        }

        return $this;
    }

    public function getLeasingInitialConditionOdometer(): ?int
    {
        return $this->leasingInitialConditionOdometer;
    }

    public function setLeasingInitialConditionOdometer(?int $leasingInitialConditionOdometer): self
    {
        $this->leasingInitialConditionOdometer = $leasingInitialConditionOdometer;

        return $this;
    }

    public function getLeasingInitialConditionDate(): ?\DateTimeInterface
    {
        return $this->leasingInitialConditionDate;
    }

    public function setLeasingInitialConditionDate(?\DateTimeInterface $leasingInitialConditionDate): self
    {
        $this->leasingInitialConditionDate = $leasingInitialConditionDate;

        return $this;
    }

    public function getLeasingReturnConditionOdometer(): ?int
    {
        return $this->leasingReturnConditionOdometer;
    }

    public function setLeasingReturnConditionOdometer(?int $leasingReturnConditionOdometer): self
    {
        $this->leasingReturnConditionOdometer = $leasingReturnConditionOdometer;

        return $this;
    }

    public function getLeasingReturnConditionDate(): ?\DateTimeInterface
    {
        return $this->leasingReturnConditionDate;
    }

    public function setLeasingReturnConditionDate(?\DateTimeInterface $leasingReturnConditionDate): self
    {
        $this->leasingReturnConditionDate = $leasingReturnConditionDate;

        return $this;
    }

    public function getRentalAgreementNumber(): ?string
    {
        return $this->rentalAgreementNumber;
    }

    public function setRentalAgreementNumber(?string $rentalAgreementNumber): self
    {
        $this->rentalAgreementNumber = $rentalAgreementNumber;

        return $this;
    }

    public function getRentalPricePerMonth(): ?string
    {
        return $this->rentalPricePerMonth;
    }

    public function setRentalPricePerMonth(?string $rentalPricePerMonth): self
    {
        $this->rentalPricePerMonth = $rentalPricePerMonth;

        return $this;
    }

    public function getRentalStartDate(): ?\DateTimeInterface
    {
        return $this->rentalStartDate;
    }

    public function setRentalStartDate(?\DateTimeInterface $rentalStartDate): self
    {
        $this->rentalStartDate = $rentalStartDate;

        return $this;
    }

    public function getRentalEndDate(): ?\DateTimeInterface
    {
        return $this->rentalEndDate;
    }

    public function setRentalEndDate(?\DateTimeInterface $rentalEndDate): self
    {
        $this->rentalEndDate = $rentalEndDate;

        return $this;
    }

    public function getRentalDeposit(): ?int
    {
        return $this->rentalDeposit;
    }

    public function setRentalDeposit(?int $rentalDeposit): self
    {
        $this->rentalDeposit = $rentalDeposit;

        return $this;
    }

    public function getRentalInitialConditionOdometer(): ?int
    {
        return $this->rentalInitialConditionOdometer;
    }

    public function setRentalInitialConditionOdometer(?int $rentalInitialConditionOdometer): self
    {
        $this->rentalInitialConditionOdometer = $rentalInitialConditionOdometer;

        return $this;
    }

    public function getRentalInitialConditionDate(): ?\DateTimeInterface
    {
        return $this->rentalInitialConditionDate;
    }

    public function setRentalInitialConditionDate(?\DateTimeInterface $rentalInitialConditionDate): self
    {
        $this->rentalInitialConditionDate = $rentalInitialConditionDate;

        return $this;
    }

    public function getRentalReturnConditionOdometer(): ?int
    {
        return $this->rentalReturnConditionOdometer;
    }

    public function setRentalReturnConditionOdometer(?int $rentalReturnConditionOdometer): self
    {
        $this->rentalReturnConditionOdometer = $rentalReturnConditionOdometer;

        return $this;
    }

    public function getRentalReturnConditionDate(): ?\DateTimeInterface
    {
        return $this->rentalReturnConditionDate;
    }

    public function setRentalReturnConditionDate(?\DateTimeInterface $rentalReturnConditionDate): self
    {
        $this->rentalReturnConditionDate = $rentalReturnConditionDate;

        return $this;
    }

    public function getOwnedPurchaseConditionPrice(): ?int
    {
        return $this->ownedPurchaseConditionPrice;
    }

    public function setOwnedPurchaseConditionPrice(?int $ownedPurchaseConditionPrice): self
    {
        $this->ownedPurchaseConditionPrice = $ownedPurchaseConditionPrice;

        return $this;
    }

    public function getOwnedPurchaseConditionOdometer(): ?int
    {
        return $this->ownedPurchaseConditionOdometer;
    }

    public function setOwnedPurchaseConditionOdometer(?int $ownedPurchaseConditionOdometer): self
    {
        $this->ownedPurchaseConditionOdometer = $ownedPurchaseConditionOdometer;

        return $this;
    }

    public function getOwnedPurchaseConditionDate(): ?\DateTimeInterface
    {
        return $this->ownedPurchaseConditionDate;
    }

    public function setOwnedPurchaseConditionDate(?\DateTimeInterface $ownedPurchaseConditionDate): self
    {
        $this->ownedPurchaseConditionDate = $ownedPurchaseConditionDate;

        return $this;
    }

    public function getOwnedSoldConditionPrice(): ?int
    {
        return $this->ownedSoldConditionPrice;
    }

    public function setOwnedSoldConditionPrice(?int $ownedSoldConditionPrice): self
    {
        $this->ownedSoldConditionPrice = $ownedSoldConditionPrice;

        return $this;
    }

    public function getOwnedSoldConditionOdometer(): ?int
    {
        return $this->ownedSoldConditionOdometer;
    }

    public function setOwnedSoldConditionOdometer(?int $ownedSoldConditionOdometer): self
    {
        $this->ownedSoldConditionOdometer = $ownedSoldConditionOdometer;

        return $this;
    }

    public function getOwnedSoldConditionDate(): ?\DateTimeInterface
    {
        return $this->ownedSoldConditionDate;
    }

    public function setOwnedSoldConditionDate(?\DateTimeInterface $ownedSoldConditionDate): self
    {
        $this->ownedSoldConditionDate = $ownedSoldConditionDate;

        return $this;
    }

    public function getIsLeased(): ?bool
    {
        return $this->isLeased;
    }

    public function setIsLeased(?bool $isLeased): self
    {
        $this->isLeased = $isLeased;

        return $this;
    }

    public function getTollCardStartDate(): ?\DateTimeInterface
    {
        return $this->tollCardStartDate;
    }

    public function setTollCardStartDate(?\DateTimeInterface $tollCardStartDate): self
    {
        $this->tollCardStartDate = $tollCardStartDate;

        return $this;
    }

    public function getTollCardEndDate(): ?\DateTimeInterface
    {
        return $this->tollCardEndDate;
    }

    public function setTollCardEndDate(?\DateTimeInterface $tollCardEndDate): self
    {
        $this->tollCardEndDate = $tollCardEndDate;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setVehicle($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getVehicle() === $this) {
                $event->setVehicle(null);
            }
        }

        return $this;
    }

    public function getCurrentMileage(): ?string
    {
        return $this->currentMileage;
    }

    public function setCurrentMileage(?string $currentMileage): self
    {
        $this->currentMileage = $currentMileage;

        return $this;
    }

    public function getCurrentGasTankLevel(): ?string
    {
        return $this->currentGasTankLevel;
    }

    public function setCurrentGasTankLevel(?string $currentGasTankLevel): self
    {
        $this->currentGasTankLevel = $currentGasTankLevel;

        return $this;
    }

    public function getCurrentUsedBy(): ?string
    {
        return $this->currentUsedBy;
    }

    public function setCurrentUsedBy(?string $currentUsedBy): self
    {
        $this->currentUsedBy = $currentUsedBy;

        return $this;
    }

    public function getLastUsedByUser(): ?User
    {
        return $this->lastUsedByUser;
    }

    public function setLastUsedByUser(?User $lastUsedByUser): self
    {
        $this->lastUsedByUser = $lastUsedByUser;

        return $this;
    }

    public function getLastUsedAt(): ?\DateTimeInterface
    {
        return $this->lastUsedAt;
    }

    public function setLastUsedAt(?\DateTimeInterface $lastUsedAt): self
    {
        $this->lastUsedAt = $lastUsedAt;

        return $this;
    }

    public function getIsPending(): ?bool
    {
        return $this->isPending;
    }

    public function setIsPending(?bool $isPending): self
    {
        $this->isPending = $isPending;

        return $this;
    }
}
