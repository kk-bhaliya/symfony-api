<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\InvoiceTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class InvoiceType
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shift", mappedBy="invoiceType")
     * @MaxDepth(1)
     */
    private $shifts;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="invoiceTypes")
     * @ORM\JoinColumn(nullable=true)
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @MaxDepth(1)
     */
    private $billableHour;

    /**
     * @ORM\ManyToOne(targetEntity="Rate", inversedBy="invoiceTypes")
     * @MaxDepth(1)
     */
    private $rateRule;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ShiftTemplate", mappedBy="invoiceWorkType")
     * @MaxDepth(1)
     */
    private $shiftTemplates;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $invoiceType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="shiftInvoiceType")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="shiftInvoiceType")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    public function __construct()
    {
        $this->shifts = new ArrayCollection();
        $this->shiftTemplates = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getBillableHour(): ?string
    {
        return $this->billableHour;
    }

    public function setBillableHour(?string $billableHour): self
    {
        $this->billableHour = $billableHour;

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setInvoiceType($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getInvoiceType() === $this) {
                $shift->setInvoiceType(null);
            }
        }

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getRateRule(): ?Rate
    {
        return $this->rateRule;
    }

    public function setRateRule(?Rate $rateRule): self
    {
        $this->rateRule = $rateRule;

        return $this;
    }

    /**
     * @return Collection|ShiftTemplate[]
     */
    public function getShiftTemplates(): Collection
    {
        return $this->shiftTemplates;
    }

    public function addShiftTemplate(ShiftTemplate $shiftTemplate): self
    {
        if (!$this->shiftTemplates->contains($shiftTemplate)) {
            $this->shiftTemplates[] = $shiftTemplate;
            $shiftTemplate->setInvoiceWorkType($this);
        }

        return $this;
    }

    public function removeShiftTemplate(ShiftTemplate $shiftTemplate): self
    {
        if ($this->shiftTemplates->contains($shiftTemplate)) {
            $this->shiftTemplates->removeElement($shiftTemplate);
            // set the owning side to null (unless already changed)
            if ($shiftTemplate->getInvoiceWorkType() === $this) {
                $shiftTemplate->setInvoiceWorkType(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getInvoiceType(): ?string
    {
        return $this->invoiceType;
    }

    public function setInvoiceType(?string $invoiceType): self
    {
        $this->invoiceType = $invoiceType;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setShiftInvoiceType($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getShiftInvoiceType() === $this) {
                $driverRoute->setShiftInvoiceType(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setShiftInvoiceType($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getShiftInvoiceType() === $this) {
                $tempDriverRoute->setShiftInvoiceType(null);
            }
        }

        return $this;
    }

}
