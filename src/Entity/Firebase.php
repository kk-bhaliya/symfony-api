<?php

namespace App\Entity;

use App\Repository\FirebaseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FirebaseRepository::class)
 */
class Firebase
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $keyPem;

    /**
     * @ORM\Column(type="text")
     */
    private $certPem;

    /**
     * @ORM\Column(type="text")
     */
    private $serverKey;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyPem(): ?string
    {
        return $this->keyPem;
    }

    public function setKeyPem(string $keyPem): self
    {
        $this->keyPem = $keyPem;

        return $this;
    }

    public function getCertPem(): ?string
    {
        return $this->certPem;
    }

    public function setCertPem(string $certPem): self
    {
        $this->certPem = $certPem;

        return $this;
    }

    public function getServerKey(): ?string
    {
        return $this->serverKey;
    }

    public function setServerKey(string $serverKey): self
    {
        $this->serverKey = $serverKey;

        return $this;
    }
}
