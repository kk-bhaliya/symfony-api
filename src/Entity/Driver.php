<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DriverRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Driver
{
    use Timestamps;

    const STATUS_TERMINATED = -2;
    const STATUS_OFFBOARDED = -1;
    const STATUS_INACTIVE = 0;
    const STATUS_ONBOARDED = 1;
    const STATUS_ACTIVE = 2;

    public static $statuses = array(
        self::STATUS_TERMINATED => 'Terminated',
        self::STATUS_OFFBOARDED => 'Offboarded',
        self::STATUS_INACTIVE => 'Inactive',
        self::STATUS_ONBOARDED => 'Onboarding',
        self::STATUS_ACTIVE => 'Active'
    );

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $jobTitle;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $personalPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $companyPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $driversLicenseNumber;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $hireDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Station", inversedBy="drivers")
     * @MaxDepth(1)
     */
    private $stations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Score", mappedBy="driver", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $scores;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $birthday;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shirtSize;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shoeSize;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pantSize;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", inversedBy="driver", cascade={"persist", "remove"}, fetch="EAGER")
     * @MaxDepth(1)
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fireDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmploymentStatus", inversedBy="drivers")
     * @ORM\JoinColumn(nullable=true)
     * @MaxDepth(1)
     */
    private $employmentStatus;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="managerDrivers")
     * @MaxDepth(1)
     */
    private $assignedManagers;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PayRate", mappedBy="driver", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $payRate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $discretionaryBonus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ssn;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $veteranStatus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $secondaryEmail;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $fullAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transporterID;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $licenseState;


    /**
     * @ORM\Column(type="date", length=512, nullable=true)
     */
    private $expectedFirstDayOnTheRoad;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $needsTraining;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $maritalStatus;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $drivingPreference;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmergencyContact", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $emergencyContacts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverSkill", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $driverSkills;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="drivers")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeOff", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $timeOffs;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $driversLicenseNumberExpiration;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Schedule", mappedBy="drivers")
     * @MaxDepth(1)
     */
    private $schedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\CortexLog", mappedBy="drivers")
     * @MaxDepth(1)
     */
    private $cortexLogs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maximumWeeklyHour;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profileImage;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="oldDriver")
     * @MaxDepth(1)
     */
    private $oldDriverRoutes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="oldDriver")
     * @MaxDepth(1)
     */
    private $oldTempDriverRoutes;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $paycomId;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity=RouteRequests::class, mappedBy="newDriver")
     * @MaxDepth(1)
     */
    private $newDriverRequest;

    /**
     * @ORM\OneToMany(targetEntity=RouteRequests::class, mappedBy="oldDriver")
     * @MaxDepth(1)
     */
    private $oldDriverRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\VehicleDriverRecord", mappedBy="driver")
     * @MaxDepth(1)
     */
    private $vehicleDriverRecords;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $terminationDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $offboardedDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, options={"default": 2})
     */
    private $employeeStatus = self::STATUS_ACTIVE;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $inactiveAt;



    public function __construct()
    {
        // $this->vehicleDriverRecords = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->stations = new ArrayCollection();
        $this->scores = new ArrayCollection();
        $this->assignedManagers = new ArrayCollection();
        $this->emergencyContacts = new ArrayCollection();
        $this->driverSkills = new ArrayCollection();
        $this->timeOffs = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->cortexLogs = new ArrayCollection();
        $this->oldDriverRoutes = new ArrayCollection();
        $this->oldTempDriverRoutes = new ArrayCollection();
        $this->newDriverRequest = new ArrayCollection();
        $this->oldDriverRequests = new ArrayCollection();
    }

    public function getFullName(): string
    {
        return ucfirst($this->getUser()->getFriendlyName());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getJobTitle(): ?string
    {
        return $this->jobTitle;
    }

    public function setJobTitle(string $jobTitle): self
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    public function getPersonalPhone(): ?string
    {
        return $this->personalPhone;
    }

    public function setPersonalPhone(?string $personalPhone): self
    {
        $this->personalPhone = $personalPhone;

        return $this;
    }

    public function getCompanyPhone(): ?string
    {
        return $this->companyPhone;
    }

    public function setCompanyPhone(string $companyPhone): self
    {
        $this->companyPhone = $companyPhone;

        return $this;
    }

    public function getDriversLicenseNumber(): ?string
    {
        return $this->driversLicenseNumber;
    }

    public function setDriversLicenseNumber(?string $driversLicenseNumber): self
    {
        $this->driversLicenseNumber = $driversLicenseNumber;

        return $this;
    }

    public function getHireDate(): ?\DateTimeInterface
    {
        return $this->hireDate;
    }

    public function setHireDate(\DateTimeInterface $hireDate): self
    {
        $this->hireDate = $hireDate;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getShirtSize(): ?string
    {
        return $this->shirtSize;
    }

    public function setShirtSize(?string $shirtSize): self
    {
        $this->shirtSize = $shirtSize;

        return $this;
    }

    public function getShoeSize(): ?string
    {
        return $this->shoeSize;
    }

    public function setShoeSize(?string $shoeSize): self
    {
        $this->shoeSize = $shoeSize;

        return $this;
    }

    public function getPantSize(): ?string
    {
        return $this->pantSize;
    }

    public function setPantSize(?string $pantSize): self
    {
        $this->pantSize = $pantSize;

        return $this;
    }

    public function getFireDate(): ?\DateTimeInterface
    {
        return $this->fireDate;
    }

    public function setFireDate(?\DateTimeInterface $fireDate): self
    {
        $this->fireDate = $fireDate;

        return $this;
    }

    public function getDiscretionaryBonus(): ?bool
    {
        return $this->discretionaryBonus;
    }

    public function setDiscretionaryBonus(bool $discretionaryBonus): self
    {
        $this->discretionaryBonus = $discretionaryBonus;

        return $this;
    }

    public function getSsn(): ?string
    {
        return $this->ssn;
    }

    public function setSsn(?string $ssn): self
    {
        $this->ssn = $ssn;

        return $this;
    }

    public function getVeteranStatus(): ?int
    {
        return $this->veteranStatus;
    }

    public function setVeteranStatus(?int $veteranStatus): self
    {
        $this->veteranStatus = $veteranStatus;

        return $this;
    }

    public function getSecondaryEmail(): ?string
    {
        return $this->secondaryEmail;
    }

    public function setSecondaryEmail(?string $secondaryEmail): self
    {
        $this->secondaryEmail = $secondaryEmail;

        return $this;
    }

    public function getFullAddress(): ?string
    {
        return $this->fullAddress;
    }

    public function setFullAddress(?string $fullAddress): self
    {
        $this->fullAddress = $fullAddress;

        return $this;
    }

    public function getTransporterID(): ?string
    {
        return $this->transporterID;
    }

    public function setTransporterID(?string $transporterID): self
    {
        $this->transporterID = $transporterID;

        return $this;
    }

    public function getLicenseState(): ?string
    {
        return $this->licenseState;
    }

    public function setLicenseState(?string $licenseState): self
    {
        $this->licenseState = $licenseState;

        return $this;
    }

    public function getNeedsTraining(): ?bool
    {
        return $this->needsTraining;
    }

    public function setNeedsTraining(?bool $needsTraining): self
    {
        $this->needsTraining = $needsTraining;

        return $this;
    }

    /**
     * @return Collection|VehicleDriverRecord[]
     */
    // public function getVehicleDriverRecords(): Collection
    // {
    //     return $this->vehicleDriverRecords;
    // }

    // public function addVehicleDriverRecord(VehicleDriverRecord $vehicleDriverRecord): self
    // {
    //     if (!$this->vehicleDriverRecords->contains($vehicleDriverRecord)) {
    //         $this->vehicleDriverRecords[] = $vehicleDriverRecord;
    //         $vehicleDriverRecord->setDriver($this);
    //     }

    //     return $this;
    // }

    // public function removeVehicleDriverRecord(VehicleDriverRecord $vehicleDriverRecord): self
    // {
    //     if ($this->vehicleDriverRecords->contains($vehicleDriverRecord)) {
    //         $this->vehicleDriverRecords->removeElement($vehicleDriverRecord);
    //         // set the owning side to null (unless already changed)
    //         if ($vehicleDriverRecord->getDriver() === $this) {
    //             $vehicleDriverRecord->setDriver(null);
    //         }
    //     }

    //     return $this;
    // }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setDriver($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getDriver() === $this) {
                $driverRoute->setDriver(null);
            }
        }

        return $this;
    }


    /**
     * Return First Station
     * @return Station
     * @throws Exception
     */
    public function getMainStation(): Station
    {
        $stations = $this->stations;
        if ($stations->count() > 0) {
            foreach ($stations as $station) {
                /** @var Station $station */
                if ($station->getIsArchive() === false) {
                    return $station;
                }
            }
        }
        throw new Exception('Station not found with Driver ID: ' . $this->getId());
    }

    /**
     * @return Collection|Station[]
     */
    public function getStations(): Collection
    {
        return $this->stations;
    }

    public function addStation(Station $station): self
    {
        if (!$this->stations->contains($station)) {
            $this->stations[] = $station;
        }

        return $this;
    }

    public function removeStation(Station $station): self
    {
        if ($this->stations->contains($station)) {
            $this->stations->removeElement($station);
        }

        return $this;
    }

    /**
     * @return Collection|Score[]
     */
    public function getScores(): Collection
    {
        return $this->scores;
    }

    public function addScore(Score $score): self
    {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setDriver($this);
        }

        return $this;
    }

    public function removeScore(Score $score): self
    {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getDriver() === $this) {
                $score->setDriver(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEmploymentStatus(): ?EmploymentStatus
    {
        return $this->employmentStatus;
    }

    public function setEmploymentStatus(?EmploymentStatus $employmentStatus): self
    {
        $this->employmentStatus = $employmentStatus;

        return $this;
    }


    public function getMainManager(): ?User
    {
        $managers = $this->assignedManagers;
        if ($managers->count() > 0) {
            foreach ($managers as $manager) {
                /** @var User $manager */
                if ($manager->getIsArchive() === false) {
                    return $manager;
                }
            }
        }
        return null;
    }

    /**
     * @return Collection|User[]
     */
    public function getAssignedManagers(): Collection
    {
        return $this->assignedManagers;
    }

    public function addAssignedManager(User $assignedManager): self
    {
        if (!$this->assignedManagers->contains($assignedManager)) {
            $this->assignedManagers[] = $assignedManager;
        }

        return $this;
    }

    public function removeAssignedManager(User $assignedManager): self
    {
        if ($this->assignedManagers->contains($assignedManager)) {
            $this->assignedManagers->removeElement($assignedManager);
        }

        return $this;
    }

    // public function getPayRate(): ?PayRate
    // {
    //     return $this->payRate;
    // }

    // public function setPayRate(?PayRate $payRate): self
    // {
    //     $this->payRate = $payRate;

    //     // set (or unset) the owning side of the relation if necessary
    //     $newDriver = null === $payRate ? null : $this;
    //     if ($payRate->getDriver() !== $newDriver) {
    //         $payRate->setDriver($newDriver);
    //     }

    //     return $this;
    // }

    public function getMaritalStatus(): ?string
    {
        return $this->maritalStatus;
    }

    public function setMaritalStatus(?string $maritalStatus): self
    {
        $this->maritalStatus = $maritalStatus;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getDrivingPreference(): ?string
    {
        return $this->drivingPreference;
    }

    public function setDrivingPreference(?string $drivingPreference): self
    {
        $this->drivingPreference = $drivingPreference;

        return $this;
    }

    /**
     * @return Collection|EmergencyContact[]
     */
    public function getEmergencyContacts(): Collection
    {
        return $this->emergencyContacts;
    }

    public function addEmergencyContact(EmergencyContact $emergencyContact): self
    {
        if (!$this->emergencyContacts->contains($emergencyContact)) {
            $this->emergencyContacts[] = $emergencyContact;
            $emergencyContact->setDriver($this);
        }

        return $this;
    }

    public function removeEmergencyContact(EmergencyContact $emergencyContact): self
    {
        if ($this->emergencyContacts->contains($emergencyContact)) {
            $this->emergencyContacts->removeElement($emergencyContact);
            // set the owning side to null (unless already changed)
            if ($emergencyContact->getDriver() === $this) {
                $emergencyContact->setDriver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DriverSkill[]
     */
    public function getDriverSkills(): Collection
    {
        return $this->driverSkills;
    }

    public function addDriverSkill(DriverSkill $driverSkill): self
    {
        if (!$this->driverSkills->contains($driverSkill)) {
            $this->driverSkills[] = $driverSkill;
            $driverSkill->setDriver($this);
        }

        return $this;
    }

    public function removeDriverSkill(DriverSkill $driverSkill): self
    {
        if ($this->driverSkills->contains($driverSkill)) {
            $this->driverSkills->removeElement($driverSkill);
            // set the owning side to null (unless already changed)
            if ($driverSkill->getDriver() === $this) {
                $driverSkill->setDriver(null);
            }
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|TimeOff[]
     */
    public function getTimeOffs(): Collection
    {
        return $this->timeOffs;
    }

    public function addTimeOff(TimeOff $timeOff): self
    {
        if (!$this->timeOffs->contains($timeOff)) {
            $this->timeOffs[] = $timeOff;
            $timeOff->setDriver($this);
        }

        return $this;
    }

    public function removeTimeOff(TimeOff $timeOff): self
    {
        if ($this->timeOffs->contains($timeOff)) {
            $this->timeOffs->removeElement($timeOff);
            // set the owning side to null (unless already changed)
            if ($timeOff->getDriver() === $this) {
                $timeOff->setDriver(null);
            }
        }

        return $this;
    }

    public function getExpectedFirstDayOnTheRoad(): ?\DateTimeInterface
    {
        return $this->expectedFirstDayOnTheRoad;
    }

    public function setExpectedFirstDayOnTheRoad(?\DateTimeInterface $expectedFirstDayOnTheRoad): self
    {
        $this->expectedFirstDayOnTheRoad = $expectedFirstDayOnTheRoad;

        return $this;
    }

    public function getDriversLicenseNumberExpiration(): ?\DateTimeInterface
    {
        return $this->driversLicenseNumberExpiration;
    }

    public function setDriversLicenseNumberExpiration(?\DateTimeInterface $driversLicenseNumberExpiration): self
    {
        $this->driversLicenseNumberExpiration = $driversLicenseNumberExpiration;

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->addDriver($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->contains($schedule)) {
            $this->schedules->removeElement($schedule);
            $schedule->removeDriver($this);
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setDriver($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getDriver() === $this) {
                $tempDriverRoute->setDriver(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    /**
     * @return Collection|CortexLog[]
     */
    public function getCortexLogs(): Collection
    {
        return $this->cortexLogs;
    }

    public function addCortexLog(CortexLog $cortexLog): self
    {
        if (!$this->cortexLogs->contains($cortexLog)) {
            $this->cortexLogs[] = $cortexLog;
            $cortexLog->addDriver($this);
        }

        return $this;
    }

    public function removeCortexLog(CortexLog $cortexLog): self
    {
        if ($this->cortexLogs->contains($cortexLog)) {
            $this->cortexLogs->removeElement($cortexLog);
            $cortexLog->removeDriver($this);
        }

        return $this;
    }

    public function getMaximumWeeklyHour(): ?int
    {
        return $this->maximumWeeklyHour;
    }

    public function setMaximumWeeklyHour(?int $maximumWeeklyHour): self
    {
        $this->maximumWeeklyHour = $maximumWeeklyHour;

        return $this;
    }

    public function getProfileImage(): ?string
    {
        return $this->profileImage;
    }

    public function setProfileImage(?string $profileImage): self
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getOldDriverRoutes(): Collection
    {
        return $this->oldDriverRoutes;
    }

    public function addOldDriverRoute(DriverRoute $oldDriverRoute): self
    {
        if (!$this->oldDriverRoutes->contains($oldDriverRoute)) {
            $this->oldDriverRoutes[] = $oldDriverRoute;
            $oldDriverRoute->setOldDriver($this);
        }

        return $this;
    }

    public function removeOldDriverRoute(DriverRoute $oldDriverRoute): self
    {
        if ($this->oldDriverRoutes->contains($oldDriverRoute)) {
            $this->oldDriverRoutes->removeElement($oldDriverRoute);
            // set the owning side to null (unless already changed)
            if ($oldDriverRoute->getOldDriver() === $this) {
                $oldDriverRoute->setOldDriver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getOldTempDriverRoutes(): Collection
    {
        return $this->oldTempDriverRoutes;
    }

    public function addOldTempDriverRoute(TempDriverRoute $oldTempDriverRoute): self
    {
        if (!$this->oldTempDriverRoutes->contains($oldTempDriverRoute)) {
            $this->oldTempDriverRoutes[] = $oldTempDriverRoute;
            $oldTempDriverRoute->setOldDriver($this);
        }

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setDriver($this);
        }

        return $this;
    }

    public function removeOldTempDriverRoute(TempDriverRoute $oldTempDriverRoute): self
    {
        if ($this->oldTempDriverRoutes->contains($oldTempDriverRoute)) {
            $this->oldTempDriverRoutes->removeElement($oldTempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($oldTempDriverRoute->getOldDriver() === $this) {
                $oldTempDriverRoute->setOldDriver(null);
            }
        }

        return $this;
    }

    public function getPaycomId(): ?string
    {
        return $this->paycomId;
    }

    public function setPaycomId(?string $paycomId): self
    {
        $this->paycomId = $paycomId;

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getDriver() === $this) {
                $incident->setDriver(null);
            }
        }
        return $this;
    }

    public function getEmail(): ?string
    {
        $user = $this->getUser();
        if ($user) {
            return $user->getEmail();
        }
        return null;
    }

    public function getChatIdentifier(): ?string
    {
        $user = $this->getUser();
        if ($user) {
            return $user->getChatIdentifier();
        }
        return null;
    }

    public function getFriendlyName(): ?string
    {
        return $this->getUser()->getFriendlyName();
    }

    /**
     * @return Collection|RouteRequests[]
     */
    public function getNewDriverRequest(): Collection
    {
        return $this->newDriverRequest;
    }

    public function addNewDriverRequest(RouteRequests $newDriverRequest): self
    {
        if (!$this->newDriverRequest->contains($newDriverRequest)) {
            $this->newDriverRequest[] = $newDriverRequest;
            $newDriverRequest->setNewDriver($this);
        }

        return $this;
    }

    public function removeNewDriverRequest(RouteRequests $newDriverRequest): self
    {
        if ($this->newDriverRequest->contains($newDriverRequest)) {
            $this->newDriverRequest->removeElement($newDriverRequest);
            // set the owning side to null (unless already changed)
            if ($newDriverRequest->getNewDriver() === $this) {
                $newDriverRequest->setNewDriver(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RouteRequests[]
     */
    public function getOldDriverRequests(): Collection
    {
        return $this->oldDriverRequests;
    }

    public function addOldDriverRequest(RouteRequests $oldDriverRequest): self
    {
        if (!$this->oldDriverRequests->contains($oldDriverRequest)) {
            $this->oldDriverRequests[] = $oldDriverRequest;
            $oldDriverRequest->setOldDriver($this);
        }

        return $this;
    }

    public function removeOldDriverRequest(RouteRequests $oldDriverRequest): self
    {
        if ($this->oldDriverRequests->contains($oldDriverRequest)) {
            $this->oldDriverRequests->removeElement($oldDriverRequest);
            // set the owning side to null (unless already changed)
            if ($oldDriverRequest->getOldDriver() === $this) {
                $oldDriverRequest->setOldDriver(null);
            }
        }

        return $this;
    }

    public function getTerminationDate(): ?\DateTimeInterface
    {
        return $this->terminationDate;
    }

    public function setTerminationDate(?\DateTimeInterface $terminationDate): self
    {
        $this->terminationDate = $terminationDate;

        return $this;
    }

    public function getOffboardedDate(): ?\DateTimeInterface
    {
        return $this->offboardedDate;
    }

    public function setOffboardedDate(?\DateTimeInterface $offboardedDate): self
    {
        $this->offboardedDate = $offboardedDate;

        return $this;
    }

    public function getEmployeeStatus(): ?string
    {
        return $this->employeeStatus;
    }

    public function setEmployeeStatus(?string $employeeStatus): self
    {
        $this->employeeStatus = $employeeStatus;

        return $this;
    }

    public function getInactiveAt(): ?\DateTimeInterface
    {
        return $this->inactiveAt;
    }

    public function setInactiveAt(?\DateTimeInterface $inactiveAt): self
    {
        $this->inactiveAt = $inactiveAt;

        return $this;
    }
}
