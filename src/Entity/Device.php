<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Device
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="devices")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imei;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $carrier;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deviceCondition;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $history;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $device;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $assetTag;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $platform;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $osVersion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mdmClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $number;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowKickoff = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deviceId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $deviceUid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="devices")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPersonalDevice = false;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOwned = true;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $ownedTermStartDate;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRented = false;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentedStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $rentedEndDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $lastInventoryDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="device")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\DriverRoute", mappedBy="device")
     */
    private $driverRoutes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowLoadOutProcess = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $allowReturnToStation = true;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="device")
     * @MaxDepth(1)
     */
    private $events;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $usedBy;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $usedByUser;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $usedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isPending;

    public function __construct()
    {
        $this->driverRoutes = new ArrayCollection();
        //$this->tempDriverRoutes = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImei(): ?string
    {
        return $this->imei;
    }

    public function setImei(?string $imei): self
    {
        $this->imei = $imei;

        return $this;
    }

    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    public function setCarrier(?string $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getDeviceCondition(): ?string
    {
        return $this->deviceCondition;
    }

    public function setDeviceCondition(?string $deviceCondition): self
    {
        $this->deviceCondition = $deviceCondition;

        return $this;
    }

    public function getHistory(): ?string
    {
        return $this->history;
    }

    public function setHistory(?string $history): self
    {
        $this->history = $history;

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice(?string $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setDevice($this);
        }

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(?string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getAssetTag(): ?string
    {
        return $this->assetTag;
    }

    public function setAssetTag(?string $assetTag): self
    {
        $this->assetTag = $assetTag;

        return $this;
    }

    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getOsVersion(): ?string
    {
        return $this->osVersion;
    }

    public function setOsVersion(string $osVersion): self
    {
        $this->osVersion = $osVersion;

        return $this;
    }

    public function getMdmClient(): ?string
    {
        return $this->mdmClient;
    }

    public function setMdmClient(string $mdmClient): self
    {
        $this->mdmClient = $mdmClient;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getNumber(): ?string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getAllowKickoff(): ?bool
    {
        return $this->allowKickoff;
    }

    public function setAllowKickoff(bool $allowKickoff): self
    {
        $this->allowKickoff = $allowKickoff;

        return $this;
    }

    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    public function setDeviceId(string $deviceId): self
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    public function getDeviceUid(): ?string
    {
        return $this->deviceUid;
    }

    public function setDeviceUid(string $deviceUid): self
    {
        $this->deviceUid = $deviceUid;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getIsPersonalDevice(): ?bool
    {
        return $this->isPersonalDevice;
    }

    public function setIsPersonalDevice(bool $isPersonalDevice): self
    {
        $this->isPersonalDevice = $isPersonalDevice;

        return $this;
    }

    public function getIsOwned(): ?bool
    {
        return $this->isOwned;
    }

    public function setIsOwned(?bool $isOwned): self
    {
        $this->isOwned = $isOwned;

        return $this;
    }

    public function getOwnedTermStartDate(): ?\DateTimeInterface
    {
        return $this->ownedTermStartDate;
    }

    public function setOwnedTermStartDate(?\DateTimeInterface $ownedTermStartDate): self
    {
        $this->ownedTermStartDate = $ownedTermStartDate;

        return $this;
    }

    public function getIsRented(): ?bool
    {
        return $this->isRented;
    }

    public function setIsRented(?bool $isRented): self
    {
        $this->isRented = $isRented;

        return $this;
    }

    public function getRentedStartDate(): ?\DateTimeInterface
    {
        return $this->rentedStartDate;
    }

    public function setRentedStartDate(?\DateTimeInterface $rentedStartDate): self
    {
        $this->rentedStartDate = $rentedStartDate;

        return $this;
    }

    public function getRentedEndDate(): ?\DateTimeInterface
    {
        return $this->rentedEndDate;
    }

    public function setRentedEndDate(?\DateTimeInterface $rentedEndDate): self
    {
        $this->rentedEndDate = $rentedEndDate;

        return $this;
    }

    public function getLastInventoryDate(): ?\DateTimeInterface
    {
        return $this->lastInventoryDate;
    }

    public function setLastInventoryDate(?\DateTimeInterface $lastInventoryDate): self
    {
        $this->lastInventoryDate = $lastInventoryDate;

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getDevice() === $this) {
                $incident->setDevice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->addDevice($this);
        }

        return $this;
    }


    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            $driverRoute->removeDevice($this);
        }

        return $this;
    }

    public function getAllowLoadOutProcess(): ?bool
    {
        return $this->allowLoadOutProcess;
    }

    public function setAllowLoadOutProcess(bool $allowLoadOutProcess): self
    {
        $this->allowLoadOutProcess = $allowLoadOutProcess;

        return $this;
    }

    public function getAllowReturnToStation(): ?bool
    {
        return $this->allowReturnToStation;
    }

    public function setAllowReturnToStation(bool $allowReturnToStation): self
    {
        $this->allowReturnToStation = $allowReturnToStation;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setDevice($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getDevice() === $this) {
                $event->setDevice(null);
            }
        }

        return $this;
    }

    public function getUsedBy(): ?string
    {
        return $this->usedBy;
    }

    public function setUsedBy(?string $usedBy): self
    {
        $this->usedBy = $usedBy;

        return $this;
    }

    public function getUsedByUser(): ?User
    {
        return $this->usedByUser;
    }

    public function setUsedByUser(?User $usedByUser): self
    {
        $this->usedByUser = $usedByUser;

        return $this;
    }

    public function getUsedAt(): ?\DateTimeInterface
    {
        return $this->usedAt;
    }

    public function setUsedAt(?\DateTimeInterface $usedAt): self
    {
        $this->usedAt = $usedAt;

        return $this;
    }

    public function getIsPending(): ?bool
    {
        return $this->isPending;
    }

    public function setIsPending(?bool $isPending): self
    {
        $this->isPending = $isPending;

        return $this;
    }
}
