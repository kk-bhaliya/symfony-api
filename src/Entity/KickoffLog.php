<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KickoffLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class KickoffLog
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedInAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedTouchPayrollAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedAssignedVanAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $scannedQRCodeAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedTouchFlexAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedTouchMentorAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $assignedVan;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $mileage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gasTankLevel;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedVanSuppliesAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $loggedMileageAndGasAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\DriverRoute", cascade={"persist", "remove"})
     */
    private $driverRoute;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $vanSuppliesResult = [];

    /**
     * @ORM\Column(type="boolean")
     */
    private $processCompleted = false;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle")
     */
    private $vehicle;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $stationArrivalAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $scannedQRCode = false;

    /**
     * @ORM\ManyToOne(targetEntity=Device::class)
     */
    private $device;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qrCodeValue;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $manualEntryVehicleIdentificationAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $completedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $qrCodePictureUrl;


    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLoggedInAt(): ?\DateTimeInterface
    {
        return $this->loggedInAt;
    }

    public function setLoggedInAt(?\DateTimeInterface $loggedInAt): self
    {
        $this->loggedInAt = $loggedInAt;

        return $this;
    }

    public function getScannedQRCodeAt(): ?\DateTimeInterface
    {
        return $this->scannedQRCodeAt;
    }

    public function setScannedQRCodeAt(?\DateTimeInterface $scannedQRCodeAt): self
    {
        $this->scannedQRCodeAt = $scannedQRCodeAt;

        return $this;
    }

    public function getLoggedTouchFlexAt(): ?\DateTimeInterface
    {
        return $this->loggedTouchFlexAt;
    }

    public function setLoggedTouchFlexAt(?\DateTimeInterface $loggedTouchFlexAt): self
    {
        $this->loggedTouchFlexAt = $loggedTouchFlexAt;

        return $this;
    }


    public function getLoggedTouchMentorAt(): ?\DateTimeInterface
    {
        return $this->loggedTouchMentorAt;
    }

    public function setLoggedTouchMentorAt(?\DateTimeInterface $loggedTouchMentorAt): self
    {
        $this->loggedTouchMentorAt = $loggedTouchMentorAt;

        return $this;
    }

    public function getLoggedTouchPayrollAt(): ?\DateTimeInterface
    {
        return $this->loggedTouchPayrollAt;
    }

    public function setLoggedTouchPayrollAt(?\DateTimeInterface $loggedTouchPayrollAt): self
    {
        $this->loggedTouchPayrollAt = $loggedTouchPayrollAt;

        return $this;
    }

    public function getAssignedVan(): ?bool
    {
        return $this->assignedVan;
    }

    public function setAssignedVan(?bool $assignedVan): self
    {
        $this->assignedVan = $assignedVan;

        return $this;
    }

    public function getMileage(): ?float
    {
        return $this->mileage;
    }

    public function setMileage(?float $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getGasTankLevel(): ?string
    {
        return $this->gasTankLevel;
    }

    public function setGasTankLevel(?string $gasTankLevel): self
    {
        $this->gasTankLevel = $gasTankLevel;

        return $this;
    }

    public function getLoggedVanSuppliesAt(): ?\DateTimeInterface
    {
        return $this->loggedVanSuppliesAt;
    }

    public function setLoggedVanSuppliesAt(?\DateTimeInterface $loggedVanSuppliesAt): self
    {
        $this->loggedVanSuppliesAt = $loggedVanSuppliesAt;

        return $this;
    }

    public function getLoggedAssignedVanAt(): ?\DateTimeInterface
    {
        return $this->loggedAssignedVanAt;
    }

    public function setLoggedAssignedVanAt(?\DateTimeInterface $loggedAssignedVanAt): self
    {
        $this->loggedAssignedVanAt = $loggedAssignedVanAt;

        return $this;
    }

    public function getLoggedMileageAndGasAt(): ?\DateTimeInterface
    {
        return $this->loggedMileageAndGasAt;
    }

    public function setLoggedMileageAndGasAt(?\DateTimeInterface $loggedMileageAndGasAt): self
    {
        $this->loggedMileageAndGasAt = $loggedMileageAndGasAt;

        return $this;
    }

    public function getVanSuppliesResult()
    {
        return $this->vanSuppliesResult;
    }

    public function setVanSuppliesResult($vanSuppliesResult): self
    {
        $this->vanSuppliesResult = $vanSuppliesResult;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getDriverRoute(): ?DriverRoute
    {
        return $this->driverRoute;
    }

    public function setDriverRoute(?DriverRoute $driverRoute): self
    {
        $this->driverRoute = $driverRoute;

        return $this;
    }

    public function getProcessCompleted(): ?bool
    {
        return $this->processCompleted;
    }

    public function setProcessCompleted(bool $processCompleted): self
    {
        $this->processCompleted = $processCompleted;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getStationArrivalAt(): ?\DateTimeInterface
    {
        return $this->stationArrivalAt;
    }

    public function setStationArrivalAt(?\DateTimeInterface $stationArrivalAt): self
    {
        $this->stationArrivalAt = $stationArrivalAt;

        return $this;
    }

    public function getScannedQRCode(): ?bool
    {
        return $this->scannedQRCode;
    }

    public function setScannedQRCode(bool $scannedQRCode): self
    {
        $this->scannedQRCode = $scannedQRCode;

        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getQrCodeValue(): ?string
    {
        return $this->qrCodeValue;
    }

    public function setQrCodeValue(?string $qrCodeValue): self
    {
        $this->qrCodeValue = $qrCodeValue;

        return $this;
    }

    public function getManualEntryVehicleIdentificationAt(): ?\DateTimeInterface
    {
        return $this->manualEntryVehicleIdentificationAt;
    }

    public function setManualEntryVehicleIdentificationAt(?\DateTimeInterface $manualEntryVehicleIdentificationAt): self
    {
        $this->manualEntryVehicleIdentificationAt = $manualEntryVehicleIdentificationAt;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeInterface
    {
        return $this->startedAt;
    }

    public function setStartedAt(?\DateTimeInterface $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getCompletedAt(): ?\DateTimeInterface
    {
        return $this->completedAt;
    }

    public function setCompletedAt(?\DateTimeInterface $completedAt): self
    {
        $this->completedAt = $completedAt;

        return $this;
    }

    public function getQrCodePictureUrl(): ?string
    {
        return $this->qrCodePictureUrl;
    }

    public function setQrCodePictureUrl(?string $qrCodePictureUrl): self
    {
        $this->qrCodePictureUrl = $qrCodePictureUrl;

        return $this;
    }

}
