<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DriverRouteRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class DriverRoute
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $routeCode;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateCreated;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $skill;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicle", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $vehicle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $punchIn;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $punchOut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $breakPunchOut;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $breakPunchIn;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $status;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRescuer = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRescued = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Shift", inversedBy="driverRoutes")
     * @ORM\JoinColumn(name="shift_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @MaxDepth(1)
     */
    private $shiftType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Schedule", inversedBy="driverRoutes")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $schedule;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="routeId")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetimeStarted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetimeEnded;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isActive = 1;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOpenShift = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isLoadOut = 0;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Driver", inversedBy="oldDriverRoutes")
     * @MaxDepth(1)
     */
    private $oldDriver;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DriverRoute", inversedBy="driverRoutes", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $oldDriverRoute;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="oldDriverRoute")
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\Column(type="boolean")
     * @MaxDepth(1)
     */
    private $isBackup = 0;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="oldDriverRoute")
     * @MaxDepth(1)
     */
    private $oldTempDriverRoutes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isTemp = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftColor;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $unpaidBreak;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $shiftNote;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\InvoiceType", inversedBy="driverRoutes")
     * @MaxDepth(1)
     */
    private $shiftInvoiceType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="driverShiftStation")
     * @MaxDepth(1)
     */
    private $shiftStation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill", inversedBy="driverShiftSkill")
     * @MaxDepth(1)
     */
    private $shiftSkill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $shiftCategory;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BalanceGroup", inversedBy="driverRoutes")
     * @MaxDepth(1)
     */
    private $shiftBalanceGroup;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $shiftTextColor;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\DriverRouteCode", inversedBy="driverRoutes")
     * @MaxDepth(1)
     */
    private $driverRouteCodes;

    /**
     * @ORM\Column(type="integer", length=255)
     */
    private $numberOfPackage = 0;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\KickoffLog", cascade={"persist", "remove"})
     */
    private $kickoffLog;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ReturnToStation", cascade={"persist", "remove"})
     */
    private $returnToStation;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Device", inversedBy="driverRoutes")
     */
    private $device;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $insideStationAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $insideParkingLotAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $outsideStationAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $outsideParkingLotAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insideStationLatitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insideStationLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insideParkingLotLatitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insideParkingLotLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outsideStationLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outsideStationLatitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outsideParkingLotLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $outsideParkingLotLatitude;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfDeliveredPackages;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="driverRoute")
     * @MaxDepth(1)
     */
    private $events;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfTotalPackages;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfReturningPackages;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfAttemptedPackages;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfTotalStops;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $completedCountOfStops;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countOfMissingPackages;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mobileVersionUsed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isReleaseShiftRequested = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isReleaseShiftRequestProcessCompleted = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isAddTrain = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isLightDuty = 0;

    /**
     * @ORM\ManyToOne(targetEntity=RouteRequests::class, inversedBy="driverRoutes")
     * @MaxDepth(1)
     */
    private $requestForRoute;

    /**
     * @ORM\OneToMany(targetEntity=RouteRequests::class, mappedBy="oldRoute")
     * @MaxDepth(1)
     */
    private $oldRouteRequest;

    /**
     * @ORM\OneToMany(targetEntity=RouteRequests::class, mappedBy="newRoute")
     * @MaxDepth(1)
     */
    private $newRouteRequest;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="driverRoute")
     */
    private $incidents;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $hasBag = 0;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default": 0})
     */
    private $isSecondShift = false;

    /**
     * @ORM\OneToMany(targetEntity=Location::class, mappedBy="driverRoute")
     */
    private $locations;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $currentNote;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $newNote;

    public function __construct()
    {
        $this->tempDriverRoutes = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->oldTempDriverRoutes = new ArrayCollection();
        $this->driverRouteCodes = new ArrayCollection();
        $this->device = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->oldRouteRequest = new ArrayCollection();
        $this->newRouteRequest = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->locations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getIsRequestCompleted()
    {
        if ($this->getRequestForRoute()) {
            if ($this->getRequestForRoute()->getIsRequestCompleted()) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getPunchIn(): ?\DateTimeInterface
    {
        return $this->punchIn;
    }

    public function setPunchIn(?\DateTimeInterface $punchIn): self
    {
        $this->punchIn = $punchIn;

        return $this;
    }

    // TODO: IS THIS OK?
    public function getPunchOut($dailySummary = null): ?\DateTimeInterface
    {
        if (is_null($dailySummary)) {
            $now = new \DateTime('now', new \DateTimeZone($this->station->getTimeZoneName()));
        } else {
            $now = new \DateTime('now');
        }

        $now->setTime(0, 0, 0);
        if (empty($this->punchOut) && $this->breakPunchIn && empty($this->breakPunchOut) && $this->breakPunchIn < $now)
            return $this->breakPunchIn;
        else
            return $this->punchOut;
    }

    public function getDBPunchOut(): ?\DateTimeInterface
    {
        return $this->punchOut;
    }

    public function setPunchOut(?\DateTimeInterface $punchOut): self
    {
        $this->punchOut = $punchOut;

        return $this;
    }

    public function getBreakPunchOut(): ?\DateTimeInterface
    {
        return $this->breakPunchOut;
    }

    public function setBreakPunchOut(?\DateTimeInterface $breakPunchOut): self
    {
        $this->breakPunchOut = $breakPunchOut;

        return $this;
    }

    public function getBreakPunchIn($dailySummary = null): ?\DateTimeInterface
    {
        if (is_null($dailySummary)) {
            $now = new \DateTime('now', new \DateTimeZone($this->station->getTimeZoneName()));
        } else {
            $now = new \DateTime('now');
        }
        $now->setTime(0, 0, 0);
        if (!$this->punchOut && $this->breakPunchIn && !$this->breakPunchOut && $this->breakPunchIn < $now)
            return null;

        return $this->breakPunchIn;
    }

    public function getDBBreakPunchIn(): ?\DateTimeInterface
    {
        return $this->breakPunchIn;
    }

    public function setBreakPunchIn(?\DateTimeInterface $breakPunchIn): self
    {
        $this->breakPunchIn = $breakPunchIn;

        return $this;
    }

    public function getRouteCode(): ?string
    {
        return $this->routeCode;
    }

    public function setRouteCode(?string $routeCode): self
    {
        $this->routeCode = $routeCode;

        return $this;
    }

    public function getSkill(): ?Skill
    {
        return $this->skill;
    }

    public function setSkill(?Skill $skill): self
    {
        $this->skill = $skill;

        return $this;
    }

    public function getVehicle(): ?Vehicle
    {
        return $this->vehicle;
    }

    public function setVehicle(?Vehicle $vehicle): self
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIsRescuer(): ?bool
    {
        return $this->isRescuer;
    }

    public function setIsRescuer(?bool $isRescuer): self
    {
        $this->isRescuer = $isRescuer;

        return $this;
    }

    public function getIsRescued(): ?bool
    {
        return $this->isRescued;
    }

    public function setIsRescued(?bool $isRescued): self
    {
        $this->isRescued = $isRescued;

        return $this;
    }

    public function getShiftType(): ?Shift
    {
        return $this->shiftType;
    }

    public function setShiftType(?Shift $shiftType): self
    {
        $this->shiftType = $shiftType;

        return $this;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setRouteId($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getRouteId() === $this) {
                $tempDriverRoute->setRouteId(null);
            }
        }

        return $this;
    }

    public function getDatetimeStarted(): ?\DateTimeInterface
    {
        return $this->datetimeStarted;
    }

    public function setDatetimeStarted(?\DateTimeInterface $datetimeStarted): self
    {
        $this->datetimeStarted = $datetimeStarted;

        return $this;
    }

    public function getDatetimeEnded(): ?\DateTimeInterface
    {
        return $this->datetimeEnded;
    }

    public function setDatetimeEnded(?\DateTimeInterface $datetimeEnded): self
    {
        $this->datetimeEnded = $datetimeEnded;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(?bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsOpenShift(): ?bool
    {
        return $this->isOpenShift;
    }

    public function setIsOpenShift(?bool $isOpenShift): self
    {
        $this->isOpenShift = $isOpenShift;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        if (!$this->startTime) {
            if ($this->getShiftType()) {
                return $this->getShiftType()->getStartTime();
            } else {
                return null;
            }
        }
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        if (!$this->endTime) {
            if ($this->datetimeEnded)
                return $this->datetimeEnded;

            if ($this->getShiftType()) {
                return $this->getShiftType()->getEndTime();
            } else {
                return null;
            }
        }
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getIsLoadOut(): ?bool
    {
        return $this->isLoadOut;
    }

    public function setIsLoadOut(bool $isLoadOut): self
    {
        $this->isLoadOut = $isLoadOut;

        return $this;
    }

    public function getOldDriver(): ?Driver
    {
        return $this->oldDriver;
    }

    public function setOldDriver(?Driver $oldDriver): self
    {
        $this->oldDriver = $oldDriver;

        return $this;
    }

    public function getOldDriverRoute(): ?self
    {
        return $this->oldDriverRoute;
    }

    public function setOldDriverRoute(?self $oldDriverRoute): self
    {
        $this->oldDriverRoute = $oldDriverRoute;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(self $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setOldDriverRoute($this);
        }

        return $this;
    }

    public function removeDriverRoute(self $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getOldDriverRoute() === $this) {
                $driverRoute->setOldDriverRoute(null);
            }
        }

        return $this;
    }

    public function getIsBackup(): ?bool
    {
        return $this->isBackup;
    }

    public function setIsBackup(?bool $isBackup): self
    {
        $this->isBackup = $isBackup;

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getOldTempDriverRoutes(): Collection
    {
        return $this->oldTempDriverRoutes;
    }

    public function addOldTempDriverRoute(TempDriverRoute $oldTempDriverRoute): self
    {
        if (!$this->oldTempDriverRoutes->contains($oldTempDriverRoute)) {
            $this->oldTempDriverRoutes[] = $oldTempDriverRoute;
            $oldTempDriverRoute->setOldDriverRoute($this);
        }

        return $this;
    }

    public function removeOldTempDriverRoute(TempDriverRoute $oldTempDriverRoute): self
    {
        if ($this->oldTempDriverRoutes->contains($oldTempDriverRoute)) {
            $this->oldTempDriverRoutes->removeElement($oldTempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($oldTempDriverRoute->getOldDriverRoute() === $this) {
                $oldTempDriverRoute->setOldDriverRoute(null);
            }
        }
        return $this;
    }

    public function getIsTemp(): ?bool
    {
        return $this->isTemp;
    }

    public function setIsTemp(?bool $isTemp): self
    {
        $this->isTemp = $isTemp;

        return $this;
    }

    public function getShiftName(): ?string
    {
        if (!$this->shiftName) {
            return $this->getShiftType()->getName();
        }
        return $this->shiftName;
    }

    public function setShiftName(?string $shiftName): self
    {
        $this->shiftName = $shiftName;

        return $this;
    }

    public function getShiftColor(): ?string
    {
        if (!$this->shiftColor) {
            return $this->getShiftType()->getColor();
        }
        return $this->shiftColor;
    }

    public function setShiftColor(?string $shiftColor): self
    {
        $this->shiftColor = $shiftColor;

        return $this;
    }

    public function getUnpaidBreak(): ?int
    {
        if (!$this->unpaidBreak) {
            return $this->getShiftType()->getUnpaidBreak();
        }
        return $this->unpaidBreak;
    }

    public function setUnpaidBreak(?int $unpaidBreak): self
    {
        $this->unpaidBreak = $unpaidBreak;

        return $this;
    }

    public function getShiftNote(): ?string
    {
        if (!empty(trim($this->shiftNote))) {
            return $this->shiftNote;
        }

        return $this->getShiftType()->getNote();
    }

    public function setShiftNote(?string $shiftNote): self
    {
        $this->shiftNote = $shiftNote;

        return $this;
    }

    public function getShiftInvoiceType(): ?InvoiceType
    {
        return $this->shiftInvoiceType;
    }

    public function setShiftInvoiceType(?InvoiceType $shiftInvoiceType): self
    {
        $this->shiftInvoiceType = $shiftInvoiceType;

        return $this;
    }

    public function getShiftStation(): ?Station
    {
        if (!$this->shiftStation) {
            return $this->getShiftType()->getStation();
        }
        return $this->shiftStation;
    }

    public function setShiftStation(?Station $shiftStation): self
    {
        $this->shiftStation = $shiftStation;

        return $this;
    }

    public function getShiftSkill(): ?Skill
    {
        if (!$this->shiftSkill) {
            return $this->getShiftType()->getSkill();
        }
        return $this->shiftSkill;
    }

    public function setShiftSkill(?Skill $shiftSkill): self
    {
        $this->shiftSkill = $shiftSkill;

        return $this;
    }

    public function getShiftCategory(): ?int
    {
        if (!$this->shiftCategory) {
            return $this->getShiftType()->getCategory();
        }
        return $this->shiftCategory;
    }

    public function setShiftCategory(?int $shiftCategory): self
    {
        $this->shiftCategory = $shiftCategory;

        return $this;
    }

    public function getShiftBalanceGroup(): ?BalanceGroup
    {
        return $this->shiftBalanceGroup;
    }

    public function setShiftBalanceGroup(?BalanceGroup $shiftBalanceGroup): self
    {
        $this->shiftBalanceGroup = $shiftBalanceGroup;

        return $this;
    }

    public function getShiftTextColor(): ?string
    {
        if (!$this->shiftTextColor) {
            return $this->getShiftType()->getTextColor();
        }
        return $this->shiftTextColor;
    }

    public function setShiftTextColor(?string $shiftTextColor): self
    {
        $this->shiftTextColor = $shiftTextColor;

        return $this;
    }

    /**
     * @return Collection|DriverRouteCode[]
     */
    public function getDriverRouteCodes(): Collection
    {
        return $this->driverRouteCodes;
    }

    public function addDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if (!$this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes[] = $driverRouteCode;
        }

        return $this;
    }

    public function removeDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if ($this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes->removeElement($driverRouteCode);
        }

        return $this;
    }

    public function getNumberOfPackage(): ?string
    {
        return $this->numberOfPackage;
    }

    public function setNumberOfPackage(?string $numberOfPackage): self
    {
        $this->numberOfPackage = $numberOfPackage;

        return $this;
    }

    public function getKickoffLog(): ?KickoffLog
    {
        return $this->kickoffLog;
    }

    public function setKickoffLog(?KickoffLog $kickoffLog): self
    {
        $this->kickoffLog = $kickoffLog;

        return $this;
    }

    public function getReturnToStation(): ?ReturnToStation
    {
        return $this->returnToStation;
    }

    public function setReturnToStation(?ReturnToStation $returnToStation): self
    {
        $this->returnToStation = $returnToStation;

        // set (or unset) the owning side of the relation if necessary
        $newDriverRoute = null === $returnToStation ? null : $this;
        if ($returnToStation->getDriverRoute() !== $newDriverRoute) {
            $returnToStation->setDriverRoute($newDriverRoute);
        }

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevice(): Collection
    {
        return $this->device;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->device->contains($device)) {
            $this->device[] = $device;
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->device->contains($device)) {
            $this->device->removeElement($device);
        }

        return $this;
    }

    public function getInsideStationAt(): ?\DateTimeInterface
    {
        return $this->insideStationAt;
    }

    public function setInsideStationAt(?\DateTimeInterface $insideStationAt): self
    {
        $this->insideStationAt = $insideStationAt;

        return $this;
    }

    public function getInsideParkingLotAt(): ?\DateTimeInterface
    {
        return $this->insideParkingLotAt;
    }

    public function setInsideParkingLotAt(?\DateTimeInterface $insideParkingLotAt): self
    {
        $this->insideParkingLotAt = $insideParkingLotAt;

        return $this;
    }

    public function getOutsideStationAt(): ?\DateTimeInterface
    {
        return $this->outsideStationAt;
    }

    public function setOutsideStationAt(?\DateTimeInterface $outsideStationAt): self
    {
        $this->outsideStationAt = $outsideStationAt;

        return $this;
    }

    public function getOutsideParkingLotAt(): ?\DateTimeInterface
    {
        return $this->outsideParkingLotAt;
    }

    public function setOutsideParkingLotAt(?\DateTimeInterface $outsideParkingLotAt): self
    {
        $this->outsideParkingLotAt = $outsideParkingLotAt;

        return $this;
    }

    public function getInsideStationLatitude(): ?string
    {
        return $this->insideStationLatitude;
    }

    public function setInsideStationLatitude(?string $insideStationLatitude): self
    {
        $this->insideStationLatitude = $insideStationLatitude;

        return $this;
    }

    public function getOutsideStationLongitude(): ?string
    {
        return $this->outsideStationLongitude;
    }

    public function setOutsideStationLongitude(?string $outsideStationLongitude): self
    {
        $this->outsideStationLongitude = $outsideStationLongitude;

        return $this;
    }

    public function getInsideParkingLotLatitude(): ?string
    {
        return $this->insideParkingLotLatitude;
    }

    public function setInsideParkingLotLatitude(?string $insideParkingLotLatitude): self
    {
        $this->insideParkingLotLatitude = $insideParkingLotLatitude;

        return $this;
    }

    public function getOutsideParkingLotLongitude(): ?string
    {
        return $this->outsideParkingLotLongitude;
    }

    public function setOutsideParkingLotLongitude(?string $outsideParkingLotLongitude): self
    {
        $this->outsideParkingLotLongitude = $outsideParkingLotLongitude;

        return $this;
    }

    public function getInsideStationLongitude(): ?string
    {
        return $this->insideStationLongitude;
    }

    public function setInsideStationLongitude(?string $insideStationLongitude): self
    {
        $this->insideStationLongitude = $insideStationLongitude;

        return $this;
    }

    public function getOutsideStationLatitude(): ?string
    {
        return $this->outsideStationLatitude;
    }

    public function setOutsideStationLatitude(?string $outsideStationLatitude): self
    {
        $this->outsideStationLatitude = $outsideStationLatitude;

        return $this;
    }

    public function getInsideParkingLotLongitude(): ?string
    {
        return $this->insideParkingLotLongitude;
    }

    public function setInsideParkingLotLongitude(?string $insideParkingLotLongitude): self
    {
        $this->insideParkingLotLongitude = $insideParkingLotLongitude;

        return $this;
    }

    public function getOutsideParkingLotLatitude(): ?string
    {
        return $this->outsideParkingLotLatitude;
    }

    public function setOutsideParkingLotLatitude(?string $outsideParkingLotLatitude): self
    {
        $this->outsideParkingLotLatitude = $outsideParkingLotLatitude;

        return $this;
    }

    public function getCountOfDeliveredPackages(): ?int
    {
        return $this->countOfDeliveredPackages;
    }

    public function setCountOfDeliveredPackages(?int $countOfDeliveredPackages): self
    {
        $this->countOfDeliveredPackages = $countOfDeliveredPackages;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setDriverRoute($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getDriverRoute() === $this) {
                $event->setDriverRoute(null);
            }
        }
        return $this;
    }

    public function getCountOfTotalPackages(): ?int
    {
        return $this->countOfTotalPackages;
    }

    public function setCountOfTotalPackages(?int $countOfTotalPackages): self
    {
        $this->countOfTotalPackages = $countOfTotalPackages;

        return $this;
    }

    public function getCountOfReturningPackages(): ?int
    {
        return $this->countOfReturningPackages;
    }

    public function setCountOfReturningPackages(?int $countOfReturningPackages): self
    {
        $this->countOfReturningPackages = $countOfReturningPackages;

        return $this;
    }

    public function getCountOfAttemptedPackages(): ?int
    {
        return $this->countOfAttemptedPackages;
    }

    public function setCountOfAttemptedPackages(int $countOfAttemptedPackages): self
    {
        $this->countOfAttemptedPackages = $countOfAttemptedPackages;

        return $this;
    }

    public function getCountOfTotalStops(): ?int
    {
        return $this->countOfTotalStops;
    }

    public function setCountOfTotalStops(?int $countOfTotalStops): self
    {
        $this->countOfTotalStops = $countOfTotalStops;

        return $this;
    }

    public function getCompletedCountOfStops(): ?int
    {
        return $this->completedCountOfStops;
    }

    public function setCompletedCountOfStops(?int $completedCountOfStops): self
    {
        $this->completedCountOfStops = $completedCountOfStops;

        return $this;
    }

    public function getCountOfMissingPackages(): ?int
    {
        return $this->countOfMissingPackages;
    }

    public function setCountOfMissingPackages(?int $countOfMissingPackages): self
    {
        $this->countOfMissingPackages = $countOfMissingPackages;

        return $this;
    }

    public function getMobileVersionUsed(): ?string
    {
        return $this->mobileVersionUsed;
    }

    public function setMobileVersionUsed(?string $mobileVersionUsed): self
    {
        $this->mobileVersionUsed = $mobileVersionUsed;

        return $this;
    }

    public function getIsReleaseShiftRequested(): ?bool
    {
        return $this->isReleaseShiftRequested;
    }

    public function setIsReleaseShiftRequested(?bool $isReleaseShiftRequested): self
    {
        $this->isReleaseShiftRequested = $isReleaseShiftRequested;

        return $this;
    }

    public function getIsAddTrain(): ?bool
    {
        return $this->isAddTrain;
    }

    public function setIsAddTrain(bool $isAddTrain): self
    {
        $this->isAddTrain = $isAddTrain;

        return $this;
    }

    public function getIsLightDuty(): ?bool
    {
        return $this->isLightDuty;
    }

    public function setIsLightDuty(bool $isLightDuty): self
    {
        $this->isLightDuty = $isLightDuty;

        return $this;
    }

    public function getRequestForRoute(): ?RouteRequests
    {
        return $this->requestForRoute;
    }

    public function setRequestForRoute(?RouteRequests $requestForRoute): self
    {
        $this->requestForRoute = $requestForRoute;

        return $this;
    }

    /**
     * @return Collection|RouteRequests[]
     */
    public function getOldRouteRequest(): Collection
    {
        return $this->oldRouteRequest;
    }

    public function addOldRouteRequest(RouteRequests $oldRouteRequest): self
    {
        if (!$this->oldRouteRequest->contains($oldRouteRequest)) {
            $this->oldRouteRequest[] = $oldRouteRequest;
            $oldRouteRequest->setOldRoute($this);
        }

        return $this;
    }

    public function removeOldRouteRequest(RouteRequests $oldRouteRequest): self
    {
        if ($this->oldRouteRequest->contains($oldRouteRequest)) {
            $this->oldRouteRequest->removeElement($oldRouteRequest);
            // set the owning side to null (unless already changed)
            if ($oldRouteRequest->getOldRoute() === $this) {
                $oldRouteRequest->setOldRoute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RouteRequests[]
     */
    public function getNewRouteRequest(): Collection
    {
        return $this->newRouteRequest;
    }

    public function addNewRouteRequest(RouteRequests $newRouteRequest): self
    {
        if (!$this->newRouteRequest->contains($newRouteRequest)) {
            $this->newRouteRequest[] = $newRouteRequest;
            $newRouteRequest->setNewRoute($this);
        }

        return $this;
    }

    public function removeNewRouteRequest(RouteRequests $newRouteRequest): self
    {
        if ($this->newRouteRequest->contains($newRouteRequest)) {
            $this->newRouteRequest->removeElement($newRouteRequest);
            // set the owning side to null (unless already changed)
            if ($newRouteRequest->getNewRoute() === $this) {
                $newRouteRequest->setNewRoute(null);
            }
        }

        return $this;
    }

    public function getIsReleaseShiftRequestProcessCompleted(): ?bool
    {
        return $this->isReleaseShiftRequestProcessCompleted;
    }

    public function setIsReleaseShiftRequestProcessCompleted(bool $isReleaseShiftRequestProcessCompleted): self
    {
        $this->isReleaseShiftRequestProcessCompleted = $isReleaseShiftRequestProcessCompleted;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setDriverRoute($this);
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getDriverRoute() === $this) {
                $incident->setDriverRoute(null);
            }
        }

        return $this;
    }

    public function getHasBag(): ?bool
    {
        return $this->hasBag;
    }

    public function setHasBag(bool $hasBag): self
    {
        $this->hasBag = $hasBag;

        return $this;
    }

    public function getIsSecondShift(): ?bool
    {
        return $this->isSecondShift;
    }

    public function setIsSecondShift(?bool $isSecondShift): self
    {
        $this->isSecondShift = $isSecondShift;

        return $this;
    }

    /**
     * @return Collection|Location[]
     */
    public function getLocations(): Collection
    {
        return $this->locations;
    }

    public function addLocation(Location $location): self
    {
        if (!$this->locations->contains($location)) {
            $this->locations[] = $location;
            $location->setDriverRoute($this);
        }

        return $this;
    }

    public function removeLocation(Location $location): self
    {
        if ($this->locations->contains($location)) {
            $this->locations->removeElement($location);
            // set the owning side to null (unless already changed)
            if ($location->getDriverRoute() === $this) {
                $location->setDriverRoute(null);
            }
        }

        return $this;
    }

    public function getIsSentHome(): bool
    {
        $events = $this->events->filter(function (Event $item) {
            return $item->getIsActive() === true;
        });

        return $events->filter(function (Event $event) {
            return $event->getMessage() === 'SEND_HOME';
        })->count() > $events->filter(function (Event $event) {
            return $event->getMessage() === 'UNSEND_HOME';
        })->count();
    }

    public function getCurrentNote(): ?string
    {
        return $this->currentNote;
    }

    public function setCurrentNote(?string $currentNote): self
    {
        $this->currentNote = $currentNote;

        return $this;
    }

    public function getNewNote(): ?string
    {
        return $this->newNote;
    }

    public function setNewNote(?string $newNote): self
    {
        $this->newNote = $newNote;

        return $this;
    }
}
