<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentFormFieldValueRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentFormFieldValue
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncidentFormFields", inversedBy="incidentFormFieldValues")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $incidentFormField;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Incident", inversedBy="incidentFormFieldValues")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $incident;

    /**
     * @ORM\Column(type="array")
     */
    private $value = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentFile", mappedBy="incidentFormFieldValue", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentFiles;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    public function __construct()
    {
        $this->incidentFiles = new ArrayCollection();
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIncidentFormField(): ?IncidentFormFields
    {
        return $this->incidentFormField;
    }

    public function setIncidentFormField(?IncidentFormFields $incidentFormField): self
    {
        $this->incidentFormField = $incidentFormField;

        return $this;
    }

    public function getIncident(): ?Incident
    {
        return $this->incident;
    }

    public function setIncident(?Incident $incident): self
    {
        $this->incident = $incident;

        return $this;
    }

    public function getValue(): ?array
    {
        return $this->value;
    }

    public function setValue(array $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return Collection|IncidentFile[]
     */
    public function getIncidentFiles(): Collection
    {
        return $this->incidentFiles;
    }

    public function addIncidentFile(IncidentFile $incidentFile): self
    {
        if (!$this->incidentFiles->contains($incidentFile)) {
            $this->incidentFiles[] = $incidentFile;
            $incidentFile->setIncidentFormFieldValue($this);
        }

        return $this;
    }

    public function removeIncidentFile(IncidentFile $incidentFile): self
    {
        if ($this->incidentFiles->contains($incidentFile)) {
            $this->incidentFiles->removeElement($incidentFile);
            // set the owning side to null (unless already changed)
            if ($incidentFile->getIncidentFormFieldValue() === $this) {
                $incidentFile->setIncidentFormFieldValue(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
