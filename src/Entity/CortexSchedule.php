<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CortexScheduleRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CortexSchedule
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Station", inversedBy="cortexSchedules")
     * @MaxDepth(1)
     */
    private $station;

    /**
     * @ORM\Column(type="json")
     */
    private $routeSummeries = [];

    /**
     * @ORM\Column(type="json")
     */
    private $transportersList = [];

    /**
     * @ORM\Column(type="json")
     */
    private $transporterPackageSummaries = [];

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getRouteSummeries(): ?array
    {
        return $this->routeSummeries;
    }

    public function setRouteSummeries(array $routeSummeries): self
    {
        $this->routeSummeries = $routeSummeries;

        return $this;
    }

    public function getTransportersList(): ?array
    {
        return $this->transportersList;
    }

    public function setTransportersList(array $transportersList): self
    {
        $this->transportersList = $transportersList;

        return $this;
    }

    public function getTransporterPackageSummaries(): ?array
    {
        return $this->transporterPackageSummaries;
    }

    public function setTransporterPackageSummaries(array $transporterPackageSummaries): self
    {
        $this->transporterPackageSummaries = $transporterPackageSummaries;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }
}
