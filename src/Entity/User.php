<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;


    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Skill", mappedBy="users")
     * @MaxDepth(1)
     */
    private $skills;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isHidden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEnabled = true;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Driver", mappedBy="user", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $driver;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Driver", mappedBy="assignedManagers", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $managerDrivers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="users")
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $middleName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $userSid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $friendlyName;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOwner;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role", inversedBy="users")
     * @MaxDepth(1)
     */
    private $userRoles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="createdBy")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $profileImage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $chatIdentifier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $notificationsChannelSid;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="manager")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity=IncidentSubmitedPhase::class, mappedBy="submitedBy")
     */
    private $incidentSubmitedPhases;

    /**
     * @ORM\OneToMany(targetEntity=Event::class, mappedBy="user")
     * @MaxDepth(1)
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity=DailyDriverSummary::class, mappedBy="createdBy")
     */
    private $dailyDriverSummaries;

    /**
     * @ORM\ManyToMany(targetEntity=DailyDriverSummary::class, inversedBy="users")
     */
    private $dailyDriverSummariesUsers;

    /**
     * @ORM\OneToMany(targetEntity=DailyDriverSummary::class, mappedBy="modifiedBy")
     */
    private $dailyDriverSummariesModifieds;

    /**
     * @ORM\OneToMany(targetEntity=RouteRequests::class, mappedBy="approvedBy")
     * @MaxDepth(1)
     */
    private $routeRequests;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isResetPassword = true;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="createdBy")
     */
    private $createdByIncidents;

    /**
     * @ORM\OneToMany(targetEntity=IncidentSubmitedPhase::class, mappedBy="reviewedBy")
     */
    private $incidentReviewedPhases;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mainManagerChannelSid;

    /**
     * @ORM\ManyToMany(targetEntity=Task::class, mappedBy="assignees")
     */
    private $tasks;

    public function __construct()
    {
        $this->userRoles = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->managerDrivers = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->incidentSubmitedPhases = new ArrayCollection();
        $this->dailyDriverSummaries = new ArrayCollection();
        $this->dailyDriverSummariesUsers = new ArrayCollection();
        $this->dailyDriverSummariesModifieds = new ArrayCollection();
        $this->routeRequests = new ArrayCollection();
        $this->createdByIncidents = new ArrayCollection();
        $this->incidentReviewedPhases = new ArrayCollection();
        $this->tasks = new ArrayCollection();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }


    /**
     * @inheritDoc
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * @inheritDoc
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @inheritDoc
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifier(): string
    {
        return strval($this->id);
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getIsHidden(): ?bool
    {
        return $this->isHidden;
    }

    public function setIsHidden(?bool $isHidden): self
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(?bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    public function setPhoneNumber(?string $phoneNumber): self
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;
        $this->setFriendlyName($firstName . ' ' . $this->getLastName());

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        $this->setFriendlyName($this->getFirstName() . ' ' . $lastName);

        return $this;
    }

    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    public function setMiddleName(?string $middleName): self
    {
        $this->middleName = $middleName;

        return $this;
    }

    public function getMainPosition(): Role
    {

        if ($this->userRoles->isEmpty()) {
            $userRole = new Role();
            if ($this->isOwner === true) {
                $userRole->setName('CEO');
                return $userRole;
            }
            $userRole->setName('Driver');
            return $userRole;
        }

        return $this->userRoles->first();
    }

    /**
     * @return Collection|Skill[]
     */
    public function getSkills(): Collection
    {
        return $this->skills;
    }

    public function addSkill(Skill $skill): self
    {
        if (!$this->skills->contains($skill)) {
            $this->skills[] = $skill;
            $skill->addUser($this);
        }

        return $this;
    }

    public function removeSkill(Skill $skill): self
    {
        if ($this->skills->contains($skill)) {
            $this->skills->removeElement($skill);
            $skill->removeUser($this);
        }

        return $this;
    }


    public function getDriver(): ?Driver
    {
        return $this->driver;
    }

    public function setDriver(?Driver $driver): self
    {
        $this->driver = $driver;

        // set (or unset) the owning side of the relation if necessary
        $newUser = null === $driver ? null : $this;
        if ($driver->getUser() !== $newUser) {
            $driver->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return Collection|Driver[]
     */
    public function getManagerDrivers(): Collection
    {
        return $this->managerDrivers;
    }

    public function addManagerDriver(Driver $managerDriver): self
    {
        if (!$this->managerDrivers->contains($managerDriver)) {
            $this->managerDrivers[] = $managerDriver;
            $managerDriver->addAssignedManager($this);
        }

        return $this;
    }

    public function removeManagerDriver(Driver $managerDriver): self
    {
        if ($this->managerDrivers->contains($managerDriver)) {
            $this->managerDrivers->removeElement($managerDriver);
            $managerDriver->removeAssignedManager($this);
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getUserSid(): ?string
    {
        return $this->userSid;
    }

    public function setUserSid(?string $userSid): self
    {
        $this->userSid = $userSid;

        return $this;
    }

    public function getFriendlyName(): string
    {
        if (is_string($this->friendlyName)) {
            return $this->friendlyName;
        }
        if (is_string($this->getFirstName() . ' ' . $this->getLastName())) {
            return $this->getFirstName() . ' ' . $this->getLastName();
        }
        if (is_string($this->getFirstName())) {
            return $this->getFirstName();
        }
        if (is_string($this->getLastName())) {
            return $this->getFirstName();
        }
        return $this->getIdentifier();
    }

    public function getRoleForChat(): string
    {
        if ($this->userRoles->isEmpty()) {
            $userRole = new Role();
            if ($this->isOwner === true) {
                $userRole->setName('ROLE_OWNER');
                return $userRole->getRoleName();
            }
            $userRole->setName('ROLE_DELIVERY_ASSOCIATE');
            return $userRole->getRoleName();
        }

        /** @var Role $role */
        $role = $this->userRoles->first();
        if ($role instanceof Role && $role->getRoleName() !== null) {
            return $role->getRoleName();
        }

        return 'Delivery Associate';
    }

    public function getRoleName(): string
    {
        if ($this->userRoles->isEmpty()) {
            if ($this->isOwner === true) {
                return 'Owner';
            }
        }

        /** @var Role $role */
        $role = $this->userRoles->first();
        if ($role instanceof Role) {
            return $role->getRoleName();
        }

        return 'Delivery Associate';
    }

    public function getRoleRawName(): string
    {
        if ($this->userRoles->isEmpty()) {
            if ($this->isOwner === true) {
                return 'ROLE_OWNER';
            }
        }

        /** @var Role $role */
        $role = $this->userRoles->first();
        if ($role instanceof Role) {
            return $role->getName();
        }

        return 'ROLE_DELIVERY_ASSOCIATE';
    }

    public function setFriendlyName(?string $friendlyName): self
    {
        $this->friendlyName = $friendlyName;

        return $this;
    }

    public function getIsOwner(): ?bool
    {
        return $this->isOwner;
    }

    public function setIsOwner(?bool $isOwner): self
    {
        $this->isOwner = $isOwner;

        return $this;
    }

    /**
     * @return Collection|Role[]
     */
    public function getUserRoles(): Collection
    {
        return $this->userRoles;
    }

    public function getUserFirstRole()
    {
        $role = $this->userRoles->first();
        if ($role instanceof Role) {
            return $role->getName();
        }
        return 'ROLE_DELIVERY_ASSOCIATE';
    }

    public function addUserRole(Role $userRole): self
    {
        if (!$this->userRoles->contains($userRole)) {
            $this->userRoles[] = $userRole;
        }

        return $this;
    }

    public function removeUserRole(Role $userRole): self
    {
        if ($this->userRoles->contains($userRole)) {
            $this->userRoles->removeElement($userRole);
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setCreatedBy($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getCreatedBy() === $this) {
                $tempDriverRoute->setCreatedBy(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidents(): Collection
    {
        return $this->incidents;
    }

    public function addIncident(Incident $incident): self
    {
        if (!$this->incidents->contains($incident)) {
            $this->incidents[] = $incident;
            $incident->setManager($this);
        }

        return $this;
    }

    public function getProfileImage(): ?string
    {
        return $this->profileImage;
    }

    public function setProfileImage(?string $profileImage): self
    {
        $this->profileImage = $profileImage;

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getManager() === $this) {
                $incident->setManager(null);
            }
        }

        return $this;
    }

    public function getChatIdentifier(): ?string
    {
        return $this->chatIdentifier;
    }

    public function setChatIdentifier(?string $chatIdentifier): self
    {
        $this->chatIdentifier = $chatIdentifier;

        return $this;
    }

    public function getNotificationsChannelSid(): ?string
    {
        return $this->notificationsChannelSid;
    }

    public function setNotificationsChannelSid(?string $notificationsChannelSid): self
    {
        $this->notificationsChannelSid = $notificationsChannelSid;

        return $this;
    }

    /**
     * @return Collection|IncidentSubmitedPhase[]
     */
    public function getIncidentSubmitedPhases(): Collection
    {
        return $this->incidentSubmitedPhases;
    }

    public function addIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if (!$this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases[] = $incidentSubmitedPhase;
            $incidentSubmitedPhase->setSubmitedBy($this);
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setUser($this);
        }

        return $this;
    }

    public function removeIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if ($this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases->removeElement($incidentSubmitedPhase);
            // set the owning side to null (unless already changed)
            if ($incidentSubmitedPhase->getSubmitedBy() === $this) {
                $incidentSubmitedPhase->setSubmitedBy(null);
            }
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getUser() === $this) {
                $event->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DailyDriverSummary[]
     */
    public function getDailyDriverSummaries(): Collection
    {
        return $this->dailyDriverSummaries;
    }

    public function addDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
    {
        if (!$this->dailyDriverSummaries->contains($dailyDriverSummary)) {
            $this->dailyDriverSummaries[] = $dailyDriverSummary;
            $dailyDriverSummary->setCreatedBy($this);
        }

        return $this;
    }

    public function removeDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
    {
        if ($this->dailyDriverSummaries->contains($dailyDriverSummary)) {
            $this->dailyDriverSummaries->removeElement($dailyDriverSummary);
            // set the owning side to null (unless already changed)
            if ($dailyDriverSummary->getCreatedBy() === $this) {
                $dailyDriverSummary->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DailyDriverSummary[]
     */
    public function getDailyDriverSummariesUsers(): Collection
    {
        return $this->dailyDriverSummariesUsers;
    }

    public function addDailyDriverSummariesUser(DailyDriverSummary $dailyDriverSummariesUser): self
    {
        if (!$this->dailyDriverSummariesUsers->contains($dailyDriverSummariesUser)) {
            $this->dailyDriverSummariesUsers[] = $dailyDriverSummariesUser;
        }

        return $this;
    }

    public function removeDailyDriverSummariesUser(DailyDriverSummary $dailyDriverSummariesUser): self
    {
        if ($this->dailyDriverSummariesUsers->contains($dailyDriverSummariesUser)) {
            $this->dailyDriverSummariesUsers->removeElement($dailyDriverSummariesUser);
        }

        return $this;
    }

    /**
     * @return Collection|DailyDriverSummary[]
     */
    public function getDailyDriverSummariesModifieds(): Collection
    {
        return $this->dailyDriverSummariesModifieds;
    }

    public function addDailyDriverSummariesModified(DailyDriverSummary $dailyDriverSummariesModified): self
    {
        if (!$this->dailyDriverSummariesModifieds->contains($dailyDriverSummariesModified)) {
            $this->dailyDriverSummariesModifieds[] = $dailyDriverSummariesModified;
            $dailyDriverSummariesModified->setModifiedBy($this);
        }

        return $this;
    }

    public function removeDailyDriverSummariesModified(DailyDriverSummary $dailyDriverSummariesModified): self
    {
        if ($this->dailyDriverSummariesModifieds->contains($dailyDriverSummariesModified)) {
            $this->dailyDriverSummariesModifieds->removeElement($dailyDriverSummariesModified);
            // set the owning side to null (unless already changed)
            if ($dailyDriverSummariesModified->getModifiedBy() === $this) {
                $dailyDriverSummariesModified->setModifiedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RouteRequests[]
     */
    public function getRouteRequests(): Collection
    {
        return $this->routeRequests;
    }

    public function addRouteRequest(RouteRequests $routeRequest): self
    {
        if (!$this->routeRequests->contains($routeRequest)) {
            $this->routeRequests[] = $routeRequest;
            $routeRequest->setApprovedBy($this);
        }

        return $this;
    }

    public function removeRouteRequest(RouteRequests $routeRequest): self
    {
        if ($this->routeRequests->contains($routeRequest)) {
            $this->routeRequests->removeElement($routeRequest);
            // set the owning side to null (unless already changed)
            if ($routeRequest->getApprovedBy() === $this) {
                $routeRequest->setApprovedBy(null);
            }
        }

        return $this;
    }

    public function getIsResetPassword(): ?bool
    {
        return $this->isResetPassword;
    }

    public function setIsResetPassword(bool $isResetPassword): self
    {
        $this->isResetPassword = $isResetPassword;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getCreatedByIncidents(): Collection
    {
        return $this->createdByIncidents;
    }

    public function addCreatedByIncident(Incident $createdByIncident): self
    {
        if (!$this->createdByIncidents->contains($createdByIncident)) {
            $this->createdByIncidents[] = $createdByIncident;
            $createdByIncident->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedByIncident(Incident $createdByIncident): self
    {
        if ($this->createdByIncidents->contains($createdByIncident)) {
            $this->createdByIncidents->removeElement($createdByIncident);
            // set the owning side to null (unless already changed)
            if ($createdByIncident->getCreatedBy() === $this) {
                $createdByIncident->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|IncidentSubmitedPhase[]
     */
    public function getIncidentReviewedPhases(): Collection
    {
        return $this->incidentReviewedPhases;
    }

    public function addIncidentReviewedPhase(IncidentSubmitedPhase $incidentReviewedPhase): self
    {
        if (!$this->incidentReviewedPhases->contains($incidentReviewedPhase)) {
            $this->incidentReviewedPhases[] = $incidentReviewedPhase;
            $incidentReviewedPhase->setReviewedBy($this);
        }

        return $this;
    }

    public function removeIncidentReviewedPhase(IncidentSubmitedPhase $incidentReviewedPhase): self
    {
        if ($this->incidentReviewedPhases->contains($incidentReviewedPhase)) {
            $this->incidentReviewedPhases->removeElement($incidentReviewedPhase);
            // set the owning side to null (unless already changed)
            if ($incidentReviewedPhase->getReviewedBy() === $this) {
                $incidentReviewedPhase->setReviewedBy(null);
            }
        }

        return $this;
    }

    public function getMainManagerChannelSid(): ?string
    {
        return $this->mainManagerChannelSid;
    }

    public function setMainManagerChannelSid(?string $mainManagerChannelSid): self
    {
        $this->mainManagerChannelSid = $mainManagerChannelSid;

        return $this;
    }


    public function getMainManger(): ?User
    {
        $driver = $this->getDriver();
        if ($driver instanceof Driver) {
            return $driver->getMainManager();
        }
        return null;
    }


    /**
     * @return Station
     * @throws Exception
     */
    public function getMainStation(): Station
    {
        $driver = $this->getDriver();
        if ($driver instanceof Driver) {
            return $driver->getMainStation();
        }
        throw new Exception('Station not found with User ID: ' . $this->getId());
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }

    public function addTask(Task $task): self
    {
        if (!$this->tasks->contains($task)) {
            $this->tasks[] = $task;
            $task->addAssignee($this);
        }

        return $this;
    }

    public function removeTask(Task $task): self
    {
        if ($this->tasks->contains($task)) {
            $this->tasks->removeElement($task);
            $task->removeAssignee($this);
        }

        return $this;
    }
}
