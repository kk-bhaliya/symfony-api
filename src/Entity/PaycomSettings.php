<?php

namespace App\Entity;

use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PaycomSettingsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PaycomSettings
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Company", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clientCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Encrypted
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $pin1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $pin2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $pin3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $pin4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Encrypted
     */
    private $pin5;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $isEnabled;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $dateCreated;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"}, columnDefinition="DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
     */
    private $dateUpdated;

    /**
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $isArchive;

    public function __construct()
    {
        $this->setIsEnabled(1);
        $this->setIsArchive(0);
        $this->setDateCreated((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
        $this->setDateUpdated((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getClientCode(): ?string
    {
        return $this->clientCode;
    }

    public function setClientCode(string $clientCode): self
    {
        $this->clientCode = $clientCode;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPin1(): ?string
    {
        return $this->pin1;
    }

    public function setPin1(?string $pin1): self
    {
        $this->pin1 = $pin1;

        return $this;
    }

    public function getPin2(): ?string
    {
        return $this->pin2;
    }

    public function setPin2(?string $pin2): self
    {
        $this->pin2 = $pin2;

        return $this;
    }

    public function getPin3(): ?string
    {
        return $this->pin3;
    }

    public function setPin3(?string $pin3): self
    {
        $this->pin3 = $pin3;

        return $this;
    }

    public function getPin4(): ?string
    {
        return $this->pin4;
    }

    public function setPin4(?string $pin4): self
    {
        $this->pin4 = $pin4;

        return $this;
    }

    public function getPin5(): ?string
    {
        return $this->pin5;
    }

    public function setPin5(?string $pin5): self
    {
        $this->pin5 = $pin5;

        return $this;
    }

    public function getIsEnabled(): ?bool
    {
        return $this->isEnabled;
    }

    public function setIsEnabled(bool $isEnabled): self
    {
        $this->isEnabled = $isEnabled;

        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(\DateTimeInterface $dateCreated): self
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    public function getDateUpdated(): ?\DateTimeInterface
    {
        return $this->dateUpdated;
    }

    public function setDateUpdated(\DateTimeInterface $dateUpdated): self
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }
}
