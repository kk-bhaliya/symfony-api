<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Image
{
    use Timestamps;
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentFile", mappedBy="image")
     * @MaxDepth(1)
     */
    private $incidentFiles;

    public function __construct()
    {
       $this->incidentFiles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|IncidentFile[]
     */
    public function getIncidentFiles(): Collection
    {
        return $this->incidentFiles;
    }

    public function addIncidentFile(IncidentFile $incidentFile): self
    {
        if (!$this->incidentFiles->contains($incidentFile)) {
            $this->incidentFiles[] = $incidentFile;
            $incidentFile->setImage($this);
        }

        return $this;
    }

    public function removeIncidentFile(IncidentFile $incidentFile): self
    {
        if ($this->incidentFiles->contains($incidentFile)) {
            $this->incidentFiles->removeElement($incidentFile);
            // set the owning side to null (unless already changed)
            if ($incidentFile->getImage() === $this) {
                $incidentFile->setImage(null);
            }
        }

        return $this;
    }

}
