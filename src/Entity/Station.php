<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Station
{

    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Driver", mappedBy="stations")
     * @MaxDepth(1)
     */
    private $drivers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="station", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $driverRoutes;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stationAddress;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stationLatitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stationLongitude;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stationGeofence;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parkingLotAddress;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parkingLotLatitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parkingLotLongitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $parkingLotGeofence;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Company", inversedBy="stations")
     * @ORM\JoinColumn(nullable=false)
     * @MaxDepth(1)
     */
    private $company;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InvoiceType", mappedBy="station", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $invoiceTypes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Shift", mappedBy="station", orphanRemoval=true)
     * @MaxDepth(1)
     */
    private $shifts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SkillRate", mappedBy="station", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $skillRates;

    /**
     * @ORM\OneToMany(targetEntity="Rate", mappedBy="station", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $rates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Schedule", mappedBy="station")
     * @MaxDepth(1)
     */
    private $schedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="station")
     * @MaxDepth(1)
     */
    private $tempDriverRoutes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RouteCommitment", mappedBy="station")
     * @MaxDepth(1)
     */
    private $routeCommitments;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GradingSettings", mappedBy="station")
     * @MaxDepth(1)
     */
    private $stationGradingSettings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BalanceGroup", mappedBy="station")
     * @MaxDepth(1)
     */
    private $balanceGroups;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Device", mappedBy="station")
     * @MaxDepth(1)
     */
    private $devices;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Vehicle", mappedBy="station")
     * @MaxDepth(1)
     */
    private $vehicles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timezone;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRoute", mappedBy="shiftStation")
     * @MaxDepth(1)
     */
    private $driverShiftStation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TempDriverRoute", mappedBy="shiftStation")
     * @MaxDepth(1)
     */
    private $tempDriverRouteShiftStation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $channelSid;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incident", mappedBy="station")
     * @MaxDepth(1)
     */
    private $incidents;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DriverRouteCode", mappedBy="station")
     * @MaxDepth(1)
     */
    private $driverRouteCodes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CortexSchedule", mappedBy="station")
     * @MaxDepth(1)
     */
    private $cortexSchedules;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CortexLog", mappedBy="station")
     * @MaxDepth(1)
     */
    private $cortexLogs;

    /**
     * @ORM\OneToMany(targetEntity=UpdatedSchedule::class, mappedBy="station")
     */
    private $updatedSchedules;

    /**
     * @ORM\OneToMany(targetEntity=DailyDriverSummary::class, mappedBy="station")
     */
    private $dailyDriverSummaries;

    /**
     * @ORM\Column(type="boolean", options={"default": 1})
     */
    private $isStationSummary = 1;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\VehicleDriverRecord", mappedBy="station")
     * @MaxDepth(1)
     */
    private $vehicleDriverRecord;

    public function __construct()
    {
        $this->drivers = new ArrayCollection();
        $this->driverRoutes = new ArrayCollection();
        $this->invoiceTypes = new ArrayCollection();
        $this->shifts = new ArrayCollection();
        $this->skillRates = new ArrayCollection();
        $this->rates = new ArrayCollection();
        $this->schedules = new ArrayCollection();
        $this->tempDriverRoutes = new ArrayCollection();
        $this->routeCommitments = new ArrayCollection();
        $this->stationGradingSettings = new ArrayCollection();
        $this->balanceGroups = new ArrayCollection();
        $this->devices = new ArrayCollection();
        $this->vehicles = new ArrayCollection();
        $this->driverShiftStation = new ArrayCollection();
        $this->tempDriverRouteShiftStation = new ArrayCollection();
        $this->driverRouteCodes = new ArrayCollection();
        $this->cortexSchedules = new ArrayCollection();
        $this->cortexLogs = new ArrayCollection();
        $this->incidents = new ArrayCollection();
        $this->updatedSchedules = new ArrayCollection();
        $this->dailyDriverSummaries = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getStationAddress(): ?string
    {
        return $this->stationAddress;
    }

    public function setStationAddress(string $stationAddress): self
    {
        $this->stationAddress = $stationAddress;

        return $this;
    }

    public function getStationLatitude(): ?string
    {
        return $this->stationLatitude;
    }

    public function setStationLatitude(string $stationLatitude): self
    {
        $this->stationLatitude = $stationLatitude;

        return $this;
    }

    public function getStationLongitude(): ?string
    {
        return $this->stationLongitude;
    }

    public function setStationLongitude(string $stationLongitude): self
    {
        $this->stationLongitude = $stationLongitude;

        return $this;
    }

    public function getStationGeofence(): ?string
    {
        return $this->stationGeofence;
    }

    public function setStationGeofence(string $stationGeofence): self
    {
        $this->stationGeofence = $stationGeofence;

        return $this;
    }

    public function getParkingLotAddress(): ?string
    {
        return $this->parkingLotAddress;
    }

    public function setParkingLotAddress(?string $parkingLotAddress): self
    {
        $this->parkingLotAddress = $parkingLotAddress;

        return $this;
    }

    public function getParkingLotLatitude(): ?string
    {
        return $this->parkingLotLatitude;
    }

    public function setParkingLotLatitude(?string $parkingLotLatitude): self
    {
        $this->parkingLotLatitude = $parkingLotLatitude;

        return $this;
    }

    public function getParkingLotLongitude(): ?string
    {
        return $this->parkingLotLongitude;
    }

    public function setParkingLotLongitude(?string $parkingLotLongitude): self
    {
        $this->parkingLotLongitude = $parkingLotLongitude;

        return $this;
    }

    public function getParkingLotGeofence(): ?string
    {
        return $this->parkingLotGeofence;
    }

    public function setParkingLotGeofence(?string $parkingLotGeofence): self
    {
        $this->parkingLotGeofence = $parkingLotGeofence;

        return $this;
    }

    /**
     * @return Collection|Driver[]
     */
    public function getDrivers(): Collection
    {
        return $this->drivers;
    }

    public function addDriver(Driver $driver): self
    {
        if (!$this->drivers->contains($driver)) {
            $this->drivers[] = $driver;
            $driver->addStation($this);
        }

        return $this;
    }

    public function removeDriver(Driver $driver): self
    {
        if ($this->drivers->contains($driver)) {
            $this->drivers->removeElement($driver);
            $driver->removeStation($this);
        }

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverRoutes(): Collection
    {
        return $this->driverRoutes;
    }

    public function addDriverRoute(DriverRoute $driverRoute): self
    {
        if (!$this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes[] = $driverRoute;
            $driverRoute->setStation($this);
        }

        return $this;
    }

    public function removeDriverRoute(DriverRoute $driverRoute): self
    {
        if ($this->driverRoutes->contains($driverRoute)) {
            $this->driverRoutes->removeElement($driverRoute);
            // set the owning side to null (unless already changed)
            if ($driverRoute->getStation() === $this) {
                $driverRoute->setStation(null);
            }
        }

        return $this;
    }

    public function getVehicleDriverRecord(): ?VehicleDriverRecord
    {
        return $this->vehicleDriverRecord;
    }

    public function setVehicleDriverRecord(?VehicleDriverRecord $vehicleDriverRecord): self
    {
        $this->vehicleDriverRecord = $vehicleDriverRecord;

        // set (or unset) the owning side of the relation if necessary
        $newStation = null === $vehicleDriverRecord ? null : $this;
        if ($vehicleDriverRecord->getStation() !== $newStation) {
            $vehicleDriverRecord->setStation($newStation);
        }

        return $this;
    }

    public function getCompany(): ?Company
    {
        return $this->company;
    }

    public function setCompany(?Company $company): self
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return Collection|InvoiceType[]
     */
    public function getInvoiceTypes(): Collection
    {
        return $this->invoiceTypes;
    }

    public function addInvoiceType(InvoiceType $invoiceType): self
    {
        if (!$this->invoiceTypes->contains($invoiceType)) {
            $this->invoiceTypes[] = $invoiceType;
            $invoiceType->setStation($this);
        }

        return $this;
    }

    public function removeInvoiceType(InvoiceType $invoiceType): self
    {
        if ($this->invoiceTypes->contains($invoiceType)) {
            $this->invoiceTypes->removeElement($invoiceType);
            // set the owning side to null (unless already changed)
            if ($invoiceType->getStation() === $this) {
                $invoiceType->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Shift[]
     */
    public function getShifts(): Collection
    {
        return $this->shifts;
    }

    public function addShift(Shift $shift): self
    {
        if (!$this->shifts->contains($shift)) {
            $this->shifts[] = $shift;
            $shift->setStation($this);
        }

        return $this;
    }

    public function removeShift(Shift $shift): self
    {
        if ($this->shifts->contains($shift)) {
            $this->shifts->removeElement($shift);
            // set the owning side to null (unless already changed)
            if ($shift->getStation() === $this) {
                $shift->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SkillRate[]
     */
    public function getSkillRates(): Collection
    {
        return $this->skillRates;
    }

    public function addSkillRate(SkillRate $skillRate): self
    {
        if (!$this->skillRates->contains($skillRate)) {
            $this->skillRates[] = $skillRate;
            $skillRate->setStation($this);
        }

        return $this;
    }

    public function removeSkillRate(SkillRate $skillRate): self
    {
        if ($this->skillRates->contains($skillRate)) {
            $this->skillRates->removeElement($skillRate);
            // set the owning side to null (unless already changed)
            if ($skillRate->getStation() === $this) {
                $skillRate->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rate[]
     */
    public function getRates(): Collection
    {
        return $this->rates;
    }

    public function addRate(Rate $rate): self
    {
        if (!$this->rates->contains($rate)) {
            $this->rates[] = $rate;
            $rate->setStation($this);
        }

        return $this;
    }

    public function removeRate(Rate $rate): self
    {
        if ($this->rates->contains($rate)) {
            $this->rates->removeElement($rate);
            // set the owning side to null (unless already changed)
            if ($rate->getStation() === $this) {
                $rate->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Schedule[]
     */
    public function getSchedules(): Collection
    {
        return $this->schedules;
    }

    public function addSchedule(Schedule $schedule): self
    {
        if (!$this->schedules->contains($schedule)) {
            $this->schedules[] = $schedule;
            $schedule->setStation($this);
        }

        return $this;
    }

    public function removeSchedule(Schedule $schedule): self
    {
        if ($this->schedules->contains($schedule)) {
            $this->schedules->removeElement($schedule);
            // set the owning side to null (unless already changed)
            if ($schedule->getStation() === $this) {
                $schedule->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRoutes(): Collection
    {
        return $this->tempDriverRoutes;
    }

    public function addTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if (!$this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes[] = $tempDriverRoute;
            $tempDriverRoute->setStation($this);
        }

        return $this;
    }

    public function removeTempDriverRoute(TempDriverRoute $tempDriverRoute): self
    {
        if ($this->tempDriverRoutes->contains($tempDriverRoute)) {
            $this->tempDriverRoutes->removeElement($tempDriverRoute);
            // set the owning side to null (unless already changed)
            if ($tempDriverRoute->getStation() === $this) {
                $tempDriverRoute->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RouteCommitment[]
     */
    public function getRouteCommitments(): Collection
    {
        return $this->routeCommitments;
    }

    public function addRouteCommitment(RouteCommitment $routeCommitment): self
    {
        if (!$this->routeCommitments->contains($routeCommitment)) {
            $this->routeCommitments[] = $routeCommitment;
            $routeCommitment->setStation($this);
        }

        return $this;
    }

    public function removeRouteCommitment(RouteCommitment $routeCommitment): self
    {
        if ($this->routeCommitments->contains($routeCommitment)) {
            $this->routeCommitments->removeElement($routeCommitment);
            // set the owning side to null (unless already changed)
            if ($routeCommitment->getStation() === $this) {
                $routeCommitment->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|GradingSettings[]
     */
    public function getStationGradingSettings(): Collection
    {
        return $this->stationGradingSettings;
    }

    public function addStationGradingSetting(GradingSettings $stationGradingSetting): self
    {
        if (!$this->stationGradingSettings->contains($stationGradingSetting)) {
            $this->stationGradingSettings[] = $stationGradingSetting;
            $stationGradingSetting->setStation($this);
        }

        return $this;
    }

    public function removeStationGradingSetting(GradingSettings $stationGradingSetting): self
    {
        if ($this->stationGradingSettings->contains($stationGradingSetting)) {
            $this->stationGradingSettings->removeElement($stationGradingSetting);
            // set the owning side to null (unless already changed)
            if ($stationGradingSetting->getStation() === $this) {
                $stationGradingSetting->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BalanceGroup[]
     */
    public function getBalanceGroups(): Collection
    {
        return $this->balanceGroups;
    }

    public function addBalanceGroup(BalanceGroup $balanceGroup): self
    {
        if (!$this->balanceGroups->contains($balanceGroup)) {
            $this->balanceGroups[] = $balanceGroup;
            $balanceGroup->setStation($this);
        }

        return $this;
    }

   /**
    * @return Collection|Incident[]
    */
   public function getIncidents(): Collection
   {
       return $this->incidents;
   }

   public function addIncident(Incident $incident): self
   {
       if (!$this->incidents->contains($incident)) {
           $this->incidents[] = $incident;
           $incident->setStation($this);
       }

       return $this;
   }

    public function removeBalanceGroup(BalanceGroup $balanceGroup): self
    {
        if ($this->balanceGroups->contains($balanceGroup)) {
            $this->balanceGroups->removeElement($balanceGroup);
            // set the owning side to null (unless already changed)
            if ($balanceGroup->getStation() === $this) {
                $balanceGroup->setStation(null);
            }
        }

        return $this;
    }

    public function removeIncident(Incident $incident): self
    {
        if ($this->incidents->contains($incident)) {
            $this->incidents->removeElement($incident);
            // set the owning side to null (unless already changed)
            if ($incident->getStation() === $this) {
                $incident->setStation(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * @return Collection|Device[]
     */
    public function getDevices(): Collection
    {
        return $this->devices;
    }

    public function addDevice(Device $device): self
    {
        if (!$this->devices->contains($device)) {
            $this->devices[] = $device;
            $device->setStation($this);
        }

        return $this;
    }

    public function removeDevice(Device $device): self
    {
        if ($this->devices->contains($device)) {
            $this->devices->removeElement($device);
            // set the owning side to null (unless already changed)
            if ($device->getStation() === $this) {
                $device->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Vehicle[]
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): self
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles[] = $vehicle;
            $vehicle->setStation($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): self
    {
        if ($this->vehicles->contains($vehicle)) {
            $this->vehicles->removeElement($vehicle);
            // set the owning side to null (unless already changed)
            if ($vehicle->getStation() === $this) {
                $vehicle->setStation(null);
            }
        }

        return $this;
    }

    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    public function setTimezone(?string $timezone): self
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @return Collection|DriverRoute[]
     */
    public function getDriverShiftStation(): Collection
    {
        return $this->driverShiftStation;
    }

    public function addDriverShiftStation(DriverRoute $driverShiftStation): self
    {
        if (!$this->driverShiftStation->contains($driverShiftStation)) {
            $this->driverShiftStation[] = $driverShiftStation;
            $driverShiftStation->setShiftStation($this);
        }

        return $this;
    }

    public function removeDriverShiftStation(DriverRoute $driverShiftStation): self
    {
        if ($this->driverShiftStation->contains($driverShiftStation)) {
            $this->driverShiftStation->removeElement($driverShiftStation);
            // set the owning side to null (unless already changed)
            if ($driverShiftStation->getShiftStation() === $this) {
                $driverShiftStation->setShiftStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TempDriverRoute[]
     */
    public function getTempDriverRouteShiftStation(): Collection
    {
        return $this->tempDriverRouteShiftStation;
    }

    public function addTempDriverRouteShiftStation(TempDriverRoute $tempDriverRouteShiftStation): self
    {
        if (!$this->tempDriverRouteShiftStation->contains($tempDriverRouteShiftStation)) {
            $this->tempDriverRouteShiftStation[] = $tempDriverRouteShiftStation;
            $tempDriverRouteShiftStation->setShiftStation($this);
        }

        return $this;
    }

    public function removeTempDriverRouteShiftStation(TempDriverRoute $tempDriverRouteShiftStation): self
    {
        if ($this->tempDriverRouteShiftStation->contains($tempDriverRouteShiftStation)) {
            $this->tempDriverRouteShiftStation->removeElement($tempDriverRouteShiftStation);
            // set the owning side to null (unless already changed)
            if ($tempDriverRouteShiftStation->getShiftStation() === $this) {
                $tempDriverRouteShiftStation->setShiftStation(null);
            }
        }

        return $this;
    }

    public function getChannelSid(): ?string
    {
        return $this->channelSid;
    }

    public function setChannelSid(?string $channelSid): self
    {
        $this->channelSid = $channelSid;

        return $this;
    }

    /**
     * @return Collection|DriverRouteCode[]
     */
    public function getDriverRouteCodes(): Collection
    {
        return $this->driverRouteCodes;
    }

    public function addDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if (!$this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes[] = $driverRouteCode;
            $driverRouteCode->setStation($this);
        }

        return $this;
    }

    public function removeDriverRouteCode(DriverRouteCode $driverRouteCode): self
    {
        if ($this->driverRouteCodes->contains($driverRouteCode)) {
            $this->driverRouteCodes->removeElement($driverRouteCode);
            // set the owning side to null (unless already changed)
            if ($driverRouteCode->getStation() === $this) {
                $driverRouteCode->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CortexSchedule[]
     */
    public function getCortexSchedules(): Collection
    {
        return $this->cortexSchedules;
    }

    public function addCortexSchedule(CortexSchedule $cortexSchedule): self
    {
        if (!$this->cortexSchedules->contains($cortexSchedule)) {
            $this->cortexSchedules[] = $cortexSchedule;
            $cortexSchedule->setStation($this);
        }

        return $this;
    }

    public function removeCortexSchedule(CortexSchedule $cortexSchedule): self
    {
        if ($this->cortexSchedules->contains($cortexSchedule)) {
            $this->cortexSchedules->removeElement($cortexSchedule);
            // set the owning side to null (unless already changed)
            if ($cortexSchedule->getStation() === $this) {
                $cortexSchedule->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|CortexLog[]
     */
    public function getCortexLogs(): Collection
    {
        return $this->cortexLogs;
    }

    public function addCortexLog(CortexLog $cortexLog): self
    {
        if (!$this->cortexLogs->contains($cortexLog)) {
            $this->cortexLogs[] = $cortexLog;
            $cortexLog->setStation($this);
        }

        return $this;
    }

    public function removeCortexLog(CortexLog $cortexLog): self
    {
        if ($this->cortexLogs->contains($cortexLog)) {
            $this->cortexLogs->removeElement($cortexLog);
            // set the owning side to null (unless already changed)
            if ($cortexLog->getStation() === $this) {
                $cortexLog->setStation(null);
            }
        }

        return $this;
    }

    public function getTimeZoneName(): string {
        $timezones['ET'] = 'America/Detroit';
        $timezones['CT'] = 'America/Chicago';
        $timezones['MT'] = 'America/Denver';
        $timezones['PT'] = 'America/Los_Angeles';
        $timezones['AKT'] = 'US/Alaska';
        $timezones['HT'] = 'Pacific/Honolulu';

        if($this->timezone !== null){
            return $timezones[$this->timezone];
        }
        return 'America/Detroit';
    }

    /**
     * @return Collection|UpdatedSchedule[]
     */
    public function getUpdatedSchedules(): Collection
    {
        return $this->updatedSchedules;
    }

    public function addUpdatedSchedule(UpdatedSchedule $updatedSchedule): self
    {
        if (!$this->updatedSchedules->contains($updatedSchedule)) {
            $this->updatedSchedules[] = $updatedSchedule;
            $updatedSchedule->setStation($this);
        }

        return $this;
    }

    public function removeUpdatedSchedule(UpdatedSchedule $updatedSchedule): self
    {
        if ($this->updatedSchedules->contains($updatedSchedule)) {
            $this->updatedSchedules->removeElement($updatedSchedule);
            // set the owning side to null (unless already changed)
            if ($updatedSchedule->getStation() === $this) {
                $updatedSchedule->setStation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|DailyDriverSummary[]
     */
    public function getDailyDriverSummaries(): Collection
    {
        return $this->dailyDriverSummaries;
    }

    public function addDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
    {
        if (!$this->dailyDriverSummaries->contains($dailyDriverSummary)) {
            $this->dailyDriverSummaries[] = $dailyDriverSummary;
            $dailyDriverSummary->setStation($this);
        }

        return $this;
    }

    public function removeDailyDriverSummary(DailyDriverSummary $dailyDriverSummary): self
    {
        if ($this->dailyDriverSummaries->contains($dailyDriverSummary)) {
            $this->dailyDriverSummaries->removeElement($dailyDriverSummary);
            // set the owning side to null (unless already changed)
            if ($dailyDriverSummary->getStation() === $this) {
                $dailyDriverSummary->setStation(null);
            }
        }

        return $this;
    }

    public function getIsStationSummary(): ?bool
    {
        return $this->isStationSummary;
    }

    public function setIsStationSummary(bool $isStationSummary): self
    {
        $this->isStationSummary = $isStationSummary;

        return $this;
    }
}
