<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IncidentPhaseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentPhase
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\IncidentType", inversedBy="incidentPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $incidentType;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="incidentPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @MaxDepth(1)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordering;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\IncidentStep", mappedBy="phase", cascade={"persist", "remove"})
     * @MaxDepth(1)
     */
    private $incidentSteps;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $allocatedTime;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInApp = true;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="incidentCompletedPhase", cascade={"persist", "remove"})
     */
    private $incidentCompletedPhases;

    /**
     * @ORM\OneToMany(targetEntity=Incident::class, mappedBy="incidentCurrentPhase", cascade={"persist", "remove"})
     */
    private $incidentCurrentPhases;

    /**
     * @ORM\OneToMany(targetEntity=IncidentSubmitedPhase::class, mappedBy="incidentPhase"), cascade={"persist", "remove"}
     */
    private $incidentSubmitedPhases;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isInWeb = true;

    /**
     * @ORM\Column(type="boolean")
     */
    private $onlyAccessToManagerOfDriver = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $token;

    /**
     * @ORM\Column(type="boolean")
     */
    private $hasReview;



    public function __construct()
    {
        $this->incidentSteps = new ArrayCollection();
        $this->incidentCompletedPhases = new ArrayCollection();
        $this->incidentCurrentPhases = new ArrayCollection();
        $this->incidentSubmitedPhases = new ArrayCollection();
        $this->createdDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIncidentType(): ?IncidentType
    {
        return $this->incidentType;
    }

    public function setIncidentType(?IncidentType $incidentType): self
    {
        $this->incidentType = $incidentType;

        return $this;
    }

    public function getRole(): ?Role
    {
        return $this->role;
    }

    public function setRole(?Role $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getOrdering(): ?int
    {
        return $this->ordering;
    }

    public function setOrdering(int $ordering): self
    {
        $this->ordering = $ordering;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->createdDate;
    }

    public function setCreatedDate(\DateTimeInterface $createdDate): self
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * @return Collection|IncidentStep[]
     */
    public function getIncidentSteps(): Collection
    {
        return $this->incidentSteps;
    }

    public function addIncidentStep(IncidentStep $incidentStep): self
    {
        if (!$this->incidentSteps->contains($incidentStep)) {
            $this->incidentSteps[] = $incidentStep;
            $incidentStep->setPhase($this);
        }

        return $this;
    }

    public function removeIncidentStep(IncidentStep $incidentStep): self
    {
        if ($this->incidentSteps->contains($incidentStep)) {
            $this->incidentSteps->removeElement($incidentStep);
            // set the owning side to null (unless already changed)
            if ($incidentStep->getPhase() === $this) {
                $incidentStep->setPhase(null);
            }
        }

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getAllocatedTime(): ?int
    {
        return $this->allocatedTime;
    }

    public function setAllocatedTime(?int $allocatedTime): self
    {
        $this->allocatedTime = $allocatedTime;

        return $this;
    }

    public function getIsInApp(): ?bool
    {
        return $this->isInApp;
    }

    public function setIsInApp(bool $isInApp): self
    {
        $this->isInApp = $isInApp;

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidentCompletedPhases(): Collection
    {
        return $this->incidentCompletedPhases;
    }

    public function addIncidentCompletedPhase(Incident $incidentCompletedPhase): self
    {
        if (!$this->incidentCompletedPhases->contains($incidentCompletedPhase)) {
            $this->incidentCompletedPhases[] = $incidentCompletedPhase;
            $incidentCompletedPhase->setIncidentCompletedPhase($this);
        }

        return $this;
    }

    public function removeIncidentCompletedPhase(Incident $incidentCompletedPhase): self
    {
        if ($this->incidentCompletedPhases->contains($incidentCompletedPhase)) {
            $this->incidentCompletedPhases->removeElement($incidentCompletedPhase);
            // set the owning side to null (unless already changed)
            if ($incidentCompletedPhase->getIncidentCompletedPhase() === $this) {
                $incidentCompletedPhase->setIncidentCompletedPhase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Incident[]
     */
    public function getIncidentCurrentPhases(): Collection
    {
        return $this->incidentCurrentPhases;
    }

    public function addIncidentCurrentPhase(Incident $incidentCurrentPhase): self
    {
        if (!$this->incidentCurrentPhases->contains($incidentCurrentPhase)) {
            $this->incidentCurrentPhases[] = $incidentCurrentPhase;
            $incidentCurrentPhase->setIncidentCurrentPhase($this);
        }

        return $this;
    }

    public function removeIncidentCurrentPhase(Incident $incidentCurrentPhase): self
    {
        if ($this->incidentCurrentPhases->contains($incidentCurrentPhase)) {
            $this->incidentCurrentPhases->removeElement($incidentCurrentPhase);
            // set the owning side to null (unless already changed)
            if ($incidentCurrentPhase->getIncidentCurrentPhase() === $this) {
                $incidentCurrentPhase->setIncidentCurrentPhase(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|IncidentSubmitedPhase[]
     */
    public function getIncidentSubmitedPhases(): Collection
    {
        return $this->incidentSubmitedPhases;
    }

    public function addIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if (!$this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases[] = $incidentSubmitedPhase;
            $incidentSubmitedPhase->setIncidentPhase($this);
        }

        return $this;
    }

    public function removeIncidentSubmitedPhase(IncidentSubmitedPhase $incidentSubmitedPhase): self
    {
        if ($this->incidentSubmitedPhases->contains($incidentSubmitedPhase)) {
            $this->incidentSubmitedPhases->removeElement($incidentSubmitedPhase);
            // set the owning side to null (unless already changed)
            if ($incidentSubmitedPhase->getIncidentPhase() === $this) {
                $incidentSubmitedPhase->setIncidentPhase(null);
            }
        }

        return $this;
    }

    public function getIsInWeb(): ?bool
    {
        return $this->isInWeb;
    }

    public function setIsInWeb(bool $isInWeb): self
    {
        $this->isInWeb = $isInWeb;

        return $this;
    }

    public function getOnlyAccessToManagerOfDriver(): ?bool
    {
        return $this->onlyAccessToManagerOfDriver;
    }

    public function setOnlyAccessToManagerOfDriver(bool $onlyAccessToManagerOfDriver): self
    {
        $this->onlyAccessToManagerOfDriver = $onlyAccessToManagerOfDriver;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getHasReview(): ?bool
    {
        return $this->hasReview;
    }

    public function setHasReview(bool $hasReview): self
    {
        $this->hasReview = $hasReview;

        return $this;
    }
}
