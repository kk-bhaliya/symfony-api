<?php

namespace App\Entity;

use App\Repository\IncidentSubmitedPhaseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=IncidentSubmitedPhaseRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class IncidentSubmitedPhase
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=IncidentPhase::class, inversedBy="incidentSubmitedPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incidentPhase;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="incidentSubmitedPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $submitedBy;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $submittedAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isArchive = false;

    /**
     * @ORM\ManyToOne(targetEntity=Incident::class, inversedBy="incidentSubmitedPhases")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $incident;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="incidentReviewedPhases")
     * @ORM\JoinColumn(nullable=true)
     */
    private $reviewedBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $reviewedAt;

    public function __construct()
    {
        $this->submittedAt = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIncidentPhase(): ?IncidentPhase
    {
        return $this->incidentPhase;
    }

    public function setIncidentPhase(?IncidentPhase $incidentPhase): self
    {
        $this->incidentPhase = $incidentPhase;

        return $this;
    }

    public function getSubmitedBy(): ?User
    {
        return $this->submitedBy;
    }

    public function setSubmitedBy(?User $submitedBy): self
    {
        $this->submitedBy = $submitedBy;

        return $this;
    }

    public function getSubmittedAt(): ?\DateTimeInterface
    {
        return $this->submittedAt;
    }

    public function setSubmittedAt(\DateTimeInterface $submittedAt): self
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    public function getIsArchive(): ?bool
    {
        return $this->isArchive;
    }

    public function setIsArchive(bool $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    public function getIncident(): ?Incident
    {
        return $this->incident;
    }

    public function setIncident(?Incident $incident): self
    {
        $this->incident = $incident;

        return $this;
    }

    public function getReviewedBy(): ?User
    {
        return $this->reviewedBy;
    }

    public function setReviewedBy(?User $reviewedBy): self
    {
        $this->reviewedBy = $reviewedBy;

        return $this;
    }

    public function getReviewedAt(): ?\DateTimeInterface
    {
        return $this->reviewedAt;
    }

    public function setReviewedAt(?\DateTimeInterface $reviewedAt): self
    {
        $this->reviewedAt = $reviewedAt;

        return $this;
    }
}
