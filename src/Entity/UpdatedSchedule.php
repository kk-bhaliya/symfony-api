<?php

namespace App\Entity;

use App\Repository\UpdatedScheduleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UpdatedScheduleRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class UpdatedSchedule
{
    use Timestamps;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Schedule::class, inversedBy="updatedSchedules")
     */
    private $schedule;

    /**
     * @ORM\ManyToOne(targetEntity=Station::class, inversedBy="updatedSchedules")
     */
    private $station;

    /**
     * @ORM\Column(type="boolean")
     */
    private $status = 0;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $scheduleDate;

    public function __construct()
    {
        $this->setScheduleDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchedule(): ?Schedule
    {
        return $this->schedule;
    }

    public function setSchedule(?Schedule $schedule): self
    {
        $this->schedule = $schedule;

        return $this;
    }

    public function getStation(): ?Station
    {
        return $this->station;
    }

    public function setStation(?Station $station): self
    {
        $this->station = $station;

        return $this;
    }

    public function getStatus(): ?bool
    {
        return $this->status;
    }

    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getScheduleDate(): ?\DateTimeInterface
    {
        return $this->scheduleDate;
    }

    public function setScheduleDate(?\DateTimeInterface $scheduleDate): self
    {
        $this->scheduleDate = $scheduleDate;

        return $this;
    }
}
