<?php


namespace App\Serializer\Normalizer;


use App\Entity\Station;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class StationNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'drivers', 'driverRoutes', 'vehicleDriverRecord', 'company',
                'invoiceTypes', 'shifts', 'skillRates', 'rates', 'schedules',
                'tempDriverRoutes', 'routeCommitments', 'stationGradingSettings',
                'balanceGroups', 'location', 'devices', 'vehicles',
                'driverShiftStation', 'tempDriverRouteShiftStation', 'driverRouteCodes',
                'cortexSchedules','cortexLogs', 'updatedSchedules', 'incidents',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy',
            ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Station;
    }
}
