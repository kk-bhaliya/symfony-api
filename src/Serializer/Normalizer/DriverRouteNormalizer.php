<?php


namespace App\Serializer\Normalizer;


use App\Entity\BalanceGroup;
use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\InvoiceType;
use App\Entity\KickoffLog;
use App\Entity\ReturnToStation;
use App\Entity\RouteRequests;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use App\Entity\Vehicle;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DriverRouteNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }


    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'routeCode', 'vehicle', 'driver', 'vehicleDriverRecord', 'status',
                'schedule', 'oldDriver',
                'oldDriverRoute', 'driverRoutes', 'shiftStation',
                'shiftSkill', 'shiftBalanceGroup',
                'tempDriverRoutes', 'oldTempDriverRoutes', 'events',
                'oldRouteRequest', 'newRouteRequest', 'incidents', 'device',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy', 'locations',
                'outsideStationAt', 'outsideParkingLotAt', 'insideStationLatitude', 'outsideStationLongitude', 'insideParkingLotLatitude',
                "outsideParkingLotLongitude",
                "insideStationLongitude",
                "outsideStationLatitude",
                "insideParkingLotLongitude",
                "outsideParkingLotLatitude"
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'requestForRoute' => function ($innerObject) {
                if ($innerObject instanceof RouteRequests) {
                    return [
                        'id' => $innerObject->getId(),
                        'reasonForRequest' => $innerObject->getReasonForRequest(),
                        'isSwapRequest' => $innerObject->getIsSwapRequest(),
                        'isRequestCompleted' => $innerObject->getIsRequestCompleted(),
                        'isArchive' => $innerObject->getIsArchive(),
                    ];
                }
                return null;
            },
            'skill' => function ($innerObject) {
                if ($innerObject instanceof Skill) {
                    return [
                        'id' => $innerObject->getId(),
                        'name' => $innerObject->getName(),
                        'isArchive' => $innerObject->getIsArchive(),
                    ];
                }
                return null;
            },
            'shiftType' => function ($innerObject) {
                if ($innerObject instanceof Shift) {
                    return [
                        'id' => $innerObject->getId(),
                        'color' => $innerObject->getColor(),
                        'textColor' => $innerObject->getTextColor(),
                        'category' => $innerObject->getCategory(),
                        'isArchive' => $innerObject->getIsArchive(),
                    ];
                }
                return null;
            },
            'shiftInvoiceType' => function ($innerObject) {
                if ($innerObject instanceof Shift) {
                    return [
                        'id' => $innerObject->getId(),
                        'isArchive' => $innerObject->getIsArchive(),
                    ];
                }
                return null;
            },
            'driverRouteCodes' => function ($innerObject) {
                if ($innerObject instanceof Collection && $innerObject->count() > 0) {
                    $result = [];
                    /** @var DriverRouteCode $item */
                    foreach ($innerObject as $item) {
                        if ($item instanceof DriverRoute && $item->getIsArchive() === false) {
                            $result[] = $item->getCode();
                        }
                    }
                    return $result;
                }
                return [];
            },
//            'tempDriverRoutes' => function ($innerObject) {
//                if ($innerObject instanceof Collection && $innerObject->count() > 0) {
//                    return array_map(function (TempDriverRoute $item) {
//                        return [
//                            'id' => $item->getId(),
//                            'isArchive' => $item->getIsArchive(),
//                            'routeId' => $item->getRouteId()
//                        ];
//                    }, $innerObject->toArray());
//                }
//                return [];
//            },
//            'oldTempDriverRoutes' => function ($innerObject) {
//                if ($innerObject instanceof Collection && $innerObject->count() > 0) {
//                    return array_map(function (TempDriverRoute $item) {
//                        return [
//                            'id' => $item->getId(),
//                            'isArchive' => $item->getIsArchive(),
//                            'routeId' => $item->getRouteId()
//                        ];
//                    }, $innerObject->toArray());
//                }
//                return [];
//            },
            'kickoffLog' => function ($innerObject) {
                if ($innerObject instanceof KickoffLog) {
                    return $innerObject->getId();
                }
                return null;
            },
            'returnToStation' => function ($innerObject) {
                if ($innerObject instanceof ReturnToStation) {
                    return $innerObject->getId();
                }
                return null;
            },
            'station' => function ($innerObject) {
                if ($innerObject instanceof Station) {
                    return [
                        'id' => $innerObject->getId(),
                        'name' => $innerObject->getName(),
                        'code' => $innerObject->getCode(),
                        'timezoneName' => $innerObject->getTimeZoneName(),
                        'timezone' => $innerObject->getTimezone(),
                        'isArchive' => $innerObject->getIsArchive(),
                    ];
                }
                return null;
            }
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof DriverRoute;
    }
}
