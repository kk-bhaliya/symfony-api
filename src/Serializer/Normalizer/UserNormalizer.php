<?php


namespace App\Serializer\Normalizer;


use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::ATTRIBUTES] =
            [
                'driver', 'userRoles', 'id', 'email', 'isEnabled',
                'friendlyName', 'isArchive', 'profileImage', 'chatIdentifier',
                'notificationsChannelSid', 'isResetPassword', 'company', 'createdAt', 'updatedAt', 'updatedBy'
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'company' => function ($innerObject) {
                if ($innerObject instanceof Company) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'driver' => function ($innerObject) {
                if ($innerObject instanceof Driver) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'userRoles' => function ($innerObject) {
                if ($innerObject instanceof Collection) {
                    $roles = [];
                    foreach ($innerObject as $role) {
                        $roles[] = $role;
                    }
                    return $roles;
                }
                return null;
            },
            'updatedBy' => function($innerObject){
                if ($innerObject instanceof User) {
                   return [
                       'id' => $innerObject->getId()
                   ];
                }
                return null;
            }
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof User;
    }
}
