<?php


namespace App\Serializer\Normalizer;


use App\Entity\Driver;
use App\Entity\DriverIncidentRecord;
use App\Entity\Incident;
use App\Entity\Station;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DriverIncidentRecordNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy'
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'driver' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                if ($innerObject instanceof Driver) {
                    return [
                        'id' => $innerObject->getId()];
                }
                return null;
            },
            'incident' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                if ($innerObject instanceof Incident) {
                    return [
                        'id' => $innerObject->getId()];
                }
                return null;
            }
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof DriverIncidentRecord;
    }
}