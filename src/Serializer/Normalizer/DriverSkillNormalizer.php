<?php


namespace App\Serializer\Normalizer;


use App\Entity\DriverSkill;
use App\Entity\InvoiceType;
use App\Entity\Skill;
use Symfony\Component\Serializer\Exception\CircularReferenceException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Exception\InvalidArgumentException;
use Symfony\Component\Serializer\Exception\LogicException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DriverSkillNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'driver', 'skillRate',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy'
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'skill' => function ($innerObject) {
                if ($innerObject instanceof Skill) {
                    return $innerObject;
                }
                return null;
            },
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof DriverSkill;
    }
}