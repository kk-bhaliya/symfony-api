<?php


namespace App\Serializer\Normalizer;

use App\Entity\Driver;
use App\Entity\Task;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TaskNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy'
            ];
        $context[AbstractNormalizer::CALLBACKS] = [
            'assignees' => function ($innerObject) {
                if ($innerObject instanceof Collection && is_array($innerObject->toArray())) {
                    return array_map(function (User $item) {
                        return [
                            'userId' => $item->getId(),
                            'friendlyName' => $item->getFriendlyName(),
                            'identity' => $item->getChatIdentifier(),
                        ];
                    }, $innerObject->toArray());
                }
                return null;
            },
        ];
        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Task;
    }
}