<?php


namespace App\Serializer\Normalizer;


use App\Entity\BalanceGroup;
use App\Entity\Company;
use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\EmploymentStatus;
use App\Entity\GradingSettings;
use App\Entity\Image;
use App\Entity\Rate;
use App\Entity\Role;
use App\Entity\RouteCommitment;
use App\Entity\Schedule;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\Status;
use App\Entity\User;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CompanyNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {

        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'companyRoles', 'skills', 'devices', 'statuses',
                'users', 'employmentStatuses', 'schedules', 'stripeData',
                'locations', 'stations', 'rates', 'drivers', 'balanceGroups',
                'station', 'driverRouteCodes', 'vehicles', 'shifts', 'gradingSettings', 'companyRouteCommitments', 'cortexLogs',
                'image', 'owners', 'incidentTypes', 'incidentQuestions', 'incidents',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy'
            ];
        $context[AbstractNormalizer::CALLBACKS] = [
            'companyRoles' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                if ($innerObject instanceof Role) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'skills' => function ($innerObject) {
                if ($innerObject instanceof Skill) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'devices' => function ($innerObject) {
                if ($innerObject instanceof Device) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'image' => function ($innerObject) {
                if ($innerObject instanceof Image) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'statuses' => function ($innerObject) {
                if ($innerObject instanceof Status) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'users' => function ($innerObject) {
                if ($innerObject instanceof User) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'employmentStatuses' => function ($innerObject) {
                if ($innerObject instanceof EmploymentStatus) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'stations' => function ($innerObject) {
                if ($innerObject instanceof Station) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'schedules' => function ($innerObject) {
                if ($innerObject instanceof Schedule) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'rates' => function ($innerObject) {
                if ($innerObject instanceof Rate) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'drivers' => function ($innerObject) {
                if ($innerObject instanceof Driver) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'balanceGroups' => function ($innerObject) {
                if ($innerObject instanceof BalanceGroup) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'companyRouteCommitments' => function ($innerObject) {
                if ($innerObject instanceof RouteCommitment) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'gradingSettings' => function ($innerObject) {
                if ($innerObject instanceof GradingSettings) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'shifts' => function ($innerObject) {
                if ($innerObject instanceof Shift) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);

    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Company;
    }
}
