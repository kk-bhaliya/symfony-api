<?php


namespace App\Serializer\Normalizer;


use App\Entity\Device;
use App\Entity\DriverRoute;
use App\Entity\ReturnToStation;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ReturnToStationNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy',
            ];
        $context[AbstractNormalizer::CALLBACKS] = [
            'driverRoute' => function ($innerObject) {
                if ($innerObject instanceof DriverRoute) {
                    return ['id' => $innerObject->getId()];
                }
                return null;
            },
            'device' => function ($innerObject) {
                if ($innerObject instanceof Device) {
                    return [
                        'id' => $innerObject->getId(),
                    ];
                }
                return null;
            },
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof ReturnToStation;
    }
}