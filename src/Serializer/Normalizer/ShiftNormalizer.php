<?php


namespace App\Serializer\Normalizer;


use App\Entity\Shift;
use App\Entity\Station;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class ShiftNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'invoiceType', 'scheduleDesigns', 'skill',
                'shiftTemplate', 'driverRoutes', 'tempDriverRoutes',
                'balanceGroup', 'shiftRouteCommitments', 'company',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy',
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'station' => function ($innerObject) {
                if ($innerObject instanceof Station) {
                    return [
                        'id' => $innerObject->getId()];
                }
                return null;
            }
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Shift;
    }
}