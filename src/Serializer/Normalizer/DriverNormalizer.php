<?php


namespace App\Serializer\Normalizer;


use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class DriverNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }


    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'vehicleDriverRecords', 'driverIncidentRecords',
                'scores', 'employmentStatus',
                'payRate', 'emergencyContacts', 'company',
                'timeOffs', 'schedules', 'tempDriverRoutes', 'cortexLogs',
                'oldDriverRoutes', 'oldTempDriverRoutes',
                'ssn', 'driverSkills', 'newDriverRequest', 'oldDriverRequests',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy' , 'tasks'
            ];

        $context[AbstractNormalizer::CALLBACKS] = [
            'user' => function ($innerObject) {
                if ($innerObject instanceof User) {
                    return [
                        'id' => $innerObject->getId(),
                        'email' => $innerObject->getEmail(),
                        'channel' => $innerObject->getNotificationsChannelSid(),
                        'identity' => $innerObject->getChatIdentifier(),
                    ];
                }
                return null;
            },
            'driverRoutes' => function ($innerObject) {
                if ($innerObject instanceOf Collection && is_array($innerObject->toArray())) {
                    return array_map(function (DriverRoute $item ) {
                        return [
                            'driverRouteId' => $item->getId(),
                            'dateCreated' => $item->getDateCreated()->format('Y-m-d'),
                            'shiftId' => $item->getShiftType()->getId(),
                            'shiftName' => $item->getShiftType()->getName(),
                        ];
                    }, $innerObject->toArray());
                }
                return null;
            },
            'assignedManagers' => function ($innerObject) {
                if ($innerObject instanceOf Collection && is_array($innerObject->toArray())) {
                    return array_map(function (User $item ) {
                        return [
                            'id' => $item->getId(),
                            'friendlyName' => $item->getFriendlyName(),
                            'driverId' => $item->getDriver() ? $item->getDriver()->getId() : null,
                            'phoneNumber' => $item->getPhoneNumber(),
                            'email' => $item->getEmail(),
                            'chatIdentifier' => $item->getChatIdentifier(),
                        ];
                    }, $innerObject->toArray());
                }
                return null;
            },
        ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Driver;
    }
}
