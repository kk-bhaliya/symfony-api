<?php


namespace App\Serializer\Normalizer;


use App\Entity\InvoiceType;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\TempDriverRoute;
use Proxies\__CG__\App\Entity\BalanceGroup;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class TempDriverRouteNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'skill', 'vehicle', 'device', 'driver', 'station',
                'status', 'shiftType', 'schedule',
                'routeId', 'kickoffLog', 'vehicleDriverRecord',
                'oldDriver', 'oldDriverRoute', 'shiftInvoiceType', 'shiftStation','shiftBalanceGroup',
                'shiftSkill',
                '__cloner__', '__isInitialized__', '__initializer__',
                'updatedBy',
            ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof TempDriverRoute;
    }
}