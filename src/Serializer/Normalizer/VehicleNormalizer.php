<?php


namespace App\Serializer\Normalizer;


use App\Entity\Vehicle;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class VehicleNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                'company', 'tempDriverRoutes', 'vehicleDriverRecords',
                'driverRoutes', 'location', 'station', 'incidents', 'events',
                'usedByUser', 'updatedBy',
                '__cloner__', '__isInitialized__', '__initializer__',
            ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof Vehicle;
    }
}
