<?php


namespace App\Serializer\Normalizer;


use App\Entity\Device;
use App\Entity\DriverRoute;
use App\Entity\KickoffLog;
use App\Entity\Vehicle;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class KickoffLogNormalizer implements NormalizerInterface
{
    /**
     * @var ObjectNormalizer
     */
    private $objectNormalizer;

    public function __construct(ObjectNormalizer $objectNormalizer)
    {
        $this->objectNormalizer = $objectNormalizer;
    }

    public function normalize($object, $format = null, array $context = [])
    {
        $context[AbstractNormalizer::IGNORED_ATTRIBUTES] =
            [
                '__cloner__', '__isInitialized__', '__initializer__',
                'driverRoute', 'device', 'vehicle',
                'stationArrivalAt', 'vanSupplies',
                'updatedBy'
            ];

        return $this->objectNormalizer->normalize($object, $format, $context);
    }

    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof KickoffLog;
    }
}