<?php

namespace App\Repository;

use App\Entity\CortexSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CortexSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method CortexSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method CortexSettings[]    findAll()
 * @method CortexSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CortexSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CortexSettings::class);
    }
}
