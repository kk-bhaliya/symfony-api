<?php

namespace App\Repository;

use App\Entity\DailyDriverSummary;
use App\Entity\Company;
use App\Entity\Station;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method DailyDriverSummary|null find($id, $lockMode = null, $lockVersion = null)
 * @method DailyDriverSummary|null findOneBy(array $criteria, array $orderBy = null)
 * @method DailyDriverSummary[]    findAll()
 * @method DailyDriverSummary[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DailyDriverSummaryRepository extends ServiceEntityRepository
{
    private $kernel;
    protected $tokenStorage;

    public function __construct(ManagerRegistry $registry, KernelInterface $kernel, TokenStorageInterface $tokenStorage)
    {
        $this->kernel = $kernel;
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, DailyDriverSummary::class);
    }

    public function getDailyDriverSummaryByStation($station, $passedDate = null)
    {
        $stationTz = new \DateTimeZone($station->getTimeZoneName());
        $stationNow = new \DateTime('now', $stationTz);
        $offset = $stationNow->format('P');

        $now =  new \DateTime('now');

        if ($passedDate == null) {
            $passedDate = $now;
        } else {
            $passedDate = new \DateTime($passedDate);
            $passedDate->setTime($passedDate->format('h'), $passedDate->format('i'), $passedDate->format('s'));
        }

        $utcDateObj = $passedDate->setTimezone(new \DateTimeZone('UTC'));
        $utcNow = $utcDateObj->format('Y-m-d H:i:00');

        return $this->createQueryBuilder('dds')
                    ->where("dds.summaryDate  = DATE(CONVERT_TZ(:utcNow, '+00:00', :offset))")
                    ->setParameter('utcNow', $utcNow)
                    ->setParameter('offset', $offset)
                    ->andWhere('dds.station = :station')
                    ->setParameter('station', $station)
                    ->getQuery()
                    ->getResult();
    }

    public function getDailySummaryCommandByStation(array $criteria)
    {
        $company = $criteria['company'];
        $station = $criteria['station'];
        $date = new \DateTime($criteria['date']);
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $dailyDriverSummary = $this->findOneBy(['station'=>$station,'summaryDate'=>$date]);
        if(!$dailyDriverSummary){
            $dailyDriverSummary = new DailyDriverSummary();
            $dailyDriverSummary->setCompany($this->getEntityManager()->getReference('App:Company', $company));
            $dailyDriverSummary->setSummaryDate($date);
            $dailyDriverSummary->setStation($this->getEntityManager()->getReference('App:Station', $station));
            $dailyDriverSummary->setCreatedBy($userLogin);
            $dailyDriverSummary->setModifiedBy($userLogin);
            $dailyDriverSummary->setCreatedDate(new \DateTime());
            $dailyDriverSummary->setUpdatedDate(new \DateTime());
            $dailyDriverSummary->setScheduledInterviews(0);
            $dailyDriverSummary->setInterviewsCompleted(0);
            $this->getEntityManager()->persist($dailyDriverSummary);
            $this->getEntityManager()->flush();
        }

        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(['command' => 'dsp:dailysummary:email', '--companies' => [$company], '--station' => $station, '--date' => $criteria['date']]);
        $output = new BufferedOutput();
        $application->run($input, $output);
        return $dailyDriverSummary->getId();
    }
    public function dailySummaryDownload(array $criteria)
    {
        $company = $criteria['company'];
        $station = $criteria['station'];
        $date = new \DateTime($criteria['date']);
        $dailyDriverSummary = $this->findOneBy(['station'=>$station,'summaryDate'=>$date]);        
        if($dailyDriverSummary){
            return $dailyDriverSummary->getReportUrl();
        } else {
            return null;
        }
    }
}
