<?php

namespace App\Repository;

use App\Entity\IncidentQuestion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IncidentQuestion|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentQuestion|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentQuestion[]    findAll()
 * @method IncidentQuestion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentQuestionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentQuestion::class);
    }

    public function removeAllIncidentQuestionsByCompany($company)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentQuestion', 'it')
            ->set('it.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('it.company = :company')
            ->setParameter('company', $company)
            ->andWhere('it.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->execute();

        return true;
    }

    public function deleteAllIncidentQuestionsByCompany($company)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\IncidentQuestion', 'it')
            ->where('it.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->execute();

        return true;
    }
}
