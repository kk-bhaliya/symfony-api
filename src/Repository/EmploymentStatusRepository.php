<?php

namespace App\Repository;

use App\Entity\EmploymentStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method EmploymentStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmploymentStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmploymentStatus[]    findAll()
 * @method EmploymentStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmploymentStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmploymentStatus::class);
    }
}
