<?php

namespace App\Repository;

use App\Entity\CortexLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CortexLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method CortexLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method CortexLog[]    findAll()
 * @method CortexLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CortexLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CortexLog::class);
    }
}
