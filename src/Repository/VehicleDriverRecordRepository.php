<?php

namespace App\Repository;

use App\Entity\VehicleDriverRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method VehicleDriverRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleDriverRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleDriverRecord[]    findAll()
 * @method VehicleDriverRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleDriverRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleDriverRecord::class);
    }

    public function updateStationIsArchiveInVehicleDriverRecord($stationId)
    {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\VehicleDriverRecord','vdr')
                    ->set('vdr.isArchive', true)
                    ->where('vdr.station = :stationId')
                    ->setParameter( 'stationId', $stationId )
                    ->getQuery()
                    ->execute();

        return true;
    }
}
