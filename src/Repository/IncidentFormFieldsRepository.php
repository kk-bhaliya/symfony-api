<?php

namespace App\Repository;

use App\Entity\IncidentFormFields;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IncidentFormFields|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentFormFields|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentFormFields[]    findAll()
 * @method IncidentFormFields[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentFormFieldsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentFormFields::class);
    }
}
