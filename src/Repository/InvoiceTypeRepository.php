<?php

namespace App\Repository;

use App\Entity\InvoiceType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method InvoiceType|null find($id, $lockMode = null, $lockVersion = null)
 * @method InvoiceType|null findOneBy(array $criteria, array $orderBy = null)
 * @method InvoiceType[]    findAll()
 * @method InvoiceType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InvoiceTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InvoiceType::class);
    }

    public function updateRateNullInInvoiceType($rateId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\InvoiceType','iv')
                        ->set('iv.rateRule', 'NULL');
                        if (is_array($rateId)) {
                            $qb->where($qb->expr()->in('iv.rateRule', ':rateRule'))
                               ->setParameter('rateRule', $rateId);
                        } else {
                            $qb->where('iv.rateRule = :rateRule')
                               ->setParameter('rateRule', $rateId);
                        }
                        $qb->getQuery()
                        ->execute();
        return true;
    }

    public function getInvoiceTypeByStation($stationId)
    {
        $results = $this->createQueryBuilder('iv')
                         ->select('iv.id')
                         ->andWhere('iv.station = :station')
                         ->setParameter('station', $stationId )
                         ->andWhere('iv.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->getQuery()
                         ->getResult(Query::HYDRATE_ARRAY);


        return array_column($results, 'id');
    }

    public function updateStationIsArchiveInInvoiceType($stationId)
    {

        $invoiceTypeIds = $this->getInvoiceTypeByStation($stationId);
        if (!empty($invoiceTypeIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\InvoiceType','iv')
                            ->set('iv.isArchive', true)
                            ->where($qb->expr()->in('iv.id', ':invoiceTypeIds'))
                            ->setParameter( 'invoiceTypeIds', $invoiceTypeIds )
                            ->getQuery()
                            ->execute();

            //Call Shift and shift Template isArchive true set
            $this->getEntityManager()->getRepository('App\Entity\Shift')->updateShiftSetIsArchive($invoiceTypeIds);
            $this->getEntityManager()->getRepository('App\Entity\ShiftTemplate')->updateShiftTemplateSetIsArchive($invoiceTypeIds);
        }

        return true;
    }

    public function getAllInvoiceTypeByStation($criteria)
    {
        $results = $this->createQueryBuilder('iv')
                         ->andWhere('iv.station = :station')
                         ->setParameter('station', $criteria['stationId'] )
                         ->andWhere('iv.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->getQuery()
                         ->getResult();


        return $results;
    }

    public function getInvoiceTypeForScheduler($criteria)
    {
        $query = $this->createQueryBuilder('it')
            ->leftJoin('it.station', 's')
            ->leftJoin('s.company', 'c');

        if (isset($criteria['station'])) {
            $query->andWhere("it.station = :station")
                ->setParameter('station', $criteria['station']);
        }

        $result = $query->andWhere('s.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('it.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->addOrderBy('s.name', 'ASC')
            ->addOrderBy('it.name', 'ASC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    public function getInvoiceTypeImportData($name, $station, $rateRule, $hour, $invoiceTypeData)
    {
        $em = $this->getEntityManager();

        $invoiceType = new InvoiceType();
        $invoiceType->setName($name);
        $invoiceType->setStation($station);
        $invoiceType->setRateRule($rateRule);
        $invoiceType->setBillableHour($hour);
        $invoiceType->setInvoiceType($invoiceTypeData);
        $em->persist($invoiceType);
        $em->flush();

        return $invoiceType;
    }
}
