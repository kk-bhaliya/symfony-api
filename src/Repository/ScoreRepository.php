<?php

namespace App\Repository;

use App\Entity\Score;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Score|null find($id, $lockMode = null, $lockVersion = null)
 * @method Score|null findOneBy(array $criteria, array $orderBy = null)
 * @method Score[]    findAll()
 * @method Score[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScoreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Score::class);
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getScores($criteria)
    {

        $dateObj1 = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $date1 = $dateObj1->modify('-7 day');
        $dateObj2 = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $date2 = $dateObj2->modify('-14 day');

        $response=[];
        $last7days =  $this->createQueryBuilder('s')
            ->select('SUM(s.deliveryScore) As deliveryScore,SUM(s.attendanceScore) AS attendanceScore,SUM(s.coreValueScore) AS coreValueScore')
            ->where('s.driver = :val')
            ->andWhere('s.createdAt >= :days')
            ->andWhere('s.isArchive = :isArchive')
            ->setParameter('val', $criteria['id'])
            ->setParameter('days',$date1)
            ->setParameter('isArchive',false)
            ->groupBy('s.driver')
            ->getQuery()
            ->getOneOrNullResult();


        $last14days =  $this->createQueryBuilder('s')
            ->select('SUM(s.deliveryScore) As deliveryScore,SUM(s.attendanceScore) AS attendanceScore,SUM(s.coreValueScore) AS coreValueScore')
            ->where('s.driver = :val')
            ->andWhere('s.createdAt BETWEEN :date1 AND :date2')
            ->andWhere('s.isArchive = :isArchive')
            ->setParameter('val', $criteria['id'])
            ->setParameter('date1', $date2)
            ->setParameter('date2', $date1)
            ->setParameter('isArchive',false)
            ->groupBy('s.driver')
            ->getQuery()
            ->getOneOrNullResult();

        $response['sevendays']=$last7days;
        $response['seven_of_sevenday']=$last14days;
        return $response;

    }


    /*
    public function findOneBySomeField($value): ?Score
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
