<?php

namespace App\Repository;

use App\Entity\Company;
use App\Entity\Device;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\Station;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Utils\GlobalUtility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use DateTime;
use Error;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method KickoffLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method KickoffLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method KickoffLog[]    findAll()
 * @method KickoffLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KickoffLogRepository extends ServiceEntityRepository
{
    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var GlobalUtility
     */
    private $globalUtils;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage, EntityManagerInterface $em, GlobalUtility $globalUtils)
    {
        parent::__construct($registry, KickoffLog::class);
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->globalUtils = $globalUtils;
    }

    /**
     * @param array $criteria
     * @return array|int|mixed|string
     * @throws NonUniqueResultException
     */
    public function loadOut(array $criteria)
    {
        $qb = $this->createQueryBuilder('k')
            ->select('k.id')
            ->addSelect('k.gasTankLevel')
            ->addSelect('k.loggedAssignedVanAt')
            ->addSelect('k.loggedMileageAndGasAt')
            ->addSelect('k.loggedTouchFlexAt')
            ->addSelect('k.loggedTouchMentorAt')
            ->addSelect('k.loggedVanSuppliesAt')
            ->addSelect('k.vanSuppliesResult')
            ->addSelect('k.mileage')
            ->addSelect('dr.id AS driverRouteId')
            ->leftJoin('k.driverRoute', 'dr')
            ->where('k.id = :id')
            ->setParameter('id', $criteria['id']);
        return $qb->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function setVanSupplies(array $criteria)
    {
        try {
            $entity = $this->find($criteria['id']);
            if (!($entity instanceof KickoffLog)) {
                throw new Exception('KickoffLog not found');
            }
            if (!empty($criteria['loggedVanSuppliesAt'])) {
                $entity->setLoggedVanSuppliesAt(new DateTime($criteria['loggedVanSuppliesAt']));
            }
            if (is_array($criteria['vanSuppliesResult'])) {
                $entity->setVanSuppliesResult($criteria['vanSuppliesResult']);
            }
            $this->_em->flush();
            return ['success' => true, 'id' => $entity->getId(), 'loggedVanSuppliesAt' => $entity->getLoggedVanSuppliesAt(), 'vanSuppliesResult' => $entity->getVanSuppliesResult()];
        } catch (OptimisticLockException $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        } catch (ORMException $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        } catch (Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        } catch (Error $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function setMileage(array $criteria)
    {
        $entity = $this->globalUtils->validateFieldsSetMileage($criteria, 'KickoffLog');
        $driverRoute = $entity->getDriverRoute();
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.');
        }
        $device = $this->globalUtils->getDevice($criteria);
        $user = $this->globalUtils->getTokenUser();
        $messageBy = ' by ' . $user->getFriendlyName() . '.';
        $messageMileage = 'Mileage at Load Out is ' . $criteria['mileage'] . ' miles ' . $messageBy;
        $messageGasTankLevel = 'Gas Tank Level at Load Out is ' . $criteria['gasTankLevel'] . $messageBy;
        $vehicle = $driverRoute->getVehicle();
        if ($vehicle instanceof Vehicle) {
            $vehicle->setCurrentGasTankLevel($criteria['gasTankLevel']);
            $vehicle->setCurrentMileage($criteria['mileage']);
            $this->_em->flush();
            $mileageEvent = $this->em->getRepository(Event::class)->createEvent('MILEAGE', 'Mileage at Load Out.', $messageMileage, null, $driverRoute, $vehicle, $device, $user);
            $gasTankLevelEvent = $this->em->getRepository(Event::class)->createEvent('GAS', 'Gas Tank Level at Load Out.', $messageGasTankLevel, null, $driverRoute, $vehicle, $device, $user);
            $vehicle->addEvent($mileageEvent);
            $vehicle->addEvent($gasTankLevelEvent);
            $driverRoute->addEvent($mileageEvent);
            $driverRoute->addEvent($gasTankLevelEvent);
            $this->_em->flush();

            return [
                'success' => true,
                'kickoffLog' => ['id' => $criteria['id']],
                'vehicle' => ['id' => $vehicle->getId()],
                'driverRoute' => ['id' => $driverRoute->getId()],
                'gasTankLevelEvent' => ['id' => $gasTankLevelEvent->getId()],
                'mileageEvent' => ['id' => $mileageEvent->getId()],
            ];
        }

        $message = 'Load Out process was not filled with Mileage and Gas Tank Level in due to van could not be found' . $messageBy;
        $errorVehicleEvent = $this->em->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED_ERROR', 'Mileage and Gas Tank Level at Load Out.', $message, null, $driverRoute, null, $device, $user, 'error');
        $mileageEvent = $this->em->getRepository(Event::class)->createEvent('MILEAGE', 'Mileage at Load Out.', $messageMileage, null, $driverRoute, null, $device, $user);
        $gasTankLevelEvent = $this->em->getRepository(Event::class)->createEvent('GAS', 'Gas Tank Level at Load Out.', $messageGasTankLevel, null, $driverRoute, null, $device, $user);
        $driverRoute->addEvent($mileageEvent);
        $driverRoute->addEvent($gasTankLevelEvent);
        $driverRoute->addEvent($errorVehicleEvent);
        $this->_em->flush();

        return [
            'success' => true,
            'kickoffLog' => ['id' => $criteria['id']],
            'driverRoute' => ['id' => $driverRoute->getId()],
            'gasTankLevelEvent' => ['id' => $gasTankLevelEvent->getId()],
            'mileageEvent' => ['id' => $mileageEvent->getId()],
            'errorVehicleEvent' => ['id' => $errorVehicleEvent->getId()],
        ];
    }

    /**
     * @param KickoffLog $kickoffLog
     * @return DriverRoute
     * @throws Exception
     */
    private function fetchDriverRoute(KickoffLog $kickoffLog)
    {
        $driverRoute = $kickoffLog->getDriverRoute();
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.');
        }
        return $driverRoute;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function setUpVinBasedOnScanner(array $data)
    {
        if (empty($data['kickoffLogId'])) {
            throw new Exception('kickoffLogId missing.');
        }
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }
        if (empty($data['vin'])) {
            throw new Exception('VIN is missing.');
        }
        if (empty($data['vinRaw'])) {
            throw new Exception('VIN Raw Value is missing.');
        }
        if (empty($data['scannedQRCodeAt'])) {
            throw new Exception('scannedQRCodeAt is missing.');
        }
        if (empty($data['lastUsedAt'])) {
            throw new Exception('lastUsedAt is missing.');
        }

        $kickoffLog = $this->find($data['kickoffLogId']);
        if (!($kickoffLog instanceof KickoffLog)) {
            throw new Exception('KickoffLog not found.');
        }
        $driverRoute = $this->fetchDriverRoute($kickoffLog);
        $kickoffLog->setQrCodeValue($data['vinRaw']);
        $kickoffLog->setScannedQRCodeAt($this->globalUtils->getDateTimeObject($data['scannedQRCodeAt']));
        $kickoffLog->setScannedQRCode(true);
        $this->em->flush();
        $vin = $this->globalUtils->getVinFromInput($data['vin']);
        $vehicle = $this->em->getRepository(Vehicle::class)->findOneVehicleByVin(['vin' => $vin, 'company' => $data['companyId'], 'isArchive' => false]);
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        if ($vehicle instanceof Vehicle) {
            $driverRoute->setVehicle($vehicle);
            $kickoffLog->setVehicle($vehicle);
            $vehicle->setLastUsedAt($this->globalUtils->getDateTimeObject($data['lastUsedAt']));
            $vehicle->setCurrentUsedBy($user->getFriendlyName());
            $vehicle->setLastUsedByUser($user);
            $this->em->flush();
            $vehicleId = $vehicle->getVehicleId();
            $vin = $vehicle->getVin();

            if (is_string($vehicleId)) {
                $message = $user->getFriendlyName() . ' scanned successfully the vehicle ID: ' . $vehicleId;
            } elseif (is_string($vin)) {
                $message = $user->getFriendlyName() . ' scanned successfully the vehicle VIN: ' . $vin;
            } else {
                $message = $user->getFriendlyName() . ' scanned successfully the vehicle #: ' . strval($vehicle->getId());
            }

            $event = $this->em
                ->getRepository(Event::class)
                ->createEvent('VEHICLE_ATTACHED', 'Attached vehicle in Load Out Process.', $message, null, $driverRoute, $vehicle, null);

            return [
                'success' => true,
                'vehicle' => [
                    'id' => $vehicle->getId(),
                    'vin' => $vehicle->getVin(),
                    'vehicleId' => $vehicle->getVehicleId(),
                ],
                'event' => [
                    'id' => $event->getId()
                ]
            ];
        }

        $message = $user->getFriendlyName() . ' failed to scan successfully.';
        $errorEvent = $this->em
            ->getRepository(Event::class)
            ->createEvent('VEHICLE_ATTACHED_ERROR', 'Failed to attach vehicle in Load Out Process.', $message, null, $driverRoute, null, null);

        return [
            'success' => false,
            'message' => 'We could not find the vehicle in the inventory.',
            'event' => [
                'id' => $errorEvent->getId()
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function partialSearchVan(array $data)
    {
        if (empty($data['kickoffLogId'])) {
            throw new Exception('kickoffLogId missing.');
        }
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }
        $picture = null;
        if (!empty($data['picture'])) {
            $picture = $data['picture'];
        }
        if (empty($data['vin'])) {
            throw new Exception('vin missing.');
        }
        $location = null;
        $device = null;
        $vehicle = null;
        if (!empty($data['deviceId'])) {
            $device = $this->em->find(Device::class, $data['deviceId']);
        }
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $kickoffLog = $this->find($data['kickoffLogId']);
        $prevQrCodePictureUrl = $kickoffLog->getQrCodePictureUrl();
        $driverRoute = $this->fetchDriverRoute($kickoffLog);

        $vehicle = $this->em->getRepository(Vehicle::class)->findOneVehicleByVin(['vin' => $data['vin'], 'company' => $data['companyId'], 'isArchive' => false]);
        if ($vehicle instanceof Vehicle) {
            $vehicle->setLastUsedAt(new DateTime('now'));
            $vehicle->setCurrentUsedBy($user->getFriendlyName());
            $vehicle->setLastUsedByUser($user);
            $kickoffLog->setVehicle($vehicle);
            $kickoffLog->setQrCodePictureUrl($picture);
            $driverRoute->setVehicle($vehicle);
            $this->em->flush();
            $vehicleId = $vehicle->getVehicleId();
            $vin = $vehicle->getVin();
            $username = $user->getFriendlyName();
            if (is_string($vehicleId)) {
                $message = $username . ' partially searched the van with Vehicle ID: ' . $vehicleId . '.';
                $taskName = "Check the QR Code of van with vehicle ID: " . $vehicleId;
            } elseif (is_string($vin)) {
                $message = $username . ' partially searched with the van with VIN: ' . $vin . '.';
                $taskName = "Check the QR Code of van with VIN: " . $vin;
            } else {
                $message = $username . ' partially searched with the vehicle #: ' . strval($vehicle->getId()) . '.';
                $taskName = "Check the QR Code of van with vehicle #: " . strval($vehicle->getId());
            }
            $currQrCodePictureUrl = $kickoffLog->getQrCodePictureUrl();
            $taskData = [
                'create' => is_string($currQrCodePictureUrl) && is_string($prevQrCodePictureUrl) && $prevQrCodePictureUrl === $currQrCodePictureUrl ? false : true,
                'name' => $taskName,
                'type' => 'QR_CODE_FAILURE',
                'description' => $message,
                'stationId' => $data['stationId'],
                'data' => [
                    'stationId' => $data['stationId'],
                    'companyId' => $data['companyId'],
                    'kickoffLogId' => $data['kickoffLogId'],
                    'driverRouteId' => $driverRoute->getId(),
                    'picture' => $picture,
                    'driver' => [
                        'userId' => $user->getId()
                    ],
                    'vehicleId' => $vehicle->getId()
                ]
            ];

            $event = $this->em->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED', 'Attached vehicle in Load Out Process.', $message, $location, $driverRoute, $vehicle, $device, $user);

            return [
                'success' => true,
                'message' => $message,
                'vehicle' => [
                    'id' => $vehicle->getId(),
                    'vin' => $vin,
                    'vehicleId' => $vehicleId
                ],
                'taskData' => $taskData,
                'event' => [
                    'id' => $event->getId()
                ]
            ];
        }

        $kickoffLog->setQrCodePictureUrl($data['picture']);
        $this->em->flush();

        $userBy = $user->getFriendlyName();
        $message = $userBy . ' failed to find vehicle by partial manual search.';
        if (!empty($data['vin']) && is_string($data['vin'])) {
            $message = $userBy . ' failed to find vehicle by partial manual search with VIN: ' . $data['vin'];
        } elseif (!empty($data['vehicleId']) && is_string($data['vehicleId'])) {
            $message = $message . ' failed to find vehicle by partial manual search with Vehicle ID: ' . $data['vehicleId'];
        }

        $eventData = [
            'name' => 'VEHICLE_ATTACHED_ERROR',
            'eventName' => 'Failed to attach vehicle in Load Out Process.',
            'message' => $message,
            'location' => $location,
            'driverRoute' => $driverRoute,
            'vehicle' => $vehicle,
            'device' => $device
        ];

        $event = $this->em->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED_ERROR', 'Failed to attach vehicle in Load Out Process.', $message, $location, $driverRoute, $vehicle, $device, $user);

        return [
            'success' => false,
            'message' => 'We could not find the vehicle in the inventory.',
            'eventData' => $eventData,
            'event' => [
                'id' => $event->getId()
            ]
        ];
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function fullSearchVan(array $data)
    {
        if (empty($data['kickoffLogId'])) {
            throw new Exception('kickoffLogId missing.');
        }
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }
        if (empty($data['vin'])) {
            throw new Exception('vin missing.');
        }
        if (!empty($data['vin']) && !is_string($data['vin'])) {
            throw new Exception('vin field has to be string.');
        }
        if (empty($data['picture'])) {
            throw new Exception('picture missing.');
        }
        $picture = $data['picture'];
        $device = $this->globalUtils->getDevice($data);
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $kickoffLog = $this->find($data['kickoffLogId']);
        $kickoffLog->setQrCodePictureUrl($picture);
        $this->em->flush();
        $driverRoute = $this->fetchDriverRoute($kickoffLog);
        $vin = $this->globalUtils->getVinFromInput($data['vin']);
        $username = $user->getFriendlyName();
        $vehicle = $this->em->getRepository(Vehicle::class)->findOneVehicleByVin(['vin' => $vin, 'company' => $data['companyId'], 'isArchive' => false]);
        if ($vehicle instanceof Vehicle) {
            $kickoffLog->setVehicle($vehicle);
            $driverRoute->setVehicle($vehicle);
            $this->em->flush();
            $vehicleVehicleId = $vehicle->getVehicleId();
            $vehicleID = $vehicle->getId();
            $vin = $vehicle->getVin();
            if (is_string($vehicleVehicleId)) {
                $message = $username . ' full searched successfully the vehicle ID: ' . $vehicleVehicleId . " .";
            } elseif (is_string($vin)) {
                $message = $username . ' full searched successfully the vehicle VIN: ' . $vin . " .";
            } else {
                $message = $username . ' full searched successfully the vehicle #: ' . strval($vehicleID) . " .";
            }

            $event = $this->em->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED', 'Attached vehicle in Load Out Process.', $message, null, $driverRoute, $vehicle, $device, $user);

            return [
                'success' => true,
                'message' => $message,
                'vehicle' => [
                    'id' => $vehicleID,
                    'vin' => $vin,
                    'vehicleId' => $vehicleVehicleId,
                    'isPending' => $vehicle->getIsPending()
                ],
                'event' => [
                    'id' => $event->getId()
                ]
            ];
        }

        /** @var Vehicle $vehicle */
        $vehicle = $this->em->getRepository(Vehicle::class)->createVan(['vin' => $vin, 'vehicleId' => substr($vin, -6), 'notes' => $message]);
        $station = $driverRoute->getStation();
        $company = $this->em->find(Company::class, $data['companyId']);
        if ($station instanceof Station) {
            $vehicle->setStation($station);
        }
        if ($company instanceof Company) {
            $vehicle->setCompany($company);
        }
        $vehicle->setLastUsedAt(new DateTime('now'));
        $vehicle->setCurrentUsedBy($user->getFriendlyName());
        $vehicle->setLastUsedByUser($user);
        $vehicle->setIsPending(true);
        $this->em->flush();
        $vehicleID = $vehicle->getId();
        $vehicleVin = $vehicle->getVin();
        $vehicleVehicleId = $vehicle->getVehicleId();
        $vehicleIsPending = $vehicle->getIsPending();
        $kickoffLog->setVehicle($vehicle);
        $driverRoute->setVehicle($vehicle);
        $this->em->flush();
        $message = $username . ' created a new vehicle with VIN: ' . $vehicleVin . ' in Load Out Process.';
        $newVehicleEvent = $this->em->getRepository(Event::class)->createEvent('NEW_VEHICLE', 'Registered new vehicle in Load Out', $message, null, $driverRoute, $vehicle, $device, $user);

        $taskData = [
            'create' => true,
            'name' => 'Review the new vehicle with VIN: ' . $vehicleVin . '.',
            'type' => 'NEW_VEHICLE',
            'description' => $message,
            'stationId' => $data['stationId'],
            'userId' => $user->getId(),
            'vehicleId' => $vehicle->getId(),
            'data' => [
                'stationId' => $data['stationId'],
                'companyId' => $data['companyId'],
                'kickoffLogId' => $data['kickoffLogId'],
                'driverRouteId' => $driverRoute->getId(),
                'picture' => $picture,
                'driver' => [
                    'userId' => $user->getId()
                ],
                'vehicleId' => $vehicle->getId()
            ]
        ];

        return [
            'success' => true,
            'message' => $message,
            'newVehicleEvent' => [
                'id' => $newVehicleEvent->getId()
            ],
            'vehicle' => [
                'id' => $vehicleID,
                'vin' => $vehicleVin,
                'vehicleId' => $vehicleVehicleId,
                'isPending' => $vehicleIsPending
            ],
            'taskData' => $taskData
        ];
    }
}
