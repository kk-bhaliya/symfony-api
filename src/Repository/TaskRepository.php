<?php

namespace App\Repository;

use App\Entity\Task;
use App\Entity\User;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }


    /**
     * @param string $name
     * @param User[] $users
     * @param string $type
     * @param string|null $description
     * @param array|null $data
     * @param DateTimeInterface|null $dueDate
     * @return Task
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createTask(string $name, array $users, string $type, ?string $description = "N/A", ?array $data = [], ?DateTimeInterface $dueDate = null)
    {
        $task = new Task();
        $task->setName($name);
        $task->setType($type);

        if ($description) {
            $task->setDescription($description);
        }
        if ($data) {
            $task->setData($data);
        }
        if ($dueDate) {
            $task->setDueDate($dueDate);
        }

        foreach ($users as $user) {
            $task->addAssignee($user);
        }
        $this->_em->persist($task);
        $this->_em->flush();
        return $task;
    }

    // /**
    //  * @return Task[] Returns an array of Task objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Task
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
