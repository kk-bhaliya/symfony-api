<?php

namespace App\Repository;

use App\Entity\UpdatedSchedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UpdatedSchedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method UpdatedSchedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method UpdatedSchedule[]    findAll()
 * @method UpdatedSchedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UpdatedScheduleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UpdatedSchedule::class);
    }
}
