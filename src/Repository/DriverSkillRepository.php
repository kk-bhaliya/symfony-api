<?php

namespace App\Repository;

use App\Entity\Driver;
use App\Entity\DriverSkill;
use App\Entity\Skill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method DriverSkill|null find($id, $lockMode = null, $lockVersion = null)
 * @method DriverSkill|null findOneBy(array $criteria, array $orderBy = null)
 * @method DriverSkill[]    findAll()
 * @method DriverSkill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DriverSkillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DriverSkill::class);
    }

    public function updateSkillNullInDriverSkill($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverSkill', 'sk')
            ->set('sk.skill', 'NULL')
            ->where('sk.skill = :skillId')
            ->setParameter('skillId', $skillId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateDriverSkillFromSkillRate($critearea)
    {
        $skillRateId = $critearea['skillRate'];
        $oldHourlyRate = $critearea['oldHourlyRate'];
        $hourlyRate = $critearea['hourlyRate'];

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->update('App\Entity\DriverSkill', 'ds')
            ->set('ds.hourlyRate', ':hourlyRate')
            ->where('ds.skillRate = :skillRateId')
            ->andWhere('ds.hourlyRate = :oldHourlyRate')
            ->andWhere('ds.isArchive = 0')
            ->setParameter('skillRateId', $skillRateId)
            ->setParameter('oldHourlyRate', $oldHourlyRate)
            ->setParameter('hourlyRate', $hourlyRate)
            ->getQuery()
            ->execute();

        return true;
    }

    /**
     * @param array $criteria
     * @return Driver[]
     */
    public function getDriversBySkillName(array $criteria)
    {
        $skills = $this->_em->getRepository(Skill::class)
            ->findBy(['isArchive' => false, 'company' => $criteria['companyId'], 'name' => $criteria['name']]);

        $skills = array_map(function (Skill $skill) {
            return $skill->getId();
        }, $skills);

        $driverSkills =  $this->createQueryBuilder('ds')
            ->andWhere('ds.skill in (:skills)')
            ->setParameter('skills', $skills)
            ->andWhere('ds.driver IS NOT NULL')
            ->orderBy('ds.driver', 'ASC')
            ->getQuery()
            ->getResult();

        return array_map(function (DriverSkill $driverSkill){
            return $driverSkill->getDriver();
        }, $driverSkills);
    }


    public function getDriverSkill($driverId)
    {
        $results = $this->createQueryBuilder('ds')
                         ->select('skill.id')
                         ->leftJoin('ds.driver','dr')
                         ->leftJoin('ds.skill','skill')
                         ->where('dr.id = :driverId')
                         ->setParameter('driverId',$driverId)
                         ->getQuery()
                         ->getResult(Query::HYDRATE_ARRAY);

        return array_column($results, 'id');
          
    }
}
