<?php

namespace App\Repository;

use App\Entity\IncidentFormFieldValue;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IncidentFormFieldValue|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentFormFieldValue|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentFormFieldValue[]    findAll()
 * @method IncidentFormFieldValue[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentFormFieldValueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentFormFieldValue::class);
    }
    
    public function removeIncidentFormFieldValues($incidentId, $formFieldIds)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\IncidentFormFieldValue', 'iv')
            ->where('iv.incident = :incident')
            ->setParameter('incident', $incidentId);        
        if (!empty($formFieldIds)) {    
            $qb->andWhere($qb->expr()->in('iv.incidentFormField', ':formFieldIds'))
               ->setParameter('formFieldIds', $formFieldIds);
        }    
        $qb->getQuery()
           ->execute();

        return true;
    }
}
