<?php

namespace App\Repository;

use App\Entity\RouteRequests;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method RouteRequests|null find($id, $lockMode = null, $lockVersion = null)
 * @method RouteRequests|null findOneBy(array $criteria, array $orderBy = null)
 * @method RouteRequests[]    findAll()
 * @method RouteRequests[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouteRequestsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RouteRequests::class);
    }

    public function getRouteRequestByParam($oldRouteId)
    {
        return $this->createQueryBuilder('r')
            ->where('r.newDriver IS NULL')
            ->andWhere('r.approvedBy IS NOT NULL')
            ->andWhere('r.reasonForRequest IS NOT NULL')
            ->andWhere('r.isSwapRequest = :isSwapRequest')
            ->setParameter('isSwapRequest', false)
            ->andWhere('r.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('r.isRequestCompleted = :isRequestCompleted')
            ->setParameter('isRequestCompleted', false)
            ->andWhere('r.oldRoute = :oldRoute')
            ->setParameter('oldRoute', $oldRouteId)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
