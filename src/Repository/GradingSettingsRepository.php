<?php

namespace App\Repository;

use App\Entity\GradingSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method GradingSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method GradingSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method GradingSettings[]    findAll()
 * @method GradingSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GradingSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GradingSettings::class);
    }

    public function updateStationIsArchiveInGradingSettings($stationId)
    {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\GradingSettings','gs')
                        ->set('gs.isArchive', true)
                        ->where('gs.station = :stationId')
                        ->setParameter( 'stationId', $stationId )
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function getGradingRulesSettings($comapny)
    {
        $results =  $this->createQueryBuilder('gs')
            ->select('gs.id,gs.min,gs.max,gs.color,gs.textColor,gs.description')
            ->where('gs.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('gs.company = :company')
            ->setParameter('company', $comapny)
            ->orderBy('gs.min', 'ASC')
            ->getQuery()
            ->getScalarResult();
    
        return $results;
    }
}
