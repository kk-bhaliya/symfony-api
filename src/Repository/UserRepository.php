<?php

namespace App\Repository;

use App\Entity\Driver;
use App\Entity\Task;
use App\Entity\User;
use App\Utils\GlobalUtility;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use phpDocumentor\Reflection\Types\This;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    /**
     * @var GlobalUtility
     */
    private $globalUtility;

    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     * @param GlobalUtility $globalUtility
     */
    public function __construct(ManagerRegistry $registry, GlobalUtility $globalUtility)
    {
        parent::__construct($registry, User::class);
        $this->globalUtility = $globalUtility;
    }

    public function findManagers($criteria)
    {
        $company = $this->getEntityManager()->getRepository('App\Entity\Company')->find($criteria['company']);

        $station = (array_key_exists('station', $criteria)) ? $criteria['station'] : '';
        $firstName = (array_key_exists('firstName', $criteria)) ? $criteria['firstName'] : '';
        $lastName = (array_key_exists('lastName', $criteria)) ? $criteria['lastName'] : '';
        /* TODO: Add the following roles only ROLE_ASSISTANT_MANAGER / STATION_MANAGER / OWNER to be in the Manager dropdown */

        $qb = $this->createQueryBuilder('u');
        $qb->leftJoin('u.userRoles', 'r')
            ->leftJoin('u.driver', 'd')
            ->leftJoin('d.stations', 's')
            ->where('r.name = :assistant_manager OR r.name = :station_manager OR r.name = :owner OR r.name = :operations_manager OR r.name = :operations_account_manager')
            ->andWhere('u.company = :company')
            ->andWhere('u.isArchive = 0')
            ->setParameter('company', $criteria['company'])
            ->setParameter('operations_manager', 'ROLE_OPERATIONS_MANAGER')
            ->setParameter('assistant_manager', 'ROLE_ASSISTANT_STATION_MANAGER')
            ->setParameter('station_manager', 'ROLE_STATION_MANAGER')
            ->setParameter('operations_account_manager', 'ROLE_OPERATIONS_ACCOUNT_MANAGER')
            ->setParameter('owner', 'ROLE_OWNER');
        if ($station != '') {
            $qb->andWhere('(s.id = :station AND s.isArchive = 0) OR u.id = :ownerId')
                ->setParameter('station', $station)
                ->setParameter('ownerId', $company->getUsers()->first()->getId());
        }
        if ($firstName != '') {
            $qb->andWhere('LOWER(u.firstName) = :firstName')
                ->setParameter('firstName', strtolower($firstName));
        }
        if ($lastName != '') {
            $qb->andWhere('LOWER(u.lastName) = :lastName')
                ->setParameter('lastName', strtolower($lastName));
        }

        $qb->distinct()
            ->orderBy('u.firstName', 'ASC');
        if ($firstName != '' && $lastName != '') {
            $users = $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
        } else {
            $users = $qb->select('u.id,u.firstName,u.lastName')->getQuery()->getArrayResult();
        }
        return $users;
    }

    public function findTwilioUsers(int $companyId, int $currentUserId)
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->andWhere('u.company = :company')
            ->andWhere('u.id != :currentUser')
            ->setParameter('currentUser', $currentUserId)
            ->setParameter('company', $companyId)
            ->orderBy('u.id', 'DESC');
        return $qb->getQuery()->getResult();
    }

    public function findTwilioUsersArray(int $companyId, int $currentUserId)
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->andWhere('u.company = :company')
            ->andWhere('u.id != :currentUser')
            ->setParameter('currentUser', $currentUserId)
            ->setParameter('company', $companyId)
            ->orderBy('u.id', 'DESC');
        return $qb->getQuery()->getArrayResult();
    }

    public function getEmailExist($email, $user = null)
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->andWhere('u.email = :email')
            ->setParameter('email', $email);

        if ($user) {
            $qb
                ->andWhere('u.id != :userId')
                ->setParameter('userId', $user);
        };

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $criteria
     * @return User[]
     * @throws Exception
     */
    public function getUsersNotIn(array $criteria)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere('u.isArchive = :archive')
            ->setParameter('archive', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere($qb->expr()->notIn('u.id', $criteria['notUsers']));

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $criteria
     * @return User[]
     * @throws Exception
     */
    public function getUsersNotInByChatIdentifier(array $criteria)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere('u.isArchive = :archive')
            ->setParameter('archive', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere($qb->expr()->notIn('u.chatIdentifier', $criteria['users']));

        return $qb->getQuery()->getResult();
    }

    public function getNotTwilioUsers(array $criteria)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isArchive = :archive')
            ->setParameter('archive', false)
            ->andWhere('u.chatIdentifier IS NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->getQuery()->getResult();
    }

    public function getTwilioUsers(array $criteria)
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.driver', 'd')
            ->where('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->getQuery()->getResult();
    }

    public function getTwilioUsersByCompany(array $criteria)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isArchive = :archive')
            ->setParameter('archive', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->getQuery()->getResult();
    }

    public function getTwilioUsersV2($criteria)
    {
        $qb = $this->createQueryBuilder('u');
        $qb
            ->leftJoin('u.driver', 'd')
            ->leftJoin('d.stations', 's')
            ->leftJoin('d.driverSkills', 'ds')
            ->leftJoin('ds.skill', 'dsk')
            ->leftJoin('u.userRoles', 'ur')
            ->addSelect('ur.name AS roleName')
            ->addSelect('u.id')
            ->addSelect('d.id AS driverId')
            ->addSelect('u.chatIdentifier AS identity')
            ->addSelect('u.email AS email')
            ->addSelect('u.firstName')
            ->addSelect('u.lastName')
            ->addSelect('u.profileImage')
            ->addSelect('u.friendlyName')
            ->addSelect('d.jobTitle')
            ->addSelect('s.name as stationName')
            ->addSelect('s.code as stationCode')
            ->addSelect('s.id AS stationId')
            ->addSelect('dsk.name AS skillName')
            ->addSelect('dsk.id AS skillId')
            ->andWhere('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('s.isArchive = :isStationArchive')
            ->setParameter('isStationArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $criteria['company']);
        return $qb->getQuery()->getScalarResult();
    }

    public function getTwilioUsersV1(array $criteria)
    {
        $qb = $this->createQueryBuilder('u');

        $qb->leftJoin('u.driver', 'd');
        if (!empty($criteria['station'])) {
            $qb->leftJoin('d.stations', 's');
        }
        $qb
            ->select('u.id AS userId, d.id AS driverId, u.chatIdentifier AS identity, u.email, u.firstName, u.lastName, u.profileImage AS image, u.friendlyName')
            ->where('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company']);
        if (!empty($criteria['station'])) {
            $qb
                ->andWhere('s.id = :station')
                ->setParameter('station', $criteria['station']);
        }

        return $qb->getQuery()->getScalarResult();
    }

    public function getNotArchivedTwilioUsers()
    {
        return $this->createQueryBuilder('u')
            ->leftJoin('u.driver', 'd')
            ->where('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.chatIdentifier IS NOT NULL');
    }

    /**
     * @param array $criteria
     * @return User[] | array
     */
    public function getTwilioUsersFilter(array $criteria)
    {
        $qb = $this->getNotArchivedTwilioUsers();

        if (isset($criteria['company'])) {
            $qb->andWhere('u.company = :company')
                ->setParameter('company', $criteria['company']);
        }

        if (isset($criteria['notificationsChannelSid']) && $criteria['notificationsChannelSid'] === 'IS NOT NULL') {
            $qb
                ->andWhere('u.notificationsChannelSid IS NOT NULL');
        }

        if (isset($criteria['notificationsChannelSid']) && $criteria['notificationsChannelSid'] === 'IS NULL') {
            $qb
                ->andWhere('u.notificationsChannelSid IS NULL');
        }
        if (isset($criteria['scalarResult']) && $criteria['scalarResult']) {
            return $qb->getQuery()->getScalarResult();
        }
        return $qb->getQuery()->getResult();
    }

    public function fetchUser(array $criteria)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.chatIdentifier, u.friendlyName, u.email, u.profileImage, u.isArchive, u.isResetPassword')
            ->where('u.id = :id')
            ->setParameter('id', $criteria['userId']);
        return $qb->getQuery()->getScalarResult();
    }


    /**
     * Adding entities result as empty when is not found.
     * @param array $criteria
     * @return array
     */
    public function loginUser(array $criteria)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.company', 'c')
            ->leftJoin('u.driver', 'd')
            ->leftJoin('u.userRoles', 'ur')
            ->leftJoin('d.stations', 's')
            ->select('u.id AS userId, u.isArchive, u.isEnabled, u.chatIdentifier, u.friendlyName, u.email, u.profileImage, u.isArchive, u.isResetPassword')
            ->addSelect('c.id AS company')
            ->addSelect('d.id AS driverId')
            ->addSelect('ur.name AS role')
            ->addSelect('ur.id AS userRoleId')
            ->addSelect('s.name AS stationName')
            ->addSelect('s.id AS stationId')
            ->addSelect('s.code AS stationCode')
            ->andWhere('s.isArchive = 0')
            ->andWhere('ur.isArchive = 0')
            ->andWhere('u.id = :id')
            ->setParameter('id', $criteria['userId']);

        return $qb->getQuery()->getArrayResult();
    }


    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function userInformation(array $criteria)
    {
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.company', 'c')
            ->leftJoin('u.driver', 'd')
            ->leftJoin('u.userRoles', 'ur')
            ->leftJoin('d.stations', 's')
            ->select('u.id AS userId, u.isArchive, u.isEnabled, u.chatIdentifier, u.friendlyName, u.email, u.profileImage, u.isArchive, u.isResetPassword')
            ->addSelect('c.id AS company')
            ->addSelect('d.id AS driverId')
            ->addSelect('ur.name AS role')
            ->addSelect('s.name AS stationName')
            ->addSelect('s.id AS stationId')
            ->addSelect('s.code AS stationCode')
            ->andWhere('u.isArchive = 0')
            ->andWhere('d.isArchive = 0')
            ->andWhere('s.isArchive = 0')
            ->andWhere('ur.isArchive = 0')
            ->andWhere('u.id = :id')
            ->setParameter('id', $criteria['userId'])
            ->setMaxResults(1);
        $users = $qb->getQuery()->getArrayResult();
        if (sizeof($users) < 1) {
            throw new Exception('Users not found.');
        }
        $user = $users[0];
        $user['position'] = $this->globalUtility->getPosition($user['role']);

        $managers = $this->getManagersInChargeByStation($user['stationId']);

        $manager = null;
        foreach ($managers as $item) {
            if ($item['role'] === 'ROLE_STATION_MANAGER') {
                $manager = $item;
                break;
            }
            if ($item['role'] === 'ROLE_ASSISTANT_STATION_MANAGER') {
                $manager = $item;
                break;
            }
            if ($item['role'] === 'ROLE_LEAD_DRIVER') {
                $manager = $item;
                break;
            }
        }

        $station = [
            'id' => $user['stationId'],
            'name' => $user['stationName'],
            'code' => $user['stationCode'],
        ];

        $assignedManager = $this->getFirstAssignedManager($user['driverId'], false);

        return ['user' => $user, 'manager' => $manager, 'station' => $station, 'managers' => $managers, 'assignedManager' => $assignedManager];
    }

    /**
     * @param $driverId
     * @param bool $enableException
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     * @throws Exception
     */
    public function getFirstAssignedManager($driverId, bool $enableException)
    {
        $driver = $this->_em->find(Driver::class, $driverId);
        $assignedManager = null;
        if ($driver instanceof Driver) {
            $assignedManagers = $driver->getAssignedManagers();
            foreach ($assignedManagers as $item) {
                $archive = $item->getIsArchive();
                $chatIdentifier = $item->getChatIdentifier();
                if ($item instanceof User && $archive === false && is_string($chatIdentifier)) {
                    $assignedManager = [
                        'userId' => $item->getId(),
                        'chatIdentifier' => $chatIdentifier,
                        'friendlyName' => $item->getFriendlyName(),
                        'role' => $item->getRoleRawName(),
                    ];
                    break;
                }
            }
        }

        if ($enableException === false) {
            return $assignedManager;
        }

        if (!$assignedManager) {
            throw new Exception('Assigned manager not correctly set up for driver ID: ' . $driverId);
        }
        return $assignedManager;
    }

    /**
     * @param $stationId
     * @param bool $entity
     * @return array
     */
    public function getManagersInChargeByStation($stationId, bool $entity = false)
    {
        if ($entity) {
            return $this->createQueryBuilder('u')
                ->leftJoin('u.driver', 'd')
                ->leftJoin('u.userRoles', 'ur')
                ->leftJoin('d.stations', 's')
                ->andWhere('u.chatIdentifier IS NOT NULL')
                ->andWhere('u.isArchive = 0')
                ->andWhere('d.isArchive = 0')
                ->andWhere('s.isArchive = 0')
                ->andWhere('ur.isArchive = 0')
                ->andWhere('s.id = :station')
                ->setParameter('station', $stationId)
                ->andWhere('(ur.name LIKE :roleLeadDriver or ur.name LIKE :roleAssistantStationManager or ur.name LIKE :roleStationManager)')
                ->setParameter('roleLeadDriver', 'ROLE_LEAD_DRIVER')
                ->setParameter('roleAssistantStationManager', 'ROLE_ASSISTANT_STATION_MANAGER')
                ->setParameter('roleStationManager', 'ROLE_STATION_MANAGER')
                ->orderBy('ur.id')->getQuery()->getResult();
        }

        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.driver', 'd')
            ->leftJoin('u.userRoles', 'ur')
            ->leftJoin('d.stations', 's')
            ->select('u.id AS userId, u.chatIdentifier, u.friendlyName')
            ->addSelect('ur.name AS role')
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.isArchive = 0')
            ->andWhere('d.isArchive = 0')
            ->andWhere('s.isArchive = 0')
            ->andWhere('ur.isArchive = 0')
            ->andWhere('s.id = :station')
            ->setParameter('station', $stationId)
            ->andWhere('(ur.name LIKE :roleLeadDriver or ur.name LIKE :roleAssistantStationManager or ur.name LIKE :roleStationManager)')
            ->setParameter('roleLeadDriver', 'ROLE_LEAD_DRIVER')
            ->setParameter('roleAssistantStationManager', 'ROLE_ASSISTANT_STATION_MANAGER')
            ->setParameter('roleStationManager', 'ROLE_STATION_MANAGER')
            ->orderBy('ur.id');

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * Adding entities result as empty when is not found.
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function homeUser(array $criteria)
    {
        $date = new DateTime($criteria['date']);
        $qb = $this->createQueryBuilder('u')
            ->leftJoin('u.company', 'c')
            ->leftJoin('u.driver', 'd')
            ->leftJoin('d.driverRoutes', 'dr')
            ->leftJoin('dr.shiftType', 'shift_type')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date)
            ->orderBy('shift_type.startTime', 'ASC')
            ->select('u.id AS userId, u.isArchive, u.isEnabled, u.friendlyName, u.profileImage, u.isArchive, u.isResetPassword')
            ->addSelect('dr.id as driverRouteId')
            ->andWhere('u.id = :id')
            ->setParameter('id', $criteria['userId']);

        return $qb->getQuery()->getArrayResult();
    }

    /**
     * @param array $criteria
     * @return User[]
     */
    public function getUserWithNullNotificationSid(array $criteria)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.notificationsChannelSid IS NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company'])
            ->getQuery()->getResult();
    }

    /**
     * @param int|string $userId
     * @param bool $all
     * @return Task[]|array|ArrayCollection|Collection
     * @throws Exception
     */
    public function getTasksByUser($userId, bool $all = false)
    {
        $user = $this->find($userId);
        if (!($user instanceof User)) {
            throw new Exception('User not found by userId: ' . $userId);
        }
        $tasks = $user->getTasks();
        if ($tasks->count() === 0) {
            return [];
        }
        if ($all) {
            return $tasks;
        }
        $filterTasks = new ArrayCollection();
        foreach ($tasks as $task) {
            $isArchive = $task->getIsArchive();
            $completed = $task->getCompleted();
            if ($isArchive === false && $completed === false) {
                $filterTasks->add($task);
            }
        }
        return $filterTasks;
    }
}
