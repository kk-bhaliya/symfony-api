<?php

namespace App\Repository;

use App\Entity\IncidentFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IncidentFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentFile[]    findAll()
 * @method IncidentFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentFile::class);
    }
}
