<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\Rate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Rate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Rate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Rate[]    findAll()
 * @method Rate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Rate::class);
    }

    public function getRateByStation($criteria)
    {
        return $this->createQueryBuilder('r')
                    ->where('r.station = :station')
                    ->setParameter('station', $criteria['station'] )
                    ->addCriteria(Common::notArchived('r'))
                    ->getQuery()
                    ->getResult();
    }

    public function getRateIdByStation($stationId)
    {
        $results = $this->createQueryBuilder('r')
                         ->select('r.id')
                         ->andWhere('r.station = :station')
                         ->setParameter('station', $stationId )
                         ->andWhere('r.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->getQuery()
                         ->getResult(Query::HYDRATE_ARRAY);


        return array_column($results, 'id');

    }
    public function updateStationIsArchiveInRate($stationId)
    {

        $rateIds = $this->getRateIdByStation($stationId);
        if (!empty($rateIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\Rate','r')
                            ->set('r.isArchive', true)
                            ->where($qb->expr()->in('r.id', ':rateId'))
                            ->setParameter( 'rateId', $rateIds )
                            ->getQuery()
                            ->execute();

            //Call Invoice Type rate id null set
            $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->updateRateNullInInvoiceType($rateIds);
        }

        return true;
    }
    public function getRateImportData($name, $station, $criteria, $effectiveRate, $cumulativeRate, $company)
    {
        $em = $this->getEntityManager();

        $rate = new Rate();
        $rate->setName($name);
        $rate->setStation($station);
        $rate->setCriteria($criteria);
        $rate->setEffectiveRate($effectiveRate);
        $rate->setCumulativeRate($cumulativeRate);
        $rate->setCompany($company);
        $em->persist($rate);
        $em->flush();

        return $rate;
    }
}
