<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\DriverSkill;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\ReturnToStation;
use App\Entity\RouteRequests;
use App\Entity\Schedule;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Entity\Incident;
use App\Entity\DriverRouteLog;
use App\Services\BotService;
use App\Services\UserService;
use App\Utils\EventEmitter;
use App\Utils\GlobalUtility;
use App\Services\AmazonS3Service;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @method DriverRoute|null find($id, $lockMode = null, $lockVersion = null)
 * @method DriverRoute|null findOneBy(array $criteria, array $orderBy = null)
 * @method DriverRoute[]    findAll()
 * @method DriverRoute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DriverRouteRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $em;
    private $kernel;
    private $params;
    private $globalUtils;
    private $eventEmitter;
    protected $s3;
    private $stopwatch;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var BotService
     */
    private $botService;


    /**
     * DriverRouteRepository constructor.
     * @param ManagerRegistry $registry
     * @param TokenStorageInterface $tokenStorage
     * @param EntityManagerInterface $em
     * @param KernelInterface $kernel
     * @param ContainerBagInterface $params
     * @param EventEmitter $eventEmitter
     * @param GlobalUtility $globalUtils
     * @param AmazonS3Service $s3
     * @param Stopwatch $stopwatch
     * @param UserService $userService
     * @param BotService $botService
     */
    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $em,
        KernelInterface $kernel,
        ContainerBagInterface $params,
        EventEmitter $eventEmitter,
        GlobalUtility $globalUtils,
        AmazonS3Service $s3,
        Stopwatch $stopwatch,
        UserService $userService,
        BotService $botService
    )
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, DriverRoute::class);
        $this->em = $em;
        $this->kernel = $kernel;
        $this->params = $params;
        $this->eventEmitter = $eventEmitter;
        $this->globalUtils = $globalUtils;
        $this->s3 = $s3;
        $this->stopwatch = $stopwatch;
        $this->userService = $userService;
        $this->botService = $botService;
    }


    /**
     * @param array $criteria
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    public function currentMileageAndGas(array $criteria)
    {
        $driverRoute = $this->_em->find(DriverRoute::class, $criteria['driverRoute']['id']);
        $vehicle = $driverRoute->getVehicle();

        if ($vehicle instanceof Vehicle) {
            return [
                'success' => true,
                'vehicle' => [
                    'id' => $vehicle->getId(),
                    'currentMileage' => $vehicle->getCurrentMileage(),
                    'currentGasLevel' => $vehicle->getCurrentGasTankLevel(),
                ]
            ];
        }

        return [
            'success' => false,
            'driverRoute' => [
                'id' => $criteria['driverRoute']['id']
            ],
        ];
    }

    public function validateVan(array $criteria)
    {
        $driverRoute = $this->em->find(DriverRoute::class, $criteria['driverRouteId']);
        if (!($driverRoute instanceof DriverRoute)) {
            return ['success' => false, 'message' => 'Driver Route not found'];
        }

        $vehicle = $this->em->getRepository(Vehicle::class)->findOneBy(['vin' => $criteria['vin'], 'isArchive' => false]);
        if ($vehicle instanceof Vehicle) {
            try {
                if (!empty($criteria['scannedQRCode'])) {
                    $driverRoute->getKickoffLog()->setScannedQRCode($criteria['scannedQRCode']);
                }
                if (!empty($criteria['scannedQRCodeAt'])) {
                    $date = new DateTime($criteria['scannedQRCodeAt']);
                    $driverRoute->getKickoffLog()->setScannedQRCodeAt($date);
                }
                $driverRoute->getKickoffLog()->setVehicle($vehicle);
                $driverRoute->setVehicle($vehicle);
                $this->em->flush();
                return ['success' => true];
            } catch (Exception $exception) {
                return ['success' => false, 'message' => $exception->getMessage()];
            }
        }

        $vehicle = $this->em->getRepository(Vehicle::class)
            ->findOneBy(['vehicleId' => $criteria['vehicleId'], 'company' => $criteria['companyId'], 'isArchive' => false]);
        if ($vehicle instanceof Vehicle) {
            try {
                if (!empty($criteria['scannedQRCode'])) {
                    $driverRoute->getKickoffLog()->setScannedQRCode($criteria['scannedQRCode']);
                }
                if (!empty($criteria['scannedQRCodeAt'])) {
                    $date = new DateTime($criteria['scannedQRCodeAt']);
                    $driverRoute->getKickoffLog()->setScannedQRCodeAt($date);
                }
                $driverRoute->getKickoffLog()->setVehicle($vehicle);
                $driverRoute->setVehicle($vehicle);
                $this->em->flush();
                return ['success' => true];
            } catch (Exception $exception) {
                return ['success' => false, 'message' => $exception->getMessage()];
            }
        }

        return ['success' => false, 'message' => 'Vehicle not found'];
    }

    public function dropOpenRouteByDriverRoute($criteria)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', 1)
            ->where('dr.id = :routeId')
            ->setParameter('routeId', $criteria['routeId'])
            ->getQuery()
            ->execute();

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', 1)
            ->where('tdr.routeId = :routeId')
            ->setParameter('routeId', $criteria['routeId'])
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->execute();
        return true;
    }

    /**
     * @param $driverId
     * @param string $timezone
     * @param integer|null $maxResults
     * @param bool|null $driverRouteId
     * @return DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutes($driverId, $timezone = 'America/Detroit', $maxResults = null, $driverRouteId = false)
    {
        $today = new DateTime('now', new DateTimeZone($timezone));
        $qb = $this->createQueryBuilder('dr')
            ->where('dr.driver = :driver')
            ->setParameter('driver', $driverId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated <= :today')
            ->setParameter('today', $today)
            ->orderBy('dr.dateCreated', 'DESC')
            ->setMaxResults($maxResults);

        if ($driverRouteId) {
            $qb->andWhere('dr.id = :drid')
                ->setParameter('drid', $driverRouteId);
        }

        return $qb->getQuery()->getResult();
    }

    public function getLoadoutTodayDriverRoutes($driverId, $date)
    {
        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.driverRouteCodes', 'rc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.driver', 'd')
            ->leftJoin('d.user', 'u')
            ->where('rc.id IS NULL')
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $driverId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('d.isArchive = :isArchiveDriver')
            ->setParameter('isArchiveDriver', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.dateCreated = :today')
            ->setParameter('today', $date)
            ->orderBy('dr.dateCreated', 'DESC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $driverId
     * @param string $timezone
     * @return DriverRoute|null
     */
    public function getTodayDriverRoutes($driverId, $timezone = 'America/Detroit')
    {
        /** @var Collection|DriverRoute[] $driverRoutes */
        $driverRoutes = $this->getDriverRoutes($driverId, $timezone);
        if (is_null($driverRoutes)) {
            return null;
        }
        if (sizeof($driverRoutes) === 0) {
            return null;
        }
        return $driverRoutes[0];
    }

    public function getTodayDriverRouteStationTimeData($companyId, $currentDate, $station = '')
    {
        $qb = $this->createQueryBuilder('dr')
            ->select('st.id,st.name')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('(dr.driver IS NOT NULL and dr.isOpenShift = 0) or (dr.driver IS NULL and tdr.driver IS NOT NULL and tdr.isNew = 1 and dr.isOpenShift = 1 and tdr.isArchive = 0)')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('dr.isRescuer = :isRescuer')
            ->setParameter('isRescuer', false)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        return $qb->getQuery()->getScalarResult();

    }

    public function getReplaceDriverRouteDriverListing($criteria)
    {
        $station = $criteria['stationId'];

        $routeId = $criteria['routeId'];
        $currentDate = $datetime = isset($criteria['date']) ? $criteria['date'] : date('Y-m-d');
        $checkIncident = isset($criteria['checkIncident']) ? $criteria['checkIncident'] : false;

        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($routeId);
        $driverId = !empty($driverRoute->getDriver()) ? $driverRoute->getDriver()->getId() : '';

        $userLogin = $this->tokenStorage->getToken()->getUser();

        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = (new \DateTime($currentDate));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');

        $qb2 = $this->getEntityManager()->createQueryBuilder();

        $qb2->select('drv.id')
            ->from('App\Entity\Incident', 'i')
            ->leftJoin('i.driver', 'drv')
            ->where('i.isArchive = :isArchiveIncident')
            ->andWhere('i.dateTime >= :startDate')
            ->andWhere('i.dateTime <= :endDate')
            ->setParameter('isArchiveIncident', false)
            ->setParameter('startDate', $currentDate . ' 00:00:00')
            ->setParameter('endDate', $currentDate . ' 23:59:59');

        if (!empty($station)) {
            $qb2->andWhere('i.station = :station')
                ->setParameter('station', $station);
        }
        $driverWithIncidentResult = $qb2->getQuery()->getResult();
        $driverWithIncident = !empty($driverWithIncidentResult) ? array_column($driverWithIncidentResult, 'id') : [];

        $qb = $this->createQueryBuilder('dr');
        $qb->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'd')
            ->leftJoin('d.user', 'u')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('d.isArchive = :isArchiveDriver')
            ->setParameter('isArchiveDriver', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere($qb->expr()->notIn('dr.driver', ':driverId'))
            ->setParameter('driverId', $driverId)
            ->andWhere('dr.startTime IS NOT NULL')
            ->groupBy('dr.driver')
            ->orderBy('u.friendlyName', 'ASC');


        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        $newQb = $qb;
        if (!empty($startTime) && !empty($endTime)) {
            $qb->andWhere('dr.startTime >= :shiftStartTime')
                ->andWhere('dr.startTime < :shiftEndTime')
                ->setParameter('shiftStartTime', $startTime)
                ->setParameter('shiftEndTime', $endTime);
        }

        $newdriversResults = $newQb->getQuery()->getResult();
        $driversResults = $qb->getQuery()->getResult();
        $driversData = [];
        $driversIds = [];

        $counts = 0;
        foreach ($newdriversResults as $driversResult) {
            if (!empty($driversResult->getDriver()) && !in_array($driversResult->getDriver()->getId(), $driverWithIncident)) {
                array_push($driversIds, $driversResult->getDriver()->getId());
            }
        }

        foreach ($driversResults as $driversResult) {
            if (!empty($driversResult->getDriver()) && !in_array($driversResult->getDriver()->getId(), $driverWithIncident)) {
                // array_push($driversIds, $driversResult->getDriver()->getId());

                // if(strtotime(date('Y-m-d').' '.$driversResult->getStartTime()->format('H:i:s')) >= strtotime(date('Y-m-d').' '.$startTime) && strtotime(date('Y-m-d').' '.$driversResult->getStartTime()->format('H:i:s')) < strtotime(date('Y-m-d').' '.$endTime)){
                //if(strtotime($driversResult->getStartTime()->format('H:i')) >= strtotime($startTime) && strtotime($driversResult->getStartTime()->format('H:i')) < strtotime($endTime)){

                if (($driversResult->getDriver()->getInactiveAt() && $driversResult->getDriver()->getInactiveAt()->format('Y-m-d') > $currentDate) || ($driversResult->getDriver()->getTerminationDate() && $driversResult->getDriver()->getTerminationDate()->format('Y-m-d') > $currentDate) || ($driversResult->getDriver()->getOffboardedDate() && $driversResult->getDriver()->getOffboardedDate()->format('Y-m-d') > $currentDate)) {

                    if (count($driversResult->getDriverRouteCodes()) < 1) {
                        if ($counts == 0) {
                            $driversData[] = [
                                'name' => 'Unassigned Drivers',
                            ];
                        }
                        $driversData[] = [
                            'id' => $driversResult->getDriver()->getId(),
                            'name' => $driversResult->getDriver()->getFullName(),
                            'title' => $driversResult->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                            'img' => $driversResult->getDriver()->getUser()->getProfileImage(),
                            'routeId' => $driversResult->getId(),
                        ];
                        $counts++;
                    }
                } else {
                    if (count($driversResult->getDriverRouteCodes()) < 1) {
                        if ($counts == 0) {
                            $driversData[] = [
                                'name' => 'Unassigned Drivers',
                            ];
                        }
                        $driversData[] = [
                            'id' => $driversResult->getDriver()->getId(),
                            'name' => $driversResult->getDriver()->getFullName(),
                            'title' => $driversResult->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                            'img' => $driversResult->getDriver()->getUser()->getProfileImage(),
                            'routeId' => $driversResult->getId(),
                        ];
                        $counts++;
                    }
                }
                // }
            }
        }

        $counts = 0;
        $qb3 = $this->getEntityManager()->createQueryBuilder();
        $unassignDrivers = $this->em->getRepository('App\Entity\Driver')->getDriversForAsignRoute($driversIds, $station, $driverWithIncident);
        foreach ($unassignDrivers as $unassignDriver) {
            if ($driverId != $unassignDriver->getId()) {
                if ($counts == 0) {
                    $driversData[] = [
                        'name' => 'Unscheduled Drivers',
                    ];
                }
                $driversData[] = [
                    'id' => $unassignDriver->getId(),
                    'name' => $unassignDriver->getFullName(),
                    'title' => $unassignDriver->getUser()->getUserRoles()[0]->getRoleName(),
                    'img' => $unassignDriver->getUser()->getProfileImage(),
                    'routeId' => '',
                ];
                $counts++;
            }
        }

        return $driversData;

    }

    public function getTodayDriverRouteRescuereListingData($userLogin, $currentDate, $station, $startTime = '', $endTime = '', $tempOpenShiftRoutesIdArr = [])
    {

        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->leftJoin('dr.driver', 'dri')
            ->leftJoin('tdr.driver', 'tdri')
            ->leftJoin('dri.user', 'u')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('(dr.driver IS NOT NULL and dr.isOpenShift = 0 and dri.isEnabled = 1 and dri.isArchive = false) or (tdri.isArchive = false and tdri.isEnabled = 1 and dr.driver IS NULL and tdr.driver IS NOT NULL and tdr.isNew = 1 and dr.isOpenShift = 1 and tdr.isArchive = 0)')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('dr.isRescuer = :isRescuer')
            ->setParameter('isRescuer', false)
            ->andWhere('dr.isSecondShift != :isSecShift')
            ->setParameter('isSecShift', 1)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL')
            ->orderBy('u.friendlyName', 'ASC');

        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        if (!empty($startTime) && !empty($endTime)) {
            $qb->andWhere('(case when dr.startTime is null then sf.startTime else dr.startTime end) >= :shiftStartTime')
                ->andWhere('(case when dr.startTime is null then sf.startTime else dr.startTime end) < :shiftEndTime')
                ->setParameter('shiftStartTime', $startTime)
                ->setParameter('shiftEndTime', $endTime);
        }

        if (count($tempOpenShiftRoutesIdArr) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':tempOpenShiftRoutesIdArr'))
                ->setParameter('tempOpenShiftRoutesIdArr', $tempOpenShiftRoutesIdArr);
        }
        return $qb->getQuery()->getResult();
    }

    public function getTodayDriverRouteRescuereListingDataNew($companyId, $currentDate, $station, $startTime = '', $endTime = '', $tempOpenShiftRoutesIdArr = [], $driverRouteIsRescuerFalseIdArr = [], $isRescuer = false)
    {
        $tempdriverRouteIds = '';
        if (count($tempOpenShiftRoutesIdArr) > 0) {
            $routeIds = implode("','",$tempOpenShiftRoutesIdArr);
            $tempdriverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
        }
        $driverRouteIds = '';
        if($isRescuer == true){
            $driverRouteIds = ' AND dr.is_rescuer = 1';
            if (count($driverRouteIsRescuerFalseIdArr) > 0) {
                $routeIds = implode("','",$driverRouteIsRescuerFalseIdArr);
                $driverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
            }
        } else {
            $driverRouteIds = ' AND dr.is_rescuer = 0';
        }

        $sqlQuery = "SELECT
        dr.id as id,
        dr.station_id as stationId,
        st.timezone as station_timezone,
        dr.start_time as startTime,
        dr.end_time as endTime,
        dr.punch_in as punchIn,
        dr.punch_out as punchOut,
        dr.break_punch_in as breakPunchIn,
        dr.break_punch_out as breakPunchOut,
        dr.is_rescuer as is_rescuer,
        dr.shift_invoice_type_id as shift_invoice_type_id,
        dr.shift_station_id as shift_station_id,
        dr.shift_skill_id as shift_skill_id,
        dr.shift_balance_group_id as shift_balance_group_id,
        dr.shift_name as dr_shift_name,
        dr.shift_color as dr_shift_color,
        dr.unpaid_break as unpaid_break,
        dr.shift_note as shift_note,
        dr.shift_category as dr_shift_category,
        dr.shift_type_id as shift_type_id,
        dri.id as driverId,
        u.id as user_id,
        u.friendly_name as user_friendlyName,
        u.first_name as user_firstName,
        u.last_name as user_lastName,
        u.profile_image as user_profileImage,
        u.friendly_name as friendly_name,
        u.email as user_email,
        dr.schedule_id as scheduleId,
        sf.id as shift_id,
        sf.category as shift_category,
        sf.name as shift_name,
        sf.color as shift_color,
        sf.text_color as shift_textColor,
        sf.note as note,
        sf.start_time as shiftStartTime,
        sf.end_time as shiftEndTime,
        sf.balance_group_id as shift_balance_group,
        sf.invoice_type_id as invoice_type_id,
        odr.driver_id as oldDriverId,
        odr.id as oldDriverRoute,
        odr.is_backup as isBackup,
        odr.is_second_shift as isSecondShift,
        odrShift.color as odrShiftColor,
        odrUser.first_name as old_route_user_firstName,
        odrUser.last_name as old_route_user_lastName,
        odrUser.friendly_name as old_user_friendly_name,
        tdr.is_new as isNew,
        dr.driver_id as driverId,
        sf.balance_group_id as balanceGroupId,
        dr.datetime_started as datetimeStarted,
        dr.datetime_ended as datetimeEnded,
        dr.is_add_train as isAddTrain,
        dr.is_light_duty as isLightDuty,
        dr.date_created as dateCreated,
        dr.has_bag as hasBag,
        dri.employee_status as employeeStatus,
        kick.station_arrival_at as stationArrivalAt,
        kick.id as kickoffLog,
        tdr.is_new as isNew,
        v.vehicle_id as VehicleUnit,
        v.id as VehicleId,
        skill.id as skill_id,
        skill.name as skill_name,
        sh.id as schedule_id,
        sh.name as schedule_name,
        (SELECT GROUP_CONCAT(skill.id) FROM skill LEFT JOIN driver_skill ON driver_skill.skill_id = skill.id WHERE skill.is_archive = 0  AND driver_skill.driver_id = dri.id) as skillId,
        (SELECT GROUP_CONCAT(driver_route_code.code) FROM driver_route_code LEFT JOIN driver_route_driver_route_code ON driver_route_driver_route_code.driver_route_code_id = driver_route_code.id WHERE driver_route_code.is_archive = 0  AND driver_route_driver_route_code. 	driver_route_id = dr.id) as routeCode,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
        FROM
            driver_route dr
        LEFT JOIN
            temp_driver_route tdr
            ON
            dr.id = tdr.route_id_id
        LEFT JOIN
            shift sf
            ON
            sf.id = dr.shift_type_id
        LEFT JOIN
            driver dri
            ON
            dri.id = dr.driver_id
        LEFT JOIN
            driver tdri
            ON
            tdri.id = tdr.driver_id
        LEFT JOIN
            user u
            ON
            u.id = dri.user_id
        LEFT JOIN
            station st
            ON
            st.id = dr.station_id

        LEFT JOIN
            kickoff_log kick
            ON
            kick.id = dr.kickoff_log_id
        LEFT JOIN
            driver_route odr
            ON
            odr.id = dr.old_driver_route_id
        LEFT JOIN
            shift odrShift
            ON
            odrShift.id = odr.shift_type_id

        LEFT JOIN
            driver odrDriver
            ON
            odrDriver.id = odr.driver_id
        LEFT JOIN
            user odrUser
            ON
            odrUser.id = odrDriver.user_id
        LEFT JOIN
            vehicle v
            ON
            v.id = dr.vehicle_id
        LEFT JOIN
            skill skill
            ON
            skill.id = dr.skill_id
        LEFT JOIN
            schedule sh
            ON
            sh.id = dr.schedule_id
        WHERE dr.date_created = '".$currentDate."'
        AND dr.station_id = ".$station." AND sf.company_id = ".$companyId." AND dr.is_archive = 0 AND dr.is_second_shift = 0 AND dr.is_active = 1 AND st.is_archive = 0
        AND ((dr.driver_id IS NOT NULL AND dr.is_open_shift = 0 AND dri.is_enabled = 1 AND dri.is_archive = 0)
        OR
        (tdri.is_archive = 0 AND tdri.is_enabled = 1 AND dr.driver_id IS NULL AND tdr.driver_id IS NOT NULL AND tdr.is_new = 1 AND dr.is_open_shift = 1 AND tdr.is_archive = 0))".$tempdriverRouteIds.$driverRouteIds.' GROUP BY dr.id order by friendly_name ASC';
        $driverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $driverRouteData;
    }

    public function getTodayDriverRouteAllListingData($userLogin, $currentDate, $station, $startTime = '', $endTime = '', $tempOpenShiftRoutesIdArr = [], $driverRouteIsRescuerFalseIdArr = [])
    {

       /* $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->leftJoin('dr.driver', 'dri')
            ->leftJoin('tdr.driver', 'tdri')
            ->leftJoin('dri.user', 'u')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('(dr.driver IS NOT NULL and dr.isOpenShift = 0 and dri.isEnabled = 1 and dri.isArchive = false) or (tdri.isArchive = false and tdri.isEnabled = 1 and dr.driver IS NULL and tdr.driver IS NOT NULL and tdr.isNew = 1 and dr.isOpenShift = 1 and tdr.isArchive = 0)')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('dr.isRescuer = :isRescuer')
            ->setParameter('isRescuer', true)
            ->andWhere('dr.isSecondShift != :isSecondShift')
            ->setParameter('isSecondShift', true)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL')
            ->orderBy('u.friendlyName', 'ASC');

        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        if (count($tempOpenShiftRoutesIdArr) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':tempOpenShiftRoutesIdArr'))
                ->setParameter('tempOpenShiftRoutesIdArr', $tempOpenShiftRoutesIdArr);
        }

        if (count($driverRouteIsRescuerFalseIdArr) > 0) {
            $qb->andWhere($qb->expr()->In('dr.oldDriverRoute', ':oldDriverRoute'))
                ->setParameter('oldDriverRoute', $driverRouteIsRescuerFalseIdArr);
        }

        return $qb->getQuery()->getResult(); */
    }

    public function getTodayOpenDriverRoutePopupListingData($companyId, $currentDate, $station, $startTime, $endTime, $tempOpenShiftDriverRoutesIdArr)
    {
        $qb = $this->createQueryBuilder('dr')
            ->select('dr.id as id')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('dr.driver IS NULL')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        if (count($tempOpenShiftDriverRoutesIdArr) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':tempOpenShiftDriverRoutesIdArr'))
                ->setParameter('tempOpenShiftDriverRoutesIdArr', $tempOpenShiftDriverRoutesIdArr);
        }

        return $qb->getQuery()->getScalarResult();
    }

    public function getTodayOpenDriverRoutePopupListingDataNew($companyId, $currentDate, $station, $startTime, $endTime, $tempOpenShiftDriverRoutesIdArr)
    {
        $driverRouteIds = '';
        if (count($tempOpenShiftDriverRoutesIdArr) > 0) {
            $routeIds = implode("','",$tempOpenShiftDriverRoutesIdArr);
            $driverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
        }

        $sqlQuery = "SELECT
        dr.id as routeId,
        sf.id as shift_id,
        sf.category as shift_category,
        sf.name as shift_name,
        sf.color as shift_color,
        sf.start_time as shiftStartTime,
        dr.request_for_route_id as request_for_route_id,
        rr.approved_by_id,
        rr.is_request_completed,
        (SELECT GROUP_CONCAT(driver_route_code.code) FROM driver_route_code LEFT JOIN driver_route_driver_route_code ON driver_route_driver_route_code.driver_route_code_id = driver_route_code.id WHERE driver_route_code.is_archive = 0  AND driver_route_driver_route_code. 	driver_route_id = dr.id) as routeCode
        FROM
            driver_route dr
        LEFT JOIN
            temp_driver_route tdr
            ON
            dr.id = tdr.route_id_id
        LEFT JOIN
            shift sf
            ON
            sf.id = dr.shift_type_id
        LEFT JOIN
            station st
            ON
            st.id = dr.station_id
        LEFT JOIN
            route_requests rr
            ON
            rr.id = dr.request_for_route_id

        WHERE dr.date_created = '".$currentDate."' AND dr.station_id = ".$station." AND sf.company_id = ".$companyId." AND dr.is_archive = 0 AND sf.is_archive = 0 AND st.is_archive = 0 AND dr.is_active = 1 AND dr.driver_id IS NULL AND sf.start_time IS NOT NULL ".$driverRouteIds;
        $driverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $driverRouteData;
    }

    public function getTodayDriverRouteByShift($companyId, $currentDate, $station, $startTime, $endTime, $shiftId, $isDriver = true)
    {
        $qb = $this->createQueryBuilder('dr')
            ->select('count(dr.id)')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.shiftType = :shiftId')
            ->setParameter('shiftId', $shiftId)
            ->andWhere('dr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        if (!empty($startTime) && !empty($endTime)) {
            $qb->andWhere('dr.startTime >= :shiftStartTime')
                ->andWhere('dr.startTime < :shiftEndTime')
                ->setParameter('shiftStartTime', $startTime)
                ->setParameter('shiftEndTime', $endTime);
        }
        if ($isDriver) {
            $qb->andWhere('dr.driver IS NOT NULL');
        }

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getSchedularLoadOutShiftStartEndTimeData($userLogin, $currentDate, $station, $requestTimeZone)
    {
        $topFilterTime = [];
        //Get end time logic
        $driverRouteShiftRecords = $this->getTodayDriverRouteStationTimeData($userLogin, $currentDate, $station);
        foreach ($driverRouteShiftRecords as $driverRouteRecord) {
            //Get shift time for drop down
            $timeObj = $this->globalUtils->getTimeZoneConversation($driverRouteRecord->getStartTime(), $requestTimeZone);
            $topFilterTime[$timeObj->format('H')] = [
                'value' => (int)$timeObj->format('H'),
                'name' => rtrim($timeObj->format('ha'), 'm'),
            ];
        }
        if (!empty($topFilterTime)) {
            $topFilterTime = array_values($topFilterTime);
            //    $topFilterTime[0] = array_merge($topFilterTime[0],['selected' => true]);
            $filterName = array_column($topFilterTime, 'value');
            array_multisort($filterName, SORT_ASC, $topFilterTime);
        }
        return $topFilterTime;
    }

    public function getSchedularLoadOutFilterData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = isset($criteria['date']) ? (new \DateTime($criteria['date'])) : (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');
        $companyId =  $userLogin->getCompany()->getId();
        $driverRouteRecords = $this->getTodayDriverRouteStationTimeData($companyId, $currentDate);


        /*****STEP 1 filter data station and shift start time******/
        $filterAllArray = $topFilterStations = $topFilterTime = [];
        foreach ($driverRouteRecords as $driverRouteRecord) {
            //Get top filter dropdown stations
            if (isset($driverRouteRecord['id'])) {
                $topFilterStations[$driverRouteRecord['id']] = [
                    'value' => $driverRouteRecord['id'],
                    'name' => $driverRouteRecord['name'],
                ];
            }
        }


        $stationId = '';
        $filterSchedule = $filterSkill = $filterShift = $listingsDriverRoutes = [];
        if (!empty($topFilterStations)) {
            $topFilterStations = array_values($topFilterStations);
            // $topFilterStations[0] = array_merge($topFilterStations[0],['selected' => true]);
            $stationId = $topFilterStations[0]['value'];
        }

        /*****STEP 2 filter data station,shift,skill,driver******/
        $station = !empty($criteria['stationId']) ? $criteria['stationId'] : $stationId;

        //$topFilterTime = $this->getSchedularLoadOutShiftStartEndTimeData($userLogin, $currentDate, $station, $requestTimeZone);

        $driverRouteIsRescuerFalseIdArr = [];
        $driverRouteDataByParams = $this->getTodayDriverRouteRescuereListingDataNew($companyId, $currentDate, $station);


        foreach ($driverRouteDataByParams as $driverRouteDataByParam) {
            array_push($driverRouteIsRescuerFalseIdArr, $driverRouteDataByParam['id']);

            if (!empty($driverRouteDataByParam['shift_id'])) {
                $filterShift[$driverRouteDataByParam['shift_id']] = [
                    'value' => $driverRouteDataByParam['shift_id'],
                    'name' => $driverRouteDataByParam['shift_name'],
                    'color' => $driverRouteDataByParam['shift_color'],
                ];
            }
            if (!empty($driverRouteDataByParam['skill_id'])) {
                $filterSkill[$driverRouteDataByParam['skill_id']] = [
                    'value' => $driverRouteDataByParam['skill_id'],
                    'name' => $driverRouteDataByParam['skill_name'],
                ];
            }
            if (!empty($driverRouteDataByParam['schedule_id'])) {
                $filterSchedule[$driverRouteDataByParam['schedule_id']] = [
                    'value' => $driverRouteDataByParam['schedule_id'],
                    'name' => $driverRouteDataByParam['schedule_name'],
                ];
            }
        }

        $filterAllArray['stations'] = $topFilterStations;
       // $filterAllArray['startTimes'] = $topFilterTime;

        $filterSchedule = array_values($filterSchedule);
        usort($filterSchedule, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        $filterAllArray['schedules'] = $filterSchedule;

        $filterShift = array_values($filterShift);
        usort($filterShift, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        $filterAllArray['shiftTypes'] = $filterShift;

        $filterSkill = array_values($filterSkill);
        usort($filterSkill, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });
        $filterAllArray['skills'] = $filterSkill;
        return $filterAllArray;
    }

    public function getRescuedRouteByRouteCodeAndDate($routeDate, $routeCode = [])
    {
        $qb = $this->createQueryBuilder('dr')
            ->select('sf.id as shift_id,sf.name as shift_name,sf.color as shift_color,sf.category as shift_category,dri.id as driver_id')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.driver', 'dri')
            ->where('dr.dateCreated = :dateCreated')
            ->setParameter('dateCreated', $routeDate)
            ->andWhere('dr.isRescued = :isRescued')
            ->setParameter('isRescued', true)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        if (!empty($routeCode)) {
            $qb->andWhere($qb->expr()->in('drc.code', ':codeArray'))
                ->setParameter('codeArray', $routeCode);
        }
        return $qb->distinct()->setMaxResults(1)->getQuery()->getScalarResult();

    }

    public function getSchedularLoadOutListingData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = isset($criteria['date']) ? (new \DateTime($criteria['date'])) : (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');
        $station = $criteria['stationId'];
        $companyId = $userLogin->getCompany()->getId();
        //$startTime = $endTime = $startTimeData = '';

        //Open shift driver route data
        $tempOpenShiftRoutesIdArr = [];
        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTodayLoadOutTempDriverRouteDriverNullDataNew($companyId, $currentDate, $station);
        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute['id'];
        }
        //Get temp driver route data
        $tempRoutesData = $openShiftTempDriverData = $changes = [];

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTodayLoadOutTempDriverRouteDriverNotNullDataNew($companyId, $currentDate, $station);

        foreach ($tempDriverRoutes as $tempDriverRoute) {
            $changeDate = new \DateTime($tempDriverRoute['dateCreated']);
            $tempRoutesData[$tempDriverRoute['routeId']] = $tempDriverRoute;
            $openShiftTempDriverData[] = $tempDriverRoute['routeId'];
            $changes[] = $changeDate->format('U') * 1000;
        }

        $driverRoutesListings = $listingsDriverRoutes = $shiftBoxListings = [];

        $driverRouteDataByParams = $this->getTodayDriverRouteRescuereListingDataNew($companyId, $currentDate, $station, $startTime = null, $endTime = null, $tempOpenShiftRoutesIdArr);

        $nCount = $nNotPunchInCount = $nNotDepartedCount = 0;
        $driverRouteIsRescuerFalseIdArr = $driverArray = $shiftArray = [];
        $mustLeaveUntilTime = false;
        $assignRoutes['drivers'] = $assignRoutes['routes'] = [];
        $unAssignRoutes = [];
        $shiftArray = [];
        $cotexRoute = 0;
        $tempCounts = 0;
        foreach ($driverRouteDataByParams as $driverRouteDataByParam) {
            array_push($driverRouteIsRescuerFalseIdArr, $driverRouteDataByParam['id']);

            $driverRouteDataByParam = isset($tempRoutesData[$driverRouteDataByParam['id']]) ? $tempRoutesData[$driverRouteDataByParam['id']] : $driverRouteDataByParam;
           /*$incident = $driverRouteDataByParam->getIncidents()->filter(function (Incident $item) {
                return $item->getIsArchive() === false;
            });
            //if (!empty($incident->first())) continue;*/

            list($payload, $nNotDepartedCount, $nNotPunchInCount) = $this->globalUtils->getLoadoutPayloadDataNew($driverRouteDataByParam, $nCount, $nNotDepartedCount, $nNotPunchInCount, $requestTimeZone);

            $listingsDriverRoutes[] = $payload;

            if (!empty($payload)) {
                if (!empty($payload['routes'])) {
                    $assignRoutes['drivers'][] = $payload['driverId'];
                    $assignRoutes['routes'] = isset($assignRoutes['routes']) ? array_merge($assignRoutes['routes'], $payload['routes']) : $payload['routes'];
                } else {
                    $unAssignRoutes['drivers'][] = $payload['driverId'];
                }
            }
            $nCount++;

            //Loadout remove starttime
            $stTime = new \DateTime($driverRouteDataByParam['startTime']);
            $shiftArray[$driverRouteDataByParam['shift_type_id']] = [
                "time" => rtrim($stTime->format('h:ia'), 'm'),
                "pickerTime" => rtrim($stTime->format('H:i'), 'm'),
                //'endTime' => rtrim($driverRouteDataByParam->getEndTime()->format('H:i'), 'm'),
                "invoiceType" => !empty($driverRouteDataByParam['invoice_type_id']) ? $driverRouteDataByParam['invoice_type_id'] : null,
            ];



            $key = (count($payload['routes']) > 0) ? 'Assigned' : 'Unassigned';
            if(!isset($driverArray[$key][$driverRouteDataByParam['shift_type_id']])){
                $driverArray[$key][$driverRouteDataByParam['shift_type_id']] = 0;
            }
            if( count($payload['routes']) > 0 ) {
                $driverArray[$key][$driverRouteDataByParam['shift_type_id']] = $driverArray[$key][$driverRouteDataByParam['shift_type_id']] + 1;
            } else {
                $driverArray[$key][$driverRouteDataByParam['shift_type_id']] = $driverArray[$key][$driverRouteDataByParam['shift_type_id']] + 1;
            }
            $tempCounts++;
        }

        if($driverRouteDataByParams){
            $cotexRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRouteCode')->getTodayDriverRouteByRouteCodeNew($companyId, $currentDate, $station, $startTime = null, $endTime = null, null, false, null);
        }


        //exit;
        //Open Driver Routes popup listings
        $openRouteListingRecords = $this->getTodayOpenDriverRoutePopupListingDataNew($companyId, $currentDate, $station, $startTime = null, $endTime = null, $openShiftTempDriverData);

        $openRouteListingPopup = [];

        foreach ($openRouteListingRecords as $openRouteListingRecord) {
            $timeObj = new \DateTime($openRouteListingRecord['shiftStartTime']);
            $openRouteListingPopup[] = [
                "id" => $openRouteListingRecord['routeId'],
                "code" => !empty($openRouteListingRecord['routeCode']) ? $openRouteListingRecord['routeCode'] : '-',
                "shiftType" => [
                    "id" => $openRouteListingRecord['shift_id'],
                    "name" => $openRouteListingRecord['shift_name'],
                    "color" => $openRouteListingRecord['shift_color'],
                    "category" => $openRouteListingRecord['routeCode'],
                    "time" => !empty($openRouteListingRecord['shiftStartTime']) ? rtrim($timeObj->format('H:ia'), 'm') : null, //rtrim($openRouteListingRecord->getStartTime()->format('H:ia'), 'm'),
                ],
                'isRequestCompleted' => $openRouteListingRecord['is_request_completed'],
                'isRequested' => !empty($openRouteListingRecord['request_for_route_id']) ? true : false,
                'requestApprovedBy' => !empty($openRouteListingRecord['is_request_completed']) ? true : false, //(!empty($openRouteListingRecord->getRequestForRoute()) && $openRouteListingRecord->getRequestForRoute()->getApprovedBy()) ? true : false,
            ];
        }
        usort($openRouteListingPopup, function ($i, $j) {
            return strnatcmp($i['code'], $j['code']);
        });

        $driverRouteDataByParams = $this->getTodayDriverRouteRescuereListingDataNew($companyId, $currentDate, $station, $startTime = null, $endTime = null, $tempOpenShiftRoutesIdArr,$driverRouteIsRescuerFalseIdArr,true);

        foreach ($driverRouteDataByParams as $driverRouteData) {
           /* $incident = $driverRouteData->getIncidents()->filter(function (Incident $item) {
                return $item->getIsArchive() === false;
            });
            //if (!empty($incident->first())) continue; */

            list($payload, $nNotDepartedCount, $nNotPunchInCount) = $this->globalUtils->getLoadoutPayloadDataNew($driverRouteData, $nCount, $nNotDepartedCount, $nNotPunchInCount, $requestTimeZone);

            $listingsDriverRoutes[] = $payload;
            if (!empty($payload)) {
                if (!empty($payload['routes'])) {
                    $assignRoutes['drivers'][] = $payload['driverId'];
                    $assignRoutes['routes'] = isset($assignRoutes['routes']) ? array_merge($assignRoutes['routes'], $payload['routes']) : $payload['routes'];
                } else {
                    $unAssignRoutes['drivers'][] = $payload['driverId'];
                }
            }

            $nCount++;
        }

        $shiftBoxListings[0] = [
            "id" => 'Assigned',
            "name" => 'Assigned',
            "category" => '',
            "drivers" => $assignRoutes ? count(array_unique($assignRoutes['drivers'])) : 0,
            "routes" => $assignRoutes ? count(array_unique($assignRoutes['routes'])) : 0,
            "time" => isset($tempArray['Assigned']) ? $shiftArray[$tempArray['Assigned'][0]]['time'] : '',
            "pickerTime" => isset($tempArray['Assigned']) ? $shiftArray[$tempArray['Assigned'][0]]['pickerTime'] : '',
            //'endTime' => isset($tempArray['Assigned']) ? $shiftArray[$tempArray['Assigned'][0]]['endTime'] : '',
            "shiftTimeDiffrence" => '',
            "cortexRoute" => $cotexRoute,
            "invoiceType" => isset($tempArray['Assigned']) ? $shiftArray[$tempArray['Assigned'][0]]['invoiceType'] : '',
            "isBackup" => 0,
            "isShift" => 0
        ];

        $shiftBoxListings[1] = [
            "id" => 'Unassigned',
            "name" => 'Unassigned',
            "category" => '',
            "drivers" => $unAssignRoutes ? count(array_unique($unAssignRoutes['drivers'])) : 0,
            "routes" => 0,
            "time" => isset($tempArray['Unassigned']) ? $shiftArray[$tempArray['Unassigned'][0]]['time'] : '',
            "pickerTime" => isset($tempArray['Unassigned']) ? $shiftArray[$tempArray['Unassigned'][0]]['pickerTime'] : '',
            //'endTime' => isset($tempArray['Unassigned']) ? $shiftArray[$tempArray['Unassigned'][0]]['endTime'] : '',
            "shiftTimeDiffrence" => '',
            "cortexRoute" => $cotexRoute,
            "invoiceType" => isset($tempArray['Unassigned']) ? $shiftArray[$tempArray['Unassigned'][0]]['invoiceType'] : '',
            "isBackup" => 0,
            "isShift" => 0
        ];

        $driverRoutesListings['station'] = $station;
        //Loadout remove starttime
        //$driverRoutesListings['startTime'] = $criteria['startTime'];
        $driverRoutesListings['balanceGroups'] = array_values($shiftBoxListings);
        $driverRoutesListings['openRoutes'] = $openRouteListingPopup;
        $driverRoutesListings['notPunchedIn'] = $nNotPunchInCount;
        $driverRoutesListings['notDeparted'] = $nNotDepartedCount;
        $driverRoutesListings['changes'] = [
            $station => $changes
        ];
        $driverRoutesListings['drivers'] = $listingsDriverRoutes;
        $driverRoutesListings['routeCommitment']['commitments'] = $this->getEntityManager()->getRepository('App\Entity\RouteCommitment')->getTodayRouteCommitmentCountsNew($station, $currentDate);
        $driverRoutesListings['routeCommitment']['drivers'] = $this->getEntityManager()->getRepository('App\Entity\Driver')->getTodaysDriverCountsNew($station, $currentDate);
        $driverRoutesListings['routeCommitment']['driversWithRouteCode'] = $this->getEntityManager()->getRepository('App\Entity\Driver')->getTodaysDriverWithRouteCodeCountsNew($station, $currentDate);
        $driverRoutesListings['routeCommitment']['codes'] = $this->getTodaysRouteCodeCountsNew($station, $currentDate);

        return $driverRoutesListings;
    }

    public function updateDriverOpenShiftInDriverRoute($driverId)
    {
        $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.driver', 'NULL')
            ->set('dr.schedule', 'NULL')
            ->set('dr.isOpenShift', true)
            ->where('dr.driver = :driverId')
            ->setParameter('driverId', $driverId)
            ->andWhere('dr.dateCreated > :dateCreated')
            ->setParameter('dateCreated', $currentDateObj)
            ->getQuery()
            ->execute();
        return true;
    }


    public function unscheduleAllShiftByDriverRoute($criteria)
    {
        $result = [];
        if (!empty($criteria['routeId'])) {
            $data = [];
            $qb = $this->getEntityManager()->createQueryBuilder();
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();

            if ($criteria['type'] == 'Unassign') {

                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.driver', 'NULL')
                    ->set('dr.schedule', 'NULL')
                    ->set('dr.isOpenShift', true)
                    ->where($qb->expr()->in('dr.id', ':routeId'))
                    ->setParameter('routeId', $criteria['routeId'])
                    ->getQuery()
                    ->execute();

            } else {
                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.isArchive', true)
                    ->where($qb->expr()->in('dr.id', ':routeId'))
                    ->setParameter('routeId', $criteria['routeId'])
                    ->getQuery()
                    ->execute();
            }

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->delete('App\Entity\TempDriverRoute', 'tdr')
                ->where($qb->expr()->in('tdr.routeId', ':routeId'))
                ->setParameter('routeId', $criteria['routeId'])
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()
                ->execute();

            if (!empty($criteria['message']) && is_string($criteria['message']) && !empty($criteria['sms']) && $criteria['sms']) {
                /** @var Driver[] $drivers */
                $driverIDs = $criteria['driverIds'];
                foreach ($driverIDs as $id) {
                    $driver = $this->em->find(Driver::class, $id);
                    if ($driver instanceof Driver) {
                        $data[] = [
                            "destinationPhoneNumber" => $driver->getPersonalPhone(),
                            "text" => $criteria['message'],
                        ];
                    }
                }
                $result['sms'] = $this->botService->sendPlivoSMSDrivers($data, $user->getCompany());
            }

        }

        $result['success'] = true;
        return $result;
    }

    public function setHolidayAndNonBusinessDayByDriverRoute($criteria)
    {
        if (!empty($criteria['routeId'])) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\DriverRoute', 'dr')
                ->set('dr.isActive', 0)
                ->where($qb->expr()->in('dr.id', ':routeId'))
                ->setParameter('routeId', $criteria['routeId'])
                ->getQuery()
                ->execute();

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->delete('App\Entity\TempDriverRoute', 'tdr')
                ->where($qb->expr()->in('tdr.routeId', ':routeId'))
                ->setParameter('routeId', $criteria['routeId'])
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()
                ->execute();
        }
        return true;
    }

    public function getDriverRouteByStation($stationId)
    {
        $results = $this->createQueryBuilder('dr')
            ->select('dr.id')
            ->andWhere('dr.station = :station')
            ->setParameter('station', $stationId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return array_column($results, 'id');

    }

    public function updateStationIsArchiveInDriverRoute($stationId)
    {

        $driverRouteIds = $this->getDriverRouteByStation($stationId);
        if (!empty($driverRouteIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\DriverRoute', 'dr')
                ->set('dr.isArchive', true)
                ->where($qb->expr()->in('dr.id', ':routeId'))
                ->setParameter('routeId', $driverRouteIds)
                ->getQuery()
                ->execute();

            //Call Temp driver route isArchive true set
            $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->updateTempDriverRouteSetIsArchive($driverRouteIds);
        }

        return true;
    }

    public function updateDriverRouteSetIsArchiveByShift($shifts)
    {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', true)
            ->where($qb->expr()->in('dr.shiftType', ':shifts'))
            ->setParameter('shifts', $shifts)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateSkillNullInDriverRoute($skillId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.skill', 'NULL')
            ->where('dr.skill = :skillId')
            ->setParameter('skillId', $skillId)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateShiftIsArchiveInDriverRoute($shiftId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', true)
            ->where('dr.shiftType = :shiftId')
            ->setParameter('shiftId', $shiftId)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateScheduleIsArchiveInDriverRoute($scheduleId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', true);
        if (is_array($scheduleId)) {
            $qb->where($qb->expr()->in('dr.schedule', ':scheduleId'))
                ->setParameter('scheduleId', $scheduleId);
        } else {
            $qb->where('dr.schedule = :schedule')
                ->setParameter('schedule', $scheduleId);
        }
        $qb->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    /**
     * @param array $criteria
     * @return DriverRoute|array|null
     */
    public function findRoutes(array $criteria)
    {
        try {
            $dateTime = new DateTime($criteria['date']);
            $route = $this->findOneBy(['driver' => $criteria['driverId'], 'dateCreated' => $dateTime]);
            $result = [];
            if ($route instanceof DriverRoute) {
                if ($route->getKickoffLog() === null) {
                    $kickOffLog = new KickoffLog();
                    $kickOffLog->setDriverRoute($route);
                    $this->em->persist($kickOffLog);
                    $this->em->flush();
                    $route->setKickoffLog($kickOffLog);
                }
                if ($route->getReturnToStation() === null) {
                    $returnToStation = new ReturnToStation();
                    $returnToStation->setDriverRoute($route);
                    $this->em->persist($returnToStation);
                    $this->em->flush();
                    $route->setReturnToStation($returnToStation);
                }
                $this->em->flush();
                $result = [
                    'kickoffLogId' => $route->getKickoffLog()->getId(),
                    'returnToStation' => $route->getReturnToStation()->getId(),
                    'routeCodes' => array_map(function (DriverRouteCode $item) {
                        return $item->getCode();
                    }, $route->getDriverRouteCodes()->toArray()),
                ];
            }
            return $result;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws Exception
     */
    public function findSchedule($criteria)
    {
        $query = $this->createQueryBuilder('dr');
        if ($criteria['where']) {
            foreach ($criteria['where'] as $key => $where) {
                if (isset($where['condition']) === 'BETWEEN') {
                    $from = new DateTime($where['from']);
                    $to = new DateTime($where['to']);
                    $query
                        ->andWhere('dr.shiftTime BETWEEN :from AND :to')
                        ->setParameter('from', $from)
                        ->setParameter('to', $to);
                }
            }
        }
        return $query->getQuery()->getResult();
    }

    public function countBy(array $criteria)
    {
        $persister = $this->_em->getUnitOfWork()->getEntityPersister($this->_entityName);
        return $persister->count($criteria);
    }

    public function shiftDateChangeRemoveDriverRoute($shiftId, $scheduleId, $weekDayArray, $week, $year, $updatedDriverRoutes, $today = null)
    {
        $dateTime = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dateTime->setISODate($year, $week, 0);
        $startDate = $dateTime->format('Y-m-d');
        $dateTime->modify('+6 days');
        $endDate = $dateTime->format('Y-m-d');

        $today = new \DateTime($today);
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('dr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.shiftType = :shiftId')
            ->setParameter('shiftId', $shiftId)
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('dr.dateCreated <= :endDate')
            ->setParameter('endDate', $endDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today);

        if (!empty($weekDayArray)) {

            $qb->andWhere($qb->expr()->notIn('dr.dateCreated', ':weekDayArray'))
                ->setParameter('weekDayArray', $weekDayArray);
        }

        if (!empty($updatedDriverRoutes)) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $updatedDriverRoutes);
        }

        $qb->getQuery()->execute();

        return true;
    }

    public function removeDriverRouteWhichNotMatachWithTotalWeek($shiftId, $scheduleId, $endDateOfSchedule, $updatedDriverRoutes)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('dr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.shiftType = :shiftId')
            ->setParameter('shiftId', $shiftId)
            ->andWhere('dr.dateCreated > :endDateOfSchedule')
            ->setParameter('endDateOfSchedule', $endDateOfSchedule)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        if (!empty($updatedDriverRoutes)) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $updatedDriverRoutes);
        }

        $qb->getQuery()->execute();

        return true;
    }

    public function shiftChangeRemoveDriverRoute($scheduleId, $scheduleShiftIdArray, $week, $year, $updatedDriverRoutes, $today = null)
    {
        $dateTime = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dateTime->setISODate($year, $week, 0);
        $startDate = $dateTime->format('Y-m-d');
        $dateTime->modify('+6 days');
        $endDate = $dateTime->format('Y-m-d');

        $today = new \DateTime($today);
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('dr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('dr.dateCreated <= :endDate')
            ->setParameter('endDate', $endDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today);

        if (!empty($scheduleShiftIdArray)) {
            $qb->andWhere($qb->expr()->notIn('dr.shiftType', ':scheduleShiftIdArray'))
                ->setParameter('scheduleShiftIdArray', $scheduleShiftIdArray);
        }

        if (!empty($updatedDriverRoutes)) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $updatedDriverRoutes);
        }

        $qb->getQuery()->execute();

        return true;
    }

    public function getDriverRoutesWithParams($driverId, $weekDate, $shiftId, $scheduleId)
    {
        $results = $this->createQueryBuilder('dr')
            ->where('dr.driver = :driver')
            ->andWhere('dr.dateCreated = :weekDate')
            ->andWhere('dr.shiftType = :shift')
            ->andWhere('dr.schedule = :schedule')
            ->setParameter('driver', $driverId)
            ->setParameter('weekDate', $weekDate)
            ->setParameter('shift', $shiftId)
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult();

        return $results;
    }

    public function addOrUpdateScheduleDriverRoute($weekDate, $shiftId, $scheduleId, $isActive, $updatedDriverRoutes, $today, $driverId = null)
    {

        $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($shiftId);
        $schedule = $this->getEntityManager()->getRepository('App\Entity\Schedule')->find($scheduleId);

        $driver = null;
        $results = [];
        $isOpenShift = 0;
        if ($driverId) {
            $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
            $results = $this->getDriverRoutesWithParams($driverId, $weekDate, $shiftId, $scheduleId);
        } else {
            $isOpenShift = 1;
        }

        if (count($results) <= 0) {
            $driverRoute = null;
            if ($driver) {
                $driverRoute = $this->createQueryBuilder('dr')
                    ->where('dr.shiftType = :shift')
                    ->setParameter('shift', $shiftId)
                    ->andWhere('dr.dateCreated = :weekDate')
                    ->setParameter('weekDate', $weekDate)
                    ->andWhere('dr.isArchive = :isArchive')
                    ->setParameter('isArchive', false)
                    ->andWhere('dr.startTime = :startTime')
                    ->setParameter('startTime', $shift->getStartTime())
                    ->andWhere('dr.endTime = :endTime')
                    ->setParameter('endTime', $shift->getEndTime())
                    ->andWhere('dr.driver IS NULL')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();
            } else {
                $today = new \DateTime($today);
                $today->setTime(23, 59, 59);

                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.isArchive', ':makeArchive')
                    ->setParameter('makeArchive', true)
                    ->where('dr.schedule = :scheduleId')
                    ->setParameter('scheduleId', $scheduleId)
                    ->andWhere('dr.schedule IS NOT NULL')
                    ->andWhere('dr.isOpenShift = :isOpenShift')
                    ->setParameter('isOpenShift', true)
                    ->andWhere('dr.dateCreated > :today')
                    ->setParameter('today', $today)
                    ->andWhere('dr.dateCreated = :weekDate')
                    ->setParameter('weekDate', $weekDate);
                if (!empty($updatedDriverRoutes)) {
                    $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                        ->setParameter('driverRouteArray', $updatedDriverRoutes);
                }
                $qb->getQuery()->execute();
            }

            if (empty($driverRoute)) {
                $driverRoute = new DriverRoute();
                $driverRoute->setDateCreated(new DateTime($weekDate));
                $driverRoute->setShiftType($shift);
                $driverRoute->setSkill($shift->getSkill());
            }

            $driverRoute->setSchedule($schedule);
            $driverRoute->setStation($shift->getStation());
            $driverRoute->setIsActive($isActive);
            $driverRoute->setIsOpenShift($isOpenShift);
            $driverRoute->setDriver($driver);
            $driverRoute->setIsTemp(false);
            $driverRoute->setShiftName($shift->getName());
            $driverRoute->setShiftColor($shift->getColor());
            $driverRoute->setUnpaidBreak($shift->getUnpaidBreak());
            $driverRoute->setShiftNote($shift->getNote());
            $driverRoute->setCurrentNote($shift->getNote());
            $driverRoute->setShiftInvoiceType($shift->getInvoiceType());
            $driverRoute->setShiftStation($shift->getStation());
            $driverRoute->setShiftSkill($shift->getSkill());
            $driverRoute->setShiftCategory($shift->getCategory());
            $driverRoute->setShiftBalanceGroup($shift->getBalanceGroup());
            $driverRoute->setShiftTextColor($shift->getTextColor());
            $driverRoute->setStartTime($shift->getStartTime());
            $driverRoute->setEndTime($shift->getEndTime());
            $driverRoute->setIsRescuer(false);
            $driverRoute->setIsRescued(false);
            $driverRoute->setIsLoadOut(false);
            $driverRoute->setIsBackup(false);

            $this->getEntityManager()->persist($driverRoute);
            $this->getEntityManager()->flush($driverRoute);
        }


        return true;
    }


    public function updateDriverNullInDriverRoute($scheduleId, $scheduleDriver, $updatedDriverRoutes)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.driver', 'NULL')
            ->set('dr.isOpenShift', true)
            ->where('dr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today);

        if (!empty($scheduleDriver)) {
            $qb->andWhere($qb->expr()->notIn('dr.driver', ':driverArray'))
                ->setParameter('driverArray', $scheduleDriver);
        }

        if (!empty($updatedDriverRoutes)) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $updatedDriverRoutes);
        }

        $qb->getQuery()->execute();

        return true;
    }

    public function publishDriverRoute($criteria)
    {
        $tempDriverRouteIdArray = [];
        if (!is_array($criteria['tempDriverRouteIds'])) {
            $tempDriverRouteIds = [$criteria['tempDriverRouteIds']];
        }
        foreach ($criteria['tempDriverRouteIds'] as $routeId) {
            $valueArray = explode('-', $routeId);
            if (count($valueArray) > 1) {
                $tempDriverRouteIdArray[] = $valueArray[0];
                $tempDriverRouteIdArray[] = $valueArray[1];
            } else {
                $tempDriverRouteIdArray[] = $valueArray[0];
            }
        }
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRouteIds = [];
        foreach ($tempDriverRouteIdArray as $tempDriverRouteId) {
            //Get temp driver route
            $tempDriverRoute = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->find($tempDriverRouteId);
            //Update temp driver route
            $driverRoute = $tempDriverRoute->getRouteId();
            $driverRouteIds[] = $driverRoute->getId();
            if ($driverRoute->getRequestForRoute()) {
                $routeRequest = $driverRoute->getRequestForRoute();
                if (!$driverRoute->getRequestForRoute()->getNewDriver()) {
                    $routeRequest->setApprovedBy($userLogin);
                } else {
                    $routeRequest->setApprovedBy($userLogin);
                    $routeRequest->setIsRequestCompleted(true);
                }

                $this->getEntityManager()->persist($routeRequest);
                $this->getEntityManager()->flush($routeRequest);

                if ($driverRoute->getRequestForRoute()->getNewDriver()) {
                    $this->getEntityManager()->flush();
                }
                $this->submitPublishDriverRoute($driverRoute, $tempDriverRoute, $criteria['view']);
            } else {
                $this->submitPublishDriverRoute($driverRoute, $tempDriverRoute, $criteria['view']);
            }
        }
        //Event published
        if (!empty($driverRouteIds)) {
            $this->getEntityManager()->getRepository('App\Entity\Event')->updateEventData($driverRouteIds);
        }
        return true;
    }

    public function submitPublishDriverRoute($driverRoute, $tempDriverRoute, $view)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRoute->setSkill($tempDriverRoute->getSkill());
        $driverRoute->setShiftType($tempDriverRoute->getShiftType());
        $driverRoute->setIsTemp(false);
        $driverRoute->setShiftName($tempDriverRoute->getShiftName());
        $driverRoute->setShiftColor($tempDriverRoute->getShiftColor());
        $driverRoute->setUnpaidBreak($tempDriverRoute->getUnpaidBreak());
        $driverRoute->setShiftNote($tempDriverRoute->getShiftNote());
        $driverRoute->setShiftInvoiceType($tempDriverRoute->getShiftInvoiceType());
        $driverRoute->setShiftStation($tempDriverRoute->getShiftStation());
        $driverRoute->setShiftSkill($tempDriverRoute->getShiftSkill());
        $driverRoute->setShiftCategory($tempDriverRoute->getShiftCategory());
        $driverRoute->setShiftBalanceGroup($tempDriverRoute->getShiftBalanceGroup());
        $driverRoute->setShiftTextColor($tempDriverRoute->getShiftTextColor());
        $driverRoute->setStartTime($tempDriverRoute->getStartTime());
        $driverRoute->setEndTime($tempDriverRoute->getEndTime());
        $driverRoute->setPunchIn($tempDriverRoute->getPunchIn());
        $driverRoute->setPunchOut($tempDriverRoute->getPunchOut());
        $driverRoute->setDateCreated($tempDriverRoute->getDateCreated());
        $driverRoute->setDriver(!empty($tempDriverRoute->getDriver()) ? $tempDriverRoute->getDriver() : null);
        $driverRoute->setIsOpenShift(empty($tempDriverRoute->getDriver()) ? 1 : 0);
        $driverRoute->setIsArchive(($tempDriverRoute->getAction() == 'SHIFT_DELETED') ? true : false);

        $driverRoute->setCurrentNote($tempDriverRoute->getCurrentNote());
        $driverRoute->setNewNote($tempDriverRoute->getNewNote());

        if ($view == 'loadOut') {
            $driverRoute->setOldDriver(!empty($tempDriverRoute->getOldDriver()) ? $tempDriverRoute->getOldDriver() : null);
            $driverRoute->setOldDriverRoute(!empty($tempDriverRoute->getOldDriverRoute()) ? $tempDriverRoute->getOldDriverRoute() : null);
            $driverRoute->setIsBackup(!empty($tempDriverRoute->getIsBackup()) ? $tempDriverRoute->getIsBackup() : 0);
        }

        $this->getEntityManager()->persist($driverRoute);
        $this->getEntityManager()->flush();

        //Update created by old route id with isNew = 0

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteByRouteId($tempDriverRoute->getRouteId(), false);
        $tempRoute = $tempDriverRoutes[0];
        $tempRoute->setCreatedBy($userLogin);
        $tempRoute->setAction($tempDriverRoute->getAction());
        $this->getEntityManager()->persist($tempRoute);
        $this->getEntityManager()->flush();

        //Delete all same route updated records with isNew = 1
        $qb = $this->getEntityManager()->createQueryBuilder();
        $query = $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.routeId = :routeId')
            ->setParameter('routeId', $tempDriverRoute->getRouteId())
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery();
        $query->execute();
        return true;
    }

    public function checkRoute($schedule, $driver, $shift, $date)
    {

        $results = $this->createQueryBuilder('dr')
            ->where('dr.driver = :driver')
            ->andWhere('dr.shiftType = :shift')
            ->andWhere('dr.schedule = :schedule')
            ->andWhere('dr.station = :station')
            ->andWhere('dr.dateCreated = :date')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->setParameter('driver', $driver)
            ->setParameter('shift', $shift)
            ->setParameter('schedule', $schedule)
            ->setParameter('station', $schedule->getStation())
            ->setParameter('date', new Datetime($date->format('Y-m-d 00:00:00')))
            ->getQuery()
            ->getResult();
        if ($results) {
            return false;
        } else {
            return true;
        }
    }

    public function getDriverRouteCountByShift($shiftId, $companyId, $date)
    {
        return $this->createQueryBuilder('dr')
            ->select('count(dr.id)')
            ->leftJoin('dr.schedule', 'sh')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :date')
            ->andWhere('dr.shiftType = :shiftId')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->setParameter('date', $date)
            ->setParameter('shiftId', $shiftId)
            ->andWhere('sh.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->getQuery()->getSingleScalarResult();
    }

    public function setScheduleDriverRouteActive($weekDate, $driverId, $scheduleId, $today)
    {
        $today = new \DateTime($today);
        $today->setTime(23, 59, 59);


        $results = $this->createQueryBuilder('dr')
            ->select('dr.id')
            ->leftJoin('dr.schedule', 'sh')
            ->where('dr.schedule is NOT NULL')
            ->andWhere('sh.previousAssignSchedule = 0')
            ->andWhere('dr.schedule != :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('dr.dateCreated = :weekDate')
            ->setParameter('weekDate', $weekDate)
            ->andWhere('dr.driver = :driverId')
            ->setParameter('driverId', $driverId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        $driverRouteArray = array_column($results, 'id');

        if (!empty($driverRouteArray)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $results = $qb->update('App\Entity\DriverRoute', 'dr')
                ->set('dr.isActive', '0')
                ->where($qb->expr()->in('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $driverRouteArray)
                ->getQuery()
                ->execute();
        }
    }

    public function setCurrentScheduleDriverRouteFalse($weekDate, $driverId, $scheduleId, $today)
    {
        $today = new \DateTime($today);
        $today->setTime(23, 59, 59);

        return $this->createQueryBuilder('dr')
            ->select('count(dr.id)')
            ->leftJoin('dr.schedule', 'sh')
            ->where('dr.schedule is NOT NULL')
            ->andWhere('dr.schedule != :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('sh.previousAssignSchedule = 1')
            ->andWhere('dr.dateCreated = :weekDate')
            ->setParameter('weekDate', $weekDate)
            ->andWhere('dr.driver = :driverId')
            ->setParameter('driverId', $driverId)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->getSingleScalarResult();
    }


    public function getScheduleDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $routeIdArrayWhichIsInTemp = [], $driverId = null)
    {
        $tempDriverRouteIds = '';
        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $routeIds = implode("','",$routeIdArrayWhichIsInTemp);
            $tempDriverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
        }
        $driverCriteria = '';
        if (!empty($driverId)) {
            $driverCriteria = " AND dr.driver_id = ".$driverId;
        }

        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                        dr.id as id,dr.date_created as dateCreated,dr.start_time as startTime,dr.end_time as endTime,dr.unpaid_break as unpaidBreak,
                        dr.shift_color as shiftColor,dr.shift_text_color as shiftTextColor,dr.shift_name as shiftName,dr.number_of_package as numberOfPackage,
                        dr.is_temp as isTemp,dr.is_rescued as isRescued,dr.is_rescuer as isRescuer,dr.new_note as newNote,dr.shift_note as shiftNote,dr.current_note as currentNote,
                        dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,dr.is_open_shift as isOpenShift,
                        tdr.is_new as isNew,tdr.action as action,tdr.created_date as createdDate,
                        dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.mobile_version_used as mobileVersionUsed,
                        dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                        dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                        odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,odr.is_archive as oldDriverRoute_isArchive,
                        odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,
                        sh.id as schedule_id,sh.name as schedule_name,
                        dri.id as driver_id,dri.driving_preference as driver_drivingPreference,
                        dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                        rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                        rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                        es.id as es_id,es.name as es_name,
                        u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                        u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                        dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                        s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                        sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                        (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        driver_route dr
                    LEFT JOIN
                        temp_driver_route tdr
                    ON
                        dr.id = tdr.route_id_id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = dr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = dr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = dr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = dr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = dr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = dr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = dr.shift_invoice_type_id
                    LEFT JOIN
                        driver temp_dri
                    ON
                        temp_dri.id = tdr.driver_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE dr.date_created >= '".$weekStartEndDay['week_start']."' AND dr.date_created <= '".$weekStartEndDay['week_end']."' AND sf.company_id = ".$userLogin->getCompany()->getId()." AND dr.is_archive = 0 AND dr.is_active = 1 AND sf.is_archive = 0 AND st.is_archive = 0 AND ((dr.driver_id IS NOT NULL AND dri.is_enabled = 1 AND dr.is_open_shift = 0 AND dri.is_archive = 0 AND u.is_archive = 0) OR (temp_dri.is_enabled = 1 AND dr.driver_id IS NULL AND tdr.driver_id IS NOT NULL AND tdr.is_new = 1 AND dr.is_open_shift = 1 AND tdr.is_archive = 0))".$tempDriverRouteIds.$driverCriteria." GROUP BY dr.id ORDER BY dr.date_created ASC,u.first_name ASC";

        $driverRouteResult = $this->getEntityManager()
                ->getConnection()
                ->query($sqlQuery)
                ->fetchAll();

        return $driverRouteResult;
    }

    public function getScheduleDriverRoutesWithParam($weekStartEndDay, $userLogin, $routeIdArrayWhichIsInTemp = [], $driverId = null)
    {
        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'dri')
            ->leftJoin('dri.user', 'u')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->leftJoin('tdr.driver', 'tdri')
            ->where('dr.dateCreated >= :sunday')
            ->andWhere('dr.dateCreated <= :saturday')
            ->setParameter('sunday', $weekStartEndDay['week_start'])
            ->setParameter('saturday', $weekStartEndDay['week_end'])
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('(dr.driver IS NOT NULL and dri.isEnabled = 1 and dr.isOpenShift = 0 and dri.isArchive = 0 and u.isArchive = 0) or (tdri.isEnabled = 1 and dr.driver IS NULL and tdr.driver IS NOT NULL and tdr.isNew = 1 and dr.isOpenShift = 1 and tdr.isArchive = 0)')
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->orderBy('dr.dateCreated', 'ASC')
            ->orderBy('u.firstName', 'ASC');


        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':routeIdArrayWhichIsInTemp'))
                ->setParameter('routeIdArrayWhichIsInTemp', $routeIdArrayWhichIsInTemp);
        }

        if (!empty($driverId)) {
            $qb->andWhere('dr.driver = :driver')->setParameter('driver', $driverId);
        }

        return $qb->getQuery()->getResult();
    }

    public function getScheduleDriverRoutesWithParamForDayView($currentPassedDate, $userLogin, $routeIdArrayWhichIsInTemp)
    {

        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'dri')
            ->leftJoin('dri.user', 'u')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->where('dr.dateCreated = :currentPassedDate')
            ->setParameter('currentPassedDate', $currentPassedDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('(dr.driver IS NOT NULL and dr.isOpenShift = 0 and dri.isArchive = 0 and u.isArchive = 0) or (dr.driver IS NULL and tdr.driver IS NOT NULL and tdr.isNew = 1 and dr.isOpenShift = 1 and tdr.isArchive = 0)')
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->orderBy('dr.dateCreated', 'ASC');

        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':routeIdArrayWhichIsInTemp'))
                ->setParameter('routeIdArrayWhichIsInTemp', $routeIdArrayWhichIsInTemp);
        }

        return $qb->getQuery()->getResult();
    }

     public function getScheduleOpenShiftDriverRoutesWithParam($weekStartEndDay, $userLogin, $routeIdArrayWhichIsInTemp)
     {
         $qb = $this->createQueryBuilder('dr')
             ->leftJoin('dr.shiftType', 'sf')
             ->leftJoin('dr.driver', 'dri')
             ->leftJoin('dri.user', 'u')
             ->leftJoin('dr.station', 'st')
             ->leftJoin('dr.tempDriverRoutes', 'tdr')
             ->where('dr.dateCreated >= :sunday')
             ->andWhere('dr.dateCreated <= :saturday')
             ->setParameter('sunday', $weekStartEndDay['week_start'])
             ->setParameter('saturday', $weekStartEndDay['week_end'])
             ->andWhere('sf.company = :company')
             ->setParameter('company', $userLogin->getCompany()->getId())
             ->andWhere('st.isArchive = :isArchiveStation')
             ->setParameter('isArchiveStation', false)
             ->andWhere('(dr.driver IS NULL and dr.isOpenShift = 1) or (dr.driver IS NOT NULL and tdr.driver IS NULL and tdr.isNew = 1 and dr.isOpenShift = 0 and tdr.isArchive = 0)')
             ->andWhere('dr.isActive = :isActive')
             ->setParameter('isActive', 1)
             ->andWhere('dr.isArchive = :isArchive')
             ->setParameter('isArchive', false);

         if (count($routeIdArrayWhichIsInTemp) > 0) {
             $qb->andWhere($qb->expr()->notIn('dr.id', ':routeIdArrayWhichIsInTemp'))
                 ->setParameter('routeIdArrayWhichIsInTemp', $routeIdArrayWhichIsInTemp);
         }

         return $qb->getQuery()->getResult();
     }


    public function getScheduleOpenShiftDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $routeIdArrayWhichIsInTemp)
    {
        $driverRouteIds = '';
        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $routeIds = implode("','",$routeIdArrayWhichIsInTemp);
            $driverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
        }
        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                        dr.id as id,dr.date_created as dateCreated,dr.start_time as startTime,dr.end_time as endTime,dr.unpaid_break as unpaidBreak,dr.shift_color as shiftColor,dr.shift_text_color as shiftTextColor,dr.shift_name as shiftName,dr.number_of_package as numberOfPackage,
                        dr.is_temp as isTemp,dr.is_rescued as isRescued,dr.is_rescuer as isRescuer,dr.shift_note as shiftNote,dr.new_note as newNote,dr.current_note as currentNote,
                        dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,dr.is_open_shift as isOpenShift,
                        tdr.is_new as isNew,tdr.action as action,tdr.created_date as createdDate,
                        dr.mobile_version_used as mobileVersionUsed,dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                        dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                        odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,
                        odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,
                        odr.is_archive as oldDriverRoute_isArchive,
                        sh.id as schedule_id,sh.name as schedule_name,
                        dri.id as driver_id,dri.driving_preference as driver_drivingPreference,dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                        rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                        rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                        es.id as es_id,es.name as es_name,
                        u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                        u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                        dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                        s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                        sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                        (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        driver_route dr
                    LEFT JOIN
                        temp_driver_route tdr
                    ON
                        dr.id = tdr.route_id_id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = dr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = dr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = dr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = dr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = dr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = dr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = dr.shift_invoice_type_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE dr.date_created >= '".$weekStartEndDay['week_start']."' AND dr.date_created <= '".$weekStartEndDay['week_end']."' AND sf.company_id = ".$userLogin->getCompany()->getId()." AND dr.is_archive = 0 AND dr.is_active = 1 AND st.is_archive = 0 AND ((dr.driver_id IS NULL AND dr.is_open_shift = 1) or (dr.driver_id IS NOT NULL and tdr.driver_id IS NULL and tdr.is_new = 1 and dr.is_open_shift = 0 and tdr.is_archive = 0))".$driverRouteIds." GROUP BY dr.id";

        $driverRouteData = $this->getEntityManager()
                            ->getConnection()
                            ->query($sqlQuery)
                            ->fetchAll();

        return $driverRouteData;
    }

    public function getScheduleOpenShiftDriverRoutesWithParamForDayViewNew($currentPassedDate, $userLogin, $routeIdArrayWhichIsInTemp)
    {
        $driverRouteIds = '';
        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $routeIds = implode("','",$routeIdArrayWhichIsInTemp);
            $driverRouteIds = " AND dr.id NOT IN ('".$routeIds."')";
        }

        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                        dr.id as id,dr.date_created as dateCreated,dr.start_time as startTime,dr.end_time as endTime,dr.unpaid_break as unpaidBreak,dr.shift_color as shiftColor,dr.shift_text_color as shiftTextColor,dr.shift_name as shiftName,dr.number_of_package as numberOfPackage,
                        dr.is_temp as isTemp,dr.is_rescued as isRescued,dr.is_rescuer as isRescuer,dr.shift_note as shiftNote,dr.new_note as newNote,dr.current_note as currentNote,
                        dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,dr.is_open_shift as isOpenShift,
                        tdr.is_new as isNew,tdr.action as action,tdr.created_date as createdDate,
                        dr.mobile_version_used as mobileVersionUsed,dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                        dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                        odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,
                        odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,
                        odr.is_archive as oldDriverRoute_isArchive,
                        sh.id as schedule_id,sh.name as schedule_name,
                        dri.id as driver_id,dri.driving_preference as driver_drivingPreference,dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                        rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                        rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                        es.id as es_id,es.name as es_name,
                        u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                        u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                        dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                        s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                        sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                        (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        driver_route dr
                    LEFT JOIN
                        temp_driver_route tdr
                    ON
                        dr.id = tdr.route_id_id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = dr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = dr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = dr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = dr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = dr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = dr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = dr.shift_invoice_type_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE dr.date_created = '".$currentPassedDate."'
                    AND sf.company_id = ".$userLogin->getCompany()->getId()."
                    AND dr.is_archive = 0
                    AND dr.is_active = 1
                    AND st.is_archive = 0
                    AND ((dr.driver_id IS NULL AND dr.is_open_shift = 1) or
                    (dr.driver_id IS NOT NULL AND tdr.driver_id IS NULL AND tdr.is_new = 1 AND dr.is_open_shift = 0 AND tdr.is_archive = 0))".$driverRouteIds." GROUP By dr.id ";



        $driverRouteData = $this->getEntityManager()
                            ->getConnection()
                            ->query($sqlQuery)
                            ->fetchAll();

        return $driverRouteData;
    }

    public function getScheduleOpenShiftDriverRoutesWithParamForDayView($currentPassedDate, $userLogin, $routeIdArrayWhichIsInTemp)
    {

        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.driver', 'dri')
            ->leftJoin('dri.user', 'u')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->where('dr.dateCreated = :currentPassedDate')
            ->setParameter('currentPassedDate', $currentPassedDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('(dr.driver IS NULL and dr.isOpenShift = 1) or (dr.driver IS NOT NULL and tdr.driver IS NULL and tdr.isNew = 1 and dr.isOpenShift = 0 and tdr.isArchive = 0)')
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        if (count($routeIdArrayWhichIsInTemp) > 0) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':routeIdArrayWhichIsInTemp'))
                ->setParameter('routeIdArrayWhichIsInTemp', $routeIdArrayWhichIsInTemp);
        }

        return $qb->getQuery()->getResult();
    }

    public function getdriverFromSchedule($weekNumber)
    {
        $this->stopwatch->start('week');

        $_station = $weekNumber['station'] ?? false;
        if ($_station)
            $_station = (int)$_station;

        $userLogin = $this->tokenStorage->getToken()->getUser();
        $weekStartEndDay = $this->getStartAndEndDate($weekNumber['wknumber'], $weekNumber['year']);

        $requestTimeZone = isset($weekNumber['timezoneOffset']) ? $weekNumber['timezoneOffset'] : 0;

        $tempOpenShiftRoutesData = $tempOpenShiftRoutesIdArr = $tempRoutesData = $tempRoutesIdArr = $schedulerData =
        $driverSkills = $openShiftRoutes = $dayOfWeekOpenShift = $groupByOpenShiftType = $schedulerData =
        $filterArrayScheduler = $dayOfWeek = $biggerWeekCount = $pastShiftData = $futureShiftData = [];
        $hasDeleted = 0;

        $footerArray = [
            'totalDriverArray' => [],
            'driverArrayByDay' => [],
            'routeCodeArrayByDay' => [],
            'routeArrayByDay' => [],
            'footerData' => [],
            'shiftArrayByDay' => [],
            'shiftArrayForDriverAndRoute' => [],
        ];

        $drivers = $this->getEntityManager()->getRepository(Driver::class)->createQueryBuilder('d')
            ->select('d.id as d_id', 'd.drivingPreference as d_drivingPreference', 'u.friendlyName as u_friendlyName', 'u.firstName as u_firstName', 'u.lastName as u_lastName', 'u.profileImage as u_profileImage')
            ->innerJoin('d.stations', 's', 'WITH', 's.id = :station')
            ->setParameter('station', $_station)
            ->leftJoin('d.user', 'u')
            ->andWhere('d.isArchive = 0')
            ->andWhere('d.isEnabled = 1')
            ->andWhere('u.isArchive = 0')
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('d.employeeStatus = :employeeStatus')
            ->setParameter('employeeStatus', Driver::STATUS_ACTIVE)
            ->addOrderBy('u.friendlyName', 'ASC')
            ->getQuery()
            ->getScalarResult();

        $this->stopwatch->lap('week');

        $routeCommitments = $this->getEntityManager()->getRepository('App\Entity\RouteCommitment')->getRouteCommitmentsByWeekNew($weekNumber['wknumber'], $userLogin->getCompany()->getId(), (int)$weekNumber['year'], $_station);
        $this->stopwatch->lap('week');

        $gradingRules = $this->getEntityManager()->getRepository('App\Entity\GradingSettings')->getGradingRulesSettings($userLogin->getCompany()->getId());
        $this->stopwatch->lap('week');

        $routeCommitmentArrayByDay = [];
        $totalRouteCommitment = 0;
        $gradingRulesIds = [];
        foreach ($gradingRules as $rule) {
            $gradingRulesIds[] = $rule['id'];
        }

        foreach ($routeCommitments as $routeCommitment) {
            $weekDayaOfRouteCommitment = date('w', strtotime($routeCommitment['rc_weekDate']));

            $routeCommitmentArrayByDay[$weekDayaOfRouteCommitment] = isset($routeCommitmentArrayByDay[$weekDayaOfRouteCommitment]) ? (int)$routeCommitmentArrayByDay[$weekDayaOfRouteCommitment] + (int)$routeCommitment['rc_route'] : (int)$routeCommitment['rc_route'];

            $footerArray['shiftArrayForDriverAndRoute'][$weekDayaOfRouteCommitment][$routeCommitment['sf_id']]['shiftRoute'] = (int)$routeCommitment['rc_route'];

            $r = (int)$routeCommitment['rc_route'];
            $rate = $r > 0 ? (0 / $r) * 100 : 0;

            $grading = [];
            if ($r > 0) {
                foreach ($gradingRules as $rule) {
                    if (((int)$rule['min'] !== null && (int)$rule['max'] !== null && (int)$rule['min'] <= $rate && (int)$rule['max'] >= $rate)
                        || ((int)$rule['max'] === null && (int)$rule['min'] <= $rate)
                        || ((int)$rule['min'] === null && (int)$rule['max'] >= $rate)) {
                        $grading = [
                            'id' => (int)$rule['id'],
                            'color' => $rule['color'],
                            'textColor' => $rule['textColor'],
                            'description' => $rule['description'],
                        ];
                    }
                }
            }
            $footerArray['shiftArrayByDay'][$weekDayaOfRouteCommitment][$routeCommitment['sf_id']] = [
                'background' => $routeCommitment['sf_color'],
                'type' => $routeCommitment['sf_name'],
                'd' => 0,
                'r' => $r,
                'code' => 0,
                'grading' => $grading,
                'date'=> $routeCommitment['rc_weekDate']
            ];

            $totalRouteCommitment = (int)$totalRouteCommitment + (int)$routeCommitment['rc_route'];

        }
        $this->stopwatch->lap('week');

        $driverIds = '';
        $driverSkillsQueryResult = '';
        $driverStations = '';
        if (!empty($drivers)) {
            $driverIds = implode(', ', array_map(function ($driver) {
                return $driver['d_id'];
            }, $drivers));


            $driverSkillsQuery = "
              SELECT
                  s.id as skillId,
                  d.id as driverId
              FROM
                  skill s
              INNER JOIN
                  driver_skill ds ON ds.skill_id = s.id
              INNER JOIN
                  driver d ON ds.driver_id = d.id
              WHERE
                  d.id IN ({$driverIds})
          ";

            $driverSkillsQueryResult = $this->getEntityManager()
                ->getConnection()
                ->query($driverSkillsQuery)
                ->fetchAll();

            foreach ($driverSkillsQueryResult as $result) {
                $driverSkills[$result['driverId']][] = (int)$result['skillId'];
            }

            $driverStationsQuery = "
              SELECT
                  s.id as stationId,
                  d.id as driverId
              FROM
                  station s
              INNER JOIN
                  driver_station ds ON ds.station_id = s.id
              INNER JOIN
                  driver d ON ds.driver_id = d.id
              WHERE
                  d.id IN ({$driverIds})
          ";
            $driverStationsQueryResult = $this->getEntityManager()
                ->getConnection()
                ->query($driverStationsQuery)
                ->fetchAll();

            $driverStations = array_reduce($driverStationsQueryResult, function ($all, $single) {
                $all[$single['driverId']] = $single['stationId'];
                return $all;
            }, []);
        }
        $this->stopwatch->lap('week');

        if (!empty($drivers)) {
            foreach ($drivers as $driver) {
                $currentDriverSkills = isset($driverSkills[$driver['d_id']]) ? $driverSkills[$driver['d_id']] : [];
                $driverStationId = isset($driverStations[$driver['d_id']]) ? $driverStations[$driver['d_id']] : null;

                if ($_station && (int)$driverStationId !== $_station)
                    continue;

                /** @var Driver $driver */
                $schedulerData[$driver['d_id']] = [
                    'biggerWeek' => 1,
                    'id' => (int)$driver['d_id'],
                    'clockedIn' => true,
                    'driverId' => (int)$driver['d_id'],
                    'scheduledId' => null,
                    'stationId' => (int)$driverStationId,
                    'driver' => ucfirst($driver['u_friendlyName']),
                    'notes' => $driver['d_drivingPreference'],
                    'workHours' => 0,
                    'estimatedWorkHours' => 0,
                    'startTime' => 0,
                    'endTime' => 0,
                    'firstName' => $driver['u_firstName'],
                    'lastName' => $driver['u_lastName'],
                    'profile' => $driver['u_profileImage'],
                    'skills' => $currentDriverSkills,
                    'w0' => [],
                    'w1' => [],
                    'w2' => [],
                    'w3' => [],
                    'w4' => [],
                    'w5' => [],
                    'w6' => [],
                ];
            }
        }
        $this->stopwatch->lap('week');

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getScheduleOpenShiftTempDriverRoutesWithParamNew($weekStartEndDay, $userLogin);
        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            if ($_station && (int)$tempOpenDriverRoute['station_id'] !== $_station)
                continue;

            if (empty($tempOpenDriverRoute['routeId'])) {
                $tempOpenShiftRoutesData['empty'][] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = null;
            } else {
                $tempOpenShiftRoutesData[$tempOpenDriverRoute['routeId']] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute['routeId'];
            }
        }
        $this->stopwatch->lap('week');

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteDataNew($weekStartEndDay, $userLogin);
        foreach ($tempDriverRoutes as $tempDriverRoute) {
            if ($_station && (int)$tempDriverRoute['station_id'] !== $_station)
                continue;

            if (empty($tempDriverRoute['routeId'])) {
                $tempRoutesData['empty'][] = $tempDriverRoute;
                $tempRoutesIdArr[] = null;
            } else {
                $tempRoutesData[$tempDriverRoute['routeId']] = $tempDriverRoute;
                $tempRoutesIdArr[] = $tempDriverRoute['routeId'];
            }
        }
        $this->stopwatch->lap('week');

        $openShiftDriverRoutes = $this->getScheduleOpenShiftDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $tempRoutesIdArr);

        $results = $this->getScheduleDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $tempOpenShiftRoutesIdArr);
        $this->stopwatch->lap('week');

        $shiftBiggerWeekCount = $shiftGroupByTypeCount = 0;

        if (isset($tempOpenShiftRoutesData['empty'])) {
            foreach ($tempOpenShiftRoutesData['empty'] as $tempDriverRoute) {
               $openShiftDriverRoutes[] = $tempDriverRoute;
            }
        }
        if ($_station)
            $openShiftDriverRoutes = array_filter($openShiftDriverRoutes, function ($dr) use ($_station) {
                return $_station === (int)$dr['station_id'];
            });

        foreach ($openShiftDriverRoutes as $openShiftDriverRoute) {
            $openShiftDriverRoute = isset($tempOpenShiftRoutesData[$openShiftDriverRoute['id']]) ? $tempOpenShiftRoutesData[$openShiftDriverRoute['id']] : $openShiftDriverRoute;

            $datetimeOpenShift = (new \DateTime($openShiftDriverRoute['dateCreated']))->format('Y-m-d');
            $week = date('w', strtotime($datetimeOpenShift));

            $dayOfWeekOpenShift[$week][] = $this->globalUtils->getShiftDataNew($openShiftDriverRoute, $weekNumber['wknumber'], $requestTimeZone, false);

            if (isset($openShiftDriverRoute['isNew'])) {
                $filterArray['changes'][$_station] = $filterArray['changes'][$_station] ?? [];
                $filterArray['changes'][$_station][] = (new \DateTime($openShiftDriverRoute['createdDate']))->format('U') * 1000;
            }

            $shiftBiggerWeekCount = (count($dayOfWeekOpenShift[$week]) > $shiftBiggerWeekCount) ? count($dayOfWeekOpenShift[$week]) : $shiftBiggerWeekCount;

            $groupByOpenShiftType[$week][$openShiftDriverRoute['shift_id']][] = $dayOfWeekOpenShift[$week];

            $shiftGroupByTypeCount = (count($groupByOpenShiftType[$week]) > $shiftGroupByTypeCount) ? count($groupByOpenShiftType[$week]) : $shiftGroupByTypeCount;
        }

        $groupByOpenShiftTypeShiftsArray = array_reduce(
            $openShiftDriverRoutes,
            function ($prev, $dr) {
                if (!$prev) {
                    return [$dr['shift_id']];
                }

                $str = $dr['shift_id'];

                if (in_array($str, $prev)) {
                    return $prev;
                }

                $prev[] = $str;

                return $prev;
            }
        );

        $groupByOpenShiftTypeShifts = array_count_values(empty($groupByOpenShiftTypeShiftsArray) ? [] : $groupByOpenShiftTypeShiftsArray);

        $groupByOpenShiftTypeShiftsCount = (count($groupByOpenShiftTypeShifts) <= 0) ? 1 : count($groupByOpenShiftTypeShifts);

        $openShiftRoutes = [
            'id' => '0.0.0',
            'qty' => count($openShiftDriverRoutes),
            'shift_requests' => 0,
            'biggerWeek' => $shiftBiggerWeekCount,
            'groupQty' => $groupByOpenShiftTypeShiftsCount,
            'status' => 0,
            'w0' => isset($dayOfWeekOpenShift[0]) ? $dayOfWeekOpenShift[0] : [],
            'w1' => isset($dayOfWeekOpenShift[1]) ? $dayOfWeekOpenShift[1] : [],
            'w2' => isset($dayOfWeekOpenShift[2]) ? $dayOfWeekOpenShift[2] : [],
            'w3' => isset($dayOfWeekOpenShift[3]) ? $dayOfWeekOpenShift[3] : [],
            'w4' => isset($dayOfWeekOpenShift[4]) ? $dayOfWeekOpenShift[4] : [],
            'w5' => isset($dayOfWeekOpenShift[5]) ? $dayOfWeekOpenShift[5] : [],
            'w6' => isset($dayOfWeekOpenShift[6]) ? $dayOfWeekOpenShift[6] : [],
        ];

        if (isset($tempRoutesData['empty'])) {
            foreach ($tempRoutesData['empty'] as $tempDriverRoute) {
                $results[] = $tempDriverRoute;
            }
        }
        $this->stopwatch->lap('week');
        $AlldriverRouteArray=[];

        foreach ($results as $result) {
            $driverRouteArray=[];
            $result = isset($tempRoutesData[$result['id']]) ? $tempRoutesData[$result['id']] : $result;

            if ($_station && (int)$result['station_id'] !== $_station)
                continue;

            if (isset($result['action']) && $result['action'] == 'SHIFT_DELETED') {
                $hasDeleted++;
                continue;
            }

            $datetime = (new \DateTime($result['dateCreated']))->format('Y-m-d H:i:s');
            $datetimeMainShift = (new \DateTime($result['dateCreated']))->format('Y-m-d');
            $week = date('w', strtotime($datetime));

            // Create two new calculate working hour
            $driverRouteStartTimeObj = !empty($result['startTime']) ? new \DateTime($result['startTime']) : new \DateTime($result['shift_startTime']);
            $driverRouteEndTimeObj = !empty($result['endTime']) ? new \DateTime($result['endTime']) : new \DateTime($result['shift_endTime']);
            $date1 = !empty($result['punchIn']) ? new \DateTime($result['punchIn']) : $driverRouteStartTimeObj;
            $date2 = !empty($result['punchOut']) ? new \DateTime($result['punchOut']) : $driverRouteEndTimeObj;

            $diff = $date2->diff($date1);
            $totalMinutes = (($diff->h * 60) + $diff->i) - (!empty($result['unpaidBreak']) ? $result['unpaidBreak'] : $result['shift_unpaidBreak']);
            $currDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $currDate->setTime(0, 0, 0);

            if ((new \DateTime($result['dateCreated'])) < $currDate) {

                if (isset($isPunchedIn[$result['driver_id']][$week])) {
                    if ($isPunchedIn[$result['driver_id']][$week] != true) {
                        $isPunchedIn[$result['driver_id']][$week] = ($result['punchIn'] == null || $result['punchIn'] == "") ? false : true;
                    }
                } else {
                    $isPunchedIn[$result['driver_id']][$week] = ($result['punchIn'] == null || $result['punchIn'] == "") ? false : true;
                }

                for ($day = 0; $day <= 6; $day++) {
                    if ($week == $day) {
                        $shiftHoursByDate[$result['driver_id']][$week] = isset($shiftHoursByDate[$result['driver_id']][$week]) ? $shiftHoursByDate[$result['driver_id']][$week] + $totalMinutes : $totalMinutes;
                    } else {
                        $shiftHoursByDate[$result['driver_id']][$day] = isset($shiftHoursByDate[$result['driver_id']][$day]) ? $shiftHoursByDate[$result['driver_id']][$day] : 0;
                    }
                }

                $attendedShiftHoursOfDriver[$result['driver_id']] = 0;
                for ($day = 0; $day <= 6; $day++) {
                    if (isset($isPunchedIn[$result['driver_id']][$day]) && $isPunchedIn[$result['driver_id']][$day]) {
                        $attendedShiftHoursOfDriver[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']] + $shiftHoursByDate[$result['driver_id']][$day];
                    }
                }

                $pastShiftData[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']];

            } else {
                $futureShiftData[$result['driver_id']] = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] + $totalMinutes : $totalMinutes;
            }

            $pastDateShiftHours = isset($pastShiftData[$result['driver_id']]) ? $pastShiftData[$result['driver_id']] : 0;

            $futureDateShiftHours = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] : 0;

            $completedShiftHours[$result['driver_id']]['hour'] = $pastDateShiftHours;
            $shiftWorkingHours[$result['driver_id']]['hour'] = $pastDateShiftHours + $futureDateShiftHours;

            $dayOfWeek[$result['driver_id']][$week][] = $this->globalUtils->getShiftDataNew($result, $weekNumber['wknumber'], $requestTimeZone, false);

            if (isset($result['isNew'])) {
                $filterArray['changes'][$_station] = $filterArray['changes'][$_station] ?? [];
                $filterArray['changes'][$_station][] = (new \DateTime($result['createdDate']))->format('U') * 1000;
            }

            $footerArray['totalDriverArray'][$result['driver_id']] = $result['driver_id'];
            $footerArray['driverArrayByDay'][$week][] = $result['driver_id'];
            $driverRouteCodes = $this->getEntityManager()->getRepository(DriverRouteCode::class)->getRouteCodeByRouteId($result['routeId']);
            foreach ($driverRouteCodes as $code) {
                $driverRouteArray[] = $code['code'];
                $AlldriverRouteArray[$week][$result['shift_id']][] = $code['code'];
            }
            $footerArray['routeCodeArrayByDay'][$week][] = $driverRouteArray ;
            $footerArray['date'][$week][] = (new \DateTime($result['dateCreated']))->format('Y-m-d');
            $footerArray['shiftArrayForDriverAndRoute'][$week][$result['shift_id']]['shiftDriver'][$result['driver_id']] = $result['shift_id'];
            $d = isset($footerArray['shiftArrayForDriverAndRoute'][$week][$result['shift_id']]['shiftDriver']) ? count($footerArray['shiftArrayForDriverAndRoute'][$week][$result['shift_id']]['shiftDriver']) : 0;
            $r = isset($footerArray['shiftArrayForDriverAndRoute'][$week][$result['shift_id']]['shiftRoute']) ? $footerArray['shiftArrayForDriverAndRoute'][$week][$result['shift_id']]['shiftRoute'] : 0;

            $rate = $r > 0 ? ($d / $r) * 100 : 0;
            $grading = [];
            if ($r > 0) {
                foreach ($gradingRules as $rule) {
                    if (((int)$rule['min'] !== null && (int)$rule['max'] !== null && (int)$rule['min'] <= $rate && (int)$rule['max'] >= $rate)
                        || ((int)$rule['max'] === null && (int)$rule['min'] <= $rate)
                        || ((int)$rule['min'] === null && (int)$rule['max'] >= $rate)) {
                        $grading = [
                            'id' => (int)$rule['id'],
                            'color' => $rule['color'],
                            'textColor' => $rule['textColor'],
                            'description' => $rule['description'],
                        ];
                    }
                }
            }

            $footerArray['shiftArrayByDay'][$week][$result['shift_id']] = [
                'background' => $result['shiftColor'],
                'type' => $result['shiftName'],
                'd' => $d,
                'r' => $r,
                'code'=>isset($AlldriverRouteArray[$week][$result['shift_id']]) ? count(array_unique($AlldriverRouteArray[$week][$result['shift_id']])) : 0 ,
                'grading' => $grading,
                'date' => (new \DateTime($result['dateCreated']))->format('Y-m-d')
            ];

            $countOfShiftInWeek = isset($dayOfWeek[$result['driver_id']][$week]) ? count($dayOfWeek[$result['driver_id']][$week]) : 0;

            $biggerWeekTempValue = isset($biggerWeekCount[$result['driver_id']]) ? $biggerWeekCount[$result['driver_id']] : 0;

            $biggerWeekCount[$result['driver_id']] = ($biggerWeekTempValue < $countOfShiftInWeek) ? $countOfShiftInWeek : $biggerWeekTempValue;

            $totalMinutes = $completedShiftHours[$result['driver_id']]['hour'];
            $minutes = $hours = '00';
            $hours = floor($totalMinutes / 60);
            $minutes = ($totalMinutes % 60);
            $completedShiftHour = (float)$hours . (($minutes > 0) ? '.' . round(($minutes / 60) * 100) : '');
            $completedShiftHour = sprintf('%g', $completedShiftHour);

            $totalMinutes = $shiftWorkingHours[$result['driver_id']]['hour'];
            $minutes = $hours = '00';
            $hours = floor($totalMinutes / 60);
            $minutes = ($totalMinutes % 60);
            $shiftWorkingHour = (float)$hours . (($minutes > 0) ? '.' . round(($minutes / 60) * 100) : '');
            $shiftWorkingHour = sprintf('%g', $shiftWorkingHour);

            $schedulerData[$result['driver_id']] = [
                'biggerWeek' => $biggerWeekCount[$result['driver_id']],
                'id' => (int)$result['driver_id'],
                'clockedIn' => true,
                'driverId' => (int)$result['driver_id'],
                'scheduledId' => !empty($result['schedule_id']) ? (int)$result['schedule_id'] : null,
                'stationId' => (int)$result['station_id'],
                'timezone' => $result['station_timezone'],
                'driver' => $result['user_friendlyName'],
                'notes' => $result['driver_drivingPreference'],
                'employeeStatus'=>$result['driver_employeeStatus'] == Driver::STATUS_ACTIVE ? true : false,
                'inactiveDate'=>!empty($result['driver_inactiveAt']) ?  (new \DateTime($result['driver_inactiveAt']))->format('Y-m-d') : null,
                'terminationDate'=>!empty($result['driver_terminationDate']) ?  (new \DateTime($result['driver_terminationDate']))->format('Y-m-d') : null,
                'offboardedDate'=>!empty($result['driver_offboardedDate']) ?  (new \DateTime($result['driver_offboardedDate']))->format('Y-m-d') : null,
                'workHours' => $completedShiftHour,
                'estimatedWorkHours' => $shiftWorkingHour,
                'startTime' => !empty($result['startTime']) ? (new \DateTime($result['startTime']))->format($this->params->get('time_format')) : (new \DateTime($result['shift_startTime']))->format($this->params->get('time_format')),
                'endTime' => !empty($result['endTime']) ? (new \DateTime($result['endTime']))->format($this->params->get('time_format')) : (new \DateTime($result['shift_endTime']))->format($this->params->get('time_format')),
                'firstName' => $result['user_firstName'],
                'lastName' => $result['user_lastName'],
                'profile' => $result['user_profileImage'],
                'skills' => ($driverSkills[$result['driver_id']] ?? []),
                'w0' => isset($dayOfWeek[$result['driver_id']][0]) ? $dayOfWeek[$result['driver_id']][0] : [],
                'w1' => isset($dayOfWeek[$result['driver_id']][1]) ? $dayOfWeek[$result['driver_id']][1] : [],
                'w2' => isset($dayOfWeek[$result['driver_id']][2]) ? $dayOfWeek[$result['driver_id']][2] : [],
                'w3' => isset($dayOfWeek[$result['driver_id']][3]) ? $dayOfWeek[$result['driver_id']][3] : [],
                'w4' => isset($dayOfWeek[$result['driver_id']][4]) ? $dayOfWeek[$result['driver_id']][4] : [],
                'w5' => isset($dayOfWeek[$result['driver_id']][5]) ? $dayOfWeek[$result['driver_id']][5] : [],
                'w6' => isset($dayOfWeek[$result['driver_id']][6]) ? $dayOfWeek[$result['driver_id']][6] : [],
            ];
            $filterArrayScheduler['drivers'][$result['driver_id']] = ['value' => (int)$result['driver_id'], 'name' => ucfirst($result['user_friendlyName'])];

            //Schedule left sidebar data prepare
            //Stations
            $filterArrayScheduler['stations'][$result['station_id']] =
                ['value' => $result['station_id'], 'name' => $result['station_name']];
            //Schedules
            if (!empty($result['schedule_id'])) {
                $filterArrayScheduler['schedules'][$result['schedule_id']] =
                    ['value' => $result['schedule_id'], 'name' => $result['schedule_name'], 'station' => $result['station_id']];
            }
            //shift types
            $filterArrayScheduler['shiftTypes'][$result['shift_id']] =
                ['value' => $result['shift_id'], 'name' => $result['shiftName'], 'color' => $result['shiftColor']];

        }

        $this->stopwatch->lap('week');

        // All Drivers
        if (!empty($drivers)) {
            foreach ($drivers as $key => $val) {
                $filterArrayScheduler['drivers'][$key] = ['value' => (int)$val['d_id'], 'name' => ucfirst($val['u_friendlyName'])];
            }
        }

        $footerArray['footerData'][0]['totalRoutes'] = $totalRouteCommitment;
        $footerArray['footerData'][0]['RoutesWithDriver'] = count($results);

        for ($day = 0; $day <= 6; $day++) {

            $tempRoute = [];
            if (isset($footerArray['routeCodeArrayByDay'][$day]))
                foreach ($footerArray['routeCodeArrayByDay'][$day] as $rc) {
                    if ($rc) {
                        foreach ($rc as $r) {
                            $tempRoute[] = $r;
                        }
                    }
                }
            $tempRoute = array_unique($tempRoute);
            $footerArray['footerData'][$day + 1]['DriversWithRoute'] = isset($footerArray['driverArrayByDay'][$day]) ? count($footerArray['driverArrayByDay'][$day]) : 0;
            $footerArray['footerData'][$day + 1]['routeCodes'] = isset($footerArray['routeCodeArrayByDay'][$day]) ? count($tempRoute) : 0;
            $footerArray['footerData'][$day + 1]['date'] = isset($footerArray['date'][$day]) ? $footerArray['date'][$day][0] : 0;


            $footerArray['footerData'][$day + 1]['PlanRoutes'] = isset($routeCommitmentArrayByDay[$day]) ? $routeCommitmentArrayByDay[$day] : 0;

            $footerArray['footerData'][$day + 1]['groups'] = isset($footerArray['shiftArrayByDay'][$day]) ? array_values($footerArray['shiftArrayByDay'][$day]) : [];

            $d = $footerArray['footerData'][$day + 1]['DriversWithRoute'];
            $r = $footerArray['footerData'][$day + 1]['PlanRoutes'];
            $rate = $r > 0 ? ($d / $r) * 100 : 0;
            $grading = [];
            if ($r > 0) {
                $shiftArray = array_values($footerArray['shiftArrayByDay'][$day]);
                $tempShift = [];
                foreach ($shiftArray as $tShift) {
                    if (isset($tShift['grading']['id'])) {
                        $tempShift[] = $tShift['grading']['id'];
                    }
                }

                foreach( $gradingRulesIds as $key => $ids){
                    if(in_array( $ids,$tempShift)){
                        $grading = [
                            'color' => $gradingRules[$key]['color'],
                            'textColor' => $gradingRules[$key]['textColor'],
                            'description' => $gradingRules[$key]['description'],
                        ];
                        break;
                    }

                }
            }

            $footerArray['footerData'][$day + 1]['grading'] = $grading;
        }

        $this->stopwatch->lap('week');
        $sort = array();
        foreach ($schedulerData as $k => $v) {
            $sort['driver'][$k] = $v['driver'];
        }
        # sort by event_type desc and then title asc
        array_multisort($sort['driver'], SORT_ASC, $schedulerData);

        //Append openshift data
        array_unshift($schedulerData, $openShiftRoutes);

        $filterArray['schedulerRouteData'] = array_values($schedulerData);

        $ret = $this->getSchedulerBasicFilterData($_station);
        $filterArray['schedulerFilterArray']['schedules'] = $ret['schedules'];
        $filterArray['schedulerFilterArray']['shiftTypes'] = $ret['shiftTypes'];

        $filterArray['schedulerFilterArray']['drivers'] = isset($filterArrayScheduler['drivers']) ? array_values($filterArrayScheduler['drivers']) : [];


        $filterArray['schedulerFooterDataArray'] = $footerArray['footerData'];

        $filterArray['hasDeleted'] = $hasDeleted;
        // $filterArray['changes'] = array_reduce(
        //     $openShiftTempDriverRoutes+$tempDriverRoutes,
        //     function ($all, $dr) {
        //         /** @var DriverRoute $dr */
        //         $all[$dr->getStation()->getId()] = $all[$dr->getStation()->getId()] ?? 0;
        //         $all[$dr->getStation()->getId()] += 1;
        //         return $all;
        //     },
        //     []
        // );

        $this->stopwatch->stop('week');

        return $filterArray;
    }

    private function getSchedulerBasicFilterData($_station = false)
    {
        $ret = [
            'schedules' => [],
            'shiftTypes' => [],
        ];

        if ($_station) {
            $stations = [$this->em->getRepository(Station::class)->find($_station)];
        } else {
            $company = $this->tokenStorage->getToken()->getUser()->getCompany();
            $stations = $this->em->getRepository(Station::class)->findBy([
                'isArchive' => 0,
                'company' => $company,
            ], [
                'name' => 'ASC',
            ]);
        }

        foreach ($stations as $station) {
            $schedules = $this->em->getRepository(Schedule::class)->findBy([
                'isArchive' => 0,
                'station' => $station,
            ], [
                'name' => 'ASC',
            ]);
            foreach ($schedules as $schedule) {
                if (!$_station && !in_array($station->getName(), array_column($ret['schedules'], 'name'))) {
                    $ret['schedules'][] = [
                        'name' => $station->getName(),
                    ];
                }

                $ret['schedules'][] = [
                    'name' => $schedule->getName(),
                    'value' => $schedule->getId(),
                ];
            }

            $shiftTypes = $this->em->getRepository(Shift::class)->findBy([
                'station' => $station,
                'isArchive' => 0,
            ], [
                'name' => 'ASC',
            ]);
            foreach ($shiftTypes as $shiftType) {
                if (!$_station && !in_array($station->getName(), array_column($ret['shiftTypes'], 'name'))) {
                    $ret['shiftTypes'][] = [
                        'name' => $shiftType->getStation()->getName(),
                    ];
                }

                $ret['shiftTypes'][] = [
                    'color' => $shiftType->getColor(),
                    'name' => $shiftType->getName(),
                    'value' => $shiftType->getId(),
                ];
            }
        }

        return $ret;
    }

    private function getSchedulerBasicFilterDataNew($_station = false)
    {
        $ret = [
            'schedules' => [],
            'shiftTypes' => [],
        ];

        if ($_station) {
            $stations = [$this->em->getRepository(Station::class)->find($_station)];
        } else {
            $company = $this->tokenStorage->getToken()->getUser()->getCompany();
            $stations = $this->em->getRepository(Station::class)->findBy([
                'isArchive' => 0,
                'company' => $company,
            ], [
                'name' => 'ASC',
            ]);
        }

        foreach ($stations as $station) {
            $schedules = $this->em->getRepository(Schedule::class)->findBy([
                'isArchive' => 0,
                'station' => $station,
            ], [
                'name' => 'ASC',
            ]);
            foreach ($schedules as $schedule) {
                if (!$_station && !in_array($station->getName(), array_column($ret['schedules'], 'name'))) {
                    $ret['schedules'][] = [
                        'name' => $station->getName(),
                    ];
                }

                $ret['schedules'][] = [
                    'name' => $schedule->getName(),
                    'value' => $schedule->getId(),
                ];
            }

            $shiftTypes = $this->em->getRepository(Shift::class)->findBy([
                'station' => $station,
                'isArchive' => 0,
            ], [
                'name' => 'ASC',
            ]);
            foreach ($shiftTypes as $shiftType) {
                if (!$_station && !in_array($station->getName(), array_column($ret['shiftTypes'], 'name'))) {
                    $ret['shiftTypes'][] = [
                        'name' => $shiftType->getStation()->getName(),
                    ];
                }

                $ret['shiftTypes'][] = [
                    'color' => $shiftType->getColor(),
                    'name' => $shiftType->getName(),
                    'value' => $shiftType->getId(),
                ];
            }
        }

        return $ret;
    }

    public function getStartAndEndDate($week, $year = null)
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }

        $dto = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dto->setISODate($year, $week);

        $dto->modify('-1 days');
        $result['week_start'] = $dto->format('Y-m-d');
        $dto->modify('+6 days');
        $result['week_end'] = $dto->format('Y-m-d');
        return $result;
    }

    public function updateDriverRoutes($scheduleID)
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);

        $scheduleDesign = $this->em->getRepository('App\Entity\ScheduleDesign')->find($scheduleID['id']);

        $input = new ArrayInput(['command' => 'schedule:add:driverroute', 'scheduleId' => $scheduleDesign->getSchedule()->getId()]);
        $output = new BufferedOutput();
        $application->run($input, $output);
        return true;
    }

    public function updateDriverRoutesBySchedule($scheduleID)
    {
        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(['command' => 'schedule:add:driverroute', 'scheduleId' => $scheduleID['id']]);
        $output = new BufferedOutput();
        $application->run($input, $output);
        return true;
    }

    public function getDriverRouteWithDriverCount($company, $shift, $date)
    {
        $results = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 's')
            ->addSelect('s')
            ->where('dr.shiftType = :shiftType')
            ->setParameter('shiftType', $shift)
            ->andWhere('s.company = :company')
            ->setParameter('company', $company)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);
        return count($results->getQuery()->getResult());
    }

    public function getDriverRouteWithoutDriverCount($company, $shift, $date)
    {
        $results = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 's')
            ->addSelect('s')
            ->where('dr.shiftType = :shiftType')
            ->setParameter('shiftType', $shift)
            ->andWhere('s.company = :company')
            ->setParameter('company', $company)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date->format('Y-m-d'))
            ->andWhere('dr.driver IS NULL')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);
        return count($results->getQuery()->getResult());
    }

    public function getDriverRouteByWeekUnmatchShift($week, $shiftArray, $year)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $dateTime = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dateTime->setISODate($year, $week, 0);
        $startDate = $dateTime->format('Y-m-d');
        $dateTime->modify('+6 days');
        $endDate = $dateTime->format('Y-m-d');

        return $qb->select('dr')
            ->from('App\Entity\DriverRoute', 'dr')
            ->where('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('dr.dateCreated <= :endDate')
            ->setParameter('endDate', $endDate)
            ->andWhere($qb->expr()->notIn('dr.shiftType', ':shiftArray'))
            ->setParameter('shiftArray', $shiftArray)
            ->andWhere('dr.driver IS NULL')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult();
    }

    public function getCurrentAndFutureScheduleShiftsByDriver($criteria)
    {
        $currentDate = date("Y-m-d");
        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 's')
            ->where('dr.dateCreated >= :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('s.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driver'])
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getShiftsByDriver($criteria)
    {

        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 's')
            ->andWhere('s.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driver'])
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws Exception
     */
    public function getSheduledShiftOfDriver($criteria)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(00, 00, 00);

        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $date->modify('+14 day');
        $response = $sheduleArray = $rescuerArray = $rescuedArray = [];

        $results = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 's')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['id'])
            ->andWhere('dr.dateCreated >= :today')
            ->setParameter('today', $today)
            ->andWhere('dr.dateCreated <= :nextDay')
            ->setParameter('nextDay', $date)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($results as $result) {
            $startTime = $this->globalUtils->getTimeZoneConversation($result->getStartTime(), $criteria['timezoneOffset']);
            $endTime = $this->globalUtils->getTimeZoneConversation($result->getEndTime(), $criteria['timezoneOffset']);
            $sheduleArray[$result->getId()]['d_formate'] = $result->getDateCreated()->format('Y-m-d');
            $sheduleArray[$result->getId()]['date'] = $result->getDateCreated()->format('D, d M');
            $sheduleArray[$result->getId()]['name'] = $result->getShiftName();
            $sheduleArray[$result->getId()]['color'] = $result->getShiftColor();
            $sheduleArray[$result->getId()]['textColor'] = $result->getShiftTextColor();
            $sheduleArray[$result->getId()]['time'] = $startTime->format($this->params->get('time_format')) . ' - ' . $endTime->format($this->params->get('time_format'));
        }
        $sheduleArray = array_values($sheduleArray);

        $currentQuarter = $this->globalUtils->getDatesOfQuarter('byday', [91, 1]);
        $previousQuarter = $this->globalUtils->getDatesOfQuarter('byday', [181, 92]);

        $currentQuarterShiftCounfOfRescuerAndRescued = $this->createQueryBuilder('dr')
            ->select('COUNT(r.id) as rescuer, COUNT(rd.id) as rescued')
            ->leftJoin($this->_entityName, 'r', 'WITH', 'dr.id= r.id AND r.isRescuer = 1')
            ->leftJoin($this->_entityName, 'rd', 'WITH', 'dr.id= rd.id AND rd.isRescued = 1')
            ->where('dr.driver = :val')
            ->andWhere('dr.dateCreated >= :currentQuarterStartDate')
            ->andWhere('dr.dateCreated <= :currentQuarterEndDate')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('val', $criteria['id'])
            ->setParameter('currentQuarterStartDate', $currentQuarter['start'])
            ->setParameter('currentQuarterEndDate', $currentQuarter['end'])
            ->setParameter('isArchive', false)
            ->groupBy('dr.driver')
            ->getQuery()
            ->getOneOrNullResult();

        $previousQuarterShiftCounfOfRescuerAndRescued = $this->createQueryBuilder('dr')
            ->select('COUNT(r.id) as rescuer, COUNT(rd.id) as rescued')
            ->leftJoin($this->_entityName, 'r', 'WITH', 'dr.id= r.id AND r.isRescuer = 1')
            ->leftJoin($this->_entityName, 'rd', 'WITH', 'dr.id= rd.id AND rd.isRescued = 1')
            ->where('dr.driver = :val')
            ->andWhere('dr.dateCreated >= :previousQuarterStartDate')
            ->andWhere('dr.dateCreated <= :previousQuarterEndDate')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('val', $criteria['id'])
            ->setParameter('previousQuarterStartDate', $previousQuarter['start'])
            ->setParameter('previousQuarterEndDate', $previousQuarter['end'])
            ->setParameter('isArchive', false)
            ->groupBy('dr.driver')
            ->getQuery()
            ->getOneOrNullResult();

        $currentQuarterShiftOfRescuer = $this->createQueryBuilder('dr')
            ->where('dr.driver = :val')
            ->andWhere('dr.dateCreated >= :currentQuarterStartDate')
            ->andWhere('dr.dateCreated <= :currentQuarterEndDate')
            ->andWhere('dr.isArchive = :isArchive')
            ->andWhere('dr.isRescuer = :isRescuer')
            ->setParameter('val', $criteria['id'])
            ->setParameter('currentQuarterStartDate', $currentQuarter['start'])
            ->setParameter('currentQuarterEndDate', $currentQuarter['end'])
            ->setParameter('isArchive', false)
            ->setParameter('isRescuer', true)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($currentQuarterShiftOfRescuer as $result) {
            $startTime = $this->globalUtils->getTimeZoneConversation($result->getStartTime(), $criteria['timezoneOffset']);
            $endTime = $this->globalUtils->getTimeZoneConversation($result->getEndTime(), $criteria['timezoneOffset']);
            $rescuerArray[$result->getId()]['d_formate'] = $result->getDateCreated()->format('Y-m-d');
            $rescuerArray[$result->getId()]['date'] = $result->getDateCreated()->format('D, d M');
            $rescuerArray[$result->getId()]['name'] = $result->getShiftName();
            $rescuerArray[$result->getId()]['color'] = $result->getShiftColor();
            $rescuerArray[$result->getId()]['textColor'] = $result->getShiftTextColor();
            $rescuerArray[$result->getId()]['time'] = $startTime->format($this->params->get('time_format')) . ' - ' . $endTime->format($this->params->get('time_format'));
        }
        $rescuerArray = array_values($rescuerArray);
        $currentQuarterShiftOfRescued = $this->createQueryBuilder('dr')
            ->where('dr.driver = :val')
            ->andWhere('dr.dateCreated >= :currentQuarterStartDate')
            ->andWhere('dr.dateCreated <= :currentQuarterEndDate')
            ->andWhere('dr.isArchive = :isArchive')
            ->andWhere('dr.isRescued = :isRescued')
            ->setParameter('val', $criteria['id'])
            ->setParameter('currentQuarterStartDate', $currentQuarter['start'])
            ->setParameter('currentQuarterEndDate', $currentQuarter['end'])
            ->setParameter('isArchive', false)
            ->setParameter('isRescued', true)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();

        foreach ($currentQuarterShiftOfRescued as $result) {
            $startTime = $this->globalUtils->getTimeZoneConversation($result->getStartTime(), $criteria['timezoneOffset']);
            $endTime = $this->globalUtils->getTimeZoneConversation($result->getEndTime(), $criteria['timezoneOffset']);
            $rescuedArray[$result->getId()]['d_formate'] = $result->getDateCreated()->format('Y-m-d');
            $rescuedArray[$result->getId()]['date'] = $result->getDateCreated()->format('D, d M');
            $rescuedArray[$result->getId()]['name'] = $result->getShiftName();
            $rescuedArray[$result->getId()]['color'] = $result->getShiftColor();
            $rescuedArray[$result->getId()]['textColor'] = $result->getShiftTextColor();
            $rescuedArray[$result->getId()]['time'] = $startTime->format($this->params->get('time_format')) . ' - ' . $endTime->format($this->params->get('time_format'));
        }
        $rescuedArray = array_values($rescuedArray);

        $response['scheduledShift'] = $sheduleArray;
        $response['rescues']['currentQuarterShiftCounfOfRescuerAndRescued'] = $currentQuarterShiftCounfOfRescuerAndRescued;
        $response['rescues']['previousQuarterShiftCounfOfRescuerAndRescued'] = $previousQuarterShiftCounfOfRescuerAndRescued;
        $response['rescuerShifts'] = $rescuerArray;
        $response['rescuedShifts'] = $rescuedArray;
        return $response;
    }

    public function getDriverShiftByDateForDayView($criteria)
    {
        $routeArray = $openShiftArray = $tempOpenShiftRoutesData = $tempOpenShiftRoutesIdArr =
        $tempRoutesData = $tempRoutesIdArr = $backupDriverArray = $mainDriverArray = $shiftArrayForBackupDriver =
        $shiftArrayForMainDriver = $shiftArrayByDriverForBackup = $shiftArrayByDriverForMain = $pastShiftData =
        $futureShiftData = $startTimeArray = $endTimeArray = $hoursArray = $filterArrayScheduler = $backupDrivers =
        $mainDrivers = $completedShiftHours = $shiftWorkingHours = [];

        $_station = $criteria['station'] ?? false;
        if ($_station)
            $_station = (int)$_station;

        $userLogin = $this->tokenStorage->getToken()->getUser();
        $hasDeleted = false;
        $tempRecord = 0;
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $currentPassedDate = $criteria['date'];
        $newDate = new \DateTime();
        $selectedDateForDayView = new \DateTime($currentPassedDate);
        $selectedDateForDayView->setTime($newDate->format('h'), $newDate->format('i'), $newDate->format('s'));
        $selectedDateForDayView = $selectedDateForDayView->setTimezone(new \DateTimeZone('UTC'));
        // $selectedDateForDayView = $this->globalUtils->getTimeZoneConversation($selectedDateForDayView, $requestTimeZone);
        $selectedDateForDayView->setTime(0, 0, 0);

        // Fix to force Sunday to be on the right week
        // if ($selectedDateForDayView->format('l') == 'Sunday') {
        //     $week = $selectedDateForDayView->format('W') + 1;
        // } else {
        //     $week = $selectedDateForDayView->format('W');
        // }
        $week = $criteria['week'];
        $weekStartEndDay = $this->getStartAndEndDate($week, (int)$criteria['year']);

        $currDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currDate = $this->globalUtils->getTimeZoneConversation($currDate, $requestTimeZone);
        $currDate->setTime(0, 0, 0);

        $drivers = $this->getEntityManager()->getRepository('App\Entity\Driver')->getAllDriversNew($userLogin->getCompany()->getId(), $_station);

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getScheduleOpenShiftTempDriverRoutesWithParamNew($weekStartEndDay, $userLogin);
        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            $routeArray['changes'][$tempOpenDriverRoute['station_id']] = $routeArray['changes'][$tempOpenDriverRoute['station_id']] ?? [];
            $routeArray['changes'][$tempOpenDriverRoute['station_id']][] = (new \DateTime($tempOpenDriverRoute['createdDate']))->format('U') * 1000;

            if (empty($tempOpenDriverRoute['routeId'])) {
                $tempOpenShiftRoutesData['empty'][] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = null;
            } else {
                $tempOpenShiftRoutesData[$tempOpenDriverRoute['routeId']] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute['routeId'];
            }
        }

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteDataForDayViewNew($currentPassedDate, $userLogin);
        foreach ($tempDriverRoutes as $tempDriverRoute) {
            $routeArray['changes'][$tempDriverRoute['station_id']] = $routeArray['changes'][$tempDriverRoute['station_id']] ?? [];
            $routeArray['changes'][$tempDriverRoute['station_id']][] = (new \DateTime($tempDriverRoute['createdDate']))->format('U') * 1000;

            if (empty($tempDriverRoute['routeId'])) {
                $tempRoutesData['empty'][] = $tempDriverRoute;
                $tempRoutesIdArr[] = null;
            } else {
                $tempRoutesData[$tempDriverRoute['routeId']] = $tempDriverRoute;
                $tempRoutesIdArr[] = $tempDriverRoute['routeId'];
            }
        }

        $tempRecord = count($tempDriverRoutes);

        $openShiftDriverRoutes = $this->getScheduleOpenShiftDriverRoutesWithParamForDayViewNew($currentPassedDate, $userLogin, $tempRoutesIdArr);

        foreach ($openShiftDriverRoutes as $openShiftDriverRoute) {
            $openShiftDriverRoute = isset($tempOpenShiftRoutesData[$openShiftDriverRoute['id']]) ? $tempOpenShiftRoutesData[$openShiftDriverRoute['id']] : $openShiftDriverRoute;

            $tempRecord = !empty($openShiftDriverRoute['routeId']) ? ($tempRecord + 1) : $tempRecord;
            $openShiftArray[] = $this->globalUtils->getShiftDataNew($openShiftDriverRoute, $week, $requestTimeZone, false);

            $driverRouteStartTimeObj = !empty($openShiftDriverRoute['startTime']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['startTime']),$requestTimeZone) : $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['shift_startTime'],$requestTimeZone));
            $driverRouteEndTimeObj = !empty($openShiftDriverRoute['endTime']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['endTime']),$requestTimeZone) : $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['shift_endTime']),$requestTimeZone);

            $startTimeObj = !empty($openShiftDriverRoute['punchIn']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['punchIn']),$requestTimeZone) : $driverRouteStartTimeObj;
            $endTimeObj = !empty($openShiftDriverRoute['punchOut']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($openShiftDriverRoute['punchOut']),$requestTimeZone) : $driverRouteEndTimeObj;

            $startTimeArray[] = (int)date("G", strtotime($startTimeObj->format($this->params->get('time_format'))));
            if (date("G", strtotime($endTimeObj->format($this->params->get('time_format')))) >= date("G:i", strtotime($endTimeObj->format($this->params->get('time_format'))))) {
                $endTimeArray[] = (int)date("G", strtotime($endTimeObj->format($this->params->get('time_format'))));
            } else {
                $endTimeArray[] = (int)date("G", strtotime($endTimeObj->format($this->params->get('time_format')))) + 1;
            }
        }

        $results = $this->getScheduleDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $tempOpenShiftRoutesIdArr);
        foreach ($results as $result) {
            $result = isset($tempRoutesData[$result['id']]) ? $tempRoutesData[$result['id']] : $result;

            if (isset($result['action']) && $result['action'] == 'SHIFT_DELETED') {
                $hasDeleted++;
                continue;
            }

            if (empty($result['driver_id'])) {
                continue;
            }

            $datetime = (new \DateTime($result['dateCreated']))->format('Y-m-d H:i:s');
            $datetimeMainShift = (new \DateTime($result['dateCreated']))->format('Y-m-d');
            $dayNumber = date('w', strtotime($datetime));

            // Create two new calculate working hour

            $driverRouteStartTimeObj = !empty($result['startTime']) ? new \DateTime($result['startTime']) : new \DateTime($result['shift_startTime']);
            $driverRouteEndTimeObj = !empty($result['endTime']) ? new \DateTime($result['endTime']) : new \DateTime($result['shift_endTime']);

            $date1 = !empty($result['punchIn']) ? new \DateTime($result['punchIn']) : $driverRouteStartTimeObj;
            $date2 = !empty($result['punchOut']) ? new \DateTime($result['punchOut']) : $driverRouteEndTimeObj;

            $diff = $date2->diff($date1);
            $totalMinutes = (($diff->h * 60) + $diff->i) - (!empty($result['unpaidBreak']) ? $result['unpaidBreak'] : $result['shift_unpaidBreak']);
            if ((new \DateTime($result['dateCreated']))->format('Y-m-d') < $currDate->format('Y-m-d')) {

                if (isset($isPunchedIn[$result['driver_id']][$dayNumber])) {
                    if ($isPunchedIn[$result['driver_id']][$dayNumber] != true) {
                        $isPunchedIn[$result['driver_id']][$dayNumber] = empty($result['punchIn']) ? false : true;
                    }
                } else {
                    $isPunchedIn[$result['driver_id']][$dayNumber] = empty($result['punchIn']) ? false : true;
                }

                for ($day = 0; $day <= 6; $day++) {
                    if ($dayNumber == $day) {
                        $shiftHoursByDate[$result['driver_id']][$dayNumber] = isset($shiftHoursByDate[$result['driver_id']][$dayNumber]) ? $shiftHoursByDate[$result['driver_id']][$dayNumber] + $totalMinutes : $totalMinutes;
                    } else {
                        $shiftHoursByDate[$result['driver_id']][$day] = isset($shiftHoursByDate[$result['driver_id']][$day]) ? $shiftHoursByDate[$result['driver_id']][$day] : 0;
                    }
                }

                $attendedShiftHoursOfDriver[$result['driver_id']] = 0;
                for ($day = 0; $day <= 6; $day++) {
                    if (isset($isPunchedIn[$result['driver_id']][$day]) && $isPunchedIn[$result['driver_id']][$day]) {
                        $attendedShiftHoursOfDriver[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']] + $shiftHoursByDate[$result['driver_id']][$day];
                    }
                }

                $pastShiftData[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']];
            } else {
                $futureShiftData[$result['driver_id']] = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] + $totalMinutes : $totalMinutes;
            }

            $pastDateShiftHours = isset($pastShiftData[$result['driver_id']]) ? $pastShiftData[$result['driver_id']] : 0;

            $futureDateShiftHours = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] : 0;

            $completedShiftHours[$result['driver_id']]['hour'] = $pastDateShiftHours;
            $shiftWorkingHours[$result['driver_id']]['hour'] = $pastDateShiftHours + $futureDateShiftHours;

            if ((new \DateTime($result['dateCreated']))->format('Y-m-d') == $selectedDateForDayView->format('Y-m-d')) {

                $codeArr = [];
                $routeCodes = $this->getEntityManager()->getRepository(DriverRouteCode::class)->getRouteCodeByRouteId($result['routeId']);
                if (!empty($routeCodes)) {
                    $codeArr = array_column($routeCodes,'code');
                }

                if ($result['shift_category'] == \App\Entity\Shift::BACKUP) {
                    $backupDrivers[$result['driver_id']] = $result['driver_id'];
                    $shiftArrayForBackupDriver[$result['driver_id']]['shifts'][] = $this->globalUtils->getShiftDataNew($result, $week, $requestTimeZone, false);

                } else {
                    $mainDrivers[$result['driver_id']] = $result['driver_id'];
                    $shiftDataArray = $this->globalUtils->getShiftDataNew($result, $week, $requestTimeZone, true);

                    $driverRouteStartTimeObj = !empty($result['startTime']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($result['startTime']),$requestTimeZone) : $this->globalUtils->getTimeZoneConversation(new \DateTime($result['shift_startTime']),$requestTimeZone);
                    
                    $shiftStartTime = !empty($result['punchIn']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($result['punchIn']),$requestTimeZone) : $driverRouteStartTimeObj;
                    $shiftStartTime = $shiftStartTime->format('G.i');
                    $shiftArrayForMainDriver[$result['driver_id']]['shift_' . $shiftStartTime] = $shiftDataArray;
                }



                $startTimeObj = !empty($result['punchIn']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($result['punchIn']),$requestTimeZone) :$driverRouteStartTimeObj ;
                $endTimeObj = !empty($result['punchOut']) ? $this->globalUtils->getTimeZoneConversation(new \DateTime($result['punchOut']),$requestTimeZone) :$driverRouteEndTimeObj ;

                $startTimeArray[] = (int)date("G", strtotime($startTimeObj->format($this->params->get('time_format'))));
                if (date("G", strtotime($endTimeObj->format($this->params->get('time_format')))) >= date("G:i", strtotime($endTimeObj->format($this->params->get('time_format'))))) {
                    $endTimeArray[] = (int)date("G", strtotime($endTimeObj->format($this->params->get('time_format'))));
                } else {
                    $endTimeArray[] = (int)date("G", strtotime($endTimeObj->format($this->params->get('time_format')))) + 1;
                }
            }
        }

        $backupDrivers = array_values($backupDrivers);
        $mainDrivers = array_values($mainDrivers);
        $schedulesByDrivers = array_reduce($this->em->getConnection()->query("
            SELECT
                sd.*
            FROM
                schedule_driver sd
            LEFT JOIN
                driver d ON sd.driver_id = d.id
        ")->fetchAll(), function ($list, $row) {
            $list[$row['driver_id']] = $list[$row['driver_id']] ?? [];
            $list[$row['driver_id']][] = $row['schedule_id'];
            return $list;
        }, []);

        $stationsByDrivers = array_reduce($this->em->getConnection()->query("
            SELECT
                ds.*
            FROM
                driver_station ds
            LEFT JOIN
                driver d ON ds.driver_id = d.id
        ")->fetchAll(), function ($list, $row) {
            $list[$row['driver_id']] = $list[$row['driver_id']] ?? [];
            $list[$row['driver_id']][] = $row['station_id'];
            return $list;
        }, []);

        foreach ($drivers as $key => $val) {
            $filterArrayScheduler['drivers'][$key] = ['value' => $val['id'], 'name' => $val['friendlyName']];
            $completedShiftHour = 0;
            if (isset($completedShiftHours[$val['id']])) {
                $totalMinutes = $completedShiftHours[$val['id']]['hour'];
                $hours = floor($totalMinutes / 60);
                $minutes = ($totalMinutes % 60);
                $completedShiftHour = (float)$hours . (($minutes > 0) ? '.' . round(($minutes / 60) * 100) : '');
                $completedShiftHour = sprintf('%g', $completedShiftHour);
            }
            $shiftWorkingHour = 0;
            if (isset($shiftWorkingHours[$val['id']])) {
                $totalMinutes = $shiftWorkingHours[$val['id']]['hour'];
                $hours = floor($totalMinutes / 60);
                $minutes = ($totalMinutes % 60);
                $shiftWorkingHour = (float)$hours . (($minutes > 0) ? '.' . round(($minutes / 60) * 100) : '');
                $shiftWorkingHour = sprintf('%g', $shiftWorkingHour);
            }

            if (in_array($val['id'], $backupDrivers)) {
                $backupDriverArray[$val['id']]['id'] = (int)$val['id'];
                $backupDriverArray[$val['id']]['name'] = $val['friendlyName'];
                $backupDriverArray[$val['id']]['workHours'] = $completedShiftHour;
                $backupDriverArray[$val['id']]['expectedWorkHours'] = $shiftWorkingHour;
                $backupDriverArray[$val['id']]['stationId'] = $stationsByDrivers[$val['id']][0];
                $backupDriverArray[$val['id']]['notes'] = $val['drivingPreference'];
                $backupDriverArray[$val['id']]['profile'] = $val['profileImage'];
                $backupDriverArray[$val['id']]['shifts'] = (isset($shiftArrayForBackupDriver[$val['id']])) ? $shiftArrayForBackupDriver[$val['id']]['shifts'] : [];
            }

            $nameInfo = $val['friendlyName'];
            $nameArray = explode(' ', $nameInfo);
            $mainDriverArray[$val['id']]['id'] = (int)$val['id'];
            $mainDriverArray[$val['id']]['name'] = $nameInfo;
            $mainDriverArray[$val['id']]['lastName'] = (isset($nameArray[1])) ? $nameArray[1] : '';
            $mainDriverArray[$val['id']]['workHours'] = $completedShiftHour;
            $mainDriverArray[$val['id']]['expectedWorkHours'] = $shiftWorkingHour;
            $mainDriverArray[$val['id']]['employeeStatus'] = $val['employeeStatus'] == 2 ? true : false;
            $mainDriverArray[$val['id']]['inactiveDate'] = !empty($val['inactiveAt']) ?  (new \DateTime($val['inactiveAt']))->format('Y-m-d') : null;
            $mainDriverArray[$val['id']]['terminationDate'] = !empty($val['terminationDate']) ?  (new \DateTime($val['terminationDate']))->format('Y-m-d') : null;
            $mainDriverArray[$val['id']]['offboardedDate'] = !empty($val['offboardedDate']) ?  (new \DateTime($val['offboardedDate']))->format('Y-m-d') : null;
            $mainDriverArray[$val['id']]['stationId'] = $stationsByDrivers[$val['id']][0];
            $mainDriverArray[$val['id']]['notes'] = $val['drivingPreference'];
            $mainDriverArray[$val['id']]['profile'] = $val['profileImage'];
            $mainDriverArray[$val['id']]['schedules'] = $schedulesByDrivers[$val['id']] ?? [];
            $shiftArrayForMainDriver[$val['id']] = isset($shiftArrayForMainDriver[$val['id']]) ? $shiftArrayForMainDriver[$val['id']] : [];
            $mainDriverArray[$val['id']] = array_merge($mainDriverArray[$val['id']], $shiftArrayForMainDriver[$val['id']]);
        }

        asort($startTimeArray);
        asort($endTimeArray);

        if (count($startTimeArray) > 0) {
            $startPoint = array_values($startTimeArray)[0];
            $endPoint = array_values($endTimeArray)[count($endTimeArray) - 1];
            if ($startPoint <= $endPoint) {
                for ($i = $startPoint; $i <= $endPoint; $i++) {
                    array_push($hoursArray, ['hour' => (int)$i, 'text' => date("ga", strtotime($i . ':00:00'))]);
                }
            } else {
                for ($i = $startPoint; $i <= 23; $i++) {
                    array_push($hoursArray, ['hour' => (int)$i, 'text' => date("ga", strtotime($i . ':00:00'))]);
                }
                for ($i = 0; $i <= $endPoint; $i++) {
                    array_push($hoursArray, ['hour' => (int)$i, 'text' => date("ga", strtotime($i . ':00:00'))]);
                }
            }
        }

        $filterData = $this->getSchedulerBasicFilterDataNew();
        $routeArray['filter']['schedules'] = $filterData['schedules'];
        $routeArray['filter']['shiftTypes'] = $filterData['shiftTypes'];

        $routeArray['filter']['drivers'] = isset($filterArrayScheduler['drivers']) ? array_values($filterArrayScheduler['drivers']) : [];
        $routeArray['openShifts'] = array_values($openShiftArray);
        $routeArray['backupDrivers'] = array_values($backupDriverArray);
        $routeArray['drivers'] = array_values($mainDriverArray);
        $routeArray['hours'] = $hoursArray;

        return $routeArray;
    }

    public function getPublishedScheduleOfDriverRoutes($weekStartEndDay, $company)
    {

        $qb = $this->createQueryBuilder('dr')
            ->select('dr', 'sf', 'st', 'bg')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('sf.balanceGroup', 'bg')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated >= :sunday')
            ->andWhere('dr.dateCreated <= :saturday')
            ->setParameter('sunday', $weekStartEndDay['w0'])
            ->setParameter('saturday', $weekStartEndDay['w6'])
            ->andWhere('sf.company = :company')
            ->setParameter('company', $company)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        return $qb->getQuery()->getScalarResult();
    }

    public function getPublishedScheduleOfDriverRoutesByBalanceGroup($currentSelectedDate, $company, $balanceGroupId)
    {

        $qb = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('sf.balanceGroup', 'bg')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currentSelectedDate')
            ->setParameter('currentSelectedDate', $currentSelectedDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $company)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('bg.id = :balanceGroup')
            ->setParameter('balanceGroup', $balanceGroupId);

        return $qb->getQuery()->getResult();
    }

    public function schedulerLoadOutAddDriverSave($criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = isset($criteria['date']) ? (new \DateTime($criteria['date'])) : (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $currentDateObj->format('Y-m-d');

        $userLogin = $this->tokenStorage->getToken()->getUser();
        $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->insertRecordForSchedulerLoadoutAddDriverInDriverAndTempDriver($criteria['shift'], $userLogin, $currentDateObj, $criteria['driverId'], $criteria['startTime'], $criteria['endTime'], $criteria['invoiceType']);

        return true;
    }

    public function getRoutesFromShiftAndSkill($criteria)
    {

        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currentDate = $date->format('Y-m-d');

        $qb = $this->createQueryBuilder('dr')
            ->where('dr.dateCreated >= :currentDate')
            ->setParameter('currentDate', $currentDate);

        if ($criteria['type'] === 'skill') {
            $qb->andWhere('dr.skill = :skill')
                ->setParameter('skill', $criteria['id']);
        } else {
            $qb->andWhere('dr.shiftType = :shiftType')
                ->setParameter('shiftType', $criteria['id']);
        }

        $qb->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        $results = $qb->getQuery()->getResult();
        $sheduleArray = [];
        $sheduleNameArray = [];
        foreach ($results as $result) {
            if ($result->getSchedule()) {
                $sheduleNameArray[$result->getSchedule()->getId()] = $result->getSchedule()->getName();
                $sheduleArray[$result->getSchedule()->getId()][] = !empty($result->getDriver()) ? $result->getDriver()->getUser()->getFirstName() : null;
            } else {
                if ($result->getDriver()) {
                    $sheduleNameArray[0] = 'No Schedule Involved';
                    $sheduleArray[0][] = $result->getDriver()->getUser()->getFirstName();
                }
            }
        }
        $schedule = [];
        foreach ($sheduleNameArray as $key => $value) {
            $driver = '';
            foreach (array_unique($sheduleArray[$key]) as $key => $d) {
                $driver .= $d . ',';
            }
            $schedule[] = ['schedule' => $value, 'driver' => rtrim($driver, ", ")];
        }
        return $schedule;
    }

    public function updateFutureDateRoutes($criteria)
    {

        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(0, 0, 0);

        $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($criteria['id']);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.shiftName', ':shiftName')
            ->setParameter('shiftName', $shift->getName())
            ->set('dr.shiftColor', ':shiftColor')
            ->setParameter('shiftColor', $shift->getColor())
            ->set('dr.unpaidBreak', ':unpaidBreak')
            ->setParameter('unpaidBreak', $shift->getUnpaidBreak())
            ->set('dr.shiftNote', ':shiftNote')
            ->setParameter('shiftNote', $shift->getNote())
            ->set('dr.shiftInvoiceType', ':shiftInvoiceType')
            ->setParameter('shiftInvoiceType', $shift->getInvoiceType())
            ->set('dr.shiftStation', ':shiftStation')
            ->setParameter('shiftStation', $shift->getStation())
            ->set('dr.shiftSkill', ':shiftSkill')
            ->setParameter('shiftSkill', $shift->getSkill())
            ->set('dr.shiftCategory', ':shiftCategory')
            ->setParameter('shiftCategory', $shift->getCategory())
            ->set('dr.shiftBalanceGroup', ':shiftBalanceGroup')
            ->setParameter('shiftBalanceGroup', $shift->getBalanceGroup())
            ->set('dr.shiftTextColor', ':shiftTextColor')
            ->setParameter('shiftTextColor', $shift->getTextColor())
            ->set('dr.startTime', ':startTime')
            ->setParameter('startTime', $shift->getStartTime())
            ->set('dr.endTime', ':endTime')
            ->setParameter('endTime', $shift->getEndTime())
            ->where('dr.shiftType = :shiftType')
            ->setParameter('shiftType', $criteria['id'])
            ->andWhere('dr.dateCreated > :currentDate')
            ->setParameter('currentDate', $today)
            ->getQuery()
            ->execute();

        $tempDR = $this->getEntityManager()->createQueryBuilder();
        $tempDR->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.shiftName', ':shiftName')
            ->setParameter('shiftName', $shift->getName())
            ->set('tdr.shiftColor', ':shiftColor')
            ->setParameter('shiftColor', $shift->getColor())
            ->set('tdr.unpaidBreak', ':unpaidBreak')
            ->setParameter('unpaidBreak', $shift->getUnpaidBreak())
            ->set('tdr.shiftNote', ':shiftNote')
            ->setParameter('shiftNote', $shift->getNote())
            ->set('tdr.shiftInvoiceType', ':shiftInvoiceType')
            ->setParameter('shiftInvoiceType', $shift->getInvoiceType())
            ->set('tdr.shiftStation', ':shiftStation')
            ->setParameter('shiftStation', $shift->getStation())
            ->set('tdr.shiftSkill', ':shiftSkill')
            ->setParameter('shiftSkill', $shift->getSkill())
            ->set('tdr.shiftCategory', ':shiftCategory')
            ->setParameter('shiftCategory', $shift->getCategory())
            ->set('tdr.shiftBalanceGroup', ':shiftBalanceGroup')
            ->setParameter('shiftBalanceGroup', $shift->getBalanceGroup())
            ->set('tdr.shiftTextColor', ':shiftTextColor')
            ->setParameter('shiftTextColor', $shift->getTextColor())
            ->set('tdr.startTime', ':startTime')
            ->setParameter('startTime', $shift->getStartTime())
            ->set('tdr.endTime', ':endTime')
            ->setParameter('endTime', $shift->getEndTime())
            ->where('tdr.shiftType = :shiftType')
            ->setParameter('shiftType', $criteria['id'])
            ->andWhere('tdr.dateCreated > :currentDate')
            ->setParameter('currentDate', $today)
            ->getQuery()
            ->execute();

        return true;
    }

    public function updateAllRoutes()
    {
        // $stations = [50, 54, 62, 63, 66];
        // foreach ($stations as $station) {
        //     $routes = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->findBy(['station' => $station]);
        //     foreach ($routes as $route) {
        //         if ($route->getShiftType()) {
        //             $startTime = $route->getStartTime();
        //             $endTime = $route->getEndTime();
        //             $startTime = $startTime->modify("+300 minutes");
        //             $endTime = $endTime->modify("+300 minutes");

        //             $punchInTime = null;
        //             if (!empty($route->getPunchIn())) {
        //                 $punchInTime = $route->getPunchIn();
        //                 $punchInTime = $punchInTime->modify("+300 minutes");
        //             }

        //             $punchOutTime = null;
        //             if (!empty($route->getPunchOut())) {
        //                 $punchOutTime = $route->getPunchOut();
        //                 $punchOutTime = $punchOutTime->modify("+300 minutes");
        //             }

        //             $qb = $this->getEntityManager()->createQueryBuilder();
        //             $qb->update('App\Entity\DriverRoute', 'dr')
        //                 ->set('dr.startTime', ':startTime')
        //                 ->setParameter('startTime', $startTime)
        //                 ->set('dr.endTime', ':endTime')
        //                 ->setParameter('endTime', $endTime)
        //                 ->set('dr.punchIn', ':punchIn')
        //                 ->setParameter('punchIn', $punchInTime)
        //                 ->set('dr.punchOut', ':punchOut')
        //                 ->setParameter('punchOut', $punchOutTime)
        //                 ->where('dr.id = :drid')
        //                 ->setParameter('drid', $route->getId())
        //                 ->getQuery()
        //                 ->execute();
        //         }
        //     }
        //     $shifts = $this->getEntityManager()->getRepository('App\Entity\Shift')->findBy(['station' => $station]);
        //     foreach ($shifts as $shift) {
        //         $startTime = $shift->getStartTime();
        //         $endTime = $shift->getEndTime();
        //         $startTime = $startTime->modify("+300 minutes");
        //         $endTime = $endTime->modify("+300 minutes");

        //         $qb = $this->getEntityManager()->createQueryBuilder();
        //         $qb->update('App\Entity\Shift', 'sf')
        //             ->set('sf.startTime', ':startTime')
        //             ->setParameter('startTime', $startTime)
        //             ->set('sf.endTime', ':endTime')
        //             ->setParameter('endTime', $endTime)
        //             ->where('sf.id = :shiftid')
        //             ->setParameter('shiftid', $shift->getId())
        //             ->getQuery()
        //             ->execute();
        //     }
        // }

        return true;
    }

    public function removeDriverRoute($criteria)
    {
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['id']);

        $return = 'complete-deleted';
        if ($driverRoute->getDriver()) {
            $tempCriteria['timezoneOffset'] = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
            $tempCriteria['newDriverId'] = $driverRoute->getDriver()->getId();
            $tempCriteria['routeId'] = $driverRoute->getId();
            $tempCriteria['isNew'] = true;
            $tempCriteria['newDate'] = $driverRoute->getDateCreated()->format('Y-m-d 00:00:00');
            $tempCriteria['shiftToDate'] = $driverRoute->getDateCreated()->format('Y-m-d');
            $tempCriteria['isTemp'] = $driverRoute->getIsTemp();
            $tempCriteria['isOpenShift'] = false;
            $tempCriteria['shiftId'] = $driverRoute->getShiftType()->getId();
            $tempCriteria['startTime'] = $driverRoute->getStartTime()->format('H:i');
            $tempCriteria['endTime'] = $driverRoute->getEndTime()->format('H:i');
            $tempCriteria['shiftInvoiceType'] = !empty($driverRoute->getShiftInvoiceType()) ? $driverRoute->getShiftInvoiceType()->getId() : null;
            $tempCriteria['hasAlert'] = true;
            $tempCriteria['action'] = 'SHIFT_DELETED';
            $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->addTempDriverRoute($tempCriteria);
            $return = 'unpublish-deleted';
        } else {
            if ($driverRoute->getIsTemp()) {
                $this->getEntityManager()->remove($driverRoute);
                $this->getEntityManager()->flush();
            } else {
                if ($criteria['isNew']) {
                    $results = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->findBy(['routeId' => $driverRoute->getId(), 'isNew' => 1]);
                    if (count($results) == 1) {
                        $qb = $this->getEntityManager()->createQueryBuilder();
                        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
                            ->where('tdr.routeId = :routeId')
                            ->setParameter('routeId', $driverRoute->getId())
                            ->andWhere('tdr.isNew = :isNew')
                            ->setParameter('isNew', 0)
                            ->andWhere('tdr.createdBy IS NULL')
                            ->getQuery()
                            ->execute();
                    }

                    foreach ($results as $result) {
                        $this->getEntityManager()->remove($result);
                    }
                    $this->getEntityManager()->flush();
                }

                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.isArchive', ':makeArchive')
                    ->setParameter('makeArchive', true)
                    ->where('dr.id = :id')
                    ->setParameter('id', $criteria['id'])
                    ->getQuery()->execute();
            }
        }

        return $return;
    }

    public function addUpdateTrainAndDuty($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $data = $criteria['data'];
        $date = new DateTime($criteria['shiftDate']);
        $week = $date->format("W");
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $driverRouteOld = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['id']);

        if ($criteria['isEdit'] == true) {
            $driverRouteOld->setStartTime(new DateTime($data['startTime']));
            $driverRouteOld->setEndTime(new DateTime($data['endTime']));
            $driverRouteOld->setNewNote($data['newNote']);
            $driverRouteOld->setIsAddTrain($criteria['isTrain']);
            $driverRouteOld->setIsLightDuty($criteria['isDuty']);
            $this->getEntityManager()->persist($driverRouteOld);
            $uow = $this->getEntityManager()->getUnitOfWork();
            $uow->computeChangeSets();
            $shiftDiffrances = $uow->getEntityChangeSet($driverRouteOld);
            $this->getEntityManager()->flush();

            if ($criteria['isTrain']) {
                $data = ['driverRoute' => $driverRouteOld, 'isTrain' => true, 'isEdit' => true, 'shiftDiffrances' => $shiftDiffrances];
                $this->addTrainOrDutyEventData($data);
            }
            if ($criteria['isDuty']) {
                $data = ['driverRoute' => $driverRouteOld, 'isDuty' => true, 'isEdit' => true, 'shiftDiffrances' => $shiftDiffrances];
                $this->addTrainOrDutyEventData($data);
            }
            $driverRouteObj = $driverRouteOld;
        } else {
            $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($data['shiftType']);
            $driverRouteOld->setShiftType($shift);
            $driverRouteOld->setShiftName($shift->getName());
            $driverRouteOld->setShiftColor($shift->getColor());
            $driverRouteOld->setUnpaidBreak($shift->getUnpaidBreak());
            $driverRouteOld->setShiftStation($shift->getStation());
            $driverRouteOld->setShiftSkill($shift->getSkill());
            $driverRouteOld->setShiftCategory($shift->getCategory());
            $driverRouteOld->setShiftBalanceGroup($shift->getBalanceGroup());
            $driverRouteOld->setShiftTextColor($shift->getTextColor());
            $driverRouteOld->setNewNote($data['newNote']);
            $driverRouteOld->setIsAddTrain($criteria['isTrain']);
            $driverRouteOld->setIsLightDuty($criteria['isDuty']);

            $this->getEntityManager()->persist($driverRouteOld);
            $this->getEntityManager()->flush();

            $qb = $this->createQueryBuilder('dr');
            $trainees = $qb->andWhere(
                $qb->expr()->in(
                    'dr.driver',
                    $criteria['isTrain'] ? $data['trainees'] : [$data['pairDriver']]
                ))
                ->addCriteria(Common::notArchived('dr'))
                ->andWhere('dr.dateCreated = :shiftDate')
                ->setParameter('shiftDate', $criteria['shiftDate'])
                ->getQuery()
                ->getResult();

            foreach ($trainees as $trainee) {
                $trainee->setOldDriverRoute($driverRouteOld);
                $this->getEntityManager()->persist($trainee);
            }

            if ($criteria['isTrain']) {
                $data = ['driverRoute' => $driverRouteOld, 'isTrain' => true, 'isEdit' => false];
                $this->addTrainOrDutyEventData($data);
                $messageData = array('user_name' => $userLogin->getFriendlyName());
                $message = $this->getEntityManager()->getRepository('App\Entity\Event')->getMessageByType('ADD_TRAIN', $messageData, 300);
            }
            if ($criteria['isDuty']) {
                $data = ['driverRoute' => $driverRouteOld, 'isDuty' => true, 'isEdit' => false];
                $this->addTrainOrDutyEventData($data);
                $messageData = array('user_name' => $userLogin->getFriendlyName());
                $message = $this->getEntityManager()->getRepository('App\Entity\Event')->getMessageByType('ADD_DUTY', $messageData, 300);
            }
            $currentDate = new \DateTime('now');
            $driverRouteOld->setNewNote($driverRouteOld->getNewNote() . ' => ' . $currentDate->format('Y-m-d') . ' : ' . $message);
            $this->em->persist($driverRouteOld);
            $this->getEntityManager()->flush();
        }

        return $this->globalUtils->getShiftData($driverRouteOld, $week, $requestTimeZone);
    }

    public function AddRescueShift($criteria)
    {
        $data = $criteria['data'];

        $date = new DateTime($criteria['shiftDate']);
        $week = $date->format("W");

        if ($data['shiftInvoiceType'] == 0) {
            $shiftInvoiceType = null;
        } else {
            $shiftInvoiceType = $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($data['shiftInvoiceType']);
        }

        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $codeArr = [];

        /** @var DriverRoute $driverRouteOld */
        $driverRouteOld = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['id']);

        $note = $newNote = $currentNote = '';

        if (isset($data['note'])) {
            $note = $data['note'] . "\n\r";
            $currentNote = $data['note'];
        }

        if (isset($data['newNote'])) {
            $note = $note . $data['newNote'];
            $newNote = $data['newNote'];
        }
        $userLogin = $this->tokenStorage->getToken()->getUser();

        if ($criteria['isEdit'] == true) {
            $code = $driverRouteOld->getDriverRouteCodes()[0];

            $driverRouteOld->setStartTime(new DateTime($data['startTime']));
            $driverRouteOld->setDatetimeEnded(new DateTime($data['endTime']));
            $driverRouteOld->setNumberOfPackage($data['numberOfPackage']);
            $driverRouteOld->setShiftNote($note);

            $driverRouteOld->setCurrentNote($currentNote);
            $driverRouteOld->setNewNote($newNote);

            $driverRouteOld->setShiftInvoiceType($shiftInvoiceType);
            $this->getEntityManager()->persist($driverRouteOld);
            $uow = $this->getEntityManager()->getUnitOfWork();
            $uow->computeChangeSets();
            $shiftDiffrances = $uow->getEntityChangeSet($driverRouteOld);

            if (!empty($shiftDiffrances)) {
                $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->editRescueShiftEventDataByObject($shiftDiffrances, $driverRouteOld->getShiftType(), $userLogin, $driverRouteOld);
            }
            $this->getEntityManager()->flush();

            $oldRoute = $driverRouteOld->getOldDriverRoute();
            if (empty($oldRoute->getPunchIn())) {
                $oldRoute->setEndTime(new DateTime($data['endTimeOfPriorShift']));
            } else {
//                $oldRoute->setPunchOut(new DateTime($data['endTimeOfPriorShift']));
                $oldRoute->setEndTime(new DateTime($data['endTimeOfPriorShift']));
            }

            $this->getEntityManager()->persist($oldRoute);

            $this->getEntityManager()->flush();
            $driverRouteObj = $driverRouteOld;
            //$codeArr = [];
        } else {
            $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($data['shiftType']);
            $driverRouteObj = new DriverRoute();
            $driverRouteObj->setDateCreated($driverRouteOld->getDateCreated());
            $driverRouteObj->setSkill($shift->getSkill());
            $driverRouteObj->setDriver($driverRouteOld->getDriver());
            $driverRouteObj->setStation($driverRouteOld->getStation());
            $driverRouteObj->setStatus($driverRouteOld->getStatus());
            $driverRouteObj->setNumberOfPackage($data['numberOfPackage']);
            $driverRouteObj->setShiftNote($note);
            $driverRouteObj->setIsRescuer(1);
            $driverRouteObj->setStartTime(new DateTime($data['startTime']));
            $driverRouteObj->setEndTime(new DateTime($data['endTime']));
            $driverRouteObj->setIsRescued(0);
            $driverRouteObj->setShiftType($shift);
            $driverRouteObj->setIsTemp(0);
            $driverRouteObj->setIsLoadOut(0);
            $driverRouteObj->setIsOpenShift(false);

            $driverRouteObj->setShiftName($shift->getName());
            $driverRouteObj->setShiftColor($shift->getColor());
            $driverRouteObj->setUnpaidBreak($shift->getUnpaidBreak());
            $driverRouteObj->setShiftInvoiceType($shiftInvoiceType);
            $driverRouteObj->setShiftStation($shift->getStation());
            $driverRouteObj->setShiftSkill($shift->getSkill());
            $driverRouteObj->setShiftCategory($shift->getCategory());
            $driverRouteObj->setShiftBalanceGroup($shift->getBalanceGroup());
            $driverRouteObj->setShiftTextColor($shift->getTextColor());
            $driverRouteObj->setOldDriverRoute($driverRouteOld);

            $driverRouteObj->setCurrentNote($currentNote);
            $driverRouteObj->setNewNote($newNote);

            $this->getEntityManager()->persist($driverRouteObj);
            $this->getEntityManager()->flush();

            if (empty($driverRouteOld->getPunchIn())) {
                $driverRouteOld->setDatetimeEnded(new DateTime($data['endTimeOfPriorShift']));
            } else {
//                $driverRouteOld->setPunchOut(new DateTime($data['endTimeOfPriorShift']));
                $driverRouteOld->setDatetimeEnded(new DateTime($data['endTimeOfPriorShift']));
            }

            if ($driverRouteOld->getPunchOut()) {
                $driverRouteObj->setPunchOut($driverRouteOld->getPunchOut());
                $driverRouteOld->setPunchOut(null);
            }

            $driverRouteOld->setIsRescued(true);

            $this->getEntityManager()->persist($driverRouteOld);
            $this->getEntityManager()->flush();


            $criteria = [
                'code' => $data['routeCode'][0],
                'station' => $driverRouteOld->getStation()->getId(),
                'company' => $driverRouteOld->getStation()->getCompany()->getId(),
                'dateCreated' => $driverRouteOld->getDateCreated()
            ];
            $driverRouteCode = $this->getEntityManager()->getRepository('App\Entity\DriverRouteCode')->getTodayRouteCodeForInsert($criteria);

            if ($driverRouteCode) {
                $codeArr[] = $data['routeCode'][0];

                $driverRouteCode->addDriverRoute($driverRouteObj);
                $this->getEntityManager()->persist($driverRouteCode);
                $this->getEntityManager()->flush();

                $driverRoutes = $this->createQueryBuilder('dr')
                    ->leftJoin('dr.driverRouteCodes', 'drc')
                    ->where('dr.dateCreated = :dateCreated')
                    ->setParameter('dateCreated', $driverRouteOld->getDateCreated())
                    ->andWhere('drc.code = :code')
                    ->setParameter('code', $data['routeCode'][0])
                    ->andWhere('dr.isRescuer = :isRescuer')
                    ->setParameter('isRescuer', false)
                    ->andWhere('dr.isActive = :isActive')
                    ->setParameter('isActive', true)
                    ->andWhere('dr.isArchive = :isArchive')
                    ->setParameter('isArchive', false)
                    ->distinct()
                    ->getQuery()
                    ->getResult();

                if ($driverRoutes) {
                    foreach ($driverRoutes as $route) {
                        $route->setIsRescued(1);
                        $this->getEntityManager()->persist($route);
                        $this->getEntityManager()->flush();
                    }
                }
            }
            $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->addRescueShiftEventDataByObject($userLogin, $driverRouteObj, $driverRouteOld);
        }

        $emitter = $this->tokenStorage->getToken()->getUser()->getId();
        $newShift = $this->globalUtils->getShiftData($driverRouteObj, $week, $requestTimeZone, true);
        $updatedOldShift = $this->globalUtils->getShiftData($driverRouteOld, $week, $requestTimeZone, true);

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'loadout'
            ])
            ->setEvent('REFRESH')
            ->setEmitter($emitter)
            ->setData(true)
            ->dispatch();

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEvent('MOVED')
            ->setEmitter(-1) // set to -1 so the person that created the rescue also see the old shift change
            ->setData([
                'from' => [
                    'routeId' => $driverRouteOld->getId(),
                    'driverId' => $driverRouteOld->getDriver()->getId(),
                    'week' => $driverRouteOld->getDateCreated()->format('w'),
                ],
                'to' => [
                    'driverId' => $driverRouteOld->getDriver()->getId(),
                    'week' => $driverRouteOld->getDateCreated()->format('w'),
                ],
                'item' => $updatedOldShift
            ])
            ->dispatch();

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEvent('ADDED')
            ->setEmitter($emitter)
            ->setData([
                'to' => [
                    'driverId' => $driverRouteObj->getDriver()->getId(),
                    'week' => $driverRouteObj->getDateCreated()->format('w'),
                ],
                'item' => $newShift
            ])
            ->dispatch();

        return $newShift;
    }

    public function addRescueShiftEventDataByObject($userLogin, $driverRouteObj, $driverRouteOld)
    {
        $messageData = array('shift_name' => $driverRouteOld->getShiftName(), 'user_name' => $userLogin->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift Add', $driverRouteObj, 'RESCUE_SHIFT_ADD', $messageData, true);
    }

    public function editRescueShiftEventDataByObject($shiftDiffrances, $shift, $userLogin, $driverRoute, $isActive = true)
    {
        foreach ($shiftDiffrances as $keyName => $shiftDiffrance) {
            if ('startTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_start_time' => (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_start_time' => (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift StartTime Edit', $driverRoute, 'RESCUE_SHIFT_START_TIME_EDIT', $messageData, $isActive);
            }
            if ('endTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_end_time' => (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_end_time' => (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift EndTime Edit', $driverRoute, 'RESCUE_SHIFT_END_TIME_EDIT', $messageData, $isActive);
            }
            if ('shiftInvoiceType' == $keyName) {
                $messageData = array(
                    'old_invoice_type' => (is_object($shiftDiffrance[0]) ? $shiftDiffrance[0]->getName() : $shiftDiffrance[0]['name']),
                    'new_invoice_type' => (is_object($shiftDiffrance[1]) ? $shiftDiffrance[1]->getName() : $shiftDiffrance[1]['name']),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift Invoice Edit', $driverRoute, 'RESCUE_SHIFT_INVOICE_TYPE_EDIT', $messageData, $isActive);
            }
            if ('currentNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift Note Edit', $driverRoute, 'RESCUE_SHIFT_NOTE_EDIT', $messageData, $isActive);
            }
            if ('newNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift Driver Note Edit', $driverRoute, 'RESCUE_SHIFT_DRIVER_NOTE_EDIT', $messageData, $isActive);
            }
            if ('numberOfPackage' == $keyName) {
                $messageData = array(
                    'old_package' => $shiftDiffrance[0],
                    'new_package' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Rescue Shift Package Edit', $driverRoute, 'RESCUE_SHIFT_PACKAGE_EDIT', $messageData, $isActive);
            }
        }
    }

    public function addSecondShift($criteria)
    {
        $data = $criteria['data'];

        $date = new DateTime($criteria['shiftDate']);
        $week = $date->format("W");

        if ($data['shiftInvoiceType'] == 0) {
            $shiftInvoiceType = null;
        } else {
            $shiftInvoiceType = $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($data['shiftInvoiceType']);
        }

        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $codeArr = [];

        /** @var DriverRoute $driverRouteOld */
        $driverRouteOld = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['id']);

        $note = $newNote = $currentNote = '';

        if (isset($data['note'])) {
            $note = $data['note'] . "\n\r";
            $currentNote = $data['note'];
        }

        if (isset($data['newNote'])) {
            $note = $note . $data['newNote'];
            $newNote = $data['newNote'];
        }
        $user = $this->tokenStorage->getToken()->getUser();
        $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($data['shiftType']);
        if ($criteria['isEdit'] == true) {
            $driverRouteOld->setShiftType($shift);
            $driverRouteOld->setStartTime(new DateTime($data['startTime']));
            $driverRouteOld->setEndTime(new DateTime($data['endTime']));
            $driverRouteOld->setShiftNote($note);

            $driverRouteOld->setCurrentNote($currentNote);
            $driverRouteOld->setNewNote($newNote);
            $driverRouteOld->setShiftName($shift->getName());
            $driverRouteOld->setShiftColor($shift->getColor());
            $driverRouteOld->setUnpaidBreak($shift->getUnpaidBreak());
            $driverRouteOld->setShiftInvoiceType($shiftInvoiceType);
            $driverRouteOld->setShiftStation($shift->getStation());
            $driverRouteOld->setShiftSkill($shift->getSkill());
            $driverRouteOld->setShiftCategory($shift->getCategory());
            $driverRouteOld->setShiftBalanceGroup($shift->getBalanceGroup());
            $driverRouteOld->setShiftTextColor($shift->getTextColor());

            $this->getEntityManager()->persist($driverRouteOld);
            $uow = $this->getEntityManager()->getUnitOfWork();
            $uow->computeChangeSets();
            $shiftDiffrances = $uow->getEntityChangeSet($driverRouteOld);

            if (!empty($shiftDiffrances)) {
                $this->editSecondShiftEventDataByObject($shiftDiffrances, $driverRouteOld->getShiftType(), $user, $driverRouteOld);
            }

            $this->getEntityManager()->flush();

            $oldRoute = $driverRouteOld->getOldDriverRoute();
            $oldRoute->setEndTime(new DateTime($data['endTimeOfPriorShift']));

            $this->getEntityManager()->persist($oldRoute);
            $this->getEntityManager()->flush();
            $driverRouteObj = $driverRouteOld;
            // $messageData = array(
            //     // 'old_vehicle' => $driverRoutes->getVehicle()->getVehicleId(),
            //     // 'new_vehicle' => $vehicleId->getVehicleId(),
            //     // 'shift_name'=>$driverRoutes->getShiftName(),
            //     'user_name'=>$user->getFriendlyName());
            // $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Edit', $driverRouteOld, 'SECOND_SHIFT_EDIT', $messageData, true);

        } else {
            $driverRouteObj = new DriverRoute();
            $driverRouteObj->setDateCreated($driverRouteOld->getDateCreated());
            $driverRouteObj->setSkill($shift->getSkill());
            $driverRouteObj->setDriver($driverRouteOld->getDriver());
            $driverRouteObj->setStation($driverRouteOld->getStation());
            $driverRouteObj->setStatus($driverRouteOld->getStatus());
            $driverRouteObj->setShiftNote($note);
            $driverRouteObj->setIsRescuer(false);
            $driverRouteObj->setStartTime(new DateTime($data['startTime']));
            $driverRouteObj->setEndTime(new DateTime($data['endTime']));
            $driverRouteObj->setIsRescued(false);
            $driverRouteObj->setShiftType($shift);
            $driverRouteObj->setIsTemp(0);
            $driverRouteObj->setIsLoadOut(0);
            $driverRouteObj->setIsOpenShift(false);

            if ($driverRouteOld->getPunchOut()) {
                $driverRouteObj->setPunchOut($driverRouteOld->getPunchOut());
                $driverRouteOld->setPunchOut(null);
            }

            $driverRouteObj->setShiftName($shift->getName());
            $driverRouteObj->setShiftColor($shift->getColor());
            $driverRouteObj->setUnpaidBreak($shift->getUnpaidBreak());
            $driverRouteObj->setShiftInvoiceType($shiftInvoiceType);
            $driverRouteObj->setShiftStation($shift->getStation());
            $driverRouteObj->setShiftSkill($shift->getSkill());
            $driverRouteObj->setShiftCategory($shift->getCategory());
            $driverRouteObj->setShiftBalanceGroup($shift->getBalanceGroup());
            $driverRouteObj->setShiftTextColor($shift->getTextColor());
            $driverRouteObj->setOldDriverRoute($driverRouteOld);
            $driverRouteObj->setOldDriver($driverRouteOld->getDriver());

            $driverRouteObj->setCurrentNote($currentNote);
            $driverRouteObj->setNewNote($newNote);
            $driverRouteObj->setIsSecondShift(true);

            $this->getEntityManager()->persist($driverRouteObj);
            $this->getEntityManager()->flush();

            $driverRouteOld->setDatetimeEnded(new DateTime($data['endTimeOfPriorShift']));
            $driverRouteOld->setOldDriverRoute($driverRouteObj);

            $this->getEntityManager()->persist($driverRouteOld);
            $this->getEntityManager()->flush();

            $messageData = array(
                // 'old_vehicle' => $driverRoutes->getVehicle()->getVehicleId(),
                // 'new_vehicle' => $vehicleId->getVehicleId(),
                'shift_name' => $driverRouteObj->getShiftName(),
                'user_name' => $user->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Add', $driverRouteObj, 'SECOND_SHIFT_ADD', $messageData, true);
        }

        $emitter = $user->getId();
        $newShift = $this->globalUtils->getShiftData($driverRouteObj, $week, $requestTimeZone, true);
        $updatedOldShift = $this->globalUtils->getShiftData($driverRouteOld, $week, $requestTimeZone, true);

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'loadout'
            ])
            ->setEvent('REFRESH')
            ->setEmitter($emitter)
            ->setData(true)
            ->dispatch();

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEvent('MOVED')
            ->setEmitter(-1) // set to -1 so the person that created the rescue also see the old shift change
            ->setData([
                'from' => [
                    'routeId' => $driverRouteOld->getId(),
                    'driverId' => $driverRouteOld->getDriver()->getId(),
                    'week' => $driverRouteOld->getDateCreated()->format('w'),
                ],
                'to' => [
                    'driverId' => $driverRouteOld->getDriver()->getId(),
                    'week' => $driverRouteOld->getDateCreated()->format('w'),
                ],
                'item' => $updatedOldShift
            ])
            ->dispatch();

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $driverRouteObj->getStation()->getCompany()->getId(),
                'station' => $newShift['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEvent('ADDED')
            ->setEmitter($emitter)
            ->setData([
                'to' => [
                    'driverId' => $driverRouteObj->getDriver()->getId(),
                    'week' => $driverRouteObj->getDateCreated()->format('w'),
                ],
                'item' => $newShift
            ])
            ->dispatch();

        return $newShift;
    }

    public function editSecondShiftEventDataByObject($shiftDiffrances, $shift, $userLogin, $driverRoute, $isActive = true)
    {
        foreach ($shiftDiffrances as $keyName => $shiftDiffrance) {
            if ('startTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_start_time' => (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_start_time' => (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift StartTime Edit', $driverRoute, 'SECOND_SHIFT_START_TIME_EDIT', $messageData, $isActive);
            }
            if ('endTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_end_time' => (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_end_time' => (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift EndTime Edit', $driverRoute, 'SECOND_SHIFT_END_TIME_EDIT', $messageData, $isActive);
            }
            if ('shiftInvoiceType' == $keyName) {
                $messageData = array(
                    'old_invoice_type' => (is_object($shiftDiffrance[0]) ? $shiftDiffrance[0]->getName() : $shiftDiffrance[0]['name']),
                    'new_invoice_type' => (is_object($shiftDiffrance[1]) ? $shiftDiffrance[1]->getName() : $shiftDiffrance[1]['name']),
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Invoice Edit', $driverRoute, 'SECOND_SHIFT_INVOICE_TYPE_EDIT', $messageData, $isActive);
            }
            if ('currentNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Note Edit', $driverRoute, 'SECOND_SHIFT_NOTE_EDIT', $messageData, $isActive);
            }
            if ('newNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Driver Note Edit', $driverRoute, 'SECOND_SHIFT_DRIVER_NOTE_EDIT', $messageData, $isActive);
            }
            if ('numberOfPackage' == $keyName) {
                $messageData = array(
                    'old_package' => $shiftDiffrance[0],
                    'new_package' => $shiftDiffrance[1],
                    'shift_name' => $shift->getName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Second Shift Package Edit', $driverRoute, 'SECOND_SHIFT_PACKAGE_EDIT', $messageData, $isActive);
            }
        }
    }


    public function getToDaysDriverCode($criteria)
    {
        $currentCompany = $this->tokenStorage->getToken()->getUser()->getCompany()->getId();

        $driverRoutes = $this->createQueryBuilder('dr')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('st.company', 'c')
            ->where('dr.dateCreated = :dateCreated')
            ->setParameter('dateCreated', new DateTime($criteria['date']))
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere("dr.id != :currentRoute")
            ->setParameter("currentRoute", $criteria['routeId'])
            ->andWhere("c.id = :company")
            ->setParameter("company", $currentCompany)
            ->distinct()
            ->getQuery()
            ->getResult();

        $codeArr = [];
        if ($driverRoutes) {
            foreach ($driverRoutes as $route) {

                foreach ($route->getDriverRouteCodes()->filter(function ($routeCode) {
                    return !$routeCode->getIsArchive();
                }) as $code) {
                    $codeArr[] = $code->getCode();
                }
            }
        }

        $codeArr = array_unique($codeArr);
        return array_values($codeArr);
    }

    public function updateBalanceGroupNullInDriverRoute($balanceGroupId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.shiftBalanceGroup', 'NULL');
        if (is_array($balanceGroupId)) {
            $qb->where($qb->expr()->in('dr.shiftBalanceGroup', ':balanceGroupId'))
                ->setParameter('balanceGroupId', $balanceGroupId);
        } else {
            $qb->where('dr.shiftBalanceGroup = :balanceGroup')
                ->setParameter('balanceGroup', $balanceGroupId);
        }
        $qb->getQuery()
            ->execute();
        return true;
    }

    public function getWorkHoursEstimatedWorkHours($criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentPassedDate = $criteria['date'];
        $date = new \DateTime();
        $currDate = new \DateTime($currentPassedDate);
        $currDate->setTime($date->format('h'), $date->format('i'), $date->format('s'));
        $currDate = $currDate->setTimezone(new \DateTimeZone('UTC'));
        $currDate = $this->globalUtils->getTimeZoneConversation($currDate, $requestTimeZone);
        $currDate->setTime(0, 0, 0);

        if ($currDate->format('l') == 'Sunday') {
            $week = $currDate->format('W') + 1;
            $timeStamp = strtotime($currDate->format('Y-m-d') . ' +1 day');
        } else {
            $week = $currDate->format('W');
            $timeStamp = strtotime($currDate->format('Y-m-d'));
        }
        $year = date("Y", strtotime('this week', $timeStamp));
        $weekStartEndDay = $this->getStartAndEndDate($week, $year);

        $tempOpenShiftRoutesData = $tempOpenShiftRoutesIdArr = $tempRoutesData = $tempRoutesIdArr = [];
        $completedShiftHours = $futureShiftData = $shiftWorkingHours = 0;

        $userLogin = $this->tokenStorage->getToken()->getUser();

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getScheduleOpenShiftTempDriverRoutesWithParam($weekStartEndDay, $userLogin, $criteria['id']);

        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            if (!empty($tempOpenDriverRoute->getRouteId())) {
                $tempOpenShiftRoutesData[$tempOpenDriverRoute->getRouteId()->getId()] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute->getRouteId()->getId();
            }
        }

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteDataForDayView($currentPassedDate, $userLogin, $criteria['id']);

        foreach ($tempDriverRoutes as $tempDriverRoute) {
            if (!empty($tempDriverRoute->getRouteId())) {
                $tempRoutesData[$tempDriverRoute->getRouteId()->getId()] = $tempDriverRoute;
                $tempRoutesIdArr[] = $tempDriverRoute->getRouteId()->getId();
            }
        }

        $results = $this->getScheduleDriverRoutesWithParam($weekStartEndDay, $userLogin, $tempOpenShiftRoutesIdArr, $criteria['id']);
        // $m = [];
        foreach ($results as $result) {
            $result = isset($tempRoutesData[$result->getId()]) ? $tempRoutesData[$result->getId()] : $result;

            if (method_exists($result, 'getAction') && $result->getAction() == 'SHIFT_DELETED') {
                $hasDeleted = true;
                continue;
            }

            $datetime = $result->getDateCreated()->format('Y-m-d H:i:s');
            $dayNumber = date('w', strtotime($datetime));

            $date1 = $result->getPunchIn() ? $result->getPunchIn() : $result->getStartTime();
            $date2 = $result->getPunchOut() ? $result->getPunchOut() : $result->getEndTime();

            $diff = $date2->diff($date1);
            $totalMinutes = (($diff->h * 60) + $diff->i) - $result->getUnpaidBreak();
            // $m[$datetime]['h'] = $diff->h;
            // $m[$datetime]['m'] = $diff->i;
            // $m[$datetime]['up'] = $result->getUnpaidBreak();
            if ($result->getDateCreated() < $currDate) {

                if (isset($isPunchedIn[$dayNumber])) {
                    if ($isPunchedIn[$dayNumber] != true) {
                        $isPunchedIn[$dayNumber] = ($result->getPunchIn() == null || $result->getPunchIn() == "") ? false : true;
                    }
                } else {
                    $isPunchedIn[$dayNumber] = ($result->getPunchIn() == null || $result->getPunchIn() == "") ? false : true;
                }
                // $m[$datetime]['PunchedIn'] = $isPunchedIn[$dayNumber];

                for ($day = 0; $day <= 6; $day++) {
                    if ($dayNumber == $day) {
                        $shiftHoursByDate[$dayNumber] = isset($shiftHoursByDate[$dayNumber]) ? $shiftHoursByDate[$dayNumber] + $totalMinutes : $totalMinutes;
                    } else {
                        $shiftHoursByDate[$day] = isset($shiftHoursByDate[$day]) ? $shiftHoursByDate[$day] : 0;
                    }
                }

                $attendedShiftHoursOfDriver = 0;
                for ($day = 0; $day <= 6; $day++) {
                    if (isset($isPunchedIn[$day]) && $isPunchedIn[$day]) {
                        $attendedShiftHoursOfDriver = $attendedShiftHoursOfDriver + $shiftHoursByDate[$day];
                    }
                }

                $completedShiftHours = $attendedShiftHoursOfDriver;

            } else {
                $futureShiftData = $futureShiftData + $totalMinutes;
            }

            $shiftWorkingHours = $completedShiftHours + $futureShiftData;
        }
        // dd($results[0]->getDriver()->getFullName(),$m);

        $hoursOfCompletedShiftHours = floor($completedShiftHours / 60);
        $minutesOfCompletedShiftHours = ($completedShiftHours % 60);
        $completedShiftHour = (float)$hoursOfCompletedShiftHours . (($minutesOfCompletedShiftHours > 0) ? '.' . round(($minutesOfCompletedShiftHours / 60) * 100) : '');
        $completedShiftHour = sprintf('%g', $completedShiftHour);
        $hoursShiftWorkingHours = floor($shiftWorkingHours / 60);
        $minutesShiftWorkingHours = ($shiftWorkingHours % 60);
        $shiftWorkingHour = (float)$hoursShiftWorkingHours . (($minutesShiftWorkingHours > 0) ? '.' . round(($minutesShiftWorkingHours / 60) * 100) : '');
        $shiftWorkingHour = sprintf('%g', $shiftWorkingHour);

        return [$completedShiftHour, $shiftWorkingHour];
    }

    public function addDriverRouteForCortex(Shift $shift, \DateTimeInterface $date, ?Driver $driver, ?DriverRouteCode $routeCode)
    {
        $driverRouteNew = new DriverRoute();
        $driverRouteNew->setDateCreated($date);

        if (!empty($driver))
            $driverRouteNew->setDriver($driver);

        $driverRouteNew->setStation($shift->getStation());
        $driverRouteNew->setSkill($shift->getSkill());
        $driverRouteNew->setIsActive(true);
        $driverRouteNew->setIsOpenShift(empty($driver));
        $driverRouteNew->setIsTemp(false);
        $driverRouteNew->setShiftName($shift->getName());
        $driverRouteNew->setShiftColor($shift->getColor());
        $driverRouteNew->setUnpaidBreak($shift->getUnpaidBreak());
        $driverRouteNew->setShiftNote($shift->getNote());
        $driverRouteNew->setShiftStation($shift->getStation());
        $driverRouteNew->setShiftSkill($shift->getSkill());
        $driverRouteNew->setShiftCategory($shift->getCategory());
        $driverRouteNew->setShiftBalanceGroup($shift->getBalanceGroup());
        $driverRouteNew->setShiftTextColor($shift->getTextColor());
        $driverRouteNew->setStartTime($shift->getStartTime());
        $driverRouteNew->setEndTime($shift->getEndTime());
        $driverRouteNew->setShiftInvoiceType($shift->getInvoiceType());
        $driverRouteNew->setIsRescuer(false);
        $driverRouteNew->setIsRescued(false);
        // $driverRouteNew->setIsNew(false);
        // $driverRouteNew->setIsLoadOut(false);
        // $driverRouteNew->setIsBackup(false);

        if (!empty($routeCode)) {
            $driverRouteNew->addDriverRouteCode($routeCode);
            $driverRouteNew->setRouteCode($routeCode->getCode());
        }

        $driverRouteNew->setShiftType($shift);
        // $this->getEntityManager()->persist($driverRouteNew);
        // $this->getEntityManager()->flush($driverRouteNew);

        return $driverRouteNew;
    }

    public function datesChangeRemoveDriverRoute($scheduleId, $startDateOfSchedule, $endDateOfSchedule, $updatedDriverRoutes)
    {
        $today = new \DateTime('now');
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $r = $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('dr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere(
                '
                (dr.dateCreated < :startDate and dr.dateCreated > :today)
                OR
                (dr.dateCreated > :endDate and dr.dateCreated > :today)
                '
            )
            ->setParameter('startDate', $startDateOfSchedule)
            ->setParameter('endDate', $endDateOfSchedule)
            ->setParameter('today', $today)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', 0);

        if (!empty($updatedDriverRoutes)) {
            $qb->andWhere($qb->expr()->notIn('dr.id', ':driverRouteArray'))
                ->setParameter('driverRouteArray', $updatedDriverRoutes);
        }

        $qb->getQuery()->execute();

        return true;
    }

    public function updateDriverStationInFutureRoutes($criteria)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $station = $this->getEntityManager()->getRepository('App\Entity\Station')->find($criteria['station_id']);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.station', ':shiftStation')
            ->set('dr.shiftStation', ':shiftStation')
            ->setParameter('shiftStation', $station)
            ->where('dr.station != :station_id')
            ->setParameter('station_id', $criteria['station_id'])
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driver_id'])
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();

        return true;
    }

    public function getDriverShiftByDateForDayViewForExport($criteria)
    {

        $routeArray = $openShiftArray = $tempOpenShiftRoutesData = $tempOpenShiftRoutesIdArr = $tempRoutesData = $tempRoutesIdArr = $backupDriverArray = $mainDriverArray = $shiftArrayForBackupDriver = $shiftArrayForMainDriver = $shiftArrayByDriverForBackup = $shiftArrayByDriverForMain = $pastShiftData = $futureShiftData = $startTimeArray = $endTimeArray = $hoursArray = $filterArrayScheduler = $backupDrivers = $mainDrivers = $completedShiftHours = $shiftWorkingHours = [];

        $userLogin = $this->tokenStorage->getToken()->getUser();
        $hasDeleted = false;
        $tempRecord = 0;
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $currentPassedDate = $criteria['date'];
        $newDate = new \DateTime();
        $selectedDateForDayView = $selectedDate = new \DateTime($currentPassedDate);
        $selectedDateForDayView->setTime($newDate->format('h'), $newDate->format('i'), $newDate->format('s'));
        $selectedDateForDayView = $selectedDateForDayView->setTimezone(new \DateTimeZone('UTC'));
        // $selectedDateForDayView = $this->globalUtils->getTimeZoneConversation($selectedDateForDayView, $requestTimeZone);
        $selectedDateForDayView->setTime(0, 0, 0);

        // Fix to force Sunday to be on the right week
        // if ($selectedDateForDayView->format('l') == 'Sunday') {
        //     $week = $selectedDateForDayView->format('W') + 1;
        // } else {
        //     $week = $selectedDateForDayView->format('W');
        // }
        $week = $criteria['week'];
        $weekStartEndDay = $this->getStartAndEndDate($week, (int)$criteria['year']);

        $currDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currDate = $this->globalUtils->getTimeZoneConversation($currDate, $requestTimeZone);
        $currDate->setTime(0, 0, 0);

        $drivers = $this->getEntityManager()->getRepository('App\Entity\Driver')->getAllDriversNew($userLogin->getCompany()->getId());

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getScheduleOpenShiftTempDriverRoutesWithParamNew($weekStartEndDay, $userLogin);

        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            if (empty($tempOpenDriverRoute['routeId'])) {
                $tempOpenShiftRoutesData['empty'][] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = null;
            } else {
                $tempOpenShiftRoutesData[$tempOpenDriverRoute['routeId']] = $tempOpenDriverRoute;
                $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute['routeId'];
            }
        }

        $tempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteDataForDayViewNew($currentPassedDate, $userLogin);

        foreach ($tempDriverRoutes as $tempDriverRoute) {
            if (empty($tempDriverRoute['routeId'])) {
                $tempRoutesData['empty'][] = $tempDriverRoute;
                $tempRoutesIdArr[] = null;
            } else {
                $tempRoutesData[$tempDriverRoute['routeId']] = $tempDriverRoute;
                $tempRoutesIdArr[] = $tempDriverRoute['routeId'];
            }
        }

        $tempRecord = count($tempDriverRoutes);

        $openShiftDriverRoutes = $this->getScheduleOpenShiftDriverRoutesWithParamForDayViewNew($currentPassedDate, $userLogin, $tempRoutesIdArr);

        $results = $this->getScheduleDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $tempOpenShiftRoutesIdArr);

        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $formatedDate = $selectedDate->format('l, F dS');
        $sheet->setCellValue('A1', 'Day View Export for ' . $formatedDate);
        $sheet->setCellValue('A4', 'Driver');
        $sheet->setCellValue('B4', 'Shift Type');
        $sheet->setCellValue('C4', 'Invoice Type');
        $sheet->setCellValue('D4', 'Incident');
        $sheet->setCellValue('E4', 'Notes:');
        $sheet->setCellValue('F4', 'First Geofence Entry');
        $sheet->setCellValue('G4', 'Last Geofence Entry');
        $sheet->setCellValue('H4', 'Punch In');
        $sheet->setCellValue('I4', 'Punch Out');
        $sheet->setCellValue('J4', 'Lunch');
        $sheet->setCellValue('K4', 'Hours Worked');
        $sheet->setCellValue('L4', 'Routes');
        $sheet->setCellValue('M4', 'Routes Rescued');
        $sheet->setCellValue('N4', 'Rescue Hours');
        $sheet->setCellValue('O4', 'Rescue Notes');
        $sheet->setCellValue('P4', 'Vehicle');
        $sheet->setCellValue('Q4', 'Devices');
        $sheet->setCellValue('R4', 'Stops');
        $sheet->setCellValue('S4', 'Packages');
        $sheet->setCellValue('T4', 'Missing (Cortex)');
        $sheet->setCellValue('U4', ' Returning (Cortex)');
        $sheet->setCellValue('V4', 'Missing (RTS)');
        $sheet->setCellValue('W4', 'Returning (RTS)');
        $sheet->setCellValue('X4', 'Shift Notes');


        $sheet->mergeCells('A1:I3');
        $sheet->getStyle('A1')->applyFromArray(['font' => ['size' => 18]]);
        $sheet->getStyle('A4:X4')->applyFromArray(['font' => ['bold' => true, 'size' => 10]]);
        $counter = 5;

        foreach ($results as $result) {
            $result = isset($tempRoutesData[$result['id']]) ? $tempRoutesData[$result['id']] : $result;

            if (isset($result['action']) && $result['action'] == 'SHIFT_DELETED') {
                $hasDeleted = true;
                continue;
            }

            if ($result['isRescuer'] == 1) {
                continue;
            }

            if (empty($result['driver_id'])) {
                continue;
            }

            $datetime = (new \DateTime($result['dateCreated']))->format('Y-m-d H:i:s');
            $datetimeMainShift = (new \DateTime($result['dateCreated']))->format('Y-m-d');
            $dayNumber = date('w', strtotime($datetime));

            // Create two new calculate working hour
            $driverRouteStartTimeObj = !empty($result['startTime']) ? new \DateTime($result['startTime']) : new \DateTime($result['shift_startTime']);
            $driverRouteEndTimeObj = !empty($result['endTime']) ? new \DateTime($result['endTime']) : new \DateTime($result['shift_endTime']);

            $date1 = !empty($result['punchIn']) ? new \DateTime($result['punchIn']) : $driverRouteStartTimeObj;
            $date2 = !empty($result['punchOut']) ? new \DateTime($result['punchOut']) : $driverRouteEndTimeObj;

            $diff = $date2->diff($date1);
            $totalMinutes = (($diff->h * 60) + $diff->i) - (!empty($result['unpaidBreak']) ? $result['unpaidBreak'] : $result['shift_unpaidBreak']);

            if ((new \DateTime($result['dateCreated']))->format('Y-m-d') < $currDate->format('Y-m-d')) {

                if (isset($isPunchedIn[$result['driver_id']][$dayNumber])) {
                    if ($isPunchedIn[$result['driver_id']][$dayNumber] != true) {
                        $isPunchedIn[$result['driver_id']][$dayNumber] = empty($result['punchIn']) ? false : true;
                    }
                } else {
                    $isPunchedIn[$result['driver_id']][$dayNumber] = empty($result['punchIn']) ? false : true;
                }

                for ($day = 0; $day <= 6; $day++) {
                    if ($dayNumber == $day) {
                        $shiftHoursByDate[$result['driver_id']][$dayNumber] = isset($shiftHoursByDate[$result['driver_id']][$dayNumber]) ? $shiftHoursByDate[$result['driver_id']][$dayNumber] + $totalMinutes : $totalMinutes;
                    } else {
                        $shiftHoursByDate[$result['driver_id']][$day] = isset($shiftHoursByDate[$result['driver_id']][$day]) ? $shiftHoursByDate[$result['driver_id']][$day] : 0;
                    }
                }

                $attendedShiftHoursOfDriver[$result['driver_id']] = 0;
                for ($day = 0; $day <= 6; $day++) {
                    if (isset($isPunchedIn[$result['driver_id']][$day]) && $isPunchedIn[$result['driver_id']][$day]) {
                        $attendedShiftHoursOfDriver[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']] + $shiftHoursByDate[$result['driver_id']][$day];
                    }
                }

                $pastShiftData[$result['driver_id']] = $attendedShiftHoursOfDriver[$result['driver_id']];

            } else {
                $futureShiftData[$result['driver_id']] = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] + $totalMinutes : $totalMinutes;
            }

            $pastDateShiftHours = isset($pastShiftData[$result['driver_id']]) ? $pastShiftData[$result['driver_id']] : 0;

            $futureDateShiftHours = isset($futureShiftData[$result['driver_id']]) ? $futureShiftData[$result['driver_id']] : 0;

            $completedShiftHours[$result['driver_id']]['hour'] = $pastDateShiftHours;

            $shiftWorkingHours[$result['driver_id']]['hour'] = $pastDateShiftHours + $futureDateShiftHours;

            if ((new \DateTime($result['dateCreated']))->format('Y-m-d') == $selectedDateForDayView->format('Y-m-d')) {
                $routeCodesStringArr = [];
                $routeCodesString = '';
                $routeCodes = $this->getEntityManager()->getRepository(DriverRouteCode::class)->getRouteCodeByRouteId($result['routeId']);
                if (!empty($routeCodes)) {
                    $routeCodesStringArr = array_column($routeCodes,'code');
                }
                array_unique($routeCodesStringArr);
                $routeCodesString = implode(" , ", $routeCodesStringArr);


                $mainDrivers[$result['driver_id']] = $result['driver_id'];
                $shiftDataArray = $this->globalUtils->getShiftDataNew($result, $week, $requestTimeZone, true);

                $shiftStartTime = !empty($result['punchIn']) ? new \DateTime($result['punchIn']) : $driverRouteStartTimeObj;
                $shiftStartTime = $shiftStartTime->format('G');

                /* $shiftArrayForMainDriver[$result->getDriver()->getId()]['shift_' . $shiftStartTime] = $shiftDataArray; */
                $incident = $this->getEntityManager()->getRepository('App\Entity\Incident')->getIncidentByDriverDate($result['driver_id'], $selectedDate);

                if ($criteria['station'] === 0 || (int)$result['station_id'] === $criteria['station']) {
                    if (!$incident) {
                        $sentHome = !empty($result['datetimeEnded']) ? " (Sent Home)" :  '';

                        $sheet->setCellValue('A' . $counter, $result['user_firstName'] . ' ' . $result['user_lastName']);
                        $sheet->setCellValue('B' . $counter, !empty($result['shiftName']) ? $result['shiftName'] : $result['shift_name'] . $sentHome);
                        $sheet->setCellValue('C' . $counter, !empty($result['shiftInvoiceType_name']) ? $result['shiftInvoiceType_name'] : 'N/A');
                        $sheet->setCellValue('D' . $counter, !empty($incident) ? $incident[0]['name'] : '');
                        $sheet->setCellValue('E' . $counter, !empty($incident) ? $incident[0]['title'] : '');
                        $geoforceFirst = $geoforceLast = $punchIn = $punchOut = '';
                        foreach ($shiftDataArray['timeline'] as $timeline) {
                            if ($timeline['type'] == 'GeoferenceIn') {
                                $geoforceFirst = $timeline['eventTime'] . '-' . $timeline['description'];
                            }
                            if ($timeline['type'] == 'GeoferenceOut') {
                                $geoforceLast = $timeline['eventTime'] . '-' . $timeline['description'];
                            }
                            if ($timeline['type'] == 'PunchIn') {
                                $punchIn = $timeline['eventTime'];
                            }
                            if ($timeline['type'] == 'PunchOut') {
                                $punchOut = $timeline['eventTime'];
                            }
                        }

                        $deviceString = '';
                        $devices = $this->getEntityManager()->getRepository('App\Entity\Device')->getDevicesByRouteId($result['id']);
                        if (!empty($devices)) {
                            foreach ($devices as $device) {
                                $deviceString .= $device['name'] . ',';
                            }
                        } else {
                            $deviceString = 'N';
                        }
                        $totalMin = 0;
                        if (!empty($result['breakPunchIn']) && !empty($result['breakPunchOut'])) {
                            $date1 = !empty($result['breakPunchIn']) ? new \DateTime($result['breakPunchIn']) : $driverRouteStartTimeObj;
                            $date2 = !empty($result['breakPunchOut']) ? new \DateTime($result['breakPunchOut']) : $driverRouteEndTimeObj;

                            $diff = $date2->diff($date1);
                            $totalMin = (($diff->h * 60) + $diff->i);
                        }

                        $totalRescueMin = 0;
                        $rescueNote = "";

                        $oldDriverRoutes = $this->getEntityManager()->getRepository(DriverRoute::class)->getOldDriverRouteByRouteId($result['id']);
                        foreach ($oldDriverRoutes as $rescuerOrBackup) {
                            if ($rescuerOrBackup['isRescuer'] == 1) {
                                $date1 = $driverRouteStartTimeObj;
                                $date2 = $driverRouteEndTimeObj;

                                $diff = $date2->diff($date1);
                                $totalRescueMin = (($diff->h * 60) + $diff->i);

                                $rescueNote = $rescuerOrBackup['shiftNote'];
                            }
                        }

                        $rescueShift = $this->getIsRescuerShift($result['driver_id'], new \DateTime($result['dateCreated']));

                        $totalWorkMin = 0;
                        if (!empty($result['punchIn']) && !empty($result['punchOut'])) {
                            $date1 = new \DateTime($result['punchIn']);
                            $date2 = new \DateTime($result['punchOut']);

                            $diff = $date2->diff($date1);
                            $totalWorkMin = (($diff->h * 60) + $diff->i) - $totalMin;
                        }

                        $returnToStation = $this->getEntityManager()->getRepository(ReturnToStation::class)->getReturnToStationByRouteId($result['id']);
                        if (!empty($returnToStation)) {
                            if (!empty($returnToStation['packagesMissing'])) {
                                $shiftDataArray['MissingRTS'] = $returnToStation['packagesMissing'];
                            } else {
                                $shiftDataArray['MissingRTS'] = '';
                            }
                            if (!empty($returnToStation['packagesReturning'])) {
                                $shiftDataArray['ReturningRTS'] = $returnToStation['packagesReturning'];
                            } else {
                                $shiftDataArray['ReturningRTS'] = '';
                            }
                        } else {
                            $shiftDataArray['MissingRTS'] = '';
                            $shiftDataArray['ReturningRTS'] = '';
                        }

                        $sheet->setCellValue('F' . $counter, $geoforceFirst);
                        $sheet->setCellValue('G' . $counter, $geoforceLast);
                        $sheet->setCellValue('H' . $counter, !empty($punchIn) ? date_format(date_create($punchIn), 'h:i:s A') : 'N');
                        $sheet->setCellValue('I' . $counter, !empty($punchOut) ? date_format(date_create($punchOut), 'h:i:s A') : 'N');
                        $sheet->setCellValue('J' . $counter, !empty($totalMin) ? $this->convertToHoursMins($totalMin) : 'N');

                        $sheet->setCellValue('K' . $counter, $this->convertToHoursMins($totalWorkMin, true));

                        $sheet->setCellValue('L' . $counter, rtrim($routeCodesString, ','));
                        $sheet->setCellValue('M' . $counter, $rescueShift['codes']);
                        $sheet->setCellValue('N' . $counter, $rescueShift['times']);
                        $sheet->setCellValue('O' . $counter, $rescueShift['notes']);
                        $sheet->setCellValue('P' . $counter, !empty($result['vehicle_id']) ? $result['vehicle_id'] : '');
                        $sheet->setCellValue('Q' . $counter, !empty($deviceString) ? rtrim($deviceString, ',') : '');
                        $sheet->setCellValue('R' . $counter, !empty($shiftDataArray['completedCountOfStops']) ? $shiftDataArray['completedCountOfStops'] : '');
                        $sheet->setCellValue('S' . $counter, !empty($shiftDataArray['countOfTotalPackages']) ? $shiftDataArray['countOfTotalPackages'] : '');
                        $sheet->setCellValue('T' . $counter, !empty($shiftDataArray['countOfMissingPackages']) ? $shiftDataArray['countOfMissingPackages'] : '');
                        $sheet->setCellValue('U' . $counter, !empty($shiftDataArray['countOfReturningPackages']) ? $shiftDataArray['countOfReturningPackages'] : '');
                        $sheet->setCellValue('V' . $counter, $shiftDataArray['MissingRTS']);
                        $sheet->setCellValue('W' . $counter, $shiftDataArray['ReturningRTS']);
                        $sheet->setCellValue('X' . $counter, $result['newNote']);

                    } else {
                        $sheet->setCellValue('A' . $counter, $result['user_firstName'] . ' ' . $result['user_lastName']);
                        $sheet->setCellValue('B' . $counter, '');
                        $sheet->setCellValue('C' . $counter, '');
                        $sheet->setCellValue('D' . $counter, !empty($incident) ? $incident[0]['name'] : '');
                        $sheet->setCellValue('E' . $counter, !empty($incident) ? $incident[0]['title'] : '');

                        $sheet->setCellValue('F' . $counter, '');
                        $sheet->setCellValue('G' . $counter, '');
                        $sheet->setCellValue('H' . $counter, '');
                        $sheet->setCellValue('I' . $counter, '');
                        $sheet->setCellValue('J' . $counter, '');

                        $sheet->setCellValue('K' . $counter, '');

                        $sheet->setCellValue('L' . $counter, '');
                        $sheet->setCellValue('M' . $counter, '');
                        $sheet->setCellValue('N' . $counter, '');
                        $sheet->setCellValue('O' . $counter, '');
                        $sheet->setCellValue('P' . $counter, '');
                        $sheet->setCellValue('Q' . $counter, '');
                        $sheet->setCellValue('R' . $counter, '');
                        $sheet->setCellValue('S' . $counter, '');
                        $sheet->setCellValue('T' . $counter, '');
                        $sheet->setCellValue('U' . $counter, '');
                        $sheet->setCellValue('V' . $counter, '');
                        $sheet->setCellValue('W' . $counter, '');
                        $sheet->setCellValue('X' . $counter, '');

                    }
                    $counter++;
                }

            }
        }
        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // In this case, we want to write the file in the public directory
        $publicDirectory = $this->params->get('kernel.project_dir') . '/public';
        // e.g /var/www/project/public/my_first_excel_symfony4.xlsx
        $currentDate = new \DateTime();
        $cDate = $currentDate->format('Y_m_d_H_i_s') . '_dayview_sheet.xlsx';

        $excelFilepath = $publicDirectory . '/excelsheet/' . $cDate;

        // Create the file
        $writer->save($excelFilepath);

        $s3result = $this->s3->uploadFile($excelFilepath, $cDate, 'dsp.data.storage.general');

        if (is_string($s3result)) {
            throw new Exception($s3result);
        }

        unlink($excelFilepath);

        return $s3result['ObjectURL'];
    }

    public function getIsRescuerShift($driver, $date)
    {
        $results = $this->createQueryBuilder('dr')
            ->where('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $driver)
            ->andWhere('dr.dateCreated = :date')
            ->andWhere('dr.isRescuer = 1')
            ->setParameter('date', $date)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();


        $rescueNote = $rescueCode = $rescueTime = "";
        foreach ($results as $route) {
            $totalRescueMin = 0;
            $date1 = $route->getStartTime();
            $date2 = $route->getEndTime();

            $diff = $date2->diff($date1);
            $totalRescueMin = (($diff->h * 60) + $diff->i);
            $time = 0;
            if ($this->convertToHoursMins($totalRescueMin, true) < 1) {
                $time = '0' . $this->convertToHoursMins($totalRescueMin, true) . ',';
            } else {
                $time = rtrim($this->convertToHoursMins($totalRescueMin, true), '.') . ',';
            }
            $rescueTime .= $time;
            $rescueNote .= $route->getShiftNote() . ',';
            $routeCodes = $route->getDriverRouteCodes();
            foreach ($routeCodes as $code) {
                $rescueCode .= $code->getCode() . ',';
            }
        }
        return ['codes' => rtrim($rescueCode, ','), 'notes' => rtrim($rescueNote, ','), 'times' => rtrim($rescueTime, ',')];

    }


    public function getSchedularLoadOutListingDataForExport($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = isset($criteria['date']) ? new \DateTime($criteria['date']) : new \DateTime();
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');
        $station = $criteria['stationId'];
        $drivers = $criteria['drivers'];

        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();
        $formatedDate = $currentDateObj->format('l, F dS');
        $sheet->setCellValue('A1', 'Load Out Export for ' . $formatedDate);
        $sheet->setCellValue('A4', 'Driver');
        $sheet->setCellValue('B4', 'ShiftType');
        $sheet->setCellValue('C4', 'Status');
        $sheet->setCellValue('D4', 'Routes');
        $sheet->setCellValue('E4', 'Vehicle');
        $sheet->setCellValue('F4', 'Device');

        $sheet->getStyle('A4:F4')->applyFromArray(['font' => ['bold' => true, 'size' => 10]]);
        $sheet->mergeCells('A1:G3');
        $sheet->getStyle('A1')->applyFromArray(['font' => ['size' => 18]]);
        $counter = 5;

        foreach ($drivers as $driver) {
            $routeCodestring = $devicestring = '';
            foreach ($driver['routes'] as $code) {
                $routeCodestring .= $code . ', ';
            }
            foreach ($driver['devices'] as $device) {
                $devicestring .= $device['name'] . ', ';
            }
            $status = '';
            if (!empty($driver['dateTimeEnded'])) {
                $status = 'Sent Home';
            } else if (!empty($driver['incident']) && $driver['incident']['type'] == 'NCNS') {
                $status = 'NCNS';
            } else if (!empty($driver['incident']) && $driver['incident']['type'] == 'Call In') {
                $status = 'Call In';
            } else if ($driver['punchedIn'] === false && $driver['clockedOut'] === false && $driver['onBreak'] === false && $driver['breakPunchOut'] === false) {
                $status = 'No data';
            } else if ($driver['punchedIn'] && $driver['clockedOut'] === false && $driver['onBreak'] && $driver['breakPunchOut'] === false) {
                $status = 'Out to Lunch';
            } else if ($driver['punchedIn'] && $driver['clockedOut'] === false && $driver['onBreak'] && $driver['breakPunchOut']) {
                $status = 'In from Lunch';
            } else if ($driver['clockedOut']) {
                $status = 'Punched Out';
            } else if ($driver['returned']) {
                $status = 'Returned to Station';
            } else if ($driver['punchedIn'] === false) {
                $status = 'Not Punched In';
            } else if ($driver['insideStation']) {
                $status = 'Inside Station';
            } else if ($driver['onRoute']) {
                $status = 'On Route';
            } else if ($driver['punchedIn'] && $driver['vehicle'] && count($driver['devices']) > 0 && count($driver['routes']) > 0) {
                $status = 'Ready to Stage';
            } else {
                $status = 'Punched In';
            }

            $sentHome = (!empty($driver['dateTimeEnded'])) ? " (Sent Home)" : '';
            $sheet->setCellValue('A' . $counter, $driver['name']);
            $sheet->setCellValue('B' . $counter, $driver['shiftType']['name'] . $sentHome);
            $sheet->setCellValue('C' . $counter, $status);
            $sheet->setCellValue('D' . $counter, rtrim($routeCodestring, ', '));
            $sheet->setCellValue('E' . $counter, $driver['vehicle'] ? $driver['vehicle']['unit'] : '');
            $sheet->setCellValue('F' . $counter, rtrim($devicestring, ', '));
            $counter++;
        }

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // In this case, we want to write the file in the public directory
        $publicDirectory = $this->params->get('kernel.project_dir') . '/public';
        // e.g /var/www/project/public/my_first_excel_symfony4.xlsx

        $currentDate = new \DateTime();
        $cDate = $currentDate->format('Y_m_d_H_i') . '_loadout_sheet.xlsx';

        $excelFilepath = $publicDirectory . '/excelsheet/' . $cDate;

        // Create the file
        $writer->save($excelFilepath);


        $s3result = $this->s3->uploadFile($excelFilepath, $cDate, 'dsp.data.storage.general');

        if (is_string($s3result)) {
            throw new Exception($s3result);
        }

        unlink($excelFilepath);

        return $s3result['ObjectURL'];
    }

    public function getTodayDriverRouteByStation($station, $passedDate = null)
    {
        $stationTz = new \DateTimeZone($station->getTimeZoneName());
        $stationNow = new \DateTime('now', $stationTz);
        $offset = $stationNow->format('P');

        $now = new \DateTime('now');

        if ($passedDate == null) {
            $passedDate = $now;
        } else {
            $passedDate = new \DateTime($passedDate);
            $passedDate->setTime($passedDate->format('h'), $passedDate->format('i'), $passedDate->format('s'));
        }

        $utcDateObj = $passedDate->setTimezone(new \DateTimeZone('UTC'));
        $utcNow = $utcDateObj->format('Y-m-d H:i:00');
        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.driver', 'd')
            ->leftJoin('d.user', 'u')
            ->addCriteria(Common::notArchived('dr'))
            ->andWhere("dr.dateCreated = DATE(CONVERT_TZ(:utcNow, '+00:00', :offset))")
            ->setParameter('utcNow', $utcNow)
            ->setParameter('offset', $offset)
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('d.isArchive = :isArchiveDriver')
            ->setParameter('isArchiveDriver', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->getQuery()
            ->getResult();
    }

    public function getRouteCountByDriverAndScheduleBetweenParticularDates($station, $between)
    {
        return $this->createQueryBuilder('dr')
            ->select('COUNT(dr.id) as betweenDatesDriverRoutesCount,d.id as driverId')
            ->leftJoin('dr.driver', 'd')
            ->addCriteria(Common::notArchived('dr'))
            ->addCriteria(Common::notArchived('d'))
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->andWhere('dr.dateCreated >= :betweenStartDate')
            ->setParameter('betweenStartDate', $between['start'])
            ->andWhere('dr.dateCreated <= :betweenEndDate')
            ->setParameter('betweenEndDate', $between['end'])
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->groupBy('dr.driver')
            ->orderBy('betweenDatesDriverRoutesCount', 'DESC')
            ->getQuery()
            ->getResult();
    }

    public function getDriverRoutesBetweenTwoDatesByStation($station, $between, $exculdeOpenRoute = true)
    {
        $query = $this->createQueryBuilder('dr')
            ->leftJoin('dr.driverRouteCodes', 'rc')
            ->where('rc.id IS NOT NULL')
            ->addCriteria(Common::notArchived('dr'))
            ->andWhere('dr.dateCreated >= :betweenStartDate')
            ->setParameter('betweenStartDate', $between['start'])
            ->andWhere('dr.dateCreated <= :betweenEndDate')
            ->setParameter('betweenEndDate', $between['end'])
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('dr.dateCreated', 'ASC')
            ->groupBy('rc.id');

        if ($exculdeOpenRoute) {
            $query->leftJoin('dr.driver', 'd')->addCriteria(Common::notArchived('d'));
        }
        return $query->getQuery()->getResult();
    }

    public function form_getTodayRoutes($timezone = 'America/Detroit')
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $today = new DateTime('now', new DateTimeZone($timezone));
        $qb = $this->createQueryBuilder('dr')
            ->select('dr.id as id, dr.shiftName as name')
            // ->where('dr.driver = :driver')
            // ->setParameter('driver', $userLogin->getDriver()->getId())
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated <= :today')
            ->setParameter('today', $today)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('dr.dateCreated', 'DESC');
        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param array $criteria
     * @return Collection|DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesByDriverByDate(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('driverId is missing');
        }
        if (empty($criteria['date'])) {
            throw new Exception('startDate is missing');
        }
        $date = new DateTime($criteria['date']);
        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'shift_type')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driverId'])
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date)
            ->orderBy('dr.dateCreated', 'ASC')
            ->addOrderBy('shift_type.startTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $criteria
     * @return Collection|DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesByDriverByRangeDate(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('driverId is missing');
        }
        if (empty($criteria['startDate'])) {
            throw new Exception('startDate is missing');
        }
        if (empty($criteria['endDate'])) {
            throw new Exception('endDate is missing');
        }
        $startDate = new DateTime($criteria['startDate']);
        $endDate = new DateTime($criteria['endDate']);
        return $this->createQueryBuilder('dr')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driverId'])
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('dr.dateCreated <= :endDate')
            ->setParameter('endDate', $endDate)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $criteria
     * @return Collection|DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesByDriverByMonthByYear(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('driverId is missing');
        }
        if (empty($criteria['month'])) {
            throw new Exception('month is missing');
        }
        if (empty($criteria['year'])) {
            throw new Exception('year is missing');
        }

        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'shift_type')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driverId'])
            ->andWhere('MONTH(dr.dateCreated) = :month')
            ->setParameter('month', $criteria['month'])
            ->andWhere('YEAR(dr.dateCreated) = :year')
            ->setParameter('year', $criteria['year'])
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->orderBy('dr.dateCreated', 'ASC')
            ->addOrderBy('shift_type.startTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $criteria
     * @return Collection|DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesByDriverByMonthByYearMobile(array $criteria)
    {
        $driverRoutes = $this->getDriverRoutesByDriverByMonthByYear($criteria);
        $result = [];
        foreach ($driverRoutes as $driverRoute) {
            $result[$driverRoute->getDateCreated()->format('yy-m-d')][] = $driverRoute;
        }
        return [
            'success' => true,
            'driverRoutes' => $result,
            'message' => sizeof($result) . ' DriverRoutes sorted by date fetched',
        ];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getCurrentOpenShiftsForDriver(array $criteria)
    {
        if (empty($criteria['startDate'])) {
            throw new Exception('startDate field missing');
        }
        if (empty($criteria['month'])) {
            throw new Exception('month field missing');
        }
        if (empty($criteria['year'])) {
            throw new Exception('year field missing');
        }

        $user = $this->globalUtils->getTokenUser();
        /** @var Driver $driver */
        $driver = $user->getDriver();
        if (!($driver instanceof Driver)) {
            throw new Exception('User has no Driver attached.');
        }
        $driverSkills = $driver->getDriverSkills();
        $skills = [];
        foreach ($driverSkills as $driverSkill) {
            $skill = $driverSkill->getSkill();
            if ($skill instanceof Skill && $skill->getIsArchive() === false) {
                $skills[] = $skill->getId();
            }
        }

        $driverStations = $driver->getStations();
        $stations = [];
        foreach ($driverStations as $driverStation) {
            $station = $driverStation;
            if ($station instanceof Station && $station->getIsArchive() === false) {
                $stations[] = $station->getId();
            }
        }

        $startDate = new DateTime($criteria['startDate']);
        $driverRoutes = $this->createQueryBuilder('dr')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->andWhere('dr.isArchive = 0')
            ->andWhere('dr.isActive = 1')
//            ->andWhere('dr.isOpenShift = 1')
                // '(dr.driver IS NULL and dr.isOpenShift = 1) or (dr.driver IS NOT NULL and tdr.driver IS NULL and tdr.isNew = 1 and dr.isOpenShift = 0 and tdr.isArchive = 0)'
            ->andWhere('dr.driver IS NULL AND dr.isOpenShift = 1')
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('MONTH(dr.dateCreated) = :month')
            ->setParameter('month', $criteria['month'])
            ->andWhere('YEAR(dr.dateCreated) = :year')
            ->setParameter('year', $criteria['year'])
            ->andWhere('dr.station in (:stations)')
            ->setParameter('stations', $stations)
            ->andWhere('dr.skill in (:skills)')
            ->setParameter('skills', $skills)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($driverRoutes as $driverRoute) {
            $result[$driverRoute->getDateCreated()->format('yy-m-d')][] = $driverRoute;
        }

        if (!empty($criteria['debug'])) {
            return [
                'user' => [
                    'id' => $user->getId(),
                    'email' => $user->getEmail(),
                    'friendlyName' => $user->getFriendlyName(),
                ],
                'driverSkills' => $skills,
                'driverStations' => $stations,
                'shiftSkills' => array_map(function (DriverRoute $item) {
                    return $item->getShiftSkill();
                }, $driverRoutes),
                'skills' => array_map(function (DriverRoute $item) {
                    return $item->getSkill();
                }, $driverRoutes),
                'count' => sizeof($driverRoutes),
                'driverRoutes' => $result,
            ];
        }

        return [
            'driverRoutes' => $result,
        ];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getOpenShiftsForDriver(array $criteria)
    {
        if (empty($criteria['startDate'])) {
            throw new Exception('startDate field missing');
        }
        if (empty($criteria['month'])) {
            throw new Exception('month field missing');
        }
        if (empty($criteria['year'])) {
            throw new Exception('year field missing');
        }

        $user = $this->globalUtils->getTokenUser();
        /** @var Driver $driver */
        $driver = $user->getDriver();
        if (!($driver instanceof Driver)) {
            throw new Exception('User has no Driver attached.');
        }
        $driverSkills = $driver->getDriverSkills();
        $skills = [];
        foreach ($driverSkills as $driverSkill) {
            $skill = $driverSkill->getSkill();
            if ($skill instanceof Skill && $skill->getIsArchive() === false) {
                $skills[] = $skill->getId();
            }
        }

        $driverStations = $driver->getStations();
        $stations = [];
        foreach ($driverStations as $driverStation) {
            $station = $driverStation;
            if ($station instanceof Station && $station->getIsArchive() === false) {
                $stations[] = $station->getId();
            }
        }

        $startDate = new DateTime($criteria['startDate']);
        $driverRoutes = $this->createQueryBuilder('dr')
            ->leftJoin('dr.tempDriverRoutes', 'tdr')
            ->andWhere('dr.isArchive = 0')
            ->andWhere('dr.isActive = 1')
//            ->andWhere('dr.isOpenShift = 1')
            // '(dr.driver IS NULL and dr.isOpenShift = 1) or (dr.driver IS NOT NULL and tdr.driver IS NULL and tdr.isNew = 1 and dr.isOpenShift = 0 and tdr.isArchive = 0)'
            ->andWhere('dr.driver IS NULL AND dr.isOpenShift = 1')
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $startDate)
            ->andWhere('MONTH(dr.dateCreated) = :month')
            ->setParameter('month', $criteria['month'])
            ->andWhere('YEAR(dr.dateCreated) = :year')
            ->setParameter('year', $criteria['year'])
            ->andWhere('dr.station in (:stations)')
            ->setParameter('stations', $stations)
            ->andWhere('dr.skill in (:skills)')
            ->setParameter('skills', $skills)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();

        $result = [];
        /** @var DriverRoute $driverRoute */
        foreach ($driverRoutes as $driverRoute) {
            $date = $driverRoute->getDateCreated()->format('yy-m-d');
            $result[$date]['date'] = $date;
            $result[$date]['id'] = $this->globalUtils->getUniqueIdentifier();
            $result[$date]['list'][] = $driverRoute;
        }

        return [
            'count' => count($result),
            'shifts' => array_values($result),
        ];
    }

    public function convertToHoursMins($time, $flag = false)
    {
        if ($time < 1) {
            return;
        }

        $hours = floor($time / 60);
        $minutes = ($time % 60);
        $hour = $min = '';
        if ($hours > 0) {
            $hour = ($flag) ? $hours : $hours . ' H ';
        }
        if ($minutes) {
            $min = ($flag) ? $minutes : $minutes . ' Min ';
        }

        if ($hour == '' & $min == '') {
            return '';
        } else {
            return ($flag) ? $hour . '.' . $min : $hour . $min;
        }

    }

    public function swapShifts($criteria)
    {
        $currDate = new \DateTime();
        $currDate = $currDate->setTimezone(new \DateTimeZone('UTC'));
        $currDate->setTime(0, 0, 0);

        if ($currDate->format('l') == 'Sunday') {
            $week = $currDate->format('W') + 1;
            $timeStamp = strtotime($currDate->format('Y-m-d') . ' +1 day');
        } else {
            $week = $currDate->format('W');
            $timeStamp = strtotime($currDate->format('Y-m-d'));
        }
        $year = date("Y", strtotime('this week', $timeStamp));
        $weekStartEndDay = $this->getStartAndEndDate($week, $year);

        $currentDate = date("Y-m-d");
        $startTime = date("H:i:s");
        $response = [];
        if ($criteria['isSwap'] === true) {
            if ($criteria['newRouteId'] === null && $criteria['withConfirmation'] === 0) {

                $driverRouteIdArray = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteIds($criteria['newDriverId']);
                $result = $this->createQueryBuilder('dr')
                    ->select('dr.id,sf.name')
                    ->leftJoin('dr.driver', 'dri')
                    ->leftJoin('dr.shiftType', 'sf')
                    ->andWhere('(dr.dateCreated >= :sunday AND dr.dateCreated <= :saturday AND dr.dateCreated >= :currentDate) OR (dr.dateCreated = :currentDate AND dr.startTime >= :shiftStartTime)')
                    ->setParameter('currentDate', $currentDate)
                    ->setParameter('sunday', date('Y-m-d'))
                    ->setParameter('saturday', $weekStartEndDay['week_end'])
                    ->setParameter('shiftStartTime', $startTime)
                    ->andWhere('dri.id = :driverId')
                    ->setParameter('driverId', $criteria['newDriverId'])
                    ->andWhere('dr.isActive = :isActive')
                    ->setParameter('isActive', 1)
                    ->andWhere('dr.isArchive = :isArchive')
                    ->setParameter('isArchive', false)
                    ->orderBy('dr.dateCreated', 'ASC');

                if ($driverRouteIdArray) {
                    $result->andWhere($result->expr()->notIn('dr.id', ':driverRouteId'))
                        ->setParameter('driverRouteId', $driverRouteIdArray);
                }
                $result = $result->getQuery()->getResult();

                if ($result) {
                    $response['success'] = true;
                    $response['result'] = $result;
                    $response['message'] = '';
                } else {
                    $response['success'] = false;
                    $response['result'] = [];
                    $response['message'] = 'The selected driver does not have any remaining, assigned shifts for the week.  Would you like to choose another driver?';
                }

            } else if ($criteria['newRouteId'] !== null && $criteria['withConfirmation'] === 0) {
                $newDriverSkill = $this->getEntityManager()->getRepository('App\Entity\DriverSkill')->getDriverSkill($criteria['newDriverId']);

                $oldDriverSkill = $this->getEntityManager()->getRepository('App\Entity\DriverSkill')->getDriverSkill($criteria['driverId']);

                $oldDriverRoute = $this->find($criteria['driverRouteId']);
                $newDriverRoute = $this->find($criteria['newRouteId']);

                if (in_array($oldDriverRoute->getShiftSkill()->getId(), $newDriverSkill) && in_array($newDriverRoute->getShiftSkill()->getId(), $oldDriverSkill)) {

                    $message = 'You would like to swap your current ' . $oldDriverRoute->getShiftName() . ' shift at ' . $oldDriverRoute->getStartTime()->format('h:iA') . ' on ' . $oldDriverRoute->getDateCreated()->format('M d') . ' for ' . $newDriverRoute->getDriver()->getUser()->getFirstName() . ' ' . $newDriverRoute->getDriver()->getUser()->getLastName() . ' current ' . $newDriverRoute->getShiftName() . ' shift at ' . $newDriverRoute->getStartTime()->format('h:iA') . ' on ' . $newDriverRoute->getDateCreated()->format('M d') . '.';
                    $response['success'] = true;
                    $response['result'] = [];
                    $response['message'] = $message;
                } else {
                    $response['success'] = false;
                    $response['result'] = [];
                    $response['message'] = "Both Drivers need to posses the required skills for each other's Shifts";
                }
            } elseif ($criteria['withConfirmation'] === 1) {

                $oldDriverRoute = $this->find($criteria['driverRouteId']);
                $newDriverRoute = $this->find($criteria['newRouteId']);

                $routeRequests = new RouteRequests();
                $routeRequests->setOldRoute($oldDriverRoute);
                $routeRequests->setNewRoute($newDriverRoute);
                $routeRequests->setOldDriver($oldDriverRoute->getDriver());
                $routeRequests->setNewDriver($newDriverRoute->getDriver());
                $this->getEntityManager()->persist($routeRequests);
                $this->getEntityManager()->flush();


                $AddTempRecord = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->saveTempDriverRoute('SWAP_REQUEST', false, $oldDriverRoute, $newDriverRoute);

                $AddNewTempRecord = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->saveTempDriverRoute('SWAP_REQUEST', false, $newDriverRoute, $oldDriverRoute);

                $oldDriverRoute->setRequestForRoute($routeRequests);
                $this->getEntityManager()->persist($oldDriverRoute);
                $this->getEntityManager()->flush();


                $newDriverRoute->setRequestForRoute($routeRequests);

                $this->getEntityManager()->persist($newDriverRoute);
                $this->getEntityManager()->flush();

                $response['success'] = true;
                $response['result'] = [];
                $response['message'] = 'Swap request sent successfully';
            } else {
                $response['success'] = false;
                $response['result'] = [];
                $response['message'] = 'Required parameter not found !!!';
            }
        } else {
            if (!empty($criteria['reasonForRequest'])) {

                $oldDriverRoute = $this->find($criteria['driverRouteId']);
                $routeRequests = new RouteRequests();
                $routeRequests->setOldRoute($oldDriverRoute);
                $routeRequests->setOldDriver($oldDriverRoute->getDriver());
                $routeRequests->setReasonForRequest($criteria['reasonForRequest']);
                $routeRequests->setIsSwapRequest(false);
                $this->getEntityManager()->persist($routeRequests);
                $this->getEntityManager()->flush();

                $oldDriverRoute->setRequestForRoute($routeRequests);
                $this->getEntityManager()->persist($oldDriverRoute);
                $this->getEntityManager()->flush();

                $AddNewTempRecord = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->saveTempDriverRoute('RELEASE_REQUEST', true, $oldDriverRoute);

                $response['success'] = true;
                $response['result'] = [];
                $response['message'] = 'Drop request sent successfully';

            } else {
                $oldDriverRoute = $this->find($criteria['driverRouteId']);
                $routeRequests = $this->getEntityManager()->getRepository('App\Entity\RouteRequests')->getRouteRequestByParam($criteria['driverRouteId']);
                if (!empty($routeRequests)) {
                    $routeRequests->setNewDriver($this->getEntityManager()->getRepository('App\Entity\Driver')->find($criteria['newDriverId']));
                    $this->getEntityManager()->persist($routeRequests);
                    $this->getEntityManager()->flush();

                    $AddNewTempRecord = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->saveTempDriverRoute('SHIFT_REQUEST', false, $oldDriverRoute, null, $criteria['newDriverId']);
                }
                $response['success'] = true;
                $response['result'] = [];
                $response['message'] = 'Shift request sent successfully';
            }
        }
        return $response;
    }


    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function swapShiftsV2(array $criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $response = [];
        $oldDriverRoute = $this->find($criteria['driverRouteId']);
        $newDriverRoute = $this->find($criteria['newRouteId']);
        $oldDriver = $oldDriverRoute->getDriver();
        $newDriver = $newDriverRoute->getDriver();

        $newSkillRoute = $newDriverRoute->getShiftType()->getSkill()->getName();
        $oldDriverSkills = array_map(function (DriverSkill $item) {
            if ($item->getSkill()) {
                return $item->getSkill()->getName();
            }
            return null;
        }, $oldDriver->getDriverSkills()->toArray());

        if (!in_array($newSkillRoute, $oldDriverSkills)) {
            $response['success'] = false;
            $response['message'] = $oldDriver->getFriendlyName() . ' does not have the skill for the Shift to exchange. The driver need to have ' . $newSkillRoute;
        }
        $oldSkillRoute = $oldDriverRoute->getShiftType()->getSkill()->getName();

        $newDriverSkills = array_map(function (DriverSkill $item) {
            if ($item->getSkill()) {
                return $item->getSkill()->getName();
            }
            return null;
        }, $newDriver->getDriverSkills()->toArray());

        if (!in_array($oldSkillRoute, $newDriverSkills)) {
            $response['success'] = false;
            $response['message'] = $newDriver->getFriendlyName() . ' does not have the skill for the Shift to exchange. The driver need to have ' . $oldSkillRoute;
        }


        if (!($oldDriver instanceof Driver)) {
            throw new Exception('old driver not found.');
        }
        if (!($newDriver instanceof Driver)) {
            throw new Exception('new driver not found.');
        }
        $updateTempDriverRouteResult = $this->getEntityManager()->getRepository(TempDriverRoute::class)->updateTempDriverRoute($oldDriverRoute, $newDriverRoute, $newDriver, $oldDriver);

        $routeRequests = new RouteRequests();
        $routeRequests->setOldRoute($oldDriverRoute);
        $routeRequests->setOldDriver($oldDriver);
        $routeRequests->setNewRoute($newDriverRoute);
        $routeRequests->setNewDriver($newDriver);
        $routeRequests->setIsSwapRequest(true);
        $this->getEntityManager()->persist($routeRequests);
        $this->getEntityManager()->flush();

        $oldDriverRoute->setRequestForRoute($routeRequests);
        $newDriverRoute->setRequestForRoute($routeRequests);
        $this->getEntityManager()->flush();

        $response['success'] = true;
        $response['result'] = [
            'user' => $userLogin instanceof User ? $userLogin->getFriendlyName() : '',
            'dataReceived' => $criteria,
            'updateTempDriverRouteResult' => $updateTempDriverRouteResult
        ];
        $response['message'] = 'Swap request sent successfully';
        return $response;
    }


    public function reassignRouteCode(array $criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $routeCodes = $criteria['routeCodes'];
        $driverRoute = '';
        $driverRoute = $this->em->getRepository('App\Entity\DriverRoute')->findOneBy([
            'id' => $criteria['driverRoute']
        ]);
        $routeCodesArr = [];

        foreach ($driverRoute->getDriverRouteCodes()->filter(function ($routeCode) {
            return !$routeCode->getIsArchive();
        }) as $code) {
            if (in_array($code->getCode(), $routeCodes)) {
                $routeCodesArr[] = $code;
                $driverRoute->removeDriverRouteCode($code);
                $this->em->persist($driverRoute);
                $this->em->flush($driverRoute);
            }
        }

        $newDriverRoute = $this->em->getRepository('App\Entity\DriverRoute')->findOneBy([
            'driver' => $criteria['newDriver'],
            'dateCreated' => $driverRoute->getDateCreated(),
            'isArchive' => false
        ]);

        if (!$newDriverRoute) {
            $newDriverRoute = clone $driverRoute;
            // $newDriverRoute->setPunchIn(null);
            // $newDriverRoute->setPunchOut(null);
            $newDriverRoute->setDriver(
                $this->em->getRepository(Driver::class)->find($criteria['newDriver'])
            );
            foreach ($newDriverRoute->getDriverRouteCodes() as $driverRouteCode) {
                $newDriverRoute->removeDriverRouteCode($driverRouteCode);
                $this->em->persist($newDriverRoute);
                $this->em->flush($newDriverRoute);
            }
        }

        foreach ($routeCodesArr as $code) {
            $newDriverRoute->addDriverRouteCode($code);
            $this->em->persist($newDriverRoute);
            $this->em->flush($newDriverRoute);
        }
        $codeString = implode(" , ", $routeCodes);

        $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->assignRouteCodeEventDataByObject($userLogin, $codeString, $newDriverRoute, $driverRoute);

        return $this->globalUtils->getShiftData($newDriverRoute, $newDriverRoute->getDateCreated()->format('w'), $criteria['timezoneOffset']);
    }

    public function assignRouteCodeEventDataByObject($userLogin, $codeString, $newDriverRoute, $driverRoute)
    {
        if (!empty($codeString)) {
            $messageDataAssign = array('route_code' => $codeString, 'assigned_user' => $newDriverRoute->getDriver()->getFriendlyName(), 'user_name' => $userLogin->getFriendlyName());
            $messageDataUnassign = array('route_code' => $codeString, 'assigned_user' => $driverRoute->getDriver()->getFriendlyName(), 'user_name' => $userLogin->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Route Code Assigned', $driverRoute, 'ROUTE_ASSIGNED', $messageDataAssign, true);
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Route Code Unassigned', $newDriverRoute, 'ROUTE_UNASSIGNED', $messageDataUnassign, true);
        }
    }

    /**
     * @param array $criteria
     * @return DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesSortedByStartTimeFilterByDay(array $criteria)
    {
        $date = new DateTime($criteria['date']);
        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'shift_type')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date)
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $criteria['driverId'])
            ->orderBy('shift_type.startTime', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Assume only first DriverRoute
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function checkIfDriverRoutesExists(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('Driver Id not found.');
        }

        if (empty($criteria['date'])) {
            throw new Exception('Date not found.');
        }

        $driverRoutes = $driverRoutes = $this->_em->getRepository(DriverRoute::class)
            ->getDriverRoutesSortedByStartTimeFilterByDay($criteria);

        $count = sizeof($driverRoutes);
        if ($count < 1) {
            return [
                'success' => false,
                'message' => 'You do not have a shift for today.'
            ];
        }

        if (isset($criteria['debug']) && $criteria['debug']) {
            return [
                'count' => $count,
                'driverRoutes' => $driverRoutes
            ];
        }
        /** @var DriverRoute $driverRoute */
        $driverRoute = $driverRoutes[0];
        $kickoffLog = $driverRoute->getKickoffLog();
        $returnToStation = $driverRoute->getReturnToStation();

        return [
            'count' => $count,
            'driverRoute' => [
                'id' => $driverRoutes[0]->getId(),
            ],
            'kickoffLog' => $kickoffLog instanceof KickoffLog ? [
                'id' => $kickoffLog->getId(),
                'processCompleted' => $kickoffLog->getProcessCompleted()
            ] : null,
            'returnToStation' => $returnToStation instanceof ReturnToStation ? [
                'id' => $returnToStation->getId(),
                'processCompleted' => $returnToStation->getProcessCompleted()
            ] : null,
            'message' => 'Successful check of Driver Route.'
        ];
    }

    /**
     * Unscheduled
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function createUnscheduledDriverRoute(array $criteria)
    {
        $date = new DateTime('now');
        if (!empty($criteria['date'])) {
            $date = new DateTime($criteria['date']);
        }

        /** @var Driver $driver */
        $driver = $this->_em->getRepository(Driver::class)->find($criteria['driverId']);
        if (!($driver instanceof Driver)) {
            throw new Exception('Driver not found.');
        }

        $stations = $driver->getStations();
        if ($stations->count() < 1) {
            throw new Exception('Stations not found.');
        }

        /** @var Station $station */
        $station = $stations->first();
        if (!($station instanceof Station)) {
            throw new Exception('Station not found.');
        }

        $category = 7;
        $shift = $this->_em->getRepository(Shift::class)->findOneBy([
            'isArchive' => false,
            'station' => $station,
            'category' => $category
        ]);

        if (!($shift instanceof Shift)) {
            $company = $driver->getCompany();
            $startTime = new DateTime('now');
            $endTime = new DateTime('now');
            $interval = new DateInterval('PT8H');
            $endTime->add($interval);
            $shift = new Shift();
            $shift->setStartTime($startTime);
            $shift->setEndTime($endTime);
            $shift->setCategory($category);
            $shift->setName('Unscheduled Driver');
            $shift->setColor('#f03063');
            $skill = $this->em->getRepository(Skill::class)->findOneBy(['name' => 'Driver', 'company' => $company->getId()]);
            $shift->setSkill($skill);
            $shift->setCompany($company);
            $shift->setTextColor('#fff');
            $shift->setStation($station);
            $shift->setUnpaidBreak(15);
            $shift->setNote('Unscheduled Driver Shift Note automatically created.');
            $this->_em->persist($shift);
            $this->_em->flush();
        }

        $checkUnscheduledDriverRoute = $this->createQueryBuilder('dr')
            ->where('dr.driver = :driver')
            ->andWhere('dr.dateCreated = :scheduleDate')
            ->andWhere('dr.shiftType = :shift')
            ->setParameter('driver', $driver)
            ->setParameter('scheduleDate', $date)
            ->setParameter('shift', $shift)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getOneOrNullResult();

        if (!empty($checkUnscheduledDriverRoute)) {
            return [
                'message' => "Unscheduled Driver Shift, Driver Route",
                'shift' => [
                    'id' => $shift->getId(),
                ],
                'driverRoute' => [
                    'id' => $checkUnscheduledDriverRoute->getId(),
                ]
            ];
        }

        $unscheduledDriverRoute = $this->_em->getRepository(DriverRoute::class)
            ->addDriverRouteForCortex(
                $shift,
                $date,
                $driver,
                null
            );
        $this->_em->persist($unscheduledDriverRoute);
        $this->_em->flush();

        $unscheduledTempDriverRoute = new TempDriverRoute();
        $unscheduledTempDriverRoute->setStation($station);
        $unscheduledTempDriverRoute->setDriver($driver);
        $unscheduledTempDriverRoute->setShiftType($shift);
        $unscheduledTempDriverRoute->setDateCreated($date);
        $unscheduledTempDriverRoute->setRouteId($unscheduledDriverRoute);
        $unscheduledTempDriverRoute->setStartTime($shift->getStartTime());
        $unscheduledTempDriverRoute->setEndTime($shift->getEndTime());
        $this->_em->persist($unscheduledTempDriverRoute);
        $this->_em->flush();

        if (isset($criteria['debug']) && $criteria['debug']) {
            return [
                'message' => "Unscheduled Driver - DEBUG",
                'shift' => $shift,
                'driverRoute' => $unscheduledDriverRoute,
                'tempDriverRoute' => $unscheduledTempDriverRoute,
                'shifts' => $station->getShifts(),
            ];
        }


        $user = $this->globalUtils->getTokenUser();
        $device = $this->globalUtils->getDevice($criteria);
        $message = $user->getFriendlyName() . " created an Unscheduled Shift.";
        $event = $this->em->getRepository(Event::class)->createEvent('UNSCHEDULED_DRIVER_ROUTE', 'Unscheduled Shift created in Load Out', $message, null, $unscheduledDriverRoute, null, $device);
        $unscheduledDriverRoute->addEvent($event);
        $this->em->flush();
        $eventId = $event->getId();

        $messageManagers = [
            'body' => $message,
            'stationId' => $station->getId(),
            'companyId' => $user->getCompany()->getId(),
        ];

        return [
            'message' => "Unscheduled Driver Shift, Driver Route, Temp. Driver Route",
            'shift' => [
                'id' => $shift->getId(),
            ],
            'driverRoute' => [
                'id' => $unscheduledDriverRoute->getId(),
            ],
            'tempDriverRoute' => [
                'id' => $unscheduledTempDriverRoute->getId(),
            ],
            'event' => [
                'id' => $eventId
            ],
            'messageManagers' => $messageManagers,
        ];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getEvents(array $criteria)
    {
        if (empty($criteria['driverRouteId'])) {
            throw new Exception('The Driver Route Id is missing.');
        }

        $driverRoute = $this->em->find(DriverRoute::class, $criteria['driverRouteId']);

        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.');
        }

        $events = $this->em->getRepository(Event::class)
            ->findBy(['driverRoute' => $criteria['driverRouteId'], 'isActive' => true], ['id' => 'DESC']);

        return [
            'driverRoute' => $driverRoute,
            'events' => $events,
        ];
    }

    /**
     * @param int $driverRouteId Driver route ID.
     * @param int $requestTimeZone
     *
     * @return array Shift data array.
     */
    public function getShiftDataById($criteria)
    {
        if (!isset($criteria['driverRouteId'])) {
            return ['success' => false, 'message' => 'Please pass driver route ID!!!'];
        }
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $driverRouteObj = $this->find($criteria['driverRouteId']);
        if (!($driverRouteObj instanceof DriverRoute)) {
            return ['success' => false, 'message' => 'Driver Route not found'];
        }
        $weekNumber = $driverRouteObj->getDateCreated()->format('W');
        return $this->globalUtils->getShiftData($driverRouteObj, $weekNumber, $requestTimeZone);
    }

    /**
     * @param array $criteria
     * @return DriverRoute[]
     * @throws Exception
     */
    public function getDriverRoutesOfToday(array $criteria)
    {
        $date = new DateTime('today');
        return $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param array $criteria
     * @return DriverRoute[]
     * @throws Exception
     */
    public function asignOpenRouteOnDriver(array $criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverId = isset($criteria['driverId']) ? $criteria['driverId'] : '';
        $routeId = isset($criteria['routeId']) ? $criteria['routeId'] : '';
        if ($driverId && $routeId) {
            $driverRoute = $this->find($routeId);
            $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
            $checkDriverRoute = $this->getLoadoutTodayDriverRoutes($driverId, $driverRoute->getDateCreated()->format('Y-m-d'));
            if (!empty($checkDriverRoute) && isset($checkDriverRoute[0])) {
                $shiftDriverRoute = array_filter($checkDriverRoute, function ($drObj, $v) use ($driverRoute) {
                    return $drObj->getShiftType()->getId() === $driverRoute->getShiftType()->getId();
                }, ARRAY_FILTER_USE_BOTH);
                $newDriverRoute = !empty($shiftDriverRoute) ? $shiftDriverRoute[0] : $checkDriverRoute[0];
                $newDriverRoute->setSkill($driverRoute->getSkill());
                $newDriverRoute->setStation($driverRoute->getStation());
                $newDriverRoute->setSchedule($driverRoute->getSchedule());
                $newDriverRoute->setShiftType($driverRoute->getShiftType());
                $newDriverRoute->setIsTemp(false);
                $newDriverRoute->setIsOpenShift(false);
                $newDriverRoute->setShiftName($driverRoute->getShiftName());
                $newDriverRoute->setShiftColor($driverRoute->getShiftColor());
                $newDriverRoute->setUnpaidBreak($driverRoute->getUnpaidBreak());
                $newDriverRoute->setShiftNote($driverRoute->getShiftNote());
                $newDriverRoute->setShiftInvoiceType($driverRoute->getShiftInvoiceType());
                $newDriverRoute->setShiftStation($driverRoute->getShiftStation());
                $newDriverRoute->setShiftSkill($driverRoute->getShiftSkill());
                $newDriverRoute->setShiftCategory($driverRoute->getShiftCategory());
                $newDriverRoute->setShiftBalanceGroup($driverRoute->getShiftBalanceGroup());
                $newDriverRoute->setShiftTextColor($driverRoute->getShiftTextColor());
                $newDriverRoute->setStartTime($driverRoute->getStartTime());
                $newDriverRoute->setEndTime($driverRoute->getEndTime());
                $newDriverRoute->setIsRescuer($driverRoute->getIsRescuer());
                $newDriverRoute->setIsRescued($driverRoute->getIsRescued());
                $newDriverRoute->setIsLoadOut($driverRoute->getIsLoadOut());
                $newDriverRoute->setIsBackup($driverRoute->getIsBackup());
                $newDriverRoute->setCurrentNote($driverRoute->getCurrentNote());
                $newDriverRoute->setNewNote($driverRoute->getNewNote());
                // $newDriverRoute->setPunchIn(null);
                // $newDriverRoute->setPunchOut(null);
                $newDriverRoute->setBreakPunchOut(null);
                $newDriverRoute->setBreakPunchIn(null);
            } else {
                $newDriverRoute = clone $driverRoute;
                $newDriverRoute->setDriver($driver);
                $newDriverRoute->setIsOpenShift(0);
                // $newDriverRoute->setPunchIn(null);
                // $newDriverRoute->setPunchOut(null);
                $newDriverRoute->setBreakPunchOut(null);
                $newDriverRoute->setBreakPunchIn(null);
            }
            if ($driverRoute->getVehicle()) {
                $newDriverRoute->setVehicle($driverRoute->getVehicle());
            }
            $this->em->persist($newDriverRoute);
            $this->em->flush($newDriverRoute);

            foreach ($driverRoute->getDriverRouteCodes() as $routeCode) {
                $newDriverRoute->addDriverRouteCode($routeCode);
                $this->em->persist($newDriverRoute);
                $this->em->flush($newDriverRoute);
                $driverRoute->removeDriverRouteCode($routeCode);
                $this->em->persist($driverRoute);
                $this->em->flush($driverRoute);
            }
            foreach ($driverRoute->getDevice() as $device) {
                $newDriverRoute->addDevice($device);
                $this->em->persist($newDriverRoute);
                $this->em->flush($newDriverRoute);
            }
            $driverRoute->setIsArchive(true);
            $this->em->persist($driverRoute);
            $this->em->flush($driverRoute);

            $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->assignOpenRouteToDriverForTimeline($userLogin, $driverId, $newDriverRoute);
        }
        return true;
    }

    public function assignOpenRouteToDriverForTimeline($userLogin, $driverId, $driverRoute, $isActive = true)
    {
        $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
        $messageData = array('driver' => $driver->getUser()->getFriendlyName(), 'user_name' => $userLogin->getFriendlyName());

        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Assign OpenRoute To Driver', $driverRoute, 'ASSIGN_OPENROUTE_TO_DRIVER', $messageData, true);
    }

    public function conflictRouteQuery($dates, $drivers, $schedule)
    {
        $qb = $this->createQueryBuilder('dr')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', true)
            ->andWhere('dr.schedule IS NOT NULL')
            ->orderBy('dr.dateCreated', 'ASC');

        $qb->andWhere($qb->expr()->in('dr.driver', ':driverId'))
            ->setParameter('driverId', $drivers);
        $qb->andWhere($qb->expr()->in('dr.dateCreated', ':createdDates'))
            ->setParameter('createdDates', $dates);
        if ($schedule > 0) {
            $qb->andWhere('dr.schedule != :scheduleId')
                ->setParameter('scheduleId', $schedule);
        }

        return $qb->getQuery()->getResult();
    }

    public function getConflictDriverRoutes(array $criteria)
    {

        $scheduleDateArray = $temp = [];
        $dateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currentDate = $dateObj->format('Y-m-d');

        $dayname = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

        $getStartYear = new \DateTime($criteria['startDate']);
        $currentDate = $getStartYear->format('Y-m-d');
        $getEndYear = new \DateTime($criteria['endDate']);
        $endDate = $getEndYear->format('Y-m-d');
        $startYear = (int)$getStartYear->format('Y');
        $endYear = (int)$getEndYear->format('Y');

        foreach ($criteria['design'] as $design) {
            foreach ($design as $ds) {

                foreach ($ds['days'] as $key => $day) {
                    if ($day[$dayname[$key]] === 1) {
                        $date = $dateObj->setISODate($getStartYear->format('Y'), $ds['week'], $key)->format('Y-m-d');
                        if ($date >= $currentDate && $date <= $endDate) {
                            $scheduleDateArray[] = $date;
                        }
                    }
                }
            }
        }

        $results = $this->conflictRouteQuery($scheduleDateArray, $criteria['driver'], $criteria['scheduleId']);

        $conflictArray = [];
        $requestTimeZone = 0;
        foreach ($results as $result) {

            $startTimeObj = !empty($result->getPunchIn()) ? $result->getPunchIn() : $result->getStartTime();
            $endTimeObj = !empty($result->getPunchOut()) ? $result->getPunchOut() : $result->getEndTime();

            $startTime = $startTimeObj->format('G.i');
            $endTime = $endTimeObj->format('G.i');

            $startTimeObj = $this->globalUtils->getTimeZoneConversation($startTimeObj, $requestTimeZone);
            $endTimeObj = $this->globalUtils->getTimeZoneConversation($endTimeObj, $requestTimeZone);
            $endTimeOfPriorShiftObj = !empty($endTimeOfPriorShiftObj) ? $this->globalUtils->getTimeZoneConversation($endTimeOfPriorShiftObj, $requestTimeZone) : null;

            $startHour = $startTimeObj->format($this->params->get('time_format'));
            $endHour = $endTimeObj->format($this->params->get('time_format'));


            $conflictArray[$result->getDriver()->getId()][] = [
                'id' => $result->getId(),
                "driverName" => $result->getDriver()->getFullName(),
                'date' => $result->getDateCreated()->format('Y-m-d'),
                'time' => $startHour . ' - ' . $endHour,
                'type' => $result->getShiftName(),
                'category' => $result->getShiftType()->getCategory(),
                'bg' => $result->getShiftColor(),
                'color' => $result->getShiftTextColor(),
                'skill' => !empty($result->getSkill()) ? $result->getSkill()->getId() : '',
                "title" => $result->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                "img" => $result->getDriver()->getUser()->getProfileImage(),
            ];
        }
        return array_values($conflictArray);
    }

    public function getDriverTimeline($criteria)
    {
        $date = $this->globalUtils->getDatesOfQuarter('byday', [7, 0]);

        $driverRoutesArr = $this->createQueryBuilder('dr')
            ->where('dr.driver = :driverId')
            ->setParameter('driverId', $criteria['driverId'])
            ->andWhere('dr.dateCreated >= :startDate')
            ->setParameter('startDate', $date['start'])
            ->andWhere('dr.dateCreated <= :endDate')
            ->setParameter('endDate', $date['end'])
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult();

        $driverRouteDate = [];
        for ($count = 0; $count < 7; $count++) {
            $driverRouteDate[$date['start']->modify('+1 day')->format('Y-m-d')] = [];
        }

        foreach ($driverRoutesArr as $driverRoute) {
            $driverRouteTempData = [];
            $driverRouteTempData['id'] = $driverRoute->getId();
            // for Backup Driver
            if ($driverRoute->getIsBackUp() === true) {
                $driverRouteTempData['worked'] = 'Backup Driver';
                if (!empty($driverRoute->getDateTimeEnded())) {
                    $driverRouteTempData['sent home'] = $driverRoute->getDateTimeEnded()->format('H:i');
                }
                $lines = [
                    [
                        'type' => 'breakOut',
                        'className' => '',
                        'information' => '',
                        'iconType' => 'LunchOut',
                        'data' => $driverRoute->getBreakPunchOut(),
                        'title' => 'Break Out ',
                    ],
                    [
                        'type' => 'breakIn',
                        'className' => '',
                        'information' => '',
                        'iconType' => 'LunchIn',
                        'data' => $driverRoute->getBreakPunchIn(),
                        'title' => 'Break In ',
                    ],
                    [
                        'type' => 'punchIn',
                        'information' => 'Clocked In',
                        'iconType' => 'PunchIn',
                        'data' => $driverRoute->getPunchIn(),
                        'title' => 'Clocked In ',
                    ],
                    [
                        'type' => 'punchOut',
                        'information' => '',
                        'iconType' => 'PunchOut',
                        'data' => $driverRoute->getPunchOut(),
                        'title' => 'Clocked Out ',
                    ],
                ];

                usort($lines, function ($a, $b) {
                    return $b['data'] > $a['data'];
                });
                $driverRouteTempData['timeline'] = $lines;
            } else {
                // for Rescue Route
                if (!empty($driverRoute->getRequestForRoute())) {
                    if ($driverRoute->getSchedule()) {
                        $driverRouteTempData['swapped_shift'] = [
                            'name' => $driverRoute->getRequestForRoute()->getNewDriver()->getUser()->getFriendlyName(),
                            'schedule' => $driverRoute->getRequestForRoute()->getNewRoute()->getSchedule()->getName(),
                            'shiftTime' => $driverRoute->getRequestForRoute()->getNewRoute()->getShiftType()->getStartTime()->format('H:i'),
                            'Shift' => $driverRoute->getRequestForRoute()->getNewRoute()->getShiftType()->getName()
                        ];
                        $driverRouteTempData['assigned'] = [
                            'name' => $driverRoute->getDriver()->getUser()->getFriendlyName(),
                            'schedule' => $driverRoute->getSchedule()->getName(),
                            'shiftTime' => $driverRoute->getShiftType()->getStartTime()->format('H:i'),
                            'shift' => $driverRoute->getShiftType()->getName()
                        ];
                    }
                    $driverRouteTempData['Worked'] = $driverRoute->getShiftType()->getName();
                } else { // for Regular Route
                    if ($driverRoute->getSchedule()) {
                        $driverRouteTempData['assigned'] = [
                            'schedule' => $driverRoute->getSchedule()->getName(),
                            'shiftTime' => $driverRoute->getShiftType()->getStartTime()->format('H:i'),
                            'shift' => $driverRoute->getShiftType()->getName()
                        ];
                        //$driverRoute->getSchedule() ? $driverRoute->getSchedule()->getName() .','. $driverRoute->getShiftType()->getStartTime()->format('H:i') .','.$driverRoute->getShiftType()->getName() : '';
                    }
                    $driverRouteTempData['Worked'] = $driverRoute->getShiftType()->getName();
                }

                $lines = [
                    [
                        'type' => 'breakOut',
                        'className' => '',
                        'information' => '',
                        'iconType' => 'LunchOut',
                        'data' => $driverRoute->getBreakPunchOut(),
                        'title' => 'Break Out ',
                    ],
                    [
                        'type' => 'breakIn',
                        'className' => '',
                        'information' => '',
                        'iconType' => 'LunchIn',
                        'data' => $driverRoute->getBreakPunchIn(),
                        'title' => 'Break In ',
                    ],
                    [
                        'type' => 'punchIn',
                        'information' => 'Clocked In',
                        'iconType' => 'PunchIn',
                        'data' => $driverRoute->getPunchIn(),
                        'title' => 'Clocked In ',
                    ],
                    [
                        'type' => 'punchOut',
                        'information' => '',
                        'iconType' => 'PunchOut',
                        'data' => $driverRoute->getPunchOut(),
                        'title' => 'Clocked Out ',
                    ],
                ];

                usort($lines, function ($a, $b) {
                    return $b['data'] > $a['data'];
                });
                $driverRouteTempData['timeline'] = $lines;
                $tempRoute = [];
                foreach ($driverRoute->getDriverRouteCodes() as $routes) {
                    $tempRoute[] = $routes->getCode();
                }
                $tempDevice = [];
                foreach ($driverRoute->getDevice() as $device) {
                    $tempDevice[] = $device->getName();
                }
                if (!empty($driverRoute->getReturnToStation()) && !empty($driverRoute->getReturnToStation()->getLoggedStationCheckAt())) {
                    $driverRouteTempData['return_to_station'] = $driverRoute->getReturnToStation()->getLoggedStationCheckAt()->format('H:i');
                }
                $driverRouteTempData['route'] = (count($tempRoute) > 0 ? implode(", ", $tempRoute) : '');
                $driverRouteTempData['vehicle'] = ($driverRoute->getVehicle() ? $driverRoute->getVehicle()->getVehicleId() : '');
                $driverRouteTempData['device'] = (count($tempDevice) > 0 ? implode(", ", $tempDevice) : '');
                $driverRouteTempData['incident'] = (count($driverRoute->getIncidents()));
                $driverRouteTempData['rescuer'] = ($driverRoute->getIsRescuer() === false ? '0' : '1');
                $driverRouteTempData['rescued'] = ($driverRoute->getIsRescued() === false ? '0' : '1');

            }

            // for unfold day
            $unfoldDay = [
                [
                    'type' => 'punchIn',
                    'information' => 'Clocked In',
                    'iconType' => 'PunchIn',
                    'data' => $driverRoute->getPunchIn(),
                    'title' => 'Paycom Punch In ' . (($driverRoute->getPunchIn()) ? $driverRoute->getPunchIn()->format('H:i') : ($driverRoute->getInsideParkingLotAt() ? 'not Reported, estimated At ' . $driverRoute->getInsideParkingLotAt()->modify("+1 minutes")->format('H:i') : '')),
                ],
                [
                    'type' => 'punchInReported',
                    'information' => '',
                    'iconType' => 'PunchInReported',
                    'data' => $driverRoute->getPunchIn(),
                    'title' => 'Completed Punch In Reported : ' . (!empty($driverRoute->getPunchIn()) ? 'Yes' : 'No'),
                ],
                [
                    'type' => 'insideParkingLotAt',
                    'iconType' => 'ParkingGeoIn',
                    'information' => '',
                    'data' => $driverRoute->getInsideParkingLotAt(),
                    'title' => 'Entered Parking Lot Geo-fence At ',
                ],
                [
                    'type' => 'insideStationAt',
                    'information' => '',
                    'iconType' => 'GeofenceIn',
                    'data' => $driverRoute->getInsideStationAt(),
                    'title' => 'Entered Station Geo-fence At ',
                ],
                [
                    'type' => 'outsideParkingLotAt',
                    'iconType' => 'ParkingGeoOut',
                    'information' => '',
                    'data' => $driverRoute->getOutsideParkingLotAt(),
                    'title' => 'Left Parking Lot Geo-fence At ',
                ],
                [
                    'type' => 'outsideStationAt',
                    'information' => '',
                    'iconType' => 'GeofenceOut',
                    'data' => $driverRoute->getOutsideStationAt(),
                    'title' => 'Left Station Geo-fence At ',
                ],
                [
                    'type' => 'breakOut',
                    'className' => '',
                    'information' => '',
                    'iconType' => 'LunchOut',
                    'data' => $driverRoute->getBreakPunchOut(),
                    'title' => 'Lunch Out At ',
                ],
                [
                    'type' => 'breakIn',
                    'className' => '',
                    'information' => '',
                    'iconType' => 'LunchIn',
                    'data' => $driverRoute->getBreakPunchIn(),
                    'title' => 'Lunch In At ',
                ],
                [
                    'type' => 'punchOut',
                    'information' => '',
                    'iconType' => 'PunchOut',
                    'data' => $driverRoute->getPunchOut(),
                    'title' => 'Punch out at ',
                ],
                [
                    'type' => 'routePackages',
                    'information' => '',
                    'iconType' => 'routePackages',
                    'data' => "",
                    'title' => "Route : " . ((count($tempRoute) > 0) ? implode(", ", $tempRoute) : "N/A ") . " | " . (!empty($driverRoute->getCountOfTotalPackages()) ? $driverRoute->getCountOfTotalPackages() . " Packages" : "0 Packages"),
                ],
                [
                    'type' => 'vanAsigned',
                    'information' => '',
                    'iconType' => 'vanAsigned',
                    'data' => (!empty($driverRoute->getReturnToStation())) ? $driverRoute->getReturnToStation()->getLoggedMileageAndGasAt() : null,
                    'title' => "Van Assigned : " . (!empty($driverRoute->getVehicle()) ? $driverRoute->getVehicle()->getVehicleId() : "N/A") . " | Mileage : " . ((!empty($driverRoute->getReturnToStation()) && !empty($driverRoute->getReturnToStation()->getMileage())) ? $driverRoute->getReturnToStation()->getMileage() : "N/A") . " | Tank : " . ((!empty($driverRoute->getReturnToStation()) && !empty($driverRoute->getReturnToStation()->getGasTankLevel())) ? $driverRoute->getReturnToStation()->getGasTankLevel() : "N/A"),
                ],
                [
                    'type' => 'swappedVanAssign',
                    'information' => '',
                    'iconType' => 'swappedVanAssign',
                    'data' => null,
                    'title' => "Swapped Van Assignment : N/A | Mileage : N/A | Tank : N/A",
                ],
                [
                    'type' => 'mobileAssigned',
                    'information' => '',
                    'iconType' => 'mobileAssigned',
                    'data' => null,
                    'title' => "Mobile Device Assigned",
                ],
                [
                    'type' => 'loadoutProcedure',
                    'information' => '',
                    'iconType' => 'loadoutProcedure',
                    'data' => null,
                    'title' => "Started Load Out procedure",
                ],
                [
                    'type' => 'completedPunchInReported',
                    'information' => '',
                    'iconType' => 'completedPunchInReported',
                    'data' => null,
                    'title' => "completed Punch In Reported",
                ],
                [
                    'type' => 'completedMentorReported',
                    'information' => '',
                    'iconType' => 'completedMentorReported',
                    'data' => null,
                    'title' => "completed Mentor Reported",
                ],
                [
                    'type' => 'routeDelivered',
                    'information' => '',
                    'iconType' => 'routeDelivered',
                    'data' => null,
                    'title' => "Route 100% Delivered. 2:43pm, 2 missing packages, etc al....?",
                ],
                [
                    'type' => 'rescueRouteDelivered',
                    'information' => '',
                    'iconType' => 'rescueRouteDelivered',
                    'data' => null,
                    'title' => "Rescue 100% delivered. 4:45, 0 missing packages, etc al...?",
                ],
                [
                    'type' => 'rescueRouteDelivered',
                    'information' => '',
                    'iconType' => 'rescueRouteDelivered',
                    'data' => null,
                    'title' => "Van Assignment Confirmed : N/A | Mileage : N/A | Tank : N/A",
                ],
                [
                    'type' => 'returnToStationProcedure',
                    'information' => '',
                    'iconType' => 'returnToStationProcedure',
                    'data' => (!empty($driverRoute->getReturnToStation())) ? $driverRoute->getReturnToStation()->getLoggedMileageAndGasAt() : null,
                    'title' => "Started Return to Station procedure ",
                ],
                [
                    'type' => 'enterStationAt',
                    'information' => '',
                    'iconType' => 'enterStationAt',
                    'data' => (!empty($driverRoute->getReturnToStation())) ? $driverRoute->getReturnToStation()->getLoggedStationCheckAt() : null,
                    'title' => "Enter Station Geo-Fence ",
                ],
                [
                    'type' => 'enteredParkingLot',
                    'information' => '',
                    'iconType' => 'enteredParkingLot',
                    'data' => (!empty($driverRoute->getReturnToStation())) ? $driverRoute->getReturnToStation()->getLoggedReturnToParkingLotAt() : null,
                    'title' => "Entered Parking Lot Geo-fence ",
                ],
                [
                    'type' => 'leftStationAt',
                    'information' => '',
                    'iconType' => 'leftStationAt',
                    'data' => (!empty($driverRoute->getReturnToStation())) ? $driverRoute->getReturnToStation()->getLoggedOutPayrollAt() : null,
                    'title' => "Left Station Geo-Fence ",
                ],
            ];
            foreach ($driverRoute->getIncidents() as $incident) {
                array_push($unfoldDay, [
                    'type' => 'incident',
                    'iconType' => 'incident',
                    'information' => '',
                    'data' => $incident->getDateTime(),
                    'title' => 'Incident ' . $incident->getIncidentType()->getPrefix() . '#' . sprintf("%04d", $incident->getId()) . ' Created at ',
                ]);
            }
            if ($driverRoute->getIsRescued() == 1) {
                $routeCodes = [];
                foreach ($driverRoute->getDriverRouteCodes() as $codes) {
                    $routeCodes[] = $codes->getId();
                }
                $qb = $this->createQueryBuilder('dr');
                $rescuedRouteData = $qb->leftJoin('dr.shiftType', 'st')
                    ->leftJoin('dr.driverRouteCodes', 'drc')
                    ->andWhere('dr.isRescuer = 1')
                    ->andWhere('dr.dateCreated = :dateCreated')
                    ->andWhere('dr.isArchive = :isArchive')
                    ->andWhere($qb->expr()->in('drc.id', ':codeId'))
                    ->andWhere('st.name = :name')
                    ->setParameter('name', 'Rescue')
                    ->setParameter('dateCreated', $driverRoute->getDateCreated()->format('Y-m-d'))
                    ->setParameter('codeId', $routeCodes)
                    ->setParameter('isArchive', false)
                    ->getQuery()
                    ->getResult();

                foreach ($rescuedRouteData as $rescuedRoute) {
                    $tempRoute = [];
                    foreach ($rescuedRoute->getDriverRouteCodes() as $routes) {
                        $tempRoute[] = $routes->getCode();
                    }

                    array_push($unfoldDay, [
                        'type' => 'rescueRoutePackages',
                        'iconType' => 'rescueRoutePackages',
                        'information' => '',
                        'data' => null,
                        'title' => "Route : " . ((count($tempRoute) > 0) ? implode(", ", $tempRoute) : " N/A ") . " | " . (!empty($rescuedRoute->getCountOfTotalPackages()) ? $rescuedRoute->getCountOfTotalPackages() . " Packages" : "0 Packages"),
                    ]);
                }
            }

            usort($unfoldDay, function ($a, $b) {
                return $b['data'] > $a['data'];
            });
            $driverRouteTempData['unfoldDay'] = $unfoldDay;

            array_push($driverRouteDate[$driverRoute->getDateCreated()->format('Y-m-d')], $driverRouteTempData);
        }
        return $driverRouteDate;
    }

    public function disableConflictRoutes($effectivedate, $dates, $drivers, $scheduleId)
    {
        $results = $this->conflictRouteQuery($dates, $drivers, $scheduleId);
        $dateArray = $driverArray = [];
        if ($results) {

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update
            ('App\Entity\DriverRoute', 'dr')
                ->set('dr.isActive', 1)
                ->where('dr.schedule = :sheduleId')
                ->setParameter('sheduleId', $scheduleId)
                ->andwhere('dr.dateCreated > :dateCreated')
                ->setParameter('dateCreated', $effectivedate);
            $qb->getQuery()->execute();

            foreach ($results as $result) {
                if ($result->getDateCreated() > $effectivedate) {
                    $result->setIsActive(0);
                    $this->getEntityManager()->persist($result);
                    $this->getEntityManager()->flush($result);
                } else {
                    $dateArray[] = $result->getDateCreated()->format('Y-m-d');
                    $driverArray[] = $result->getDriver()->getId();
                }
            }

            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update
            ('App\Entity\DriverRoute', 'dr')
                ->set('dr.isActive', 0)
                ->where('dr.schedule = :sheduleId')
                ->setParameter('sheduleId', $scheduleId);
            $qb->andWhere($qb->expr()->in('dr.driver', ':driverId'))
                ->setParameter('driverId', $driverArray);
            $qb->andWhere($qb->expr()->in('dr.dateCreated', ':createdDates'))
                ->setParameter('createdDates', $dateArray);
            $qb->getQuery()->execute();

            return true;
        } else {
            return false;
        }
    }

    public function getTodaysRouteCodeCounts($station, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');
        $incidentQuery = $this->createQueryBuilder('dr')
            ->select('dr.id')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.incidents', 'inc')
            ->leftJoin('dr.driver', 'dri')
            ->andWhere('dr.dateCreated = :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('inc.isArchive = :incIsArchive')
            ->setParameter('incIsArchive', false)
            ->andWhere('dri.isArchive = :driverIsArchive')
            ->setParameter('driverIsArchive', false)
            ->andWhere('st.isArchive = :stationIsArchive')
            ->setParameter('stationIsArchive', false);
        if ($station) {
            $incidentQuery->andWhere('st.id = :station')
                ->setParameter('station', $station);
        }

        $resultsInc = $incidentQuery->getQuery()->getResult();

        $query = $this->createQueryBuilder('dr')
            ->select('drc.id as codes')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'dri')
            ->andWhere('dr.dateCreated = :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('st.isArchive = :stationIsArchive')
            ->setParameter('stationIsArchive', false)
            ->andWhere('dri.isArchive = :driverIsArchive OR dr.driver IS NULL')
            ->setParameter('driverIsArchive', false)
            ->groupBy('drc.code');
        if ($resultsInc) {
            $query->andWhere($query->expr()->notIn('dr.id', ':incidentRoutes'))
                ->setParameter('incidentRoutes', $resultsInc);
        }

        if ($station) {
            $query->andWhere('st.id = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getResult();

        if ($results) {
            $results = count($results);
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getTodaysRouteCodeCountsNew($station, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');
        $incidentQuery = $this->createQueryBuilder('dr')
            ->select('dr.id')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.incidents', 'inc')
            ->leftJoin('dr.driver', 'dri')
            ->andWhere('dr.dateCreated = :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('inc.isArchive = :incIsArchive')
            ->setParameter('incIsArchive', false)
            ->andWhere('dri.isArchive = :driverIsArchive')
            ->setParameter('driverIsArchive', false)
            ->andWhere('st.isArchive = :stationIsArchive')
            ->setParameter('stationIsArchive', false);
        if ($station) {
            $incidentQuery->andWhere('st.id = :station')
                ->setParameter('station', $station);
        }

        $resultsInc = $incidentQuery->getQuery()->getResult();

        $query = $this->createQueryBuilder('dr')
            ->select('drc.id as codes')
            ->leftJoin('dr.driverRouteCodes', 'drc')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'dri')
            ->andWhere('dr.dateCreated = :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('drc.isArchive = :routeCodeIsArchive')
            ->setParameter('routeCodeIsArchive', false)
            ->andWhere('st.isArchive = :stationIsArchive')
            ->setParameter('stationIsArchive', false)
            ->andWhere('dri.isArchive = :driverIsArchive OR dr.driver IS NULL')
            ->setParameter('driverIsArchive', false)
            ->groupBy('drc.code');
        if ($resultsInc) {
            $query->andWhere($query->expr()->notIn('dr.id', ':incidentRoutes'))
                ->setParameter('incidentRoutes', $resultsInc);
        }

        if ($station) {
            $query->andWhere('st.id = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getResult();

        if ($results) {
            $results = count($results);
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getDriverRouteForIncident(array $criteria)
    {
        $station = $criteria['station'];
        $driver = $criteria['driver'];
        $dateCreated = $criteria['dateCreated'];
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $driverRoutes = $this->createQueryBuilder('dr')
            ->andWhere('dr.driver = :driver')
            ->setParameter('driver', $driver)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $dateCreated)
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->orderBy('dr.dateCreated', 'ASC')
            ->getQuery()
            ->getResult();
        $driverRoutesArr = [];
        foreach ($driverRoutes as $driverRoute) {
            $startTime = $this->globalUtils->getTimeZoneConversation($driverRoute->getStartTime(), $criteria['timezoneOffset']);
            $endTime = $this->globalUtils->getTimeZoneConversation($driverRoute->getEndTime(), $criteria['timezoneOffset']);
            $tempDriverRoutesArr['id'] = $driverRoute->getId();
            $tempDriverRoutesArr['startTime'] = $startTime->format($this->params->get('time_format'));
            $tempDriverRoutesArr['endTime'] = $endTime->format($this->params->get('time_format'));
            $tempDriverRoutesArr['time'] = $startTime->format($this->params->get('time_format')) . ' - ' . $endTime->format($this->params->get('time_format'));
            $tempDriverRoutesArr['shiftType']['name'] = $driverRoute->getShiftName();
            $tempDriverRoutesArr['shiftType']['color'] = $driverRoute->getShiftColor();
            $tempDriverRoutesArr['shiftType']['textColor'] = $driverRoute->getShiftTextColor();
            $driverRoutesArr[] = $tempDriverRoutesArr;
        }
        return $driverRoutesArr;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function loadOutAndRTSMobile(array $criteria)
    {
        $device = $this->em->getRepository(Device::class)->findOneBy(['id' => $criteria['deviceId'], 'isArchive' => false, 'company' => $criteria['companyId']]);
        if (!($device instanceof Device)) {
            throw new Exception('Device not found');
        }
        if ($device->getAllowKickoff() !== true) {
            throw new Exception('Device is not allowed');
        }
        $now = new DateTime($criteria['date']);
        $driverRoute = $this->findOneBy(['driver' => $criteria['driverId'], 'dateCreated' => $now, 'isArchive' => false]);
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found');
        }
        if (!empty($criteria['mobileVersionUsed'])) {
            $driverRoute->setMobileVersionUsed($criteria['mobileVersionUsed']);
        }

        if (!empty($criteria['RTS']) && $criteria['RTS']) {
            $returnToStation = $driverRoute->getReturnToStation();
            if (!($returnToStation instanceof ReturnToStation)) {
                $returnToStation = new ReturnToStation();
                $returnToStation->setDevice($device);
                $returnToStation->setDriverRoute($driverRoute);
                $returnToStation->setStationCheckListResult([
                    ["name" => "Are you inside the station?", "checked" => false],
                    ["name" => "Windows down?", "checked" => false],
                    ["name" => "Light on?", "checked" => false],
                    ["name" => "Flasher on?", "checked" => false],
                    ["name" => "Turn off van when parked", "checked" => false],
                    ["name" => "Leave the key in the ignition", "checked" => false],
                    ["name" => "Return empty totes to the Station designated location", "checked" => false],
                    ["name" => "Check-in with Amazon RTS personnel to report route completion", "checked" => false],
                    ["name" => "Return any remaining and/or report any missing packages", "checked" => false],
                ]);
                $this->_em->persist($returnToStation);
                $this->_em->flush();
                $driverRoute->setReturnToStation($returnToStation);
                $this->_em->flush();
            }
            return
                [
                    'id' => $driverRoute->getId(),
                    'returnToStation' => $returnToStation,
                    'driverRouteCodes' => array_map(function (DriverRouteCode $item) {
                        return $item->getCode();
                    }, $driverRoute->getDriverRouteCodes()->toArray()),
                ];
        }

        $kickOffLog = $driverRoute->getKickoffLog();
        if (!($kickOffLog instanceof KickoffLog)) {
            $kickOffLog = new KickoffLog();
            $kickOffLog->setDriverRoute($driverRoute);
            $kickOffLog->setDevice($device);
            $kickOffLog->setVanSuppliesResult([
                ["name" => "Charging Cord", 'checked' => false],
                ["name" => "Is Mobile device charge up to 100%?", 'checked' => false],
                ["name" => "Phone mount", 'checked' => false],
                ["name" => "Fuel Card", 'checked' => false],
                ["name" => "Van Off", 'checked' => false],
                ["name" => "Battery Packs", 'checked' => false],
                ["name" => "Snow Shovel", 'checked' => false],
                ["name" => "Sand Bag", 'checked' => false],
                ["name" => "Ice scraper", 'checked' => false],
                ["name" => "Spare Tire", 'checked' => false],
            ]);
            $this->_em->persist($kickOffLog);
            $this->_em->flush();
            $driverRoute->setKickoffLog($kickOffLog);
            $this->_em->flush();
        }

        return
            [
                'id' => $driverRoute->getId(),
                'kickoffLog' => $kickOffLog->getId(),
                'driverRouteCodes' => array_map(function (DriverRouteCode $item) {
                    return $item->getCode();
                }, $driverRoute->getDriverRouteCodes()->toArray()),
            ];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getLoadOut(array $criteria)
    {
        if (empty($criteria['driverRouteId'])) {
            throw new Exception('Driver Route Id not found.');
        }

        $driverRoute = $this->find($criteria['driverRouteId']);
        $driverRouteCodes = [];
        $routeCodes = $driverRoute->getDriverRouteCodes();
        foreach ($routeCodes as $routeCode) {
            if ($routeCode instanceof DriverRouteCode) {
                $driverRouteCodes[] = $routeCode->getCode();
            }
        }

        return [
            'success' => true,
            'message' => 'Getting Load Out.',
            'driverRoute' =>
                [
                    'id' => $driverRoute->getId(),
                    'kickoffLog' => $driverRoute->getKickoffLog(),
                    'driverRouteCodes' => $driverRouteCodes,
                ],
        ];
    }

    /**
     * This function assume only the first found Driver Route as the Main Driver Route
     * @param array $criteria
     * @return array|array[]
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function setDriverRoute(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('Driver Id not found.');
        }
        if (empty($criteria['date'])) {
            throw new Exception('Date not found.');
        }
        if (empty($criteria['deviceId'])) {
            throw new Exception('Device Id not found.');
        }

        $driverRoutes = $this->_em->getRepository(DriverRoute::class)
            ->getDriverRoutesSortedByStartTimeFilterByDay($criteria);

        $count = count($driverRoutes);
        if ($count < 1) {
            throw new Exception('Driver Route Not Found.');
        }

        $driverRoute = $driverRoutes[0];

        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('Driver Route is undefined.');
        }

        $device = $this->_em->getRepository(Device::class)->find($criteria['deviceId']);

        if ($device instanceof Device) {
            $driverRoute->addDevice($device);
        }

        if (!empty($criteria['mobileVersionUsed'])) {
            $driverRoute->setMobileVersionUsed($criteria['mobileVersionUsed']);
        }

        $kickOffLog = $driverRoute->getKickoffLog();
        $init = false;
        if (!($kickOffLog instanceof KickoffLog)) {
            $init = true;
            $kickOffLog = new KickoffLog();
            $kickOffLog->setStartedAt(new DateTime('now'));
            $kickOffLog->setDriverRoute($driverRoute);
            if ($device instanceof Device) {
                $kickOffLog->setDevice($device);
            }
            $kickOffLog->setVanSuppliesResult(
                [
                    ["name" => "Charging Cord", 'checked' => false],
                    ["name" => "Is Mobile device charge up to 100%?", 'checked' => false],
                    ["name" => "Phone mount", 'checked' => false],
                    ["name" => "Fuel Card", 'checked' => false],
                    ["name" => "Van Off", 'checked' => false],
                    ["name" => "Battery Packs", 'checked' => false],
                    ["name" => "Snow Shovel", 'checked' => false],
                    ["name" => "Sand Bag", 'checked' => false],
                    ["name" => "Ice scraper", 'checked' => false],
                    ["name" => "Spare Tire", 'checked' => false],
                ]);
            $this->_em->persist($kickOffLog);
            $this->_em->flush();
            $driverRoute->setKickoffLog($kickOffLog);
            $this->_em->flush();

            $user = $this->tokenStorage->getToken()->getUser();
            $title = "Load Out started.";
            $message = $user->getFriendlyName() . " started the Load Out Process.";
            $this->em->getRepository(Event::class)->createEvent('LOAD_OUT', $title, $message, null, $driverRoute, null, null);

        }

        $returnToStation = $driverRoute->getReturnToStation();
        if (!($returnToStation instanceof ReturnToStation)) {
            $returnToStation = new ReturnToStation();
            if ($device instanceof Device) {
                $returnToStation->setDevice($device);
            }
            $returnToStation->setDriverRoute($driverRoute);
            $returnToStation->setStationCheckListResult(
                [
                    ["name" => "Are you inside the station?", "checked" => false],
                    ["name" => "Windows down?", "checked" => false],
                    ["name" => "Light on?", "checked" => false],
                    ["name" => "Flasher on?", "checked" => false],
                    ["name" => "Turn off van when parked", "checked" => false],
                    ["name" => "Leave the key in the ignition", "checked" => false],
                    ["name" => "Return empty totes to the Station designated location", "checked" => false],
                    ["name" => "Check-in with Amazon RTS personnel to report route completion", "checked" => false],
                    ["name" => "Return any remaining and/or report any missing packages", "checked" => false],
                ]);
            $this->_em->persist($returnToStation);
            $this->_em->flush();
            $driverRoute->setReturnToStation($returnToStation);
            $this->_em->flush();
        }
        $driverRouteCodes = [];
        $routeCodes = $driverRoute->getDriverRouteCodes();
        foreach ($routeCodes as $routeCode) {
            if ($routeCode instanceof DriverRouteCode && $routeCode->getIsArchive() === false) {
                $driverRouteCodes[] = $routeCode->getCode();
            }
        }

        $result['response'] = [
            'success' => true,
            'message' => 'Setting Load Out & RTS.',
            'driverRoute' =>
                [
                    'id' => $driverRoute->getId(),
                    'kickoffLog' => $kickOffLog,
                    'returnToStation' => ['id' => $returnToStation->getId()],
                    'driverRouteCodes' => $driverRouteCodes,
                ],
        ];

        if (!empty($criteria['companyId'])) {
            $result['companyId'] = $criteria['companyId'];
        } else {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $company = $user->getCompany();
            $result['companyId'] = $company->getId();
        }

        if (!empty($criteria['platform'])) {
            $result['platform'] = $criteria['platform'];
        }

        $result['init'] = $init;
        $result['driverRoute'] = $driverRoute;

        return $result;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getRTS(array $criteria)
    {
        if (empty($criteria['driverRouteId'])) throw new Exception('Driver Route Id not found.');
        $driverRoute = $this->find($criteria['driverRouteId']);
        $kickoffLog = $driverRoute->getKickoffLog();
        if (!($kickoffLog instanceof KickoffLog)) throw new Exception('KickoffLog not found.');
        $returnToStation = $driverRoute->getReturnToStation();
        try {
            if ($returnToStation instanceof ReturnToStation && is_null($returnToStation->getStartedAt()) && $kickoffLog->getProcessCompleted()) {
                $returnToStation->setStartedAt(new DateTime('now'));
                $this->em->flush();
                $message = $this->userService->getCurrentUserFriendlyName() . " starts the Return Station Process.";
                $this->em->getRepository(Event::class)->createEvent('RTS', 'Return Station started.', $message, null, $driverRoute, null, null);
            }
        } catch (Exception $exception) {
            $message = $this->userService->getCurrentUserFriendlyName() . " " . $exception->getMessage();
            $event = $this->em->getRepository(Event::class)->createEvent('RTS', 'Error', $message, null, $driverRoute, null, null);
            return [
                'success' => true,
                'message' => 'Getting RTS. ' . $exception->getMessage(),
                'driverRoute' => [
                    'id' => $driverRoute->getId()
                ],
                'kickoffLog' => [
                    'processCompleted' => $kickoffLog->getProcessCompleted()
                ],
                'station' => [
                    'id' => $driverRoute->getStation()->getId(),
                    'name' => $driverRoute->getStation()->getName(),
                ],
                'returnToStationId' => $returnToStation->getId(),
                'event' => $event,
            ];
        }


        return [
            'success' => true,
            'message' => 'Getting RTS.',
            'driverRoute' => [
                'id' => $driverRoute->getId()
            ],
            'kickoffLog' => [
                'processCompleted' => $kickoffLog->getProcessCompleted()
            ],
            'returnToStation' => $returnToStation,
        ];
    }

    public function getRescuerLastTwoWeeks($station, $between, $driverId, $type)
    {
        $routeCodes = $this->em->getRepository(DriverRouteCode::class)->getRouteCodeFromDriver($driverId, $between, $type);
        if ($routeCodes) {
            $query = $this->createQueryBuilder('dr')
                ->leftJoin('dr.driverRouteCodes', 'rc')
                ->where('rc.id IS NOT NULL')
                ->addCriteria(Common::notArchived('dr'));
            if ($type == 'week') {
                $query->andWhere('dr.dateCreated >= :betweenStartDate')
                    ->setParameter('betweenStartDate', $between['start'])
                    ->andWhere('dr.dateCreated <= :betweenEndDate')
                    ->setParameter('betweenEndDate', $between['end']);
            } else {
                $query->andWhere('dr.dateCreated = :toDayDate')
                    ->setParameter('toDayDate', $between);
            }
            $query->andWhere('dr.station = :station')
                ->setParameter('station', $station)
                ->andWhere('dr.isActive = :isActive')
                ->setParameter('isActive', true)
                ->andWhere('dr.isArchive = :isDriverRouteArchive')
                ->setParameter('isDriverRouteArchive', false)
                ->andWhere('dr.isRescuer = :isRescuer')
                ->setParameter('isRescuer', true)
                ->orderBy('dr.dateCreated', 'ASC')
                ->andWhere($query->expr()->in('rc.id', ':routes'))
                ->setParameter('routes', $routeCodes);
            return $query->getQuery()->getResult();
        }
        return [];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getDateWiseRouteCodes(array $criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $results = $this->createQueryBuilder('dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driverRouteCodes', 'dcode')
            ->where('dr.dateCreated = :date')
            ->setParameter('date', $criteria['date'])
            ->andWhere('st.id = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('sf.isArchive = :isArchiveShift')
            ->setParameter('isArchiveShift', false)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dcode.code = :isArchiveRouteCode')
            ->setParameter('isArchiveRouteCode', false)
            ->getQuery()->getResult();

        $codeArr['open'] = $codeArr['driver'] = [];
        $open = [];
        $driver = [];
        foreach ($results as $route) {
            $shiftStartTime = !empty($route->getPunchIn()) ? $route->getPunchIn() : $route->getStartTime();
            $startTimeObj = $this->globalUtils->getTimeZoneConversation($shiftStartTime, $requestTimeZone);
            $startTime = $startTimeObj->format('G');

            $codeArr = [];
            $codeId = null;
            foreach ($route->getDriverRouteCodes()->filter(function ($routeCode) {
                return !$routeCode->getIsArchive();
            }) as $code) {
                if ($code->getCode() == $criteria['code'])
                    $codeId = $code->getId();
                $codeArr[] = $code->getCode();
            }

            if (empty($route->getDriver()) && $route->getIsOpenShift() === true) {
                $open[$route->getId()]['code'] = $codeArr;
                $open[$route->getId()]['id'] = $route->getId();
                $open[$route->getId()]['codeId'] = $codeId;
                $open[$route->getId()]['driverId'] = null;
                $open[$route->getId()]['date'] = $route->getDateCreated()->format('Y-m-d');
                $open[$route->getId()]['startTime'] = 'shift_' . $startTime;
                $open[$route->getId()]['codeCount'] = count($codeArr);
            } else {
                $driver[$route->getId()]['code'] = $codeArr;
                $driver[$route->getId()]['name'] = $route->getDriver()->getFullName();
                $driver[$route->getId()]['id'] = $route->getId();
                $driver[$route->getId()]['codeId'] = $codeId;
                $driver[$route->getId()]['driverId'] = $route->getDriver()->getId();
                $driver[$route->getId()]['date'] = $route->getDateCreated()->format('Y-m-d');
                $driver[$route->getId()]['startTime'] = 'shift_' . $startTime;
                $driver[$route->getId()]['codeCount'] = count($codeArr);
            }
        }
        $codeArr['open'] = array_values($open);
        $codeArr['driver'] = array_values($driver);
        return $codeArr;
    }

    public function addSendOrUnSendHomeEventData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['driverRouteId']);
        $shift = $driverRoute->getShiftType();
        if ($criteria['isSendHome']) {
            $messageData = array('shift_name' => $shift->getName(), 'user_name' => $userLogin->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Send Home', $driverRoute, 'SEND_HOME', $messageData, true);
            $message = $this->getEntityManager()->getRepository('App\Entity\Event')->getMessageByType('SEND_HOME', $messageData, 300);
        } else {
            $messageData = array('shift_name' => $shift->getName(), 'user_name' => $userLogin->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Un-Send Home', $driverRoute, 'UNSEND_HOME', $messageData, true);
            $message = $this->getEntityManager()->getRepository('App\Entity\Event')->getMessageByType('UNSEND_HOME', $messageData, 300);
        }
        $currentDate = new DateTime('now');
        $driverRoute->setNewNote($driverRoute->getNewNote() . ' => ' . $currentDate->format('Y-m-d') . ' : ' . $message);
        $this->em->persist($driverRoute);
        $this->em->flush($driverRoute);

        return $driverRoute->getNewNote();
    }

    public function addTrainOrDutyEventData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRoute = $criteria['driverRoute'];
        $shift = $driverRoute->getShiftType();
        if (isset($criteria['isTrain'])) {
            if ($criteria['isEdit']) {
                foreach ($criteria['shiftDiffrances'] as $keyName => $shiftDiffrance) {
                    if ('startTime' == $keyName && $shiftDiffrance[0]->format('H:i:s') != $shiftDiffrance[1]->format('H:i:s')) {
                        $messageData = array(
                            'old_start_time' => $shiftDiffrance[0]->format('H:i:s'),
                            'new_start_time' => $shiftDiffrance[1]->format('H:i:s'),
                            'user_name' => $userLogin->getFriendlyName(),
                        );
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Edit Train Shift StartTime', $driverRoute, 'EDIT_TRAIN_START_TIME', $messageData, true);
                    }
                    if ('endTime' == $keyName && $shiftDiffrance[0]->format('H:i:s') != $shiftDiffrance[1]->format('H:i:s')) {
                        $messageData = array(
                            'old_end_time' => $shiftDiffrance[0]->format('H:i:s'),
                            'new_end_time' => $shiftDiffrance[1]->format('H:i:s'),
                            'user_name' => $userLogin->getFriendlyName(),
                        );
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Edit Train Shift EndTime', $driverRoute, 'EDIT_TRAIN_END_TIME', $messageData, true);
                    }
                }

            } else {
                $messageData = array('user_name' => $userLogin->getFriendlyName());
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Add Train', $driverRoute, 'ADD_TRAIN', $messageData, true);
            }
        } else {
            if ($criteria['isEdit']) {
                foreach ($criteria['shiftDiffrances'] as $keyName => $shiftDiffrance) {
                    if ('startTime' == $keyName && $shiftDiffrance[0]->format('h:i:s') != $shiftDiffrance[1]->format('h:i:s')) {
                        $messageData = array(
                            'old_start_time' => $shiftDiffrance[0]->format('h:i:s'),
                            'new_start_time' => $shiftDiffrance[1]->format('h:i:s'),
                            'user_name' => $userLogin->getFriendlyName(),
                        );
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Edit Duty Shift StartTime', $driverRoute, 'EDIT_DUTY_START_TIME', $messageData, true);
                    }
                    if ('endTime' == $keyName && $shiftDiffrance[0]->format('h:i:s') != $shiftDiffrance[1]->format('h:i:s')) {
                        $messageData = array(
                            'old_end_time' => $shiftDiffrance[0]->format('h:i:s'),
                            'new_end_time' => $shiftDiffrance[1]->format('h:i:s'),
                            'user_name' => $userLogin->getFriendlyName(),
                        );
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Edit Duty Shift EndTime', $driverRoute, 'EDIT_DUTY_END_TIME', $messageData, true);
                    }
                }

            } else {
                $messageData = array('user_name' => $userLogin->getFriendlyName());
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Add Duty', $driverRoute, 'ADD_DUTY', $messageData, true);
            }
        }
    }

    public function deleteShiftEventForTimeline($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['driverRouteId']);
        $shift = $driverRoute->getShiftType();

        $messageData = array('shift_name' => $shift->getName(), 'user_name' => $userLogin->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Shift Delete', $driverRoute, 'SHIFT_DELETE', $messageData, true);
    }

    public function addEventOfPunchedInDrivers($criteria)
    {
        if (!empty($criteria['routeId'])) {
            $userLogin = $this->tokenStorage->getToken()->getUser();
            foreach ($criteria['routeId'] as $id) {
                $driverRoute = $this->find($id);
                $messageData = ['driver_name' => $driverRoute->getDriver()->getFullName(), 'shift_name' => $driverRoute->getShiftType()->getName(), 'user_name' => $userLogin->getFriendlyName()];
                if ($criteria['type'] == 'Unassign') {
                    $result = $this->getEntityManager()->getRepository('App\Entity\Event')->findBy(['name' => 'UNASSIGN_AND_POST_TO_OPEN_SHIFT', 'driverRoute' => $driverRoute]);
                    if (!$result) {
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Unassign and Post to Open', $driverRoute, 'UNASSIGN_AND_POST_TO_OPEN_SHIFT', $messageData, true);
                    }
                } else {
                    $result = $this->getEntityManager()->getRepository('App\Entity\Event')->findBy(['name' => 'UNASSIGN_AND_DELETE_SHIFT', 'driverRoute' => $driverRoute]);
                    if (!$result) {
                        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Unassign and Delete Shifts', $driverRoute, 'UNASSIGN_AND_DELETE_SHIFT', $messageData, true);
                    }
                }
            }
        }
        return true;
    }

    public function deleteDeviceFromDriverRoute($device)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $device = $this->em->getRepository('App\Entity\Device')->find($device);
        $results = $this->createQueryBuilder('dr')
            ->leftJoin('dr.device', 'd')
            ->where('d.id = :deviceId')
            ->setParameter('deviceId', $device)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()->getResult();
        foreach ($results as $result) {
            $result->removeDevice($device);
            $this->em->persist($result);
            $this->em->flush($result);
        }
        return true;
    }

    public function updateBalanceGroupNullInDriverRouteAndTempDriverRoute($balanceGroupId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.shiftBalanceGroup', 'NULL')
            ->where('dr.shiftBalanceGroup = :balanceGroup')
            ->setParameter('balanceGroup', $balanceGroupId)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.shiftBalanceGroup = :deletebalanceGroup')
            ->setParameter('deletebalanceGroup', $balanceGroupId)
            ->andWhere('tdr.isNew = 1')
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateInvoiceTypeNullInDriverRouteAndTempDriverRoute($invoiceTypeId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.shiftInvoiceType', 'NULL')
            ->where('dr.shiftInvoiceType = :invoiceType')
            ->setParameter('invoiceType', $invoiceTypeId)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.shiftInvoiceType = :deleteinvoiceType')
            ->setParameter('deleteinvoiceType', $invoiceTypeId)
            ->andWhere('tdr.isNew = 1')
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateVehicleNullInDriverRoute($vehicleId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.vehicle', 'NULL')
            ->where('dr.vehicle = :vehicleId')
            ->setParameter('vehicleId', $vehicleId)
            ->andWhere('dr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function removeDriverRoutesByEmployeeStatus($criteria)
    {
        $currentDateObj = (new \DateTime($criteria['date']))->setTimezone(new \DateTimeZone('UTC'));
        $currentDateObj->setTime(00, 00, 00);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\DriverRoute', 'dr')
            ->set('dr.isArchive', true)
            ->where('dr.driver = :driverId')
            ->setParameter('driverId', $criteria['id'])
            ->andWhere('dr.dateCreated > :dateCreated')
            ->setParameter('dateCreated', $currentDateObj)
            ->getQuery()
            ->execute();

        $qb1 = $this->getEntityManager()->createQueryBuilder();
        $qb1->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.driver = :driverId')
            ->setParameter('driverId', $criteria['id'])
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->execute();

        $sql = "DELETE FROM schedule_driver where driver_id=" . $criteria['id'];
        $this->getEntityManager()
            ->getConnection()
            ->query($sql)
            ->execute();

        return true;
    }

    public function getOldDriverRouteByRouteId($routeId)
    {
        $qb = $this->createQueryBuilder('dr')
            ->select('odr.id,dri.id as dri_id,u.friendlyName,odr.isRescuer,odr.shiftNote')
            ->innerJoin('dr.oldDriverRoute', 'odr')
            ->leftJoin('odr.driver', 'dri')
            ->leftJoin('dri.user', 'u')
            ->where('dr.id = :routeId')
            ->setParameter('routeId', $routeId);

        return $qb->getQuery()->getScalarResult();
    }

    public function movePunchTimesFromArchivedRescueShift($id)
    {
        /** @var DriverRoute $archived */
        $archived = $this->find($id);

        if ($archived->getPunchOut()) {
            $this->em->transactional(function () use ($archived) {
                $old = $archived->getOldDriverRoute();
                $old->setPunchOut($archived->getPunchOut());
                $old->setDatetimeEnded(null);
                $archived->setPunchOut(null);
                $this->em->persist($old);
                $this->em->persist($archived);
                $this->em->flush();
            });
        }
    }
}
