<?php

namespace App\Repository;

use App\Entity\CompanyPlan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CompanyPlan|null find($id, $lockMode = null, $lockVersion = null)
 * @method CompanyPlan|null findOneBy(array $criteria, array $orderBy = null)
 * @method CompanyPlan[]    findAll()
 * @method CompanyPlan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CompanyPlanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CompanyPlan::class);
    }
}
