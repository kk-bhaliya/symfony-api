<?php

namespace App\Repository;

use App\Entity\Station;
use App\Entity\CopyStation;
use App\Utils\GlobalUtility;
use App\Criteria\Common;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Worker\CopyStationWorker;

/**
 * @method Station|null find($id, $lockMode = null, $lockVersion = null)
 * @method Station|null findOneBy(array $criteria, array $orderBy = null)
 * @method Station[]    findAll()
 * @method Station[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StationRepository extends ServiceEntityRepository
{
    private $kernel;
    private $params;
    protected $tokenStorage;
    private $em;
    private $worker;

    public function __construct(
        ManagerRegistry $registry,
        KernelInterface $kernel,
        ContainerBagInterface $params,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $em,
        CopyStationWorker $worker)
    {
        $this->kernel = $kernel;
        $this->params = $params;
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->worker = $worker;
        parent::__construct($registry, Station::class);
    }

    public function findSkillRates($criteria)
    {
    }

    public function stationCopyOldData($criteria)
    {
        $worker = $this->worker->later()->copyStation($criteria['oldStationId'], $criteria['newStationId']);
        return true;
    }

    public function getAllStationsByCompany($criteria)
    {
        return $this->createQueryBuilder('s')
            ->addCriteria(Common::notArchived('s'))
            ->andWhere('s.company = :company')
            ->setParameter('company', $criteria['company'])
            ->getQuery()
            ->getResult();
    }

    public function getStationByCompany($company)
    {
        return $this->createQueryBuilder('s')
            ->addCriteria(Common::notArchived('s'))
            ->andWhere('s.company = :company')
            ->setParameter('company', $company)
            ->andWhere('s.isStationSummary = :isStationSummary')
            ->setParameter('isStationSummary', 1)
            ->getQuery()
            ->getResult();
    }

    public function form_getStations()
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $qb = $this->createQueryBuilder('st')
            ->select('st.id as id, st.name as name')
            ->where('st.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        return $qb->getQuery()->getResult();
    }

    public function getStationCodesByCompany($companyId)
    {
        $stations = $this->createQueryBuilder('st')
            ->select('st.code')
            ->where('st.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('st.code IS NOT NULL')
            ->getQuery()->getResult();

        return array_map(function ($stations) {
            return $stations['code'];
        }, $stations);
    }

    public function getFleetManagerWidgetByStation($criteria)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $roleExist = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->checkRolesExist("ROLE_FLEET_MANAGER");
        $qb = $this->createQueryBuilder('st')
            ->select('st.id,st.name')
            ->where('st.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('st.code IS NOT NULL');
        if($roleExist == false){
            $qb->leftJoin('st.drivers','d')
            ->leftJoin('d.user','u')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $user->getId());
        }
        $stations = $qb->orderBy('st.name','ASC')->getQuery()->getScalarResult();

        if(count($stations) <= 0){
            return [];
        }

        $tempStationData = [];
        foreach($stations as $station){
            $stationData = [];
            $stationData['name'] = $station['name'];
            $stationData['data'] = [];
            $stationData['data']['active'] = 0;
            $stationData['data']['inactive'] = 0;
            $stationData['data']['inMaintenance'] = 0;
            $stationData['data']['maintenanceRQ'] = 0;
            $stationData['data']['oilChangeSoon'] = 0;
            $stationData['data']['tires'] = 0;
            $stationData['data']['cargoVan'] = 0;
            $stationData['data']['boxTruck'] = 0;
            $stationData['data']['owned'] = 0;
            $stationData['data']['uHaul'] = 0;
            $stationData['data']['enterprise'] = 0;
            $stationData['data']['armada'] = 0;
            $stationData['data']['totalRented'] = 0;

            $tempStationData[$station['id']] = $stationData;
        }
        $stationIds = array_column($stations, 'id');

        $qb = $this->createQueryBuilder('st')
            ->select('st.id,v.status,v.modelType,v.type')
            ->leftJoin('st.vehicles','v')
            ->where($qb->expr()->In('st.id', $stationIds))
            ->andWhere('st.isArchive = :isArchive')
            ->andWhere('v.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('st.code IS NOT NULL');

        $vehicles = $qb->getQuery()->getScalarResult();

        foreach($vehicles as $vehicle){
            if($vehicle['status'] == 'Active'){$tempStationData[$vehicle['id']]['data']['active']=$tempStationData[$vehicle['id']]['data']['active']+1;}
            if($vehicle['status'] == 'Inactive'){$tempStationData[$vehicle['id']]['data']['inactive']=$tempStationData[$vehicle['id']]['data']['inactive']+1;}

            if($vehicle['modelType'] == 'cargo_van'){$tempStationData[$vehicle['id']]['data']['cargoVan']=$tempStationData[$vehicle['id']]['data']['cargoVan']+1;}
            if($vehicle['modelType'] == 'box_truck'){$tempStationData[$vehicle['id']]['data']['boxTruck']=$tempStationData[$vehicle['id']]['data']['boxTruck']+1;}

            if($vehicle['type'] == 'Owned'){$tempStationData[$vehicle['id']]['data']['owned']=$tempStationData[$vehicle['id']]['data']['owned']+1;}
            if($vehicle['type'] == 'Rented'){$tempStationData[$vehicle['id']]['data']['totalRented']=$tempStationData[$vehicle['id']]['data']['totalRented']+1;}
        }
        
        return array_values($tempStationData);
    }

    public function getStationByRole($criteria)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $roleExist = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->checkRolesExist("ROLE_CENTRAL_DISPATCHER");

        $qb = $this->createQueryBuilder('st')
            ->select()
            ->where('st.company = :company')
            ->setParameter('company', $user->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('st.code IS NOT NULL');
        if($roleExist == false){
            $qb->leftJoin('st.drivers','d')
            ->leftJoin('d.user','u')
            ->andWhere('u.id = :userId')
            ->setParameter('userId', $user->getId());
        }
        $stations = $qb->getQuery()->getResult();

        return $stations;
    }
}
