<?php

namespace App\Repository;

use App\Entity\ShiftTemplate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ShiftTemplate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShiftTemplate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShiftTemplate[]    findAll()
 * @method ShiftTemplate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiftTemplateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShiftTemplate::class);
    }

    public function updateShiftTemplateSetIsArchive($invoiceTypeIds)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\ShiftTemplate','st')
                        ->set('st.isArchive', true)
                        ->where($qb->expr()->in('st.invoiceWorkType', ':invoiceTypeIds'))
                        ->setParameter('invoiceTypeIds', $invoiceTypeIds )
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function updateSkillNullInShiftTemplate($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\ShiftTemplate','sk')
                        ->set('sk.skill', 'NULL')
                        ->where('sk.skill = :skillId')
                        ->setParameter('skillId', $skillId)
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function updateInvoiceTypeNullInShiftTemplate($invoiceTypeId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\ShiftTemplate','sft')
                        ->set('sft.invoiceWorkType', 'NULL')
                        ->where('sft.invoiceWorkType = :invoiceType')
                        ->setParameter('invoiceType', $invoiceTypeId)
                        ->getQuery()
                        ->execute();
        return true;
    }
}
