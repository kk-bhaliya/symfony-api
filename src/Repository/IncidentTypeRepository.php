<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\IncidentType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IncidentType|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentType|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentType[]    findAll()
 * @method IncidentType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentTypeRepository extends ServiceEntityRepository
{
    protected $tokenStorage;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, IncidentType::class);
    }

    public function getIncidentTypeByCompany($criteria)
    {
        $isInWeb = isset($criteria['isInWeb']) ? $criteria['isInWeb'] : null;
        $isInApp = isset($criteria['isInApp']) ? $criteria['isInApp'] : null;
        $userLogin = $this->tokenStorage->getToken()->getUser();
        
        
        $isAuthenticate = (in_array( $userLogin->getUserFirstRole(),array('ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER','ROLE_DISPATCHER','ROLE_LEAD_DRIVER','ROLE_OPERATIONS_ACCOUNT_MANAGER')));

        $qb = $this->createQueryBuilder('i')
            ->innerJoin('i.incidentPhases', 'ip')
            ->innerJoin('ip.role', 'r')
            ->addCriteria(Common::notArchived('i'))
            ->andWhere('(i.prefix = :callin AND  true = :isAuthenticate) or (i.prefix != :callin)')
            ->setParameter('isAuthenticate', $isAuthenticate)
            ->setParameter('callin', 'Cal')
            ->andWhere('i.company = :company')
            ->setParameter('company', $criteria['company'])
            ->orderBy('i.name', 'ASC');

        if (!empty($isInWeb)) {
            $qb->andWhere('i.isInWeb = :isInWeb')->setParameter('isInWeb', $criteria['isInWeb']);
        }
        
        if (!empty($isInApp)) {
            $qb->andWhere('i.isInApp = :isInApp')->setParameter('isInApp', $criteria['isInApp']);
        }

        return $qb->getQuery()->getResult();
    }

    public function updateSetIsArchiveByIncidentType($incidentType)
    {
        $incidentPhaseIds = $incidentStepIds = [];
        foreach ($incidentType->getIncidentPhases() as $incidentPhase) {
            $incidentPhaseIds[] = $incidentPhase->getId();
            foreach ($incidentPhase->getIncidentSteps() as $incidentStep) {
                $incidentStepIds[] = $incidentStep->getId();
            }
        }
        //Update Incident Phase
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentPhase', 'ip')
            ->set('ip.isArchive', true)
            ->where('ip.incidentType = :incidentType')
            ->setParameter('incidentType', $incidentType)
            ->getQuery()
            ->execute();

        //Update Incident Step
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentStep', 'i')
            ->set('i.isArchive', true)
            ->where($qb->expr()->in('i.phase', ':phase'))
            ->setParameter('phase', $incidentPhaseIds)
            ->getQuery()
            ->execute();

        //Update Incident Form Fields
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentFormFields', 'iff')
            ->set('iff.isArchive', true)
            ->where($qb->expr()->in('iff.step', ':step'))
            ->setParameter('step', $incidentStepIds)
            ->getQuery()
            ->execute();
        return true;
    }

    public function removeAllIncidentTypesByCompany($company)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentType', 'it')
            ->set('it.isArchive', ':makeArchive')
            ->setParameter('makeArchive', true)
            ->where('it.company = :company')
            ->setParameter('company', $company)
            ->andWhere('it.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->execute();

        return true;
    }

    public function deleteAllIncidentTypesByCompany($company)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\IncidentType', 'it')
            ->where('it.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->execute();

        return true;
    }
}
