<?php

namespace App\Repository;

use App\Entity\TimeOff;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TimeOff|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeOff|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeOff[]    findAll()
 * @method TimeOff[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeOffRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeOff::class);
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getTimeOff($criteria)
    {
        $date1 = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $date1->modify('+365 day');
        $date2 = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));

        $quarterBeforeQuarter =  $this->createQueryBuilder('to')
            ->select('to.timeOff')
            ->where('to.driver = :val')
            ->andWhere('to.timeOff > :date2 AND to.timeOff < :date1')
            ->andWhere('to.isArchive = :isArchive')
            ->setParameter('val', $criteria['id'])
            ->setParameter('date1', $date1)
            ->setParameter('date2', $date2)
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult();
        return $quarterBeforeQuarter;
    }
}
