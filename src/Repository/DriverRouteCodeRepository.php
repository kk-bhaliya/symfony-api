<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\DriverRouteCode;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method DriverRouteCode|null find($id, $lockMode = null, $lockVersion = null)
 * @method DriverRouteCode|null findOneBy(array $criteria, array $orderBy = null)
 * @method DriverRouteCode[]    findAll()
 * @method DriverRouteCode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DriverRouteCodeRepository extends ServiceEntityRepository
{
    private $em;
    protected $tokenStorage;

    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        parent::__construct($registry, DriverRouteCode::class);
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function addDriverRouteCode($criteria)
    {
        $driverRoute = $this->em->getRepository('App\Entity\DriverRoute')->find($criteria['id']);
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $oldRouteCodesArr = [];

        foreach ($driverRoute->getDriverRouteCodes()->filter(function ($routeCode) {
            return !$routeCode->getIsArchive();
        }) as $code) {
            $oldRouteCodesArr[] = $code->getCode();
            $driverRoute->removeDriverRouteCode($code);
            $this->em->persist($driverRoute);
            $this->em->flush();
        }

        foreach ($criteria['code'] as $key => $value) {
            //$driverRouteCode = $this->em->getRepository('App\Entity\DriverRouteCode')->findOneBy(['code'=>$value,'company'=>$criteria['company']]);
            /** @var DriverRouteCode $driverRouteCode */
            $driverRouteCode = $this->em->getRepository('App\Entity\DriverRouteCode')->getTodayRouteCodeForInsert([
                'code'=> $value,
                'dateCreated' => $driverRoute->getDateCreated(),
                'station' => $driverRoute->getStation()->getId()
            ]);

            if (!$driverRouteCode) {
                $driverRouteCode = new DriverRouteCode();
                $driverRouteCode->setCompany($this->em->getRepository('App\Entity\Company')->find($criteria['company']));
                $driverRouteCode->setStation($driverRoute->getStation());
                $driverRouteCode->setCode($value);
                $driverRouteCode->setIsArchive(false);
                $driverRouteCode->setDateCreated(new \DateTime($criteria['date']));
                $this->em->persist($driverRouteCode);
                $this->em->flush();
            } else {
                // remove open routes
                foreach ($driverRouteCode->getDriverRoute() as $dr) {
                    if ($dr->getDriver() !== null)
                        continue;

                    $dr->removeDriverRouteCode($driverRouteCode);
                    $dr->setIsArchive(true);
                    $this->em->persist($dr);
                }
            }

            $driverRoute->addDriverRouteCode($driverRouteCode);
            $this->em->persist($driverRoute);
            $this->em->flush();
        }
        $codeString = implode(" , ",$criteria['code']);
        $oldCodeString = implode(" , ",$oldRouteCodesArr);
        if (!empty($oldRouteCodesArr) && $codeString != $oldCodeString) {
            $this->getEntityManager()->getRepository('App\Entity\DriverRouteCode')->editRouteCodeEventDataByObject($userLogin, $codeString, $oldCodeString, $driverRoute);
        } elseif($codeString != $oldCodeString) {
            $this->getEntityManager()->getRepository('App\Entity\DriverRouteCode')->addRouteCodeEventDataByObject($userLogin, $codeString, $driverRoute);
        }
        return true;
    }

    public function addRouteCodeEventDataByObject($userLogin, $codeString, $driverRoute)
    {
        $messageData = array('route_code'=>$codeString, 'shift_name'=>$driverRoute->getShiftName(), 'user_name'=>$userLogin->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Route Code Add', $driverRoute, 'ROUTE_CODE_ADD', $messageData, true);
    }

    public function editRouteCodeEventDataByObject($userLogin, $codeString, $oldCodeString, $driverRoute)
    {
        $messageData = array('new_route_code'=>$codeString, 'old_route_code'=>$oldCodeString, 'shift_name'=>$driverRoute->getShiftName(), 'user_name'=>$userLogin->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Route Code Edit', $driverRoute, 'ROUTE_CODE_EDIT', $messageData, true);
    }

    public function getTodayRouteCodeForInsert($criteria)
    {
        $startDate = $criteria['dateCreated']->format('Y-m-d 00:00:00');
        $endDate = $criteria['dateCreated']->format('Y-m-d 23:59:59');

        return $this->createQueryBuilder('drc')
            ->leftJoin('drc.station', 'st')
            ->where('drc.dateCreated >= :startDate AND drc.dateCreated <= :endDate')
            ->setParameter('startDate', $startDate)
            ->setParameter('endDate', $endDate)
            ->andWhere('st.id = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('drc.isArchive = :drcIsArchive')
            ->setParameter('drcIsArchive', false)
            ->andWhere("drc.code = :drcCode")
            ->setParameter("drcCode", $criteria['code'])
            ->addOrderBy('drc.code', 'ASC')
            ->addGroupBy('drc.code')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getTodayRouteCode($criteria)
    {
        $qb = $this->createQueryBuilder('drc')
            ->leftJoin('drc.driverRoutes', 'dr')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $criteria['dateCreated'])
            ->andWhere('st.id = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('drc.isArchive = :drcIsArchive')
            ->setParameter('drcIsArchive', false)
        ;

        if(isset($criteria['isRescue']) && !empty($criteria['isRescue'])) {
            $qb->andWhere('dr.driver IS NOT NULL');
        }

        if(isset($criteria['routeId']) && !empty($criteria['routeId'])) {
            $qb->andWhere("dr.id != :currentRoute")
                ->setParameter("currentRoute", $criteria['routeId']);
        }

        return $qb->addOrderBy('drc.code', 'ASC')
                ->addGroupBy('drc.code')
                ->getQuery()
                ->getResult();
    }


    public function getTodayDriverRouteByRouteCode($companyId, $currentDate, $station, $startTime, $endTime, $groupId, $isDriver = true, $shiftId = null)
    {
        $qb = $this->createQueryBuilder('drc')
            ->select('count(drc.id)')
            ->leftJoin('drc.driverRoutes', 'dr')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getTodayDriverRouteByRouteCodeNew($companyId, $currentDate, $station, $startTime, $endTime, $groupId, $isDriver = true, $shiftId = null)
    {
        $qb = $this->createQueryBuilder('drc')
            ->select('count(drc.id)')
            ->leftJoin('drc.driverRoutes', 'dr')
            ->leftJoin('dr.station', 'st')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL');

        return $qb->getQuery()->getSingleScalarResult();
    }

    public function getRouteCodeFromDriver($driver, $between, $type)
    {
        $query = $this->createQueryBuilder('c')
            ->select('c.id')
            ->innerJoin('c.driverRoutes', 'dr')
            ->leftJoin('dr.driver', 'd')
            ->where('c.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->addCriteria(Common::notArchived('dr'));
        if($type == 'week'){
            $query->andWhere('dr.dateCreated >= :betweenStartDate')
            ->setParameter('betweenStartDate', $between['start'])
            ->andWhere('dr.dateCreated <= :betweenEndDate')
            ->setParameter('betweenEndDate', $between['end']);
        } else {
            $query->andWhere('dr.dateCreated = :toDayDate')
            ->setParameter('toDayDate', $between);;
        }
        $query->andWhere('dr.driver = :driver')
            ->setParameter('driver', $driver)
            ->andWhere('dr.isRescued = :isRescued')
            ->setParameter('isRescued', true)
            ->orderBy('dr.dateCreated', 'ASC');
        $results = $query->getQuery()->getResult();

        return array_column($results,'id');
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function removetRouteCode($criteria)
    {
        $driverRoute = $this->em->getRepository('App\Entity\DriverRoute')->find($criteria['route']);

        if(empty($driverRoute->getDriver())  && count($driverRoute->getDriverRouteCodes()) == 1){
            $driverRoute->setIsArchive(1);
            $this->em->persist($driverRoute);
            $this->em->flush($driverRoute);
        }
        $code = $this->find($criteria['code']);
        if($code ){
            $driverRoute->removeDriverRouteCode($code);
            $this->em->persist($driverRoute);
            $this->em->flush();
            return true;
        }
    }

    public function getRouteCodeByRouteId($routeId)
    {
        $qb = $this->createQueryBuilder('drc')
            ->select('drc.code')
            ->innerJoin('drc.driverRoutes', 'dr')
            ->where('dr.id = :routeId')
            ->setParameter('routeId', $routeId)
            ->andWhere('drc.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        return $qb->getQuery()->getScalarResult();
    }
}
