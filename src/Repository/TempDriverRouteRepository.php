<?php

namespace App\Repository;

use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\DriverSkill;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use App\Entity\Event;
use App\Utils\EventEmitter;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mercure\Update;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Utils\GlobalUtility;
use Doctrine\ORM\Query;

/**
 * @method TempDriverRoute|null find($id, $lockMode = null, $lockVersion = null)
 * @method TempDriverRoute|null findOneBy(array $criteria, array $orderBy = null)
 * @method TempDriverRoute[]    findAll()
 * @method TempDriverRoute[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TempDriverRouteRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    protected $em;
    protected $eventEmitter;
    private $globalUtils;
    private $params;
    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        EventEmitter $eventEmitter,
        GlobalUtility $globalUtils,
        ContainerBagInterface $params
    )
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->params = $params;
        parent::__construct($registry, TempDriverRoute::class);

        $this->eventEmitter = $eventEmitter;
        $this->globalUtils = $globalUtils;
    }

    public function publishChangesForSchedulerLoadOut($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        foreach ($criteria['tempDriverRouteId'] as $tempDriverRouteId) {
            //Get temp driver route
            $tempDriverRoute = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->find($tempDriverRouteId);
            //Update temp driver route
            $driverRoute = $tempDriverRoute->getRouteId();
            $driverRoute->setDateCreated($tempDriverRoute->getDateCreated());
            $driverRoute->setDriver(!empty($tempDriverRoute->getDriver()) ? $tempDriverRoute->getDriver() : null);
            $driverRoute->setOldDriver(!empty($tempDriverRoute->getOldDriver()) ? $tempDriverRoute->getOldDriver() : null);
            $driverRoute->setOldDriverRoute(!empty($tempDriverRoute->getOldDriverRoute()) ? $tempDriverRoute->getOldDriverRoute() : null);
            $driverRoute->setIsBackup(!empty($tempDriverRoute->getIsBackup()) ? $tempDriverRoute->getIsBackup() : 0);
            $driverRoute->setIsTemp(0);
            $driverRoute->setIsOpenShift(empty($tempDriverRoute->getDriver()) ? 1 : 0);

            $driverRoute->setSkill($tempDriverRoute->getSkill());
            $driverRoute->setShiftType($tempDriverRoute->getShiftType());
            $driverRoute->setShiftName($tempDriverRoute->getShiftName());
            $driverRoute->setShiftColor($tempDriverRoute->getShiftColor());
            $driverRoute->setUnpaidBreak($tempDriverRoute->getUnpaidBreak());
            $driverRoute->setShiftNote($tempDriverRoute->getShiftNote());
            $driverRoute->setShiftInvoiceType($tempDriverRoute->getShiftInvoiceType());
            $driverRoute->setShiftStation($tempDriverRoute->getShiftStation());
            $driverRoute->setShiftSkill($tempDriverRoute->getShiftSkill());
            $driverRoute->setShiftCategory($tempDriverRoute->getShiftCategory());
            $driverRoute->setShiftBalanceGroup($tempDriverRoute->getShiftBalanceGroup());
            $driverRoute->setShiftTextColor($tempDriverRoute->getShiftTextColor());
            $driverRoute->setStartTime($tempDriverRoute->getStartTime());
            $driverRoute->setEndTime($tempDriverRoute->getEndTime());
            $driverRoute->setPunchIn($tempDriverRoute->getPunchIn());
            $driverRoute->setPunchOut($tempDriverRoute->getPunchOut());
            $driverRoute->setIsArchive(($tempDriverRoute->getAction() == 'SHIFT_DELETED') ? true : false);
            $this->getEntityManager()->persist($driverRoute);
            $this->getEntityManager()->flush();

            //Update created by old route id with isNew = 0
            $tempDriverRoute = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTempDriverRouteByRouteId($tempDriverRoute->getRouteId());
            $tempDriverRoute = $tempDriverRoute[0];
            $tempDriverRoute->setCreatedBy($userLogin);
            $this->getEntityManager()->persist($tempDriverRoute);
            $this->getEntityManager()->flush();

            //Delete all same route updated records with isNew = 1
            $qb = $this->getEntityManager()->createQueryBuilder();
            $query = $qb->delete('App\Entity\TempDriverRoute', 'tdr')
                ->where('tdr.routeId = :routeId')
                ->setParameter('routeId', $tempDriverRoute->getRouteId())
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery();
            $query->execute();
        }
    }

    public function deleteTodayLoadOutTempDriverRoute()
    {
        $currentDate = date('Y-m-d');
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.isLoadOut = :isLoadOut')
            ->setParameter('isLoadOut', 0)
            ->andWhere('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateDriverIsArchiveInTempDriverRoute($driverId)
    {
        $currentDateObj =  (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true)
            ->where('tdr.driver = :driverId')
            ->setParameter('driverId', $driverId)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.dateCreated > :dateCreated')
            ->setParameter('dateCreated', $currentDateObj)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateTempDriverRouteSetIsArchive($driverRouteIds)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true)
            ->where($qb->expr()->in('tdr.routeId', ':routeId'))
            ->setParameter('routeId', $driverRouteIds)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateTempDriverRouteSetIsArchiveByShift($shifts)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true)
            ->where($qb->expr()->in('tdr.shiftType', ':shifts'))
            ->setParameter('shifts', $shifts)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateScheduleIsArchiveInTempDriverRoute($scheduleId)
    {
        $today = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true);
        if (is_array($scheduleId)) {
            $qb->where($qb->expr()->in('tdr.schedule', ':scheduleId'))
                ->setParameter('scheduleId', $scheduleId);
        } else {
            $qb->where('tdr.schedule = :schedule')
                ->setParameter('schedule', $scheduleId);
        }

        $qb->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateSkillNullInTempDriverRoute($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'sk')
            ->set('sk.skill', 'NULL')
            ->where('sk.skill = :skillId')
            ->setParameter('skillId', $skillId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function getTodayLoadOutTempDriverRouteDriverNotNullData($companyId, $currentDate, $station)
    {
        $qb = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->leftJoin('tdr.driver', 'dri')
            ->where('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('tdr.driver IS NOT NULL')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('tdr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('sf.isArchive = :shiftIsArchive')
            ->setParameter('shiftIsArchive', false)
            // ->andWhere('tdr.createdBy = :loginUser')
            // ->setParameter('loginUser', $userLogin->getId())
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('dri.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('tdr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('tdr.station= :station')
                ->setParameter('station', $station);
        }
        return $qb->getQuery()->getResult();
    }

    public function getTodayLoadOutTempDriverRouteDriverNotNullDataNew($companyId, $currentDate, $station)
    {
        $sqlQuery = "SELECT
        tdr.id as id,
        dr.id as routeId,
        st.timezone as station_timezone,
        tdr.station_id as stationId,
        tdr.start_time as startTime,
        tdr.end_time as endTime,
        dr.punch_in as punchIn,
        dr.punch_out as punchOut,
        dr.break_punch_in as breakPunchIn,
        dr.break_punch_out as breakPunchOut,
        dr.is_rescuer as is_rescuer,
        tdr.shift_invoice_type_id as shift_invoice_type_id,
        tdr.shift_station_id as shift_station_id,
        tdr.shift_skill_id as shift_skill_id,
        tdr.shift_balance_group_id as shift_balance_group_id,
        tdr.shift_name as dr_shift_name,
        tdr.shift_color as dr_shift_color,
        tdr.unpaid_break as unpaid_break,
        tdr.shift_note as shift_note,
        tdr.shift_category as dr_shift_category,
        tdr.shift_type_id as shift_type_id,
        dri.id as driverId,
        u.id as user_id,
        u.friendly_name as user_friendlyName,
        u.first_name as user_firstName,
        u.last_name as user_lastName,
        u.profile_image as user_profileImage,
        u.email as user_email,
        u.friendly_name as friendly_name,
        dr.schedule_id as scheduleId,
        sf.id as shift_id,
        sf.category as shift_category,
        sf.name as shift_name,
        sf.color as shift_color,
        sf.text_color as shift_textColor,
        sf.note as note,
        sf.start_time as shiftStartTime,
        sf.end_time as shiftEndTime,
        sf.balance_group_id as shift_balance_group,
        sf.invoice_type_id as invoice_type_id,
        odr.driver_id as oldDriverId,
        odr.id as oldDriverRoute,
        odr.is_backup as isBackup,
        odr.is_second_shift as isSecondShift,
        odrShift.color as odrShiftColor,
        odrUser.first_name as old_route_user_firstName,
        odrUser.last_name as old_route_user_lastName,
        odrUser.friendly_name as old_user_friendly_name,
        tdr.is_new as isNew,
        sf.balance_group_id as balanceGroupId,
        dr.datetime_started as datetimeStarted,
        dr.datetime_ended as datetimeEnded,
        dr.is_add_train as isAddTrain,
        dr.is_light_duty as isLightDuty,
        dr.date_created as dateCreated,
        dr.has_bag as hasBag,
        dri.employee_status as employeeStatus,
        kick.station_arrival_at as stationArrivalAt,
        kick.id as kickoffLog,
        tdr.is_new as isNew,
        v.vehicle_id as VehicleUnit,
        v.id as VehicleId,
        (SELECT GROUP_CONCAT(skill.id) FROM skill LEFT JOIN driver_skill ON driver_skill.skill_id = skill.id WHERE skill.is_archive = 0  AND driver_skill.driver_id = dri.id) as skillId,
        (SELECT GROUP_CONCAT(driver_route_code.code) FROM driver_route_code LEFT JOIN driver_route_driver_route_code ON driver_route_driver_route_code.driver_route_code_id = driver_route_code.id WHERE driver_route_code.is_archive = 0  AND driver_route_driver_route_code. 	driver_route_id = dr.id) as routeCode,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
        FROM
            temp_driver_route tdr
        LEFT JOIN
            driver_route dr
            ON
            dr.id = tdr.route_id_id
        LEFT JOIN
            shift sf
            ON
            sf.id = tdr.shift_type_id
        LEFT JOIN
            driver dri
            ON
            dri.id = tdr.driver_id

        LEFT JOIN
            user u
            ON
            u.id = dri.user_id
        LEFT JOIN
            station st
            ON
            st.id = tdr.station_id

        LEFT JOIN
            kickoff_log kick
            ON
            kick.id = dr.kickoff_log_id
        LEFT JOIN
            driver_route odr
            ON
            odr.id = tdr.old_driver_route_id
        LEFT JOIN
            shift odrShift
            ON
            odrShift.id = odr.shift_type_id

        LEFT JOIN
            driver odrDriver
            ON
            odrDriver.id = odr.driver_id
        LEFT JOIN
            user odrUser
            ON
            odrUser.id = odrDriver.user_id
        LEFT JOIN
            vehicle v
            ON
            v.id = dr.vehicle_id
        WHERE
        tdr.date_created = '".$currentDate."'
        AND tdr.driver_id IS NOT NULL
        AND sf.company_id = ".$companyId."
        AND tdr.is_archive = 0
        AND sf.is_archive = 0
        AND tdr.is_new = 1
        AND dri.is_enabled = 1
        AND tdr.start_time IS NOT NULL
        AND tdr.station_id = ".$station." GROUP BY dr.id order by friendly_name ASC ";


        $driverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $driverRouteData;

    }

    public function getTodayLoadOutTempDriverRouteDriverNullData($userLogin, $currentDate, $station)
    {
        $qb = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->where('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('tdr.driver IS NULL')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('tdr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('sf.isArchive = :shiftIsArchive')
            ->setParameter('shiftIsArchive', false)
            // ->andWhere('tdr.createdBy = :loginUser')
            // ->setParameter('loginUser', $userLogin->getId())
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('tdr.station= :station')
                ->setParameter('station', $station);
        }

        return $qb->getQuery()->getResult();
    }

    public function getTodayLoadOutTempDriverRouteDriverNullDataNew($companyId, $currentDate, $station)
    {
        $qb = $this->createQueryBuilder('tdr')
            ->select('dr.id')
            ->leftJoin('tdr.routeId', 'dr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->where('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('tdr.driver IS NULL')
            ->andWhere('sf.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('tdr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('sf.isArchive = :shiftIsArchive')
            ->setParameter('shiftIsArchive', false)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.startTime IS NOT NULL');

        if (!empty($station)) {
            $qb->andWhere('tdr.station= :station')
                ->setParameter('station', $station);
        }

        return $qb->getQuery()->getArrayResult();
    }

    public function getTempDriverRouteData($weekStartEndDay, $userLogin)
    {
        $results = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->leftJoin('tdr.driver', 'dri')
            ->where('tdr.dateCreated >= :sunday')
            ->andWhere('tdr.dateCreated <= :saturday')
            ->setParameter('sunday', $weekStartEndDay['week_start'])
            ->setParameter('saturday', $weekStartEndDay['week_end'])
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.driver IS NOT NULL')
            ->andWhere('tdr.isNew = :isNew')
            ->andWhere('dri.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->setParameter('isNew', 1)
            ->andWhere('tdr.isArchive = 0')
            ->getQuery()
            ->getResult();

        return $results;
    }

    public function getTempDriverRouteDataNew($weekStartEndDay, $userLogin)
    {
        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                            tdr.id as id,tdr.date_created  as dateCreated,tdr.is_new as isNew,tdr.action as action,
                            tdr.start_time as startTime,tdr.end_time as endTime,tdr.unpaid_break as unpaidBreak,
                            tdr.created_date as createdDate,tdr.shift_color as shiftColor,tdr.shift_text_color as shiftTextColor,tdr.shift_name as shiftName,tdr.is_open_shift as isOpenShift,tdr.number_of_package as numberOfPackage,
                            tdr.is_temp as isTemp,tdr.is_rescued as isRescued,tdr.is_rescuer as isRescuer,tdr.shift_note as shiftNote,tdr.new_note as newNote,tdr.current_note as currentNote,
                            dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,
                            dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.mobile_version_used as mobileVersionUsed,
                            dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                            dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                            odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,
                            odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,odr.is_archive as oldDriverRoute_isArchive,
                            sh.id as schedule_id,sh.name as schedule_name,
                            dri.id as driver_id,dri.driving_preference as driver_drivingPreference,dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                            rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                            rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                            es.id as es_id,es.name as es_name,
                            u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                            u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                            dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                            s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                            sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                            (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                            (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        temp_driver_route tdr
                    LEFT JOIN
                        driver_route dr
                    ON
                        tdr.route_id_id = dr.id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = tdr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = tdr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = tdr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = tdr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = tdr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = tdr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = tdr.shift_invoice_type_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE tdr.date_created >= '".$weekStartEndDay['week_start']."' AND tdr.date_created <= '".$weekStartEndDay['week_end']."'
                            AND sf.company_id = ".$userLogin->getCompany()->getId()." AND tdr.is_new = 1 AND tdr.is_archive = 0 AND
                            tdr.driver_id IS NOT NULL AND dri.is_enabled = 1 AND st.is_archive = 0";

        $tempDriverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $tempDriverRouteData;
    }

    public function getTempDriverRouteDataForDayView($currentPassedDate, $userLogin, $driverId = null)
    {
        $qb = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->leftJoin('tdr.driver', 'dri')
            ->leftJoin('dri.user', 'u')
            ->where('tdr.dateCreated = :currentPassedDate')
            ->setParameter('currentPassedDate', $currentPassedDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.driver IS NOT NULL')
            ->andWhere('dri.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->orderBy('u.firstName', 'ASC');

        if (!empty($driverId)) {
            $qb->andWhere('tdr.driver = :driver')->setParameter('driver', $driverId);
        }

        return $qb->getQuery()->getResult();
    }

    public function getTempDriverRouteDataForDayViewNew($currentPassedDate, $userLogin, $driverId = null)
    {
        $driverCriteria = '';
        if (!empty($driverId)) {
            $driverCriteria = " AND tdr.driver_id = ".$driverId;
        }
        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                            tdr.id as id,tdr.date_created  as dateCreated,tdr.is_new as isNew,tdr.action as action,
                            tdr.start_time as startTime,tdr.end_time as endTime,tdr.unpaid_break as unpaidBreak,
                            tdr.created_date as createdDate,tdr.shift_color as shiftColor,tdr.shift_text_color as shiftTextColor,tdr.shift_name as shiftName,tdr.is_open_shift as isOpenShift,tdr.number_of_package as numberOfPackage,
                            tdr.is_temp as isTemp,tdr.is_rescued as isRescued,tdr.is_rescuer as isRescuer,tdr.shift_note as shiftNote,tdr.new_note as newNote,tdr.current_note as currentNote,
                            dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,
                            dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.mobile_version_used as mobileVersionUsed,
                            dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                            dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                            odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,
                            odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,odr.is_archive as oldDriverRoute_isArchive,
                            sh.id as schedule_id,sh.name as schedule_name,
                            dri.id as driver_id,dri.driving_preference as driver_drivingPreference,dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                            rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                            rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                            es.id as es_id,es.name as es_name,
                            u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                            u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                            dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                            s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                            sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                            (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                            (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        temp_driver_route tdr
                    LEFT JOIN
                        driver_route dr
                    ON
                        tdr.route_id_id = dr.id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = tdr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = tdr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = tdr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = tdr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = tdr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = tdr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = tdr.shift_invoice_type_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE tdr.date_created = '".$currentPassedDate."' AND sf.company_id = ".$userLogin->getCompany()->getId()." AND tdr.is_new = 1 AND st.is_archive = 0 AND tdr.driver_id IS NOT NULL AND dri.is_enabled = 1 AND tdr.is_new = 1".$driverCriteria." ORDER BY u.first_name ASC";

        $tempDriverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $tempDriverRouteData;
    }

    public function updateDriverRouteIsArchiveInTempDriverRoute($driverRouteId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23,59,59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true)
            ->where('tdr.routeId = :driverRouteId')
            ->setParameter('driverRouteId', $driverRouteId)
            ->andWhere('tdr.isNew = 0')
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today',$today)
            ->getQuery()
            ->execute();

        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.routeId = :driverRouteId')
            ->setParameter('driverRouteId', $driverRouteId)
            ->andWhere('tdr.isNew = 1')
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today',$today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateShiftIsArchiveInTempDriverRoute($shiftId)
    {
        $today = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $today->setTime(23, 59, 59);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.isArchive', true)
            ->where('tdr.shiftType = :shiftId')
            ->setParameter('shiftId', $shiftId)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today', $today)
            ->getQuery()
            ->execute();
        return true;
    }

    public function getAllTempDriverRemove()
    {
        $currentDate = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $this->createQueryBuilder('tdr')
            ->delete()
            ->where('tdr.dateCreated < :createdDate')
            ->andWhere('tdr.isNew = 1 or (tdr.createdBy is NULL AND tdr.isNew = 0)')
            ->setParameter('createdDate', $currentDate->format('Y-m-d'))
            ->getQuery()->execute();

        return true;
    }

    public function getScheduleOpenShiftTempDriverRoutesWithParamNew($weekStartEndDay, $userLogin, $driverId = null)
    {
        $driverCriteria = '';
        if (!empty($driverId)) {
            $driverCriteria = " AND tdr.driver_id = ".$driverId;
        }
        $sqlQuery = "SELECT st.id as station_id,st.name as station_name,st.timezone as station_timezone,
                        tdr.id as id,tdr.date_created  as dateCreated,tdr.is_new as isNew,tdr.action as action,
                        tdr.start_time as startTime,tdr.end_time as endTime,tdr.unpaid_break as unpaidBreak,
                        tdr.created_date as createdDate,tdr.shift_color as shiftColor,tdr.shift_text_color as shiftTextColor,tdr.shift_name as shiftName,tdr.is_open_shift as isOpenShift,tdr.number_of_package as numberOfPackage,
                        tdr.is_temp as isTemp,tdr.is_rescued as isRescued,tdr.is_rescuer as isRescuer,tdr.shift_note as shiftNote,tdr.new_note as newNote,tdr.current_note as currentNote,
                        dr.id as routeId, dr.punch_in as punchIn, dr.punch_out as punchOut,dr.break_punch_in as breakPunchIn,dr.break_punch_out as breakPunchOut,
                        dr.datetime_ended as datetimeEnded,dr.count_of_total_stops as countOfTotalStops,dr.completed_count_of_stops as completedCountOfStops,dr.count_of_total_packages as countOfTotalPackages,dr.mobile_version_used as mobileVersionUsed,
                        dr.count_of_attempted_packages as countOfAttemptedPackages,dr.count_of_returning_packages  as countOfReturningPackages,dr.count_of_delivered_packages  as countOfDeliveredPackages,
                        dr.count_of_missing_packages as countOfMissingPackages,dr.is_light_duty as isLightDuty,dr.is_add_train as isAddTrain,dr.is_second_shift as isSecondShift,
                        odr.id as oldDriverRoute_id,odr.end_time as oldDriverRoute_endTime,odr.is_backup as oldDriverRoute_isBackup,
                        odr.is_rescuer  as oldDriverRoute_isRescuer,odr.is_add_train as oldDriverRoute_isAddTrain,odr.is_light_duty as oldDriverRoute_isLightDuty,odr.is_second_shift as oldDriverRoute_isSecondShift,odr.is_archive as oldDriverRoute_isArchive,
                        sh.id as schedule_id,sh.name as schedule_name,
                        dri.id as driver_id,dri.driving_preference as driver_drivingPreference,
                        dri.employee_status as driver_employeeStatus,dri.offboarded_date as driver_offboardedDate,dri.termination_date as driver_terminationDate,dri.inactive_at as driver_inactiveAt,
                        rfr.id as requestForRoute_id,rfr.is_swap_request as requestForRoute_isSwapRequest,rfr.reason_for_request as requestForRoute_reasonForRequest,rfr.is_request_completed as requestForRoute_isRequestCompleted,
                        rfr_dri.id as requestForRoute_driver_id,rfr_user.id as requestForRoute_user_id,
                        es.id as es_id,es.name as es_name,
                        u.id as user_id,u.friendly_name as user_friendlyName,u.first_name as user_firstName,
                        u.last_name as user_lastName,u.profile_image as user_profileImage,u.email as user_email,u1.id as updatedBy_id,u1.friendly_name as updatedBy_friendly_name,
                        dv.id as vehicle_id,dv.vin as vehicle_vin,dv.vehicle_id as vehicle_vehicleId,
                        s.id as skill_id,shi.id as shiftInvoiceType_id,shi.name as shiftInvoiceType_name,
                        sf.id as shift_id,sf.unpaid_break as shift_unpaidBreak,sf.category as shift_category,sf.name as shift_name,sf.color as shift_color,sf.text_color as shift_textColor,sf.start_time as shift_startTime,sf.end_time as shift_endTime,sf.note as note,
                        (SELECT dd.imei from device dd INNER JOIN driver_route_device drd ON dd.id = drd.device_id where drd.driver_route_id = dr.id order by dd.id desc limit 1) as device_imei_latest,
                        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
                    FROM
                        temp_driver_route tdr
                    LEFT JOIN
                        driver_route dr
                    ON
                        tdr.route_id_id = dr.id
                    LEFT JOIN
                        shift sf
                    ON
                        sf.id = tdr.shift_type_id
                    LEFT JOIN
                        station st
                    ON
                        st.id = tdr.station_id
                    LEFT JOIN
                        skill s
                    ON
                        s.id = tdr.skill_id
                    LEFT JOIN
                        route_requests rfr
                    ON
                        rfr.id = dr.request_for_route_id
                    LEFT JOIN
                        driver rfr_dri
                    ON
                        rfr_dri.id = rfr.new_driver_id
                    LEFT JOIN
                        user rfr_user
                    ON
                        rfr_user.id = rfr.approved_by_id
                    LEFT JOIN
                        driver_route odr
                    ON
                        odr.id = tdr.old_driver_route_id
                    LEFT JOIN
                        vehicle dv
                    ON
                        dv.id = dr.vehicle_id
                    LEFT JOIN
                        driver dri
                    ON
                        dri.id = tdr.driver_id
                    LEFT JOIN
                        employment_status es
                    ON
                        es.id = dri.employment_status_id
                    LEFT JOIN
                        user u
                    ON
                        u.id = dri.user_id
                    LEFT JOIN
                        schedule sh
                    ON
                        sh.id = tdr.schedule_id
                    LEFT JOIN
                        invoice_type shi
                    ON
                        shi.id = tdr.shift_invoice_type_id
                    LEFT JOIN
                        user u1
                    ON
                        u1.id = dr.updated_by_id
                    WHERE tdr.date_created >= '".$weekStartEndDay['week_start']."' AND tdr.date_created <= '".$weekStartEndDay['week_end']."' AND sf.company_id = ".$userLogin->getCompany()->getId()." AND tdr.driver_id IS NULL AND tdr.is_new = 1 AND tdr.is_archive = 0".$driverCriteria;

        $tempDriverRouteData = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        return $tempDriverRouteData;
    }

    public function getScheduleOpenShiftTempDriverRoutesWithParam($weekStartEndDay, $userLogin, $driverId = null)
    {
        $qb = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->where('tdr.dateCreated >= :sunday')
            ->andWhere('tdr.dateCreated <= :saturday')
            ->setParameter('sunday', $weekStartEndDay['week_start'])
            ->setParameter('saturday', $weekStartEndDay['week_end'])
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('tdr.driver IS NULL')
            ->andWhere('tdr.isNew = 1')
            ->andWhere('tdr.isArchive = 0');

        if (!empty($driverId)) {
            $qb->andWhere('tdr.driver = :driver')->setParameter('driver', $driverId);
        }

        return $qb->getQuery()->getResult();
    }

    public function getScheduleOpenShiftTempDriverRoutesWithParamForDayView($currentPassedDate, $userLogin)
    {

        $results = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->where('tdr.dateCreated = :currentPassedDate')
            ->setParameter('currentPassedDate', $currentPassedDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.driver IS NULL')
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->getResult();

        return $results;
    }

    public function removeDriverNullInTempDriverRoute($scheduleId, $scheduleDriver)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.isNew = 1')
            ->andWhere('tdr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId);
        if (!empty($scheduleDriver)) {
            $qb->andWhere($qb->expr()->notIn('tdr.driver', ':driverArray'))
                ->setParameter('driverArray', $scheduleDriver);
        }
        $qb->getQuery()
            ->execute();

        return true;
    }

    public function removeDropAllChangesTempDriverRouteData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        foreach ($criteria['routeId'] as $driverRouteId) {
            $results = $this->createQueryBuilder('tdr')
                ->where('tdr.routeId = :routeId')
                ->setParameter('routeId', $driverRouteId)
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()->getResult();

            if (count($results) == 1) {
                $this->createQueryBuilder('tdr')
                    ->delete()
                    ->where('tdr.routeId = :routeId')
                    ->setParameter('routeId', $driverRouteId)
                    ->andWhere('tdr.isNew = :isNew')
                    ->setParameter('isNew', 0)
                    ->andWhere('tdr.createdBy IS NULL')
                    ->getQuery()
                    ->execute();
            }
            //User all temp changes remove
            $this->createQueryBuilder('tdr')
                ->delete()
                ->where('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->andWhere('tdr.routeId = :routeIds')
                ->setParameter('routeIds', $driverRouteId)
                // ->andWhere('tdr.createdBy = :userId')
                // ->setParameter('userId', $userLogin->getId())
                ->getQuery()
                ->execute();
        }
        return true;
    }

    public function removeDropUnpublishChanges($criteria)
    {
        if(!is_array($criteria['tempDriverRouteId'])){
            $criteria['tempDriverRouteId']=[$criteria['tempDriverRouteId']];
        }
        $tempDriverRouteIdArray=[];

        foreach ($criteria['tempDriverRouteId'] as $routeId) {
            $valueArray = explode('-',$routeId);
            if(count($valueArray) > 1){
                $tempDriverRouteIdArray[]=$valueArray[0];
                $tempDriverRouteIdArray[]=$valueArray[1];
            } else{
                $tempDriverRouteIdArray[]=$valueArray[0];
            }
        }

        foreach ($tempDriverRouteIdArray as $tempDriverRouteId) {
            $tempDriver = $this->find($tempDriverRouteId);
            if($tempDriver){
                $driverRoute = $tempDriver->getRouteId();
                if( $driverRoute->getRequestForRoute() ){
                    $request = $driverRoute->getRequestForRoute();
                    if($driverRoute->getRequestForRoute()->getIsSwapRequest() === false && $driverRoute->getRequestForRoute()->getNewDriver()){
                        $request->setNewDriver(null);
                        $this->submitRemoveDropUnpublishChanges($tempDriver);
                    } else {
                        $driverRoute->setRequestForRoute(null);
                        $request->setIsArchive(true);
                        $this->submitRemoveDropUnpublishChanges($tempDriver);
                    }
                    $this->getEntityManager()->persist($driverRoute);
                    $this->getEntityManager()->persist($request);
                    $this->getEntityManager()->flush();
                } else {
                    $this->submitRemoveDropUnpublishChanges($tempDriver);
                }
            }
        }
        return true;
    }

    public function submitRemoveDropUnpublishChanges($tempDriver)
    {
        if ($tempDriver->getIsTemp() && empty($tempDriver->getRouteCode())) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->delete('App\Entity\DriverRoute', 'dr')
                ->where('dr.id = :dr_id')
                ->setParameter('dr_id', $tempDriver->getRouteId()->getId())
                ->getQuery()
                ->execute();
        } else {
            $results = $this->createQueryBuilder('tdr')
                ->where('tdr.routeId = :routeId')
                ->setParameter('routeId', $tempDriver->getRouteId()->getId())
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()->getResult();


            if ($results[0]->getIsBackup() == true) {
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.oldDriverRoute', 'NULL')
                    ->set('dr.isActive', true)
                    ->where('dr.id = :driverRouteId')
                    ->setParameter('driverRouteId', $tempDriver->getOldDriverRoute())
                    ->getQuery()
                    ->execute();
            }

            if (count($results) == 1) {
                $this->createQueryBuilder('tdr')
                    ->delete()
                    ->where('tdr.routeId = :routeId')
                    ->setParameter('routeId', $tempDriver->getRouteId()->getId())
                    ->andWhere('tdr.isNew = :isNew')
                    ->setParameter('isNew', 0)
                    ->andWhere('tdr.createdBy IS NULL')
                    ->getQuery()
                    ->execute();
            }
            $this->getEntityManager()->remove($tempDriver);
            $this->getEntityManager()->flush();
        }
    }

    public function getTempDriverRouteByRouteId($routeId, $createdBy = true)
    {
        $query = $this->createQueryBuilder('tdr')
            ->where('tdr.isNew = :isNew')
            ->setParameter('isNew', 0)
            ->andWhere('tdr.routeId = :routeId')
            ->setParameter('routeId', $routeId);

        if ($createdBy) {
            $query->andWhere('tdr.createdBy IS NULL');
        }

        $results = $query->getQuery()->getResult();

        return $results;
    }

    public function getRouteRequest($requestId,$request = null)
    {
        if($request == 'SWAP_REQUEST'){
            $queryParam = " AND newTempdr.is_new = 1 AND oldTempdr.is_new = 1 LIMIT 0,1";
        } else {
            $queryParam = " LIMIT 0,1";
        }


        $sqlQuery = "SELECT
        req.id,
        req.new_driver_id as new_driver_id,
        req.approved_by_id as approved_by_id,
        req.old_route_id as old_route_id,
        req.new_route_id as new_route_id,
        req.old_driver_id as old_driver_id,
        req.reason_for_request as reason_for_request,
        req.is_swap_request as is_swap_request,
        req.is_request_completed as is_request_completed,
        newUser.friendly_name as new_user_friendlyName,
        oldUser.friendly_name as old_user_friendlyName,
        newUser.profile_image as new_user_profile_image,
        oldUser.profile_image as old_user_profile_image,
        newdr.date_created as new_roue_date_created,
        olddr.date_created as old_roue_date_created,
        newDrShift.name as new_shift_name,
        oldDrShift.name as old_shift_name,
        oldDrShift.name as old_shift_name,
        newTempdr.id as new_temp_route_id,
        oldTempdr.id as old_temp_route_id,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = newUser.id limit 1) as new_user_role_name ,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = oldUser.id limit 1) as old_user_role_name
        FROM
            route_requests req
        LEFT JOIN
            driver_route newdr
            ON
            newdr.id = req.new_route_id
        LEFT JOIN
            temp_driver_route newTempdr
            ON
            newTempdr.route_id_id = newdr.id
        LEFT JOIN
            driver_route olddr
            ON
            olddr.id = req.old_route_id
        LEFT JOIN
            temp_driver_route oldTempdr
            ON
            oldTempdr.route_id_id = olddr.id
        LEFT JOIN
            shift oldDrShift
            ON
            oldDrShift.id = olddr.shift_type_id
        LEFT JOIN
            shift newDrShift
            ON
            newDrShift.id = newdr.shift_type_id
        LEFT JOIN
            driver newDri
            ON
            newDri.id = req.new_driver_id
        LEFT JOIN
            user newUser
            ON
            newUser.id = newDri.user_id
        LEFT JOIN
            driver oldDri
            ON
            oldDri.id = req.old_driver_id
        LEFT JOIN
            user oldUser
            ON
            oldUser.id = oldDri.user_id
        WHERE
        req.id = ".$requestId.$queryParam;

        $results = $this->getEntityManager()
         ->getConnection()
         ->query($sqlQuery)
         ->fetchAll();
         return $results;
    }
    public function getPublishTempDriverRouteMessage($oldTempRouteData, $tempDriverData,$requestRoute=[])
    {
        //dump($oldTempRouteData, $tempDriverData);
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $resultTempRouteData=[];

        $newShiftName = !empty($tempDriverData['drt_shift_name']) ? $tempDriverData['drt_shift_name'] : $tempDriverData['shift_name'];
        $oldShiftName = !empty($oldTempRouteData['drt_shift_name']) ? $oldTempRouteData['drt_shift_name'] : $oldTempRouteData['shift_name'];

        $tempDateCreated = new \DateTime($tempDriverData['dateCreated']);
        $OldTempDateCreated = new \DateTime($oldTempRouteData['dateCreated']);

        $newShiftTime = $tempDateCreated->format('F d, Y') . ' (' . $tempDateCreated->format('D') . ')';
        $oldShiftTime = $OldTempDateCreated->format('F d, Y') . ' (' . $OldTempDateCreated->format('D') . ')';
        $routeId = $tempDriverData['route_id'];
        if(!empty($tempDriverData['created_by_id'])){
            $loginUserName = $tempDriverData['created_by_friendly_name'];
            $loginUserProfile = $tempDriverData['created_by_user_profileImage'];
            $loginRole = $this->globalUtils->getPosition($tempDriverData['created_by_role_name']);
        } else {
            $loginUserName = $userLogin->getFriendlyName();
            $loginUserProfile = $userLogin->getProfileImage();
            $loginRole = $userLogin->getRoleName();
        }
        switch ($tempDriverData['action']) {
            case 'SHIFT_ADDED':
                if(!empty($tempDriverData['driver_id'])){
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['driver_image'] = $tempDriverData['user_profileImage'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = 'New '.$newShiftName.' Created for '.$tempDriverData['friendly_name'].' dated ' . $newShiftTime;
                } else {
                    $resultTempRouteData['driver'] = 'Open Route';
                    $resultTempRouteData['driver_image'] = '';
                    $resultTempRouteData['driverProfile'] = '';
                    $resultTempRouteData['changeText'] = 'New Open Shift Created for'.$newShiftName.' dated '. $newShiftTime;
                }
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;

                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 1;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;

            case 'SHIFT_EDITED':
                if(!empty($tempDriverData['driver_id'])){
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['driver_image'] = $tempDriverData['user_profileImage'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = 'Edited '.$newShiftName.' for '.$tempDriverData['friendly_name'].' dated '. $newShiftTime;
                } else {
                    $resultTempRouteData['driver'] = 'Open Route';
                    $resultTempRouteData['driver_image'] = '';
                    $resultTempRouteData['driverProfile'] = '';
                    $resultTempRouteData['changeText'] = 'Edited Open shift '.$newShiftName.' dated '. $newShiftTime;
                }
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;

                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 1;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;
            case 'SHIFT_DELETED':
                if(!empty($tempDriverData['driver_id'])){
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['driver_image'] = $tempDriverData['user_profileImage'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = 'Deleted '.$newShiftName.' for '.$tempDriverData['friendly_name'].' dated '. $newShiftTime;
                } else {
                    $resultTempRouteData['driver'] = 'Open Route';
                    $resultTempRouteData['driver_image'] = '';
                    $resultTempRouteData['driverProfile'] = '';
                    $resultTempRouteData['changeText'] = 'Deleted Open shift '.$newShiftName.' dated '. $newShiftTime;
                }
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;

                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 1;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;
            case 'SHIFT_MOVED':
                if(empty($tempDriverData['driver_id']) && !empty($oldTempRouteData['driver_id'])){
                    $resultTempRouteData['id'] = strval($tempDriverData['id']);
                    $resultTempRouteData['driver'] = $oldTempRouteData['friendly_name'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($oldTempRouteData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] =  $oldTempRouteData['friendly_name'].' moved from '.$newShiftName.' to Open Shift dated '. $newShiftTime;
                    $resultTempRouteData['user'] = $loginUserName;
                    $resultTempRouteData['userProfile'] = $loginRole;
                    $resultTempRouteData['route_id'] = $routeId;
                    $resultTempRouteData['driver_image'] = $oldTempRouteData['user_profileImage'];
                    $resultTempRouteData['user_image'] = $loginUserProfile;
                    $resultTempRouteData['status'] = 1;
                    $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                    $resultTempRouteData['driverRouteId'] = $routeId;
                }else if(empty($oldTempRouteData['driver_id']) && !empty($tempDriverData['driver_id'])){
                    $resultTempRouteData['id'] = strval($tempDriverData['id']);
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = 'Assign Open Shift '.$newShiftName.' to '.$tempDriverData['friendly_name'].' dated '. $newShiftTime;
                    $resultTempRouteData['user'] = $loginUserName;
                    $resultTempRouteData['userProfile'] = $loginRole;
                    $resultTempRouteData['route_id'] = $routeId;
                    $resultTempRouteData['driver_image'] = $tempDriverData['user_profileImage'];
                    $resultTempRouteData['user_image'] = $loginUserProfile;
                    $resultTempRouteData['status'] = 1;
                    $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                    $resultTempRouteData['driverRouteId'] = $routeId;
                }else if(empty($oldTempRouteData['driver_id']) && empty($tempDriverData['driver_id'])){
                    $resultTempRouteData['id'] = strval($tempDriverData['id']);
                    $resultTempRouteData['driver'] = 'Open Route';
                    $resultTempRouteData['driverProfile'] = '';
                    $resultTempRouteData['changeText'] = 'Open Shift '.$newShiftName.' dated ' . $oldShiftTime . ' moved to ' . $newShiftTime;
                    $resultTempRouteData['user'] = $loginUserName;
                    $resultTempRouteData['userProfile'] = $loginRole;
                    $resultTempRouteData['route_id'] = $routeId;
                    $resultTempRouteData['driver_image'] = '';
                    $resultTempRouteData['user_image'] = $loginUserProfile;
                    $resultTempRouteData['status'] = 1;
                    $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                    $resultTempRouteData['driverRouteId'] = $routeId;

                } else {
                    $resultTempRouteData['id'] = strval($tempDriverData['id']);
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = $oldTempRouteData['friendly_name']."'s ". $oldShiftName." dated ".$oldShiftTime." moved to ".$tempDriverData['friendly_name']." dated ".$newShiftTime;
                    $resultTempRouteData['user'] = $loginUserName;
                    $resultTempRouteData['userProfile'] = $loginRole;
                    $resultTempRouteData['route_id'] = $routeId;
                    $resultTempRouteData['driver_image'] = $tempDriverData['user_profileImage'];
                    $resultTempRouteData['user_image'] = $loginUserProfile;
                    $resultTempRouteData['status'] = 1;
                    $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                    $resultTempRouteData['driverRouteId'] = $routeId;
                }
                break;
            case 'SWAP_REQUEST':
                $request = $this->getRouteRequest($tempDriverData['request_id'],'SWAP_REQUEST');
                $request = $request[0];

                $resultTempRouteData['id'] = $request['old_temp_route_id'].'-'.$request['new_temp_route_id'];
                $resultTempRouteData['driver'] = $request['new_user_friendlyName'].' and '.$request['old_user_friendlyName'];
                $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($request['new_user_role_name']);

                $oldShiftName = $request['old_shift_name'];
                $oldDateCreated = new \DateTime($request['old_roue_date_created']);
                $oldShiftTime = $oldDateCreated->format('F d, Y') . ' (' . $oldDateCreated->format('D') . ')';

                $newShiftName = $request['new_shift_name'];
                $newDateCreated = new \DateTime($request['new_roue_date_created']);
                $newShiftTime = $newDateCreated->format('F d, Y') . ' (' . $newDateCreated->format('D') . ')';

                $message = "Swap Request for ".$request['old_user_friendlyName']."'s ". $oldShiftName." dated ". $oldShiftTime. " with ".$request['new_user_friendlyName']."'s ". $newShiftName." dated ".$newShiftTime;

                $resultTempRouteData['changeText'] = $message;
                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['driver_image'] = $request['old_user_profile_image'];
                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 2;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $request['new_route_id'];
                $resultTempRouteData['swapId'] = [$request['old_temp_route_id'],$request['new_temp_route_id']];
                break;
            case 'RELEASE_REQUEST':

                $request = $this->getRouteRequest($tempDriverData['request_id']);
                $request = $request[0];

                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['driver'] = $request['old_user_friendlyName'];
                $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($request['old_user_role_name']);
                $resultTempRouteData['changeText'] = $request['old_user_friendlyName']." created release shift request for ". $oldShiftName." dated ".$oldShiftTime;

                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['driver_image'] = $request['old_user_profile_image'];
                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 3;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;
            case 'SHIFT_REQUEST':

                $request = $this->getRouteRequest($tempDriverData['request_id']);
                $request = $request[0];

                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['driver'] = $request['new_user_friendlyName'];
                $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($request['new_user_role_name']);
                $resultTempRouteData['changeText'] = $request['new_user_friendlyName']." create shift request for ". $newShiftName." dated ".$newShiftTime;

                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;
                $resultTempRouteData['route_id'] = $routeId;
                $resultTempRouteData['driver_image'] = $request['new_user_profile_image'];
                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 4;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;
            default:
                if($tempDriverData->getDriver()){
                    $resultTempRouteData['driver'] = $tempDriverData['friendly_name'];
                    $resultTempRouteData['route_id'] = $routeId;
                    $resultTempRouteData['driver_image'] = $oldTempRouteData['user_profileImage'];
                    $resultTempRouteData['driverProfile'] = $this->globalUtils->getPosition($tempDriverData['driver_user_role_name']);
                    $resultTempRouteData['changeText'] = "Some changes made in Open Shift ".$newShiftName." dated ".$newShiftTime;
                } else {
                    $resultTempRouteData['driver'] = 'Open Route';
                    $resultTempRouteData['route_id'] = 0;
                    $resultTempRouteData['driver_image'] = '';
                    $resultTempRouteData['driverProfile'] = '';
                    $resultTempRouteData['changeText'] = "Some changes made in ".$tempDriverData['friendly_name']."'s ".$newShiftName." date ".$newShiftTime;
                }
                $resultTempRouteData['id'] = strval($tempDriverData['id']);
                $resultTempRouteData['user'] = $loginUserName;
                $resultTempRouteData['userProfile'] = $loginRole;
                $resultTempRouteData['user_image'] = $loginUserProfile;
                $resultTempRouteData['status'] = 1;
                $resultTempRouteData['stationCode'] = $tempDriverData['station_code'];
                $resultTempRouteData['driverRouteId'] = $routeId;
                break;
             }
        return $resultTempRouteData ;
    }
    public function getUnPublishTempDriverRouteList($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $weekStartEndDay = $this->getStartAndEndDate($criteria['wknumber'], $criteria['year']);
        $unpublishDataDate = isset($criteria['unpublishDataDate']) ? $criteria['unpublishDataDate'] : null;

        $queryString = '';
        if (!empty($unpublishDataDate)) {
            $queryString = " AND tdr.date_created = '".$unpublishDataDate."'";
        }

        if (isset($criteria['station']) && ((is_array($criteria['station']) && count($criteria['station']) > 0) || (!is_array($criteria['station']) && $criteria['station'] > 0))) {

            if (is_array($criteria['station']) && ($key = array_search(-1, $criteria['station'])) !== false) {
                unset($criteria['station'][$key]);
            }

            $stations = $criteria['station'];

            if (!is_array($stations))
                $stations = [$stations];

            if (!empty($stations)) {
                $queryString .= " AND tdr.station_id in(".implode(",",$stations).")";
            }
        }

        $sqlQuery = "SELECT
        tdr.id,
        dr.id as route_id,
        tdr.is_new as is_new,
        dri.id as driver_id,
        u.friendly_name as friendly_name,
        u.profile_image as user_profileImage,
        cuser.friendly_name as created_by_friendly_name,
        cuser.profile_image as created_by_user_profileImage,
        tdr.shift_name as drt_shift_name,
        sf.name as shift_name,
        tdr.date_created as dateCreated,
        tdr.created_by_id as created_by_id,
        st.code as station_code,
        tdr.action as action,
        dr.request_for_route_id as request_id,
        req.new_driver_id as new_driver_id,
        req.approved_by_id as approved_by_id,
        req.old_route_id as old_route_id,
        req.new_route_id as new_route_id,
        req.old_driver_id as old_driver_id,
        req.reason_for_request as reason_for_request,
        req.is_swap_request as is_swap_request,
        req.is_request_completed as is_request_completed,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name ,
        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = cuser.id limit 1) as created_by_role_name
        FROM
            temp_driver_route tdr
        LEFT JOIN
            driver_route dr
            ON
            dr.id = tdr.route_id_id
        LEFT JOIN
            driver dri
            ON
            dri.id = tdr.driver_id

        LEFT JOIN
            user u
            ON
            u.id = dri.user_id
        LEFT JOIN
            shift sf
            ON
            sf.id = tdr.shift_type_id
        LEFT JOIN
            station st
            ON
            st.id = tdr.station_id
        LEFT JOIN
            user cuser
            ON
            cuser.id = tdr.created_by_id
        LEFT JOIN
            route_requests req
            ON
            req.id = dr.request_for_route_id

        WHERE
        tdr.date_created >= '".$weekStartEndDay['week_start']."'
        AND tdr.date_created <= '".$weekStartEndDay['week_end']."'
        AND sf.company_id = ".$userLogin->getCompany()->getId()."
        AND st.is_archive = 0
        AND dr.is_archive = 0
        AND (tdr.created_by_id is NULL OR tdr.is_new = 1)".$queryString;

        $results = $this->getEntityManager()
            ->getConnection()
            ->query($sqlQuery)
            ->fetchAll();

        $tempDriverRoutes = [];


        foreach ($results as $result) {
            $tempDriverRoutes[$result['route_id']][$result['is_new']][] = $result;
        }

        $resultTempRouteData = [];
        $requestArray = [];
        $tempRouteId = $tempDriverName = [];
        $count = 0;
        $requestRoute = [];

        foreach ($tempDriverRoutes as $key => $tempDriverRoute) {
            if (isset($tempDriverRoute[1])) {
                foreach ($tempDriverRoute[1] as $tempDriverData) {
                    // This supports the implementation for from @roberto popup.
                    // TODO: Rehana please check that this code works properly. I feel that this might be a work around (sp_corephp)
                    if (isset($tempDriverRoute[0][0])) {
                        $oldTempRouteData = $tempDriverRoute[0][0];
                    } else {
                        $oldTempRouteData = $tempDriverRoute[1][0];
                    }
                    if (!in_array($tempDriverData['id'], $requestRoute)){
                        $msgResponse = $this->getPublishTempDriverRouteMessage($oldTempRouteData, $tempDriverData,$requestRoute);
                        $resultTempRouteData[$count] = $msgResponse;
                        if(isset($msgResponse['swapId'])){
                            $requestRoute = array_unique(array_merge($requestRoute,$msgResponse['swapId']));
                        }
                        $count++;
                    }
                }
            }
        }
        return array_values($resultTempRouteData);
    }

    public function getDriverRouteDataByRouteAndUser($userLogin, $routeId)
    {
        return $this->createQueryBuilder('tdr')
            // ->where('tdr.createdBy = :userId')
            // ->setParameter('userId', $userLogin->getId())
            ->where('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->andWhere('tdr.routeId = :routeId')
            ->setParameter('routeId', $routeId)
            ->getQuery()
            ->getResult();
    }

    //This api for add vehicle,driver and device
    public function addReplaceDriverLoadOutTempDriverRouteData($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $driverId = $criteria['driverId'];
        $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);

        $routeId = $criteria['routeId'];
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($routeId);

        $oldRouteId = $criteria['oldRouteId'];
        $oldDriverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($oldRouteId);

        $routeTempData = $this->getDriverRouteDataByRouteAndUser($userLogin, $routeId);
        if (!empty($routeTempData)) {
            $routeTempData = $routeTempData[0];
            $routeTempData->setDriver($driver);
            $routeTempData->setOldDriver($driverRoute->getDriver());
            $routeTempData->setOldDriverRoute($oldDriverRoute);
            $routeTempData->setIsBackup(1);
            $routeTempData->setCreatedBy($userLogin);
            $this->getEntityManager()->persist($routeTempData);
            $this->getEntityManager()->flush();
        } else {
            //Check route id exist in temp route data
            $tempDriverRoute = $this->getTempDriverRouteByRouteId($routeId);
            if (empty($tempDriverRoute)) {
                $tempDriverRoute = new TempDriverRoute();
                $tempDriverRoute->setDateCreated($driverRoute->getDateCreated());
                $tempDriverRoute->setSkill($driverRoute->getSkill());
                $tempDriverRoute->setRouteId($driverRoute);
                $tempDriverRoute->setOldDriver($driverRoute->getOldDriver());
                $tempDriverRoute->setOldDriverRoute($driverRoute->getOldDriverRoute());
                $tempDriverRoute->setIsBackup(!empty($driverRoute->getIsBackup()) ? 1 : 0);
                // $tempDriverRoute->setCreatedBy($userLogin);
                $tempDriverRoute->setDriver($driverRoute->getDriver());
                $tempDriverRoute->setStation($driverRoute->getStation());
                $tempDriverRoute->setPunchIn($driverRoute->getPunchIn());
                $tempDriverRoute->setPunchOut($driverRoute->getPunchOut());
                //$tempDriverRoute->setShiftTime($driverRoute->getShiftTime());
                //$tempDriverRoute->setVehicleDriverRecord($driverRoute->getVehicleDriverRecord());
                //$tempDriverRoute->setKickoffLog($driverRoute->getKickoffLog());
                $tempDriverRoute->setBreakPunchOut($driverRoute->getBreakPunchOut());
                $tempDriverRoute->setBreakPunchIn($driverRoute->getBreakPunchIn());
                $tempDriverRoute->setStatus($driverRoute->getStatus());
                $tempDriverRoute->setRouteCode($driverRoute->getRouteCode());
                $tempDriverRoute->setIsRescuer($driverRoute->getIsRescuer());
                $tempDriverRoute->setIsRescued($driverRoute->getIsRescued());
                $tempDriverRoute->setShiftType($driverRoute->getShiftType());
                $tempDriverRoute->setSchedule($driverRoute->getSchedule());
                //new fields added
                $tempDriverRoute->setIsTemp($driverRoute->getIsTemp());
                $tempDriverRoute->setIsOpenShift($driverRoute->getIsOpenShift());
                $tempDriverRoute->setShiftName($driverRoute->getShiftType()->getName());
                $tempDriverRoute->setShiftColor($driverRoute->getShiftType()->getColor());
                $tempDriverRoute->setUnpaidBreak($driverRoute->getShiftType()->getUnpaidBreak());
                $tempDriverRoute->setShiftNote($driverRoute->getShiftType()->getNote());
                $tempDriverRoute->setShiftInvoiceType($driverRoute->getShiftType()->getInvoiceType());
                $tempDriverRoute->setShiftStation($driverRoute->getShiftType()->getStation());
                $tempDriverRoute->setShiftSkill($driverRoute->getShiftType()->getSkill());
                $tempDriverRoute->setShiftCategory($driverRoute->getShiftType()->getCategory());
                $tempDriverRoute->setShiftBalanceGroup($driverRoute->getShiftType()->getBalanceGroup());
                $tempDriverRoute->setShiftTextColor($driverRoute->getShiftType()->getTextColor());
                $tempDriverRoute->setStartTime($driverRoute->getStartTime());
                $tempDriverRoute->setEndTime($driverRoute->getEndTime());
                $tempDriverRoute->setIsNew(0);
                $this->getEntityManager()->persist($tempDriverRoute);
                $this->getEntityManager()->flush();
            }
            $tempDriverRoutesClone = is_array($tempDriverRoute) ? clone($tempDriverRoute[0]) : clone($tempDriverRoute);
            $tempDriverRoutesClone->setDriver($driver);
            $tempDriverRoutesClone->setOldDriver($driverRoute->getDriver());
            $tempDriverRoutesClone->setOldDriverRoute($oldDriverRoute);
            $tempDriverRoutesClone->setIsBackup(1);
            $tempDriverRoutesClone->setIsNew(1);
            $tempDriverRoutesClone->setIsLoadOut(1);
            $tempDriverRoutesClone->setCreatedBy($userLogin);
            $this->getEntityManager()->persist($tempDriverRoutesClone);
            $this->getEntityManager()->flush();
        }
        //Old Driver Route table change isActive false and oldRouteId set
        $oldDriverRoute->setIsActive(0);
        $oldDriverRoute->setOldDriverRoute($driverRoute);
        $this->getEntityManager()->persist($oldDriverRoute);
        $this->getEntityManager()->flush();

        return true;
    }

    //This api for add vehicle,driver and device
    public function addLoadOutTempDriverRouteData($criteria)
    {
        $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currentDateObj->setTime(0, 0, 0);
       /* $currentDate = $currentDateObj->format('Y-m-d');*/
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $type = isset($criteria['type'])?$criteria['type']:'';

        if (isset($criteria['deviceId'])) {
            $driverRoutes = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['routeId']);
            $oldDeviceArray = [];
            $newDeviceArray = [];
            foreach ($driverRoutes->getDevice() as $device) {
                $oldDeviceArray[] = $device->getName();
                $driverRoutes->removeDevice($device);
                $this->em->persist($driverRoutes);
                $this->em->flush();
            }
            foreach ($criteria['deviceId'] as $deviceIds) {
                $deviceId = $this->getEntityManager()->getRepository('App\Entity\Device')->find($deviceIds);
                $newDeviceArray[] = $deviceId->getName();
                $deviceId->setUsedBy($criteria['name']);
                $deviceId->setUsedByUser($driverRoutes->getDriver() ? $driverRoutes->getDriver()->getUser() : null);
                $deviceId->setUsedAt($currentDateObj);
                $this->em->persist($deviceId);
                $this->em->flush($deviceId);

                $driverRoutes->addDevice($deviceId);
            }
            $this->em->persist($driverRoutes);
            $this->em->flush();

            $oldDeviceString = implode(" , ",$oldDeviceArray);
            $newDeviceString = implode(" , ",$newDeviceArray);
            if (!empty($oldDeviceArray) && $newDeviceString != $oldDeviceString) {
                $messageData = array(
                    'old_devices' => $oldDeviceString,
                    'new_devices' => ($newDeviceString != '')?$newDeviceString:'null',
                    'shift_name'=>$driverRoutes->getShiftName(),
                    'user_name'=>$userLogin->getFriendlyName());
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Device Edit', $driverRoutes, 'DEVICE_EDIT', $messageData, true);
            } elseif($newDeviceString != $oldDeviceString) {
                $messageData = array(
                    'new_devices' => $newDeviceString,
                    'shift_name'=>$driverRoutes->getShiftName(),
                    'user_name'=>$userLogin->getFriendlyName());
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Device Add', $driverRoutes, 'DEVICE_ADD', $messageData, true);
            }
        }

        if ($type == 'vehicle') {
            $driverRoutes = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['routeId']);
            $qb = $this->getEntityManager()->createQueryBuilder();

            $vehicleId = isset($criteria['vehicleId'])?$this->getEntityManager()->getRepository('App\Entity\Vehicle')->find($criteria['vehicleId']):null;
            $qb->update('App\Entity\DriverRoute', 'dr')
                ->set('dr.vehicle', ':vehicleId')
                ->setParameter('vehicleId', $vehicleId)
                ->where('dr.id = :driverRouteId')
                ->setParameter('driverRouteId', $criteria['routeId'])
                ->getQuery()
                ->execute();
            if($vehicleId != null){
                $vehicleId->setCurrentUsedBy($criteria['name']);
                $this->em->persist($vehicleId);
                $this->em->flush($vehicleId);
            }

            if(empty($driverRoutes->getVehicle())){
                if($vehicleId){
                    $messageData = array(
                        'new_vehicle' => $vehicleId->getVehicleId(),
                        'shift_name'=>$driverRoutes->getShiftName(),
                        'user_name'=>$userLogin->getFriendlyName());
                    $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Vehicle Add', $driverRoutes, 'VEHICLE_ADD', $messageData, true);
                }
            }elseif($vehicleId == null || ($driverRoutes->getVehicle()->getId() != $vehicleId->getId())) {
                $messageData = array(
                    'old_vehicle' => $driverRoutes->getVehicle()->getVehicleId(),
                    'new_vehicle' => ($vehicleId == null)?'null':$vehicleId->getVehicleId(),
                    'shift_name'=>$driverRoutes->getShiftName(),
                    'user_name'=>$userLogin->getFriendlyName());
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Vehicle Edit', $driverRoutes, 'VEHICLE_EDIT', $messageData, true);

            }
        }

        $driver = '';
        $driverId = isset($criteria['driverId']) ? $criteria['driverId'] : '';
        if ($driverId) {
            $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);

            $routeId = $criteria['routeId'];
            // if ($routeId) {
            //     $routeCode = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
            // }
            $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($routeId);

            $routeTempData = $this->getDriverRouteDataByRouteAndUser($userLogin, $routeId);

            if (!empty($routeTempData)) {
                $routeTempData = $routeTempData[0];

                if (!empty($driver)) {
                    $routeTempData->setDriver($driver);
                }
                $routeTempData->setCreatedBy($userLogin);
                $this->getEntityManager()->persist($routeTempData);
                $this->getEntityManager()->flush();
            } else {
                //Check route id exist in temp route data
                $tempDriverRoute = $this->getTempDriverRouteByRouteId($routeId);
                if (empty($tempDriverRoute)) {
                    $tempDriverRoute = new TempDriverRoute();
                    $tempDriverRoute->setDateCreated($driverRoute->getDateCreated());
                    $tempDriverRoute->setSkill($driverRoute->getSkill());
                    $tempDriverRoute->setRouteId($driverRoute);
                    $tempDriverRoute->setDriver($driverRoute->getDriver());
                    $tempDriverRoute->setStation($driverRoute->getStation());
                    $tempDriverRoute->setPunchIn($driverRoute->getPunchIn());
                    $tempDriverRoute->setPunchOut($driverRoute->getPunchOut());
                    //$tempDriverRoute->setShiftTime($driverRoute->getShiftTime());
                    //$tempDriverRoute->setVehicleDriverRecord($driverRoute->getVehicleDriverRecord());
                    //$tempDriverRoute->setKickoffLog($driverRoute->getKickoffLog());
                    $tempDriverRoute->setBreakPunchOut($driverRoute->getBreakPunchOut());
                    $tempDriverRoute->setBreakPunchIn($driverRoute->getBreakPunchIn());
                    $tempDriverRoute->setStatus($driverRoute->getStatus());
                    $tempDriverRoute->setRouteCode($driverRoute->getRouteCode());
                    $tempDriverRoute->setIsRescuer($driverRoute->getIsRescuer());
                    $tempDriverRoute->setIsRescued($driverRoute->getIsRescued());
                    $tempDriverRoute->setShiftType($driverRoute->getShiftType());
                    $tempDriverRoute->setSchedule($driverRoute->getSchedule());
                    //new fields added
                    $tempDriverRoute->setOldDriver($driverRoute->getOldDriver());
                    $tempDriverRoute->setOldDriverRoute($driverRoute->getOldDriverRoute());
                    $tempDriverRoute->setIsBackup($driverRoute->getIsBackup());
                    $tempDriverRoute->setIsTemp($driverRoute->getIsTemp());
                    $tempDriverRoute->setIsOpenShift($driverRoute->getIsOpenShift());
                    $tempDriverRoute->setShiftName($driverRoute->getShiftType()->getName());
                    $tempDriverRoute->setShiftColor($driverRoute->getShiftType()->getColor());
                    $tempDriverRoute->setUnpaidBreak($driverRoute->getShiftType()->getUnpaidBreak());
                    $tempDriverRoute->setShiftNote($driverRoute->getShiftType()->getNote());
                    $tempDriverRoute->setShiftInvoiceType($driverRoute->getShiftType()->getInvoiceType());
                    $tempDriverRoute->setShiftStation($driverRoute->getShiftType()->getStation());
                    $tempDriverRoute->setShiftSkill($driverRoute->getShiftType()->getSkill());
                    $tempDriverRoute->setShiftCategory($driverRoute->getShiftType()->getCategory());
                    $tempDriverRoute->setShiftBalanceGroup($driverRoute->getShiftType()->getBalanceGroup());
                    $tempDriverRoute->setShiftTextColor($driverRoute->getShiftType()->getTextColor());
                    $tempDriverRoute->setStartTime($driverRoute->getStartTime());
                    $tempDriverRoute->setEndTime($driverRoute->getEndTime());
                    $tempDriverRoute->setIsNew(0);
                    $this->getEntityManager()->persist($tempDriverRoute);
                    $this->getEntityManager()->flush();
                }
                $tempDriverRoutesClone = is_array($tempDriverRoute) ? clone($tempDriverRoute[0]) : clone($tempDriverRoute);

                if (!empty($driver)) {
                    $tempDriverRoutesClone->setDriver($driver);
                }
                $tempDriverRoutesClone->setIsNew(1);
                $tempDriverRoutesClone->setIsLoadOut(1);
                $tempDriverRoutesClone->setCreatedBy($userLogin);
                $this->getEntityManager()->persist($tempDriverRoutesClone);
                $this->getEntityManager()->flush();
            }
        }
        return true;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function releaseDriverRoute(array $criteria)
    {
        if (empty($criteria['driverRouteId'])) {
            throw new Exception('driverRouteId field is missing');
        }
        if (empty($criteria['weekFrom'])) {
            throw new Exception('weekFrom field is missing');
        }
        if (empty($criteria['weekTo'])) {
            throw new Exception('weekTo field is missing');
        }
        if (empty($criteria['timezoneOffset'])) {
            throw new Exception('timezoneOffset field is missing');
        }
        if (empty($criteria['weekNumber'])) {
            throw new Exception('weekNumber field is missing');
        }
        if(empty($criteria['reasonForReleasing'])){
            throw new Exception('reasonForReleasing field is missing');
        }
        $result = $this->em->getRepository(DriverRoute::class)->findOneBy(['id' => $criteria['driverRouteId']]);
        if (!($result instanceof DriverRoute)) {
            throw new Exception('DriverRoute with ID: ' . $criteria['driverRouteId'] . ' was not found.');
        }
        $routeId = $result->getId();
        if (empty($routeId)) {
            throw new Exception('$routeId is empty.');
        }
        $shiftId = $result->getShiftType()->getId();
        if (empty($shiftId)) {
            throw new Exception('$shiftId is empty.');
        }
        $driverId = $result->getDriver()->getId();
        if (empty($driverId)) {
            throw new Exception('$driverId is empty.');
        }
        $stationId = $result->getStation()->getId();
        if (empty($stationId)) {
            throw new Exception('$stationId is empty.');
        }
        $startTime = floatval($result->getStartTime()->format('G.i'));
        $endTime = floatval($result->getEndTime()->format('G.i'));
        $endTimeOfPriorShiftObj = !empty($result->getOldDriverRoute()) ? $result->getOldDriverRoute()->getEndTime() : null;
        $endTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;
        $utcEndTimeOfPriorShift = !empty($endTimeOfPriorShiftObj) ? $endTimeOfPriorShiftObj->format('G.i') : 0;
        $startTimeObj = $this->globalUtils->getTimeZoneConversation($result->getStartTime(), $criteria['timezoneOffset']);
        $endTimeObj = $this->globalUtils->getTimeZoneConversation($result->getEndTime(), $criteria['timezoneOffset']);
        $startHour = $startTimeObj->format($this->params->get('time_format'));
        $endHour = $endTimeObj->format($this->params->get('time_format'));
        $data['routeId'] = $routeId;
        $data['isNew'] = null;
        $data["newDriverId"] = "0.0.0";
        $data["newDate"] = $result->getDateCreated()->format('yy-m-d').' 00:00:00';
        $data["isOpenShift"] = null;
        $data["shiftId"] = $shiftId;
        $data["oldDriverId"] = $driverId;
        $data["weekFrom"] = $criteria['weekFrom'];
        $data["weekTo"] = $criteria['weekTo'];
        $data["indexSource"] = 0;
        $data["hasAlert"] = false;
        $data["item"]["id"] = $routeId;
        $data["item"]["hour"] = $startHour . '-' . $endHour; //"8:15am-12:15pm";
        $data["item"]["startTime"] = $startTime;
        $data["item"]["endTime"] = $endTime;
        $data["item"]["utcStartTime"] = $startTime;
        $data["item"]["utcEndTime"] = $endTime;
        $data["item"]["endTimeOfPriorShift"] = $endTimeOfPriorShift;
        $data["item"]["utcEndTimeOfPriorShift"] = $utcEndTimeOfPriorShift;
        $data["item"]["typeId"] = $shiftId;
        $data["item"]["type"] = $result->getShiftName();
        $data["item"]["category"] = $result->getShiftCategory();
        $data["item"]["bg"] = $result->getShiftColor(); //"#7FD1DE";
        $data["item"]["color"] = $result->getShiftTextColor();//;"#333333";
        $data["item"]["skill"] = $result->getSkill()->getId();///209;
        $data["item"]["note"] = $result->getShiftNote();
        $data["item"]["countOfTotalStops"] = $result->getCountOfTotalStops();
        $data["item"]["completedCountOfStops"] = $result->getCompletedCountOfStops();
        $data["item"]["countOfTotalPackages"] = $result->getCountOfTotalPackages();
        $data["item"]["countOfAttemptedPackages"] = $result->getCountOfAttemptedPackages();
        $data["item"]["countOfReturningPackages"] = $result->getCountOfReturningPackages();
        $data["item"]["countOfDeliveredPackages"] = $result->getCountOfDeliveredPackages();
        $data["item"]["countOfMissingPackages"] = $result->getCountOfMissingPackages();
        $data["item"]["percent"] = "0%";
        $data["item"]["time"] = "00] = 00";
        $data["item"]["isNew"] = false;
        $data["item"]["dateTimeEnded"] = null;
        $data["item"]["currentWeek"] = $criteria['weekNumber'];
        $data["item"]["originalDay"] = $criteria['weekFrom'];
        $data["item"]["isOpenShift"] = false;
        $data["item"]["stationId"] = $stationId;
        $data["item"]["routeId"] = $routeId;
        $data["item"]["priorShiftRouteId"] = null;
        $data["item"]["shiftDate"] = $result->getDateCreated()->format('yy-m-d');
        $data["item"]["driverId"] = $driverId;
        $data["item"]["routeCode"] = array_map(function (DriverRouteCode $item) {
            return $item->getCode();
        }, $result->getDriverRouteCodes()->toArray());
        $data["item"]["timezone"] = $result->getStation()->getTimeZone();
        $data["item"]["isTemp"] = false;
        $data["item"]["canDelete"] = true;
        $data["item"]["isRescued"] = false;
        $data["item"]["isRescuer"] = false;
        $data["item"]["numberOfPackage"] = $result->getNumberOfPackage() ? $result->getNumberOfPackage() : 0;
        $data["item"]["invoiceTypeId"] = $result->getShiftInvoiceType() ? $result->getShiftInvoiceType()->getId() : null;
        $data["item"]["timeline"] = [];
        $data["item"]["driverStation"] = $stationId;
        $data["item"]["isExempt"] = false;
        $data["item"]["isBackup"] = false;
        $data["item"]["driver"]["id"] = $driverId;
        $data["item"]["driver"]["name"] = $result->getDriver()->getFullName();
        $data["item"]["driver"]["img"] = $result->getDriver()->getUser()->getProfileImage();
        // TODO: CHECK PROCESS
        list($workHours, $estimatedWorkHours) = $this->em->getRepository(DriverRoute::class)->getWorkHoursEstimatedWorkHours(['id' => $driverId, 'date' => $result->getDateCreated()->format('Y-m-d'), 'timezoneOffset' => $criteria['timezoneOffset']]);
        $data["item"]["driver"]["workHours"] = $workHours;
        $data["item"]["driver"]["estimatedWorkHours"] = $estimatedWorkHours;
        // TODO: CHECK PROCESS
        $data["item"]["driver"]["skills"] = array_map(function (DriverSkill $item) {
            if ($item->getSkill()) {
                return $item->getSkill()->getId();
            }
            return null;
        }, $result->getDriver()->getDriverSkills()->toArray());
        $data["item"]["driver"]["stationId"] = $stationId;
        $data["isTemp"] = false;
        $data['startTime'] = $startTime; //12.15;
        $data['endTime'] = $endTime; // 16.15;
        $data['timezoneOffset'] = $criteria['timezoneOffset'];

        if(!empty($criteria['reasonForReleasing'])){
            $result->setIsReleaseShiftRequested(true);
            $result->setReasonForReleasing($criteria['reasonForReleasing']);
            $this->em->flush();
        }

        return $this->addTempDriverRoute($data);
    }

    /**
     * @param array $criteria
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function addTempDriverRoute(array $criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $driverId = $criteria['newDriverId'];
        if ($driverId == '0.0.0') {$driverId = null;}
        $routeId = $criteria['routeId'];
        $isNew = $criteria['isNew'];
        $date = $criteria['newDate'];
        $isTemp = $criteria['isTemp'];
        $qty = isset($criteria['item']['qty']) ? (int)$criteria['item']['qty'] : 1;
        $qty = ($qty == 0) ? 1 : $qty;
        $shiftInvoiceType = null;
        /** @var Shift $shiftTypeObj */
        $shiftTypeObj = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($criteria['shiftId']);
        $dateObj = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $startTime = isset($criteria['startTime']) ? new DateTime($dateObj->format('Y-m-d ' . $criteria['startTime'] . ':00')) : ($shiftTypeObj ? $shiftTypeObj->getStartTime() : null);
        $endTime = isset($criteria['endTime']) ? new DateTime($dateObj->format('Y-m-d ' . $criteria['endTime'] . ':00')) : ($shiftTypeObj ? $shiftTypeObj->getEndTime() : null);
        if (isset($criteria['shiftInvoiceType'])) {
            if ($criteria['shiftInvoiceType'] == 0 || $criteria['shiftInvoiceType'] == 'null' )  {
                $shiftInvoiceType = null;
            } else {
                $shiftInvoiceType = $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($criteria['shiftInvoiceType']);
            }
        }
        $ret = true;
        $userLogin = $this->tokenStorage->getToken()->getUser();

        for ($i = 0; $i < $qty; $i++) {

            $driver = null;
            if ($driverId) {
                $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
            }

            $driverRoute = null;
            if ($routeId) {
                $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($routeId);
            }

            $note = $newNote = $currentNote = '';
            if (isset($criteria['item']['note'])) {
                $note = $currentNote = $criteria['item']['note'];
            }
            if (isset($criteria['item']['newNote'])) {
                $newNote  = $criteria['item']['newNote'];
            }

            if (empty($driverRoute)) {
                if (!isset($criteria['item']['note'])) {
                    $note = $shiftTypeObj->getNote();
                }
                $driverRoute = new DriverRoute();
                $driverRoute->setSkill($shiftTypeObj->getSkill());
                $driverRoute->setDriver($driver);
                $driverRoute->setStation($shiftTypeObj->getStation());
                $driverRoute->setDateCreated(new DateTime($date));
                $driverRoute->setShiftType($shiftTypeObj);
                $driverRoute->setIsTemp($isTemp);
                $driverRoute->setIsOpenShift(empty($driver));
                $driverRoute->setIsActive(true);
                $driverRoute->setShiftName($shiftTypeObj->getName());
                $driverRoute->setShiftColor($shiftTypeObj->getColor());
                $driverRoute->setUnpaidBreak($shiftTypeObj->getUnpaidBreak());
                $driverRoute->setShiftNote($note);
                $driverRoute->setShiftInvoiceType($shiftInvoiceType);
                $driverRoute->setShiftStation($shiftTypeObj->getStation());
                $driverRoute->setShiftSkill($shiftTypeObj->getSkill());
                $driverRoute->setShiftCategory($shiftTypeObj->getCategory());
                $driverRoute->setShiftBalanceGroup($shiftTypeObj->getBalanceGroup());
                $driverRoute->setShiftTextColor($shiftTypeObj->getTextColor());
                $driverRoute->setStartTime($startTime);
                $driverRoute->setEndTime($endTime);
                $driverRoute->setIsRescuer(false);
                $driverRoute->setIsRescued(false);
                $driverRoute->setIsLoadOut(false);
                $driverRoute->setIsBackup(false);

                $driverRoute->setCurrentNote($currentNote);
                $driverRoute->setNewNote($newNote);

                $this->getEntityManager()->persist($driverRoute);
                $this->getEntityManager()->flush();
                $this->getEntityManager()->getRepository('App\Entity\Shift')->addNewShiftEventForTimeline($userLogin, $driverRoute);
                $currentAction = 'SHIFT_ADDED';
            } else {
                $oldDriver = !empty($driverRoute->getDriver()) ? $driverRoute->getDriver()->getId() : null;
                $newDriver = !empty($driver) ? $driver->getId() : null;
                $isDriverChanged = ($oldDriver == $newDriver) ? false : true;
                $oldDate = $driverRoute->getDateCreated()->format('Y-m-d 00:00:00');
                $newDate = (new DateTime($date))->format('Y-m-d 00:00:00');
                $isDateChanged = ($oldDate == $newDate) ? false : true;
                $currentAction = isset($criteria['action']) ? $criteria['action'] : ((($isDriverChanged == false) && ($isDateChanged == false)) ? 'SHIFT_EDITED' : 'SHIFT_MOVED');
            }

            if ($driver === null && !empty($driverRoute) && $driverId !== null) {
                $driver = $driverRoute->getDriver();
            }

            if (isset($criteria['id'])) {
                $routeTempData = $this->find($criteria['id']);
            } else {
                $routeTempData = $this->createQueryBuilder('tdr')
                    ->where('tdr.isNew = :isNew')
                    ->setParameter('isNew', 1)
                    ->andWhere('tdr.routeId = :routeId')
                    ->setParameter('routeId', $routeId)
                    ->getQuery()->getResult();
            }

            $oldShiftId = $driverRoute->getShiftType()->getId();

            if (!empty($routeTempData)) {
                if (!isset($criteria['item']['note'])) {
                    $note = $shiftTypeObj->getNote() . "\n\r" . $note;
                }
                if (isset($criteria['item']['newNote'])) {
                    $note = $note . "\n\r" . $criteria['item']['newNote'];
                }
                $routeTempData = is_array($routeTempData) ? $routeTempData[0] : $routeTempData;
                $routeTempData->setDateCreated(new DateTime($date));
                $routeTempData->setAction($currentAction);
                $routeTempData->setDriver($driver);
                $routeTempData->setShiftType($shiftTypeObj);
                $routeTempData->setSkill($shiftTypeObj->getSkill());
                $routeTempData->setStation($shiftTypeObj->getStation());
                $routeTempData->setShiftName($shiftTypeObj->getName());
                $routeTempData->setShiftColor($shiftTypeObj->getColor());
                $routeTempData->setUnpaidBreak($shiftTypeObj->getUnpaidBreak());
                $routeTempData->setShiftNote($note);
                $routeTempData->setShiftStation($shiftTypeObj->getStation());
                $routeTempData->setShiftSkill($shiftTypeObj->getSkill());
                $routeTempData->setShiftCategory($shiftTypeObj->getCategory());
                $routeTempData->setShiftBalanceGroup($shiftTypeObj->getBalanceGroup());
                $routeTempData->setShiftTextColor($shiftTypeObj->getTextColor());
                $routeTempData->setShiftInvoiceType($shiftInvoiceType);
                $routeTempData->setStartTime($startTime);
                $routeTempData->setEndTime($endTime);

                // if (!empty($routeTempData->getPunchIn())) {
                //     $routeTempData->setPunchIn($startTime);
                // }
                //
                // if (!empty($routeTempData->getPunchOut())) {
                //     $routeTempData->setPunchOut($endTime);
                // }

                $routeTempData->setCurrentNote($currentNote);
                $routeTempData->setNewNote($newNote);

                $this->getEntityManager()->persist($routeTempData);
                $this->getEntityManager()->flush();

                $ret = $routeTempData->getId();
                $tempDriverRoutesClone = $routeTempData;
            } else {
                //Check route id exist in temp route data
                $tempDriverRoute = $this->getTempDriverRouteByRouteId($routeId);

                if (empty($tempDriverRoute) && isset($criteria['id'])) {
                    $tempDriverRoute = $this->find($criteria['id']);
                }

                if (empty($tempDriverRoute) && !empty($driverRoute)) {

                    if (!isset($criteria['item']['note'])) {
                        $note = $driverRoute->getShiftType()->getNote() . "\n\r" . $note;
                    }

                    if (isset($criteria['item']['newNote'])) {
                        $note = $note . "\n\r" . $criteria['item']['newNote'];
                    }

                    $tempDriverRoute = new TempDriverRoute();
                    $tempDriverRoute->setDateCreated($driverRoute->getDateCreated());
                    $tempDriverRoute->setSkill($driverRoute->getSkill());
                    $tempDriverRoute->setRouteId($driverRoute);
                    $tempDriverRoute->setDriver($driverRoute->getDriver());
                    $tempDriverRoute->setStation($driverRoute->getStation());
                    $tempDriverRoute->setPunchIn($driverRoute->getPunchIn());
                    $tempDriverRoute->setPunchOut($driverRoute->getPunchOut());
                    $tempDriverRoute->setBreakPunchOut($driverRoute->getBreakPunchOut());
                    $tempDriverRoute->setBreakPunchIn($driverRoute->getBreakPunchIn());
                    $tempDriverRoute->setStatus($driverRoute->getStatus());
                    $tempDriverRoute->setRouteCode($driverRoute->getRouteCode());
                    $tempDriverRoute->setIsRescuer($driverRoute->getIsRescuer());
                    $tempDriverRoute->setIsRescued($driverRoute->getIsRescued());
                    $tempDriverRoute->setShiftType($driverRoute->getShiftType());
                    $tempDriverRoute->setSchedule($driverRoute->getSchedule());
                    $tempDriverRoute->setIsOpenShift($driverRoute->getIsOpenShift());
                    $tempDriverRoute->setIsNew(0);
                    $tempDriverRoute->setIsTemp($isTemp);
                    $tempDriverRoute->setShiftName($driverRoute->getShiftName());
                    $tempDriverRoute->setShiftColor($driverRoute->getShiftColor());
                    $tempDriverRoute->setUnpaidBreak($driverRoute->getUnpaidBreak());
                    $tempDriverRoute->setShiftNote($note);
                    $tempDriverRoute->setShiftInvoiceType($driverRoute->getShiftInvoiceType());
                    $tempDriverRoute->setShiftStation($driverRoute->getShiftStation());
                    $tempDriverRoute->setShiftSkill($driverRoute->getShiftSkill());
                    $tempDriverRoute->setShiftCategory($driverRoute->getShiftCategory());
                    $tempDriverRoute->setShiftBalanceGroup($driverRoute->getShiftBalanceGroup());
                    $tempDriverRoute->setShiftTextColor($driverRoute->getShiftTextColor());
                    $tempDriverRoute->setStartTime($driverRoute->getStartTime());
                    $tempDriverRoute->setEndTime($driverRoute->getEndTime());
                    $tempDriverRoute->setOldDriver($driverRoute->getOldDriver());
                    $tempDriverRoute->setOldDriverRoute($driverRoute->getOldDriverRoute());
                    $tempDriverRoute->setIsBackup($driverRoute->getIsBackup());
                    $tempDriverRoute->setCurrentNote($currentNote);
                    $tempDriverRoute->setNewNote($newNote);
                    $this->getEntityManager()->persist($tempDriverRoute);
                    $this->getEntityManager()->flush();

                    $ret = $tempDriverRoute->getId();
                }

                if (!empty($tempDriverRoute)) {
                    $tempDriverRoutesClone = is_array($tempDriverRoute) ? clone($tempDriverRoute[0]) : clone($tempDriverRoute);

                    if (isset($criteria['item']['note'])) {
                        $note = $criteria['item']['note'];
                    } else {
                        $note = $shiftTypeObj->getNote();
                    }

                    if (isset($criteria['item']['newNote'])) {
                        $note = $note . "\n\r" . $criteria['item']['newNote'];
                    }

                    if ($criteria['shiftId'] != $oldShiftId) {
                        $tempDriverRoutesClone->setShiftType($shiftTypeObj);
                        $tempDriverRoutesClone->setSkill($shiftTypeObj->getSkill());
                        $tempDriverRoutesClone->setStation($shiftTypeObj->getStation());
                        $tempDriverRoutesClone->setShiftName($shiftTypeObj->getName());
                        $tempDriverRoutesClone->setShiftColor($shiftTypeObj->getColor());
                        $tempDriverRoutesClone->setUnpaidBreak($shiftTypeObj->getUnpaidBreak());
                        $tempDriverRoutesClone->setShiftStation($shiftTypeObj->getStation());
                        $tempDriverRoutesClone->setShiftSkill($shiftTypeObj->getSkill());
                        $tempDriverRoutesClone->setShiftCategory($shiftTypeObj->getCategory());
                        $tempDriverRoutesClone->setShiftBalanceGroup($shiftTypeObj->getBalanceGroup());
                        $tempDriverRoutesClone->setShiftTextColor($shiftTypeObj->getTextColor());
                    }
                    $tempDriverRoutesClone->setShiftNote($note);
                    $tempDriverRoutesClone->setShiftInvoiceType($shiftInvoiceType);
                    $tempDriverRoutesClone->setStartTime($startTime);
                    $tempDriverRoutesClone->setEndTime($endTime);

                    // if (!empty($tempDriverRoutesClone->getPunchIn())) {
                    //     $tempDriverRoutesClone->setPunchIn($startTime);
                    // }
                    //
                    // if (!empty($tempDriverRoutesClone->getPunchOut())) {
                    //     $tempDriverRoutesClone->setPunchOut($endTime);
                    // }

                } else {
                    $tempDriverRoutesClone = new TempDriverRoute();

                    if (!isset($criteria['item']['note'])) {
                        $note = $shiftTypeObj->getNote() . "\n\r" . $note;
                    }

                    if (isset($criteria['item']['newNote'])) {
                        $note = $note . "\n\r" . $criteria['item']['newNote'];
                    }

                    $tempDriverRoutesClone->setShiftType($shiftTypeObj);
                    $tempDriverRoutesClone->setSkill($shiftTypeObj->getSkill());
                    $tempDriverRoutesClone->setStation($shiftTypeObj->getStation());
                    $tempDriverRoutesClone->setBreakPunchOut($driverRoute->getBreakPunchOut());
                    $tempDriverRoutesClone->setBreakPunchIn($driverRoute->getBreakPunchIn());
                    $tempDriverRoutesClone->setStatus($driverRoute->getStatus());
                    $tempDriverRoutesClone->setRouteCode($driverRoute->getRouteCode());
                    $tempDriverRoutesClone->setIsRescuer($driverRoute->getIsRescuer());
                    $tempDriverRoutesClone->setIsRescued($driverRoute->getIsRescued());
                    $tempDriverRoutesClone->setSchedule($driverRoute->getSchedule());
                    $tempDriverRoutesClone->setShiftName($driverRoute->getShiftName());
                    $tempDriverRoutesClone->setShiftColor($driverRoute->getShiftColor());
                    $tempDriverRoutesClone->setUnpaidBreak($driverRoute->getUnpaidBreak());
                    $tempDriverRoutesClone->setShiftNote($note);
                    $tempDriverRoutesClone->setShiftStation($driverRoute->getShiftStation());
                    $tempDriverRoutesClone->setShiftSkill($driverRoute->getShiftSkill());
                    $tempDriverRoutesClone->setShiftCategory($driverRoute->getShiftCategory());
                    $tempDriverRoutesClone->setShiftBalanceGroup($driverRoute->getShiftBalanceGroup());
                    $tempDriverRoutesClone->setShiftTextColor($driverRoute->getShiftTextColor());
                    $tempDriverRoutesClone->setShiftInvoiceType($shiftInvoiceType);
                    $tempDriverRoutesClone->setStartTime($startTime);
                    $tempDriverRoutesClone->setEndTime($endTime);
                    $tempDriverRoutesClone->setOldDriver($driverRoute->getOldDriver());
                    $tempDriverRoutesClone->setOldDriverRoute($driverRoute->getOldDriverRoute());
                    $tempDriverRoutesClone->setIsBackup($driverRoute->getIsBackup());

                    // if (!empty($tempDriverRoutesClone->getPunchIn())) {
                    //     $tempDriverRoutesClone->setPunchIn($startTime);
                    // } else {
                    //     $tempDriverRoutesClone->setPunchIn($driverRoute->getPunchIn());
                    // }
                    $tempDriverRoutesClone->setPunchIn($driverRoute->getPunchIn());
                    // if (!empty($tempDriverRoutesClone->getPunchOut())) {
                    //     $tempDriverRoutesClone->setPunchOut($endTime);
                    // } else {
                    //     $tempDriverRoutesClone->setPunchOut($driverRoute->getPunchOut());
                    // }
                    $tempDriverRoutesClone->setPunchOut($driverRoute->getPunchOut());
                }
                $tempDriverRoutesClone->setRouteId($driverRoute);
                $tempDriverRoutesClone->setDateCreated(new DateTime($date));
                $tempDriverRoutesClone->setDriver($driver);
                $tempDriverRoutesClone->setIsNew(1);
                $tempDriverRoutesClone->setIsOpenShift(empty($driver));
                $tempDriverRoutesClone->setCreatedBy($userLogin);
                $tempDriverRoutesClone->setIsTemp($isTemp);
                $tempDriverRoutesClone->setAction($currentAction);
                $tempDriverRoutesClone->setCurrentNote($currentNote);
                $tempDriverRoutesClone->setNewNote($newNote);

                $this->getEntityManager()->persist($tempDriverRoutesClone);
                $this->getEntityManager()->flush();
                $ret = $tempDriverRoutesClone->getId();

                if (isset($criteria['isDrag'])) {
                    $this->getEntityManager()->getRepository('App\Entity\Shift')->dragShiftEventDataByObject($userLogin, (is_array($tempDriverRoute)) ? $tempDriverRoute[0]  : $tempDriverRoute, $tempDriverRoutesClone);
                } else {
                    $this->getEntityManager()->getRepository('App\Entity\Shift')->addShiftEventDataByObject($userLogin, $driverRoute, $tempDriverRoutesClone);
                }
            }
        }

        if (isset($criteria['newDate']) && !isset($criteria['weekTo'])) {
            $d = new DateTime($criteria['newDate']);
            $criteria['weekTo'] = $d->format('w');
        }

        if (isset($criteria['oldDate']) && !isset($criteria['weekFrom'])) {
            $d = new DateTime($criteria['oldDate']);
            $criteria['weekFrom'] = $d->format('w');
        }

        $data = [
            'to' => [
                'driverId' => $criteria['newDriverId'],
                'week' => $criteria['weekTo'],
            ],
            'hasAlert' => $criteria['hasAlert'],
        ];

        if (isset($criteria['weekFrom'])) {
            $data['from'] = [
                'driverId' => $criteria['oldDriverId'],
                'week' => $criteria['weekFrom'],
            ];

            if ( isset($criteria['indexSource']) )
                $data['from']['indexSource'] = $criteria['indexSource'];
            else
                $data['from']['routeId'] = $criteria['routeId'];
        }

        $datetime = $tempDriverRoutesClone->getDateCreated()->format('Y-m-d H:i:s');
        $week = date('W', strtotime($datetime));
        $entry = $this->globalUtils->getShiftData($tempDriverRoutesClone, $week, $requestTimeZone);

        $data['item'] = $entry;

        $event = $this->eventEmitter->create();
        $event
            ->setTopic([
                'company' => $tempDriverRoutesClone->getStation()->getCompany()->getId(),
                'station' => $entry['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEmitter($this->tokenStorage->getToken()->getUser()->getId())
            ->setEvent(isset($data['from']) ? 'MOVED' : 'ADDED')
            ->setData($data)
            ->dispatch();

        return $entry;
    }

    public function getStartAndEndDate($week, $year = null, $format = 'Y-m-d')
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }
        $dto = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $dto->setISODate($year, $week);
        $dto->modify('-1 days');
        $result['week_start'] = $dto->format($format);
        $dto->modify('+6 days');
        $result['week_end'] = $dto->format($format);
        return $result;
    }

    public function removeDriverRoute($tempDriverWeekDate, $driverWeekDate, $shift, $company, $limit)
    {
        $qb = $this->em->createQueryBuilder();

        $tempDriverRoute = $qb->select('dr.id')
            ->from('App\Entity\TempDriverRoute', 'tdr')
            ->leftJoin('tdr.routeId', 'dr')
            ->leftJoin('tdr.shiftType', 's')
            ->where('tdr.dateCreated = :dateCreated')
            ->setParameter('dateCreated', $tempDriverWeekDate)
            ->andWhere('tdr.shiftType = :shiftType')
            ->setParameter('shiftType', $shift)
            ->andWhere('s.company = :company')
            ->setParameter('company', $company)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 1)
            ->getQuery()
            ->getResult();
        $tempDriverRouteArr = array_column($tempDriverRoute, 'id');

        $qb1 = $this->em->createQueryBuilder();

        $qb1->delete('App\Entity\DriverRoute', 'dr')
            ->where('dr.dateCreated = :dateCreated')
            ->setParameter('dateCreated', $driverWeekDate)
            ->andWhere('dr.shiftType = :shiftType')
            ->setParameter('shiftType', $shift)
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.driver IS NULL')
            ->setMaxResults($limit);
        if (!empty($tempDriverRouteArr)) {
            $qb1->andWhere($qb1->expr()->notIn('dr.id', ':tempDriverRouteArray'))
                ->setParameter('tempDriverRouteArray', $tempDriverRouteArr);
        }
        $qb1->getQuery()->getResult();
    }

    public function getTempDriverRouteListForLoadoutPublishChanges($criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $currentDateObj = (new DateTime())->setTimezone(new DateTimeZone('UTC'));
        $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $currentDateObj->format('Y-m-d 00:00:00');

        $userLogin = $this->tokenStorage->getToken()->getUser();

        $results = $this->createQueryBuilder('tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->where('tdr.dateCreated = :currentDate')
            ->setParameter('currentDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('st.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('tdr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.createdBy is NULL OR tdr.isNew = 1')
            ->getQuery()
            ->getResult();

        $tempDriverRoutes = [];
        foreach ($results as $result) {
            $tempDriverRoutes[$result->getRouteId()->getId()][$result->getIsNew()][] = $result;
        }
        $resultTempRouteData = [];
        $count = 0;
        foreach ($tempDriverRoutes as $key => $tempDriverRoute) {
            if (!array_key_exists(1, $tempDriverRoute) || !array_key_exists(0, $tempDriverRoute)) {
                continue;
            }
            foreach ($tempDriverRoute[1] as $tempDriverData) {
                $oldTempRouteData = $tempDriverRoute[0][0];
                $role_name = explode('ROLE_', $userLogin->getRoles()[0]);
                $role = str_replace("_", " ", $role_name);
                if (isset($role[1])) {
                    $loginRole = ucwords(strtolower($role[1]));
                } else {
                    $loginRole = 'Driver';
                }

                if (((!empty($oldTempRouteData->getDriver()) and !empty($tempDriverData->getDriver())) and ($oldTempRouteData->getDriver()->getId() != $tempDriverData->getDriver()->getId())) or (empty($oldTempRouteData->getDriver()) and !empty($tempDriverData->getDriver()))) {
                    $resultTempRouteData[$count]['id'] = $tempDriverData->getId();
                    $resultTempRouteData[$count]['description'] = 'Driver (' . $tempDriverData->getDriver()->getFullName() . ') changed';
                    $resultTempRouteData[$count]['driver'] = [
                        'id' => $tempDriverData->getDriver()->getId(),
                        'name' => $tempDriverData->getDriver()->getFullName(),
                        'img' => $tempDriverData->getDriver()->getUser()->getProfileImage(),
                        'title' => $tempDriverData->getDriver()->getUser()->getUserRoles()[0]->getRoleName(),
                    ];
                    $resultTempRouteData[$count]['author'] = [
                        'id' => $userLogin->getId(),
                        'name' => $userLogin->getFirstName(),
                        'img' => $userLogin->getProfileImage(),
                        'title' => $loginRole,
                    ];
                } else {
                    $resultTempRouteData[$count]['id'] = $tempDriverData->getId();
                    $resultTempRouteData[$count]['description'] = 'Open Shift';
                    $resultTempRouteData[$count]['driver'] = [];
                    $resultTempRouteData[$count]['author'] = [
                        'id' => $userLogin->getId(),
                        'name' => $userLogin->getFirstName(),
                        'img' => $userLogin->getProfileImage(),
                        'title' => $loginRole,
                    ];
                }

                $count++;
            }
        }
        return array_values($resultTempRouteData);
    }

    public function removeDropUnpublishChangesForSchedulerLoadOut($criteria)
    {
        foreach ($criteria['tempDriverRouteId'] as $tempDriverRouteId) {
            $tempDriver = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->find($tempDriverRouteId);

            $results = $this->createQueryBuilder('tdr')
                ->where('tdr.routeId = :routeId')
                ->setParameter('routeId', $tempDriver->getRouteId()->getId())
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()->getResult();

            if ($results[0]->getIsBackup() == true) {
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->update('App\Entity\DriverRoute', 'dr')
                    ->set('dr.oldDriverRoute', 'NULL')
                    ->set('dr.isActive', true)
                    ->where('dr.id = :driverRouteId')
                    ->setParameter('driverRouteId', $tempDriver->getOldDriverRoute())
                    ->getQuery()
                    ->execute();
            }

            if ($results[0]->getIsTemp() == 1) {
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->delete('App\Entity\DriverRoute', 'dr')
                    ->where('dr.id = :routeId')
                    ->setParameter('routeId', $tempDriver->getRouteId()->getId())
                    ->getQuery()
                    ->execute();
            }

            if (count($results) == 1) {
                $this->createQueryBuilder('tdr')
                    ->delete()
                    ->where('tdr.routeId = :routeId')
                    ->setParameter('routeId', $tempDriver->getRouteId()->getId())
                    ->andWhere('tdr.isNew = :isNew')
                    ->setParameter('isNew', 0)
                    ->andWhere('tdr.createdBy IS NULL')
                    ->getQuery()
                    ->execute();
            }
            $this->getEntityManager()->remove($tempDriver);
            $this->getEntityManager()->flush();
        }
        return true;
    }

    public function insertUpdateRecordForSchedulerLoadoutAddDriver($driverRouteResult, $userLogin, $driverId)
    {
        $driverRoute = $driverRouteResult[0];
        $tempDriverRouteNew = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->findOneBy(['routeId' => $driverRoute->getId(), 'isNew' => 1]);
        $updateDriver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);

        if (!empty($tempDriverRouteNew)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\TempDriverRoute', 'tdr')
                ->set('tdr.driver', $driverId)
                ->where('tdr.routeId = :routeId')
                ->setParameter('routeId', $tempDriverRouteNew->getRouteId()->getId())
                ->andWhere('tdr.isNew = :isNew')
                ->setParameter('isNew', 1)
                ->getQuery()
                ->execute();
        } else {
            $tempDriverRouteNew = new TempDriverRoute();
            $tempDriverRouteNew->setDateCreated($driverRoute->getDateCreated());
            $tempDriverRouteNew->setSkill($driverRoute->getSkill());
            $tempDriverRouteNew->setRouteId($driverRoute);
            $tempDriverRouteNew->setDriver($updateDriver);
            $tempDriverRouteNew->setStation($driverRoute->getStation());
            $tempDriverRouteNew->setPunchIn($driverRoute->getPunchIn());
            $tempDriverRouteNew->setPunchOut($driverRoute->getPunchOut());
            $tempDriverRouteNew->setBreakPunchOut($driverRoute->getBreakPunchOut());
            $tempDriverRouteNew->setBreakPunchIn($driverRoute->getBreakPunchIn());
            $tempDriverRouteNew->setStatus($driverRoute->getStatus());
            $tempDriverRouteNew->setRouteCode($driverRoute->getRouteCode());
            $tempDriverRouteNew->setIsRescuer($driverRoute->getIsRescuer());
            $tempDriverRouteNew->setIsRescued($driverRoute->getIsRescued());
            $tempDriverRouteNew->setShiftType($driverRoute->getShiftType());
            $tempDriverRouteNew->setSchedule($driverRoute->getSchedule());
            $tempDriverRouteNew->setIsNew(1);
            $tempDriverRouteNew->setIsLoadOut(1);
            $tempDriverRouteNew->setCreatedBy($userLogin);
            //new fields
            $tempDriverRouteNew->setOldDriver($driverRoute->getOldDriver());
            $tempDriverRouteNew->setOldDriverRoute($driverRoute->getOldDriverRoute());
            $tempDriverRouteNew->setIsBackup($driverRoute->getIsBackup());
            $tempDriverRouteNew->setIsTemp($driverRoute->getIsTemp());
            $tempDriverRouteNew->setIsOpenShift($driverRoute->getIsOpenShift());
            $tempDriverRouteNew->setShiftName($driverRoute->getShiftType()->getName());
            $tempDriverRouteNew->setShiftColor($driverRoute->getShiftType()->getColor());
            $tempDriverRouteNew->setUnpaidBreak($driverRoute->getShiftType()->getUnpaidBreak());
            $tempDriverRouteNew->setShiftNote($driverRoute->getShiftType()->getNote());
            $tempDriverRouteNew->setShiftInvoiceType($driverRoute->getShiftType()->getInvoiceType());
            $tempDriverRouteNew->setShiftStation($driverRoute->getShiftType()->getStation());
            $tempDriverRouteNew->setShiftSkill($driverRoute->getShiftType()->getSkill());
            $tempDriverRouteNew->setShiftCategory($driverRoute->getShiftType()->getCategory());
            $tempDriverRouteNew->setShiftBalanceGroup($driverRoute->getShiftType()->getBalanceGroup());
            $tempDriverRouteNew->setShiftTextColor($driverRoute->getShiftType()->getTextColor());
            $tempDriverRouteNew->setStartTime($driverRoute->getShiftType()->getStartTime());
            $tempDriverRouteNew->setEndTime($driverRoute->getShiftType()->getEndTime());

            $this->getEntityManager()->persist($tempDriverRouteNew);
            $this->getEntityManager()->flush();
        }
        $tempDriverRouteOld = $this->getTempDriverRouteByRouteId($driverRoute->getId());
        if (empty($tempDriverRouteOld)) {
            $tempDriverRouteOld = new TempDriverRoute();
            $tempDriverRouteOld->setDateCreated($driverRoute->getDateCreated());
            $tempDriverRouteOld->setSkill($driverRoute->getSkill());
            $tempDriverRouteOld->setRouteId($driverRoute);
            $tempDriverRouteOld->setDriver($driverRoute->getDriver());
            $tempDriverRouteOld->setStation($driverRoute->getStation());
            $tempDriverRouteOld->setPunchIn($driverRoute->getPunchIn());
            $tempDriverRouteOld->setPunchOut($driverRoute->getPunchOut());
            $tempDriverRouteOld->setBreakPunchOut($driverRoute->getBreakPunchOut());
            $tempDriverRouteOld->setBreakPunchIn($driverRoute->getBreakPunchIn());
            $tempDriverRouteOld->setStatus($driverRoute->getStatus());
            $tempDriverRouteOld->setRouteCode($driverRoute->getRouteCode());
            $tempDriverRouteOld->setIsRescuer($driverRoute->getIsRescuer());
            $tempDriverRouteOld->setIsRescued($driverRoute->getIsRescued());
            $tempDriverRouteOld->setShiftType($driverRoute->getShiftType());
            $tempDriverRouteOld->setSchedule($driverRoute->getSchedule());
            //new fields
            $tempDriverRouteOld->setOldDriver($driverRoute->getOldDriver());
            $tempDriverRouteOld->setOldDriverRoute($driverRoute->getOldDriverRoute());
            $tempDriverRouteOld->setIsBackup($driverRoute->getIsBackup());
            $tempDriverRouteOld->setIsTemp($driverRoute->getIsTemp());
            $tempDriverRouteOld->setIsOpenShift($driverRoute->getIsOpenShift());
            $tempDriverRouteOld->setShiftName($driverRoute->getShiftType()->getName());
            $tempDriverRouteOld->setShiftColor($driverRoute->getShiftType()->getColor());
            $tempDriverRouteOld->setUnpaidBreak($driverRoute->getShiftType()->getUnpaidBreak());
            $tempDriverRouteOld->setShiftNote($driverRoute->getShiftType()->getNote());
            $tempDriverRouteOld->setShiftInvoiceType($driverRoute->getShiftType()->getInvoiceType());
            $tempDriverRouteOld->setShiftStation($driverRoute->getShiftType()->getStation());
            $tempDriverRouteOld->setShiftSkill($driverRoute->getShiftType()->getSkill());
            $tempDriverRouteOld->setShiftCategory($driverRoute->getShiftType()->getCategory());
            $tempDriverRouteOld->setShiftBalanceGroup($driverRoute->getShiftType()->getBalanceGroup());
            $tempDriverRouteOld->setShiftTextColor($driverRoute->getShiftType()->getTextColor());
            $tempDriverRouteOld->setStartTime($driverRoute->getShiftType()->getStartTime());
            $tempDriverRouteOld->setEndTime($driverRoute->getShiftType()->getEndTime());
            $tempDriverRouteOld->setIsNew(0);
            $this->getEntityManager()->persist($tempDriverRouteOld);
            $this->getEntityManager()->flush();
        }
    }

    public function insertRecordForSchedulerLoadoutAddDriverInDriverAndTempDriver($shift, $userLogin, $date, $driverId, $shiftStartTime, $shiftEndTime, $invoiceType)
    {
        $currentDate = $date->format('Y-m-d 00:00:00');

        $startTime = new DateTime($date->format('Y-m-d ' . $shiftStartTime . ':00'));
        $endTime = new DateTime($date->format('Y-m-d ' . $shiftEndTime . ':00'));

        $updateDriver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driverId);
        $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($shift);
        $invoice = null;
        if (!empty($invoiceType)) {
            $invoice = $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($invoiceType);
        }
        $driverRouteNew = new DriverRoute();
        $driverRouteNew->setDateCreated($date);
        $driverRouteNew->setStation($shift->getStation());
        $driverRouteNew->setShiftType($shift);
        $driverRouteNew->setSkill($shift->getSkill());
        $driverRouteNew->setIsActive(1);
        $driverRouteNew->setIsOpenShift(true);
        $driverRouteNew->setIsTemp(1);
        $driverRouteNew->setShiftName($shift->getName());
        $driverRouteNew->setShiftColor($shift->getColor());
        $driverRouteNew->setUnpaidBreak($shift->getUnpaidBreak());
        $driverRouteNew->setShiftNote($shift->getNote());
        $driverRouteNew->setShiftStation($shift->getStation());
        $driverRouteNew->setShiftSkill($shift->getSkill());
        $driverRouteNew->setShiftCategory($shift->getCategory());
        $driverRouteNew->setShiftBalanceGroup($shift->getBalanceGroup());
        $driverRouteNew->setShiftTextColor($shift->getTextColor());
        $driverRouteNew->setStartTime($startTime);
        $driverRouteNew->setEndTime($endTime);
        $driverRouteNew->setShiftInvoiceType($invoice);
        $driverRouteNew->setIsRescuer(false);
        $driverRouteNew->setIsRescued(false);
        $driverRouteNew->setIsLoadOut(true);
        $driverRouteNew->setIsBackup(false);
        $this->getEntityManager()->persist($driverRouteNew);
        $this->getEntityManager()->flush($driverRouteNew);

        $tempDriverRouteLatest = new TempDriverRoute();
        $tempDriverRouteLatest->setDateCreated(new DateTime($currentDate));
        $tempDriverRouteLatest->setSkill($driverRouteNew->getSkill());
        $tempDriverRouteLatest->setRouteId($driverRouteNew);
        $tempDriverRouteLatest->setDriver($driverRouteNew->getDriver());
        $tempDriverRouteLatest->setStation($driverRouteNew->getStation());
        $tempDriverRouteLatest->setPunchIn($driverRouteNew->getPunchIn());
        $tempDriverRouteLatest->setPunchOut($driverRouteNew->getPunchOut());
        $tempDriverRouteLatest->setBreakPunchOut($driverRouteNew->getBreakPunchOut());
        $tempDriverRouteLatest->setBreakPunchIn($driverRouteNew->getBreakPunchIn());
        $tempDriverRouteLatest->setStatus($driverRouteNew->getStatus());
        $tempDriverRouteLatest->setRouteCode($driverRouteNew->getRouteCode());
        $tempDriverRouteLatest->setIsRescuer($driverRouteNew->getIsRescuer());
        $tempDriverRouteLatest->setIsRescued($driverRouteNew->getIsRescued());
        $tempDriverRouteLatest->setShiftType($driverRouteNew->getShiftType());
        $tempDriverRouteLatest->setSchedule($driverRouteNew->getSchedule());
        //new fields
        $tempDriverRouteLatest->setOldDriver($driverRouteNew->getOldDriver());
        $tempDriverRouteLatest->setOldDriverRoute($driverRouteNew->getOldDriverRoute());
        $tempDriverRouteLatest->setIsBackup($driverRouteNew->getIsBackup());
        $tempDriverRouteLatest->setIsTemp($driverRouteNew->getIsTemp());
        $tempDriverRouteLatest->setShiftName($driverRouteNew->getShiftType()->getName());
        $tempDriverRouteLatest->setShiftColor($driverRouteNew->getShiftType()->getColor());
        $tempDriverRouteLatest->setUnpaidBreak($driverRouteNew->getShiftType()->getUnpaidBreak());
        $tempDriverRouteLatest->setShiftNote($driverRouteNew->getShiftType()->getNote());
        //$tempDriverRouteLatest->setShiftInvoiceType($driverRouteNew->getShiftType()->getInvoiceType());
        $tempDriverRouteLatest->setShiftStation($driverRouteNew->getShiftType()->getStation());
        $tempDriverRouteLatest->setShiftSkill($driverRouteNew->getShiftType()->getSkill());
        $tempDriverRouteLatest->setShiftCategory($driverRouteNew->getShiftType()->getCategory());
        $tempDriverRouteLatest->setShiftBalanceGroup($driverRouteNew->getShiftType()->getBalanceGroup());
        $tempDriverRouteLatest->setShiftTextColor($driverRouteNew->getShiftType()->getTextColor());
        $tempDriverRouteLatest->setStartTime($driverRouteNew->getStartTime());
        $tempDriverRouteLatest->setEndTime($driverRouteNew->getEndTime());
        $tempDriverRouteLatest->setShiftInvoiceType($driverRouteNew->getShiftInvoiceType());
        $tempDriverRouteLatest->setIsNew(0);
        $tempDriverRouteLatest->setIsOpenShift(true);
        $this->getEntityManager()->persist($tempDriverRouteLatest);
        //$this->getEntityManager()->flush();

        $tempDriverRouteLatestIsNew = new TempDriverRoute();
        $tempDriverRouteLatestIsNew->setDateCreated(new DateTime($currentDate));
        $tempDriverRouteLatestIsNew->setSkill($driverRouteNew->getSkill());
        $tempDriverRouteLatestIsNew->setRouteId($driverRouteNew);
        $tempDriverRouteLatestIsNew->setDriver($updateDriver);
        $tempDriverRouteLatestIsNew->setStation($driverRouteNew->getStation());
        $tempDriverRouteLatestIsNew->setPunchIn($driverRouteNew->getPunchIn());
        $tempDriverRouteLatestIsNew->setPunchOut($driverRouteNew->getPunchOut());
        $tempDriverRouteLatestIsNew->setBreakPunchOut($driverRouteNew->getBreakPunchOut());
        $tempDriverRouteLatestIsNew->setBreakPunchIn($driverRouteNew->getBreakPunchIn());
        $tempDriverRouteLatestIsNew->setStatus($driverRouteNew->getStatus());
        $tempDriverRouteLatestIsNew->setRouteCode($driverRouteNew->getRouteCode());
        $tempDriverRouteLatestIsNew->setIsRescuer($driverRouteNew->getIsRescuer());
        $tempDriverRouteLatestIsNew->setIsRescued($driverRouteNew->getIsRescued());
        $tempDriverRouteLatestIsNew->setShiftType($driverRouteNew->getShiftType());
        $tempDriverRouteLatestIsNew->setSchedule($driverRouteNew->getSchedule());
        //new fields
        $tempDriverRouteLatestIsNew->setOldDriver($driverRouteNew->getOldDriver());
        $tempDriverRouteLatestIsNew->setOldDriverRoute($driverRouteNew->getOldDriverRoute());
        $tempDriverRouteLatestIsNew->setIsBackup($driverRouteNew->getIsBackup());
        $tempDriverRouteLatestIsNew->setIsTemp($driverRouteNew->getIsTemp());
        $tempDriverRouteLatestIsNew->setShiftName($driverRouteNew->getShiftType()->getName());
        $tempDriverRouteLatestIsNew->setShiftColor($driverRouteNew->getShiftType()->getColor());
        $tempDriverRouteLatestIsNew->setUnpaidBreak($driverRouteNew->getShiftType()->getUnpaidBreak());
        $tempDriverRouteLatestIsNew->setShiftNote($driverRouteNew->getShiftType()->getNote());
        //$tempDriverRouteLatestIsNew->setShiftInvoiceType($driverRouteNew->getShiftType()->getInvoiceType());
        $tempDriverRouteLatestIsNew->setShiftStation($driverRouteNew->getShiftType()->getStation());
        $tempDriverRouteLatestIsNew->setShiftSkill($driverRouteNew->getShiftType()->getSkill());
        $tempDriverRouteLatestIsNew->setShiftCategory($driverRouteNew->getShiftType()->getCategory());
        $tempDriverRouteLatestIsNew->setShiftBalanceGroup($driverRouteNew->getShiftType()->getBalanceGroup());
        $tempDriverRouteLatestIsNew->setShiftTextColor($driverRouteNew->getShiftType()->getTextColor());
        $tempDriverRouteLatestIsNew->setStartTime($driverRouteNew->getStartTime());
        $tempDriverRouteLatestIsNew->setEndTime($driverRouteNew->getEndTime());
        $tempDriverRouteLatestIsNew->setShiftInvoiceType($driverRouteNew->getShiftInvoiceType());
        $tempDriverRouteLatestIsNew->setIsNew(1);
        $tempDriverRouteLatestIsNew->setIsLoadOut(1);
        $tempDriverRouteLatestIsNew->setIsOpenShift(false);
        $tempDriverRouteLatestIsNew->setCreatedBy($userLogin);
        $tempDriverRouteLatestIsNew->setAction('SHIFT_ADDED');
        $this->getEntityManager()->persist($tempDriverRouteLatestIsNew);
        $this->getEntityManager()->flush();

        return [
            'driverRoute' => $driverRouteNew,
            'tempDriverRoute' => $tempDriverRouteLatest,
            'tempDriverRouteIsNew' => $tempDriverRouteLatestIsNew,
        ];
    }

    public function getUpdatedDriverRouteBySchedule($scheduleId)
    {
        $results = $this->createQueryBuilder('tdr')
            ->where('tdr.schedule = :schedule')
            ->setParameter('schedule', $scheduleId)
            ->andWhere('tdr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.isNew = :isNew')
            ->setParameter('isNew', 0)
            ->getQuery()
            ->getResult();

        $updatedDriverRoutes = [];

        if (count($results) > 0) {
            foreach ($results as $result) {
                $updatedDriverRoutes[$result->getRouteId()->getId()] = $result->getRouteId()->getId();
            }
        }

        $updatedDriverRoutes = array_values($updatedDriverRoutes);

        return $updatedDriverRoutes;
    }

    public function editTempDriverRoute($criteria)
    {
        $data = $criteria['shift'];
        $tempDriverRoute = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->find($criteria['id']);
        $shift = $this->getEntityManager()->getRepository('App\Entity\Shift')->find($data['shiftType']);
        $userLogin = $this->tokenStorage->getToken()->getUser();

        if (isset($criteria['newDriverId'])) {
            $driver = $criteria['newDriverId'] === '0.0.0'
                ? null
                : $criteria['newDriverId'];

            if ($driver) {
                $driver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($driver);
            }

            $tempDriverRoute->setDriver($driver);
        }

        $note = $newNote = $currentNote = '';
        if (isset($data['note'])) {
            $note = $currentNote = $data['note'];
        } else {
            $note = $currentNote = $shift->getNote();
        }

        if (isset($data['newNote'])) {
            $newNote = $data['newNote'];
            $note = $note . "\n\r" . $data['newNote'];
        }

        $tempDriverRoute->setAction('SHIFT_EDITED');
        $tempDriverRoute->setShiftNote($note);
        $tempDriverRoute->setShiftType($shift);

        $utcNow = GlobalUtility::newUTCDateTime();

        if (isset($data['startTime'])) {
            $startTimeObj = GlobalUtility::newUTCDateTime($utcNow->format('Y-m-d ' . $data['startTime'] . ':00'));
            $tempDriverRoute->setStartTime($startTimeObj);
        }

        if (isset($data['endTime'])) {
            $endTimeObj = GlobalUtility::newUTCDateTime($utcNow->format('Y-m-d ' . $data['endTime'] . ':00'));
            $tempDriverRoute->setEndTime($endTimeObj);
        }

        $tempDriverRoute->setShiftName($shift->getName());
        $tempDriverRoute->setShiftColor($shift->getColor());
        $tempDriverRoute->setUnpaidBreak($shift->getUnpaidBreak());

        if (isset($data['shiftInvoiceType']))
            if ($data['shiftInvoiceType'] == 0) {
                $tempDriverRoute->setShiftInvoiceType(null);
            } else {
                $tempDriverRoute->setShiftInvoiceType($this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($data['shiftInvoiceType']));
            }

        $tempDriverRoute->setShiftStation($shift->getStation());
        $tempDriverRoute->setShiftSkill($shift->getSkill());
        $tempDriverRoute->setShiftCategory($shift->getCategory());
        $tempDriverRoute->setShiftBalanceGroup($shift->getBalanceGroup());
        $tempDriverRoute->setShiftTextColor($shift->getTextColor());

        $tempDriverRoute->setCurrentNote($currentNote);
        $tempDriverRoute->setNewNote($newNote);

        $this->getEntityManager()->persist($tempDriverRoute);
        $uow = $this->getEntityManager()->getUnitOfWork();
        $uow->computeChangeSets();
        $shiftDiffrances = $uow->getEntityChangeSet($tempDriverRoute);
        if (!empty($shiftDiffrances)) {
            $this->getEntityManager()->getRepository('App\Entity\Shift')->addShiftEventData($shiftDiffrances,$shift, $userLogin, $tempDriverRoute->getRouteId());
        }
        $this->getEntityManager()->flush();

        $datetime = $tempDriverRoute->getDateCreated()->format('Y-m-d H:i:s');
        $week = date('W', strtotime($datetime));
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $entry = $this->globalUtils->getShiftData($tempDriverRoute, $week, $requestTimeZone);

        $data = [
            'from' => [
                'driverId' => $criteria['oldDriverId'],
                'week' => (new DateTime($criteria['oldDate']))->format('w'),
                'routeId' => $tempDriverRoute->getId(),
            ],
            'to' => [
                'driverId' => $criteria['newDriverId'],
                'week' => (new DateTime($criteria['newDate']))->format('w'),
            ],
            'hasAlert' => true,
            'item' => $entry
        ];

        $event = $this->eventEmitter->create();
        $event
            ->setTopic([
                'company' => $shift->getCompany()->getId(),
                'station' => $entry['stationId'],
                'topic' => 'scheduler'
            ])
            ->setEmitter($this->tokenStorage->getToken()->getUser()->getId())
            ->setEvent('MOVED')
            ->setData($data)
            ->dispatch();

        return $entry;

    }

    public function updateBalanceGroupNullInTempDriverRoute($balanceGroupId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\TempDriverRoute', 'tdr')
            ->set('tdr.shiftBalanceGroup', 'NULL');
        if (is_array($balanceGroupId)) {
            $qb->where($qb->expr()->in('tdr.shiftBalanceGroup', ':balanceGroupId'))
                ->setParameter('balanceGroupId', $balanceGroupId);
        } else {
            $qb->where('tdr.shiftBalanceGroup = :balanceGroup')
                ->setParameter('balanceGroup', $balanceGroupId);
        }
        $qb->getQuery()
            ->execute();
        return true;
    }


    /**
     * @param $action
     * @param $oldRoute
     * @param $newRoute
     * @param $newDriverId
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveTempDriverRoute($action,$isOpenRoute,$oldRoute=null,$newRoute=null,$newDriverId=null)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        if( $oldRoute ){
            $oldDriver = $oldRoute->getDriver();
            $oldCreatedDate = $oldRoute->getDateCreated();
        }  else {
            $oldDriver=null;
            $oldCreatedDate = $newRoute->getDateCreated();
        }
        if( $newRoute ){
            $newDriver = $newRoute->getDriver();
            $newCreatedDate = $newRoute->getDateCreated();
        }  else {
            $newDriver=null;
            $newCreatedDate = $oldRoute->getDateCreated();
        }
        if( $oldRoute && $newRoute ){
            $newCreatedDate = $oldRoute->getDateCreated();
        }
        if($newDriverId)   {
            $oldDriver=null;
            $newDriver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($newDriverId);
        }
        for ($i=0; $i <=1 ; $i++) {
            $tempDriverRoute = new TempDriverRoute();
            $tempDriverRoute->setDateCreated($i==0 ? $oldCreatedDate : $newCreatedDate );
            $tempDriverRoute->setSkill($oldRoute->getSkill());
            $tempDriverRoute->setRouteId($oldRoute);
            $tempDriverRoute->setDriver($i==0 ? $oldDriver : $newDriver );
            $tempDriverRoute->setStation($oldRoute->getStation());
            $tempDriverRoute->setPunchIn($oldRoute->getPunchIn());
            $tempDriverRoute->setPunchOut($oldRoute->getPunchOut());
            $tempDriverRoute->setBreakPunchOut($oldRoute->getBreakPunchOut());
            $tempDriverRoute->setBreakPunchIn($oldRoute->getBreakPunchIn());
            $tempDriverRoute->setStatus($oldRoute->getStatus());
            $tempDriverRoute->setRouteCode($oldRoute->getRouteCode());
            $tempDriverRoute->setIsRescuer($oldRoute->getIsRescuer());
            $tempDriverRoute->setIsRescued($oldRoute->getIsRescued());
            $tempDriverRoute->setShiftType($oldRoute->getShiftType());
            $tempDriverRoute->setSchedule($oldRoute->getSchedule());
            $tempDriverRoute->setIsOpenShift( $isOpenRoute && $i== 1 ? true : false );
            $tempDriverRoute->setIsNew($i);
            $tempDriverRoute->setIsTemp(0);
            $tempDriverRoute->setShiftName($oldRoute->getShiftName());
            $tempDriverRoute->setShiftColor($oldRoute->getShiftColor());
            $tempDriverRoute->setUnpaidBreak($oldRoute->getUnpaidBreak());
            $tempDriverRoute->setShiftNote($oldRoute->getShiftNote());
            $tempDriverRoute->setShiftInvoiceType($oldRoute->getShiftInvoiceType());
            $tempDriverRoute->setShiftStation($oldRoute->getShiftStation());
            $tempDriverRoute->setShiftSkill($oldRoute->getShiftSkill());
            $tempDriverRoute->setShiftCategory($oldRoute->getShiftCategory());
            $tempDriverRoute->setShiftBalanceGroup($oldRoute->getShiftBalanceGroup());
            $tempDriverRoute->setShiftTextColor($oldRoute->getShiftTextColor());
            $tempDriverRoute->setStartTime($oldRoute->getStartTime());
            $tempDriverRoute->setEndTime($oldRoute->getEndTime());
            $tempDriverRoute->setOldDriver($oldRoute->getOldDriver());
            $tempDriverRoute->setOldDriverRoute($oldRoute->getOldDriverRoute());
            $tempDriverRoute->setIsBackup($oldRoute->getIsBackup());
            $tempDriverRoute->setAction($i==1 ? $action: null );
            $tempDriverRoute->setCreatedBy($userLogin);

            $this->getEntityManager()->persist($tempDriverRoute);
            $this->getEntityManager()->flush($tempDriverRoute);

        }
        return true;
    }


    public function getTempDriverRouteIds($driverId)
    {
        $currDate = new \DateTime();
        $currDate = $currDate->setTimezone(new \DateTimeZone('UTC'));
        $currDate->setTime(0, 0, 0);
        if ($currDate->format('l') == 'Sunday') {
            $week = $currDate->format('W') + 1;
            $timeStamp = strtotime($currDate->format('Y-m-d').' +1 day');
        } else {
            $week = $currDate->format('W');
            $timeStamp = strtotime($currDate->format('Y-m-d'));
        }
        $year = date("Y", strtotime('this week', $timeStamp));
        $weekStartEndDay = $this->getStartAndEndDate($week, $year);
        $results = $this->createQueryBuilder('tdr')
            ->select('dr.id')
            ->leftJoin('tdr.routeId','dr')
            ->leftJoin('tdr.driver','dri')
            ->where('dri.id = :driverId')
            ->setParameter('driverId',$driverId)
            ->andWhere('dr.dateCreated >= :sunday AND dr.dateCreated <= :saturday ')
            ->setParameter('sunday', $weekStartEndDay['week_start'])
            ->setParameter('saturday', $weekStartEndDay['week_end'])
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return array_column($results, 'id');
    }

    //Common api call day,week and loadout disapprove
    public function removeDropRequest($criteria){
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['id']);
        if($driverRoute){
            $request = $driverRoute->getRequestForRoute();
            $request->setIsArchive(true);
            $driverRoute->setRequestForRoute(null);
            $driverRoute->setDriver($request->getOldDriver());
            $driverRoute->setIsOpenShift(0);
            $this->getEntityManager()->persist($driverRoute);
            $this->getEntityManager()->persist($request);
            $this->getEntityManager()->flush();
        }
        if (isset($criteria['isLoadOut']) && $criteria['isLoadOut'] == true) {
            return $this->globalUtils->getLoadoutPayloadData($driverRoute, $nCount = 0, $nNotDepartedCount = 0, $nNotPunchInCount = 0, $requestTimeZone);
        } else {
            return $this->globalUtils->getShiftData($driverRoute, $criteria['wknumber'], $requestTimeZone);
        }
    }

    /**
     * @param DriverRoute $oldRoute
     * @param DriverRoute $newRoute
     * @param Driver $newDriver
     * @param Driver $oldDriver
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateTempDriverRoute(DriverRoute $oldRoute, DriverRoute $newRoute, Driver $newDriver, Driver $oldDriver)
    {
        /** @var User $userLogin */
        $userLogin = $this->tokenStorage->getToken()->getUser();

        for ($i = 0; $i <= 1; $i++) {
            $tempDriverRoute = new TempDriverRoute();
            $tempDriverRoute->setDateCreated($i == 0 ? $oldRoute->getDateCreated() : $newRoute->getDateCreated());
            $tempDriverRoute->setSkill($i == 0 ? $oldRoute->getSkill() : $newRoute->getSkill());
            $tempDriverRoute->setRouteId($i == 0 ? $oldRoute : $newRoute);
            $tempDriverRoute->setDriver($i == 0 ? $newDriver : $oldDriver);
            $tempDriverRoute->setStation($i == 0 ? $oldRoute->getStation() : $newRoute->getStation());
            $tempDriverRoute->setPunchIn($i == 0 ? $oldRoute->getPunchIn() : $newRoute->getPunchIn());
            $tempDriverRoute->setPunchOut($i == 0 ? $oldRoute->getPunchOut() : $newRoute->getPunchOut());
            $tempDriverRoute->setBreakPunchOut($i == 0 ? $oldRoute->getBreakPunchOut() : $newRoute->getBreakPunchOut());
            $tempDriverRoute->setBreakPunchIn($i == 0 ? $oldRoute->getBreakPunchIn() : $newRoute->getBreakPunchIn());
            $tempDriverRoute->setStatus($i == 0 ? $oldRoute->getStatus() : $newRoute->getStatus());
            $tempDriverRoute->setRouteCode($i == 0 ? $oldRoute->getRouteCode() : $newRoute->getRouteCode());
            $tempDriverRoute->setIsRescuer($i == 0 ? $oldRoute->getIsRescuer() : $newRoute->getIsRescuer());
            $tempDriverRoute->setIsRescued($i == 0 ? $oldRoute->getIsRescued() : $newRoute->getIsRescued());
            $tempDriverRoute->setShiftType($i == 0 ? $oldRoute->getShiftType() : $newRoute->getShiftType());
            $tempDriverRoute->setSchedule($i == 0 ? $oldRoute->getSchedule() : $newRoute->getSchedule());
            $tempDriverRoute->setIsOpenShift($i == 0 ? $oldRoute->getIsOpenShift() : $newRoute->getIsOpenShift());
            $tempDriverRoute->setIsNew(1);
            $tempDriverRoute->setIsTemp(1);
            $tempDriverRoute->setShiftName($i == 0 ? $oldRoute->getShiftName() : $newRoute->getShiftName());
            $tempDriverRoute->setShiftColor($i == 0 ? $oldRoute->getShiftColor() : $newRoute->getShiftColor());
            $tempDriverRoute->setUnpaidBreak($i == 0 ? $oldRoute->getUnpaidBreak() : $newRoute->getUnpaidBreak());
            $tempDriverRoute->setShiftNote($i == 0 ? $oldRoute->getShiftNote() : $newRoute->getShiftNote());
            $tempDriverRoute->setShiftInvoiceType($i == 0 ? $oldRoute->getShiftInvoiceType() : $newRoute->getShiftInvoiceType());
            $tempDriverRoute->setShiftStation($i == 0 ? $oldRoute->getShiftStation() : $newRoute->getShiftStation());
            $tempDriverRoute->setShiftSkill($i == 0 ? $oldRoute->getShiftSkill() : $newRoute->getShiftSkill());
            $tempDriverRoute->setShiftCategory($i == 0 ? $oldRoute->getShiftCategory() : $newRoute->getShiftCategory());
            $tempDriverRoute->setShiftBalanceGroup($i == 0 ? $oldRoute->getShiftBalanceGroup() : $newRoute->getShiftBalanceGroup());
            $tempDriverRoute->setShiftTextColor($i == 0 ? $oldRoute->getShiftTextColor() : $newRoute->getShiftTextColor());
            $tempDriverRoute->setStartTime($i == 0 ? $oldRoute->getStartTime() : $newRoute->getStartTime());
            $tempDriverRoute->setEndTime($i == 0 ? $oldRoute->getEndTime() : $newRoute->getEndTime());
            $tempDriverRoute->setOldDriver($i == 0 ? $oldRoute->getOldDriver() : $newRoute->getOldDriver());
            $tempDriverRoute->setOldDriverRoute($i == 0 ? $oldRoute->getOldDriverRoute() : $newRoute->getOldDriverRoute());
            $tempDriverRoute->setIsBackup($i == 0 ? $oldRoute->getIsBackup() : $newRoute->getIsBackup());
            $tempDriverRoute->setAction('SHIFT_MOVED');
            $tempDriverRoute->setCreatedBy($userLogin);

            $this->getEntityManager()->persist($tempDriverRoute);
            $this->getEntityManager()->flush($tempDriverRoute);

        }
        return true;
    }

    public function deleteTempRouteWhereIsNewOne($stationId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23,59,59);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->delete('App\Entity\TempDriverRoute', 'tdr')
            ->where('tdr.station = :stationId')
            ->setParameter('stationId', $stationId)
            ->andWhere('tdr.isNew = 1')
            ->andWhere('tdr.dateCreated > :today')
            ->setParameter('today',$today)
            ->getQuery()
            ->execute();
        return true;
    }
}
