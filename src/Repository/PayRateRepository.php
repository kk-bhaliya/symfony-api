<?php

namespace App\Repository;

use App\Entity\PayRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PayRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayRate[]    findAll()
 * @method PayRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PayRate::class);
    }

    public function updateSkillNullInPayRate($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\PayRate','sk')
                        ->set('sk.skill', 'NULL')
                        ->where('sk.skill = :skillId')
                        ->setParameter('skillId', $skillId)
                        ->getQuery()
                        ->execute();
        return true;
    }
}
