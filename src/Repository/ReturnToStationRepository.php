<?php

namespace App\Repository;

use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\ReturnToStation;
use App\Entity\Vehicle;
use App\Utils\GlobalUtility;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method ReturnToStation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnToStation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnToStation[]    findAll()
 * @method ReturnToStation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnToStationRepository extends ServiceEntityRepository
{
    /**
     * @var GlobalUtility
     */
    private $globalUtils;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * ReturnToStationRepository constructor.
     * @param ManagerRegistry $registry
     * @param GlobalUtility $globalUtils
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(ManagerRegistry $registry, GlobalUtility $globalUtils, EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        parent::__construct($registry, ReturnToStation::class);
        $this->globalUtils = $globalUtils;
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function getReturnToStationByRouteId($routeId)
    {
       return $this->createQueryBuilder('rs')
              ->select('rs.id,rs.packagesMissing,rs.packagesReturning')
              ->where('rs.driverRoute = :driverRoute')
              ->setParameter('driverRoute', $routeId)
              ->getQuery()
              ->getScalarResult();
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function validateStationCheck(array $criteria)
    {
        if (empty($criteria['id'])) {
            throw new Exception('RTS Id missing.');
        }
        $entity = $this->find($criteria['id']);
        $entity->setStationCheckListResult($criteria['stationCheckListResult']);
        if (isset($criteria['loggedStationCheckAt'])) {
            $dateTime = new DateTime($criteria['loggedStationCheckAt']);
            $entity->setLoggedStationCheckAt($dateTime);
        }
        $entity->setPackagesReturning($criteria['packagesReturning']);
        $entity->setPackagesMissing($criteria['packagesMissing']);
        $this->_em->flush();
        return [
            'success' => true,
            'id' => $entity->getId()
        ];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function updateMileage(array $criteria)
    {
        $entity = $this->globalUtils->validateFieldsSetMileage($criteria, 'ReturnToStation');
        $driverRoute = $entity->getDriverRoute();
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.');
        }
        $device = $this->globalUtils->getDevice($criteria);
        $user = $this->globalUtils->getTokenUser();
        $messageBy = ' by ' . $user->getFriendlyName() . '.';
        $messageMileage = 'Mileage at RTS is ' . $criteria['mileage'] . ' miles' . $messageBy;
        $messageGasTankLevel = 'Gas Tank Level at RTS is ' . $criteria['gasTankLevel'] . $messageBy;

        $vehicle = $driverRoute->getVehicle();
        if (($vehicle instanceof Vehicle)) {
            $mileageEvent = $this->em->getRepository(Event::class)->createEvent('MILEAGE', 'Mileage at RTS.', $messageMileage, null, $driverRoute, null, $device, $user);
            $gasTankLevelEvent = $this->em->getRepository(Event::class)->createEvent('GAS', 'Gas Tank Level at RTS.', $messageGasTankLevel, null, $driverRoute, null, $device, $user);
            $driverRoute->addEvent($mileageEvent);
            $driverRoute->addEvent($gasTankLevelEvent);
            $vehicle->setCurrentGasTankLevel($criteria['gasTankLevel']);
            $vehicle->setCurrentMileage($criteria['mileage']);
            $this->_em->flush();
            return [
                'success' => true,
                'returnToStation' => ['id' => $criteria['id']],
                'vehicle' => ['id' => $vehicle->getId()],
                'driverRoute' => ['id' => $driverRoute->getId()],
                'gasTankLevelEvent' => ['id' => $gasTankLevelEvent->getId()],
                'mileageEvent' => ['id' => $mileageEvent->getId()],
            ];
        }

        $message = 'RTS process was not filled with Mileage and Gas Tank Level in due to van could not be found' . $messageBy;
        $errorVehicleEvent = $this->em->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED_ERROR', 'Mileage and Gas Tank Level at RTS.', $message, null, $driverRoute, null, $device, $user, 'error');
        $mileageEvent = $this->em->getRepository(Event::class)->createEvent('MILEAGE', 'Mileage at RTS.', $messageMileage, null, $driverRoute, null, $device, $user);
        $gasTankLevelEvent = $this->em->getRepository(Event::class)->createEvent('GAS', 'Gas Tank Level at RTS.', $messageGasTankLevel, null, $driverRoute, null, $device, $user);
        $driverRoute->addEvent($mileageEvent);
        $driverRoute->addEvent($gasTankLevelEvent);
        $driverRoute->addEvent($errorVehicleEvent);
        $this->_em->flush();

        return [
            'success' => true,
            'returnToStation' => ['id' => $criteria['id']],
            'driverRoute' => ['id' => $driverRoute->getId()],
            'gasTankLevelEvent' => ['id' => $gasTankLevelEvent->getId()],
            'mileageEvent' => ['id' => $mileageEvent->getId()],
            'errorVehicleEvent' => ['id' => $errorVehicleEvent->getId()],
        ];
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function completeRTS(array $data)
    {
        if (empty($data['returnToStationId'])) {
            throw new Exception('kickoffLogId field missing.');
        }
        if (empty($data['companyId'])) {
            throw new Exception('companyId field missing.');
        }
        if (empty($data['platform'])) {
            throw new Exception('platform field missing.');
        }
        $dateTime = null;
        if (!empty($data['loggedOutPayrollAt'])) {
            $dateTime = new DateTime($data['loggedOutPayrollAt']);
        }
        $processCompleted = $dateTime ? true : false;
        $rts = $this->find($data['returnToStationId']);
        $rts->setLoggedOutPayrollAt($dateTime);
        $rts->setProcessCompleted($processCompleted);
        $rts->setCompletedAt($dateTime);
        $this->em->flush();
        $driverRoute = $rts->getDriverRoute();
        $event = null;
        $device = $this->globalUtils->getDevice($data);
        $vehicle = $this->globalUtils->getVehicle($data);
        if ($rts->getProcessCompleted()) {
            $title = "RTS completed.";
            $user = $this->globalUtils->getTokenUser();
            $message = $user->getFriendlyName() . " completed the RTS process.";
            $event = $this->em->getRepository(Event::class)->createEvent('RTS', $title, $message, null, $driverRoute, $vehicle, $device, $user);
            $driverRoute->addEvent($event);
            $this->em->flush();
        }

        return [
            'success' => true,
            'data' => $data,
            'event' => $event,
        ];
    }
}
