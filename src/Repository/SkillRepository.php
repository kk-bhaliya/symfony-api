<?php

namespace App\Repository;

use App\Entity\Skill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Skill|null find($id, $lockMode = null, $lockVersion = null)
 * @method Skill|null findOneBy(array $criteria, array $orderBy = null)
 * @method Skill[]    findAll()
 * @method Skill[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Skill::class);
    }

    public function updateSkillNullInSkill($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Skill', 'sk')
            ->set('sk.parent', 'NULL')
            ->where('sk.parent = :skillId')
            ->setParameter('skillId', $skillId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function getSkillByCompany($company)
    {
        $qb = $this->createQueryBuilder('sk')
            ->where('sk.company = :companyId')
            ->setParameter('companyId', $company)
            ->andWhere('sk.name = :name')
            ->setParameter('name', 'Driver')
            ->andWhere('sk.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        return $qb->getQuery()->getOneorNullResult();
    }

    public function getSkillsNameByCompany($companyId)
    {
        $skills = $this->createQueryBuilder('sk')
            ->select('sk.name')
            ->andWhere('sk.name IS NOT NULL')
            ->andWhere('sk.company = :companyId')
            ->setParameter('companyId', $companyId)
            ->andWhere('sk.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()->getResult();

        return array_map(function ($skills){
            return $skills['name'];
        }, $skills);
    }

    public function getSkillImportData($name, $company)
    {
        $em = $this->getEntityManager();

        $skill = new Skill();
        $skill->setName($name);
        $skill->setCompany($company);
        $em->persist($skill);
        $em->flush();

        return $skill;
    }
}
