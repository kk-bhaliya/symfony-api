<?php

namespace App\Repository;

use App\Entity\IncidentSubmitedPhase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method IncidentSubmitedPhase|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentSubmitedPhase|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentSubmitedPhase[]    findAll()
 * @method IncidentSubmitedPhase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentSubmitedPhaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentSubmitedPhase::class);
    }

    public function updateIncidentSubmitedPhase($incidentId, $phaseId, $submitedBy)
    {
        $currentDateTime = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentSubmitedPhase', 'isp')
        ->set('isp.submitedBy', $submitedBy)
        ->set('isp.submittedAt', ':currentDateTime')
        ->setParameter('currentDateTime', $currentDateTime)
        ->where('isp.incident = :incident')
        ->setParameter('incident', $incidentId)
        ->andWhere('isp.incidentPhase = :phaseId')
        ->setParameter('phaseId', $phaseId)
        ->getQuery()
        ->execute();
        
        return true;
    }
    
    public function reviewIncidentSubmitedPhase($incidentId, $phaseId, $reviedBy)
    {
        $currentDateTime = (!empty($reviedBy) && $reviedBy != "null" )?(new \DateTime())->setTimezone(new \DateTimeZone('UTC')):null;

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentSubmitedPhase', 'isp')
            ->set('isp.reviewedBy', $reviedBy)
            ->set('isp.reviewedAt',':currentDateTime')
            ->setParameter('currentDateTime', $currentDateTime)
            ->where('isp.incident = :incident')
            ->setParameter('incident', $incidentId)
            ->andWhere('isp.incidentPhase = :phaseId')
            ->setParameter('phaseId', $phaseId)
            ->getQuery()
            ->execute();
            
            return true;
    }
        
    public function fetchIncidentTimeline($critearia)
    {
        $incidentId = $critearia['incident'];
        $submitedPhases = $this->createQueryBuilder('isp')
            ->where('isp.incident = :incident')
            ->setParameter('incident', $incidentId)
            ->getQuery()
            ->getResult();

        $timeLine = [];

        // for incident Reported
        $incident = $this->getEntityManager()->getRepository('App\Entity\Incident')->find($incidentId);
        $timeLineRaw=[];
        $timeLineRaw['line'] = [];
        $timeLineRawLine['date'] = $incident->getCreatedDate()->format('Y-m-d H:i:sP');
        $timeLineRawLine['iconType'] = "Warning";
        $timeLineRawLine['information'] = "Reported";
        $timeLineRawLine['title'] = (method_exists($incident->getCreatedBy(), 'getFriendlyName'))? "Submitted By ".$incident->getCreatedBy()->getFriendlyName():'Submitted By ?';
        $timeLineRawLine['type'] = '';
        $timeLineRawLine['entries'] = [];
        array_push($timeLineRawLine['entries'],array('name' => 'Submitted By','value' => (method_exists($incident->getCreatedBy(), 'getFriendlyName'))?$incident->getCreatedBy()->getFriendlyName():'?'));
        array_push($timeLineRaw['line'],$timeLineRawLine);
        $timeLineRaw['date'] = $incident->getCreatedDate()->format('Y-m-d H:i:sP');
        $timeLineRaw['id'] = '';
        $timeLine[] = $timeLineRaw;

        // for incident submited phase
        foreach($submitedPhases as $submitedPhase){
            $timeLineRaw=[];
            $timeLineRaw['line'] = [];
            $timeLineRawLine['date'] = $submitedPhase->getSubmittedAt()->format('Y-m-d H:i:sP');
            $timeLineRawLine['iconType'] = "Advance";
            $timeLineRawLine['information'] = "Moved to ".$submitedPhase->getIncidentPhase()->getTitle();
            $timeLineRawLine['title'] = "Moved to ".$submitedPhase->getIncidentPhase()->getTitle();
            $timeLineRawLine['type'] = $submitedPhase->getIncidentPhase()->getTitle();
            $timeLineRawLine['entries'] = [];
            array_push($timeLineRawLine['entries'],array('name' => 'Submitted By','value' => $submitedPhase->getSubmitedBy()->getFriendlyName()));
            array_push($timeLineRaw['line'],$timeLineRawLine);
            $timeLineRaw['date'] = $submitedPhase->getSubmittedAt()->format('Y-m-d H:i:sP');
            $timeLineRaw['id'] = $submitedPhase->getId();
            $timeLine[] = $timeLineRaw;
            
            if(!empty($submitedPhase->getReviewedBy())){
                $timeLineRaw=[];
                $timeLineRaw['line'] = [];
                $timeLineRawLine['date'] = $submitedPhase->getReviewedAt()->format('Y-m-d H:i:sP');
                $timeLineRawLine['iconType'] = "Advance";
                $timeLineRawLine['information'] = "Reviewed ".$submitedPhase->getIncidentPhase()->getTitle();
                $timeLineRawLine['title'] = "Reviewed ".$submitedPhase->getIncidentPhase()->getTitle();
                $timeLineRawLine['type'] = $submitedPhase->getIncidentPhase()->getTitle();
                $timeLineRawLine['entries'] = [];
                array_push($timeLineRawLine['entries'],array('name' => 'Reviewed By','value' => $submitedPhase->getReviewedBy()->getFriendlyName()));
                array_push($timeLineRaw['line'],$timeLineRawLine);
                $timeLineRaw['date'] = $submitedPhase->getReviewedAt()->format('Y-m-d H:i:sP');
                $timeLineRaw['id'] = $submitedPhase->getId();
                $timeLine[] = $timeLineRaw;
            }

        }

        usort($timeLine, function ($a, $b) {
            return $b['date'] > $a['date'];
        });

        return $timeLine;
    }
}
