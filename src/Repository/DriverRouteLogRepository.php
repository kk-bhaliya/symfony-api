<?php

namespace App\Repository;

use App\Entity\DriverRouteLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DriverRouteLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method DriverRouteLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method DriverRouteLog[]    findAll()
 * @method DriverRouteLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DriverRouteLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DriverRouteLog::class);
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws Exception
     */
    public function getDriverRouteLogs($criteria)
    {
        $query = $this->createQueryBuilder('drl');
        if(isset($criteria['routeId'])){
            $query->andWhere('drl.routeId = :routeId ')
                  ->setParameter('routeId', $criteria['routeId']);   
        }  
        if(isset($criteria['shiftId'])){
            $query->andWhere('drl.shiftId = :shiftId ')
                  ->setParameter('shiftId', $criteria['shiftId']);   
        }  
        if(isset($criteria['driverId'])){
            $query->andWhere('drl.driverId = :driverId ')
                  ->setParameter('driverId', $criteria['driverId']);   
        }  
        if(isset($criteria['scheduleId'])){
            $query->andWhere('drl.scheduleId = :scheduleId ')
                  ->setParameter('scheduleId', $criteria['scheduleId']);   
        }  
        if(isset($criteria['scheduleDesignId'])){
            $query->andWhere('drl.scheduleDesignId = :scheduleDesignId ')
                  ->setParameter('scheduleDesignId', $criteria['scheduleDesignId']);   
        }

        if(isset($criteria['createdAt1'])){
            $query->andWhere('drl.createdAt >= :date1 ')
                  ->setParameter('date1',  new \DateTime($criteria['createdAt1']))
                  ->andWhere('drl.createdAt <= :date2 ')
                  ->setParameter('date2', new \DateTime($criteria['createdAt2']));   
        }
        if(isset($criteria['routeDate1'])){
            $query->andWhere('drl.routeDate >= :date1 ')
                  ->setParameter('date1', new \DateTime($criteria['routeDate1']))
                  ->andWhere('drl.routeDate <= :date2 ')
                  ->setParameter('date2', new \DateTime($criteria['routeDate2']));   
        }
        return $query->getQuery()->getResult();
    }
}
