<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Shift;
use App\Entity\Station;
use App\Entity\Skill;
use App\Entity\Company;
use DateTimeZone;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use function Doctrine\ORM\QueryBuilder;
use App\Utils\GlobalUtility;

/**
 * @method Shift|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shift|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shift[]    findAll()
 * @method Shift[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShiftRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $globalUtils;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage, GlobalUtility $globalUtils)
    {
        $this->tokenStorage = $tokenStorage;
        $this->globalUtils = $globalUtils;
        parent::__construct($registry, Shift::class);
    }

    public function findCategories()
    {
        $categoryInWord = $this->_entityName::$categories;
        return $categoryInWord;
    }

    public function getShiftByStation($criteria)
    {
        return $this->createQueryBuilder('sf')
            ->leftJoin('sf.skill','s')
            ->where('sf.station = :station')
            ->setParameter('station', $criteria['station'])
            ->addCriteria(Common::notArchived('sf'))
            ->addCriteria(Common::notArchived('s'))
            ->getQuery()
            ->getResult();
    }

    public function getShiftIdByStation($stationId)
    {
        $results = $this->createQueryBuilder('sf')
            ->select('sf.id')
            ->andWhere('sf.station = :station')
            ->setParameter('station', $stationId)
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);

        return array_column($results, 'id');
    }

    public function updateStationIsArchiveInShift($stationId)
    {

        $shifts = $this->getShiftIdByStation($stationId);
        if (!empty($shifts)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\Shift', 'sf')
                ->set('sf.isArchive', true)
                ->where($qb->expr()->in('sf.id', ':shifts'))
                ->setParameter('shifts', $shifts)
                ->getQuery()
                ->execute();

            //Call Temp driver route isArchive true set
            $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->updateDriverRouteSetIsArchiveByShift($shifts);
            $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->updateTempDriverRouteSetIsArchiveByShift($shifts);
            $this->getEntityManager()->getRepository('App\Entity\RouteCommitment')->updateRouteCommitmentSetIsArchiveByShift($shifts);
        }

        return true;
    }

    public function updateShiftSetIsArchive($invoiceTypeIds)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Shift', 'sf')
            ->set('sf.isArchive', true)
            ->where($qb->expr()->in('sf.invoiceType', ':invoiceTypeIds'))
            ->setParameter('invoiceTypeIds', $invoiceTypeIds)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateSkillNullInShift($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Shift', 'sk')
            ->set('sk.skill', 'NULL')
            ->where('sk.skill = :skillId')
            ->setParameter('skillId', $skillId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateBalanceGroupNullInShift($balanceGroupId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Shift', 'sf')
            ->set('sf.balanceGroup', 'NULL');
        if (is_array($balanceGroupId)) {
            $qb->where($qb->expr()->in('sf.balanceGroup', ':balanceGroupId'))
                ->setParameter('balanceGroupId', $balanceGroupId);
        } else {
            $qb->where('sf.balanceGroup = :balanceGroup')
                ->setParameter('balanceGroup', $balanceGroupId);
        }
        $qb->getQuery()
            ->execute();
        return true;
    }

    public function updateInvoiceTypeNullInShifts($invoiceTypeId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Shift', 'sf')
            ->set('sf.invoiceType', 'NULL')
            ->where('sf.invoiceType = :invoiceType')
            ->setParameter('invoiceType', $invoiceTypeId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function updateShiftTemplateNullInShifts($shiftTemplateId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Shift', 'sf')
            ->set('sf.shiftTemplate', 'NULL')
            ->where('sf.shiftTemplate = :shiftTemplateId')
            ->setParameter('shiftTemplateId', $shiftTemplateId)
            ->getQuery()
            ->execute();
        return true;
    }

    public function insertDefaultShiftByStation($criteria)
    {
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));

        $shift1StartTime = $date->format('Y-m-d 13:00:00');
        $shift1EndTime = $date->format('Y-m-d 22:00:00');

        $shift2StartTime = $date->format('Y-m-d 12:15:00');
        $shift2EndTime = $date->format('Y-m-d 23:45:00');

        $shift3StartTime = $date->format('Y-m-d 12:15:00');
        $shift3EndTime = $date->format('Y-m-d 23:45:00');

        $shift4StartTime = $date->format('Y-m-d 12:15:00');
        $shift4EndTime = $date->format('Y-m-d 15:15:00');

        $shift5StartTime = $date->format('Y-m-d 12:15:00');
        $shift5EndTime = $date->format('Y-m-d 23:45:00');

        $station = $this->getEntityManager()->getRepository('App\Entity\Station')->find($criteria['stationId']);
        $company = $this->getEntityManager()->getRepository('App\Entity\Company')->find($criteria['companyId']);
        $skill = $this->getEntityManager()->getRepository('App\Entity\Skill')->getSkillByCompany($criteria['companyId']);

        if (empty($skill)) {
            $skill = new Skill();
            $skill->setCompany($company);
            $skill->setName('Driver');
            $skill->setIsArchive(false);
            $this->getEntityManager()->persist($skill);
            $this->getEntityManager()->flush();
        }
        $shift1 = new Shift();
        $shift1->setName('Classroom Training');
        $shift1->setStartTime(new \DateTime($shift1StartTime));
        $shift1->setEndTime(new \DateTime($shift1EndTime));
        $shift1->setUnpaidBreak('30');
        $shift1->setNote('Please arrive at least 15 minutes early to have time to find parking.');
        $shift1->setColor('#DBAE60');
        $shift1->setStation($station);
        $shift1->setCompany($company);
        $shift1->setCategory('3');
        $shift1->setIsArchive(false);
        $shift1->setSkill($skill);
        $this->getEntityManager()->persist($shift1);
        $this->getEntityManager()->flush();

        $shift2 = new Shift();
        $shift2->setName('10 Hr - Lvl 1');
        $shift2->setStartTime(new \DateTime($shift2StartTime));
        $shift2->setEndTime(new \DateTime($shift2EndTime));
        $shift2->setUnpaidBreak('30');
        $shift2->setNote('Please come prepared with your badge, vest, and uniform.');
        $shift2->setColor('#516F90');
        $shift2->setStation($station);
        $shift2->setCompany($company);
        $shift2->setCategory('3');
        $shift2->setIsArchive(false);
        $shift2->setSkill($skill);
        $this->getEntityManager()->persist($shift2);
        $this->getEntityManager()->flush();

        $shift3 = new Shift();
        $shift3->setName('10 Hr - Lvl 2');
        $shift3->setStartTime(new \DateTime($shift3StartTime));
        $shift3->setEndTime(new \DateTime($shift3EndTime));
        $shift3->setUnpaidBreak('30');
        $shift3->setNote('Please come prepared with your badge, vest, and uniform.');
        $shift3->setColor('#516F90');
        $shift3->setStation($station);
        $shift3->setCompany($company);
        $shift3->setCategory('3');
        $shift3->setIsArchive(false);
        $shift3->setSkill($skill);
        $this->getEntityManager()->persist($shift3);
        $this->getEntityManager()->flush();

        $shift4 = new Shift();
        $shift4->setName('Backup Driver C1');
        $shift4->setStartTime(new \DateTime($shift4StartTime));
        $shift4->setEndTime(new \DateTime($shift4EndTime));
        $shift4->setUnpaidBreak('30');
        $shift4->setNote('Come prepared to run a full route. If all Soldier routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.');
        $shift4->setColor('#516F90');
        $shift4->setStation($station);
        $shift4->setCompany($company);
        $shift4->setCategory('2');
        $shift4->setIsArchive(false);
        $shift4->setSkill($skill);
        $this->getEntityManager()->persist($shift4);
        $this->getEntityManager()->flush();

        $shift5 = new Shift();
        $shift5->setName('Rescue');
        $shift5->setStartTime(new \DateTime($shift5StartTime));
        $shift5->setEndTime(new \DateTime($shift5EndTime));
        $shift5->setUnpaidBreak(0);
        $shift5->setNote(null);
        $shift5->setColor('#0040FF');
        $shift5->setTextColor('#ffffff');
        $shift5->setStation($station);
        $shift5->setCompany($company);
        $shift5->setCategory('4');
        $shift5->setIsArchive(false);
        $shift5->setSkill($skill);
        $this->getEntityManager()->persist($shift5);
        $this->getEntityManager()->flush();

        $shift6 = new Shift();
        $shift6->setName('Add Train');
        $shift6->setStartTime(new \DateTime($shift5StartTime));
        $shift6->setEndTime(new \DateTime($shift5EndTime));
        $shift6->setUnpaidBreak(0);
        $shift6->setNote(null);
        $shift6->setColor('#FFC300');
        $shift6->setTextColor('#ffffff');
        $shift6->setStation($station);
        $shift6->setCompany($company);
        $shift6->setCategory('5');
        $shift6->setIsArchive(false);
        $shift6->setSkill($skill);
        $this->getEntityManager()->persist($shift6);
        $this->getEntityManager()->flush();

        $shift7 = new Shift();
        $shift7->setName('Light Duty');
        $shift7->setStartTime(new \DateTime($shift5StartTime));
        $shift7->setEndTime(new \DateTime($shift5EndTime));
        $shift7->setUnpaidBreak(0);
        $shift7->setNote(null);
        $shift7->setColor('#27AE60');
        $shift7->setTextColor('#ffffff');
        $shift7->setStation($station);
        $shift7->setCompany($company);
        $shift7->setCategory('6');
        $shift7->setIsArchive(false);
        $shift7->setSkill($skill);
        $this->getEntityManager()->persist($shift7);
        $this->getEntityManager()->flush();

        $shift8 = new Shift();
        $shift8->setName('Unscheduled Driver');
        $shift8->setStartTime(new \DateTime($shift4StartTime));
        $shift8->setEndTime(new \DateTime($shift4EndTime));
        $shift8->setUnpaidBreak('30');
        $shift8->setNote('Come prepared to run a full route. If all Soldier routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.');
        $shift8->setColor('#428BCA');
        $shift8->setStation($station);
        $shift8->setCompany($company);
        $shift8->setCategory('7');
        $shift8->setIsArchive(false);
        $shift8->setSkill($skill);
        $this->getEntityManager()->persist($shift8);
        $this->getEntityManager()->flush();

        $shift9 = new Shift();
        $shift9->setName('Backup Driver C2');
        $shift9->setStartTime(new \DateTime($shift4StartTime));
        $shift9->setEndTime(new \DateTime($shift4EndTime));
        $shift9->setUnpaidBreak('30');
        $shift9->setNote('Come prepared to run a full route. If all Soldier routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.');
        $shift9->setColor('#516F90');
        $shift9->setStation($station);
        $shift9->setCompany($company);
        $shift9->setCategory('2');
        $shift9->setIsArchive(false);
        $shift9->setSkill($skill);
        $this->getEntityManager()->persist($shift9);
        $this->getEntityManager()->flush();
    }

    public function getShiftsForShiftModule($criteria)
    {
        return $this->createQueryBuilder('sf')
            ->andWhere('sf.station = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('sf.category != :categoryRescue')
            ->setParameter('categoryRescue', Shift::RESCUE)
            ->andWhere('sf.category != :categoryTrain')
            ->setParameter('categoryTrain', Shift::TRAIN)
            ->andWhere('sf.category != :categoryDuty')
            ->setParameter('categoryDuty', Shift::DUTY)
            ->andWhere('sf.category != :categorySystem')
            ->setParameter('categorySystem', Shift::SYSTEM)
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->orderBy('sf.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getShiftsForBalanceGroup($criteria)
    {
        $balanceGroup = isset($criteria['balanceGroup'])?$criteria['balanceGroup']:'';

        $qb = $this->createQueryBuilder('sf')
            ->andWhere('sf.station = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('sf.category != :categoryRescue')
            ->setParameter('categoryRescue', Shift::RESCUE)
            ->andWhere('sf.category != :categoryTrain')
            ->setParameter('categoryTrain', Shift::TRAIN)
            ->andWhere('sf.category != :categoryDuty')
            ->setParameter('categoryDuty', Shift::DUTY)
            ->andWhere('sf.category != :categorySystem')
            ->setParameter('categorySystem', Shift::SYSTEM)
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false);

            if($balanceGroup != ''){
                $qb->leftJoin('sf.balanceGroup', 'bg')
                ->andWhere('bg.id = :balanceGroupId')
                ->setParameter('balanceGroupId', $balanceGroup);
            }

        return $qb->orderBy('sf.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getNoramlAndBackupShiftByStationAndCompany($criteria)
    {
        return $this->createQueryBuilder('sf')
            ->leftJoin('sf.station', 's')
            ->andWhere('sf.station = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('sf.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('sf.category != :categoryTraining')
            ->setParameter('categoryTraining', Shift::TRAINING)
            ->andWhere('sf.category != :categoryRescue')
            ->setParameter('categoryRescue', Shift::RESCUE)
            ->andWhere('sf.category != :categoryTrain')
            ->setParameter('categoryTrain', Shift::TRAIN)
            ->andWhere('sf.category != :categoryDuty')
            ->setParameter('categoryDuty', Shift::DUTY)
            ->andWhere('sf.category != :categorySystem')
            ->setParameter('categorySystem', Shift::SYSTEM)
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->orderBy('sf.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getNoramlAndBackupShiftForScheduler($criteria)
    {
        $query = $this->createQueryBuilder('sf')
            ->leftJoin('sf.station', 's');

        if (isset($criteria['station'])) {
            $query->andWhere("sf.station = :station")
                ->setParameter('station', $criteria['station']);
        }

        $query->andWhere('sf.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
        ;

        $categories = [];
        $aShifts = [Shift::RESCUE,Shift::TRAIN,Shift::DUTY,Shift::SYSTEM];
        if (isset($criteria['isRescue']) && $criteria['isRescue'] == true) {
            $categories[] = Shift::RESCUE;
        }

        if (isset($criteria['isTrain']) && $criteria['isTrain'] == true) {
            $categories[] = Shift::TRAIN;
        }

        if (isset($criteria['isDuty']) && $criteria['isDuty'] == true) {
            $categories[] = Shift::DUTY;
        }

        if (isset($criteria['isUnscheduledDriverShift']) && $criteria['isUnscheduledDriverShift'] == true) {
            //$categories[] = Shift::SYSTEM;
            $aShifts = [Shift::RESCUE,Shift::TRAIN,Shift::DUTY];
        }

        if (count($categories)) {
            $query->andWhere(
                $query->expr()->in('sf.category', $categories)
            );
        } else {
            $query->andWhere(
                $query->expr()->notIn('sf.category', $aShifts)
            );
        }

        return $query
            ->addOrderBy('s.name', 'ASC')
            ->addOrderBy('sf.name', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function getNoramlAndBackupShiftForLoadOut($criteria)
    {
        $query = $this->createQueryBuilder('sf')
            ->leftJoin('sf.station', 's');
        if (isset($criteria['station'])) {
            $query->andWhere('sf.station = :station')
                ->setParameter('station', $criteria['station']);
        }
        if (isset($criteria['isRescue']) && $criteria['isRescue'] == true) {
            $results = $query->andWhere('sf.company = :company')
                ->setParameter('company', $criteria['company'])
                ->andWhere('sf.category = :categoryRescue')
                ->setParameter('categoryRescue', Shift::RESCUE)
                ->andWhere('sf.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->getQuery()
                ->getResult();
        } else if (isset($criteria['isTrain']) && $criteria['isTrain'] == true) {
            $results = $query->andWhere('sf.company = :company')
                ->setParameter('company', $criteria['company'])
                ->andWhere('sf.category = :categoryTrain')
                ->setParameter('categoryTrain', Shift::TRAIN)
                ->andWhere('sf.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->getQuery()
                ->getResult();
        } else if (isset($criteria['isDuty']) && $criteria['isDuty'] == true) {
            $results = $query->andWhere('sf.company = :company')
                ->setParameter('company', $criteria['company'])
                ->andWhere('sf.category = :categoryDuty')
                ->setParameter('categoryDuty', Shift::DUTY)
                ->andWhere('sf.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->getQuery()
                ->getResult();
        } else {
            $results = $query->andWhere('sf.company = :company')
                ->setParameter('company', $criteria['company'])
                //->andWhere('sf.category != :categoryTraining')
                //->setParameter('categoryTraining', Shift::TRAINING)
                ->andWhere('sf.category != :categoryRescue')
                ->setParameter('categoryRescue', Shift::RESCUE)
                ->andWhere('sf.category != :categoryTrain')
                ->setParameter('categoryTrain', Shift::TRAIN)
                ->andWhere('sf.category != :categoryDuty')
                ->setParameter('categoryDuty', Shift::DUTY)
                ->andWhere('sf.category != :categorySystem')
                ->setParameter('categorySystem', Shift::SYSTEM)
                ->andWhere('sf.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->addOrderBy('s.name', 'ASC')
                ->addOrderBy('sf.name', 'ASC')
                ->getQuery()
                ->getResult();
        }
        $shiftArray = [];
        $nCount = 0;
        foreach ($results as $result) {
            $shiftArray[$nCount]['id'] = $result->getId();
            $shiftArray[$nCount]['name'] = $result->getName();
            $shiftArray[$nCount]['color'] = $result->getColor();
            $shiftArray[$nCount]['startTime'] = (Float) $result->getStartTime()->format('G.i');
            $shiftArray[$nCount]['endTime'] = (Float) $result->getEndTime()->format('G.i');
            $shiftArray[$nCount]['station'] = ['id' => $result->getStation()->getId(),'name' => $result->getStation()->getName()];
            $shiftArray[$nCount]['textColor'] = $result->getTextColor();
            $shiftArray[$nCount]['invoiceType'] = !empty($result->getInvoiceType()) ? $result->getInvoiceType()->getId() : null;
            $shiftArray[$nCount]['shiftTimeDiffrence'] = ($result->getEndTime()->diff($result->getStartTime())->format('%h') * 60)+($result->getEndTime()->diff($result->getStartTime())->format('%i'));
            $nCount++;
        }
        return $shiftArray;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getFutureDrivers(array $criteria)
    {
        if (!(empty($criteria['shiftId']) || empty($criteria['id']))) {
            throw new Exception('Shift ID not found');
        }
        $value = empty($criteria['shiftId']) ? $criteria['id'] : $criteria['shiftId'];
        $driverRoutes = $this->_em->getRepository(DriverRoute::class)->findBy(['shiftType' => $value]);
        $drivers = [];
        $driver = null;
        if ($driverRoutes) {
            $now = new DateTime("now");
            foreach ($driverRoutes as $driverRoute) {
                /** @var DriverRoute $driverRoute */
                if ($driverRoute->getIsArchive() === false &&
                    $driverRoute->getDateCreated() >= $now &&
                    $driverRoute->getDriver() && $driverRoute->getDriver()->getIsArchive() === false
                ) {
                    $driver = $driverRoute->getDriver();
                    $drivers[$driver->getId()] = [
                        'driver' => [
                            'id' => $driver->getId(),
                            'personalPhone' => $driver->getPersonalPhone(),
                            'name' => $driver->getFullName(),
                            'email' => $driver->getEmail(),
                            'channelSid' => $driver->getUser()->getNotificationsChannelSid(),
                        ],
                        'shift' => [
                            'name' => $driverRoute->getShiftType()->getName(),
                            'id' => $driverRoute->getShiftType()->getId(),
                        ],
                    ];
                }
            }
        } else {
            throw new Exception('Driver Routes not found');
        }
        return $drivers;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getFutureDriverRoutes(array $criteria)
    {
        if (empty($criteria['shiftId'])) {
            throw new Exception('Shift ID not found');
        }

        $driverRoutes = $this->_em->getRepository(DriverRoute::class)->findBy(['shiftType' => $criteria['shiftId']]);
        $routes = [];
        if ($driverRoutes) {
            $now = new DateTime("now");
            foreach ($driverRoutes as $driverRoute) {
                /** @var DriverRoute $driverRoute */
                if ($driverRoute->getIsArchive() === false &&
                    $driverRoute->getDateCreated() >= $now &&
                    $driverRoute->getDriver() && $driverRoute->getDriver()->getIsArchive() === false
                ) {
                    $routes[$driverRoute->getId()] = [
                        'driverId' => $driverRoute->getDriver()->getId(),
                        'driverRouteId' => $driverRoute->getId(),
                        'personalPhone' => $driverRoute->getDriver()->getPersonalPhone(),
                        'driverRouteDateCreated' => $driverRoute->getDateCreated()->format('Y-m-d'),
                        'shiftType' => $driverRoute->getShiftType()->getName(),
                        'shiftId' => $driverRoute->getShiftType()->getId(),
                    ];
                }
            }
        } else {
            throw new Exception('Driver Routes not found');
        }
        return $routes;
    }

    public function form_getShifts($type, $id){
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $qb = $this->createQueryBuilder('sf')
            ->select('sf.id as id, sf.name as name')
            ->where('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('sf.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        if($type == 'station'){
            $qb->andWhere('sf.station = :stationId')
            ->setParameter('stationId', $id);
        }

        return $qb->getQuery()->getResult(Query::HYDRATE_ARRAY);
    }

    public function getShiftImportData($name, $station, $balanceGroup, $invoiceType, $company, $skill, $color, $sTime, $eTime, $unpaidBreak, $note, $category, $textColor)
    {
        $em = $this->getEntityManager();

        $shift = new Shift();
        $shift->setName($name);
        $shift->setStation($station);
        $shift->setBalanceGroup($balanceGroup);
        $shift->setInvoiceType($invoiceType);
        $shift->setCompany($company);
        $shift->setSkill($skill);
        $shift->setColor($color);
        $shift->setStartTime(new \DateTime($sTime));
        $shift->setEndTime(new \DateTime($eTime));
        $shift->setUnpaidBreak($unpaidBreak);
        $shift->setNote($note);
        $shift->setCategory($category);
        $shift->setTextColor($textColor);
        $em->persist($shift);
        $em->flush();

        return $shift;
    }

    public function updateUnscheduleCategoryShift($criteria)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $invoiceType = NULL;
        $balanceGroup = NULL;
        if (!empty($criteria['invoiceType'])) {
            $invoiceType = $this->getEntityManager()->getRepository('App\Entity\InvoiceType')->find($criteria['invoiceType']);
        }
        if (!empty($criteria['balanceGroup'])) {
            $balanceGroup = $this->getEntityManager()->getRepository('App\Entity\BalanceGroup')->find($criteria['balanceGroup']);
        }

        $skill = $this->getEntityManager()->getRepository('App\Entity\Skill')->find($criteria['skill']);

        $qb->update('App\Entity\Shift', 'sf')
            ->set('sf.startTime', ':startTime')
            ->setParameter('startTime', new \DateTime($criteria['startTime']))
            ->set('sf.endTime', ':endTime')
            ->setParameter('endTime', new \DateTime($criteria['endTime']))
            ->set('sf.note', ':note')
            ->setParameter('note', $criteria['note'])
            ->set('sf.unpaidBreak', ':unpaidBreak')
            ->setParameter('unpaidBreak', $criteria['unpaidBreak'])
            ->set('sf.skill', ':skill')
            ->setParameter('skill', $skill)
            ->set('sf.invoiceType', ':invoiceType')
            ->setParameter('invoiceType', $invoiceType)
            ->set('sf.balanceGroup', ':balanceGroup')
            ->setParameter('balanceGroup', $balanceGroup)
            ->set('sf.textColor', ':textColor')
            ->setParameter('textColor', '#ffffff')
            ->where('sf.station = :stationId')
            ->setParameter('stationId', $criteria['station'])
            ->andWhere('sf.category = :categorySystem')
            ->setParameter('categorySystem', Shift::SYSTEM)
            ->getQuery()
            ->execute();

        return true;
    }
    public function addShiftChangeEvent($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->find($criteria['driverRouteId']);
        $shift = $driverRoute->getShiftType();
        $this->addShiftEventData($criteria['shiftData'], $shift, $userLogin, $driverRoute, true);
    }

    public function addShiftEventData($shiftDiffrances, $shift, $userLogin, $driverRoute, $isActive = false)
    {
        $unscheduleMsg = ($driverRoute->getShiftCategory() == 7)?'Unschedule ':'';
        foreach ($shiftDiffrances as $keyName => $shiftDiffrance) {
            if ('startTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_start_time'=>(($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_start_time'=>(($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name'=>$shift->getName(),
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift StartTime Edit', $driverRoute, $unscheduleMsg.'SHIFT_START_TIME_EDIT', $messageData, $isActive);
            }
            if ('endTime' == $keyName && (($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))) != (($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1])))) {
                $messageData = array(
                    'old_end_time'=>(($shiftDiffrance[0] instanceof DateTime) ? $shiftDiffrance[0]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[0]))),
                    'new_end_time'=>(($shiftDiffrance[1] instanceof DateTime) ? $shiftDiffrance[1]->format('H:i:s') : date("H:i:s", strtotime($shiftDiffrance[1]))),
                    'shift_name'=>$shift->getName(),
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift EndTime Edit', $driverRoute, 'SHIFT_END_TIME_EDIT', $messageData, $isActive);
            }
            if ('newNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name'=>$shift->getName(),
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Driver Note Edit', $driverRoute, 'EDIT_DRIVER_SHIFT_NOTE', $messageData, $isActive);
            }
            if ('currentNote' == $keyName) {
                $messageData = array(
                    'old_note' => $shiftDiffrance[0],
                    'new_note' => $shiftDiffrance[1],
                    'shift_name'=>$shift->getName(),
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Note Edit', $driverRoute, 'EDIT_SHIFT_NOTE', $messageData, $isActive);
            }
            if ('shiftName' == $keyName) {
                $messageData = array(
                    'old_name' => $shiftDiffrance[0],
                    'new_name' => $shiftDiffrance[1],
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Name Edit', $driverRoute, 'EDIT_SHIFT_NAME', $messageData, $isActive);
            }
            if ('shiftInvoiceType' == $keyName) {
                $messageData = array(
                    'old_invoice_type' => (is_object($shiftDiffrance[0]) ? $shiftDiffrance[0]->getName() : $shiftDiffrance[0]['name']),
                    'new_invoice_type' => (is_object($shiftDiffrance[1]) ? $shiftDiffrance[1]->getName() : $shiftDiffrance[1]['name']),
                    'shift_name'=>$shift->getName(),
                    'user_name'=>$userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Invoice Edit', $driverRoute, 'EDIT_SHIFT_INVOICE_TYPE', $messageData, $isActive);
            }
        }
    }

    public function addShiftEventDataByObject($userLogin, $oldObject, $newObject,$isActive = false)
    {
        $unscheduleMsg = ($oldObject->getShiftCategory() == 7)?'Unschedule ':'';
        if ($oldObject->getStartTime()->format('H:i:s') != $newObject->getStartTime()->format('H:i:s')) {
            $messageData = array(
                'old_start_time' => $oldObject->getStartTime()->format('H:i:s'),
                'new_start_time' => $newObject->getStartTime()->format('H:i:s'),
                'shift_name' => $newObject->getShiftName(),
                'user_name' => $userLogin->getFriendlyName(),
            );
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift StartTime Edit', $newObject->getRouteId(), 'SHIFT_START_TIME_EDIT', $messageData, $isActive);
        }
        if ($oldObject->getEndTime()->format('H:i:s') != $newObject->getEndTime()->format('H:i:s')) {
            $messageData = array(
                'old_end_time' => $oldObject->getEndTime()->format('H:i:s'),
                'new_end_time' => $newObject->getEndTime()->format('H:i:s'),
                'shift_name' => $newObject->getShiftName(),
                'user_name' => $userLogin->getFriendlyName(),
            );
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift EndTime Edit', $newObject->getRouteId(), 'SHIFT_END_TIME_EDIT', $messageData, $isActive);
        }
        if ($oldObject->getNewNote() != $newObject->getNewNote()) {
            $messageData = array(
                'old_note' => $oldObject->getNewNote(),
                'new_note' => $newObject->getNewNote(),
                'shift_name' => $newObject->getShiftName(),
                'user_name' => $userLogin->getFriendlyName(),
            );
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Driver Note Edit', $newObject->getRouteId(), 'EDIT_DRIVER_SHIFT_NOTE', $messageData, $isActive);
        }
        if ($oldObject->getCurrentNote() != $newObject->getCurrentNote()) {
            $messageData = array(
                'old_note' => $oldObject->getCurrentNote(),
                'new_note' => $newObject->getCurrentNote(),
                'shift_name' => $newObject->getShiftName(),
                'user_name' => $userLogin->getFriendlyName(),
            );
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Note Edit', $newObject->getRouteId(), 'EDIT_SHIFT_NOTE', $messageData, $isActive);
        }
        if ($oldObject->getShiftName() != $newObject->getShiftName()) {
            $messageData = array(
                'old_name' => $oldObject->getShiftName(),
                'new_name' => $newObject->getShiftName(),
                'user_name' => $userLogin->getFriendlyName(),
            );
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Name Edit', $newObject->getRouteId(), 'EDIT_SHIFT_NAME', $messageData, $isActive);
        }

        if(!empty($oldObject->getShiftInvoiceType()) && !empty($newObject->getShiftInvoiceType())) {
            if ($oldObject->getShiftInvoiceType()->getName() != $newObject->getShiftInvoiceType()->getName()) {
                $messageData = array(
                    'old_invoice_type' => $oldObject->getShiftInvoiceType()->getName(),
                    'new_invoice_type' => $newObject->getShiftInvoiceType()->getName(),
                    'shift_name' => $newObject->getShiftName(),
                    'user_name' => $userLogin->getFriendlyName(),
                );
                $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData($unscheduleMsg.'Shift Invoice Edit', $newObject->getRouteId(), 'EDIT_SHIFT_INVOICE_TYPE', $messageData, $isActive);
            }
        }
    }

    public function addNewShiftEventForTimeline($userLogin, $driverRoute)
    {
        if (!empty($driverRoute)) {
            $messageData = array('shift_name' => $driverRoute->getShiftType()->getName(), 'user_name' => $userLogin->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Shift Add', $driverRoute, 'SHIFT_ADD', $messageData, true);
        }
    }

    public function dragShiftEventDataByObject($userLogin, $oldObject, $newObject, $isActive = false)
    {
        if (!empty($oldObject->getDriver()) && !empty($newObject->getDriver())) {
            $messageData = array('shift_name' => $oldObject->getRouteId()->getShiftType()->getName(), 'old_user' =>$oldObject->getDriver()->getUser()->getFriendlyName(), 'new_user' =>$newObject->getDriver()->getUser()->getFriendlyName(), 'old_date' =>$oldObject->getDateCreated()->format('M d,Y'), 'new_date' =>$newObject->getDateCreated()->format('M d,Y'), 'user_name' => $userLogin->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Shift Drag', $newObject->getRouteId(), 'SHIFT_DRAG', $messageData, true);
        }
    }
}
