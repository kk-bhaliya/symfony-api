<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\Company;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\Location;
use App\Entity\Station;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Utils\GlobalUtility;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;
use Doctrine\ORM\TransactionRequiredException;
use Exception;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use function Matrix\diagonal;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Services\AmazonS3Service;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

/**
 * @method Vehicle|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vehicle|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vehicle[]    findAll()
 * @method Vehicle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    private $globalUtils;
    private $s3;
    private $params;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $entityManager,
        GlobalUtility $globalUtils,
        AmazonS3Service $s3,
        ContainerBagInterface $params
    )
    {
        parent::__construct($registry, Vehicle::class);
        $this->tokenStorage = $tokenStorage;
        $this->entityManager = $entityManager;
        $this->globalUtils = $globalUtils;
        $this->s3 = $s3;
        $this->params = $params;
    }


    /**
     * @param $stationId
     * @return int|mixed|string
     * @throws QueryException
     */
    public function getVehicleByStation($stationId)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $qb = $this->createQueryBuilder('v')
            ->where('v.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->addCriteria(Common::notArchived('v'))
            ->orderBy('v.vehicleId', 'ASC');


        if (!empty($stationId)) {
            $qb->andWhere('v.station= :station')
                ->setParameter('station', $stationId);
        }

        return $qb->getQuery()->getResult();

    }

    /**
     * @param $criteria
     * @return array
     * @throws QueryException
     */
    public function getLoadOutVehicleByStation($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $currentDateObj = isset($criteria['date']) ? (new \DateTime($criteria['date'])) : (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');

        //Open shift driver route data
        $tempOpenShiftRoutesIdArr = [];

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTodayLoadOutTempDriverRouteDriverNullData($userLogin, $currentDate, $criteria['stationId']);
        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute->getRouteId()->getId();
        }
        //Loadout remove starttime
       /* $loadOutData = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->getTodayDriverRouteRescuereListingData($userLogin, $currentDate, $criteria['stationId'], $startTime = null, $endTime = null, $tempOpenShiftRoutesIdArr);
        $vehicleIds = [];
        if ($loadOutData) {
            foreach ($loadOutData as $data) {
                if ($data->getVehicle()) {
                    if ($criteria['id'] !== $data->getId()) {
                        $vehicleIds[] = $data->getVehicle()->getId();
                    }
                }
            }
        } */

        $loadOutData = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->getTodayDriverRouteRescuereListingDataNew($userLogin->getCompany()->getId(), $currentDate, $criteria['stationId'], $startTime = null, $endTime = null, $tempOpenShiftRoutesIdArr);
        
        $vehicleIds = [];
        if ($loadOutData) {
            foreach ($loadOutData as $data) {                
                if ($criteria['id'] != $data['id']) {
                    $vehicleIds[] = $data['VehicleId'];
                }             
            }
        }

        $vehicles = $this->getVehicleByStation($criteria['stationId']);

        $vehicleData = [];
        foreach ($vehicles as $vehicle) {
            if (!in_array($vehicle->getId(), $vehicleIds)) {
                $vehicleId = $vehicle->getVehicleId();
                $vin = $vehicle->getVin();
                $unit = strval($vehicle->getId());
                if (!is_string($vehicleId)) {
                    if (is_string($vin)) {
                        $unit = substr($vin, -6);
                    }
                } else {
                    $unit = $vehicleId;
                }
                $vehicleData[] = [
                    'id' => $vehicle->getId(),
                    'unit' => $unit
                ];
            }
        }
        return $vehicleData;
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function validateVanFields(array $data)
    {
        if (empty($data['driverRouteId'])) {
            throw new Exception('driverRouteId missing.');
        }
        if (!isset($data['scannedQRCode'])) {
            throw new Exception('scannedQRCode missing.');
        }
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }
    }

    /**
     * @return Vehicle
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createNewVehicle()
    {
        $vehicle = new Vehicle();
        $vehicle->setVin('NEW VIN');
        $vehicle->setVehicleId('NEW VEHICLE ID');
        $vehicle->setNotes('VEHICLE CREATED AUTOMATICALLY.');
        $vehicle->setState('NEW VAN');
        $vehicle->setModel('NEW VAN');
        $vehicle->setModelType('Box');
        $vehicle->setPolicyNumber('NEW VAN');
        $vehicle->setLicensePlate('NEW VAN');
        $vehicle->setHeight('NEW VAN');
        $vehicle->setYear('NEW VAN');
        $vehicle->setStatus('Active');
        $vehicle->setType('Leased');
        $vehicle->setMake('NEW VAN');
        $vehicle->setCreatedAt(new DateTime('now'));
        $this->_em->persist($vehicle);
        $this->_em->flush();
        return $vehicle;
    }

    /**
     * @param array $data
     * @return Vehicle
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createVan(array $data)
    {
        $vehicle = new Vehicle();
        if (isset($data['vin'])) {
            $vehicle->setVin($data['vin']);
        }
        if (isset($data['vehicleId'])) {
            $vehicle->setVehicleId($data['vehicleId']);
        }
        if (isset($data['notes'])) {
            $vehicle->setNotes($data['notes']);
        }
        if (isset($data['state'])) {
            $vehicle->setState($data['notes']);
        } else {
            $vehicle->setState('N/A');
        }
        if (isset($data['model'])) {
            $vehicle->setModel($data['notes']);
        } else {
            $vehicle->setModel('N/A');
        }
        if (isset($data['modelType'])) {
            $vehicle->setModelType($data['modelType']);
        } else {
            $vehicle->setModelType('Box');
        }
        if (isset($data['policyNumber'])) {
            $vehicle->setPolicyNumber($data['policyNumber']);
        }
        if (isset($data['licensePlate'])) {
            $vehicle->setLicensePlate($data['licensePlate']);
        } else {
            $vehicle->setLicensePlate('N/A');
        }
        if (isset($data['height'])) {
            $vehicle->setHeight($data['height']);
        } else {
            $vehicle->setHeight('N/A');
        }
        if (isset($data['year'])) {
            $vehicle->setYear($data['year']);
        }
        if (isset($data['status'])) {
            $vehicle->setStatus($data['status']);
        } else {
            $vehicle->setStatus('Active');
        }
        if (isset($data['type'])) {
            $vehicle->setType($data['type']);
        } else {
            $vehicle->setType('Leased');
        }
        if (isset($data['make'])) {
            $vehicle->setMake($data['make']);
        } else {
            $vehicle->setMake('N/A');
        }
        $this->_em->persist($vehicle);
        $this->_em->flush();
        return $vehicle;
    }

    /**
     * @param array $data
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createVehicle(array $data)
    {
        /** @var User $user */
        $user = $this->tokenStorage->getToken()->getUser();
        $driverRoute = $this->entityManager->getRepository(DriverRoute::class)->findOneBy(['id' => $data['driverRouteId'], 'isArchive' => false]);
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('Driver Route not found.');
        }
        $kickoffLog = $driverRoute->getKickoffLog();
        $vehicle = null;

        if (!empty($data['vin'])) {
            $vehicle = $this->entityManager->getRepository(Vehicle::class)->findOneVehicleByVin(['vin' => $data['vin'], 'company' => $data['companyId'], 'isArchive' => false]);

        }

        if (!empty($data['vehicleId'])) {
            if(!is_null($vehicle)){
                $vehicle = $this->entityManager->getRepository(Vehicle::class)->findOneVehicleByVehicleId(['vehicleId' => $data['vehicleId'], 'company' => $data['companyId'], 'isArchive' => false]);
            }
        }

        if ($vehicle instanceof Vehicle) {
            $driverRoute->setVehicle($vehicle);
            $kickoffLog->setVehicle($vehicle);
            $vehicle->setCurrentUsedBy($user->getFriendlyName());
            $vehicle->setLastUsedByUser($user);
            if (!empty($data['datetime'])) {
                $vehicle->setLastUsedAt(new DateTime($data['datetime']));
            } else {
                $vehicle->setLastUsedAt(new DateTime());
            }
            $this->entityManager->flush();
            return [
                'success' => true,
                'message' => 'Vehicle already exists.',
                'vehicle' => [
                    'id' => $vehicle->getId(),
                    'vin' => $vehicle->getVin(),
                    'vehicleId' => $vehicle->getVehicleId(),
                ],
            ];
        }

        if(empty($data['vin'] && empty($data['vehicleId']))){
            throw new Exception('VIN Input and Vehicle ID are empty. At least one field have to be filled.');
        }

        $vehicle = $this->createNewVehicle();
        $station = $driverRoute->getStation();
        if (!($station instanceof Station)) {
            throw new Exception('Station not found inside Driver Route.');
        }
        $company = $this->entityManager->getRepository(Company::class)->findOneBy(['id' => $data['companyId'], 'isArchive' => false]);
        if (!($company instanceof Company)) {
            throw new Exception('Company not found.');
        }
        $vehicle->setStation($station);
        $vehicle->setCompany($company);
        if (!empty($data['vin'])) {
            $vehicle->setVin($data['vin']);
        }
        if (!empty($data['vehicleId'])) {
            $vehicle->setVehicleId($data['vehicleId']);
        } else {
            if (!empty($data['vin']) && is_string($data['vin']) && strlen($data['vin']) >= 7) {
                $vehicle->setVehicleId(substr($data['vin'], -6));
            }
        }

        $vehicle->setCurrentUsedBy($user->getFriendlyName());
        $vehicle->setLastUsedByUser($user);
        $datetime = new DateTime();
        if(!empty($data['datetime'])){
            $vehicle->setLastUsedAt(new DateTime($data['datetime']));
        }else{
            $vehicle->setLastUsedAt($datetime);
        }

        $this->entityManager->flush();
        $driverRoute->setVehicle($vehicle);
        $kickoffLog->setVehicle($vehicle);
        $this->entityManager->flush();
        $message = 'KickoffLog Updated. Driver Route Updated. New vehicle created.';
        $this->createEventVehicleAttachedDriverRoute($vehicle, $driverRoute);

        return [
            'success' => true,
            'message' => $message,
            'vehicle' => [
                'id' => $vehicle->getId(),
                'vin' => $vehicle->getVin(),
                'vehicleId' => $vehicle->getVehicleId(),
            ]
        ];
    }

    /**
     * @param array $criteria
     * @param array $data
     * @param KickoffLog $kickoffLog
     * @param DriverRoute $driverRoute
     * @return Vehicle|null
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    private function findVehicleForMobile(array $criteria, array $data, KickoffLog $kickoffLog, DriverRoute $driverRoute)
    {
        $vehicle = $this->_em->getRepository(Vehicle::class)->findOneBy($criteria);
        if ($vehicle instanceof Vehicle) {
            if (!empty($data['scannedQRCodeAt'])) {
                $scannedQRCodeAt = new DateTime($data['scannedQRCodeAt']);
                $kickoffLog->setScannedQRCodeAt($scannedQRCodeAt);
            }
            if (!empty($data['manualEntryVehicleIdentificationAt'])) {
                $manualEntryVehicleIdentificationAt = new DateTime($data['manualEntryVehicleIdentificationAt']);
                $kickoffLog->setManualEntryVehicleIdentificationAt($manualEntryVehicleIdentificationAt);
            }
            $kickoffLog->setScannedQRCode($data['scannedQRCode']);
            $kickoffLog->setVehicle($vehicle);
            $driverRoute->setVehicle($vehicle);
            $this->_em->flush();
        }
        return $vehicle;
    }


    /**
     * @param array $criteria
     * @return Vehicle[]
     */
    public function findVehicleByVin(array $criteria)
    {
        $vin = $criteria['vin'];
        $company = $criteria['company'];
        $isArchive = $criteria['isArchive'];

        $qb = $this->createQueryBuilder('v')
            ->andwhere('v.company = :company')
            ->setParameter('company', $company)
            ->andWhere('v.isArchive = :archive')
            ->setParameter('archive', $isArchive)
            ->andWhere('v.vin LIKE :vin')
            ->setParameter('vin', '%' . $vin . '%');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $criteria
     * @return Vehicle[]
     */
    public function findVehicleByVehicleId(array $criteria)
    {
        $qb = $this->createQueryBuilder('v')
            ->andwhere('v.company = :company')
            ->setParameter('company', $criteria['company'])
            ->andWhere('v.isArchive = :archive')
            ->setParameter('archive', $criteria['isArchive'])
            ->andWhere('v.vehicleId LIKE :vehicleId')
            ->setParameter('vehicleId', '%' . $criteria['vehicleId'] . '%');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $criteria
     * @return Vehicle|null
     */
    public function findOneVehicleByVehicleId(array $criteria)
    {
        $vehicles = $this->findVehicleByVehicleId($criteria);
        $count = sizeof($vehicles);
        if ($count > 0 && $vehicles[0] instanceof Vehicle) {
            return $vehicles[0];
        }
        return null;
    }

    /**
     * @param array $criteria
     * @return Vehicle|null
     */
    public function findOneVehicleByVin(array $criteria)
    {
        $vehicles = $this->findVehicleByVin($criteria);
        $count = sizeof($vehicles);
        if ($count > 0 && $vehicles[0] instanceof Vehicle) {
            return $vehicles[0];
        }
        return null;
    }

    public function vinCheckLength(string $vin)
    {
        if (strlen($vin) > 17) {
            $vinResult = ltrim($vin, 'Ii');
            if (strlen($vinResult) > 17) {
                return substr($vinResult, 0, 17);
            } else {
                return $vinResult;
            }
        }
        return $vin;
    }

    public function getVinFromInput(string $vin)
    {
        $vinResult = $vin;
        if (strlen($vin) > 17) {
            if (strpos($vin, ',') !== false) {
                // Fix comma delimited issues:
                // 1GCWGAFP1L1145188,2020,CG23405,11,19,08,XBTJS3, AK5 AR7 AXK C6P C60 DE5 DRJ EF7 FE9 FE9 FHO GAZ GU6 G80 I20 JH6 K68 LV1 MAH M5U NTB QB5 U0F V8D WEN WML X88 ZLP ZW9 ZX2 1WT 1WT 6AK 7AK 93I 93W,8624,,,,ZY1
                $vinArray = explode(',', $vin);
                $vinResult = $vinArray[0]; // Get the first item in the array - probably the be a VIN number
                $vinResult = $this->vinCheckLength($vinResult);
            } elseif (strpos($vin, '=') !== false) {
                // Fix url issues:
                //  HTTP://ONRAMP.EHI.COM/RAM/PM15/LRMC/US/?V=3C6TRVAG9LE105585
                $vinArray = explode('=', $vin);
                $vinResult = $vinArray[1]; // Get the second item in the array - probably the be a VIN number
                $vinResult = $this->vinCheckLength($vinResult);
            } else {
                return $this->vinCheckLength($vinResult);
            }
        }
        return $vinResult;
    }

    /**
     * @param array $data
     * @return DateTime
     * @throws Exception
     */
    private function manualEntryDate(array $data)
    {
        $date = new DateTime('now');
        if (!empty($data['manualEntryVehicleIdentificationAt'])) {
            $date = new DateTime($data['manualEntryVehicleIdentificationAt']);
        }
        return $date;
    }

    /**
     * @param array $data
     * @return DateTime
     * @throws Exception
     */
    private function scanQRCodeDate(array $data)
    {
        $date = new DateTime('now');
        if (!empty($data['scannedQRCodeAt'])) {
            $date = new DateTime($data['scannedQRCodeAt']);
        }
        return $date;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function validateVehicleInTypeScreen(array $data)
    {
        $payload = 'PAYLOAD: ' . json_encode($data);
        $this->validateVanFields($data);
        $date = $this->manualEntryDate($data);
        $driverRoute = $this->entityManager->getRepository(DriverRoute::class)->findOneBy(['id' => $data['driverRouteId'], 'isArchive' => false]);
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.' . $payload);
        }
        $kickoffLog = $driverRoute->getKickoffLog();
        if (!($kickoffLog instanceof KickoffLog)) {
            throw new Exception('KickoffLog not found.' . $payload);
        }

        if (!empty($data['vin'])) {
            $vehicles = $this->findVehicleByVin(['vin' => $data['vin'], 'company' => $data['companyId'], 'isArchive' => false]);
            $countOfVehicles = count($vehicles);
            $result = [];
            if ($countOfVehicles > 1) {
                foreach ($vehicles as $vehicle) {
                    $result[] = [
                        'id' => $vehicle->getId(),
                        'vin' => $vehicle->getVin(),
                        'vehicleId' => $vehicle->getId(),
                    ];
                }
            }
            if ($countOfVehicles > 0 && $vehicles[0] instanceof Vehicle) {
                $vehicle = $vehicles[0];
                $this->createEventVehicleAttachedDriverRoute($vehicle, $driverRoute);
                return [
                    'success' => true,
                    'message' => 'Vehicle found by VIN.',
                    'vehicle' => [
                        'id' => $vehicle->getId(),
                        'vin' => $vehicle->getVin(),
                    ],
                    'driverRoute' => [
                        'id' => $driverRoute->getId(),
                        'vehicle' => ['id' => $driverRoute->getVehicle()->getId()],
                    ],
                    'kickOffLog' => [
                        'id' => $kickoffLog->getId(),
                        'vehicle' => ['id' => $kickoffLog->getVehicle()->getId()],
                    ],
                    'countOfVehicles' => $countOfVehicles,
                    'vehicles' => $result,
                ];

            }
        }

        if (!empty($data['vehicleId'])) {
            $vehicle = $this->findVehicleForMobile(['vehicleId' => $data['vehicleId'], 'company' => $data['companyId'], 'isArchive' => false], $data, $kickoffLog, $driverRoute);
            if ($vehicle instanceof Vehicle) {
                $this->createEventVehicleAttachedDriverRoute($vehicle, $driverRoute);
                return [
                    'success' => true,
                    'message' => 'Vehicle found by vehicleId.',
                    'vehicle' => [
                        'id' => $vehicle->getId(),
                        'vehicleId' => $vehicle->getVehicleId(),
                    ],
                    'driverRoute' => [
                        'id' => $driverRoute->getId(),
                        'vehicle' => ['id' => $driverRoute->getVehicle()->getId()],
                    ],
                    'kickOffLog' => [
                        'id' => $kickoffLog->getId(),
                        'vehicle' => ['id' => $kickoffLog->getVehicle()->getId()],
                    ],
                ];
            }
        }

        $vehicle = $this->createNewVehicle();
        if (!empty($data['vin'])) {
            $vehicle->setVin($data['vin']);
        }
        if (!empty($data['vehicleId'])) {
            $vehicle->setVehicleId($data['vehicleId']);
        }
        $station = $driverRoute->getStation();
        $company = $station->getCompany();
        if (!($station instanceof Station)) {
            throw new Exception('Station not found inside DriverRoute ID: ' . $driverRoute->getId());
        }
        if (!($company instanceof Company)) {
            throw new Exception('Company not found inside Station ID: ' . $station->getId());
        }
        $vehicle->setStation($station);
        $vehicle->setCompany($company);
        $this->entityManager->flush();

        $kickoffLog->setVehicle($vehicle);
        $driverRoute->setVehicle($vehicle);
        $this->entityManager->flush();

        $this->createEventVehicleAttachedDriverRoute($vehicle, $driverRoute);

        return [
            'success' => true,
            'message' => 'New Vehicle created.',
            'vehicle' => [
                'id' => $vehicle->getId(),
                'vehicleId' => $vehicle->getVehicleId(),
                'vin' => $vehicle->getVin(),
            ],
            'driverRoute' => [
                'id' => $driverRoute->getId(),
                'vehicleId' => $driverRoute->getVehicle()->getId(),
            ],
            'kickOffLog' => [
                'id' => $kickoffLog->getId(),
                'vehicleId' => $kickoffLog->getVehicle()->getId(),
            ]
        ];
    }


    /**
     * @param array $data
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function createNewVan(array $data)
    {
        if (empty($data['driverRouteId'])) {
            throw new Exception('driverRouteId is missing.');
        }
        $date = $this->manualEntryDate($data);
        $vehicle = $this->createNewVehicle();
        if (!empty($data['vin'])) {
            $vehicle->setVin($data['vin']);
        }
        if (!empty($data['vehicleId'])) {
            $vehicle->setVehicleId($data['vehicleId']);
        }
        $driverRoute = $this->entityManager->getRepository(DriverRoute::class)->findOneBy(['id' => $data['driverRouteId'], 'isArchive' => false]);
        if (!($driverRoute instanceof DriverRoute)) {
            throw new Exception('DriverRoute not found.');
        }
        $kickoffLog = $driverRoute->getKickoffLog();
        if (!($kickoffLog instanceof KickoffLog)) {
            throw new Exception('KickoffLog not found.');
        }
        $station = $driverRoute->getStation();
        $company = $station->getCompany();
        if (!($station instanceof Station)) {
            throw new Exception('Station not found inside DriverRoute ID: ' . $driverRoute->getId());
        }
        if (!($company instanceof Company)) {
            throw new Exception('Company not found inside Station ID: ' . $station->getId());
        }
        $vehicle->setStation($station);
        $vehicle->setCompany($company);
        $this->entityManager->flush();

        $kickoffLog->setVehicle($vehicle);
        $driverRoute->setVehicle($vehicle);
        $this->entityManager->flush();

        $this->createEventVehicleAttachedDriverRoute($vehicle, $driverRoute);

        return [
            'success' => true,
            'message' => 'New Vehicle created.',
            'vehicle' => [
                'id' => $vehicle->getId(),
                'vehicleId' => $vehicle->getVehicleId(),
                'vin' => $vehicle->getVin(),
            ],
            'driverRoute' => [
                'id' => $driverRoute->getId(),
                'vehicle' => ['id' => $driverRoute->getVehicle()->getId()],
            ],
            'kickOffLog' => [
                'id' => $kickoffLog->getId(),
                'vehicle' => ['id' => $kickoffLog->getVehicle()->getId()],
            ]
        ];
    }


    /**
     * @param Vehicle $vehicle
     * @param DriverRoute $driverRoute
     */
    private function createEventVehicleAttachedDriverRoute(Vehicle $vehicle, DriverRoute $driverRoute)
    {

        $vehicleIdentifier = $vehicle->getId();
        $eventMessage = 'Driver Route used vehicle. - ID' . $vehicleIdentifier;
        $shiftName = $driverRoute->getShiftName();
        if (!is_string($shiftName)) {
            $shiftName = 'Driver Route - ID: ' . $driverRoute->getId();
        }
        if ($vehicle->getVehicleId()) {
            $vehicleIdentifier = $vehicle->getVehicleId();
            $eventMessage = $shiftName . ' used vehicle - VehicleID: ' . $vehicleIdentifier;
        } elseif ($vehicle->getVin()) {
            $vehicleIdentifier = $vehicle->getVin();
            $eventMessage = $shiftName . ' used vehicle - VIN: ' . $vehicleIdentifier;
        }
        $event = $this->entityManager->getRepository(Event::class)->createEvent('VEHICLE_ATTACHED', $eventMessage, $eventMessage, null, $driverRoute, $vehicle, null);
        $driverRoute->addEvent($event);
        $this->entityManager->flush();
    }

    public function updateStationIsArchiveInVehicle($stationId)
    {

        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Vehicle','v')
                    ->set('v.isArchive', true)
                    ->where('v.station = :stationId')
                    ->setParameter( 'stationId', $stationId )
                    ->getQuery()
                    ->execute();
        return true;
    }

    public function getVehicleLatestMilageByDate($companyId, $startDate, $stationCodes)
    {

        $stationInCriteria = '';
        if (!empty($stationCodes)) {
            $stationCodes = "'" . implode( "','", $stationCodes ) . "'";
            $stationInCriteria = " AND s.code IN (".$stationCodes.")";
        }

        $sqlQuery = "SELECT
            s.code,v.vin,rts.mileage,rts.logged_mileage_and_gas_at,u.friendly_name,v.type
            FROM
                vehicle v
            LEFT JOIN
                driver_route dr
            ON
                (v.id = dr.vehicle_id AND dr.date_created = (SELECT MAX(date_created) FROM driver_route where date_created <= '".$startDate."' AND `vehicle_id`=v.id limit 1) AND dr.is_archive = 0)
            LEFT JOIN
                return_to_station rts
            ON
                dr.return_to_station_id = rts.id
            LEFT JOIN
                station s
            ON
                v.station_id = s.id
            LEFT JOIN
                driver dri
            ON
                dr.driver_id = dri.id
            LEFT JOIN
                user u
            ON
                dri.user_id = u.id
            WHERE
                s.company_id = ".$companyId." AND v.is_archive = 0 AND s.is_archive = 0".$stationInCriteria." GROUP BY v.id ORDER BY s.code,dr.date_created DESC";

          return  $this->getEntityManager()
                    ->getConnection()
                    ->query($sqlQuery)
                    ->fetchAll();

    }

    public function getVanMileageReport($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        $spreadsheet = new Spreadsheet();

        /* @var $sheet \PhpOffice\PhpSpreadsheet\Writer\Xlsx\Worksheet */
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Van Mileage Report For ' . $criteria['startDate']);
        $sheet->setCellValue('A4', 'Station');
        $sheet->setCellValue('B4', 'VIN');
        $sheet->setCellValue('C4', 'Mileage');
        $sheet->setCellValue('D4', 'Last Driven By');
        $sheet->setCellValue('E4', 'Last Date Driven');
        $sheet->setCellValue('F4', 'Ownership Details');

        $sheet->getStyle('A4:F4')->applyFromArray(['font' => ['bold' => true, 'size' => 10]]);
        $sheet->mergeCells('A1:H3');
        $sheet->getStyle('A1')->applyFromArray(['font' => ['size' => 18]]);

        $stationCodes = isset($criteria['stationCode']) ? $criteria['stationCode'] : '';
        $vehicles = $this->getVehicleLatestMilageByDate($criteria['companyId'],$criteria['startDate'],$stationCodes);
        
        $counter = 5;
        foreach ($vehicles as $vehicle) {
                $lastDrivenBy = isset($vehicle['mileage']) ? $vehicle['friendly_name'] : '';
                $loggedMileageAndGasAt = isset($vehicle['mileage']) ? $vehicle['logged_mileage_and_gas_at'] : '';
                $sheet->setCellValue('A' . $counter, $vehicle['code']);
                $sheet->setCellValue('B' . $counter, $vehicle['vin']);
                $sheet->setCellValue('C' . $counter, $vehicle['mileage']);
                $sheet->setCellValue('D' . $counter, $lastDrivenBy);
                $sheet->setCellValue('E' . $counter, $loggedMileageAndGasAt);
                $sheet->setCellValue('F' . $counter, $vehicle['type']);
                $counter++;
        }

        //Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // In this case, we want to write the file in the public directory
        $publicDirectory = $this->params->get('kernel.project_dir') . '/public';
        // e.g /var/www/project/public/my_first_excel_symfony4.xlsx

        $currentDate = new \DateTime();
        $cDate = $currentDate->format('Y_m_d_H_i') . '_van_mileage_report_sheet.xlsx';

        $excelFilepath = $publicDirectory . '/excelsheet/' . $cDate;

        // Create the file
        $writer->save($excelFilepath);

        $s3result = $this->s3->uploadFile($excelFilepath, $cDate, 'dsp.data.storage.general');

        if (is_string($s3result)) {
            throw new Exception($s3result);
        }

        unlink($excelFilepath);

        return $s3result['ObjectURL'];
    }
}
