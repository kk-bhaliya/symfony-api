<?php

namespace App\Repository;

use App\Entity\Role;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Role|null find($id, $lockMode = null, $lockVersion = null)
 * @method Role|null findOneBy(array $criteria, array $orderBy = null)
 * @method Role[]    findAll()
 * @method Role[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Role::class);
    }

    public function getRolesNameByCompany($companyId)
    {
        $roles = $this->createQueryBuilder('r')
            ->select('r.name')
            ->where('r.company = :company')
            ->setParameter('company', $companyId)
            ->andWhere('r.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('r.name IS NOT NULL')
            ->getQuery()->getResult();

        return array_map(function ($role) {
            $roleName = str_replace('ROLE_', "", $role['name']);
            $roleName = str_replace("_", " ", $roleName);
            $roleName = ucwords(strtolower($roleName));
            $roleName = str_replace('Hr ', 'HR ', $roleName);
            return $roleName;
        }, $roles);
    }
}
