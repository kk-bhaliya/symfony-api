<?php

namespace App\Repository;

use App\Entity\Driver;
use App\Entity\Schedule;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use App\Services\EmailService;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Utils\GlobalUtility;

/**
 * @method Schedule|null find($id, $lockMode = null, $lockVersion = null)
 * @method Schedule|null findOneBy(array $criteria, array $orderBy = null)
 * @method Schedule[]    findAll()
 * @method Schedule[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleRepository extends ServiceEntityRepository
{

    private $params;

    private $emailService;

    private $kernel;

    private $globalUtils;

    public function __construct(ManagerRegistry $registry,ContainerBagInterface $params, EmailService $emailService, KernelInterface $kernel, GlobalUtility $globalUtils)
    {
        parent::__construct($registry, Schedule::class);
        $this->params = $params;
        $this->emailService = $emailService;
        $this->kernel = $kernel;
        $this->globalUtils = $globalUtils;
    }

    public function getScheduleByStation($stationId)
    {
        $results = $this->createQueryBuilder('sh')
                         ->select('sh.id')
                         ->andWhere('sh.station = :station')
                         ->setParameter('station', $stationId )
                         ->andWhere('sh.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->getQuery()
                         ->getResult(Query::HYDRATE_ARRAY);
        return array_column($results, 'id');
    }

    public function updateStationIsArchiveInSchedule($stationId)
    {

        $scheduleIds = $this->getScheduleByStation($stationId);
        if (!empty($scheduleIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\Schedule','sh')
                            ->set('sh.isArchive', true)
                            ->where($qb->expr()->in('sh.id', ':scheduleIds'))
                            ->setParameter( 'scheduleIds', $scheduleIds )
                            ->getQuery()
                            ->execute();

            //Call ScheduleDesign and driver route and temp driver route isArchive true set
            $this->getEntityManager()->getRepository('App\Entity\ScheduleDesign')->updateScheduleIsArchiveInScheduleDesign($scheduleIds);
            $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->updateScheduleIsArchiveInDriverRoute($scheduleIds);
            $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->updateScheduleIsArchiveInTempDriverRoute($scheduleIds);
        }
        return true;
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getScheduleByShift($criteria)
    {
        $results = $this->createQueryBuilder('s')
                        ->select('s.id,s.name')
                        ->leftJoin('s.scheduleDesigns','sd')
                        ->leftJoin('sd.shifts','shift')
                        ->where('shift.skill IN (:skillFilter)')
                        ->setParameter('skillFilter', $criteria['skills'])
                        ->andWhere('s.station = :station')
                        ->setParameter('station', $criteria['station'])
                        ->andWhere('s.company = :company')
                        ->setParameter('company', $criteria['company'])
                        ->andWhere('s.isArchive = :isArchive')
                        ->setParameter('isArchive',false)
                        ->groupBy('s.id');

        return $results->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getSchedulesNext14days($sheduleId = null, $days = null)
    {
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        if(!$days){
          $days = 14;
        }
        $date->modify('+'.$days.' day');
        $results = $this->createQueryBuilder('s')
            ->where('
                   (s.startDate  = :startDate) OR
                   (s.scheduleLastDate = :endDate) OR
                   (s.startDate  = :endDate) OR
                   (s.scheduleLastDate = :startDate) OR
                    (s.startDate  <= :startDate AND :startDate <= s.scheduleLastDate) OR
                    (s.startDate  <= :endDate AND :endDate <= s.scheduleLastDate) OR
                    (:startDate <= s.startDate  AND s.startDate  <= :endDate) OR
                    (:startDate <= s.scheduleLastDate AND s.scheduleLastDate <= :endDate)')
            ->setParameter('startDate', (new \DateTime())->setTimezone(new \DateTimeZone('UTC')))
            ->setParameter('endDate', $date)
            ->andWhere('s.isArchive = :isArchive')
            ->setParameter('isArchive',false);
        if($sheduleId){
          $results->andWhere('s.id = :ID')
                  ->setParameter('ID',$sheduleId);
        }
        return $results->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function getRecurringSchedule()
    {
        $results = $this->createQueryBuilder('s')
                        ->leftJoin('s.station','st')
                        ->leftJoin('s.company','co')
                        ->where('s.isRecurring = :recurring')
                        ->setParameter('recurring', true)
                        ->andWhere('s.isArchive = :isArchive')
                        ->setParameter('isArchive',false)
                        ->andWhere('s.startDate < CURRENT_DATE()')
                        ->andWhere('st.isArchive = :isStationArchive')
                        ->setParameter('isStationArchive',false)
                        ->andWhere('co.isArchive = :isCompanyArchive')
                        ->setParameter('isCompanyArchive',false);

        return $results->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function getRecurringScheduleForMail()
    {
        $date = date('Y-m-d', strtotime('+2 days'));

        $results = $this->createQueryBuilder('s')
                        ->leftJoin('s.station','st')
                        ->leftJoin('s.company','co')
                        ->where('s.isRecurring = :recurring')
                        ->setParameter('recurring', true)
                        ->andWhere('s.isArchive = :isArchive')
                        ->setParameter('isArchive',false)
                        ->andWhere('s.scheduleLastDate = :date')
                        ->setParameter('date',$date)
                        ->andWhere('st.isArchive = :isStationArchive')
                        ->setParameter('isStationArchive',false)
                        ->andWhere('co.isArchive = :isCompanyArchive')
                        ->setParameter('isCompanyArchive',false);

        return $results->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function sendMailScheduleCreate($param)
    {
        $schedules = $this->getEntityManager()->getRepository('App\Entity\Schedule')->find($param['sheduleId']);

        if ($schedules->getIsRecurring()) {
            $application = new Application($this->kernel);
            $application->setAutoExit(false);

            $input = new ArrayInput(['command' => 'app:schedule-recurring', 'scheduleId' => $schedules->getId()]);
            $output = new BufferedOutput();
            $application->run($input, $output);
        }

        $subject = 'Schedule Design Changes - #'.$schedules->getId();
        $reccuring = $schedules->getIsRecurring()== 1 ? 'true' : 'false';
        $endDate = $schedules->getEndDate()->format('Y-m-d');
        $schedulerLastDate = $schedules->getScheduleLastDate() ? $schedules->getScheduleLastDate()->format('Y-m-d'):'-';
        $action = $param['isEdit'] == 1 ? 'Edit' : 'Add';
        $content = 'Action:- '.$action.'<br/> Name:- '.$schedules->getName().'<br/> Recurring : '.$reccuring.'<br/> Start Date : '.$schedules->getStartDate()->format('Y-m-d').'<br/> End Date : '.$endDate.'<br/> Created Date : '.$schedules->getCreatedDate()->format('Y-m-d').'<br/> Scheduler Last Date : '.$schedulerLastDate.'<br/> Start Week:- '.$schedules->getStartWeek().'<br/> End Week:- '.$schedules->getEndWeek().'<br/> Station Id:- '.$schedules->getStation()->getId().'<br/> Station Name:- '.$schedules->getStation()->getName().'<br/> Company Id:- '.$schedules->getCompany()->getId().'<br/> Company Name:- '.$schedules->getCompany()->getName().'<br/> Original Week:- '.$schedules->getOriginalWeekCount().'<br/> Week End Year :'.$schedules->getWeekEndYear();
        $url = "https://".$_SERVER['HTTP_HOST'];
        //$this->emailService->sendEmail(['parth@corephp.com','yogesh.bhatt2009@gmail.com'], 'URL', $url);
        if($url == 'https://api.dspworkplace.com' or $url == 'https://devapi.dspworkplace.com'){
                $this->emailService->sendEmail(['parth@corephp.com','yogesh.bhatt2009@gmail.com','trivedibrijesh2135@gmail.com'], $subject.' ('.$url.')', $content.'<pre> <code class="prettyprint">'.json_encode($param['schedule']).'</pre> </code>');
        }
        return $url;
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function getLatestAllSchedule()
    {
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $weekNumber = $date->format("W");
        $year = $date->format("Y");
        $results = $this->createQueryBuilder('s')
                        ->leftJoin('s.scheduleDesigns','sd')
                        ->innerJoin('s.updatedSchedules','us')
                        ->where('(sd.week >= :week AND sd.year = :year) OR (sd.week < :week AND sd.year > :year)')
                        ->setParameter('week', $weekNumber)
                        ->setParameter('year', $year)
                        ->andWhere('s.isArchive = :isArchive')
                        ->andWhere('us.status = 0')
                        ->setParameter('isArchive',false)
                        ->orderBy('s.dateUpdated','ASC')
                        ->setMaxResults(1);
        return $results->getQuery()->getOneOrNullResult();
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getEmployeeBalanceDrivers($criteria)
    {
        $results = $this->createQueryBuilder('s')
                        ->leftJoin('s.scheduleDesigns','sd')
                        ->leftJoin('s.station','st')
                        ->innerJoin('s.drivers','d')
                        ->where('sd.week = :currentweek')
                        ->setParameter('currentweek', $criteria['week'])
                        ->andWhere('s.company = :company')
                        ->setParameter('company', $criteria['company'])
                        ->andWhere('s.isArchive = :isArchive')
                        ->setParameter('isArchive',false)
                        ->andWhere('st.isArchive = :isArchiveStation')
                        ->setParameter('isArchiveStation',false)
                        ->andWhere('d.isArchive = :isArchive')
                        ->setParameter('isArchive', false)
                        ->andWhere('d.isEnabled = :isEnabled')
                        ->setParameter('isEnabled', true)
                        ->andWhere('d.employeeStatus = :employeeStatus')
                        ->setParameter('employeeStatus', Driver::STATUS_ACTIVE);
                        
        return $results->getQuery()->getResult();
    }

    public function getScheduleImportData($name, $company, $sDate, $eDate, $recurring, $station, $archive, $startWeek, $endWeek, $year, $weekEndYear, $originalWeekCount, $scheduleLastDate, $previousAssignSchedule)
    {
        $em = $this->getEntityManager();

        $schedule = new Schedule();
        $schedule->setName($name);
        $schedule->setCompany($company);
        $schedule->setCreatedDate(new \DateTime());
        $schedule->setStartDate(new \DateTime($sDate));
        $schedule->setEndDate(new \DateTime($eDate));
        $schedule->setIsRecurring($recurring);
        $schedule->setStation($station);
        $schedule->setIsArchive($archive);
        $schedule->setStartWeek($startWeek);
        $schedule->setEndWeek($endWeek);
        $schedule->setYear($year);
        $schedule->setWeekEndYear($weekEndYear);
        $schedule->setOriginalWeekCount($originalWeekCount);
        $schedule->setScheduleLastDate(new \DateTime($scheduleLastDate));
        $schedule->setPreviousAssignSchedule($previousAssignSchedule);
        $em->persist($schedule);
        $em->flush();

        return $schedule;
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */

    public function getStationWiseSchedule($criteria)
    {
        $results = $this->createQueryBuilder('s')
                        ->select('s.id,s.name,s.isRecurring,s.startDate,s.endDate,s.previousAssignSchedule,s.originalWeekCount,s.isArchive,count(DISTINCT dri.id) as drivers,count(DISTINCT shift.id) as sft')
                        ->leftJoin('s.drivers','dri')
                        ->leftJoin('s.scheduleDesigns','sd')
                        ->leftJoin('sd.shifts','shift')
                        ->where('s.station = :station')
                        ->setParameter('station', $criteria['station'])
                        ->andWhere('s.isArchive = :isArchive')
                        ->setParameter('isArchive',false)
                        ->andWhere('dri.isArchive = :driverIsArchive')
                        ->setParameter('driverIsArchive',false)
                        ->andWhere('sd.isArchive = :sdIsArchive')
                        ->setParameter('sdIsArchive',false)
                        ->andWhere('shift.isArchive = :shiftIsArchive')
                        ->setParameter('shiftIsArchive',false)
                        ->orderBy('s.name')
                        ->groupBy('s.id');
        return $results->getQuery()->getArrayResult();
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getScheduleDetail($criteria)
    {
        $results = $this->createQueryBuilder('s')
                        ->select('s.id,s.name,s.isRecurring,s.startDate,s.endDate,s.previousAssignSchedule,s.originalWeekCount,s.isArchive')
                        ->where('s.id = :scheduleId')
                        ->setParameter('scheduleId', $criteria['id']);
        $schedule = $results->getQuery()->getOneOrNullResult(\Doctrine\ORM\AbstractQuery::HYDRATE_ARRAY);
        $schedule['scheduleDesigns'] = $this->getEntityManager()->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignFromScheduleId($criteria['id']);
        $schedule['drivers'] = $this->getEntityManager()->getRepository('App\Entity\Driver')->getScheduleDriverFromScheduleId($criteria['id']);
        $driverArray = [];
        $schedule['driversWithSchedules']=[];
        if(count($schedule['drivers']) > 0 ){
            foreach($schedule['drivers'] as $driver){
                $driverArray[]= $driver['id'];
            }
            $schedule['driversWithSchedules'] = $this->getEntityManager()->getRepository('App\Entity\Driver')->getDriversWithSchedules($driverArray);
        }
        return $schedule;        
    }

    public function getStationWiseSchedules($criteria)
    {
        $scheduleQr = $this->createQueryBuilder('s')
                        ->where('s.station = :stations')
                        ->setParameter('stations',$criteria['stationId'])
                        ->andWhere('s.isArchive = :isStationArchive')
                        ->setParameter('isStationArchive',false);
       $results = $scheduleQr->getQuery()->getArrayResult();
       $resultArray=[];
       $count=0;
       foreach($results as $key =>$result){ 

            $resultArray[$key]['id'] = $result['id'];
            $resultArray[$key]['name'] = $result['name'];
            $resultArray[$key]['isRecurring'] = $result['isRecurring'];
            $resultArray[$key]['startDate'] = $result['startDate'];
            $resultArray[$key]['endDate'] = $result['endDate'];
            $resultArray[$key]['previousAssignSchedule'] = $result['previousAssignSchedule'];
            $resultArray[$key]['originalWeekCount'] = $result['originalWeekCount'];
            $resultArray[$key]['isArchive'] = $result['isArchive'];

            $qb = $this->getEntityManager()->createQueryBuilder();
            $scheduleSd = $qb->select('sd.id,sd.week,sd.sunday,sd.monday,sd.tuesday,sd.wednesday,sd.thursday,sd.friday,sd.saturday,sd.ordering')
                    ->from('App\Entity\ScheduleDesign', 'sd')
                    ->where('sd.schedule = :scheduleId')
                    ->setParameter('scheduleId',$result['id'])
                    ->andWhere('sd.isArchive = :isStationArchive')
                    ->setParameter('isStationArchive',false);
            $sdResults = $scheduleSd->getQuery()->getArrayResult();
            $scheduleDesignArray=[];
            foreach ($sdResults as $designKey => $sdResult) {
                $scheduleDesignArray[$designKey]['id'] = $sdResult['id'];
                $scheduleDesignArray[$designKey]['week'] = $sdResult['week'];
                $scheduleDesignArray[$designKey]['sunday'] = $sdResult['sunday'];
                $scheduleDesignArray[$designKey]['monday'] = $sdResult['monday'];
                $scheduleDesignArray[$designKey]['tuesday'] = $sdResult['tuesday'];
                $scheduleDesignArray[$designKey]['wednesday'] = $sdResult['wednesday'];
                $scheduleDesignArray[$designKey]['thursday'] = $sdResult['thursday'];
                $scheduleDesignArray[$designKey]['friday'] = $sdResult['friday'];
                $scheduleDesignArray[$designKey]['saturday'] = $sdResult['saturday'];
                $scheduleDesignArray[$designKey]['ordering'] = $sdResult['ordering'];

                $sdShifts = $this->getEntityManager()->createQueryBuilder();
                $sdShifts->select('shift.id,shift.name,shift.startTime,shift.endTime,shift.unpaidBreak,shift.note,shift.color,shift.category')
                                ->from('App\Entity\Shift', 'shift')
                                ->leftJoin('shift.scheduleDesigns', 'sd')
                                ->where('sd.id = :scheduleDesignId')
                                ->setParameter('scheduleDesignId',$sdResult['id'])
                                ->andWhere('shift.isArchive = :isStationArchive')
                                ->setParameter('isStationArchive',false);
                $sdShiftResults = $sdShifts->getQuery()->getArrayResult(); 
                $designShiftArray=[];     
                foreach ($sdShiftResults as $shiftKey => $sdShiftResult) {
                    $skillQr = $this->getEntityManager()->createQueryBuilder();
                    $skillQr->select('skill.id,skill.name')
                                    ->from('App\Entity\Skill', 'skill')
                                    ->leftJoin('skill.shifts', 'sf')
                                    ->where('sf.id = :shiftId')
                                    ->setParameter('shiftId',$sdShiftResult['id'])
                                    ->andWhere('skill.isArchive = :isStationArchive')
                                    ->setParameter('isStationArchive',false);
                    $skillResults = $skillQr->getQuery()->getArrayResult();  
                    $skillTempArray=[];
                    foreach ($skillResults as $skillKey => $skillResult) {
                        $skillTempArray=['id'=>$skillResult['id'],'name'=>$skillResult['name']];
                    }

                    $designShiftArray[$shiftKey]['id']=$sdShiftResult['id'];
                    $designShiftArray[$shiftKey]['name']=$sdShiftResult['name'];
                    $designShiftArray[$shiftKey]['startTime']=$sdShiftResult['startTime'];
                    $designShiftArray[$shiftKey]['endTime']=$sdShiftResult['endTime'];
                    $designShiftArray[$shiftKey]['unpaidBreak']=$sdShiftResult['unpaidBreak'];
                    $designShiftArray[$shiftKey]['note']=$sdShiftResult['note'];
                    $designShiftArray[$shiftKey]['color']=$sdShiftResult['color'];
                    $designShiftArray[$shiftKey]['category']=$sdShiftResult['category'];
                    $designShiftArray[$shiftKey]['skill']=$skillTempArray;
                }
                $scheduleDesignArray[$designKey]['shifts'] = $designShiftArray;                
            }
            $resultArray[$key]['scheduleDesigns'] = $scheduleDesignArray;

            $qb = $this->getEntityManager()->createQueryBuilder();
            $driverObj = $qb->select('d.id,d.jobTitle,d.employeeStatus,u.id as userId,u.firstName,u.profileImage,u.phoneNumber,u.isArchive,u.profileImage,u.lastName')
                    ->from('App\Entity\Driver', 'd')
                    ->leftJoin('d.schedules', 's')
                    ->leftJoin('d.user', 'u')
                    ->where('s.id = :scheduleId')
                    ->setParameter('scheduleId',$result['id'])
                    ->andWhere('d.isArchive = :isStationArchive')
                    ->setParameter('isStationArchive',false)
                    ->andWhere('d.employeeStatus = :employeeStatus')
                    ->setParameter('employeeStatus',Driver::STATUS_ACTIVE);
            $driverResults = $driverObj->getQuery()->getArrayResult();
            $driverArray=[];
            foreach ($driverResults as $driverKey => $driverResult) {
                $driverArray[$driverKey]['id'] = $driverResult['id'];
                $driverArray[$driverKey]['jobTitle']=$driverResult['jobTitle'];
                $driverArray[$driverKey]['employeeStatus']=$driverResult['employeeStatus'];                
                
                $roleQr = $this->getEntityManager()->createQueryBuilder();
                $roleQr->select('role.name')
                                ->from('App\Entity\Role', 'role')
                                ->leftJoin('role.users', 'u')
                                ->where('u.id = :userId')
                                ->setParameter('userId',$driverResult['userId']);
                $roleResults = $roleQr->getQuery()->getArrayResult();
                $roleTempArray = [];
                foreach ($roleResults as $roleKey=> $roleResult) {
                    $roleTempArray[$roleKey] ['name']= $roleResult['name'];
                    $roleTempArray[$roleKey] ['roleName']= $this->globalUtils->getPosition($roleResult['name']);
                }
                $driverArray[$driverKey]['user']=[
                    "id"=>$driverResult['userId'],
                    "firstName"=>$driverResult['firstName'],
                    "image"=>$driverResult['profileImage'],
                    "phoneNumber"=>$driverResult['phoneNumber'],
                    "isArchive"=>$driverResult['isArchive'],
                    "profileImage"=>$driverResult['profileImage'],
                    "lastName"=>$driverResult['lastName'],
                    "userRoles"=>$roleTempArray
                ];
                $roleQr = $this->getEntityManager()->createQueryBuilder();
                $roleQr->select('ds.id,ds.hourlyRate,ds.isArchive')
                                ->from('App\Entity\DriverSkill', 'ds')
                                ->where('ds.driver = :driverId')
                                ->setParameter('driverId',$driverResult['id']);
                $driverSkillResults = $roleQr->getQuery()->getArrayResult();
                foreach ($driverSkillResults as $driverSkillResult) {                
                    $skillQr = $this->getEntityManager()->createQueryBuilder();
                    $skillQr->select('skill.id,skill.name,skill.isArchive')
                                    ->from('App\Entity\Skill', 'skill')
                                    ->leftJoin('skill.driverSkills', 'ds')
                                    ->where('ds.id = :driverId')
                                    ->setParameter('driverId',$driverSkillResult['id'])
                                    ->andWhere('skill.isArchive = :isStationArchive')
                                    ->setParameter('isStationArchive',false);
                    $skillResults = $skillQr->getQuery()->getArrayResult();
                    $tempSkillArray = [];
                    foreach ($skillResults as $skillKey => $skillResult) {
                        $tempSkillArray[$skillKey]['id']=$skillResult['id'];
                        $tempSkillArray[$skillKey]['name']=$skillResult['name'];
                        $tempSkillArray[$skillKey]['isArchive']=$skillResult['isArchive'];
                    }     
                    
                    $driverArray[$driverKey]['driverSkills']=[
                        "id"=>$driverSkillResult['id'],
                        "hourlyRate"=>$driverSkillResult['hourlyRate'],
                        "isArchive"=>$driverSkillResult['isArchive'],
                        "skill"=>$tempSkillArray                        
                    ];
                }                
            }
            $resultArray[$key]['drivers'] = $driverArray;           
            $resultArray[$key]['driverRoutes'] = [];
       }
       
       return $resultArray;
    }

    public function getSchedulesNamesByDriver($driverId)
    {
        $scalarResult = $this->createQueryBuilder('schedule')
            ->leftJoin('schedule.drivers', 'drivers')
            ->select('schedule.name')
            ->andWhere('drivers.id = :driverId')
            ->setParameter('driverId', $driverId)
            ->andWhere('schedule.isArchive = 0')
            ->andWhere('drivers.isArchive = 0')
            ->getQuery()->getScalarResult();

        $result = [];

        foreach ($scalarResult as $item){
            if(!empty($item['name'])){
                $result[] = $item['name'];
            }
        }

        return $result;
    }
}
