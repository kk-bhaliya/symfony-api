<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\PaycomSettings;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaycomSettings|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaycomSettings|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaycomSettings[]    findAll()
 * @method PaycomSettings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaycomSettingsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaycomSettings::class);
    }

    public function findEnabledAccounts()
    {
        return $this->createQueryBuilder('ps')
            ->andWhere('ps.isEnabled = 1')
            ->join('ps.company', 'c')
            ->addCriteria( Common::notArchived('c') )
            ->getQuery()
            ->getResult()
        ;
    }
}
