<?php

namespace App\Repository;

use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\Location;
use App\Entity\User;
use App\Entity\Vehicle;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Utils\GlobalUtility;


/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $globalUtils;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage, GlobalUtility $globalUtils, EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->globalUtils = $globalUtils;
        parent::__construct($registry, Event::class);
        $this->em = $em;
    }

    public function createPunchEvents(string $punchType, DriverRoute $driverRoute)
    {
        try {
            if ($punchType === 'PunchIn' && is_null($driverRoute->getPunchIn()) === false) {
                return null;
            }
            if ($punchType === 'BreakPunchIn' && is_null($driverRoute->getDBBreakPunchIn()) === false) {
                return null;
            }
            if ($punchType === 'BreakPunchOut' && is_null($driverRoute->getBreakPunchOut()) === false) {
                return null;
            }
            if ($punchType === 'PunchOut' && is_null($driverRoute->getDBPunchOut()) === false) {
                return null;
            }

            $driver = $driverRoute->getDriver();
            if ($driver instanceof Driver) {
                $user = $driver->getUser();
                if ($user instanceof User) {
                    $event = new Event();
                    $event->setDriverRoute($driverRoute);
                    $event->setUser($user);
                    $name = 'N/A';
                    $eventName = 'N/A';
                    $message = 'N/A';
                    $driver = $user->getFriendlyName();
                    switch ($punchType) {
                        case 'PunchIn':
                            $name = 'PUNCH_IN';
                            $eventName = 'Punch In';
                            $message = $driver . ' punched into Paycom.';
                            break;

                        case 'BreakPunchIn':
                            $name = 'BREAK_PUNCH_IN';
                            $eventName = 'Break Punch In';
                            $message = $driver . ' took its break.';
                            break;

                        case 'BreakPunchOut':
                            $name = 'BREAK_PUNCH_OUT';
                            $eventName = 'Break Punch Out';
                            $message = $driver . ' finished its break.';
                            break;

                        case 'PunchOut':
                            $name = 'PUNCH_OUT';
                            $eventName = 'Punch Out';
                            $message = $driver . ' punched out Paycom.';
                            break;
                    }
                    $event->setName($name);
                    $event->setEventName($eventName);
                    $event->setMessage($message);
                    $event->setCreatedAt(new DateTime());
                    $this->em->persist($event);
                    $this->em->flush();
                    return $event;
                }
            }
            return null;
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * @param User $user
     * @param DriverRoute $driverRoute
     * @param string $name
     * @param string $eventName
     * @param string $message
     * @return Event
     */
    public function createDriverRouteEvent(User $user, DriverRoute $driverRoute, string $name, string $eventName, string $message)
    {
        $event = new Event();
        $event->setDriverRoute($driverRoute);
        $event->setUser($user);
        $event->setName($name);
        $event->setEventName($eventName);
        $event->setMessage($message);
        $event->setCreatedAt(new DateTime());
        $this->em->persist($event);
        $this->em->flush();
        return $event;
    }

    public function createPunchTypesEvents(DriverRoute $driverRoute)
    {
        try {
            $punchType = null;
            $driver = $driverRoute->getDriver();
            if (!($driver instanceof Driver)) {
                throw new Exception('Driver Route has no Driver.');
            }
            $user = $driver->getUser();
            if (!($user instanceof User)) {
                throw new Exception('Driver Route has no Driver and User.');
            }
            $friendlyName = $user->getFriendlyName();
            $events = [];
            if (is_null($driverRoute->getPunchIn()) === false) {
                $name = 'PUNCH_IN';
                $eventName = 'Punch In';
                $message = $friendlyName . ' punched into Paycom.';
                $events[] = $this->createDriverRouteEvent($user, $driverRoute, $name, $eventName, $message);

            }
            if (is_null($driverRoute->getDBBreakPunchIn()) === false) {
                $name = 'BREAK_PUNCH_IN';
                $eventName = 'Break Punch In';
                $message = $friendlyName . ' took its break.';
                $events[] = $this->createDriverRouteEvent($user, $driverRoute, $name, $eventName, $message);
            }
            if (is_null($driverRoute->getBreakPunchOut()) === false) {
                $name = 'BREAK_PUNCH_OUT';
                $eventName = 'Break Punch Out';
                $message = $friendlyName . ' finished its break.';
                $events[] = $this->createDriverRouteEvent($user, $driverRoute, $name, $eventName, $message);
            }
            if (is_null($driverRoute->getDBPunchOut()) === false) {
                $name = 'PUNCH_OUT';
                $eventName = 'Punch Out';
                $message = $friendlyName . ' punched out Paycom.';
                $events[] = $this->createDriverRouteEvent($user, $driverRoute, $name, $eventName, $message);
            }
            return $events;
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * @param string $name
     * @param string $eventName
     * @param string $message
     * @param Location|null $location
     * @param DriverRoute|null $driverRoute
     * @param Vehicle|null $vehicle
     * @param Device|null $device
     * @param string $status
     * @param User|null $user
     * @param array $data
     * @param bool $isActive
     * @return Event
     */
    public function createEvent(string $name, string $eventName, string $message, ?Location $location, ?DriverRoute $driverRoute, ?Vehicle $vehicle, ?Device $device, ?User $user = null, $status = 'ok', array $data = [], $isActive = true)
    {
        $event = new Event();
        $event->setName($name);
        $event->setEventName($eventName);
        $event->setDriverRoute($driverRoute);
        if ($user instanceof User) {
            $event->setUser($user);
        } else {
            $userLogin = $this->tokenStorage->getToken() ? $this->tokenStorage->getToken()->getUser() : null;
            if ($userLogin instanceof User) {
                $event->setUser($userLogin);
            }
        }
        $event->setMessage($message);
        $event->setVehicle($vehicle);
        $event->setDevice($device);
        $event->setData($data);
        $event->setStatus($status);
        $event->setLocation($location);
        $event->setIsActive($isActive);
        // The listener of entities seem to set always the same datetime so manual input is necessary.
        $event->setCreatedAt(new DateTime());
        $this->em->persist($event);
        $this->em->flush();
        return $event;
    }

    /**
     * @param string $name
     * @param DriverRoute $driverRoute
     * @param string $messageType
     * @param array $messageData
     * @param bool $isActive
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addEventData(string $name, DriverRoute $driverRoute, string $messageType, array $messageData, $isActive = false)
    {
        /** @var User $userLogin */
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $now = new DateTime('now');
        $eventObj = new Event();
        $eventObj->setName($messageType);
        $eventObj->setEventName($name);
        $eventObj->setCreatedAt($now);
        $eventObj->setDriverRoute($driverRoute);
        $eventObj->setUser($userLogin);
        $eventObj->setMessage($messageType);
        $eventObj->setData($messageData);
        $eventObj->setIsActive($isActive);
        $this->getEntityManager()->persist($eventObj);
        $this->getEntityManager()->flush();
    }

    /**
     * @param string $time
     * @param string $timezone
     * @return string
     * @throws Exception
     */
    public function stringTimeConversion(string $time, string $timezone)
    {
        $tempDate = new \DateTime($time);
        $conversation = $this->globalUtils->getTimeZoneConversation($tempDate, $timezone);
        return $conversation->format('h:ia');
    }

    /**
     * @param string $messageType
     * @param array $messageData
     * @param string $timezone
     * @return string
     * @throws Exception
     */
    public function getMessageByType(string $messageType, array $messageData, string $timezone)
    {
        $message = '';
        switch ($messageType) {
            case 'SHIFT_START_TIME_EDIT':
                $message = 'Start Time (' . $this->stringTimeConversion($messageData['old_start_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_start_time'], $timezone) . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SHIFT_END_TIME_EDIT':
                $message = 'End Time (' . $this->stringTimeConversion($messageData['old_end_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_end_time'], $timezone) . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'EDIT_DRIVER_SHIFT_NOTE':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Drivers Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'EDIT_SHIFT_NOTE':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'EDIT_SHIFT_NAME':
                $message = 'Shift (' . $messageData['old_name'] . ') to (' . $messageData['new_name'] . ') changed by ' . $messageData['user_name'];
                break;

            case 'EDIT_SHIFT_INVOICE_TYPE':
                $message = 'Invoice Type (' . $messageData['old_invoice_type'] . ') to (' . $messageData['new_invoice_type'] . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_ADD':
                $message = 'Second Shift "' . $messageData['shift_name'] . '" created by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_START_TIME_EDIT':
                $message = 'Start Time (' . $this->stringTimeConversion($messageData['old_start_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_start_time'], $timezone) . ') changed in shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_END_TIME_EDIT':
                $message = 'End Time (' . $this->stringTimeConversion($messageData['old_end_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_end_time'], $timezone) . ') changed in shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_INVOICE_TYPE_EDIT':
                $message = 'Invoice Type (' . $messageData['old_invoice_type'] . ') to (' . $messageData['new_invoice_type'] . ') changed in shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_NOTE_EDIT':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SECOND_SHIFT_DRIVER_NOTE_EDIT':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Driver Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'INCIDENT_CREATE':
                $message = 'Incident ' . $messageData['incident_type'] . ' created by ' . $messageData['user_name'];
                break;

            case 'INCIDENT_DELETE':
                $message = 'Incident ' . $messageData['incident_type'] . ' deleted by ' . $messageData['user_name'];
                break;

            case 'INCIDENT_PHASE_SAVE':
                $message = 'Incident "' . $messageData['incident_type'] . '" of phase "' . $messageData['incident_phase'] . '" saved by ' . $messageData['user_name'];
                break;

            case 'INCIDENT_PHASE_UPDATE':
                $message = 'Incident "' . $messageData['incident_type'] . '" of phase "' . $messageData['incident_phase'] . '" updated by ' . $messageData['user_name'];
                break;

            case 'DEVICE_ADD':
                $message = 'Device (' . $messageData['new_devices'] . ') added for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'DEVICE_EDIT':
                $message = 'Device changed from (' . $messageData['old_devices'] . ') to (' . $messageData['new_devices'] . ') for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'VEHICLE_ADD':
                $message = 'Vehile (' . $messageData['new_vehicle'] . ') added for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'VEHICLE_EDIT':
                $message = 'Vehile changed from (' . $messageData['old_vehicle'] . ') to (' . $messageData['new_vehicle'] . ') for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SEND_HOME':
                $message = 'Send home in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'UNSEND_HOME':
                $message = 'Un-Send home in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'ROUTE_CODE_ADD':
                $message = 'Route Code (' . $messageData['route_code'] . ') added for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'ROUTE_ASSIGNED':
                $message = 'Route Code (' . $messageData['route_code'] . ') assigned to user "' . $messageData['assigned_user'] . '" by ' . $messageData['user_name'];
                break;

            case 'ROUTE_UNASSIGNED':
                $message = 'Route Code (' . $messageData['route_code'] . ') unassigned from user "' . $messageData['assigned_user'] . '" by ' . $messageData['user_name'];
                break;

            case 'ADD_TRAIN':
                $message = 'Add Train shift added by ' . $messageData['user_name'];
                break;

            case 'EDIT_TRAIN_START_TIME':
                $message = 'Edit Train shift change start time (' . $this->stringTimeConversion($messageData['old_start_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_start_time'], $timezone) . ') by ' . $messageData['user_name'];
                break;

            case 'EDIT_TRAIN_END_TIME':
                $message = 'Edit Train shift change end time (' . $this->stringTimeConversion($messageData['old_end_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_end_time'], $timezone) . ') by ' . $messageData['user_name'];
                break;

            case 'ADD_DUTY':
                $message = 'Add light duty shift added by ' . $messageData['user_name'];
                break;

            case 'EDIT_DUTY_START_TIME':
                $message = 'Edit Duty shift change start time (' . $this->stringTimeConversion($messageData['old_start_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_start_time'], $timezone) . ') by ' . $messageData['user_name'];
                break;

            case 'EDIT_DUTY_END_TIME':
                $message = 'Edit Duty shift change end time (' . $this->stringTimeConversion($messageData['old_end_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_end_time'], $timezone) . ') by ' . $messageData['user_name'];
                break;

            case 'ROUTE_CODE_EDIT':
                $newRouteCode = $messageData['new_route_code'] ? $messageData['new_route_code'] : 'null';
                $message = 'Route Code changed from (' . $messageData['old_route_code'] . ') to (' . $newRouteCode . ') for shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_ADD':
                $message = 'Rescue Shift created using shift "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_START_TIME_EDIT':
                $message = 'Start Time (' . $this->stringTimeConversion($messageData['old_start_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_start_time'], $timezone) . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_END_TIME_EDIT':
                $message = 'End Time (' . $this->stringTimeConversion($messageData['old_end_time'], $timezone) . ') to (' . $this->stringTimeConversion($messageData['new_end_time'], $timezone) . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_INVOICE_TYPE_EDIT':
                $message = 'Invoice Type (' . $messageData['old_invoice_type'] . ') to (' . $messageData['new_invoice_type'] . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_NOTE_EDIT':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_DRIVER_NOTE_EDIT':
                $oldNote = $messageData['old_note'] ? $messageData['old_note'] : 'null';
                $newNote = $messageData['new_note'] ? $messageData['new_note'] : 'null';
                $message = 'Driver Shift Note (' . $oldNote . ') to (' . $newNote . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'RESCUE_SHIFT_PACKAGE_EDIT':
                $message = 'Number Of Package (' . $messageData['old_package'] . ') to (' . $messageData['new_package'] . ') changed in "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SHIFT_DELETE':
                $message = 'Shift deleted for ' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SHIFT_ADD':
                $message = 'Shift created "' . $messageData['shift_name'] . '" by ' . $messageData['user_name'];
                break;

            case 'SHIFT_DRAG':
                $message = $messageData['old_user'] . ' \'s shift (' . $messageData['shift_name'] . ') dated ' . $messageData['old_date'] . ' moved to ' . $messageData['new_user'] . ' dated ' . $messageData['new_date'];
                break;

            case 'ASSIGN_OPENROUTE_TO_DRIVER':
                $message = 'OpenRoute assign to (' . $messageData['driver'] . ') driver by ' . $messageData['user_name'];
                break;

            case 'UNASSIGN_AND_POST_TO_OPEN_SHIFT':
                $message = 'This day all shift has been unassign and moved to open by ' . $messageData['user_name'];
                break;

            case 'UNASSIGN_AND_DELETE_SHIFT':
                $message = 'This day all shift has been unassign and deleted by ' . $messageData['user_name'];
                break;

            default:
                # code...
                $message = 'N/A';
                break;
        }

        return $message;
    }

    public function updateEventData($driverRouteIds)
    {
        if (!empty($driverRouteIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\Event', 'ev')
                ->set('ev.isActive', true)
                ->where($qb->expr()->in('ev.driverRoute', ':drIds'))
                ->setParameter('drIds', $driverRouteIds)
                ->getQuery()
                ->execute();

        }
    }

    public function employeeTimeline(array $criteria, $maxResults = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->andWhere('e.isActive = 1')
            ->andwhere('e.driverRoute = :driverRoute')
            ->setParameter('driverRoute', $criteria['driverRoute'])
            ->andWhere('e.name NOT LIKE :name')
            ->setParameter('name', '%' . 'LOCATION' . '%')
            ->setMaxResults($maxResults);

        return $qb->getQuery()->getResult();
    }

    public function purgeLocationEvents(array $criteria)
    {
        $qb = $this->createQueryBuilder('e')
            ->andWhere('e.createdAt ')
            ->andwhere('e.driverRoute = :driverRoute')
            ->setParameter('driverRoute', $criteria['driverRoute'])
            ->andWhere('e.name NOT LIKE :name')
            ->setParameter('name', '%' . 'LOCATION' . '%')
            ->setMaxResults(30);

        return $qb->getQuery()->getResult();
    }

    public function getRouteCodeByRouteId($routeId)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e.id,e.name,e.eventName,e.createdAt')
            ->where('e.driverRoute = :driverRoute')
            ->setParameter('driverRoute', $routeId)
            ->andWhere('e.isActive = :isActive')
            ->setParameter('isActive', true);

        return $qb->getQuery()->getScalarResult();
    }
        
    public function getTimelineEvents(array $data)
    {
        $qb = $this
            ->createQueryBuilder('e')
            ->select('e.id AS id')
            ->addSelect('e.createdAt AS date')
            ->addSelect('e.name AS type')
            ->addSelect('e.eventName AS title')
            ->addSelect('e.message AS information')
            ->addSelect('e.status')
            ->andWhere('e.createdAt IS NOT NULL')
            ->andWhere('e.isActive = 1')
            ->andWhere('e.name NOT LIKE :name')
            ->setParameter('name', '%' . 'LOCATION' . '%')
            ->andWhere('e.eventName NOT LIKE :name')
            ->setParameter('name', '%' . 'Location !!!' . '%');

        if (!empty($data['vehicleId'])) {
            $vehicleId = $data['vehicleId'];
            $qb = $qb
                ->andwhere('e.vehicle = :vehicleId')
                ->setParameter('vehicleId', $vehicleId)
                ->orderBy('e.createdAt', 'DESC');
        }

        if (!empty($data['deviceId'])) {
            $deviceId = $data['deviceId'];
            $qb = $qb
                ->andwhere('e.device = :deviceId')
                ->setParameter('deviceId', $deviceId)
                ->orderBy('e.createdAt', 'DESC');
        }

        if (!empty($data['userId'])) {
            $userId = $data['userId'];
            $qb = $qb
                ->andwhere('e.user = :userId')
                ->setParameter('userId', $userId)
                ->orderBy('e.createdAt', 'DESC');
        }

        $eventList = $qb->getQuery()->getArrayResult(); // getScalarResult(); // TODO: Replace with Scalar if UTC is no longer used.

        $result = [];
        foreach ($eventList as $item) {
            /** @var DateTime $date */
            $date = $item['date'];
            $utcDate = $date->format('U'); // substr($item['date'], 0, 10);
            $date = $date->format('Y-m-d');
            $item['iconType'] = $this->globalUtils->getIconType($item['type']);
            $item['utcDate'] = $utcDate;
            $item['formatDate'] = $date;
            $result[$date]['id'] = $this->globalUtils->getUniqueIdentifier();
            $result[$date]['date'] = $date;
            $result[$date]['line'][] = $item;
        }

        $result = array_values($result);

        return $result;
    }

    /**
     * @param string|int $routeId
     */
    public function findIsSentHome($routeId)
    {
        $query="SELECT
            SUM(IF(message = 'SEND_HOME', 1, 0)) AS SEND_HOME,SUM(IF(message = 'UNSEND_HOME', 1, 0)) AS UNSEND_HOME 
            FROM event where `driver_route_id` = $routeId and is_active  = 1";   

        $eventData = $this->getEntityManager()
                        ->getConnection()
                        ->query($query)
                        ->fetchAll();
        $isSentHome = !empty($eventData[0]['SEND_HOME']) ? $eventData[0]['SEND_HOME'] : 0;
        $isUnSentHome = !empty($eventData[0]['SEND_HOME']) ? $eventData[0]['UNSEND_HOME'] : 0;
        if( $isSentHome > $isUnSentHome )
            return true;
        else
            return false;    
    }    
}
