<?php

namespace App\Repository;

use App\Entity\UserNotifications;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UserNotifications|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserNotifications|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserNotifications[]    findAll()
 * @method UserNotifications[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserNotificationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserNotifications::class);
    }
}
