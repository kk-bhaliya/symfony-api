<?php

namespace App\Repository;

use App\Entity\ScheduleDesign;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ScheduleDesign|null find($id, $lockMode = null, $lockVersion = null)
 * @method ScheduleDesign|null findOneBy(array $criteria, array $orderBy = null)
 * @method ScheduleDesign[]    findAll()
 * @method ScheduleDesign[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScheduleDesignRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ScheduleDesign::class);
    }


    /**
     * @return mixed
     * @throws \Exception
    */
    public function getScheduleDesign($schedule, $weekArray)
    {
        $results = $this->createQueryBuilder('sd');
                  $results->where('sd.schedule = :schedule')
                  ->andWhere( $results->expr()->in('sd.week', ':weeks') )
                  ->setParameter('schedule', $schedule)
                  ->setParameter('weeks', $weekArray)
                  ->orderBy('sd.id', 'ASC')
                  ->andWhere('sd.isArchive = :isArchive')
                  ->setParameter('isArchive',false);
        return $results->getQuery()->getResult();
    }

    public function updateScheduleIsArchiveInScheduleDesign($scheduleId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\ScheduleDesign','sd')
                        ->set('sd.isArchive', true);
                        if (is_array($scheduleId)) {
                            $qb->where($qb->expr()->in('sd.schedule', ':scheduleId'))
                               ->setParameter('scheduleId', $scheduleId);
                        } else {
                            $qb->where('sd.schedule = :scheduleId')
                               ->setParameter('scheduleId', $scheduleId);
                        }
                        $qb->getQuery()
                        ->execute();
        return true;
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function getScheduleDesignShiftByWeek($scheduleId, $week, $year)
    {
        $qb = $this->createQueryBuilder('sd')
                        ->select('sf.id')
                        ->innerJoin('sd.shifts', 'sf')
                        ->where('sd.schedule = :scheduleId')
                        ->setParameter('scheduleId', $scheduleId)
                        ->andWhere('sd.week = :week')
                        ->setParameter('week', $week)
                        ->andWhere('sd.year = :year')
                        ->setParameter('year', $year)
                        ->andWhere('sd.isArchive = :isArchive')
                        ->setParameter('isArchive',false);

        return $qb->getQuery()->getResult();
    }

    /**
     * @return mixed
     * @throws \Exception
    */
    public function getScheduleDesignShiftLatestWeek($scheduleId)
    {
        $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $weekNumber = $date->format("W");
        $year = $date->format("Y");
        
        $qb = $this->createQueryBuilder('sd')
                        ->where('sd.schedule = :scheduleId')
                        ->setParameter('scheduleId', $scheduleId)
                        ->andWhere('(sd.week >= :week AND sd.year = :year) OR (sd.week < :week AND sd.year > :year)')
                        ->setParameter('week', $weekNumber)
                        ->setParameter('year', $year)
                        ->andWhere('sd.isArchive = :isArchive')
                        ->setParameter('isArchive',false);

        return $qb->getQuery()->getResult();
    }

    public function getScheduleDesignImportData($schedule, $shift, $week, $sun, $mon, $tue, $wed, $thu, $fri, $sat, $order, $year, $archive)
    {
        $em = $this->getEntityManager();

        $scheduleDesign = new ScheduleDesign();
        $scheduleDesign->setSchedule($schedule);
        if ($shift) {
            $scheduleDesign->addShift($shift);
        }
        $scheduleDesign->setWeek($week);
        $scheduleDesign->setSunday($sun);
        $scheduleDesign->setMonday($mon);
        $scheduleDesign->setTuesday($tue);
        $scheduleDesign->setWednesday($wed);
        $scheduleDesign->setThursday($thu);
        $scheduleDesign->setFriday($fri);
        $scheduleDesign->setSaturday($sat);
        $scheduleDesign->setOrdering($order);
        $scheduleDesign->setYear($year);
        $scheduleDesign->setIsArchive($archive);
        $em->persist($scheduleDesign);
        $em->flush();

        return $scheduleDesign;
    }

    public function getScheduleDesignFromScheduleId($id)
    {
        $query = $this->createQueryBuilder('sd')
            ->leftJoin('sd.shifts','shift')
            ->leftJoin('shift.skill','skill')
            ->addSelect('shift')            
            ->addSelect('skill')            
            ->where('sd.schedule = :scheduleId')
            ->setParameter('scheduleId', $id)
            ->andWhere('shift.isArchive = :shiftIsArchive')
            ->setParameter('shiftIsArchive',false)
            ->andWhere('skill.isArchive = :skillIsArchive')
            ->setParameter('skillIsArchive',false)
            ->groupBy('sd.id');
        return $query->getQuery()->getArrayResult();
    }    
    
}
