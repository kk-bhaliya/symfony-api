<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\Company;
use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\Event;
use App\Entity\Station;
use App\Entity\User;
use App\Utils\GlobalUtility;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query\QueryException;
use Error;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method Device|null find($id, $lockMode = null, $lockVersion = null)
 * @method Device|null findOneBy(array $criteria, array $orderBy = null)
 * @method Device[]    findAll()
 * @method Device[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DeviceRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $globalUtils;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        GlobalUtility $globalUtils
    )
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, Device::class);
        $this->globalUtils = $globalUtils;

    }


    /**
     * @param Device $device
     * @param array $data
     * @param User $user
     * @return int|null
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    private function addEventDevice(Device $device, array $data, User $user)
    {
        if ($device instanceof Device && $user instanceof User) {
            $deviceName = $device->getName();
            if (!is_string($deviceName)) {
                $deviceName = 'N/A';
            }
            $userName = 'N/A';
            $message = $deviceName . ' was used by ' . $userName;
            if ($user instanceof User) {
                $userName = $user->getFriendlyName();
                $message = $deviceName . ' was used by ' . $userName;
            }
            $event = $this->_em->getRepository(Event::class)->createEvent('DEVICE_ATTACHED', $message, 'Device: ' . $message, null, null, null, $device);

            $this->_em->flush();
            return $event->getId();
        }
        return null;
    }

    /**
     * @param $user
     * @param $device
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function attachSetUsedBy($user, $device)
    {
        if ($user instanceof User && $device instanceof Device) {
            $device->setUsedBy($user->getFriendlyName());
            $device->setUsedByUser($user);
            $this->_em->flush();
        }
    }

    /**
     * @param Device $device
     * @param User $user
     * @param array $criteria
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function addDeviceResponse(Device $device, User $user, array $criteria)
    {
        $eventId = $this->addEventDevice($device, $criteria, $user);
        return [
            'success' => true,
            'id' => $device->getId(),
//            'imei' => $device->getImei(),
//            'number' => $device->getNumber(),
            'eventId' => $eventId,
//            'deviceUid' => $device->getDeviceUid(),
//            'usedBy' => $device->getUsedBy(),
//            'usedAt' => $device->getUsedAt(),
        ];
    }

    /**
     * @param $device
     * @param $station
     * @param $company
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function updateDeviceDetails($device, $station, $company)
    {
        $device->setCompany($company);
        if ($station instanceof Station) {
            $device->setStation($station);
            $device->setName($station->getCode() . ' - ' . $device->getId() . ' - ' . $device->getModel());
        }
        $this->_em->flush();
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function addDevice(array $criteria)
    {
        try {
            /** @var User $user */
            $user = $this->tokenStorage->getToken()->getUser();
            $company = $user->getCompany();
            if (!($company instanceof Company)) {
                throw new Exception('Company not found.');
            }
            /** @var Driver $driver */
            $driver = $user->getDriver();
            if (!($driver instanceof Driver)) {
                throw new Exception('Driver not found.');
            }
            $station = $driver->getMainStation();

            if (!empty($criteria['number'])) {
                $device = $this->findOneBy(['number' => $criteria['number'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    if (!empty($criteria['imei'])) {
                        $device->setImei($criteria['imei']);
                    }
                    $this->updateDeviceDetails($device, $station, $company);
                    $this->attachSetUsedBy($user, $device);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }

            if (!empty($criteria['imei'])) {
                $device = $this->findOneBy(['imei' => $criteria['imei'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    if (!empty($criteria['number'])) {
                        $device->setNumber($criteria['number']);
                    }
                    $this->updateDeviceDetails($device, $station, $company);
                    $this->attachSetUsedBy($user, $device);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }

            $device = new Device();
            $device->setNumber(empty($criteria['number']) ? bin2hex(random_bytes(4)) : $criteria['number']);
            $device->setDevice(empty($criteria['uid']) ? bin2hex(random_bytes(4)) : $criteria['uid']);
            $device->setDeviceUid(empty($criteria['uid']) ? bin2hex(random_bytes(4)) : $criteria['uid']);
            $device->setImei(empty($criteria['imei']) ? bin2hex(random_bytes(4)) : $criteria['imei']);
            $device->setModel(empty($criteria['model']) ? 'Other' : $criteria['model']);
            $device->setName($device->getModel());
            $device->setCarrier(empty($criteria['carrier']) ? 'Other' : $criteria['carrier']);
            $device->setPlatform(empty($criteria['platform']) ? 'Other' : $criteria['platform']);
            $device->setManufacturer(empty($criteria['manufacturer']) ? 'Other' : $criteria['manufacturer']);
            $device->setDeviceCondition(empty($criteria['condition']) ? 'Good' : $criteria['condition']);
            $device->setUsername($user->getFriendlyName());
            $device->setAllowKickoff(false);
            $device->setAllowLoadOutProcess(false);
            $device->setUsedAt(new \DateTime('now'));
            $device->setCompany($company);
            $this->_em->persist($device);
            $this->_em->flush();
            $this->attachSetUsedBy($user, $device);
            if ($station instanceof Station) {
                $device->setStation($station);
                $device->setName($station->getCode() . ' - ' . $device->getId() . ' - ' . $device->getModel());
            } else {
                $device->setName($device->getId() . ' - ' . $device->getModel());
            }
            $this->_em->flush();
            $eventId = $this->addEventDevice($device, $criteria, $user);
            return [
                'success' => true,
                'id' => $device->getId(),
                'imei' => $device->getImei(),
                'number' => $device->getNumber(),
                'deviceUid' => $device->getDeviceUid(),
                'message' => 'New Device Created',
                'eventId' => $eventId,
            ];
        } catch (Exception $e) {
            $this->addDeviceEventError($user, $e->getMessage());
            return [
                'exception' => $e->getTrace(),
                'message' => $e->getMessage()
            ];
        } catch (Error $e) {
            $this->addDeviceEventError($user, $e->getMessage());
            return [
                'error' => $e->getTrace(),
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @param User $user
     * @return Station|null
     * @throws Exception
     */
    private function getUserMainStation(User $user)
    {
        $station = null;
        /** @var Driver $driver */
        $driver = $user->getDriver();
        if (!($driver instanceof Driver)) {
            return null;
        }
        $station = $driver->getMainStation();
        return $station;
    }

    /**
     * @param Device $device
     * @param $station
     * @param $company
     * @param $user
     * @param $criteria
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    private function updateDeviceDetailsMobile(Device $device, $station, $company, $user, $criteria)
    {
        if (!empty($criteria['imei'])) {
            $imei = $device->getImei();
            if (is_string($imei)) {
                if ($criteria['imei'] !== $imei && strlen($imei) < 14) {
                    $device->setImei($criteria['imei']);
                }
            } else {
                $device->setImei($criteria['imei']);
            }
        }
        if (!empty($criteria['number'])) {
            $device->setNumber($criteria['number']);
        }
        if ($company instanceof Company) {
            $deviceCompany = $device->getCompany();
            if ($deviceCompany instanceof Company) {
                if ($deviceCompany->getId() !== $company->getId()) {
                    $device->setCompany($company);
                }
            } else {
                $device->setCompany($company);
            }
        }
        $dateTime = new DateTime('now');
        if (!empty($data['createdAt'])) {
            $dateTime = new DateTime($data['createdAt']);
        }
        $device->setUsedAt($dateTime);
        if ($station instanceof Station) {
            $device->setStation($station);
        }

        if ($user instanceof User && $device instanceof Device) {
            $device->setUsedBy($user->getFriendlyName());
            $device->setUsedByUser($user);
        }
        $this->_em->flush();
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function attachDeviceToUser(array $criteria)
    {
        try {
            if (empty($criteria['userId'])) {
                /** @var User $user */
                $user = $this->tokenStorage->getToken()->getUser();
            } else {
                /** @var User $user */
                $user = $this->_em->getRepository(User::class)->find($criteria['userId']);
            }
            if (empty($criteria['stationId'])) {
                $station = $this->getUserMainStation($user);
            } else {
                $station = $this->_em->getRepository(Station::class)->find($criteria['stationId']);
            }
            $company = $user->getCompany();
            if (!($company instanceof Company)) {
                throw new Exception('Company not found.');
            }
            if (!empty($criteria['id'])) {
                $device = $this->findOneBy(['id' => $criteria['id'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    $this->updateDeviceDetailsMobile($device, $station, $company, $user, $criteria);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }
            if (!empty($criteria['number'])) {
                $device = $this->findOneBy(['number' => $criteria['number'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    $this->updateDeviceDetailsMobile($device, $station, $company, $user, $criteria);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }
            if (!empty($criteria['imei'])) {
                $device = $this->findOneBy(['imei' => $criteria['imei'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    $this->updateDeviceDetailsMobile($device, $station, $company, $user, $criteria);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }
            if (!empty($criteria['deviceUid'])) {
                $device = $this->findOneBy(['deviceUid' => $criteria['deviceUid'], 'isArchive' => false]);
                if ($device instanceof Device) {
                    $this->updateDeviceDetailsMobile($device, $station, $company, $user, $criteria);
                    return $this->addDeviceResponse($device, $user, $criteria);
                }
            }
            $device = new Device();
            $device->setNumber(empty($criteria['number']) ? bin2hex(random_bytes(4)) : $criteria['number']);
            $device->setDevice(empty($criteria['deviceUid']) ? bin2hex(random_bytes(4)) : $criteria['deviceUid']);
            $device->setDeviceUid(empty($criteria['deviceUid']) ? bin2hex(random_bytes(4)) : $criteria['deviceUid']);
            $device->setImei(empty($criteria['imei']) ? bin2hex(random_bytes(4)) : $criteria['imei']);
            $device->setModel(empty($criteria['model']) ? 'Other' : $criteria['model']);
            $device->setCarrier(empty($criteria['carrier']) ? 'Other' : $criteria['carrier']);
            $device->setPlatform(empty($criteria['platform']) ? 'Other' : $criteria['platform']);
            $device->setManufacturer(empty($criteria['manufacturer']) ? 'Other' : $criteria['manufacturer']);
            $device->setDeviceCondition('Good');
            $device->setAllowKickoff(false);
            $device->setAllowLoadOutProcess(false);
            $this->_em->persist($device);
            $this->_em->flush();
            $this->updateDeviceDetailsMobile($device, $station, $company, $user, $criteria);
            $this->attachSetUsedBy($user, $device);
            $this->_em->flush();
            $eventId = $this->addEventDevice($device, $criteria, $user);
            return [
                'success' => true,
                'id' => $device->getId(),
                'imei' => $device->getImei(),
                'number' => $device->getNumber(),
                'deviceUid' => $device->getDeviceUid(),
                'message' => 'New Device Created',
                'eventId' => $eventId,
            ];
        } catch (Exception $e) {
            $this->addDeviceEventError($user, $e->getMessage());
            return [
                'exception' => $e->getTrace(),
                'message' => $e->getMessage()
            ];
        } catch (Error $e) {
            $this->addDeviceEventError($user, $e->getMessage());
            return [
                'error' => $e->getTrace(),
                'message' => $e->getMessage()
            ];
        }
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function attachDeviceWithUser(array $criteria)
    {
        $device = $this->globalUtils->getDevice($criteria, true, "id");
        $user = $this->globalUtils->getTokenUser();
        if (empty($criteria['stationId'])) {
            $station = null;
        } else {
            $station = $this->_em->find(Station::class, $criteria['stationId']);
        }
        $company = $user->getCompany();
        if (isset($criteria['isDeviceNew']) && is_bool($criteria['isDeviceNew'])) {
            $device->setIsPending($criteria['isDeviceNew']);
        }
        if (!empty($criteria['osVersion'])) {
            $device->setOsVersion($criteria['osVersion']);
        }
        if (!empty($criteria['usedAt'])) {
            $device->setUsedAt(new DateTime($criteria['usedAt']));
        }
        $device->setUsedBy($user->getFriendlyName());
        $device->setUsedByUser($user);

        $device->setCompany($company);
        $device->setStation($station);

        $this->_em->flush();

        $message = $device->getName() . ' was used by ' . $user->getFriendlyName();
        /** @var Event $event */
        $event = $this->_em->getRepository(Event::class)->createEvent('DEVICE_ATTACHED', $message, 'Device: ' . $message, null, null, null, $device, $user);

        $this->_em->flush();

        if(!empty($criteria['debug'])){
            return [
                'success' => true,
                'eventId' => $event,
                'device' => $device
            ];
        }

        return [
            'success' => true,
            'id' => $device->getId(),
            'eventId' => $event->getId(),
        ];
    }


    /**
     * @param User $user
     * @param string $message
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addDeviceEventError(User $user, string $message)
    {
        $event = new Event();
        $event->setName('DEVICE_ERROR');
        $event->setEventName('DEVICE_ERROR');
        $event->setUser($user);
        $event->setMessage($message);
        $this->_em->persist($event);
        $this->_em->flush();
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function allowLoadOutProcess(array $criteria)
    {
        $companyId = null;
        if (!empty($criteria['companyId'])) {
            $companyId = $criteria['companyId'];
        } else {
            /** @var Company $company */
            $company = $this->tokenStorage->getToken()->getUser()->getCompany();
            $companyId = $company->getId();
        }

        $device = $this->findOneBy(['id' => $criteria['deviceId'], 'isArchive' => false, 'company' => $companyId]);

        if (!($device instanceof Device)) {
            throw new Exception('Device not found.');
        }

        return [
            'id' => $device->getId(),
            'success' => true,
            'isAllowed' => $device->getAllowKickoff(),
        ];
    }

    /**
     * @param $stationId
     * @param bool $isLoadout
     * @return int|mixed|string
     * @throws QueryException
     */
    public function getDeviceByStation($stationId, $isLoadout = false)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $qb = $this->createQueryBuilder('d')
            ->where('d.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->addCriteria(Common::notArchived('d'))
            ->orderBy('d.name', 'ASC');

        if (!empty($stationId)) {
            $qb->andWhere('d.station= :station')
                ->setParameter('station', $stationId);
        }

        if ($isLoadout) {
            $qb->andWhere('d.allowLoadOutProcess= :allowLoadOutProcess')
                ->setParameter('allowLoadOutProcess', true);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param $criteria
     * @return array
     * @throws QueryException
     */
    public function getLoadOutDeviceByStation(array $criteria)
    {

        $userLogin = $this->tokenStorage->getToken()->getUser();
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $currentDateObj = isset($criteria['date']) ? (new \DateTime($criteria['date'])) : (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj = $this->globalUtils->getTimeZoneConversation($currentDateObj, $requestTimeZone);
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $datetime = $currentDateObj->format('Y-m-d');

        //Open shift driver route data
        $tempOpenShiftRoutesIdArr = [];

        $openShiftTempDriverRoutes = $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->getTodayLoadOutTempDriverRouteDriverNullData($userLogin, $currentDate, $criteria['stationId']);
        foreach ($openShiftTempDriverRoutes as $tempOpenDriverRoute) {
            $tempOpenShiftRoutesIdArr[] = $tempOpenDriverRoute->getRouteId()->getId();
        }
        //Loadout remove starttime
        
        $loadOutData = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->getTodayDriverRouteRescuereListingDataNew($userLogin->getCompany()->getId(), $currentDate, $criteria['stationId'], $startTime = null, $endTime = null, $tempOpenShiftRoutesIdArr);
        
        $deviceIds = [];
        if ($loadOutData) {
            foreach ($loadOutData as $data) {

                $devices= $this->getEntityManager()->getRepository(Device::class)->getDevicesByRouteId($data['id']);
                if ($devices) {
                    if ($criteria['id'] != $data['id']) {
                        foreach ($devices as $device) {
                            $deviceIds[] = $device['id'];
                        }
                    }
                }          
            }
        }
        

        $devices = $this->getDeviceByStation($criteria['stationId'], true);
        $deviceData = [];
        foreach ($devices as $device) {
            if (!in_array($device->getId(), $deviceIds)) {
                $deviceData[] = [
                    'id' => $device->getId(),
                    'name' => $device->getName(),
                    'number' => $device->getNumber()
                ];
            }
        }
        return $deviceData;
    }

    public function archivePersonalDevices()
    {
        $oldDate = date('Y-m-d', strtotime("-3 week"));
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Device', 'd')
            ->set('d.isArchive', 1)
            ->where('d.isPersonalDevice = :isPersonalDevice')
            ->setParameter('isPersonalDevice', true)
            ->andWhere('d.usedAt < :usedAt')
            ->setParameter('usedAt', $oldDate)
            ->getQuery()
            ->execute();
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function searchOneDevice(array $criteria)
    {
        $qb = $this->createQueryBuilder('d')
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false);

        if (!empty($criteria['number'])) {
            $qb->andWhere('d.number = :number')
                ->setParameter('number', $criteria['number']);
        }

        if (!empty($criteria['deviceUid'])) {
            $qb->andWhere('d.deviceUid = :deviceUid')
                ->setParameter('deviceUid', $criteria['deviceUid']);
        }

        if (!empty($criteria['imei'])) {
            $qb->andWhere('d.imei = :imei')
                ->setParameter('imei', $criteria['imei']);
        }

        $devices = $qb->getQuery()->getResult();

        if (count($devices) > 0 && $devices[0] instanceof Device) {
            $device = $devices[0];
            return ['device' =>
                [
                    'id' => $device->getId(),
                    'number' => $device->getNumber(),
                    'imei' => $device->getImei(),
                    'deviceUid' => $device->getDeviceUid(),
                    'isPersonalDevice' => $device->getIsPersonalDevice(),
                    'isOwned' => $device->getIsOwned(),
                    'isPending' => $device->getIsPending()
                ]
            ];
        }

        throw new Exception('Device not found.');
    }

    public function updateStationIsArchiveInDevice($stationId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Device','d')
                    ->set('d.isArchive', true)
                    ->where('d.station = :stationId')
                    ->setParameter( 'stationId', $stationId )
                    ->getQuery()
                    ->execute();
        return true;
    }

    public function getDevicesByRouteId($routeId)
    {
       return $this->createQueryBuilder('d')
              ->select('d.id as id,d.name as name,d.number as number')
              ->leftJoin('d.driverRoutes', 'dr')
              ->where('dr.id = :driverRoute')
              ->setParameter('driverRoute', $routeId)
              ->andWhere('d.isArchive = :isArchive')
              ->setParameter('isArchive', false)
              ->getQuery()
              ->getScalarResult();
    }
}
