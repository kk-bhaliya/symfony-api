<?php

namespace App\Repository;

use App\Entity\ImportUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ImportUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImportUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImportUser[]    findAll()
 * @method ImportUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImportUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImportUser::class);
    }
}
