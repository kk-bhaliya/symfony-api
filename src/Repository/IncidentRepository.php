<?php

namespace App\Repository;

use App\Entity\DriverRoute;
use App\Entity\Incident;
use App\Entity\Company;
use App\Entity\Station;
use App\Entity\Driver;
use App\Entity\Shift;
use App\Entity\User;
use App\Entity\IncidentType;
use App\Entity\IncidentSubmitedPhase;
use App\Entity\Image;
use App\Entity\IncidentFile;
use App\Entity\IncidentFormFieldValue;
use App\Entity\IncidentPhase;
use App\Utils\GlobalUtility;
use App\Entity\DriverSkill;
use App\Entity\Skill;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * @method Incident|null find($id, $lockMode = null, $lockVersion = null)
 * @method Incident|null findOneBy(array $criteria, array $orderBy = null)
 * @method Incident[]    findAll()
 * @method Incident[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentRepository extends ServiceEntityRepository
{
    private $globalUtils;
    protected $tokenStorage;

    private $kernel;
    private $stopwatch;

    public function __construct(ManagerRegistry $registry, GlobalUtility $globalUtils, KernelInterface $kernel, TokenStorageInterface $tokenStorage, Stopwatch $stopwatch)
    {
        parent::__construct($registry, Incident::class);
        $this->tokenStorage = $tokenStorage;
        $this->globalUtils = $globalUtils;
        $this->kernel = $kernel;
        $this->stopwatch = $stopwatch;
    }

    public function addReportIncident($criteria)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $em = $this->getEntityManager();
        $company = $em->getRepository(Company::class)->find($criteria['company']);
        $station = $em->getRepository(Station::class)->find($criteria['station']);
        $driver = $em->getRepository(Driver::class)->find($criteria['driver']);
        $incidentType = $em->getRepository(IncidentType::class)->find($criteria['incidentType']);
        $driverRoute = $em->getRepository(DriverRoute::class)->find($criteria['driverRoute']);
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $incident = new Incident();
        $incident->setCompany($company);
        $incident->setStation($station);
        $incident->setDriver($driver);
        $incident->setDateTime(new \DateTime($criteria['dateTime']));
        $incident->setIncidentType($incidentType);
        $incident->setCreatedBy($user);
        $incident->setIncidentCurrentPhase($incidentType->getIncidentPhases()->first());
        $incident->setStatus(0);

        if ($driverRoute) {
            $incident->setDriverRoute($driverRoute);
            if (!empty($driverRoute->getDriverRouteCodes()->first())) {
                $driverRoute->setDriver(null);
                $driverRoute->setIsOpenShift(true);
                $em->persist($driverRoute);
                $em->flush();
            }
            $messageData = array('incident_type' => $incidentType->getName(),'user_name'=>$user->getFriendlyName());
            $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Incident Created', $driverRoute, 'INCIDENT_CREATE', $messageData, true);
        }

        $em->persist($incident);
        $em->flush();


        return $this->globalUtils->getIncidentloadData($incident, $requestTimeZone, false);
    }

    public function updateReportIncident($criteria)
    {
        $em = $this->getEntityManager();
        $incident = $this->find($criteria['id']);
        $incident->setDateTime(new \DateTime($criteria['dateTime']));
        $em->persist($incident);
        $em->flush();

        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $incidentData['date'] = $incident->getDateTime()->format('Y-m-d');
        $incidentData['time'] = $this->globalUtils->getTimeZoneConversation($incident->getDateTime(), $requestTimeZone)->format('H:i:s');
        $incidentData['datetime'] = $incidentData['date'].' '.$incidentData['time'];


        return $incidentData['datetime'];
    }

    public function removeIncidentById($criteria)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $em = $this->getEntityManager();
        $incident = $this->find($criteria['id']);
        $incident->setIsArchive(true);
        $em->persist($incident);
        $em->flush();

        if (!empty($incident->getDriverRoute()) && !empty($incident->getDriverRoute()->getDriverRouteCodes())) {
            $driverRoute = $incident->getDriverRoute();
            $driverRoute->setDriver($incident->getDriver());
            $driverRoute->setIsArchive(false);
            $driverRoute->setIsOpenShift(false);
            $em->persist($driverRoute);
            $em->flush();
        }

        $messageData = array('incident_type' => $incident->getIncidentType()->getName(),'user_name'=>$user->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Incident Deleted', $driverRoute, 'INCIDENT_DELETE', $messageData, true);

        return true;
    }

    public function getIncidentByCompany($criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $stationId = $criteria['station'];
        $em = $this->getEntityManager();
        $this->stopwatch->start('query');
        
        $incidentQuery = "SELECT 
                        i.id,i.is_archive,i.date_time,i.status,i.created_date,i.title,i.incident_completed_phase_id,i.driver_route_id,
                        s.id as station_id,s.code as station_code,s.name as station_name,
                        dri.id as driver_id,dri.is_enabled as driver_enable,dri.is_archive as driver_isArchive,
                        u.id as user_id,u.friendly_name as user_friendlyName,dri.employee_status as user_isEnabled,
                        it.id as type_id,it.name as type_name,it.prefix as type_prefix,
                        (SELECT count(isp.id) from incident_submited_phase isp where isp.incident_id = i.id) as incident_submited_phase_count,
                        (SELECT isp.submitted_at from incident_submited_phase isp where isp.incident_phase_id = i.incident_completed_phase_id order by isp.id desc limit 1) as incident_submited_phase_submited_at,
                        (SELECT ip.title from incident_phase ip where ip.id  = i.incident_completed_phase_id) as incident_completed_phase_title,
                        (SELECT count(ip.id) from incident_phase ip where ip.incident_type_id  = it.id) as incident_total_phase_count,
                        (SELECT ip.allocated_time from incident_phase ip where ip.id  = i.incident_current_phase_id) as incident_allocated_time,
                        (SELECT r.name from role r LEFT JOIN user_role ur ON ur.role_id = r.id where ur.user_id  = u.id limit 1) as driver_user_role_name
            FROM 
                incident i 
            LEFT JOIN 
                driver_route dr 
            ON 
                i.driver_route_id = dr.id 
            LEFT JOIN 
                station s 
            ON  
                i.station_id = s.id 
            LEFT JOIN 
                shift sf 
            ON 
                dr.shift_type_id = sf.id 
            LEFT JOIN 
                driver dri 
            ON 
                i.driver_id = dri.id 
            LEFT JOIN 
                user u 
            ON 
                dri.user_id = u.id 
            LEFT JOIN 
                incident_type it 
            ON 
                i.incident_type_id = it.id 
            WHERE 
                i.company_id = ".$criteria['company']." AND i.is_archive = false AND sf.is_archive = false AND it.is_archive = false AND s.id = $stationId  
            ORDER BY 
                i.date_time DESC";
    
        $incidents = $this->getEntityManager()
          ->getConnection()
          ->query($incidentQuery)
          ->fetchAll();
        $this->stopwatch->stop('query');
        $incidentResult = [];
        foreach ($incidents as $incident) {
            $incidentResult[] = $this->globalUtils->getAllIncidentloadData($incident, $requestTimeZone, true);
        }
        return $incidentResult;
    }

    public function getIncidentByCompanyOld($criteria)
    {
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;
        $stationIds = isset($criteria['station']) ? $criteria['station'] : [];
        $em = $this->getEntityManager();
       
        $qb = $this->createQueryBuilder('i')
                          ->leftJoin('i.driverRoute', 'dr')
                          ->leftJoin('i.station', 'st')
                          ->leftJoin('dr.shiftType', 'sf')
                          ->where('i.company = :company')
                          ->setParameter('company', $criteria['company'])
                          ->andWhere('i.isArchive = :isArchive')
                          ->setParameter('isArchive', false)
                          ->andWhere('sf.isArchive = :isArchiveShift')
                          ->setParameter('isArchiveShift', false)
                          ->orderBy('i.dateTime', 'DESC');

        if(count($stationIds) > 0){
            $qb->andWhere($qb->expr()->In('st.id', $stationIds));
        }

        $incidents = $qb->setMaxResults(150)
                          ->getQuery()
                          ->getResult();

        $incidentResult = [];

        foreach ($incidents as $incident) {
            $incidentResult[] = $this->globalUtils->getIncidentloadData($incident, $requestTimeZone, true);
        }

        return $incidentResult;
    }

    public function updateIncidentIsArchiveByIncidentType($incidentType)
    {
        $incidentIds = $incidentFormFieldValueIds = [];
        foreach ($incidentType->getIncidents() as $incident) {
            $incidentIds[] = $incident->getId();
            foreach ($incident->getIncidentFormFieldValues() as $incidentFormFieldValue) {
                $incidentFormFieldValueIds[] = $incidentFormFieldValue->getId();
            }
        }
        //Update Incident
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\Incident', 'i')
            ->set('i.isArchive', true)
            ->where('i.incidentType = :incidentType')
            ->setParameter('incidentType', $incidentType)
            ->getQuery()
            ->execute();

        //Update Incident Submited Phase
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentSubmitedPhase', 'isp')
            ->set('isp.isArchive', true)
            ->where($qb->expr()->in('isp.incident', ':incident'))
            ->setParameter('incident', $incidentIds)
            ->getQuery()
            ->execute();

        //Update Incident Form Field Value
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentFormFieldValue', 'ifv')
            ->set('ifv.isArchive', true)
            ->where($qb->expr()->in('ifv.incident', ':incident'))
            ->setParameter('incident', $incidentIds)
            ->getQuery()
            ->execute();

        //Update Incident File
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\IncidentFile', 'if')
            ->set('if.isArchive', true)
            ->where($qb->expr()->in('if.incidentFormFieldValue', ':incidentFormFieldValue'))
            ->setParameter('incidentFormFieldValue', $incidentFormFieldValueIds)
            ->getQuery()
            ->execute();
        return true;
    }

    public function getIncidentByDriverDate($driverId, $dateTime)
    {
      return $this->createQueryBuilder('i')
              ->select('i.id,i.title,i.dateTime,it.name') 
              ->leftJoin('i.driverRoute', 'dr')
              ->leftJoin('i.incidentType', 'it') 
              ->where('i.driver = :driver')
              ->setParameter('driver', $driverId)
              ->andWhere('i.dateTime LIKE :dateTime')
              ->setParameter('dateTime', $dateTime->format('Y-m-d').'%')
              ->setMaxResults(1)
              ->getQuery()
              ->getScalarResult();
    }

    public function getIncidentByRouteId($routeId)
    {
      return $this->createQueryBuilder('i')
              ->select('i.id,i.title,i.dateTime,it.name') 
              ->leftJoin('i.driverRoute', 'dr')
              ->leftJoin('i.incidentType', 'it') 
              ->where('dr.id = :driverRoute')
              ->setParameter('driverRoute', $routeId)
              ->andWhere('i.isArchive = :isArchive')
              ->setParameter('isArchive', false)
              ->getQuery()
              ->getScalarResult();
    }

    public function callCreateUpdateIncidentFormCommand($criteria)
    {
        $companyIds = isset($criteria['companyIds']) ? $criteria['companyIds'] : 0;
        $oldDelete = isset($criteria['oldDelete']) ? $criteria['oldDelete'] : false;
        $arrayInput = ['command' => 'dsp:make:incidentform','--companies' => $companyIds];
        if($oldDelete == true){
            $arrayInput['-d']=true;
        }

        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput($arrayInput);
        $output = new BufferedOutput();
        $application->run($input, $output);
        return true;
    }

    public function importIncidentByIncidentType($criteria)
    {
        if ($criteria['type'] == 'call-in') {
            $this->importIncidentCallInCsv($criteria);
        } else if ($criteria['type'] == 'ncns') {
            $this->importIncidentNCNSCsv($criteria);
        }
    }

    public function importIncidentCallInCsv($criteria)
    {
        $incidentCsvFile = $criteria['amazone_incident_csv'];
        // $checkFileStatus = $this->urlFileExists($incidentCsvFile);
        // if (!$checkFileStatus) {
        //     throw new NotFoundHttpException("Amazone incident ($incidentCsvFile) file not found");
        // }
        $em = $this->getEntityManager();

        $file = fopen($incidentCsvFile, 'r');
        $dataArrKeyValue = [];
        $count = 0;
        $newLine = '
';
        $globalValidationMsg = '';
        while (($columnLine = fgetcsv($file)) !== FALSE) {
            $validationMsg = '';
            if ($count == 0) {
                $nColumCount = 0;
                foreach($columnLine as $line) {
                    if (!empty($line)) {
                        $dataArrKeyValue['header'][$line] = $nColumCount;
                    }
                    $nColumCount++;
                }
            } else if ($count == 1) {
                $dataArrKeyValue['column'] = $columnLine;
            } else if ($count >= 3) {
                $validation = true;
                $company = $this->tokenStorage->getToken()->getUser()->getCompany();
                $incidentTitle = $columnLine[0];
                if(strpos($dataArrKeyValue['column'][0], '*') !== false && empty($incidentTitle)){
                    $validationMsg = "Please enter a Title!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][1], '*') !== false && empty($columnLine[1])){
                    $validationMsg .= "Please enter a Station!, ".$newLine;
                    $validation = false;
                } else {
                    $station = $em->getRepository(Station::class)->findOneBy(['code' => trim($columnLine[1]),'isArchive' => false]);
                    if (!$station) {
                        $validationMsg .= "Please enter valid station code!, ".$newLine;
                        $validation = false;
                    }
                }
                $incidentType = $em->getRepository(IncidentType::class)->findOneBy(['prefix' => 'Cal','isArchive' => false, 'company' => $company]);
                if (!$incidentType) {
                    $validationMsg .= "Please enter valid incident type!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][3], '*') !== false && empty($columnLine[3])){
                    $validationMsg .= "Please enter a CreatedBy!, ".$newLine;
                    $validation = false;
                } else {
                    $user = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[3]),'isArchive' => false]);
                    if (!$user) {
                        $validationMsg .= "Please enter valid created by user!, ".$newLine;
                        $validation = false;
                    }
                }
                $driver = '';
                if(strpos($dataArrKeyValue['column'][4], '*') !== false && empty($columnLine[4])){
                    $validationMsg .= "Please enter a Driver!, ".$newLine;
                    $validation = false;
                } else {
                    $name = explode(" ",trim($columnLine[4]));

                    $userObj = $em->getRepository(User::class)->findOneBy(['firstName' => trim($name[0]),'lastName' => trim($name[1]),'isArchive' => false]);

                    if(empty($userObj)){
                        // create new company and user
                        $role = $em->getRepository('App\Entity\Role')->findOneBy(['company'=>$company,'name'=>'ROLE_DELIVERY_ASSOCIATE']) ;

                        $newUser = new User();
                        $newUser->setEmail(strtolower(trim($name[0]).trim($name[1])).'@imported-soliderdelivery.com');
                        $newUser->setPassword('Private1!');
                        $newUser->setFirstName(trim($name[0]));
                        $newUser->setLastName(trim($name[1]));
                        $newUser->setMiddleName('');
                        $newUser->setFriendlyName(trim($name[0]).' '.trim($name[1]));
                        $newUser->setCompany($company);
                        $newUser->setIsEnabled(false);
                        $newUser->setIsArchive(false);
                        $newUser->addUserRole($role);
                        $em->persist($newUser);
                        $em->flush();
                        $userObj = $newUser;
                        // $validationMsg .= "Employee dose not exist!, ".$newLine;
                        // $validation = false;
                    }
                    $driver = $userObj->getDriver();
                    if (empty($driver)) {
                        // create new driver
                        $driver = new Driver();

                        $driver->setBirthday(new \DateTime(date("Y-m-d")));
                        $driver->setJobTitle('Delivery Associate');
                        $driver->setSsn('1234');
                        $driver->setEmploymentStatus($em->getRepository('App\Entity\EmploymentStatus')->findOneBy(['companyId'=>$company,'name'=>'Non-Exempt']));
                        $driver->setPersonalPhone('1234567891');
                        $driver->setMaritalStatus(lcfirst('Single'));
                        $driver->setGender(lcfirst('Male'));
                        $driver->setVeteranStatus(0);
                        $driver->setSecondaryEmail('');
                        $driver->setFullAddress('Imported');
                        $driver->setNote('');
                        $driver->setHireDate(new \DateTime(date("Y-m-d")));
                        $driver->setShirtSize('M - 2XL');
                        $driver->setShoeSize('M - US 11/EU 44');
                        $driver->setPantSize('M - 2XL');
                        $driver->setTransporterID('');
                        $driver->setDriversLicenseNumber('J359123111222');
                        $driver->setDriversLicenseNumberExpiration(new \DateTime(date("Y-m-d")));
                        $driver->setLicenseState('ML');
                        $driver->setCompany($company);
                        $driver->setUser($userObj);
                        $driver->addStation($station);
                        $driver->addAssignedManager($user);
                        $driver->setIsArchive(false);
                        $driver->setIsEnabled(false);
                        $em->persist($driver);
                        $em->flush();

                        $skill = new DriverSkill();
                        $skill->setDriver($driver);
                        $companySkill = $em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => 'Driver', 'isArchive' => false]);
                        $skill->setSkill($companySkill);
                        $skillRate = $em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                        $skill->setSkillRate($skillRate);
                        $skill->setHourlyRate($skillRate->getDefaultRate());
                        $em->persist($skill);
                        $em->flush();
                    }
                }
                if(strpos($dataArrKeyValue['column'][5], '*') !== false && empty($columnLine[5])){
                    $validationMsg .= "Please enter a Shift!, ".$newLine;
                    $validation = false;
                } else {
                    $shift = $em->getRepository(Shift::class)->findOneBy(['name' => trim($columnLine[5]), 'station' => $station->getId(), 'isArchive' => false]);
                    if (!$shift) {
                        $validationMsg .= "Please enter valid shift type!, ".$newLine;
                        $validation = false;
                    }
                }

                $incidentDate = '';
                if(strpos($dataArrKeyValue['column'][6], '*') !== false && empty($columnLine[6])){
                    $validationMsg .= "Please enter a IncidentDate!, ".$newLine;
                    $validation = false;
                } else {
                    $incidentDate = new \DateTime(trim($columnLine[6]));
                    if ($incidentDate instanceof DateTime) {
                        $validationMsg .= "Please enter valid incident date pass!, ".$newLine;
                        $validation = false;
                    }
                }
                if(strpos($dataArrKeyValue['column'][7], '*') !== false && empty($columnLine[7])){
                    $validationMsg .= "Please enter a Incident Time!, ".$newLine;
                    $validation = false;
                } else {
                    $incidentTime = new \DateTime(trim($columnLine[7]));
                    if ($incidentTime instanceof DateTime) {
                        $validationMsg .= "Please enter valid incident time pass!, ".$newLine;
                        $validation = false;
                    }
                }
                if ($shift && !empty($driver) && $incidentDate && $station) {
                    $driverRoute = $em->getRepository(DriverRoute::class)->findOneBy(['shiftType' => $shift->getId(), 'driver' =>  $driver->getId(), 'station' => $station->getId(), 'dateCreated' => $incidentDate, 'isArchive' => false]);
                    if(!$driverRoute){
                        // create new driver route
                        $driverRoute = new DriverRoute();
                        $driverRoute->setSkill($shift->getSkill());
                        $driverRoute->setDriver($driver);
                        $driverRoute->setStation($shift->getStation());
                        $driverRoute->setDateCreated($incidentDate);
                        $driverRoute->setShiftType($shift);
                        $driverRoute->setIsTemp('');
                        $driverRoute->setIsOpenShift(empty($driver));
                        $driverRoute->setIsActive(true);
                        $driverRoute->setShiftName($shift->getName());
                        $driverRoute->setShiftColor($shift->getColor());
                        $driverRoute->setUnpaidBreak($shift->getUnpaidBreak());
                        $driverRoute->setShiftNote('');
                        $driverRoute->setShiftStation($shift->getStation());
                        $driverRoute->setShiftSkill($shift->getSkill());
                        $driverRoute->setShiftCategory($shift->getCategory());
                        $driverRoute->setShiftBalanceGroup($shift->getBalanceGroup());
                        $driverRoute->setShiftTextColor($shift->getTextColor());
                        $driverRoute->setStartTime($shift->getStartTime());
                        $driverRoute->setEndTime($shift->getEndTime());
                        $driverRoute->setIsRescuer(false);
                        $driverRoute->setIsRescued(false);
                        $driverRoute->setIsLoadOut(false);
                        $driverRoute->setIsBackup(false);
                        $driverRoute->setCurrentNote('');
                        $driverRoute->setNewNote('');
                        $this->getEntityManager()->persist($driverRoute);
                        $this->getEntityManager()->flush();
                    }
                }
                if(strpos($dataArrKeyValue['column'][8], '*') !== false && empty($columnLine[8])){
                    $validationMsg .= "Please enter a Phase1 Step1 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim(strtolower($columnLine[8])), array('sick', 'injury', 'pets', 'vehicle-issue', 'family-emergency', 'overslept', 'other', 'quit', 'abandoned', 'kids', 'sent-home','preventable','attitude','not-at-fault','car'))) {
                    $validationMsg .= "please fill the Phase1 Step1 Q1 with (sick, injury, pets, vehicle-issue, family-emergency, overslept, other, quit, abandoned, kids, sent-home,preventable,attitude,not-at-fault,car)!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['call-in-report-step1-field1'] = trim(strtolower($columnLine[8]));
                $incidentStepValue['call-in-report-step1-field2'] = $columnLine[9];
                $driverPhase1 = '';
                if(strpos($dataArrKeyValue['column'][10], '*') !== false && empty($columnLine[10])){
                    $validationMsg .= "Please enter a Phase1 Step2 Q1!, ".$newLine;
                    $validation = false;
                } else {
                    $userObjPhase1 = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[10]),'isArchive' => false]);
                    if ($userObjPhase1) {
                        $driverPhase1 = $userObjPhase1->getDriver();
                        if (!$driverPhase1) {
                            $validationMsg .= "Please enter valid email for assign to driver in Phase1 Step2 Q1!, ".$newLine;
                            $validation = false;
                        }
                    } else {
                      $validationMsg .= "Please enter valid email for assign to driver in Phase1 Step2 Q1!, ".$newLine;
                      $validation = false;
                    }
                    $incidentStepValue['call-in-report-step2-field1'] = $driverPhase1->getId();
                }
                if(strpos($dataArrKeyValue['column'][11], '*') !== false && empty($columnLine[11])){
                    $validationMsg .= "Please enter a Phase1 Submited By with valid Email!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedByPhase1Obj = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[11]),'isArchive' => false]);
                    if (!$submitedByPhase1Obj) {
                        $validationMsg .= "Please enter valid Phase1 Submited By user with valid Email!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedBy-1'] = $submitedByPhase1Obj;
                }
                if(strpos($dataArrKeyValue['column'][12], '*') !== false && empty($columnLine[12])){
                    $validationMsg .= "Please enter a Phase1 Submited at!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedAtPhase1 = new \DateTime(trim($columnLine[12]));
                    if ($submitedAtPhase1 instanceof DateTime) {
                        $validationMsg .= "Please enter valid Phase1 Submited at!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedAt-1'] = $submitedAtPhase1;
                }
                if(!empty($columnLine[13])){
                    $reviewByPhase1Obj = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[13]),'isArchive' => false]);
                    if (!$reviewByPhase1Obj) {
                        $validationMsg .= "Please enter valid review by!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-reviewBy-1'] = $reviewByPhase1Obj;
                }
                if(!empty($columnLine[14])){
                    $reviewAtPhase1 = new \DateTime($columnLine[14]);
                    if ($reviewAtPhase1 instanceof DateTime) {
                        $validationMsg .= "Please enter valid Phase1 review at!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-reviewAt-1'] = $reviewAtPhase1;
                }
                if(strpos($dataArrKeyValue['column'][15], '*') !== false && empty($columnLine[15])){
                    $validationMsg .= "Please enter a Phase2 Step1 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[15]), array('yes', 'no'))) {
                    $validationMsg .= "please fill the Phase2 Step1 Q1 with (yes, no)!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['call-in-review-step1-field1'] = trim($columnLine[15]);
                $incidentStepValue['call-in-review-step2-field1'] = trim($columnLine[16]);
                if(strpos($dataArrKeyValue['column'][17], '*') !== false && empty($columnLine[17])){
                    $validationMsg .= "Please enter a Phase2 Step3 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[17]), array('yes', 'no'))) {
                    $validationMsg .= "please fill the Phase3 Step3 Q1 with (yes, no)!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['call-in-review-step3-field1'] = trim($columnLine[17]);
                $incidentStepValue['call-in-review-step4-field1'] = trim($columnLine[18]);
                if(strpos($dataArrKeyValue['column'][19], '*') !== false && empty($columnLine[19])){
                    $validationMsg .= "Please enter a Phase2 Submited By!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedByPhase2Obj = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[19]),'isArchive' => false]);
                    if (!$submitedByPhase2Obj) {
                        $validationMsg .= "Please enter valid phase2 submited by with valid email!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedBy-2'] = $submitedByPhase2Obj;
                }
                if(strpos($dataArrKeyValue['column'][20], '*') !== false && empty($columnLine[20])){
                    $validationMsg .= "Please enter a Phase2 Submited at!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedAtPhase2 = new \DateTime(trim($columnLine[20]));
                    if ($submitedAtPhase2 instanceof DateTime) {
                        $validationMsg .= "Please enter valid Phase2 Submited at!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedAt-2'] = $submitedAtPhase2;
                }

                //Check validation
                if ($validation) {
                    $incidentDateTime = new \DateTime($columnLine[6].' '.$columnLine[7]);
                    $incident = $this->insertIncident($company, $incidentTitle, $station, $incidentType, $user, $driver, $incidentDateTime, $driverRoute);
                    $incidentPhases = $em->getRepository(IncidentPhase::class)->getIncidentPhaseByType($incidentType->getId());
                    $phaseCount = 1;
                        foreach ($incidentPhases as $incidentPhase) {
                            foreach ($incidentPhase->getIncidentSteps() as $step) {
                                foreach ($step->getIncidentFormFields() as $incidentFormField) {
                                    if (isset($incidentStepValue[$incidentFormField->getToken()])) {
                                        $incidentFormFieldValue = new IncidentFormFieldValue();
                                        $incidentFormFieldValue->setIncidentFormField($incidentFormField);
                                        $incidentFormFieldValue->setIncident($incident);
                                        $incidentFormFieldValue->setValue([$incidentStepValue[$incidentFormField->getToken()]]);
                                        $incidentFormFieldValue->setCreatedDate(new \DateTime());
                                        $this->getEntityManager()->persist($incidentFormFieldValue);
                                        $this->getEntityManager()->flush();
                                        if ($incidentFormField->getFieldId() == '21') {
                                            $imagesArrays = explode(',',$incidentStepValue[$incidentFormField->getToken()]);
                                            foreach($imagesArrays as $imagesArr) {
                                                $image = new Image();
                                                $image->setPath($imagesArr);
                                                $this->getEntityManager()->persist($image);
                                                $this->getEntityManager()->flush();

                                                $incidentFile = new IncidentFile();
                                                $incidentFile->setImage($image);
                                                $incidentFile->setIncidentFormFieldValue($incidentFormFieldValue);
                                                $this->getEntityManager()->persist($incidentFile);
                                                $this->getEntityManager()->flush();
                                            }
                                        }
                                    }

                                }
                            }
                            $incidentSubmitedPhase = new IncidentSubmitedPhase();
                            $incidentSubmitedPhase->setIncidentPhase($incidentPhase);
                            $incidentSubmitedPhase->setSubmitedBy($incidentStepValue['phase-submitedBy-'.$phaseCount]);
                            $incidentSubmitedPhase->setSubmittedAt($incidentStepValue['phase-submitedAt-'.$phaseCount]);
                            if (isset($incidentStepValue['phase-reviewBy-'.$phaseCount]) && isset($incidentStepValue['phase-reviewAt-'.$phaseCount])) {
                                $incidentSubmitedPhase->setReviewedBy($incidentStepValue['phase-reviewBy-'.$phaseCount]);
                                $incidentSubmitedPhase->setReviewedAt($incidentStepValue['phase-reviewAt-'.$phaseCount]);
                            }
                            $incidentSubmitedPhase->setIncident($incident);
                            $this->getEntityManager()->persist($incidentSubmitedPhase);
                            $this->getEntityManager()->flush();

                            $newPhase = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->findOneBy(['incidentType' => $incident->getIncidentType()->getId(), 'ordering' => $incidentPhase->getOrdering()+1]);
                            if($newPhase){
                                $incident->setIncidentCurrentPhase($newPhase);
                            }else{
                                $incident->setIncidentCurrentPhase(null);
                            }
                            $incident->setIncidentCompletedPhase($incidentPhase);
                            $em->persist($incident);
                            $em->flush();
                            $phaseCount++;
                        }
                } else {
                    $globalValidationMsg .= '('.($count+1).') '.$validationMsg.$newLine;
                }
            }
            $count++;
        }
        if($globalValidationMsg != ''){
            throw new NotFoundHttpException($globalValidationMsg);
        }
        fclose($file);
    }

    public function importIncidentNCNSCsv($criteria)
    {
        $incidentCsvFile = $criteria['amazone_incident_csv'];
        // $checkFileStatus = $this->urlFileExists($incidentCsvFile);
        // if (!$checkFileStatus) {
        //     throw new NotFoundHttpException("Amazone incident ($incidentCsvFile) file not found");
        // }
        $em = $this->getEntityManager();

        $file = fopen($incidentCsvFile, 'r');

        $newLine='';
        $dataArrKeyValue = [];
        $count = 0;
        $globalValidationMsg = '';
        while (($columnLine = fgetcsv($file)) !== FALSE) {
            $validationMsg = '';
            if ($count == 0) {
                $nColumCount = 0;

                foreach($columnLine as $line) {
                    if (!empty($line)) {
                        $dataArrKeyValue['header'][$line] = $nColumCount;
                    }
                    $nColumCount++;
                }
            } else if ($count == 1) {
                $dataArrKeyValue['column'] = $columnLine;
            } else if ($count >= 3) {
                $validation = true;
                $company = $this->tokenStorage->getToken()->getUser()->getCompany();
                $incidentTitle = $columnLine[0];
                if(strpos($dataArrKeyValue['column'][0], '*') !== false && empty($incidentTitle)){
                    $validationMsg = "Please enter a Title!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][1], '*') !== false && empty($columnLine[1])){
                    $validationMsg .= "Please enter a Station!, ".$newLine;
                    $validation = false;
                } else {
                    $station = $em->getRepository(Station::class)->findOneBy(['code' => trim($columnLine[1]), 'isArchive' => false]);
                    if (!$station) {
                        $validationMsg .= "Please enter valid station code!, ".$newLine;
                        $validation = false;
                    }
                }
                $incidentType = $em->getRepository(IncidentType::class)->findOneBy(['prefix' => 'ncn', 'isArchive' => false, 'company' => $company]);
                if (!$incidentType) {
                    $validationMsg .= "Please enter valid incident type!, ".$newLine;
                    $validation = false;
                }

                if(empty($columnLine[3])){
                    $validationMsg .= "Please enter a CreatedBy!, ".$newLine;
                    $validation = false;
                } else {
                    $user = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[3]), 'isArchive' => false]);
                    if (!$user) {
                        $validationMsg .= "Please enter valid created by user email!, ".$newLine;
                        $validation = false;
                    }
                }
                $driver = '';
                if(strpos($dataArrKeyValue['column'][4], '*') !== false && empty($columnLine[4])){
                    $validationMsg .= "Please enter a Driver!, ".$newLine;
                    $validation = false;
                } elseif(!empty($station)) {
                    $name = explode(" ",trim($columnLine[4]));

                    $userObj = $em->getRepository(User::class)->findOneBy(['firstName' => trim($name[0]),'lastName' => trim($name[1]),'isArchive' => false]);
                    if(empty($userObj)){
                        // create new company and user
                        $role = $em->getRepository('App\Entity\Role')->findOneBy(['company'=>$company,'name'=>'ROLE_DELIVERY_ASSOCIATE']) ;

                        $newUser = new User();
                        $newUser->setEmail(strtolower(trim($name[0]).trim($name[1])).'@imported-soliderdelivery.com');
                        $newUser->setPassword('Private1!');
                        $newUser->setFirstName(trim($name[0]));
                        $newUser->setLastName(trim($name[1]));
                        $newUser->setMiddleName('');
                        $newUser->setFriendlyName(trim($name[0]).' '.trim($name[1]));
                        $newUser->setCompany($company);
                        $newUser->setIsEnabled(false);
                        $newUser->setIsArchive(false);
                        $newUser->addUserRole($role);
                        $em->persist($newUser);
                        $em->flush();
                        $userObj = $newUser;
                        // $validationMsg .= "Employee dose not exist!, ".$newLine;
                        // $validation = false;
                    }
                    $driver = $userObj->getDriver();
                    if (empty($driver)) {
                        // create new driver
                        $driver = new Driver();

                        $driver->setBirthday(new \DateTime(date("Y-m-d")));
                        $driver->setJobTitle('Delivery Associate');
                        $driver->setSsn('1234');
                        $driver->setEmploymentStatus($em->getRepository('App\Entity\EmploymentStatus')->findOneBy(['companyId'=>$company,'name'=>'Non-Exempt']));
                        $driver->setPersonalPhone('1234567891');
                        $driver->setMaritalStatus(lcfirst('Single'));
                        $driver->setGender(lcfirst('Male'));
                        $driver->setVeteranStatus(0);
                        $driver->setSecondaryEmail('');
                        $driver->setFullAddress('Imported');
                        $driver->setNote('');
                        $driver->setHireDate(new \DateTime(date("Y-m-d")));
                        $driver->setShirtSize('M - 2XL');
                        $driver->setShoeSize('M - US 11/EU 44');
                        $driver->setPantSize('M - 2XL');
                        $driver->setTransporterID('');
                        $driver->setDriversLicenseNumber('J359123111222');
                        $driver->setDriversLicenseNumberExpiration(new \DateTime(date("Y-m-d")));
                        $driver->setLicenseState('ML');
                        $driver->setCompany($company);
                        $driver->setUser($userObj);
                        $driver->addStation($station);
                        $driver->addAssignedManager($user);
                        $driver->setIsArchive(false);
                        $driver->setIsEnabled(false);
                        $em->persist($driver);
                        $em->flush();

                        $skill = new DriverSkill();
                        $skill->setDriver($driver);
                        $companySkill = $em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => 'Driver', 'isArchive' => false]);
                        $skill->setSkill($companySkill);
                        $skillRate = $em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                        $skill->setSkillRate($skillRate);
                        $skill->setHourlyRate($skillRate->getDefaultRate());
                        $em->persist($skill);
                        $em->flush();
                    }
                }
                if(strpos($dataArrKeyValue['column'][5], '*') !== false && empty($columnLine[5])){
                    $validationMsg .= "Please enter a Shift!, ".$newLine;
                    $validation = false;
                } else {
                    $shift = $em->getRepository(Shift::class)->findOneBy(['name' => trim($columnLine[5]), 'station' => $station->getId(), 'isArchive' => false]);
                    if (!$shift) {
                        $validationMsg .= "Please enter valid shift type!, ".$newLine;
                        $validation = false;
                    }
                }
                $incidentDate = '';
                if(strpos($dataArrKeyValue['column'][6], '*') !== false && empty($columnLine[6])){
                    $validationMsg .= "Please enter a IncidentDate!, ".$newLine;
                    $validation = false;
                } else {
                    $incidentDate = new \DateTime(trim($columnLine[6]));
                    if ($incidentDate instanceof DateTime) {
                        $validationMsg .= "Please enter valid incident date pass!, ".$newLine;
                        $validation = false;
                    }
                }
                if(strpos($dataArrKeyValue['column'][7], '*') !== false && empty($columnLine[7])){
                    $validationMsg .= "Please enter a Incident Time!, ".$newLine;
                    $validation = false;
                } else {
                    $incidentTime = new \DateTime(trim($columnLine[7]));
                    if ($incidentTime instanceof DateTime) {
                        $validationMsg .= "Please enter valid incident time pass!, ".$newLine;
                        $validation = false;
                    }
                }
                if ($shift && $driver && $incidentDate && $station) {
                    $driverRoute = $em->getRepository(DriverRoute::class)->findOneBy(['shiftType' => $shift->getId(), 'driver' =>  $driver->getId(), 'station' => $station->getId(), 'dateCreated' => $incidentDate, 'isArchive' => false]);
                    if(!$driverRoute){
                        // create new driver route
                        $driverRoute = new DriverRoute();
                        $driverRoute->setSkill($shift->getSkill());
                        $driverRoute->setDriver($driver);
                        $driverRoute->setStation($shift->getStation());
                        $driverRoute->setDateCreated($incidentDate);
                        $driverRoute->setShiftType($shift);
                        $driverRoute->setIsTemp('');
                        $driverRoute->setIsOpenShift(empty($driver));
                        $driverRoute->setIsActive(true);
                        $driverRoute->setShiftName($shift->getName());
                        $driverRoute->setShiftColor($shift->getColor());
                        $driverRoute->setUnpaidBreak($shift->getUnpaidBreak());
                        $driverRoute->setShiftNote('');
                        // $driverRoute->setShiftInvoiceType($shiftInvoiceType);
                        $driverRoute->setShiftStation($shift->getStation());
                        $driverRoute->setShiftSkill($shift->getSkill());
                        $driverRoute->setShiftCategory($shift->getCategory());
                        $driverRoute->setShiftBalanceGroup($shift->getBalanceGroup());
                        $driverRoute->setShiftTextColor($shift->getTextColor());
                        $driverRoute->setStartTime($shift->getStartTime());
                        $driverRoute->setEndTime($shift->getEndTime());
                        $driverRoute->setIsRescuer(false);
                        $driverRoute->setIsRescued(false);
                        $driverRoute->setIsLoadOut(false);
                        $driverRoute->setIsBackup(false);
                        $driverRoute->setCurrentNote('');
                        $driverRoute->setNewNote('');
                        $this->getEntityManager()->persist($driverRoute);
                        $this->getEntityManager()->flush();
                    }
                }
                if(strpos($dataArrKeyValue['column'][8], '*') !== false && empty($columnLine[8])){
                    $validationMsg .= "Please enter a Phase1 Step1 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[8]), array('yes', 'no'))) {
                    $validationMsg .= "please fill the Phase1 Step1 Q1 with (yes, no)!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][9], '*') !== false && empty($columnLine[9])){
                    $validationMsg .= "Please enter a Phase1 Step2 Q1!, ";
                    $validation = false;
                } else if (!in_array(trim($columnLine[9]), array('weekday', 'weekend'))) {
                    $validationMsg .= "please fill the Phase1 Step2 Q1 with (weekday, weekend)!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['ncns-report-step2-field1'] = "".$columnLine[8];
                $incidentStepValue['ncns-report-step3-field1'] = $columnLine[9];
                $incidentStepValue['ncns-report-step3-field2'] = $columnLine[10];
                if(strpos($dataArrKeyValue['column'][11], '*') !== false && empty($columnLine[11])){
                    $validationMsg .= "Please enter a Phase1 Submited By with valid Email!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedByPhase1Obj = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[11]), 'isArchive' => false]);
                    if (!$submitedByPhase1Obj) {
                        $validationMsg .= "Please enter valid Phase1 Submited By user with valid Email!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedBy-1'] = $submitedByPhase1Obj;
                }
                if(strpos($dataArrKeyValue['column'][12], '*') !== false && empty($columnLine[12])){
                    $validationMsg .= "Please enter a Phase1 Submited at!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedAtPhase1 = new \DateTime(trim($columnLine[12]));
                    if ($submitedAtPhase1 instanceof DateTime) {
                        $validationMsg .= "Please enter valid Phase1 Submited at!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedAt-1'] = $submitedAtPhase1;
                }
                if(strpos($dataArrKeyValue['column'][13], '*') !== false && empty($columnLine[13])){
                    $validationMsg .= "Please enter a Phase2 Step1 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[13]), array('call', 'text', 'email', 'nocontacted'))) {
                    $validationMsg .= "please fill the Phase2 Step1 Q1 with (call, text, email, nocontacted)!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][14], '*') !== false && empty($columnLine[14])){
                    $validationMsg .= "Please enter a Phase2 Step2 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[14]), array('yes', 'no'))) {
                    $validationMsg .= "please fill the Phase2 Step2 Q1 with (yes, no)!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['ncns-review-step1-field1'] = trim($columnLine[13]);
                $incidentStepValue['ncns-review-step2-field1'] = trim($columnLine[14]);
                $incidentStepValue['ncns-review-step3-field1'] = trim($columnLine[15]);
                // if(strpos($dataArrKeyValue['column'][16], '*') !== false && $columnLine[16] != ''){
                //     $validationMsg .= "Please enter a Phase2 Step4 Q1!, ";
                //     $validation = false;
                // } else
                if (strpos($dataArrKeyValue['column'][16], '*') !== false && !in_array(trim($columnLine[16]), array('0', '1', '2', '3'))) {
                    $validationMsg .= "please fill the Phase2 Step4 Q1 with (0,1,2,3)!, ".$newLine;
                    $validation = false;
                }
                if(strpos($dataArrKeyValue['column'][17], '*') !== false && empty($columnLine[17])){
                    $validationMsg .= "Please enter a Phase2 Step5 Q1!, ".$newLine;
                    $validation = false;
                } else if (!in_array(trim($columnLine[17]), array('yes', 'no'))) {
                    $validationMsg .= "please fill the Phase2 Step5 Q1 with ('yes', 'no')!, ".$newLine;
                    $validation = false;
                }
                $incidentStepValue['ncns-review-step4-field1'] = trim((int)$columnLine[16]);
                $incidentStepValue['ncns-review-step5-field1'] = trim($columnLine[17]);
                $incidentStepValue['ncns-review-step5-field2'] = trim($columnLine[18]);
                if(strpos($dataArrKeyValue['column'][19], '*') !== false && empty($columnLine[19])){
                    $validationMsg .= "Please enter a Phase2 Submited By with Email!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedByPhase2Obj = $em->getRepository(User::class)->findOneBy(['email' => trim($columnLine[19]),'isArchive' => false]);
                    if (!$submitedByPhase2Obj) {
                        $validationMsg .= "Please enter valid Phase2 Submited By user with valid Email!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedBy-2'] = $submitedByPhase2Obj;
                }
                if(strpos($dataArrKeyValue['column'][20], '*') !== false && empty($columnLine[20])){
                    $validationMsg .= "Please enter a Phase2 Submited at!, ".$newLine;
                    $validation = false;
                } else {
                    $submitedAtPhase2 = new \DateTime(trim($columnLine[20]));
                    if ($submitedAtPhase2 instanceof DateTime) {
                        $validationMsg .= "Please enter valid Phase2 Submited at!, ".$newLine;
                        $validation = false;
                    }
                    $incidentStepValue['phase-submitedAt-2'] = $submitedAtPhase2;
                }
                //Check validation
                if ($validation) {
                    $incidentDateTime = new \DateTime($columnLine[6].' '.$columnLine[7]);
                    $incident = $this->insertIncident($company, $incidentTitle, $station, $incidentType, $user, $driver, $incidentDateTime, $driverRoute);
                    $incidentPhases = $em->getRepository(IncidentPhase::class)->getIncidentPhaseByType($incidentType->getId());
                    $phaseCount = 1;
                    foreach ($incidentPhases as $incidentPhase) {
                        foreach ($incidentPhase->getIncidentSteps() as $step) {
                            foreach ($step->getIncidentFormFields() as $incidentFormField) {
                                if (isset($incidentStepValue[$incidentFormField->getToken()])) {
                                    $incidentFormFieldValue = new IncidentFormFieldValue();
                                    $incidentFormFieldValue->setIncidentFormField($incidentFormField);
                                    $incidentFormFieldValue->setIncident($incident);
                                    $incidentFormFieldValue->setValue([$incidentStepValue[$incidentFormField->getToken()]]);
                                    $incidentFormFieldValue->setCreatedDate(new \DateTime());
                                    $this->getEntityManager()->persist($incidentFormFieldValue);
                                    $this->getEntityManager()->flush();
                                    if ($incidentFormField->getFieldId() == '21') {
                                        $imagesArrays = explode(',',$incidentStepValue[$incidentFormField->getToken()]);
                                        foreach($imagesArrays as $imagesArr) {
                                            $image = new Image();
                                            $image->setPath($imagesArr);
                                            $this->getEntityManager()->persist($image);
                                            $this->getEntityManager()->flush();

                                            $incidentFile = new IncidentFile();
                                            $incidentFile->setImage($image);
                                            $incidentFile->setIncidentFormFieldValue($incidentFormFieldValue);
                                            $this->getEntityManager()->persist($incidentFile);
                                            $this->getEntityManager()->flush();
                                        }
                                    }
                                }

                            }
                        }
                        $incidentSubmitedPhase = new IncidentSubmitedPhase();
                        $incidentSubmitedPhase->setIncidentPhase($incidentPhase);
                        $incidentSubmitedPhase->setSubmitedBy($incidentStepValue['phase-submitedBy-'.$phaseCount]);
                        $incidentSubmitedPhase->setSubmittedAt($incidentStepValue['phase-submitedAt-'.$phaseCount]);
                        $incidentSubmitedPhase->setIncident($incident);
                        $this->getEntityManager()->persist($incidentSubmitedPhase);
                        $this->getEntityManager()->flush();
                        $newPhase = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->findOneBy(['incidentType' => $incident->getIncidentType()->getId(), 'ordering' => $incidentPhase->getOrdering()+1]);
                        if($newPhase){
                            $incident->setIncidentCurrentPhase($newPhase);
                        }else{
                            $incident->setIncidentCurrentPhase(null);
                        }
                        $incident->setIncidentCompletedPhase($incidentPhase);
                        $em->persist($incident);
                        $em->flush();
                        $phaseCount++;
                    }

                } else {
                    $globalValidationMsg .= '('.($count+1).') '.$validationMsg.$newLine;
                }
            }
            $count++;
        }
        if($globalValidationMsg != ''){
            throw new NotFoundHttpException($globalValidationMsg);
        }
        fclose($file);
    }

    function urlFileExists($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

    public function insertIncident($company, $incidentTitle, $station, $incidentType, $user, $driver, $incidentDateTime, $driverRoute)
    {
        $em = $this->getEntityManager();
        $incident = new Incident();
        $incident->setCompany($company);
        $incident->setTitle($incidentTitle);
        $incident->setStation($station);
        $incident->setIncidentType($incidentType);
        $incident->setDriver($driver);
        $incident->setDateTime($incidentDateTime);
        $incident->setDriverRoute($driverRoute);
        $incident->setCreatedBy($user);
        $incident->setIncidentCurrentPhase($incidentType->getIncidentPhases()->first());
        $incident->setStatus(0);
        $em->persist($incident);
        $em->flush();

        return $incident;
    }
}
