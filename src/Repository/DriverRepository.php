<?php

namespace App\Repository;

use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\DriverSkill;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\ReturnToStation;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use App\Utils\GlobalUtility;

/**
 * @method Driver|null find($id, $lockMode = null, $lockVersion = null)
 * @method Driver|null findOneBy(array $criteria, array $orderBy = null)
 * @method Driver[]    findAll()
 * @method Driver[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DriverRepository extends ServiceEntityRepository
{

    protected $tokenStorage;

    private $globalUtils;

    public function __construct(
        ManagerRegistry $registry,
        TokenStorageInterface $tokenStorage,
        GlobalUtility $globalUtils
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->globalUtils = $globalUtils;
        parent::__construct($registry, Driver::class);
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getDrivers($criteria)
    {
        $stationId = $criteria['station'];
        $em = $this->getEntityManager();
        $driverQuery = "SELECT d.id ,u.friendly_name,u.phone_number,role.name as role,u.profile_image as image, u.id as user_id,
            (SELECT GROUP_CONCAT(s.name) FROM schedule s
            LEFT JOIN schedule_driver ON schedule_driver.schedule_id = s.id
            WHERE schedule_driver.driver_id = d.id AND s.start_date <= CURRENT_DATE  AND s.end_date > CURRENT_DATE AND s.is_archive = 0 ) as schedules,
            (SELECT GROUP_CONCAT(skill.name) FROM skill LEFT JOIN driver_skill ON driver_skill.skill_id = skill.id WHERE skill.is_archive = 0  AND driver_skill.driver_id = d.id) as skill,
            (SELECT GROUP_CONCAT(skill.id) FROM skill LEFT JOIN driver_skill ON driver_skill.skill_id = skill.id WHERE skill.is_archive = 0  AND driver_skill.driver_id = d.id) as skillId
            FROM driver d
            LEFT JOIN driver_station ds ON d.id = ds.driver_id
            LEFT JOIN station st ON ds.station_id = st.id
            LEFT JOIN user u ON u.id = d.user_id
            LEFT JOIN user_role ON user_role.user_id = u.id
            LEFT JOIN role ON role.id = user_role.role_id
            WHERE d.is_archive = 0 AND d.employee_status = 2 AND st.id = ".$stationId." AND u.is_archive = 0 AND st.is_archive = 0 ORDER BY u.first_name";
            $drivers = $this->getEntityManager()
                        ->getConnection()
                        ->query($driverQuery)
                        ->fetchAll();

            $results = $temp= $skills = $tempSkillName = $tempSkillId = $schedules = [];
            foreach ($drivers as $driver) {

                $tempSkill = $driver['skill'] !='' ? explode(",",$driver['skill']) : [];
                $tempSkillId = $driver['skillId'] !='' ? explode(",",$driver['skillId']) : [];
                $skillObj=[];
                if(count($tempSkill) > 0){
                    foreach($tempSkill as $key => $value){
                        $skillObj[$key] = ['id'=>$tempSkillId[$key],'skill'=>['id'=>$tempSkillId[$key],'name'=>$value]];
                    }
                }
                $schedules = $driver['schedules'] != '' ? explode(",",$driver['schedules']) : [];
                $scheduleObj=[];
                if (count($schedules) > 0) {
                    foreach ($schedules as $key => $value) {
                        $scheduleObj[$key] = ['id'=>0,'name'=>$value];
                    }
                }

                $temp['id'] = $driver['id'];
                $temp['name'] = $driver['friendly_name'];
                $temp['cel'] = $driver['phone_number'];
                $temp['role'] = $this->globalUtils->getPosition($driver['role']);
                $temp['color'] = '#00A38D';
                $temp['skills'] = $skillObj;
                $temp['schedule'] =$scheduleObj;
                $temp['op'] = [['value'=>$driver['id'],'label'=>'']];
                $temp['grade'] = 'A';
                $temp['image'] = $driver['image'];
                $temp['onAddDriver'] = true;
                $temp['userId'] = $driver['user_id'];

                array_push($results,$temp);
                $skills = array_merge($skills, explode(",",$driver['skill']));
            }
            $skills = array_values(array_unique($skills));
            return ['driver'=>$results,'skill'=>$skills];
    }

    public function getAllDrivers($companyId, $stationId = null)
    {
        $sub = $this->createQueryBuilder('d')
                ->select('d.id,d.drivingPreference,d.employeeStatus,d.terminationDate,d.offboardedDate,d.inactiveAt,
                            u.id as user_id,u.friendlyName,u.profileImage')
                ->leftJoin('d.stations', 's')
                ->leftJoin('d.user', 'u')
                ->andWhere('d.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('d.isEnabled = :isEnabled')
                ->setParameter('isEnabled', true)
                ->andWhere('u.isArchive = :isArchiveUser')
                ->setParameter('isArchiveUser', false)
                ->andWhere('u.isEnabled = :isEnabled')
                ->setParameter('isEnabled', true)
                ->andWhere('d.company = :companyId')
                ->setParameter('companyId', $companyId)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->addOrderBy('u.friendlyName', 'ASC')
                ->andWhere('d.employeeStatus = :employeeStatus')
                ->setParameter('employeeStatus', Driver::STATUS_ACTIVE);

          if (!empty($stationId)) {
              $sub->andWhere('s.id = :station')
                  ->setParameter('station', $stationId);
          }

         $results = $sub->getQuery()->getScalarResult();

         return $results;

    }

    public function getAllDriversNew($companyId, $stationId = null)
    {
        $sub = $this->createQueryBuilder('d')
                ->select('d.id,d.drivingPreference,d.employeeStatus,d.terminationDate,d.offboardedDate,d.inactiveAt,
                            u.id as user_id,u.friendlyName,u.profileImage')
                ->leftJoin('d.stations', 's')
                ->leftJoin('d.user', 'u')
                ->andWhere('d.isArchive = :isArchive')
                ->setParameter('isArchive', false)
                ->andWhere('d.isEnabled = :isEnabled')
                ->setParameter('isEnabled', true)
                ->andWhere('u.isArchive = :isArchiveUser')
                ->setParameter('isArchiveUser', false)
                ->andWhere('u.isEnabled = :isEnabled')
                ->setParameter('isEnabled', true)
                ->andWhere('d.company = :companyId')
                ->setParameter('companyId', $companyId)
                ->andWhere('s.isArchive = :isArchiveStation')
                ->setParameter('isArchiveStation', false)
                ->addOrderBy('u.friendlyName', 'ASC')
                ->andWhere('d.employeeStatus = :employeeStatus')
                ->setParameter('employeeStatus', Driver::STATUS_ACTIVE);

          if (!empty($stationId)) {
              $sub->andWhere('s.id = :station')
                  ->setParameter('station', $stationId);
          }

         $results = $sub->getQuery()->getScalarResult();

         return $results;

    }

    public function getOpenShiftPopupAllDrivers($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $currentDate = $datetime = isset($criteria['date']) ? $criteria['date'] : date('Y-m-d');

        $station = $criteria['stationId'];

        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $sub = $this->getEntityManager()->createQueryBuilder();

        $sub->select('drv.id')
            ->from('App\Entity\DriverRoute', 'dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'drv')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('dr.startTime IS NOT NULL');

        if (!empty($station)) {
            $sub->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        $results = $sub->getQuery()->getResult();
        $driverIds = array_column($results, 'id');

        $sub1 = $this->getEntityManager()->createQueryBuilder();

        $sub1->select('drv.id')
            ->from('App\Entity\TempDriverRoute', 'tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->leftJoin('tdr.driver', 'drv')
            ->where('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('tdr.driver IS NOT NULL')
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.isArchive = :isArchiveTempDriverRoute')
            ->setParameter('isArchiveTempDriverRoute', false)
            ->andWhere('tdr.startTime IS NOT NULL');

        if (!empty($station)) {
            $sub1->andWhere('tdr.station= :station')
                ->setParameter('station', $station);
        }

        $results1 = $sub1->getQuery()->getResult();
        $tempDriverIds = array_column($results1, 'id');
        $driverIds = array_merge($driverIds, $tempDriverIds);

        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin('d.user', 'u')
            ->leftJoin('d.stations', 's')
            ->where('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $userLogin->getCompany()->getId())
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false);

        if (!empty($driverIds)) {
            $qb->andWhere($qb->expr()->notIn('d.id', $driverIds));
        }

        $driversResults = $qb->getQuery()->getResult();
        $driversData = [];
        foreach ($driversResults as $driversResult) {
            $driversData[] = [
                'id' => $driversResult->getId(),
                'name' => $driversResult->getFullName(),
                'title' => $driversResult->getUser()->getUserRoles()[0]->getRoleName(),
                'img' => $driversResult->getUser()->getProfileImage(),
            ];
        }
        return $driversData;
    }

    public function getAvailableDriversForReplace($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $currentDate = $datetime = isset($criteria['date']) ? $criteria['date'] : date('Y-m-d');

        $station = $criteria['stationId'];
        $routeId = $criteria['routeId'];
        $shiftDate = $criteria['shiftDate'];
        $driverId = ($criteria['driverId'] === '0.0.0') ? null : $criteria['driverId'];

        $sub = $this->getEntityManager()->createQueryBuilder();

        $sub->select('drv.id')
            ->from('App\Entity\DriverRoute', 'dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'drv')
            ->where('dr.dateCreated = :shiftDate')
            ->setParameter('shiftDate', $shiftDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('drv.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1);

        if (!empty($station)) {
            $sub->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        $results = $sub->getQuery()->getResult();

        $driverIds = array_column($results, 'id');

        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin('d.user', 'u')
            ->leftJoin('d.stations', 's')
            ->where('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $userLogin->getCompany()->getId())
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->orderBy('u.friendlyName', 'ASC');

        if (!empty($driverIds)) {
            $qb->andWhere($qb->expr()->notIn('d.id', $driverIds));
        }

        $driversResults = $qb->getQuery()->getResult();
        $driversData = [];
        foreach ($driversResults as $driversResult) {
            $driversData[] = [
                'id' => $driversResult->getId(),
                'name' => $driversResult->getFullName(),
                'title' => $driversResult->getUser()->getUserRoles()[0]->getRoleName(),
                'img' => $driversResult->getUser()->getProfileImage(),
            ];
        }
        return $driversData;
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function searchByJobTitle(array $criteria)
    {
        if (empty($criteria['jobTitle'])) {
            return ['success' => false, 'message' => 'jobTitle Not found'];
        }
        if (empty($criteria['companyId'])) {
            return ['success' => false, 'message' => 'companyId Not found'];
        }

        $qb = $this->createQueryBuilder('d');
        $result = $qb->where('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.jobTitle LIKE :jobTitle')
            ->setParameter('jobTitle', '%' . $criteria['jobTitle'] . '%')
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $criteria['companyId'])
            ->getQuery()
            ->getResult();

        if (is_array($result) && sizeof($result) > 0) {
            $drivers = array_map(function (Driver $driver) {
                return [
                    'id' => $driver->getId(),
                    'jobTitle' => $driver->getJobTitle(),
                ];
            }, $result);
            return
                [
                    'success' => true,
                    'message' => 'Drivers Found',
                    'count' => sizeof($drivers),
                    'drivers' => $drivers,
                ];
        }
        return ['success' => false, 'message' => 'Drivers Not Found', 'count' => 0];
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getDriverRoutesByDateByDriver(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('driverId Not found');
        }
        if (empty($criteria['date'])) {
            throw new Exception('date Not found');
        }

        $date = new DateTime($criteria['date']);

        $driver = $this->find($criteria['driverId']);

        if (!($driver instanceof Driver)) {
            throw new Exception('Driver not found');
        }

        $driverRoutes = $driverRoutes = $this->_em->getRepository(DriverRoute::class)
            ->findBy(['driver' => $criteria['driverId'], 'dateCreated' => $date,
                'isArchive' => false], ['dateCreated' => 'DESC']);

        if (is_array($driverRoutes) && sizeof($driverRoutes) > 0) {
            $count = sizeof($driverRoutes);
            if (!empty($criteria['first']) && $criteria['first'] && $driverRoutes[0] instanceof DriverRoute && $driverRoutes[0]->getStation() instanceof Station) {
                $station = $driverRoutes[0]->getStation();
                return ['success' => true,
                    'message' => 'DriverRoute Found',
                    'count' => $count,
                    'driverRoute' => [
                        'id' => $driverRoutes[0]->getId(),
                        'station' => [
                            'id' => $station->getId(),
                            'code' => $station->getCode(),
                            'name' => $station->getName(),
                        ],
                    ],
                ];
            }
            return ['success' => true,
                'message' => 'DriverRoutes Found',
                'count' => $count,
                'driverRoutes' => $driverRoutes,
            ];
        }

        throw new Exception('DriverRoute Not Found');
    }

    /**
     * Assume only first DriverRoute
     * @param array $criteria
     * @return array
     */
    public function getAndSetDriverRoute(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            return ['success' => false, 'message' => 'driverId Not found'];
        }
        if (empty($criteria['date'])) {
            return ['success' => false, 'message' => 'date Not found'];
        }
        try {
            $date = new DateTime($criteria['date']);
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'new Datetime fail'];
        }

        $driver = $this->find($criteria['driverId']);

        if (!($driver instanceof Driver)) {
            return ['success' => false, 'message' => 'Driver not found'];
        }

        $driverRoutes = $driverRoutes = $this->_em->getRepository(DriverRoute::class)
            ->getDriverRoutesSortedByStartTimeFilterByDay($criteria);

        if (is_array($driverRoutes) && sizeof($driverRoutes) > 0) {
            $count = sizeof($driverRoutes);
            if ($driverRoutes[0] instanceof DriverRoute && $driverRoutes[0]->getStation() instanceof Station) {
                $station = $driverRoutes[0]->getStation();
                return [
                    'success' => true,
                    'message' => 'DriverRoute Found',
                    'count' => $count,
                    'driverRoute' => [
                        'id' => $driverRoutes[0]->getId(),
                        'dateCreated' => $driverRoutes[0]->getDateCreated(),
                        'station' => [
                            'id' => $station->getId(),
                            'code' => $station->getCode(),
                            'name' => $station->getName(),
                        ],
                    ],
                ];
            }
            return [
                'success' => true,
                'message' => 'DriverRoutes Found, station not found',
                'count' => $count,
                'driverRoute' => [
                    'id' => $driverRoutes[0] instanceof DriverRoute ? $driverRoutes[0]->getId() : null,
                    'station' => null,
                ],
            ];
        }

        /** @var Driver $driver */
        $driver = $this->_em->getRepository(Driver::class)->find($criteria['driverId']);
        if ($driver->getStations()->count() > 0) {
            /** @var Station $station */
            $station = $driver->getStations()->first();
            $backUpShift = $this->_em->getRepository(Shift::class)->findOneBy([
                'isArchive' => false,
                'station' => $station,
                'category' => 2
            ]);

            if (!($backUpShift instanceof Shift)) {
                return ['success' => false, 'message' => 'Shift Backup not found'];
            }

            $backUpDriverRoute = $this->_em->getRepository(DriverRoute::class)
                ->addDriverRouteForCortex(
                    $backUpShift,
                    $date,
                    $driver,
                    null
                );

            try {
                $this->_em->persist($backUpDriverRoute);
                $this->_em->flush();
            } catch (Exception $exception) {
                return ['success' => false, 'message' => $exception->getMessage()];
            }

            $backUpTempDriverRoute = new TempDriverRoute();
            $backUpTempDriverRoute->setStation($station);
            $backUpTempDriverRoute->setDriver($driver);
            $backUpTempDriverRoute->setShiftType($backUpShift);
            $backUpTempDriverRoute->setDateCreated($date);
            $backUpTempDriverRoute->setRouteId($backUpDriverRoute);
            $backUpTempDriverRoute->setStartTime($backUpShift->getStartTime());
            $backUpTempDriverRoute->setEndTime($backUpShift->getEndTime());

            try {
                $this->_em->persist($backUpTempDriverRoute);
                $this->_em->flush();
            } catch (Exception $exception) {
                return ['success' => false, 'message' => $exception->getMessage()];
            }

            return [
                'success' => true,
                'message' => 'DriverRoute Found and is Backup shift type.',
                'count' => 0,
                'driverRoute' => [
                    'id' => $backUpDriverRoute->getId(),
                    'station' => [
                        'id' => $station->getId(),
                        'code' => $station->getCode(),
                        'name' => $station->getName(),
                    ],
                ],
            ];

        }

        return ['success' => false, 'message' => 'Error'];
    }

    /**
     * @param $currentUser
     * @param $route
     * @param $device
     * @param $criteria
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addEventDeviceNull($currentUser, $route, $device, $criteria)
    {
        $event = new Event();
        $event->setCreatedAt(new DateTime('now'));
        $event->setDriverRoute($route);
        $name = "N/A";
        if ($currentUser) {
            $event->setUser($currentUser);
            $name = $currentUser->getFriendlyName();
        }
        $criteria['friendlyName'] = $name;
        if (is_null($device)) {
            $event->setEventName('DEVICE_NULL');
            $event->setName('DEVICE_NULL');
            $event->setData($criteria);
            $this->_em->persist($event);
            $this->_em->flush();
        }
    }

    /**
     * @param $currentUser
     * @param $route
     * @param $data
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addEventRouteCheck($currentUser, $route, $data)
    {
        $event = new Event();
        $event->setCreatedAt(new DateTime('now'));
        $event->setDriverRoute($route);
        $name = "N/A";
        if ($currentUser) {
            $event->setUser($currentUser);
            $name = $currentUser->getFriendlyName();
        }
        $data['friendlyName'] = $name;
        $event->setEventName('Load Out');
        $event->setName('LOAD_OUT');
        $event->setData($data);
        $event->setMessage($name . " starts Load Out");
        $this->_em->persist($event);
        $this->_em->flush();
    }

    /**
     * This function assume only the first found Driver Route as the Main Driver Route
     * @param array $criteria
     * @return array|array[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getTodayDriverRoute(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            return ['success' => false, 'message' => 'driverId Not found'];
        }
        if (empty($criteria['date'])) {
            return ['success' => false, 'message' => 'date Not found'];
        }
        if (empty($criteria['deviceId'])) {
            return ['success' => false, 'message' => 'deviceId Not found'];
        }

        try {
            $driverRoutes = $this->_em->getRepository(DriverRoute::class)
                ->getDriverRoutesSortedByStartTimeFilterByDay($criteria);
        } catch (Exception $e) {
            return ['success' => false, 'message' => 'DriverRoute Repository failed.'];
        }

        if (is_array($driverRoutes) && sizeof($driverRoutes) > 0) {
            /** @var DriverRoute $route */
            $route = $driverRoutes[0];
        } else {
            return ['success' => false, 'message' => 'Driver Route Not Found'];
        }

        $device = $this->_em->getRepository(Device::class)->find($criteria['deviceId']);

        /** @var User $currentUser */
        $currentUser = $this->tokenStorage->getToken()->getUser();

        $this->addEventDeviceNull($currentUser, $route, $device, $criteria);

        if (!($device instanceof Device)) {
            $device = $this->_em->getRepository(Device::class)->find(166);
        }

        if ($device instanceof Device) {
            $found = false;
            if ($route->getDevice()->count() > 0) {
                foreach ($route->getDevice() as $item) {
                    if ($item->getId() === $device->getId()) {
                        $found = true;
                        break;
                    }
                }
                if ($found === false) {
                    $route->addDevice($device);
                }
            } else {
                $route->addDevice($device);
            }
            $this->_em->flush();
        }

        if (!empty($criteria['mobileVersionUsed'])) {
            $route->setMobileVersionUsed($criteria['mobileVersionUsed']);
        }

        if (!($route->getKickoffLog() instanceof KickoffLog)) {
            $kickOffLog = new KickoffLog();
            $kickOffLog->setDriverRoute($route);
            if ($device instanceof Device) {
                $kickOffLog->setDevice($device);
            }

            $kickOffLog->setVanSuppliesResult([
                ["name" => "Charging Cord", 'checked' => false],
                ["name" => "Is Mobile device charge up to 100%?", 'checked' => false],
                ["name" => "Phone mount", 'checked' => false],
                ["name" => "Fuel Card", 'checked' => false],
                ["name" => "Van Off", 'checked' => false],
                ["name" => "Battery Packs", 'checked' => false],
                ["name" => "Snow Shovel", 'checked' => false],
                ["name" => "Sand Bag", 'checked' => false],
                ["name" => "Ice scraper", 'checked' => false],
                ["name" => "Spare Tire", 'checked' => false],
            ]);
            try {
                $this->_em->persist($kickOffLog);
            } catch (ORMException $e) {
                return ['success' => false, 'message' => 'Error Persisting Load Out'];
            }
            try {
                $this->_em->flush();
                $route->setKickoffLog($kickOffLog);
                $this->_em->flush();
            } catch (OptimisticLockException $e) {
                return ['success' => false, 'message' => 'Error Saving Load Out'];
            } catch (ORMException $e) {
                return ['success' => false, 'message' => 'Error Saving Load Out'];
            }
        }

        if (!($route->getReturnToStation() instanceof ReturnToStation)) {
            $returnToStation = new ReturnToStation();
            if ($device instanceof Device) {
                $returnToStation->setDevice($device);
            }
            $returnToStation->setDriverRoute($route);
            $returnToStation->setStationCheckListResult([
                ["name" => "Are you inside the station?", "checked" => false],
                ["name" => "Windows down?", "checked" => false],
                ["name" => "Light on?", "checked" => false],
                ["name" => "Flasher on?", "checked" => false],
                ["name" => "Turn off van when parked", "checked" => false],
                ["name" => "Leave the key in the ignition", "checked" => false],
                ["name" => "Return empty totes to the Station designated location", "checked" => false],
                ["name" => "Check-in with Amazon RTS personnel to report route completion", "checked" => false],
                ["name" => "Return any remaining and/or report any missing packages", "checked" => false],
            ]);
            try {
                $this->_em->persist($returnToStation);
            } catch (ORMException $e) {
                return ['success' => false, 'message' => 'Error Persisting RTS'];
            }
            try {
                $this->_em->flush();
                $route->setReturnToStation($returnToStation);
                $this->_em->flush();
            } catch (OptimisticLockException $e) {
                return ['success' => false, 'message' => 'Error Saving RTS'];
            } catch (ORMException $e) {
                return ['success' => false, 'message' => 'Error Saving RTS'];
            }
        }

        $driverRouteId = $route->getId();
        $kickoffLog = $route->getKickoffLog();
        $returnToStation = $route->getReturnToStation();
        $this->addEventRouteCheck($currentUser, $route, [
            'driverRouteId' => $driverRouteId,
            'kickoffLogId' => $kickoffLog ? $kickoffLog->getId() : null,
            'returnToStationId' => $returnToStation ? $returnToStation->getId() : null,
            'deviceId' => $device ? $device->getId() : null,
        ]);
        return [
            'success' => true,
            'message' => 'Saving correctly Load Out & RTS to Route',
            'route' =>
                [
                    'id' => $route->getId(),
                    'kickoffLog' => $kickoffLog,
                    'returnToStation' => $returnToStation,
                    'driverRouteCodes' => array_map(function (DriverRouteCode $item) {
                        return $item->getCode();
                    }, $route->getDriverRouteCodes()->toArray()),
                ],
        ];
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function getDriverRouteByDay(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            return [
                'success' => false,
                'message' => 'driverId not found'];
        }

        if (empty($criteria['date'])) {
            return [
                'success' => false,
                'message' => 'date not found'];
        }

        try {
            $dateTime = new DateTime($criteria['date']);
            $driverRoutes = $this->_em
                ->getRepository(DriverRoute::class)
                ->findBy(
                    ['driver' => $criteria['driverId'], 'dateCreated' => $dateTime, 'isArchive' => false],
                    ['dateCreated' => 'DESC']
                );

            return [
                'success' => true,
                'driverRoutes' => $driverRoutes,
                'count' => sizeof($driverRoutes),
            ];

        } catch (Exception $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()];
        }
    }

    /**
     * @param array $criteria
     * @return array
     */
    public function getDriverRouteByDays(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            return [
                'success' => false,
                'message' => 'driverId not found'];
        }

        if (empty($criteria['days'])) {
            return [
                'success' => false,
                'message' => 'days not found'];
        }

        $result = [];
        foreach ($criteria['days'] as $day) {
            try {
                $dateTime = new DateTime($day['dateCreated']);
            } catch (Exception $e) {
                return [
                    'success' => false,
                    'message' => $e->getMessage()];
            }

            $driverRoutes = $this->_em
                ->getRepository(DriverRoute::class)
                ->findBy(
                    ['driver' => $criteria['driverId'], 'dateCreated' => $dateTime, 'isArchive' => false],
                    ['dateCreated' => 'DESC']
                );
            if (sizeof($driverRoutes) > 0) {
                $result[] = [
                    'dateCreated' => $day['dateCreated'],
                    'driverRoutes' => $driverRoutes,
                ];
            }

        }

        return $result;
    }

    /**
     * @param array $criteria
     * @return array
     * @throws Exception
     */
    public function getTodayRoute(array $criteria)
    {
        if (empty($criteria['driverId'])) {
            throw new Exception('driverId not found');
        }
        if (empty($criteria['date'])) {
            throw new Exception('date not found');
        }

        $now = new DateTime($criteria['date']);
        $driverRoutes = $this->_em
            ->getRepository(DriverRoute::class)
            ->findBy(
                ['driver' => $criteria['driverId'], 'dateCreated' => $now, 'isArchive' => false],
                ['dateCreated' => 'DESC']
            );

        $result = [];

        if (is_array($driverRoutes) && sizeof($driverRoutes) > 0) {
            /** @var DriverRoute $driverRoute */
            foreach ($driverRoutes as $driverRoute) {
                if ($driverRoute->getIsArchive() === false) {
                    $result[] = [
                        'driverRoute' => $driverRoute,
                        'shift' => $driverRoute->getShiftType(),
                        'station' => $driverRoute->getStation(),
                    ];
                }
            }
        } else {
            throw new Exception('DriverRoutes not found');
        }
        return $result;
    }

    public function getOpenShiftPopupAllDriversForAddDriver($criteria)
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();
        $currentDate = $datetime = isset($criteria['date']) ? $criteria['date'] : date('Y-m-d');
        $station = $criteria['stationId'];

        $checkIncident = isset($criteria['checkIncident']) ? $criteria['checkIncident'] : false;

        $sub = $this->getEntityManager()->createQueryBuilder();

        $sub->select('drv.id')
            ->from('App\Entity\DriverRoute', 'dr')
            ->leftJoin('dr.shiftType', 'sf')
            ->leftJoin('dr.station', 'st')
            ->leftJoin('dr.driver', 'drv')
            ->where('dr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('dr.driver IS NOT NULL')
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.isArchive = :isArchiveDriverRoute')
            ->setParameter('isArchiveDriverRoute', false)
            ->andWhere('dr.isActive = :isActive')
            ->setParameter('isActive', 1)
            ->andWhere('drv.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1);

        if (!empty($station)) {
            $sub->andWhere('dr.station= :station')
                ->setParameter('station', $station);
        }

        $results = $sub->getQuery()->getResult();
        $driverIds = array_column($results, 'id');

        $sub1 = $this->getEntityManager()->createQueryBuilder();

        $sub1->select('drv.id')
            ->from('App\Entity\TempDriverRoute', 'tdr')
            ->leftJoin('tdr.shiftType', 'sf')
            ->leftJoin('tdr.station', 'st')
            ->leftJoin('tdr.driver', 'drv')
            ->where('tdr.dateCreated = :currDate')
            ->setParameter('currDate', $currentDate)
            ->andWhere('sf.company = :company')
            ->setParameter('company', $userLogin->getCompany()->getId())
            ->andWhere('tdr.driver IS NOT NULL')
            ->andWhere('st.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('tdr.isArchive = :isArchiveTempDriverRoute')
            ->setParameter('isArchiveTempDriverRoute', false)
            ->andWhere('drv.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1);

        if (!empty($station)) {
            $sub1->andWhere('tdr.station= :station')
                ->setParameter('station', $station);
        }

        $results1 = $sub1->getQuery()->getResult();
        $tempDriverIds = array_column($results1, 'id');
        $driverIds = array_merge($driverIds, $tempDriverIds);

        $qb2 = $this->getEntityManager()->createQueryBuilder();

        $qb2->select('drv.id')
            ->from('App\Entity\Incident', 'i')
            ->leftJoin('i.driver', 'drv')
            ->andWhere('i.isArchive = :isArchiveIncident')
            ->andWhere('i.dateTime >= :startDate')
            ->andWhere('i.dateTime <= :endDate');

        if (!empty($station)) {
            $qb2->andWhere('i.station = :driverStation');
        }

        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin('d.user', 'u')
            ->leftJoin('d.stations', 's')
            ->where('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabledDriver')
            ->setParameter('isEnabledDriver', true)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('u.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $userLogin->getCompany()->getId())
            ->andWhere('s.isArchive = :isArchiveStation')
            ->setParameter('isArchiveStation', false)
            ->andWhere('d.employeeStatus = :employeeStatus')
            ->setParameter('employeeStatus', 2)
            ->addOrderBy('u.friendlyName', 'ASC');

        if (!empty($driverIds)) {
            $qb->andWhere($qb->expr()->notIn('d.id', $driverIds));
        }

        if (!empty($station)) {
            $qb->andWhere('s.id= :driverStation')
                ->setParameter('driverStation', $station);
        }

        if ($checkIncident) {
            $qb->andWhere($qb->expr()->notIn('d.id', $qb2->getDQL()))
                ->setParameter('isArchiveIncident', false)
                ->setParameter('startDate', $currentDate . ' 00:00:00')
                ->setParameter('endDate', $currentDate . ' 23:59:59');
        }

        $driversResults = $qb->getQuery()->getResult();
        $driversData = [];
        foreach ($driversResults as $driversResult) {
            if(($driversResult->getInactiveAt() && $driversResult->getInactiveAt()->format('Y-m-d') > $currentDate)|| ($driversResult->getOffboardedDate() && $driversResult->getOffboardedDate()->format('Y-m-d') > $currentDate) || ($driversResult->getTerminationDate() && $driversResult->getTerminationDate()->format('Y-m-d') > $currentDate)){
                    $driversData[] = [
                        'id' => $driversResult->getId(),
                        'name' => $driversResult->getFullName(),
                        'title' => $driversResult->getUser()->getUserRoles()[0]->getRoleName(),
                        'img' => $driversResult->getUser()->getProfileImage(),
                    ];
            } else {
                $driversData[] = [
                    'id' => $driversResult->getId(),
                    'name' => $driversResult->getFullName(),
                    'title' => $driversResult->getUser()->getUserRoles()[0]->getRoleName(),
                    'img' => $driversResult->getUser()->getProfileImage(),
                ];
            }
        }
        return $driversData;
    }

    public function form_getDrivers()
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        return $this->createQueryBuilder('d')
            ->leftJoin('d.user', 'u')
            ->select('d.id as id, u.friendlyName as name')
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $userLogin->getCompany()->getId())
            ->orderBy('u.friendlyName', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    public function form_getDriversForAsign()
    {
        $userLogin = $this->tokenStorage->getToken()->getUser();

        return $this->createQueryBuilder('d')
            ->leftJoin('d.user', 'u')
            ->leftJoin('u.userRoles', 'r')
            ->select('d.id as id, u.friendlyName as name')
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', true)
            ->andWhere('r.name = :assistant_manager OR r.name = :station_manager OR r.name = :owner OR r.name = :operations_manager OR r.name = :operations_account_manager')
            ->setParameter('operations_manager', 'ROLE_OPERATIONS_MANAGER')
            ->setParameter('assistant_manager', 'ROLE_ASSISTANT_STATION_MANAGER')
            ->setParameter('station_manager', 'ROLE_STATION_MANAGER')
            ->setParameter('operations_account_manager', 'ROLE_OPERATIONS_ACCOUNT_MANAGER')
            ->setParameter('owner', 'ROLE_OWNER')
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $userLogin->getCompany()->getId())
            ->orderBy('u.friendlyName', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * @param array $criteria
     * @return Driver[]
     * @throws Exception
     */
    public function getDriversBySkillName(array $criteria)
    {
        if (empty($criteria['name'])) {
            throw new Exception('name field empty.');
        }
        if (empty($criteria['companyId'])) {
            throw new Exception('companyId field empty.');
        }

        $driverSkills = $this->_em->getRepository(DriverSkill::class)->findBy(['isArchive' => false], ['id' => 'ASC']);
        $driverSkills = array_filter($driverSkills, function (DriverSkill $driverSkill) use ($criteria) {
            return $driverSkill->getSkill() instanceof Skill && $driverSkill->getSkill()->getName() === $criteria['name'];
        });

        $drivers = [];
        foreach ($driverSkills as $driverSkill) {
            if ($driverSkill->getSkill() instanceof Skill && $driverSkill->getSkill()->getName() === $criteria['name']) {
                $drivers[] = $driverSkill->getDriver();
            }
        }

        return $drivers;
    }

    /**
     * @param array $criteria
     * @return DriverRoute[]
     * @throws Exception
     */
    public function getDriversFromStation(array $criteria)
    {
        $date = new DateTime($criteria['date']);
        $results = $this->createQueryBuilder('d')
            ->leftJoin('d.user', 'u')
            ->leftJoin('d.driverRoutes', 'dr')
            ->andWhere('dr.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $date)
            ->andWhere('d.id != :driverId')
            ->setParameter('driverId', $criteria['driver'])
            ->andWhere('dr.station = :station')
            ->setParameter('station', $criteria['station'])
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('d.employeeStatus = :employeeStatus')
            ->setParameter('employeeStatus', Driver::STATUS_ACTIVE)
            ->orderBy('u.friendlyName', 'ASC')
            ->getQuery()
            ->getResult();

        $temp = $driverArray = [];
        foreach ($results as $result) {
            if ($result->getUser()->getProfileImage()) {
                $imgUrl = $result->getUser()->getProfileImage();
            } else {
                $imgUrl = '';
            }
            $temp = ["fullName" => $result->getFullName(), 'id' => $result->getId(), 'jobTitle' => $result->getJobTitle(), 'user' => ['image' => $imgUrl]];
            array_push($driverArray, $temp);
        }

        return $driverArray;
    }

    public function getDriversForAsignRoute($driverIds, $station, $driverWithIncident)
    {
        $ignoredDrivers = array_merge($driverIds, $driverWithIncident);
        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin('d.user', 'u')
            ->leftJoin('d.stations', 's')
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere($qb->expr()->notIn('d.id', ':driverIds'))
            ->setParameter('driverIds', $ignoredDrivers)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->orderBy('u.friendlyName', 'ASC');
        if (!empty($station)) {
            $qb->andWhere('s.id = :station')
                ->setParameter('station', $station);
        }

        $results = $qb->getQuery()->getResult();
        return $results;
    }

    public function getDriverStations($criteria)
    {
        $qb = $this->createQueryBuilder('d');
        $qb->leftJoin('d.stations', 's')
            ->leftJoin('d.user', 'u')
            ->where('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('s.isArchive = :isStationArchive')
            ->setParameter('isStationArchive', false)
            ->andWhere('u.isArchive = :isArchiveUser')
            ->setParameter('isArchiveUser', false)
            ->andWhere('d.company = :companyId')
            ->setParameter('companyId', $criteria['company'])
            ->andWhere('s.id = :stationId')
            ->setParameter('stationId', $criteria['station']);

        if ($criteria['isGetDriver'] == false) {
            $qb->select('d.id as did,s.id,s.name');
        }
        $results = $qb->getQuery()->getResult();

        if ($criteria['isGetDriver'] == true) {
            return $results;
        }
        $station = [];
        foreach ($results as $result) {
            $station[$result['did']][] = ['id' => $result['id'], 'name' => $result['name']];
        }

        return $station;
    }

    public function getTodaysDriverCounts($station = null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $query = $this->createQueryBuilder('d')
            ->select('COUNT(DISTINCT d.id) as drivers')
            ->leftJoin('d.driverRoutes', 'dr')
            ->andWhere('dr.isArchive = :driverRouteArchive')
            ->setParameter('driverRouteArchive', false)
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $currentDate);
        if ($station) {
            $query->andWhere('dr.station = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getOneOrNullResult();
        if ($results) {
            $results = $results['drivers'];
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getTodaysDriverCountsNew($station = null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $query = $this->createQueryBuilder('d')
            ->select('COUNT(DISTINCT d.id) as drivers')
            ->leftJoin('d.driverRoutes', 'dr')
            ->andWhere('dr.isArchive = :driverRouteArchive')
            ->setParameter('driverRouteArchive', false)
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $currentDate);
        if ($station) {
            $query->andWhere('dr.station = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getOneOrNullResult();
        if ($results) {
            $results = $results['drivers'];
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getTodaysDriverWithRouteCodeCounts($station = null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $query = $this->createQueryBuilder('d')
            ->select('COUNT(DISTINCT d.id) as drivers')
            ->leftJoin('d.driverRoutes', 'dr')
            ->leftJoin('dr.driverRouteCodes', 'rc')
            ->where('rc.id IS NOT NULL')
            ->andWhere('dr.isArchive = :driverRouteArchive')
            ->setParameter('driverRouteArchive', false)
            ->andWhere('rc.isArchive = :routeCodeArchive')
            ->setParameter('routeCodeArchive', false)
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $currentDate);
        if ($station) {
            $query->andWhere('dr.station = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getOneOrNullResult();
        if ($results) {
            $results = $results['drivers'];
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getTodaysDriverWithRouteCodeCountsNew($station = null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $query = $this->createQueryBuilder('d')
            ->select('COUNT(DISTINCT d.id) as drivers')
            ->leftJoin('d.driverRoutes', 'dr')
            ->leftJoin('dr.driverRouteCodes', 'rc')
            ->where('rc.id IS NOT NULL')
            ->andWhere('dr.isArchive = :driverRouteArchive')
            ->setParameter('driverRouteArchive', false)
            ->andWhere('rc.isArchive = :routeCodeArchive')
            ->setParameter('routeCodeArchive', false)
            ->andWhere('d.isArchive = :isArchive')
            ->setParameter('isArchive', false)
            ->andWhere('d.isEnabled = :isEnabled')
            ->setParameter('isEnabled', 1)
            ->andWhere('dr.dateCreated = :date')
            ->setParameter('date', $currentDate);
        if ($station) {
            $query->andWhere('dr.station = :station')
                ->setParameter('station', $station);
        }
        $results = $query->getQuery()->getOneOrNullResult();
        if ($results) {
            $results = $results['drivers'];
        } else {
            $results = 0;
        }
        return (int)$results;
    }

    public function getTwilioUsersV1(array $criteria)
    {
        $qb = $this->createQueryBuilder('d');

        $qb
            ->leftJoin('d.user', 'u')
            ->where('d.isArchive = :isDriverArchive')
            ->setParameter('isDriverArchive', false)
            ->andWhere('u.isArchive = :isUserArchive')
            ->setParameter('isUserArchive', false)
            ->andWhere('u.chatIdentifier IS NOT NULL')
            ->andWhere('u.company = :company')
            ->setParameter('company', $criteria['company']);
        return $qb->getQuery()->getResult();
    }

    public function getListOfDrivers(array $criteria)
    {
        if (empty($criteria['companyId'])) {
            return ['success' => false, 'message' => 'companyId Not found'];
        }
        $drivers = $this->findBy(['company' => $criteria['companyId'], 'isArchive' => false, 'isEnabled' => 1], ['jobTitle' => 'DESC']);

        if (is_array($drivers) && sizeof($drivers) > 0) {

            $drivers = array_map(function (Driver $driver) {
                $user = $driver->getUser();
                $fullName = $user->getFirstName() . ' ' . $user->getLastName();
                return [
                    'driverId' => $driver->getId(),
                    'userId' => $user->getId(),
                    'jobTitle' => $driver->getJobTitle(),
                    'fullName' => $fullName,
                    'friendlyName' => $user->getFriendlyName(),
                ];
            }, $drivers);

            return [
                'success' => true,
                'message' => 'Drivers found',
                'count' => sizeof($drivers),
                'drivers' => $drivers,
            ];
        }

        return ['success' => false, 'message' => 'Drivers not found'];

    }

    public function loginUser(array $criteria)
    {
        $qb = $this->createQueryBuilder('d')
            ->leftJoin('d.company', 'c')
            ->leftJoin('d.user', 'u')
            ->leftJoin('u.userRoles', 'ur')
            ->leftJoin('d.stations', 's')
            ->addSelect('u.id AS userId, u.isArchive, u.isEnabled, u.chatIdentifier, u.friendlyName, u.email, u.profileImage, u.isArchive, u.isResetPassword')
            ->addSelect('c.id AS companyId')
            ->addSelect('c.name AS companyName')
            ->addSelect('d.id AS driverId')
            ->addSelect('ur.name AS role')
            ->addSelect('s.name AS StationName')
            ->addSelect('s.id AS StationId')
            ->addSelect('s.code AS StationCode')
            ->andWhere('u.id = :id')
            ->setParameter('id', $criteria['userId']);
        return $qb->getQuery()->getScalarResult();
    }

    public function getScheduleDriverFromScheduleId($id)
    {
        $query = $this->createQueryBuilder('d')
            ->leftJoin('d.schedules','s')
            ->leftJoin('d.user','u')
            ->leftJoin('d.driverSkills','ds')
            ->leftJoin('u.userRoles','ur')
            ->leftJoin('ds.skill','skill')
            ->addSelect('u')
            ->addSelect('ur')
            ->addSelect('ds')
            ->addSelect('skill')
            ->where('s.id = :scheduleId')
            ->setParameter('scheduleId', $id)
            ->andWhere('d.isArchive = :driverIsArchive')
            ->setParameter('driverIsArchive',false)
            ->andWhere('u.isArchive = :userIsArchive')
            ->setParameter('userIsArchive',false)
            ->andWhere('ds.isArchive = :driverSkillIsArchive')
            ->setParameter('driverSkillIsArchive',false)
            ->andWhere('ur.isArchive = :useRoleIsArchive')
            ->setParameter('useRoleIsArchive',false)
            ->andWhere('skill.isArchive = :skillIsArchive')
            ->setParameter('skillIsArchive',false);
        return $query->getQuery()->getArrayResult();
    }

    public function getDriversWithSchedules($ids)
    {
        $query = $this->createQueryBuilder('d')
            ->leftJoin('d.schedules','s')
            ->addSelect('s')
            ->where('d.id in (:scheduleId)')
            ->setParameter('scheduleId', $ids)
            ->andWhere('d.isArchive = :driverIsArchive')
            ->setParameter('driverIsArchive',false)
            ->andWhere('s.isArchive = :scheduleIsArchive')
            ->setParameter('scheduleIsArchive',false);
        return $query->getQuery()->getArrayResult();
    }


    public function updateDriverStatus(array $criteria)
    {
        $query = $this->createQueryBuilder('d')
            ->leftJoin('d.company', 'c')
            ->leftJoin('d.stations', 's')
            ->where('c.isArchive = :isCompanyArchive')
            ->setParameter('isCompanyArchive', false)
            ->andWhere('s.isArchive = :isStationArchive')
            ->setParameter('isStationArchive', false);
        if($criteria['status'] == 'isArchive'){
            $query->andWhere('d.isArchive = :isDriverArchive')
                   ->setParameter('isDriverArchive', true);
        } else {
            $query->andWhere('d.isEnabled = :isDriverEnabled')
                  ->setParameter('isDriverEnabled', false);
        }
        $results =  $query->getQuery()->getResult();
        $dateObj = (new DateTime())->setTimezone(new \DateTimeZone('UTC'));

        foreach($results as $result){
            if ($criteria['status'] == 'isArchive') {
                if( $result->getUpdatedAt() ){
                    $dateObj = $result->getUpdatedAt();
                    $result->setEmployeeStatus('-1'); // status Offboarded
                    $result->setOffboardedDate($dateObj);
                } else {
                    $result->setEmployeeStatus('-2'); // status Terminated
                    $result->setOffboardedDate($dateObj);
                }

            } else {
                $result->setEmployeeStatus('0'); // status Inactive
            }
            $result->setIsArchive(false);
            $result->setIsEnabled(true);
            $this->_em->persist($result);
            $this->_em->flush($result);

            $user = $result->getUser();
            $user->setIsArchive(false);
            $user->setIsEnabled(true);
            $this->_em->persist($user);
            $this->_em->flush($user);
        }
        return true;
    }
}
