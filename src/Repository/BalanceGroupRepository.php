<?php

namespace App\Repository;

use App\Entity\BalanceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Doctrine\ORM\Query;

/**
 * @method BalanceGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method BalanceGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method BalanceGroup[]    findAll()
 * @method BalanceGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BalanceGroupRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $em;
    private $kernel;
    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage, EntityManagerInterface $em, KernelInterface $kernel)
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, BalanceGroup::class);
        $this->em = $em;
        $this->kernel = $kernel;
    }

    public function getBalanceGroupByStation($stationId)
    {
        $results = $this->createQueryBuilder('bg')
                         ->select('bg.id')
                         ->andWhere('bg.station = :station')
                         ->setParameter('station', $stationId )
                         ->andWhere('bg.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->getQuery()
                         ->getResult(Query::HYDRATE_ARRAY);

        return array_column($results, 'id');
    }

    public function getShiftBalanceGroup($criteria)
    {
        $query = $this->createQueryBuilder('bg')
                         ->leftJoin('bg.shifts','s')
                         ->andWhere('bg.isArchive = :isArchive')
                         ->setParameter('isArchive',false)
                         ->andWhere('s.balanceGroup IS NOT NULL')
                         ->andWhere('bg.company = :companyId')
                         ->setParameter('companyId',$criteria['company'])
                         ->groupBy('bg.id');

         if ($criteria['station'] != '-1') {
             $query->andWhere('bg.station = :station')
                 ->setParameter('station', $criteria['station']);
         }

        $results = $query->getQuery()->getResult();
        return $results;
    }

    public function updateStationIsArchiveInBalanceGroup($stationId)
    {

        $bgIds = $this->getBalanceGroupByStation($stationId);
        if (!empty($bgIds)) {
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->update('App\Entity\BalanceGroup','bg')
                            ->set('bg.isArchive', true)
                            ->where($qb->expr()->in('bg.id', ':bgIds'))
                            ->setParameter( 'bgIds', $bgIds )
                            ->getQuery()
                            ->execute();

            //Call shift isArchive true set
            $this->getEntityManager()->getRepository('App\Entity\Shift')->updateBalanceGroupNullInShift($bgIds);
        }

        return true;
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function checkIfExistBalanceGroup($criteria)
    {
      $userLogin = $this->tokenStorage->getToken()->getUser();
      if(!empty($criteria['name'])) {
        $balanceGroup = $this->em->getRepository('App\Entity\BalanceGroup')->findOneBy(['name'=>$criteria['name'],'station'=>$criteria['station'],'company'=>$userLogin->getCompany()]);
        if(!$balanceGroup){
          $balanceGroup = new BalanceGroup();
          $balanceGroup->setCompany($userLogin->getCompany());
          $balanceGroup->setName($criteria['name']);
          $balanceGroup->setStation($this->em->getRepository('App\Entity\Station')->find($criteria['station']));
          $this->em->persist($balanceGroup);
          $this->em->flush();
        }
        return $balanceGroup->getId();
      }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    /*public function getImpliedBalance($criteria)
    {
        $company = $criteria['company'];
        $weekStartEndDay = $this->getStartAndEndDate($criteria['week']);

        $response=[];
        $data = $this->createQueryBuilder('bg')
                      ->select('bg.id as id','st.id as stationId','st.name as stationName','COUNT(dr.id) as routeCount','COUNT(d.id) as driverCount','bg.name as name','dr.dateCreated')
                      ->leftJoin('bg.shifts','s')
                      ->leftJoin('bg.station','st')
                      ->leftJoin('s.driverRoutes','dr')
                      ->leftJoin('dr.driver','d')
                      ->where('dr.id IS NOT NULL')
                      ->andWhere('bg.isArchive = :isArchive')
                      ->andWhere('dr.driver IS NOT NULL')
                      ->andWhere('bg.company = :company')
                      ->setParameter('isArchive', false)
                      ->andWhere('dr.dateCreated BETWEEN :date1 AND :date2')
                      ->setParameter('company', $company)
                      ->setParameter('date1', $weekStartEndDay['w0'])
                      ->setParameter('date2', $weekStartEndDay['w6'])
                      ->groupBy('bg.id','dr.dateCreated')
                      ->getQuery()
                      ->getResult();
        $footerData = [
                        'w0' => array('driver' => 0, 'routes' => 0),
                        'w1' => array('driver' => 0, 'routes' => 0),
                        'w2' => array('driver' => 0, 'routes' => 0),
                        'w3' => array('driver' => 0, 'routes' => 0),
                        'w4' => array('driver' => 0, 'routes' => 0),
                        'w5' => array('driver' => 0, 'routes' => 0),
                        'w6' => array('driver' => 0, 'routes' => 0),
                      ];
        $newData = [];
        $stationData = [];
        foreach ($data as $key => $value) {
          $tempCheck = array_search($value['id'], array_column($newData, 'id'));
          if($tempCheck === false){
          $stationCheck = array_search($value['stationId'], array_column($stationData, 'value'));
          if($stationCheck === false){
          $stationData[] = ['value'=>$value['stationId'],'name'=>$value['stationName']];
          }
          $newData[]=[
            'id' => $value['id'],
            'balanceGroup' => $value['name'],
            'station' => $value['stationId'],
            'w0' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w0'])? $footerData['w0'] = array('driver' => $footerData['w0']['driver']+$value['driverCount'], 'routes' => $footerData['w0']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w1' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w1'])? $footerData['w1'] = array('driver' => $footerData['w1']['driver']+$value['driverCount'], 'routes' => $footerData['w1']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w2' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w2'])? $footerData['w2'] = array('driver' => $footerData['w2']['driver']+$value['driverCount'], 'routes' => $footerData['w2']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w3' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w3'])? $footerData['w3'] = array('driver' => $footerData['w3']['driver']+$value['driverCount'], 'routes' => $footerData['w3']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w4' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w4'])? $footerData['w4'] = array('driver' => $footerData['w4']['driver']+$value['driverCount'], 'routes' => $footerData['w4']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w5' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w5'])? $footerData['w5'] = array('driver' => $footerData['w5']['driver']+$value['driverCount'], 'routes' => $footerData['w5']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
            'w6' => ($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w6'])? $footerData['w6'] = array('driver' => $footerData['w6']['driver']+$value['driverCount'], 'routes' => $footerData['w6']['routes']+$value['routeCount']):array('driver' => 0, 'routes' => 0),
          ];
          } else {
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w0']){
            $newData[$tempCheck]['w0'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w0'] = array('driver' => $footerData['w0']['driver']+$value['driverCount'], 'routes' => $footerData['w0']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w1']){
            $newData[$tempCheck]['w1'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w1'] = array('driver' => $footerData['w1']['driver']+$value['driverCount'], 'routes' => $footerData['w1']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w2']){
            $newData[$tempCheck]['w2'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w2'] = array('driver' => $footerData['w2']['driver']+$value['driverCount'], 'routes' => $footerData['w2']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w3']){
            $newData[$tempCheck]['w3'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w3'] = array('driver' => $footerData['w3']['driver']+$value['driverCount'], 'routes' => $footerData['w3']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w4']){
            $newData[$tempCheck]['w4'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w4'] = array('driver' => $footerData['w4']['driver']+$value['driverCount'], 'routes' => $footerData['w4']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w5']){
            $newData[$tempCheck]['w5'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w5'] = array('driver' => $footerData['w5']['driver']+$value['driverCount'], 'routes' => $footerData['w5']['routes']+$value['routeCount']);
            }
            if($value['dateCreated']->format('Y-m-d') == $weekStartEndDay['w6']){
            $newData[$tempCheck]['w6'] = array('driver' => $value['driverCount'], 'routes' => $value['routeCount']);
            $footerData['w6'] = array('driver' => $footerData['w6']['driver']+$value['driverCount'], 'routes' => $footerData['w6']['routes']+$value['routeCount']);
            }
            $tempCheck = 0;
            }
        }

        $response['mainData']=$newData;
        $response['footerData']=$footerData;
        $response['stationData']=$stationData;

        return $response;
    }*/

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getImpliedBalanceBySchedule($criteria)
    {
        $company = $criteria['company'];
        $weekDate = $criteria['weekDate'];
        $balanceId = $criteria['id'];
        // $weekStartEndDay = $this->getStartAndEndDate($criteria['week']);

        $response=[];
        $data = $this->createQueryBuilder('bg')
                      ->select('bg.id as id','schedule.name as shiftName','COUNT(d.id) as count','sk.id as skillId','sk.name as skillName')
                      ->leftJoin('bg.shifts','s')
                      ->leftJoin('s.driverRoutes','dr')
                      ->leftJoin('dr.schedule','schedule')
                      ->leftJoin('dr.driver','d')
                      ->leftJoin('dr.skill','sk')
                      ->andWhere('dr.dateCreated = :date')
                      ->andWhere('bg.id = :id')
                      ->andWhere('bg.isArchive = :isArchive')
                      ->andWhere('dr.schedule IS NOT NULL')
                      ->setParameter('date', $weekDate)
                      ->setParameter('id', $balanceId)
                      ->setParameter('isArchive', false)
                      ->groupBy('bg.id','sk.id','schedule.name')
                      ->getQuery()
                      ->getResult();

        $newData = [];
        $shiftData = [];
        foreach ($data as $key => $value) {
            if(array_key_exists($value['skillName'], $newData) && $value['skillName'] != null){
              $newData[$value['skillName']][$value['shiftName']] = $value['count'];
              if(!in_array($value['shiftName'], $shiftData)){
                $shiftData[] = $value['shiftName'];
              }
            }else {
              $newData[$value['skillName']][$value['shiftName']] = $value['count'];
              if(!in_array($value['shiftName'], $shiftData)){
                $shiftData[] = $value['shiftName'];
              }
            }
        }

        $newFormatedData = [];
        foreach ($newData as $key => $value) {
          $tempArray  = [];
          $tempArray['skills']= array($key);
          foreach ($shiftData as $shiftKey => $shiftValue) {
            if(array_key_exists($shiftValue, $value)){
              $tempArray['s'.$shiftKey]= $value[$shiftValue];
            }else {
              $tempArray['s'.$shiftKey]= '';
            }
          }
          $newFormatedData[] = $tempArray;
        }

        $response['schedules']=$newFormatedData;
        $response['shifts']=$shiftData;

        return $response;
    }

    public function updatedBalanceGroup($criteria)
    {
        $balanceId = $criteria['id'];

        $this->getEntityManager()->getRepository('App\Entity\Shift')->updateBalanceGroupNullInShift($balanceId);
        $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->updateBalanceGroupNullInDriverRoute($balanceId);
        $this->getEntityManager()->getRepository('App\Entity\TempDriverRoute')->updateBalanceGroupNullInTempDriverRoute($balanceId);

        return true;
    }

    public function getBalanceGroupImportData($station, $company, $name)
    {
        $em = $this->getEntityManager();

        $balancegroup = new BalanceGroup();
        $balancegroup->setStation($station);
        $balancegroup->setCompany($company);
        $balancegroup->setName($name);
        $em->persist($balancegroup);
        $em->flush();

        return $balancegroup;
    }
}
