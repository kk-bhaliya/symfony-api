<?php

namespace App\Repository;

use App\Criteria\Common;
use App\Entity\SkillRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SkillRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method SkillRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method SkillRate[]    findAll()
 * @method SkillRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SkillRateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SkillRate::class);
    }

    public function getSkillRateByStation($criteria)
    {
        return $this->createQueryBuilder('sr')
                    ->leftJoin('sr.skill','s')
                    ->where('sr.station = :station')
                    ->setParameter('station', $criteria['station'] )
                    ->addCriteria(Common::notArchived('sr'))
                    ->addCriteria(Common::notArchived('s'))
                    ->getQuery()
                    ->getResult();
    }

    public function updateSkillNullInSkillRate($skillId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\SkillRate','sk')
                        ->set('sk.skill', 'NULL')
                        ->where('sk.skill = :skillId')
                        ->setParameter('skillId', $skillId)
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function updateStationIsArchiveInSkillRate($stationId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\SkillRate','sk')
                        ->set('sk.isArchive', true)
                        ->where('sk.station = :station')
                        ->setParameter('station', $stationId)
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function getSkillRateImportData($station, $skill, $defaultRate)
    {
        $em = $this->getEntityManager();

        $skillRate = new SkillRate();
        $skillRate->setStation($station);
        $skillRate->setSkill($skill);
        $skillRate->setDefaultRate($defaultRate);
        $em->persist($skillRate);
        $em->flush();

        return $skillRate;
    }
}
