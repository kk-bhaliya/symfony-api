<?php

namespace App\Repository;

use App\Entity\RouteCommitment;
use App\Entity\Shift;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use DateTime;

/**
 * @method RouteCommitment|null find($id, $lockMode = null, $lockVersion = null)
 * @method RouteCommitment|null findOneBy(array $criteria, array $orderBy = null)
 * @method RouteCommitment[]    findAll()
 * @method RouteCommitment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouteCommitmentRepository extends ServiceEntityRepository
{
    private $kernel;
    public function __construct(ManagerRegistry $registry, KernelInterface $kernel)
    {
        parent::__construct($registry, RouteCommitment::class);
        $this->kernel = $kernel;
    }

    public function updateShiftIsArchiveInRouteCommitment($shiftId)
    {
        $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $today->setTime(23,59,59);
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\RouteCommitment','rc')
                        ->set('rc.isArchive', true)
                        ->where('rc.shiftType = :shiftId')
                        ->setParameter('shiftId', $shiftId)
                        ->andWhere('rc.weekDate > :today')
                        ->setParameter('today',$today)
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function updateStationIsArchiveInRouteCommitment($stationId)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\RouteCommitment','rc')
                        ->set('rc.isArchive', true)
                        ->where('rc.station = :station')
                        ->setParameter('station', $stationId)
                        ->getQuery()
                        ->execute();
        return true;
    }

    public function updateRouteCommitmentSetIsArchiveByShift($shifts)
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->update('App\Entity\RouteCommitment','rc')
                        ->set('rc.isArchive', true)
                        ->where($qb->expr()->in('rc.shiftType', ':shifts'))
                        ->setParameter('shifts', $shifts )
                        ->getQuery()
                        ->execute();
        return true;
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function getRouteCommitments($criteria)
    {
      $tempArray = [];
      $routeCommitmentsCurrentQuery =  $this->createQueryBuilder('rc')
          ->innerJoin('rc.shiftType','sf')
          ->where('rc.week = :week')
          ->andWhere('rc.company = :company')
          ->andWhere('rc.year = :year')
          ->andWhere('rc.isArchive = :isArchive')
          ->andWhere('sf.balanceGroup IS NOT NULL')
          ->setParameter('week', $criteria['week'])
          ->setParameter('company', $criteria['company'])
          ->setParameter('year', $criteria['year'])
          ->setParameter('isArchive', false);

        if ($criteria['station'] != '-1') {
            $routeCommitmentsCurrentQuery->andWhere('rc.station = :station')
                ->setParameter('station', $criteria['station']);
        }
        $routeCommitmentsCurrent = $routeCommitmentsCurrentQuery->getQuery()->getResult();

        $dto = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dto->setISODate($criteria['year'], $criteria['week']);
        $dto->modify('-7 days');

      $routeCommitmentsPreviousQuery =  $this->createQueryBuilder('rc')
          ->innerJoin('rc.shiftType','sf')
          ->where('rc.week = :week')
          ->andWhere('rc.company = :company')
          ->andWhere('rc.year = :year')
          ->andWhere('rc.isArchive = :isArchive')
          ->andWhere('sf.balanceGroup IS NOT NULL')
          ->setParameter('week',$dto->format('W'))
          ->setParameter('company', $criteria['company'])
          ->setParameter('year', $dto->format('Y'))
          ->setParameter('isArchive', false);

          if ($criteria['station'] != '-1') {
              $routeCommitmentsPreviousQuery->andWhere('rc.station = :station')
                  ->setParameter('station', $criteria['station']);
          }
          $routeCommitmentsPrevious = $routeCommitmentsPreviousQuery->getQuery()->getResult();

          $tempArray['current']=array_values($this->getRouteCommitmentsResult($routeCommitmentsCurrent));
          $tempArray['previous']=array_values($this->getRouteCommitmentsResult($routeCommitmentsPrevious));
          return $tempArray;
    }

    /**
     * @param $week
     * @param $comapny
     * @return mixed
     * @throws \Exception
     */
    public function getRouteCommitmentsByWeekNew($week, $comapny, $year = null, $station = null)
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }
        $query =  $this->createQueryBuilder('rc')
            ->select('rc.weekDate as rc_weekDate',
            'rc.route as rc_route',
            'sf.id as sf_id',
            'sf.color as sf_color',
            'sf.name as sf_name',
            'st.id as st_id',
            'st.name as st_name',
            'bg.id as bg_id',
            'bg.name as bg_name')
            ->innerJoin('rc.shiftType','sf')
            ->leftJoin('sf.balanceGroup','bg')
            ->leftJoin('bg.station','st')
            ->where('rc.week = :week')
            ->andWhere('rc.company = :company')
            ->andWhere('rc.year = :year')
            ->andWhere('rc.isArchive = :isArchive')
            ->andWhere('sf.balanceGroup IS NOT NULL')
            ->setParameter('week', $week)
            ->setParameter('company', $comapny)
            ->setParameter('year', $year)
            ->setParameter('isArchive', false);

        if ($station != null) {
            $query->andWhere('rc.station = :station')->setParameter('station', $station);
        }

        $routeCommitments = $query->orderBy('rc.weekDate','ASC')
            ->getQuery()
            ->getScalarResult();

        return $routeCommitments;
    }

    /**
     * @param $week
     * @param $comapny
     * @return mixed
     * @throws \Exception
     */
    public function getRouteCommitmentsByWeek($week, $comapny, $year = null, $station = null)
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }
        $query =  $this->createQueryBuilder('rc')
            ->select('rc.weekDate as rc_weekDate',
            'rc.route as rc_route',
            'sf.id as sf_id',
            'sf.color as sf_color',
            'sf.name as sf_name',
            'st.id as st_id',
            'st.name as st_name',
            'bg.id as bg_id',
            'bg.name as bg_name')
            ->innerJoin('rc.shiftType','sf')
            ->leftJoin('sf.balanceGroup','bg')
            ->leftJoin('bg.station','st')
            ->where('rc.week = :week')
            ->andWhere('rc.company = :company')
            ->andWhere('rc.year = :year')
            ->andWhere('rc.isArchive = :isArchive')
            ->andWhere('sf.balanceGroup IS NOT NULL')
            ->setParameter('week', $week)
            ->setParameter('company', $comapny)
            ->setParameter('year', $year)
            ->setParameter('isArchive', false);

        if ($station != null) {
            $query->andWhere('rc.station = :station')->setParameter('station', $station);
        }

        $routeCommitments = $query->orderBy('rc.weekDate','ASC')
            ->getQuery()
            ->getScalarResult();

        return $routeCommitments;
    }

    /**
     * @param $criteria
     * @return mixed
     * @throws \Exception
     */
    public function deleteRouteCommitments($criteria)
    {
      $this->createQueryBuilder('rc')
                      ->delete()
                      ->where('rc.week = :week')
                      ->andWhere('rc.company = :company')
                      ->andWhere('rc.year = :year')
                      ->andWhere('rc.isArchive = :isArchive')
                      ->andWhere('rc.station = :station')
                      ->setParameter('week', $criteria['week'])
                      ->setParameter('company', $criteria['company'])
                      ->setParameter('year', $criteria['year'])
                      ->setParameter('station', $criteria['station'])
                      ->setParameter('isArchive', false)
                      ->getQuery()->execute();
      return true;
    }
    public function getRouteCommitmentsResult($routeCommitments){
      $routeCommitmentsArray=[];
      foreach ($routeCommitments as $item) {
        $datetime = $item->getWeekDate()->format('Y-m-d H:i:s');
        $week = date('w',strtotime($datetime));
        $dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][$week] = $item->getRoute();

        $tempRouteOfDay = isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][$week]) ? ($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][$week] + $item->getRoute()) : $item->getRoute();

        $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][$week] = (int ) $tempRouteOfDay;

        $routeCommitmentsArray[$item->getShiftType()->getBalanceGroup()->getId()] = [
          'id' => $item->getShiftType()->getBalanceGroup()->getId(),
          'balanceGroup' => $item->getShiftType()->getBalanceGroup()->getName(),
          'station' => $item->getStation()->getId(),
          'cId'=> $item->getId(),
          'w0' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][0]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][0] : 0,
          'w1' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][1]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][1] : 0,
          'w2' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][2]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][2] : 0,
          'w3' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][3]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][3] : 0,
          'w4' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][4]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][4] : 0,
          'w5' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][5]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][5] : 0,
          'w6' => isset($totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][6]) ? $totalRouteOfDay[$item->getShiftType()->getBalanceGroup()->getId()][6] : 0,
        ];

        $routeShiftTypeArray[$item->getShiftType()->getBalanceGroup()->getId()]['shifts'][$item->getShiftType()->getId()] = [
          'id' => $item->getShiftType()->getId(),
          'shift' => $item->getShiftType()->getName(),
          'isBackup' => ($item->getShiftType()->getCategory() == Shift::BACKUP) ? true : false,
          'startTime' => $item->getShiftType()->getStartTime(),
          'endTime' => $item->getShiftType()->getEndTime(),
          'textTime' => $item->getShiftType()->getStartTime()->format( 'h:ia' ).' - '.$item->getShiftType()->getEndTime()->format( 'h:ia' ),
          'bg' => $item->getShiftType()->getColor(),
          'cId'=> $item->getId(),
          'w0' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][0]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][0] : 0,
          'w1' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][1]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][1] : 0,
          'w2' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][2]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][2] : 0,
          'w3' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][3]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][3] : 0,
          'w4' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][4]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][4] : 0,
          'w5' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][5]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][5] : 0,
          'w6' => isset($dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][6]) ? (int)$dayOfWeek[$item->getShiftType()->getBalanceGroup()->getId()][$item->getShiftType()->getId()][6] : 0,
        ];
        $routeCommitmentsArray[$item->getShiftType()->getBalanceGroup()->getId()]['shifts'] = array_values($routeShiftTypeArray[$item->getShiftType()->getBalanceGroup()->getId()]['shifts']);
      }
      return $routeCommitmentsArray;
    }

    public function getRouteCommitmentCount($week, $year, $company)
    {
        $results = $this->createQueryBuilder('rc')
                        ->where('rc.isArchive = :isArchive')
                        ->setParameter('isArchive', false);

        if (!empty($company))
        {
            $results->andWhere('rc.company = :company')
                    ->setParameter('company', $company);
        }

        if (!empty($week) and !empty($year))
        {
            $results->andWhere('rc.week = :week')
                    ->andWhere('rc.year = :year')
                    ->setParameter('week', $week)
                    ->setParameter('year', $year);
        } else {
            $date = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $day = date('w');
            $week_start = date('Y-m-d', strtotime('-'.$day.' days'));

            $results->andWhere('rc.weekDate >= :now')
                    ->setParameter('now', $week_start);
        }
        return $results->getQuery()->getResult();
    }

    public function commandForRouteCommitment($criteria){
        $application = new Application($this->kernel);
        $application->setAutoExit(false);
        $input = new ArrayInput(['command' => 'app:route-commitment','company' => $criteria['company'], 'week' => $criteria['week'], 'year' => $criteria['year']]);
        $output = new BufferedOutput();
        $application->run($input, $output);
        return true;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getImpliedBalance($criteria)
    {
        $company = $criteria['company'];
        $weekStartEndDay = $this->getStartAndEndDate($criteria['week'], (int) $criteria['year']);
        $driverRoute = $this->getEntityManager()->getRepository('App\Entity\DriverRoute')->getPublishedScheduleOfDriverRoutes($weekStartEndDay, $company);
        $routeCommitments = $this->getRouteCommitmentsByWeek($criteria['week'], $company, (int) $criteria['year']);

        $response = $stationData = $routeCommitmentsData = $dateWiseCommitedRoute = $totalRouteByBalanceGroup = $footerData = [];

        foreach($routeCommitments as $routeCommitment) {
            $stationData[$routeCommitment['st_id']] = ['value'=> $routeCommitment['st_id'],'name'=> $routeCommitment['st_name']];

            $dayNumber = date('w',strtotime($routeCommitment['rc_weekDate']));

            $dateWiseCommitedRoute[$routeCommitment['bg_id']]['w'.$dayNumber] = isset($dateWiseCommitedRoute[$routeCommitment['bg_id']]['w'.$dayNumber]) ? ((int) $dateWiseCommitedRoute[$routeCommitment['bg_id']]['w'.$dayNumber] + (int) $routeCommitment['rc_route']) : (int) $routeCommitment['rc_route'];

            $routeCommitmentsData[$routeCommitment['bg_id']]['id'] = (int)$routeCommitment['bg_id'];
            $routeCommitmentsData[$routeCommitment['bg_id']]['balanceGroup'] = $routeCommitment['bg_name'];
            $routeCommitmentsData[$routeCommitment['bg_id']]['station'] = (int)$routeCommitment['st_id'];

            $routeCommitmentsData[$routeCommitment['bg_id']]['w'.$dayNumber]['driver'] = 0;

            $routeCommitmentsData[$routeCommitment['bg_id']]['w'.$dayNumber]['routes'] = $dateWiseCommitedRoute[$routeCommitment['bg_id']]['w'.$dayNumber];

            $footerData['w'.$dayNumber]['driver'] = 0;
            $footerData['w'.$dayNumber]['routes'] = isset($footerData['w'.$dayNumber]['routes']) ? ((int) $footerData['w'.$dayNumber]['routes'] + (int) $routeCommitment['rc_route']) : (int) $routeCommitment['rc_route'];

        }

        foreach($driverRoute as $driverRoute) {

            if(isset($driverRoute['bg_id']) && count($routeCommitmentsData) > 0) {

                $dayNumber = date('w',strtotime($driverRoute['dr_dateCreated']->format('Y-m-d H:i:s')));

                $dateWiseDriverRoute[$driverRoute['bg_id']]['w'.$dayNumber] = isset($dateWiseDriverRoute[$driverRoute['bg_id']]['w'.$dayNumber]) ? ((int) $dateWiseDriverRoute[$driverRoute['bg_id']]['w'.$dayNumber] + 1) : 1;

                $routeCommitmentsData[$driverRoute['bg_id']]['w'.$dayNumber]['driver'] = $dateWiseDriverRoute[$driverRoute['bg_id']]['w'.$dayNumber];

                $routeCommitmentsData[$driverRoute['bg_id']]['w'.$dayNumber]['routes'] = isset($routeCommitmentsData[$driverRoute['bg_id']]['w'.$dayNumber]['routes']) ? $routeCommitmentsData[$driverRoute['bg_id']]['w'.$dayNumber]['routes'] : 0;
            }
        }

        $stationData = array_values($stationData);
        $routeCommitmentsData = array_values($routeCommitmentsData);

        $response['mainData']=$routeCommitmentsData;
        $response['footerData']=$footerData;
        $response['stationData']=$stationData;

        return $response;
    }

    public function getStartAndEndDate($week, $year = null, $format = 'Y-m-d')
    {
        if (!is_int($year)) {
            $year = (new DateTime)->format('Y');
        }

        $dto = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $dto->setISODate($year, $week);
        $dto->modify('-1 days');
        $result['w0'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w1'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w2'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w3'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w4'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w5'] = $dto->format($format);
        $dto->modify('+1 days');
        $result['w6'] = $dto->format($format);

        return $result;
    }
    /**
     * @param $week
     * @param $comapny
     * @return mixed
     * @throws \Exception
     */
    public function getRouteCommitmentsByDate($id)
    {
        $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currentDateObj->setTime(0, 0, 0);
        $currentDate = $currentDateObj->format('Y-m-d');

        $routeCommitments =  $this->createQueryBuilder('rc')
            ->select('SUM(rc.route) as routeCodeSum')
            ->leftJoin('rc.shiftType','sf')
            ->leftJoin('sf.balanceGroup','bg')
            ->where('bg.id = :bgId')
            ->andWhere('rc.weekDate = :weekDate')
            ->andWhere('rc.isArchive = :isArchive')
            ->andWhere('sf.balanceGroup IS NOT NULL')
            ->setParameter('bgId', $id)
            ->setParameter('weekDate', $currentDateObj)
            ->setParameter('isArchive', false)
            ->groupBy('bg.id')
            ->getQuery()
            ->getOneOrNullResult();

        if($routeCommitments) {
            $routeCommitments = $routeCommitments['routeCodeSum'];
        }

        return $routeCommitments;
    }

    /**
     * @param $station
     * @param $shift
     * @return mixed
     * @throws \Exception
     */
    public function getTodayRouteCommitmentCounts($station=null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $routeCommitments =  $this->createQueryBuilder('rc')
            ->select('SUM(rc.route) as routeCodeSum')
            ->where('rc.weekDate = :routeDate')
            ->andWhere('rc.isArchive = :isArchive')
            ->setParameter('routeDate', $currentDate)
            ->setParameter('isArchive', false)
            ->groupBy('rc.weekDate');
            if($station){
                $routeCommitments->andWhere('rc.station = :station')
                ->setParameter('station', $station);
            }
            $routeCommitments = $routeCommitments->getQuery()->getOneOrNullResult();

        if($routeCommitments) {
            $routeCommitments = $routeCommitments['routeCodeSum'];
        } else {
            $routeCommitments=0;
        }
        return (int) $routeCommitments;
    }

    /**
     * @param $station
     * @param $shift
     * @return mixed
     * @throws \Exception
     */
    public function getTodayRouteCommitmentCountsNew($station=null, $currentDate)
    {
        // $currentDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        // $currentDateObj->setTime(0, 0, 0);
        // $currentDate = $currentDateObj->format('Y-m-d');

        $routeCommitments =  $this->createQueryBuilder('rc')
            ->select('SUM(rc.route) as routeCodeSum')
            ->where('rc.weekDate = :routeDate')
            ->andWhere('rc.isArchive = :isArchive')
            ->setParameter('routeDate', $currentDate)
            ->setParameter('isArchive', false)
            ->groupBy('rc.weekDate');
            if($station){
                $routeCommitments->andWhere('rc.station = :station')
                ->setParameter('station', $station);
            }
            $routeCommitments = $routeCommitments->getQuery()->getOneOrNullResult();

        if($routeCommitments) {
            $routeCommitments = $routeCommitments['routeCodeSum'];
        } else {
            $routeCommitments=0;
        }
        return (int) $routeCommitments;
    }


}
