<?php

namespace App\Repository;

use App\Entity\IncidentStep;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method IncidentStep|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentStep|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentStep[]    findAll()
 * @method IncidentStep[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentStepRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, IncidentStep::class);
    }
}
