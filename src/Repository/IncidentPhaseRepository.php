<?php

namespace App\Repository;

use App\Entity\Image;
use App\Entity\IncidentFile;
use App\Entity\IncidentPhase;
use App\Entity\IncidentFormFieldValue;
use App\Entity\IncidentSubmitedPhase;
use App\Utils\GlobalUtility;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @method IncidentPhase|null find($id, $lockMode = null, $lockVersion = null)
 * @method IncidentPhase|null findOneBy(array $criteria, array $orderBy = null)
 * @method IncidentPhase[]    findAll()
 * @method IncidentPhase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncidentPhaseRepository extends ServiceEntityRepository
{
    protected $tokenStorage;
    private $globalUtils;
    private $params;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage, GlobalUtility $globalUtils, ContainerBagInterface $params)
    {
        parent::__construct($registry, IncidentPhase::class);
        $this->tokenStorage = $tokenStorage;
        $this->globalUtils = $globalUtils;
        $this->params = $params;
    }

    public function checkRolesExist($role) {
        $treeRole['ROLE_OWNER']=['ROLE_OWNER'];
        $treeRole['ROLE_OPERATIONS_ACCOUNT_MANAGER']=['ROLE_OWNER','ROLE_OPERATIONS_ACCOUNT_MANAGER'];
        $treeRole['ROLE_CENTRAL_DISPATCHER']=['ROLE_OWNER', 'ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_CENTRAL_DISPATCHER'];
        $treeRole['ROLE_HR_ADMINISTRATOR']=['ROLE_OWNER','ROLE_HR_ADMINISTRATOR'];
        $treeRole['ROLE_OPERATIONS_MANAGER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER'];
        $treeRole['ROLE_FLEET_MANAGER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_FLEET_MANAGER'];
        $treeRole['ROLE_STATION_MANAGER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER'];
        $treeRole['ROLE_ASSISTANT_STATION_MANAGER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER'];
        $treeRole['ROLE_DISPATCHER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER','ROLE_DISPATCHER'];
        $treeRole['ROLE_LEAD_DRIVER']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER','ROLE_DISPATCHER','ROLE_LEAD_DRIVER'];
        $treeRole['ROLE_SAFETY_CHAMPION']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER','ROLE_DISPATCHER','ROLE_SAFETY_CHAMPION'];
        $treeRole['ROLE_DELIVERY_ASSOCIATE']=['ROLE_OWNER','ROLE_OPERATIONS_MANAGER','ROLE_OPERATIONS_ACCOUNT_MANAGER','ROLE_STATION_MANAGER','ROLE_ASSISTANT_STATION_MANAGER','ROLE_DISPATCHER','ROLE_DELIVERY_ASSOCIATE'];
        if (array_key_exists($role, $treeRole)) {
            $accessRoles = $treeRole[$role];
            $userRole = $this->tokenStorage->getToken()->getUser()->getUserRoles()->first()->getName();
            if (in_array($userRole,$accessRoles)) {
              return true;
            }
            return false;
        }
        return false;
    }

    public function getParentRole($role) {
        $treeRole['ROLE_OWNER'] = 'ROLE_OWNER';
        $treeRole['ROLE_OPERATIONS_ACCOUNT_MANAGER'] = 'ROLE_OWNER';
        $treeRole['ROLE_CENTRAL_DISPATCHER'] = 'ROLE_OPERATIONS_ACCOUNT_MANAGER';
        $treeRole['ROLE_HR_ADMINISTRATOR'] = 'ROLE_OWNER';
        $treeRole['ROLE_OPERATIONS_MANAGER'] = 'ROLE_OWNER';
        $treeRole['ROLE_FLEET_MANAGER'] = 'ROLE_OPERATIONS_MANAGER';
        $treeRole['ROLE_STATION_MANAGER'] = 'ROLE_OPERATIONS_MANAGER';
        $treeRole['ROLE_ASSISTANT_STATION_MANAGER'] = 'ROLE_STATION_MANAGER';
        $treeRole['ROLE_DISPATCHER'] = 'ROLE_ASSISTANT_STATION_MANAGER';
        $treeRole['ROLE_LEAD_DRIVER'] = 'ROLE_DISPATCHER';
        $treeRole['ROLE_SAFETY_CHAMPION'] = 'ROLE_DISPATCHER';
        $treeRole['ROLE_DELIVERY_ASSOCIATE'] = 'ROLE_DISPATCHER';

        return array_key_exists($role,$treeRole)?$treeRole[$role]:'';
    }

    public function getIncidentData($criteria)
    {
        $loginUser = $this->tokenStorage->getToken()->getUser();
        $companyId = $criteria['company'];
        $incidentId = $criteria['incidentId'];
        $requestTimeZone = isset($criteria['timezoneOffset']) ? $criteria['timezoneOffset'] : 0;

        $incident = $this->getEntityManager()->getRepository('App\Entity\Incident')->find($incidentId);
        $incidentCompletedPhase = $incident->getIncidentCompletedPhase();
        $incidentTypeId = ($incident->getIncidentType()->getIsArchive() == false) ? $incident->getIncidentType()->getId() : 0;

        $incidentData = [];
        $incidentData['id'] = $incident->getId();
        $incidentDriver = [];
        $incidentDriver['id'] = ($incident->getDriver()->getIsArchive() == false) ? $incident->getDriver()->getId() : '';
        $incidentDriver['name'] = ($incident->getDriver()->getIsArchive() == false) ? $incident->getDriver()->getFullName() : '';
        $incidentDriver['title'] = ($incident->getDriver()->getIsArchive() == false) ? $incident->getDriver()->getJobTitle() : '';
        $incidentData['driver'] = $incidentDriver;

        if ($incident->getDriverRoute()){
          $startTime = $this->globalUtils->getTimeZoneConversation($incident->getDriverRoute()->getStartTime(), $criteria['timezoneOffset']);
          $endTime = $this->globalUtils->getTimeZoneConversation($incident->getDriverRoute()->getEndTime(), $criteria['timezoneOffset']);
          $incidentData['driverRoute']=[];
          $incidentData['driverRoute']['type'] = $incident->getDriverRoute()->getShiftName();
          $incidentData['driverRoute']['color'] = $incident->getDriverRoute()->getShiftTextColor();
          $incidentData['driverRoute']['bg'] = $incident->getDriverRoute()->getShiftColor();
          $incidentData['driverRoute']['hour'] = $startTime->format($this->params->get('time_format')) . ' - ' . $endTime->format($this->params->get('time_format'));
        }

        $incidentStation = [];
        $incidentStation['id'] = ($incident->getStation()->getIsArchive() == false) ? $incident->getStation()->getId() : '';
        $incidentStation['name'] = ($incident->getStation()->getIsArchive() == false) ? $incident->getStation()->getName() : '';
        $incidentStation['code'] = ($incident->getStation()->getIsArchive() == false) ? $incident->getStation()->getCode() : '';
        $incidentData['station'] = $incidentStation;

        $incidentData['type'] = ($incident->getIncidentType()->getIsArchive() == false) ? $incident->getIncidentType()->getName() : '';
        $incidentData['title'] = $incident->getTitle();
        $incidentData['date'] = $incident->getDateTime()->format('Y-m-d');
        $incidentData['time'] = $this->globalUtils->getTimeZoneConversation($incident->getDateTime(), $requestTimeZone)->format('H:i:s');
        $incidentData['datetime'] = $incidentData['date'].' '.$incidentData['time'];

        //GetIncident All Value
        $incidentFormFieldsValue = [];
        $incidentFormValues = $incident->getIncidentFormFieldValues()->filter(function (IncidentFormFieldValue $item) {
                return $item->getIsArchive() === false;
            });
        foreach ($incidentFormValues as $incidentValues) {
            //get Image 21 image type
             if ($incidentValues->getIncidentFormField()->getFieldId() == 21) {
                $pathImage = "";
                foreach ($incidentValues->getIncidentFiles() as $incidentFiles) {
                  $pathImage = $incidentFiles->getImage()->getPath();
                }
                $incidentFormFieldsValue[$incidentValues->getIncidentFormField()->getId()] = $pathImage;
             } else {
                $incidentFormFieldsValue[$incidentValues->getIncidentFormField()->getId()] = (!empty($incidentValues->getValue()) && is_array($incidentValues->getValue())) ? $incidentValues->getValue()[0] : $incidentValues->getValue();
             }
        }

        $incidentPhases = $this->createQueryBuilder('ip')
            ->andWhere('ip.incidentType = :incidentType')
            ->setParameter('incidentType', $incidentTypeId)
            ->orderBy('ip.ordering', 'ASC')
            ->getQuery()
            ->getResult()
        ;
        $phaseData = [];

        $count = 0;
        $fieldOptions = [];
        $isCompleted = (count($incident->getIncidentSubmitedPhases()) == count($incident->getIncidentType()->getIncidentPhases()));
        foreach ($incidentPhases as $incidentPhase) {
            $fieldCount = 0;
            if($incidentPhase->getOnlyAccessToManagerOfDriver() == true){
              $assignManager = $incident->getDriver()->getAssignedManagers()->first();
              if($assignManager->getId() == $loginUser->getId()){
                $tempPhase['hasAccess'] = true;
              }else{
                $tempPhase['hasAccess'] = $this->checkRolesExist($this->getParentRole($assignManager->getUserFirstRole()));
              }
            }elseif(method_exists($incidentPhase->getRole(),'getName')){
              if($count == 0){
                if($incident->getCreatedBy()->getId() == $loginUser->getId()){
                  $tempPhase['hasAccess'] = true;
                }else{
                  $tempPhase['hasAccess'] = $this->checkRolesExist($this->getParentRole($incident->getCreatedBy()->getUserFirstRole()));
                }
              }else{
                $tempPhase['hasAccess'] = $this->checkRolesExist($incidentPhase->getRole()->getName());
              }
            }else{
              $tempPhase['hasAccess'] = false;
            }

            $tempPhase['hasReview'] = false;
            $tempPhase['review'] = $incidentPhase->getHasReview();
            $tempPhase['id']= $incidentPhase->getId();
            $tempPhase['title']= $incidentPhase->getTitle();
            $tempPhase['current'] = false;
            if($isCompleted && $count==0){
              $tempPhase['current'] = true;
            }else{
              if (method_exists($incident, 'getIncidentCurrentPhase') && $incident->getIncidentCurrentPhase()){
                if($incident->getIncidentCurrentPhase()->getId() == $incidentPhase->getId()){
                  $tempPhase['current'] = true;
                }else{
                  $tempPhase['current'] = false;
                }
              }else{
                $tempPhase['current'] = false;
              }
            }
            $tempPhase['submittedPhases'] = [];
            $tempPhase['reviewedPhases'] = [];
            foreach($incident->getIncidentSubmitedPhases() as $incidentSubmitedPhases) {
                $tempPhase['submittedPhases'][] = $incidentSubmitedPhases->getIncidentPhase()->getId();
                if(!empty($incidentSubmitedPhases->getReviewedBy())){
                  $tempPhase['reviewedPhases'][] = $incidentSubmitedPhases->getIncidentPhase()->getId();
                }
            }
            $tempPhase['isPhaseCompleted'] = in_array($incidentPhase->getId(), $tempPhase['submittedPhases']);
            $tempPhase['currentPhase'] = !empty($incident->getIncidentCurrentPhase()) ? $incident->getIncidentCurrentPhase()->getId() : 0;

            // steps
            $tempSteps = [];
            foreach ($incidentPhase->getIncidentSteps() as $step) {
                $tempStep = [];
                if($step->getisInWeb() == true && $step->getIsArchive() == false){
                    $tempStep['id'] = $step->getId();
                    $tempStep['title'] = $step->getTitle();
                    $tempStep['description'] = $step->getInstructions();

                    $tempFields = [];
                    foreach ($step->getIncidentFormFields() as $incidentFormField) {

                      $tempField = [];
                      if($incidentFormField->getisInWeb() == true && $incidentFormField->getIsArchive() == false){
                          $tempField['id'] = $incidentFormField->getId();
                          $tempField['type'] = ($incidentFormField->getFieldId() != '')?$incidentFormField->getFieldId():$incidentFormField->getIncidentQuestion()->getFieldId();


                          //props
                          $tempProps = [];
                          $tempTitle = ($incidentFormField->getLabel() != '')?$incidentFormField->getLabel():$incidentFormField->getIncidentQuestion()->getLabel();

                          $tempProps['name'] = $step->getId().'_'.$incidentFormField->getId();
                          $fieldRule = (count($incidentFormField->getFieldRule()))?$incidentFormField->getFieldRule():$incidentFormField->getIncidentQuestion()->getFieldRule();
                          $tempProps['validation'] = $fieldRule['validations'];
                          $tempField['dependOn'] = ($fieldRule['depend']['isDepend']) ? $fieldRule['depend']['dependOn'] : null;
                          $tempField['dependOnValue'] = ($fieldRule['depend']['isDepend'] && isset($fieldRule['depend']['dependOnValue'])) ? $fieldRule['depend']['dependOnValue'] : null;
                          $dependOnIncident = ($fieldRule['depend']['isDepend'] && isset($fieldRule['depend']['dependOnIncident'])) ? $fieldRule['depend']['dependOnIncident'] : null;

                          $dynamic = 'false';
                          $dynamicValue = [];
                          if($tempField['type'] == '23'){
                            $fieldData = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
                            $tempField['type'] = $fieldData['fieldType'];
                            $entity = $fieldData['entity'];
                            $method = $fieldData['repoFunction'];
                            $dynamic = 'true';
                            $dynamicValue = [];
                            // if(array_key_exists("entity",$fieldData)){
                            //   $entity = $fieldData['entity'];
                            // }
                            if(!empty($dependOnIncident)){
                              switch ($dependOnIncident) {
                                case 'station':
                                  $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method('station', $incidentData['station']['id']);
                                  break;
                                case 'driver':
                                  $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method('driver', $incidentData['driver']['id']);
                                  break;
                                case 'company':
                                  $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method('company', $companyId);
                                  break;
                                default:
                                  $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method();
                              }
                            }else{
                              $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method();
                            }
                          }

                          if($tempField['type'] == '15'){
                            $tempProps['defaultValue'] = isset($incidentFormFieldsValue[$incidentFormField->getId()]) ? $incidentFormFieldsValue[$incidentFormField->getId()] : '';
                          }elseif ($tempField['type'] == '13') {
                            $defaultValue = isset($incidentFormFieldsValue[$incidentFormField->getId()]) ? $incidentFormFieldsValue[$incidentFormField->getId()] : '';
                            if(is_array($defaultValue)){
                              $tempProps['defaultValue'] = array_map('intval', $defaultValue);
                            }else {
                              $tempProps['defaultValue'] = $defaultValue;
                            }
                          }else {
                            $tempProps['defaultValue'] = isset($incidentFormFieldsValue[$incidentFormField->getId()]) ? $incidentFormFieldsValue[$incidentFormField->getId()] : '';
                            if($tempProps['defaultValue'] != '' && $incidentPhase->getHasReview() == true && array_key_exists('hasAsingn', $fieldRule)){
                              $reviewDriver = $this->getEntityManager()->getRepository('App\Entity\Driver')->find($tempProps['defaultValue']);
                              if($reviewDriver->getUser()->getId() == $loginUser->getId() || $this->checkRolesExist($this->getParentRole($reviewDriver->getUser()->getUserFirstRole()))){
                                $tempPhase['hasReview'] = true;
                              }
                            }
                          }

                          switch ($tempField['type']) {
                            // 1 = TYPE_TEXT = 'text'
                            case '1':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 2 = TYPE_TEXTAREA = 'textarea'
                            case '2':
                              $tempField['type'] = 'Textarea';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 3 = TYPE_PASSWORD = 'password'
                            case '3':
                              $tempField['type'] = 'Password';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 4 = TYPE_INTEGER = 'integer'
                            case '4':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'integer';
                              break;

                              // 5 = TYPE_PERCENT = 'percent'
                            case '5':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'percent';
                              break;

                            // 6 = TYPE_NUMBER = 'number'
                            case '6':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'number';
                              break;

                            // 7 = TYPE_RANGE = 'range'
                            case '7':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'range';
                              break;

                            // 8 = TYPE_EMAIL_ADDRESS = 'emailaddress'
                            case '8':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'email';
                              break;

                            // 9 = TYPE_PHONE = 'phone'
                            case '9':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'phone';
                              break;

                            // 10 = TYPE_URL = 'url'
                            case '10':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempProps['type'] = 'url';
                              break;

                            // 11 = TYPE_MONEY = 'money'
                            case '11':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 12 = TYPE_SELECT = 'select'
                            case '12':
                              // $tempField['type'] = 'Input';
                              $tempField['type'] = 'Select';
                              $tempProps['title'] = $tempTitle;
                              $tempOptions = [];
                              if($dynamic == 'true'){
                                foreach ($dynamicValue as $key => $value) {

                                  $tempOption = [];
                                  $tempOption['name'] = $value['name'];
                                  $tempOption['value'] = $value['id'];
                                  $tempOption['fid'] = $incidentFormField->getId();
                                  $tempOption['sid'] = $step->getId();

                                  array_push($tempOptions,$tempOption);
                                }
                              }else{
                                $values = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
                                foreach ($values as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['name'] = $value;
                                  $tempOption['value'] = $key;

                                  array_push($tempOptions,$tempOption);
                                }
                              }
                              $tempProps['options'] = $tempOptions;
                              $fieldOptions[$incidentFormField->getId()] = $tempOptions;
                              break;

                            // 13 = TYPE_MULTI_SELECT = 'multi-select'
                            case '13':
                              $tempProps['multiple'] = 'true';
                              $tempField['type'] = 'MultiSelect';
                              $tempProps['title'] = $tempTitle;
                              $tempOptions = [];
                              if($dynamic == 'true'){
                                foreach ($dynamicValue as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['name'] = $value['name'];
                                  $tempOption['value'] = $value['id'];

                                  array_push($tempOptions,$tempOption);
                                }
                              }else{
                                $values = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
                                foreach ($values as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['name'] = $value;
                                  $tempOption['value'] = $key;

                                  array_push($tempOptions,$tempOption);
                                }
                              }
                              $tempProps['options'] = $tempOptions;
                              break;

                            // 14 = TYPE_CHECKBOX = 'checkbox'
                            case '14':
                              // $tempField['type'] = 'Input';
                              $tempField['type'] = 'Checkbox';
                              $tempProps['title'] = $tempTitle;
                              $tempOptions = [];
                              if($dynamic == 'true'){
                                foreach ($dynamicValue as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['label'] = $value['name'];
                                  $tempOption['value'] = $value['id'];

                                  array_push($tempOptions,$tempOption);
                                }
                              }else{
                                $values = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
                                foreach ($values as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['label'] = $value;
                                  $tempOption['value'] = $key;

                                  array_push($tempOptions,$tempOption);
                                }
                              }

                              $tempProps['options'] = $tempOptions;
                              break;

                            // 15 = TYPE_RADIO = 'radio'
                            case '15':
                              $tempField['type'] = 'Radio';
                              $tempProps['title'] = $tempTitle;

                              $tempOptions = [];
                              if($dynamic == 'true'){
                                foreach ($values as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['label'] = $value['id'];
                                  $tempOption['value'] = $value['name'];

                                  array_push($tempOptions,$tempOption);
                                }
                              }else{
                                $values = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
                                foreach ($values as $key => $value) {
                                  $tempOption = [];
                                  $tempOption['label'] = $value;
                                  $tempOption['value'] = $key;

                                  array_push($tempOptions,$tempOption);
                                }
                              }

                              $tempProps['options'] = $tempOptions;
                              break;

                            // 16 = TYPE_DATE = 'date'
                            case '16':
                              // $tempField['type'] = 'Date';
                              $tempField['type'] = 'Day';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 17 = TYPE_DATETIME = 'date-time'
                            case '17':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              // $tempField['type'] = 'DateTime';
                              break;

                            // 18 = TYPE_TIME = 'time'
                            case '18':
                              // $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              $tempField['type'] = 'TimePicker';
                              break;

                            // 19 = TYPE_COLOR_PICKER = 'color-picker'
                            case '19':
                              $tempField['type'] = 'InputColor';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 20 = TYPE_CURRENCY = 'currency'
                            case '20':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 21 = TYPE_FILE = 'file'
                            case '21':
                              $tempField['type'] = 'File';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 22 = TYPE_QUESTION = 'question'
                            case '22':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 23 = TYPE_ENTITY = 'entity'
                            case '23':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 24 = TYPE_COLLECTION = 'collection'
                            case '24':
                              $tempField['type'] = 'Input';
                              $tempProps['label'] = $tempTitle;
                              break;

                            // 25 = TYPE_HIDDEN = 'hidden'
                            case '25':
                              $tempField['type'] = 'Input';
                              $tempProps['type'] = 'hidden';
                              // $tempProps['label'] = $tempTitle;
                              break;

                            default:
                              // code...
                              break;
                          }

                          $tempField['props'] = $tempProps;
                          array_push($tempFields,$tempField);
                          $fieldCount ++;
                        }
                    }

                    $tempStep['fields'] = $tempFields;
                    array_push($tempSteps,$tempStep);
                }
            }
            $tempPhase['steps']= $tempSteps;
            $tempPhase['fieldCount'] = $fieldCount;
            $count++;
            array_push($phaseData,$tempPhase);
          }


        $incidentData['phases'] = $phaseData;

        $incidentData['options'] = $fieldOptions;


        return $incidentData;
    }

    public function getDynamicOptions($criteria){
      $fieldId = $criteria['id'];
      $conditionParameter = $criteria['conditionParameter'];
      $incidentFormField = $this->getEntityManager()->getRepository('App\Entity\IncidentFormFields')->find($fieldId);

      $tempField = [];
      $tempField['type'] = ($incidentFormField->getFieldId() != '')?$incidentFormField->getFieldId():$incidentFormField->getIncidentQuestion()->getFieldId();

      $dynamicValue = [];
      $tempOptions = [];
      if($tempField['type'] == '23'){
        $fieldData = (count($incidentFormField->getFieldData()) > 0)?$incidentFormField->getFieldData():$incidentFormField->getIncidentQuestion()->getFieldData();
        $tempField['type'] = $fieldData['fieldType'];
        $entity = $fieldData['entity'];
        $method = $fieldData['repoFunction'];
        // if(array_key_exists("entity",$fieldData)){
        //   $entity = $fieldData['entity'];
        // }

        $dynamicValue = $this->getEntityManager()->getRepository('App\Entity\\'.$entity)->$method($conditionParameter);

        //props
        switch ($tempField['type']) {
          // 12 = TYPE_SELECT = 'select'
          case '12':
            foreach ($dynamicValue as $key => $value) {

              $tempOption = [];
              $tempOption['name'] = $value['name'];
              $tempOption['value'] = $value['id'];
              $tempOption['fid'] = $incidentFormField->getId();

              array_push($tempOptions,$tempOption);
            }
            break;

          // 13 = TYPE_MULTI_SELECT = 'multi-select'
          case '13':
            foreach ($dynamicValue as $key => $value) {
              $tempOption = [];
              $tempOption['name'] = $value['name'];
              $tempOption['value'] = $value['id'];
              $tempOption['fid'] = $incidentFormField->getId();

              array_push($tempOptions,$tempOption);
            }
            break;

          // 14 = TYPE_CHECKBOX = 'checkbox'
          case '14':
            foreach ($dynamicValue as $key => $value) {
              $tempOption = [];
              $tempOption['label'] = $value['name'];
              $tempOption['value'] = $value['id'];

              array_push($tempOptions,$tempOption);
            }
            break;

          // 15 = TYPE_RADIO = 'radio'
          case '15':
            foreach ($values as $key => $value) {
              $tempOption = [];
              $tempOption['label'] = $value['id'];
              $tempOption['value'] = $value['name'];

              array_push($tempOptions,$tempOption);
            }
            break;

          default:
            // code...
            break;
        }

      }
      return $tempOptions;

    }

    public function savePhase($criteria){
      $incidentId = $criteria['incidentId'];
      $data = $criteria['data'];
      $incident = $this->getEntityManager()->getRepository('App\Entity\Incident')->find($incidentId);
      $user = $this->tokenStorage->getToken()->getUser();
      $currentPhase = $incident->getIncidentCurrentPhase();
      $checkAsignFill = false;

      if(!empty($currentPhase)){
        foreach ($data['steps'] as $step) {
            foreach ($step['fields'] as $key => $value) {
              $fieldId = trim($key,'\'');

              $field = $this->getEntityManager()->getRepository('App\Entity\IncidentFormFields')->find($fieldId);

              $incidentFormFieldValue = new IncidentFormFieldValue();
              $incidentFormFieldValue->setIncidentFormField($field);
              $incidentFormFieldValue->setIncident($incident);
              $incidentFormFieldValue->setValue([$value]);
              $incidentFormFieldValue->setCreatedDate(new \DateTime());
              $this->getEntityManager()->persist($incidentFormFieldValue);
              $this->getEntityManager()->flush();
              if (is_array($value)) {
                if (isset($value['type']) && $value['type'] == 'image') {
                  $image = new Image();
                  $image->setPath($value['data']);
                  $this->getEntityManager()->persist($image);
                  $this->getEntityManager()->flush();

                  $incidentFile = new IncidentFile();
                  $incidentFile->setImage($image);
                  $incidentFile->setIncidentFormFieldValue($incidentFormFieldValue);
                  $this->getEntityManager()->persist($incidentFile);
                  $this->getEntityManager()->flush();
                }
              } else {
                $fieldRule = (count($field->getFieldRule()))?$field->getFieldRule():$field->getIncidentQuestion()->getFieldRule();
                if(array_key_exists('hasAsingn', $fieldRule) && $value != ''){
                  $checkAsignFill = true;
                }
              }
            }
        }

        $newPhase=[];
        $newPhase = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->findBy(['incidentType' => $incident->getIncidentType()->getId(), 'ordering' => $currentPhase->getOrdering()+1]);
        $incident->setIncidentCompletedPhase($currentPhase);

        if($currentPhase->getHasReview() != true || $checkAsignFill == false){
          if(count($newPhase) > 0){
              $incident->setIncidentCurrentPhase($newPhase[0]);
          }else{
              $incident->setIncidentCurrentPhase(null);
          }
        }
        $incident->setTitle($data['title']);
        $this->getEntityManager()->persist($incident);
        $this->getEntityManager()->flush();

        $incidentSubmitedPhase = new IncidentSubmitedPhase();
        $incidentSubmitedPhase->setIncidentPhase($currentPhase);
        $incidentSubmitedPhase->setSubmitedBy($user);
        $incidentSubmitedPhase->setIncident($incident);
        $this->getEntityManager()->persist($incidentSubmitedPhase);
        $this->getEntityManager()->flush();

        $messageData = array('incident_type' => $incident->getIncidentType()->getName(), 'incident_phase' => $currentPhase->getTitle(), 'user_name'=>$user->getFriendlyName());
        $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Incident Phase Save', $incident->getDriverRoute(), 'INCIDENT_PHASE_SAVE', $messageData, true);
      }
      return ['isFinish' => (count($incident->getIncidentSubmitedPhases()) == count($incident->getIncidentType()->getIncidentPhases())), 'submitted' => true];
    }

    public function editPhase($criteria){
      $incidentId = $criteria['incidentId'];
      $data = $criteria['data'];
      $incident = $this->getEntityManager()->getRepository('App\Entity\Incident')->find($incidentId);
      $user = $this->tokenStorage->getToken()->getUser();
      $currentPhase = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->find($data['currentPhaseId']);
      $incidentReviewOrNot = false;
      //Get form Fields id for remove old data
      $aFormFieldIds = [];
      foreach ($currentPhase->getIncidentSteps() as $incidentStep) {
          foreach ($incidentStep->getIncidentFormFields() as $formFields)
          {
              $aFormFieldIds[] = $formFields->getId();
          }
      }
      //Remove form fields
      $this->getEntityManager()->getRepository('App\Entity\IncidentFormFieldValue')->removeIncidentFormFieldValues($incident->getId(), $aFormFieldIds);
      foreach ($data['steps'] as $step) {
          foreach ($step['fields'] as $key => $value) {
            $fieldId = trim($key,'\'');

            $field = $this->getEntityManager()->getRepository('App\Entity\IncidentFormFields')->find($fieldId);

            $fieldRule = (count($field->getFieldRule()))?$field->getFieldRule():$field->getIncidentQuestion()->getFieldRule();
            if(array_key_exists('hasAsingn', $fieldRule) && $value != '')
              $incidentReviewOrNot = true;

            $incidentFormFieldValue = new IncidentFormFieldValue();
            $incidentFormFieldValue->setIncidentFormField($field);
            $incidentFormFieldValue->setIncident($incident);
            $incidentFormFieldValue->setValue([$value]);
            $incidentFormFieldValue->setCreatedDate(new \DateTime());
            $this->getEntityManager()->persist($incidentFormFieldValue);
            $this->getEntityManager()->flush();

            if (is_array($value)) {
              if (isset($value['type']) && $value['type'] == 'image') {
                $image = new Image();
                $image->setPath($value['data']);
                $this->getEntityManager()->persist($image);
                $this->getEntityManager()->flush();

                $incidentFile = new IncidentFile();
                $incidentFile->setImage($image);
                $incidentFile->setIncidentFormFieldValue($incidentFormFieldValue);
                $this->getEntityManager()->persist($incidentFile);
                $this->getEntityManager()->flush();
              }
            }
          }
      }

      //Update submited bY current submited phase
      if (array_key_exists('reviewPhase',$data) && $data['reviewPhase'] == true) {
        $newPhase=[];
        $newPhase = $this->getEntityManager()->getRepository('App\Entity\IncidentPhase')->findBy(['incidentType' => $incident->getIncidentType()->getId(), 'ordering' => $currentPhase->getOrdering()+1]);
        $incident->setIncidentCompletedPhase($currentPhase);
        if(count($newPhase) > 0){
          $incident->setIncidentCurrentPhase($newPhase[0]);
        }else{
          $incident->setIncidentCurrentPhase(null);
        }

        $this->getEntityManager()->getRepository('App\Entity\IncidentSubmitedPhase')->reviewIncidentSubmitedPhase($incident->getId(), $currentPhase->getId(), $user->getId());
      } elseif($incidentReviewOrNot == true) {
        $incident->setIncidentCompletedPhase($currentPhase);
        $incident->setIncidentCurrentPhase($currentPhase);
        $this->getEntityManager()->getRepository('App\Entity\IncidentSubmitedPhase')->reviewIncidentSubmitedPhase($incident->getId(), $currentPhase->getId(), 'null');

        $allSubmitedPhases = $this->getEntityManager()->getRepository('App\Entity\IncidentSubmitedPhase')->findBy(['incident' => $incident->getId()]);
        foreach($allSubmitedPhases as $submitedPhase){
          if($submitedPhase->getIncidentPhase()->getOrdering() > $currentPhase->getOrdering()){
            $this->getEntityManager()->remove($submitedPhase);
            $this->getEntityManager()->flush();
          }
        }
      } else {
        $this->getEntityManager()->getRepository('App\Entity\IncidentSubmitedPhase')->updateIncidentSubmitedPhase($incident->getId(), $currentPhase->getId(), $user->getId());
      }

      $incident->setTitle($data['title']);
      $this->getEntityManager()->persist($incident);
      $this->getEntityManager()->flush();

      $messageData = array('incident_type' => $incident->getIncidentType()->getName(), 'incident_phase' => $currentPhase->getTitle(), 'user_name'=>$user->getFriendlyName());
      $this->getEntityManager()->getRepository('App\Entity\Event')->addEventData('Incident Phase Update', $incident->getDriverRoute(), 'INCIDENT_PHASE_UPDATE', $messageData, true);

      return ['isFinish' => (count($incident->getIncidentSubmitedPhases()) == count($incident->getIncidentType()->getIncidentPhases())), 'submitted' => true];
    }
    public function getIncidentPhaseByType($incidentTypeId){
      $incidentPhases = $this->createQueryBuilder('ip')
              ->where('ip.incidentType = :incidentType')
              ->setParameter('incidentType', $incidentTypeId)
              ->andWhere('ip.isArchive = :isArchive')
              ->setParameter('isArchive', false)
              ->orderBy('ip.ordering', 'ASC')
              ->getQuery()
              ->getResult()
          ;
       return $incidentPhases;
    }
}
