<?php

namespace App\Repository;

use App\Entity\ExtraInformation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ExtraInformation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExtraInformation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExtraInformation[]    findAll()
 * @method ExtraInformation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExtraInformationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtraInformation::class);
    }
}
