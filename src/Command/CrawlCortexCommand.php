<?php

namespace App\Command;

use App\Entity\Company;
use App\Entity\CortexSettings;
use App\Entity\DriverRouteCode;
use App\Entity\Station;
use App\Entity\DriverRoute;
use App\Controller\CortexLog;
use App\Criteria\Common;
use App\Services\CortexService;
use App\Utils\EventEmitter;
use App\Utils\GlobalUtility;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Client;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;

class CrawlCortexCommand extends Command
{
    protected static $defaultName = 'dsp:crawl:cortex';

    private $endpoint = 'https://eljt9h4q8h.execute-api.us-east-2.amazonaws.com/default/cortex';

    private $em;
    private $eventEmitter;
    private $cortexService;

    public function __construct(
        EntityManagerInterface $em,
        EventEmitter $eventEmitter,
        CortexService $cortexService
    ) {
        $this->em = $em;
        $this->eventEmitter = $eventEmitter;
        $this->cortexService = $cortexService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Crawl Cortex data for today')
        ;
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $io = new SymfonyStyle($input, $output);
        $opts = (object) $input->getOptions();

        $conn = $this->em->getConnection();
        $validStationList = (new ArrayCollection($conn->query("
            SELECT 
                c.id as settings,
                s.id as station,
                (SELECT COUNT(*) FROM driver_route WHERE date_created = CURDATE() AND station_id = s.id AND is_archive = 0) as routes
            FROM 
                cortex_settings c
            JOIN station s ON s.company_id = c.company_id
            WHERE 
                c.is_archive = 0
                AND c.cortex_username IS NOT NULL 
                AND c.cortex_password IS NOT NULL
                AND s.is_archive = 0
        ")->fetchAll()))->filter(function($station) {
            return $station['routes'] > 0;
        });

        if ( !$validStationList->count() )
            return $io->comment('No enabled Cortex accounts or routes for today');

        foreach ($validStationList as $station) {
            $settings = $this->em->getRepository(CortexSettings::class)->find($station['settings']);
            $station  = $this->em->getRepository(Station::class)->find($station['station']);
            $flexData = $this->cortexService->getStationDetailsFromFlex($station->getCode());

            if (!$flexData)
                continue;

            $payload = [
                'company'  => $settings->getCompany()->getId(),
                'username' => $settings->getCortexUsername(),
                'password' => $settings->getCortexPassword(),
                'cookies'  => $settings->getCortexCookies(),
                'serviceAreaId' => $flexData->serviceAreaID
            ];

            $client = new Client();
            $result = $client->request(
                'POST',
                $this->endpoint,
                ['json' => $payload]
            );

            if ($result->getStatusCode() !== 200)
                continue;

            try {
                $json = json_decode($result->getBody()->getContents());
            } catch (\Exception $e) {
                $io->writeln("[ERROR] Company #{$settings->getCompany()->getId()} - " . $e->getMessage());
                continue;
            }

            $settings->setCortexCookies(json_encode($json->cookies));
            $this->em->persist($settings);
            $this->em->flush();

            try {
                $process = $this->cortexService->processLog((object)[
                    'serviceAreaId' => $flexData->serviceAreaID,
                    'log' => $json->records
                ]);
            } catch (\Exception $e) {
                continue;
            }

            // Do something with the existing driver routes and match the route codes
            $rsm = new Query\ResultSetMapping();
            $rsm->addEntityResult(DriverRoute::class, 'dr');
            $rsm->addFieldResult('dr', 'id', 'id');
            $rsm->addFieldResult('dr', 'route_code', 'routeCode');

            $stationTz  = new \DateTimeZone($process->station->getTimeZoneName());
            $stationNow = new \DateTime('now', $stationTz);
            $utcNow     = GlobalUtility::newUTCDateTime()->format('Y-m-d H:i:00');
            $offset     = $stationNow->format('P');

            $query = $this->em->createNativeQuery("
                SELECT
                    dr.id,
                    dr.route_code
                FROM driver_route AS dr
                    JOIN driver_route_code rc ON rc.code = dr.route_code
                WHERE
                    rc.code IN ({$process->codesQuery})
                    AND dr.station_id = {$process->station->getId()}
                    AND dr.is_archive = 0
                    AND dr.date_created = DATE(CONVERT_TZ('{$utcNow}', '+00:00', '{$offset}'))
            ", $rsm);
            $driverRoutesWithCodes = new ArrayCollection($query->getResult());

            foreach ($process->routes as $route) {
                $drs = $driverRoutesWithCodes->filter(function ($dr) use ($route) {
                    $codes = $dr->getDriverRouteCodes()->map(function ($drc) {
                        return $drc->getCode();
                    });
                    return $codes->contains($route->routeCode);
                });

                if ( !$drs->count() )
                    continue;

                foreach ($drs as $driverRoute) {
                    $this->em->getConnection()->exec("
                        UPDATE 
                            driver_route
                        SET 
                            count_of_total_stops = {$route->totalCountOfStops},
                            completed_count_of_stops = {$route->completedCountOfStops},
                            count_of_total_packages = {$route->countOfTotalPackages},
                            count_of_attempted_packages = {$route->countOfAttemptedPackages},
                            count_of_delivered_packages = {$route->countOfDeliveredPackages},
                            count_of_returning_packages = {$route->countOfReturningPackages},
                            count_of_missing_packages = {$route->routeDeliveryProgress->countOfMissingPackages}
                        WHERE
                            id = {$driverRoute->getId()}
                    ");
                    // todo send event
                }
            }
        }
    }
}
