<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\StripeService;

class CompanyStripeCommand extends Command
{
    protected static $defaultName = 'app:company-stripe';

    /**
     * @var StripeService
     */
    private $stripeService;

    private $em;

    public function __construct(
        StripeService $stripeService,
        EntityManagerInterface $em
    ) {
        /**
         * Initialize Stripe service.
         */
        $this->stripeService = $stripeService;
        $this->em = $em;

        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $users = $this->em->getRepository('App\Entity\User')->findAll();
        $this->stripeService->setApiKey();

        foreach ($users as $user){
            $company = $user->getCompany();
            if($company) {
                $stripeData = $company->getStripeData();
                if($stripeData != ''){
                    $stripeData = json_decode($stripeData,1);

                    $customerId = $stripeData['id'];

                    $company->setStripeData(json_encode($this->stripeService->getCustomer($customerId)));
                    $this->em->persist($company);
                    $this->em->flush();
                }
            }
        }

        $io->success('stripe data successfully refresh.');
    }
}
