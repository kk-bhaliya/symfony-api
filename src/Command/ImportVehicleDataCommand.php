<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Vehicle;
use App\Entity\Location;


class ImportVehicleDataCommand  extends Command
{
    protected static $defaultName = 'app:import-vehicle';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('file', InputArgument::OPTIONAL, 'Enter file name days')
            ->addArgument('company', InputArgument::OPTIONAL, 'Enter company ID')
            ->addArgument('station', InputArgument::OPTIONAL, 'Enter station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $fileName = 'public/csv/vehicle/'.$input->getArgument('file');
        $company = $input->getArgument('company');
        $station = $input->getArgument('station');

        if (file_exists($fileName)) {
            try {
                $file = fopen($fileName, "r");
                $firstRow = true;
                $cnt = 0;

                while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if($cnt > 2){  
                         if(strlen($column[10]) > 0 ){
                            $location = $this->em->getRepository('App\Entity\Location')->findOneBy(['station'=>$station]);
                            if( !$location  ){
                                $location = new Location();   
                                $location->setAddress($column[10]);
                                $location->setStation($this->em->getRepository('App\Entity\Station')->find($station));
                                $location->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
                                $this->em->persist($location);
                                $this->em->flush();                            
                            }
                         }  else {
                            $location = null;
                         }  

                         $vehicle = new Vehicle();
                         $vehicle->setStation($this->em->getRepository('App\Entity\Station')->find($station));
                         $vehicle->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
                         $vehicle->setStatus($column[3]);
                         $vehicle->setVehicleId($column[2]);
                         if($location){
                            $vehicle->setLocation($location);   
                         }                         
                        
                         $vehicle->setNotes($column[4]);
                         $vehicle->setVin($column[17]);
                         $vehicle->setState($column[11]);
                         $vehicle->setLicensePlate($column[12]);                         
                         $vehicle->setLicensePlateExpiration(strlen($column[13]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[13]))) : null);                                                  
                         $vehicle->setMake($column[5]);
                         $vehicle->setModel($column[6]);
                         $vehicle->setModelType(str_replace(' ','_',strtolower($column[7])));
                         $vehicle->setYear($column[8]);
                         $vehicle->setHeight($column[9]);
                         $vehicle->setFuelCardNumber($column[20]);
                         if($column[30] != 'N/A'){
                            $vehicle->setDateStart(strlen($column[30]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[30]))) : null);
                         }
                         if($column[31] != 'N/A'){
                            $vehicle->setDateEnd(strlen($column[31]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[31]))) : null);
                         }                            

                         $vehicle->setInsuranceCompany($column[21]);
                         $vehicle->setPolicyNumber($column[22]);
                         $vehicle->setTankCapacity($column[18]);
                         $vehicle->setGasType($column[19]);
                         $vehicle->setIsOwned($column[25] == 'Owned' ? true : false);
                         $this->em->persist($vehicle);
                         $this->em->flush(); 
                         
                    }                        
                    $cnt++;  
                }   
                $io->success($cnt.' record has been inserted successfully'); 
            } catch (Exception $e) {
                $io->warning('Error.'.$e);
            }
        } else {
            $io->warning('file not found.');
        }    
        
    }
}
