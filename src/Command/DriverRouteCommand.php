<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\DriverRoute;
use App\Entity\KickoffLog;
use App\Entity\ReturnToStation;


class DriverRouteCommand extends Command
{
    protected static $defaultName = 'app:driver-route';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('days', InputArgument::OPTIONAL, 'Enter days')
            ->addArgument('sheduleId', InputArgument::OPTIONAL, 'Enter schedule ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $days = $input->getArgument('days');
        $sheduleId = $input->getArgument('sheduleId');

        $generatedDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currentDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        
        if ($days) {
            $afterDate = 'P' . $days . 'D';
            $after14Days = $currentDate->add(new \DateInterval($afterDate));
        } else {
            $after14Days = $currentDate->add(new \DateInterval('P14D'));
        }

        $schedules = $this->em->getRepository('App\Entity\Schedule')->getSchedulesNext14days($sheduleId, $days);
        foreach ($schedules as $schedule) {
            foreach ($schedule->getDrivers() as $driver) {
                foreach ($schedule->getScheduleDesigns() as $shiftDesign) {
                    if (count($shiftDesign->getShifts()) > 0) {
                        if ($shiftDesign->getSunday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 0);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->addDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();
                                //
                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getMonday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 1);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->addDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();
                                //
                                //  $returnToStation = new ReturnToStation();
                                //  $returnToStation->addDriverRoute($driverRoute);
                                //  $this->em->persist($returnToStation);
                                //  $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getTuesday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 2);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->setDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();

                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getWednesday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 3);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->setDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();
                                //
                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getThursday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 4);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->setDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();

                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getFriday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 5);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->setDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();

                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                        if ($shiftDesign->getSaturday() == true) {
                            $generatedDate->setISODate(date('Y'), $shiftDesign->getWeek(), 6);
                            if ($generatedDate->format('Y-m-d') >= date('Y-m-d') && $generatedDate <= $after14Days && $this->em->getRepository('App\Entity\DriverRoute')->checkRoute($schedule, $driver->getId(), $shiftDesign->getShifts()[0]->getId(), $generatedDate)) {
                                $driverRoute = new DriverRoute();
                                $driverRoute->setSkill($shiftDesign->getShifts()[0]->getSkill());
                                $driverRoute->setDriver($driver);
                                $driverRoute->setSchedule($schedule);
                                $driverRoute->setStation($schedule->getStation());
                                $driverRoute->setDateCreated(new \DateTime($generatedDate->format('Y-m-d 00:00:00')));
                              //  $driverRoute->setShiftTime(new \DateTime($generatedDate->format('Y-m-d') . ' ' . $shiftDesign->getShifts()[0]->getStartTime()));
                                $driverRoute->setShiftType($shiftDesign->getShifts()[0]);
                                $this->em->persist($driverRoute);
                                $this->em->flush();

                                // $kickoff = new KickoffLog();
                                // $kickoff->setDriverRoute($driverRoute);
                                // $this->em->persist($kickoff);
                                // $this->em->flush();

                                // $returnToStation = new ReturnToStation();
                                // $returnToStation->addDriverRoute($driverRoute);
                                // $this->em->persist($returnToStation);
                                // $this->em->flush();
                            }
                        }
                    }
                }
            }
        }
        $io->success('Driver route successfully generated.');
    }
}
