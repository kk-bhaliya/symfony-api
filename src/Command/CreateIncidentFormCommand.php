<?php

namespace App\Command;

use App\Entity\IncidentFormFields;
use App\Entity\IncidentPhase;
use App\Entity\IncidentQuestion;
use App\Entity\IncidentStep;
use App\Entity\IncidentType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateIncidentFormCommand extends Command
{
    protected static $defaultName = 'incidentform:create';

    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Create Incident Form')
            ->addOption('company', 'c', InputOption::VALUE_REQUIRED, 'Company Id');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // $session = new Session();
        // $session->set('soft-delete-enable', true);
        // $incidentTypes = $this->em->getRepository('App\Entity\IncidentType')->findAll();
        // foreach ($incidentTypes as $incidentType) {
        //     $incidentType->setIsArchive(1);
        //     $this->em->persist($incidentType);
        //     $this->em->flush();
        // }
        // $session->remove('soft-delete-enable');

        $io = new SymfonyStyle($input, $output);
        $opts = (object) $input->getOptions();
        $currentDateTime = new \DateTime();

        if (!$opts->company) {
            return $io->comment('No company provided');
        }

        $company = $this->em->getRepository('App\Entity\Company')->find($opts->company);

        if (!$company) {
            return $io->comment('Invalid company id !!!');
        }

        // incidentQuestion 1
        $question1 = new IncidentQuestion();
        $question1->setCompany($company);
        $question1->setLabel('');
        $question1->setFieldId(25);
        $question1->setFieldData([]);
        $question1->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => false],
        ]);
        $question1->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question1->setIsArchive(false);
        $this->em->persist($question1);
        $this->em->flush();

        // incidentQuestion 2
        $question2 = new IncidentQuestion();
        $question2->setCompany($company);
        $question2->setLabel('');
        $question2->setFieldId(16);
        $question2->setFieldData([]);
        $question2->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question2->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question2->setIsArchive(false);
        $this->em->persist($question2);
        $this->em->flush();

        // incidentQuestion 3
        $question3 = new IncidentQuestion();
        $question3->setCompany($company);
        $question3->setLabel('Was Central Dispatch Called');
        $question3->setFieldId(15);
        $question3->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question3->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question3->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question3->setIsArchive(false);
        $this->em->persist($question3);
        $this->em->flush();

        // incidentQuestion 4
        $question4 = new IncidentQuestion();
        $question4->setCompany($company);
        $question4->setLabel('Brief Description of the Incident');
        $question4->setFieldId(1);
        $question4->setFieldData([]);
        $question4->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question4->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question4->setIsArchive(false);
        $this->em->persist($question4);
        $this->em->flush();

        // incidentQuestion 5
        $question5 = new IncidentQuestion();
        $question5->setCompany($company);
        $question5->setLabel('Incident Type');
        $question5->setFieldId(13);
        $question5->setFieldData([
            1 => 'Vehicle Accident',
            2 => 'Dog Bite',
            3 => 'Personal Injury',
            4 => 'Slip or Fall',
            5 => 'Property Damage',
            6 => 'Near Miss',
            7 => 'Stuck',
            8 => 'Vehicle Breakdown',
            9 => 'Other',
        ]);
        $question5->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question5->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question5->setIsArchive(false);
        $this->em->persist($question5);
        $this->em->flush();

        // incidentQuestion 6
        $question6 = new IncidentQuestion();
        $question6->setCompany($company);
        $question6->setLabel('Picture of Damage');
        $question6->setFieldId(21);
        $question6->setFieldData([]);
        $question6->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question6->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question6->setIsArchive(false);
        $this->em->persist($question6);
        $this->em->flush();

        // incidentQuestion 7

        $question7 = new IncidentQuestion();
        $question7->setCompany($company);
        $question7->setLabel('Odometer');
        $question7->setFieldId(1);
        $question7->setFieldData([]);
        $question7->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question7->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question7->setIsArchive(false);
        $this->em->persist($question7);
        $this->em->flush();

        // incidentQuestion 8

        $question8 = new IncidentQuestion();
        $question8->setCompany($company);
        $question8->setLabel('Front Left Corner of Vehicle');
        $question8->setFieldId(21);
        $question8->setFieldData([]);
        $question8->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question8->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question8->setIsArchive(false);
        $this->em->persist($question8);
        $this->em->flush();

        // incidentQuestion 9

        $question9 = new IncidentQuestion();
        $question9->setCompany($company);
        $question9->setLabel('Front Right Corner of Vehicle');
        $question9->setFieldId(21);
        $question9->setFieldData([]);
        $question9->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question9->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question9->setIsArchive(false);
        $this->em->persist($question9);
        $this->em->flush();

        // incidentQuestion 10

        $question10 = new IncidentQuestion();
        $question10->setCompany($company);
        $question10->setLabel('Back Right Corner of Vehicle');
        $question10->setFieldId(21);
        $question10->setFieldData([]);
        $question10->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question10->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question10->setIsArchive(false);
        $this->em->persist($question10);
        $this->em->flush();

        // incidentQuestion 11

        $question11 = new IncidentQuestion();
        $question11->setCompany($company);
        $question11->setLabel('Inside Cab');
        $question11->setFieldId(21);
        $question11->setFieldData([]);
        $question11->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question11->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question11->setIsArchive(false);
        $this->em->persist($question11);
        $this->em->flush();

        // incidentQuestion 12
        $question12 = new IncidentQuestion();
        $question12->setCompany($company);
        $question12->setLabel('Was the authoritize called');
        $question12->setFieldId(15);
        $question12->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question12->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question12->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question12->setIsArchive(false);
        $this->em->persist($question12);
        $this->em->flush();

        // incidentQuestion 13
        $question13 = new IncidentQuestion();
        $question13->setCompany($company);
        $question13->setLabel('License Plate #');
        $question13->setFieldId(21);
        $question13->setFieldData([]);
        $question13->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question13->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question13->setIsArchive(false);
        $this->em->persist($question13);
        $this->em->flush();

        // incidentQuestion 14

        $question14 = new IncidentQuestion();
        $question14->setCompany($company);
        $question14->setLabel('Pictures of Damages');
        $question14->setFieldId(21);
        $question14->setFieldData([]);
        $question14->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question14->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question14->setIsArchive(false);
        $this->em->persist($question14);
        $this->em->flush();

        // incidentQuestion 15

        $question15 = new IncidentQuestion();
        $question15->setCompany($company);
        $question15->setLabel('Pictures of insurance card');
        $question15->setFieldId(21);
        $question15->setFieldData([]);
        $question15->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question15->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question15->setIsArchive(false);
        $this->em->persist($question15);
        $this->em->flush();

        // incidentQuestion 16

        $question16 = new IncidentQuestion();
        $question16->setCompany($company);
        $question16->setLabel('Location of incident');
        $question16->setFieldId(1);
        $question16->setFieldData([]);
        $question16->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question16->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question16->setIsArchive(false);
        $this->em->persist($question16);
        $this->em->flush();

        // incidentQuestion 17

        $question17 = new IncidentQuestion();
        $question17->setCompany($company);
        $question17->setLabel('City');
        $question17->setFieldId(1);
        $question17->setFieldData([]);
        $question17->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question17->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question17->setIsArchive(false);
        $this->em->persist($question17);
        $this->em->flush();

        // incidentQuestion 18

        $question18 = new IncidentQuestion();
        $question18->setCompany($company);
        $question18->setLabel('State');
        $question18->setFieldId(1);
        $question18->setFieldData([]);
        $question18->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question18->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question18->setIsArchive(false);
        $this->em->persist($question18);
        $this->em->flush();

        // incidentQuestion 19

        $question19 = new IncidentQuestion();
        $question19->setCompany($company);
        $question19->setLabel('ZipCode');
        $question19->setFieldId(1);
        $question19->setFieldData([]);
        $question19->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question19->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question19->setIsArchive(false);
        $this->em->persist($question19);
        $this->em->flush();

        // incidentQuestion 20
        $question20 = new IncidentQuestion();
        $question20->setCompany($company);
        $question20->setLabel('Route');
        $question20->setFieldId(23);
        $question20->setFieldData(["entity" => "DriverRoute", "repoFunction" => "form_getTodayRoutes", "getter" => "", "fieldType" => "12"]);
        $question20->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question20->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question20->setIsArchive(false);
        $this->em->persist($question20);
        $this->em->flush();

        // incidentQuestion 21
        $question21 = new IncidentQuestion();
        $question21->setCompany($company);
        $question21->setLabel('Medical Attention Required?');
        $question21->setFieldId(15);
        $question21->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question21->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question21->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question21->setIsArchive(false);
        $this->em->persist($question21);
        $this->em->flush();

        // incidentQuestion 22
        $question22 = new IncidentQuestion();
        $question22->setCompany($company);
        $question22->setLabel('Third Party Injury');
        $question22->setFieldId(15);
        $question22->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question22->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question22->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question22->setIsArchive(false);
        $this->em->persist($question22);
        $this->em->flush();

        // incidentQuestion 23
        $question23 = new IncidentQuestion();
        $question23->setCompany($company);
        $question23->setLabel('Weather Conditions');
        $question23->setFieldId(15);
        $question23->setFieldData(['0' => 'Clear /Dry', '1' => 'Cloudy', '2' => 'Raining', '3' => 'Snowing', '4 ' => 'Other']);
        $question23->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question23->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question23->setIsArchive(false);
        $this->em->persist($question23);
        $this->em->flush();

        // incidentQuestion 24
        $question24 = new IncidentQuestion();
        $question24->setCompany($company);
        $question24->setLabel('Road Conditions');
        $question24->setFieldId(15);
        $question24->setFieldData(['0' => 'Clear / Dry', '1' => 'Wet', '2' => 'Icy', '3' => 'other']);
        $question24->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question24->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question24->setIsArchive(false);
        $this->em->persist($question24);
        $this->em->flush();

        // incidentQuestion 25
        $question25 = new IncidentQuestion();
        $question25->setCompany($company);
        $question25->setLabel('Personal Cell Phone Used?');
        $question25->setFieldId(15);
        $question25->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question25->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question25->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question25->setIsArchive(false);
        $this->em->persist($question25);
        $this->em->flush();

        // incidentQuestion 26
        $question26 = new IncidentQuestion();
        $question26->setCompany($company);
        $question26->setLabel('Seat belt used?');
        $question26->setFieldId(15);
        $question26->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question26->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question26->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question26->setIsArchive(false);
        $this->em->persist($question26);
        $this->em->flush();

        // incidentQuestion 27
        $question27 = new IncidentQuestion();
        $question27->setCompany($company);
        $question27->setLabel('Are there any damages');
        $question27->setFieldId(14);
        $question27->setFieldData(['0' => 'Company Delivery Vehicle', '1' => 'Third Party Vehicle', '2' => 'Property Damage', '3' => 'No Damage', '4' => 'Other']);
        $question27->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question27->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question27->setIsArchive(false);
        $this->em->persist($question27);
        $this->em->flush();

        // incidentQuestion 28

        $question28 = new IncidentQuestion();
        $question28->setCompany($company);
        $question28->setLabel('Description of Incident');
        $question28->setFieldId(2);
        $question28->setFieldData([]);
        $question28->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question28->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question28->setIsArchive(false);
        $this->em->persist($question28);
        $this->em->flush();

        // incidentQuestion 29

        $question29 = new IncidentQuestion();
        $question29->setCompany($company);
        $question29->setLabel('Reported To');
        $question29->setFieldId(14);
        $question29->setFieldData(['0' => 'HQ Dispatch', '1' => 'Station Manager/ Site Manager on site', '2' => 'Other Delivery Management']);
        $question29->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question29->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question29->setIsArchive(false);
        $this->em->persist($question29);
        $this->em->flush();

        // incidentQuestion 30

        $question30 = new IncidentQuestion();
        $question30->setCompany($company);
        $question30->setLabel('Was Delivery Associate cited');
        $question30->setFieldId(15);
        $question30->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question30->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question30->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question30->setIsArchive(false);
        $this->em->persist($question30);
        $this->em->flush();

        // incidentQuestion 31

        $question31 = new IncidentQuestion();
        $question31->setCompany($company);
        $question31->setLabel('Is their a police report');
        $question31->setFieldId(15);
        $question31->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question31->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question31->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question31->setIsArchive(false);
        $this->em->persist($question31);
        $this->em->flush();

        // incidentQuestion 32

        $question32 = new IncidentQuestion();
        $question32->setCompany($company);
        $question32->setLabel('Police Jurisdiction, Officers Name, and Report Number');
        $question32->setFieldId(1);
        $question32->setFieldData([]);
        $question32->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question32->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question32->setIsArchive(false);
        $this->em->persist($question32);
        $this->em->flush();

        // incidentQuestion 33

        $question33 = new IncidentQuestion();
        $question33->setCompany($company);
        $question33->setLabel('Was the Vehicle towed?');
        $question33->setFieldId(15);
        $question33->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question33->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question33->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question33->setIsArchive(false);
        $this->em->persist($question33);
        $this->em->flush();

        // incidentQuestion 34

        $question34 = new IncidentQuestion();
        $question34->setCompany($company);
        $question34->setLabel('Communicated with all associated parties?');
        $question34->setFieldId(15);
        $question34->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question34->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question34->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question34->setIsArchive(false);
        $this->em->persist($question34);
        $this->em->flush();
        // incidentQuestion 35

        $question35 = new IncidentQuestion();
        $question35->setCompany($company);
        $question35->setLabel('Was the driver informed that they where on a recorded line');
        $question35->setFieldId(15);
        $question35->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question35->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question35->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question35->setIsArchive(false);
        $this->em->persist($question35);
        $this->em->flush();

        // incidentQuestion 36

        $question36 = new IncidentQuestion();
        $question36->setCompany($company);
        $question36->setLabel('Was the safety and compliance office contacted');
        $question36->setFieldId(15);
        $question36->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question36->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question36->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question36->setIsArchive(false);
        $this->em->persist($question36);
        $this->em->flush();

        // incidentQuestion 37
        $question37 = new IncidentQuestion();
        $question37->setCompany($company);
        $question37->setLabel('Was documents proivded to the safety and compliance officer?');
        $question37->setFieldId(15);
        $question37->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question37->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question37->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question37->setIsArchive(false);
        $this->em->persist($question37);
        $this->em->flush();

        // incidentQuestion 38
        $question38 = new IncidentQuestion();
        $question38->setCompany($company);
        $question38->setLabel('Was the station manager contacted');
        $question38->setFieldId(15);
        $question38->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question38->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question38->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question38->setIsArchive(false);
        $this->em->persist($question38);
        $this->em->flush();

        // incidentQuestion 39

        $question39 = new IncidentQuestion();
        $question39->setCompany($company);
        $question39->setLabel('Was a rescure route setup?');
        $question39->setFieldId(15);
        $question39->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question39->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question39->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question39->setIsArchive(false);
        $this->em->persist($question39);
        $this->em->flush();

        // incidentQuestion 40

        $question40 = new IncidentQuestion();
        $question40->setCompany($company);
        $question40->setLabel('Where you needed at the scene?');
        $question40->setFieldId(15);
        $question40->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question40->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question40->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question40->setIsArchive(false);
        $this->em->persist($question40);
        $this->em->flush();

        // incidentQuestion 41

        // $question41 = new IncidentQuestion();
        // $question41->setCompany($company);
        // $question41->setLabel('Where you needed on site?');
        // $question41->setFieldId(15);
        // $question41->setFieldData(['0' => 'Yes', '1' => 'No']);
        // $question41->setFieldRule([
        //   "depend"=>["isDepend" => false,"dependOn" => null],
        //   "validations"=>["required"=>"This fields is required!!!"]
        // ]);
        // $question41->setFieldStyle(["class"=>"","style"=>"","data-attr"=>""]);
        // $question41->setIsArchive(false);
        // $this->em->persist($question41);
        // $this->em->flush();

        // incidentQuestion 42

        $question42 = new IncidentQuestion();
        $question42->setCompany($company);
        $question42->setLabel('Where you needed on site?');
        $question42->setFieldId(15);
        $question42->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question42->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question42->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question42->setIsArchive(false);
        $this->em->persist($question42);
        $this->em->flush();

        // incidentQuestion 43

        $question43 = new IncidentQuestion();
        $question43->setCompany($company);
        $question43->setLabel('Was all documents reviewed?');
        $question43->setFieldId(15);
        $question43->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question43->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question43->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question43->setIsArchive(false);
        $this->em->persist($question43);
        $this->em->flush();

        // incidentQuestion 44

        $question44 = new IncidentQuestion();
        $question44->setCompany($company);
        $question44->setLabel('Was the claim reviewed');
        $question44->setFieldId(15);
        $question44->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question44->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question44->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question44->setIsArchive(false);
        $this->em->persist($question44);
        $this->em->flush();

        // incidentQuestion 45

        $question45 = new IncidentQuestion();
        $question45->setCompany($company);
        $question45->setLabel('Where documents supplied to insurance');
        $question45->setFieldId(15);
        $question45->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question45->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question45->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question45->setIsArchive(false);
        $this->em->persist($question45);
        $this->em->flush();

        // incidentQuestion 46

        $question46 = new IncidentQuestion();
        $question46->setCompany($company);
        $question46->setLabel('Was claim sent to medical Facility?');
        $question46->setFieldId(15);
        $question46->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question46->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question46->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question46->setIsArchive(false);
        $this->em->persist($question46);
        $this->em->flush();

        // incidentQuestion 47

        $question47 = new IncidentQuestion();
        $question47->setCompany($company);
        $question47->setLabel('Was the driver contacted?');
        $question47->setFieldId(15);
        $question47->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question47->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question47->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question47->setIsArchive(false);
        $this->em->persist($question47);
        $this->em->flush();

        // incidentQuestion 48

        $question48 = new IncidentQuestion();
        $question48->setCompany($company);
        $question48->setLabel('Was a follow appointment documents submitted');
        $question48->setFieldId(15);
        $question48->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question48->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question48->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question48->setIsArchive(false);
        $this->em->persist($question48);
        $this->em->flush();

        // incidentQuestion 49

        $question49 = new IncidentQuestion();
        $question49->setCompany($company);
        $question49->setLabel('Was the vehicle claim processed');
        $question49->setFieldId(15);
        $question49->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question49->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question49->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question49->setIsArchive(false);
        $this->em->persist($question49);
        $this->em->flush();

        // incidentQuestion 50

        $question50 = new IncidentQuestion();
        $question50->setCompany($company);
        $question50->setLabel('Was there any property damage processed?');
        $question50->setFieldId(15);
        $question50->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question50->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question50->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question50->setIsArchive(false);
        $this->em->persist($question50);
        $this->em->flush();

        // incidentQuestion 51

        $question51 = new IncidentQuestion();
        $question51->setCompany($company);
        $question51->setLabel('Was the repair shop contacted?');
        $question51->setFieldId(15);
        $question51->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question51->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question51->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question51->setIsArchive(false);
        $this->em->persist($question51);
        $this->em->flush();

        // incidentQuestion 52

        $question52 = new IncidentQuestion();
        $question52->setCompany($company);
        $question52->setLabel('Was driver contacted after incident?');
        $question52->setFieldId(15);
        $question52->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question52->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question52->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question52->setIsArchive(false);
        $this->em->persist($question52);
        $this->em->flush();

        // incidentQuestion 53

        $question53 = new IncidentQuestion();
        $question53->setCompany($company);
        $question53->setLabel('Was a estimate provided?');
        $question53->setFieldId(15);
        $question53->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question53->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question53->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question53->setIsArchive(false);
        $this->em->persist($question53);
        $this->em->flush();

        // incidentQuestion 54

        $question54 = new IncidentQuestion();
        $question54->setCompany($company);
        $question54->setLabel('Was the service timely?');
        $question54->setFieldId(15);
        $question54->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question54->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question54->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question54->setIsArchive(false);
        $this->em->persist($question54);
        $this->em->flush();

        // incidentQuestion 55

        $question55 = new IncidentQuestion();
        $question55->setCompany($company);
        $question55->setLabel('Claim #');
        $question55->setFieldId(1);
        $question55->setFieldData([]);
        $question55->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question55->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question55->setIsArchive(false);
        $this->em->persist($question55);
        $this->em->flush();

        // incidentQuestion 56

        $question56 = new IncidentQuestion();
        $question56->setCompany($company);
        $question56->setLabel('Was a estimate provided?');
        $question56->setFieldId(15);
        $question56->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question56->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question56->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question56->setIsArchive(false);
        $this->em->persist($question56);
        $this->em->flush();

        // incidentQuestion 57

        $question57 = new IncidentQuestion();
        $question57->setCompany($company);
        $question57->setLabel('Was the an adjuster assigned to the case?');
        $question57->setFieldId(15);
        $question57->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question57->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question57->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question57->setIsArchive(false);
        $this->em->persist($question57);
        $this->em->flush();

        // incidentQuestion 58

        $question58 = new IncidentQuestion();
        $question58->setCompany($company);
        $question58->setLabel('Adjuster Name');
        $question58->setFieldId(1);
        $question58->setFieldData([]);
        $question58->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question58->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question58->setIsArchive(false);
        $this->em->persist($question58);
        $this->em->flush();

        // incidentQuestion 59

        $question59 = new IncidentQuestion();
        $question59->setCompany($company);
        $question59->setLabel('Was an investigation nessary');
        $question59->setFieldId(15);
        $question59->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question59->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question59->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question59->setIsArchive(false);
        $this->em->persist($question59);
        $this->em->flush();

        // incidentQuestion 60

        $question60 = new IncidentQuestion();
        $question60->setCompany($company);
        $question60->setLabel('Was this incident an out of pocket expense?');
        $question60->setFieldId(15);
        $question60->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question60->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question60->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question60->setIsArchive(false);
        $this->em->persist($question60);
        $this->em->flush();

        // incidentQuestion 61

        $question61 = new IncidentQuestion();
        $question61->setCompany($company);
        $question61->setLabel('Was the incident reviewed?');
        $question61->setFieldId(15);
        $question61->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question61->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question61->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question61->setIsArchive(false);
        $this->em->persist($question61);
        $this->em->flush();

        // incidentQuestion 62

        $question62 = new IncidentQuestion();
        $question62->setCompany($company);
        $question62->setLabel('Incident Resolved?');
        $question62->setFieldId(15);
        $question62->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question62->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question62->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question62->setIsArchive(false);
        $this->em->persist($question62);
        $this->em->flush();

        // incidentQuestion 63

        $question63 = new IncidentQuestion();
        $question63->setCompany($company);
        $question63->setLabel('');
        $question63->setFieldId(23);
        $question63->setFieldData(["entity" => "Driver", "repoFunction" => "form_getDrivers", "fieldType" => "12"]);
        $question63->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question63->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question63->setIsArchive(false);
        $this->em->persist($question63);
        $this->em->flush();

        // incidentQuestion 64

        $question64 = new IncidentQuestion();
        $question64->setCompany($company);
        $question64->setLabel('');
        $question64->setFieldId(15);
        $question64->setFieldData(['0' => 'Sick', '1' => 'Injury', '2' => 'Pets', '3' => 'Vehicle Issue', '4' => 'Family Emergency', '5' => 'Overslept', '6' => 'Other', '7' => 'Quit']);
        $question64->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question64->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question64->setIsArchive(false);
        $this->em->persist($question64);
        $this->em->flush();

        $question64a = new IncidentQuestion();
        $question64a->setCompany($company);
        $question64a->setLabel('');
        $question64a->setFieldId(2);
        $question64a->setFieldData([]);
        $question64a->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question64a->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question64a->setIsArchive(false);
        $this->em->persist($question64a);
        $this->em->flush();

        // incidentQuestion 65

        $question65 = new IncidentQuestion();
        $question65->setCompany($company);
        $question65->setLabel('Assigned To');
        $question65->setFieldId(23);
        $question65->setFieldData(["entity" => "Driver", "repoFunction" => "form_getDrivers", "fieldType" => "12"]);
        $question65->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => false],
        ]);
        $question65->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question65->setIsArchive(false);
        $this->em->persist($question65);
        $this->em->flush();

        // incidentQuestion 66

        $question66 = new IncidentQuestion();
        $question66->setCompany($company);
        $question66->setLabel('Reviewed By');
        $question66->setFieldId(25);
        $question66->setFieldData([]);
        $question66->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => false],
        ]);
        $question66->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question66->setIsArchive(false);
        $this->em->persist($question66);
        $this->em->flush();

        // incidentQuestion 67

        $question67 = new IncidentQuestion();
        $question67->setCompany($company);
        $question67->setLabel('');
        $question67->setFieldId(15);
        $question67->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question67->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question67->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question67->setIsArchive(false);
        $this->em->persist($question67);
        $this->em->flush();

        // incidentQuestion 68

        $question68 = new IncidentQuestion();
        $question68->setCompany($company);
        $question68->setLabel('Documents');
        $question68->setFieldId(21);
        $question68->setFieldData([]);
        $question68->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => false],
        ]);
        $question68->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question68->setIsArchive(false);
        $this->em->persist($question68);
        $this->em->flush();

        // incidentQuestion 69

        $question69 = new IncidentQuestion();
        $question69->setCompany($company);
        $question69->setLabel('');
        $question69->setFieldId(15);
        $question69->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question69->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question69->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question69->setIsArchive(false);
        $this->em->persist($question69);
        $this->em->flush();

        // incidentQuestion 70

        $question70 = new IncidentQuestion();
        $question70->setCompany($company);
        $question70->setLabel('');
        $question70->setFieldId(2);
        $question70->setFieldData([]);
        $question70->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => false],
        ]);
        $question70->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question70->setIsArchive(false);
        $this->em->persist($question70);
        $this->em->flush();

        // incidentQuestion 71

        $question71 = new IncidentQuestion();
        $question71->setCompany($company);
        $question71->setLabel('Revied By');
        $question71->setFieldId(25);
        $question71->setFieldData([]);
        $question71->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => false],
        ]);
        $question71->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question71->setIsArchive(false);
        $this->em->persist($question71);
        $this->em->flush();

        // incidentQuestion 72

        $question72 = new IncidentQuestion();
        $question72->setCompany($company);
        $question72->setLabel('Select Driver');
        $question72->setFieldId(23);
        $question72->setFieldData(["entity" => "Driver", "repoFunction" => "form_getDrivers", "fieldType" => "12"]);
        $question72->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question72->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question72->setIsArchive(false);
        $this->em->persist($question72);
        $this->em->flush();

        // incidentQuestion 73

        $question73 = new IncidentQuestion();
        $question73->setCompany($company);
        $question73->setLabel('Select Station');
        $question73->setFieldId(23);
        $question73->setFieldData(["entity" => "Station", "repoFunction" => "form_getStations", "fieldType" => "12"]);
        $question73->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question73->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question73->setIsArchive(false);
        $this->em->persist($question73);
        $this->em->flush();

        // incidentQuestion 74

        $question74 = new IncidentQuestion();
        $question74->setCompany($company);
        $question74->setLabel('Select Shift');
        $question74->setFieldId(23);
        $question74->setFieldData(["entity" => "Shift", "repoFunction" => "form_getShifts", "fieldType" => "12"]);
        $question74->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question74->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question74->setIsArchive(false);
        $this->em->persist($question74);
        $this->em->flush();

        // incidentQuestion 75

        $question75 = new IncidentQuestion();
        $question75->setCompany($company);
        $question75->setLabel('');
        $question75->setFieldId(15);
        $question75->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question75->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question75->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question75->setIsArchive(false);
        $this->em->persist($question75);
        $this->em->flush();

        // incidentQuestion 76

        $question76 = new IncidentQuestion();
        $question76->setCompany($company);
        $question76->setLabel('Select Shift Time');
        $question76->setFieldId(15);
        $question76->setFieldData(['0' => 'Weekday', '1' => 'Weekend']);
        $question76->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question76->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question76->setIsArchive(false);
        $this->em->persist($question76);
        $this->em->flush();

        // incidentQuestion 77

        $question77 = new IncidentQuestion();
        $question77->setCompany($company);
        $question77->setLabel('');
        $question77->setFieldId(14);
        $question77->setFieldData(['0' => 'Call', '1' => 'Text', '2' => 'Email', '3' => 'No contacted']);
        $question77->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question77->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question77->setIsArchive(false);
        $this->em->persist($question77);
        $this->em->flush();

        // incidentQuestion 78

        $question78 = new IncidentQuestion();
        $question78->setCompany($company);
        $question78->setLabel('');
        $question78->setFieldId(15);
        $question78->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question78->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question78->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question78->setIsArchive(false);
        $this->em->persist($question78);
        $this->em->flush();

        // incidentQuestion 79

        $question79 = new IncidentQuestion();
        $question79->setCompany($company);
        $question79->setLabel('Documents');
        $question79->setFieldId(21);
        $question79->setFieldData([]);
        $question79->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => false],
        ]);
        $question79->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question79->setIsArchive(false);
        $this->em->persist($question79);
        $this->em->flush();

        // incidentQuestion 80

        $question80 = new IncidentQuestion();
        $question80->setCompany($company);
        $question80->setLabel('');
        $question80->setFieldId(15);
        $question80->setFieldData(['0' => '0', '1' => '1', '2' => '2', '3' => '3']);
        $question80->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question80->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question80->setIsArchive(false);
        $this->em->persist($question80);
        $this->em->flush();

        // incidentQuestion 81

        $question81 = new IncidentQuestion();
        $question81->setCompany($company);
        $question81->setLabel('');
        $question81->setFieldId(15);
        $question81->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question81->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question81->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question81->setIsArchive(false);
        $this->em->persist($question81);
        $this->em->flush();

        // incidentQuestion 82

        $question82 = new IncidentQuestion();
        $question82->setCompany($company);
        $question82->setLabel('Revied By');
        $question82->setFieldId(25);
        $question82->setFieldData([]);
        $question82->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => false],
        ]);
        $question82->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question82->setIsArchive(false);
        $this->em->persist($question82);
        $this->em->flush();

        // incidentQuestion 83

        $question83 = new IncidentQuestion();
        $question83->setCompany($company);
        $question83->setLabel('');
        $question83->setFieldId(15);
        $question83->setFieldData(['0' => 'Call', '1' => 'Text', '2' => 'Email', '3' => 'No call']);
        $question83->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question83->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question83->setIsArchive(false);
        $this->em->persist($question83);
        $this->em->flush();

        // incidentQuestion 84

        $question84 = new IncidentQuestion();
        $question84->setCompany($company);
        $question84->setLabel('');
        $question84->setFieldId(15);
        $question84->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question84->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question84->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question84->setIsArchive(false);
        $this->em->persist($question84);
        $this->em->flush();

        // incidentQuestion 85

        $question85 = new IncidentQuestion();
        $question85->setCompany($company);
        $question85->setLabel('');
        $question85->setFieldId(15);
        $question85->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question85->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question85->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question85->setIsArchive(false);
        $this->em->persist($question85);
        $this->em->flush();

        // incidentQuestion 86

        $question86 = new IncidentQuestion();
        $question86->setCompany($company);
        $question86->setLabel('Documents');
        $question86->setFieldId(21);
        $question86->setFieldData([]);
        $question86->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question86->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question86->setIsArchive(false);
        $this->em->persist($question86);
        $this->em->flush();

        // incidentQuestion 87

        $question87 = new IncidentQuestion();
        $question87->setCompany($company);
        $question87->setLabel('');
        $question87->setFieldId(15);
        $question87->setFieldData(['0' => 'Yes', '1' => 'No']);
        $question87->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question87->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question87->setIsArchive(false);
        $this->em->persist($question87);
        $this->em->flush();

        // incidentQuestion 88

        $question88 = new IncidentQuestion();
        $question88->setCompany($company);
        $question88->setLabel('Reviewed By');
        $question88->setFieldId(25);
        $question88->setFieldData([]);
        $question88->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $question88->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question88->setIsArchive(false);
        $this->em->persist($question88);
        $this->em->flush();

        // incidentQuestion 89

        $question89 = new IncidentQuestion();
        $question89->setCompany($company);
        $question89->setLabel('Reviewed By');
        $question89->setFieldId(25);
        $question89->setFieldData([]);
        $question89->setFieldRule([
            "depend" => ["isDepend" => false, "dependOn" => null],
            "validations" => ["required" => false],
        ]);
        $question89->setFieldStyle(["class" => "", "style" => "", "data-attr" => ""]);
        $question89->setIsArchive(false);
        $this->em->persist($question89);
        $this->em->flush();

        //Incident Type Add
        // $incidentType1 = new IncidentType();
        // $incidentType1->setName('Accident Injury');
        // $incidentType1->setCompany($company);
        // $incidentType1->setPrefix('Acc');
        // $this->em->persist($incidentType1);
        // $this->em->flush();
        //Incident Phase Add
        //         $incidentPhase1 = new IncidentPhase();
        //         $incidentPhase1->setTitle('Details');
        //         $incidentPhase1->setOrdering('1');
        //         $incidentPhase1->setRole($company->getCompanyRoles()[4]);
        //         $incidentPhase1->setIncidentType($incidentType1);
        //         $incidentPhase1->setCreatedDate($currentDateTime);
        //         $incidentPhase1->setIsInApp(true);
        //         $incidentPhase1->setIsInWeb(true);
        //         $incidentPhase1->setAllocatedTime(0);
        //         $this->em->persist($incidentPhase1);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 0
        //         $incidentPhase1Step0 = new IncidentStep();
        //         $incidentPhase1Step0->setPhase($incidentPhase1);
        //         $incidentPhase1Step0->setTitle('');
        //         $incidentPhase1Step0->setInstructions('');
        //         $incidentPhase1Step0->setStepNumber(0);
        //         $incidentPhase1Step0->setOrdering(0);
        //         $this->em->persist($incidentPhase1Step0);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step0FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step0FormField1->setOrdering(1);
        //         $incidentPhase1Step0FormField1->setLabel('');
        //         $incidentPhase1Step0FormField1->setFieldId(null);
        //         $incidentPhase1Step0FormField1->setFieldData([]);
        //         $incidentPhase1Step0FormField1->setFieldRule([]);
        //         $incidentPhase1Step0FormField1->setFieldStyle([]);
        //         $incidentPhase1Step0FormField1->setIncidentQuestion($question1);
        //         $incidentPhase1Step0FormField1->setCreatedDate($currentDateTime);
        //         $incidentPhase1Step0FormField1->setStep($incidentPhase1Step0);
        //         $incidentPhase1->setIsInApp(true);
        //         $incidentPhase1->setIsInWeb(false);
        //         $this->em->persist($incidentPhase1Step0FormField1);
        //         $this->em->flush();
        //
        //         $incidentPhase1Step0FormField2 = new IncidentFormFields();
        //         $incidentPhase1Step0FormField2->setOrdering(2);
        //         $incidentPhase1Step0FormField2->setLabel('');
        //         $incidentPhase1Step0FormField2->setFieldId(null);
        //         $incidentPhase1Step0FormField2->setFieldData([]);
        //         $incidentPhase1Step0FormField2->setFieldRule([]);
        //         $incidentPhase1Step0FormField2->setFieldStyle([]);
        //         $incidentPhase1Step0FormField2->setIncidentQuestion($question2);
        //         $incidentPhase1Step0FormField2->setCreatedDate($currentDateTime);
        //         $incidentPhase1Step0FormField2->setStep($incidentPhase1Step0);
        //         $incidentPhase1->setIsInApp(false);
        //         $incidentPhase1->setIsInWeb(true);
        //         $this->em->persist($incidentPhase1Step0FormField2);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 1
        //         $incidentPhase1Step1 = new IncidentStep();
        //         $incidentPhase1Step1->setPhase($incidentPhase1);
        //         $incidentPhase1Step1->setTitle('Driver Call 911');
        //         $incidentPhase1Step1->setInstructions('Call 911 for Auto Accidents Skip this step If Authorities are NOT needed');
        //         $incidentPhase1Step1->setStepNumber(1);
        //         $incidentPhase1Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase1Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step1FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step1FormField1->setOrdering(1);
        //         $incidentPhase1Step1FormField1->setLabel('');
        //         $incidentPhase1Step1FormField1->setFieldId(null);
        //         $incidentPhase1Step1FormField1->setFieldData([]);
        //         $incidentPhase1Step1FormField1->setFieldRule([]);
        //         $incidentPhase1Step1FormField1->setFieldStyle([]);
        //         $incidentPhase1Step1FormField1->setIncidentQuestion($question12);
        //         $incidentPhase1Step1FormField1->setCreatedDate($currentDateTime);
        //         $incidentPhase1Step1FormField1->setStep($incidentPhase1Step1);
        //         $this->em->persist($incidentPhase1Step1FormField1);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 2
        //         $incidentPhase1Step2 = new IncidentStep();
        //         $incidentPhase1Step2->setPhase($incidentPhase1);
        //         $incidentPhase1Step2->setTitle('Call Central Dispatch');
        //         $incidentPhase1Step2->setInstructions('report the entire Incident / Accident Central Dispatch East: 515.650.8125 Central Dispatch West: 715.942.7813');
        //         $incidentPhase1Step2->setStepNumber(2);
        //         $incidentPhase1Step2->setOrdering(2);
        //         $this->em->persist($incidentPhase1Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step2FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step2FormField1->setOrdering(1);
        //         $incidentPhase1Step2FormField1->setLabel('');
        //         $incidentPhase1Step2FormField1->setFieldId(null);
        //         $incidentPhase1Step2FormField1->setFieldData([]);
        //         $incidentPhase1Step2FormField1->setFieldRule([]);
        //         $incidentPhase1Step2FormField1->setFieldStyle([]);
        //         $incidentPhase1Step2FormField1->setIncidentQuestion($question3);
        //         $incidentPhase1Step2FormField1->setCreatedDate($currentDateTime);
        //         $incidentPhase1Step2FormField1->setStep($incidentPhase1Step2);
        //         $this->em->persist($incidentPhase1Step2FormField1);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 3
        //         $incidentPhase1Step3 = new IncidentStep();
        //         $incidentPhase1Step3->setPhase($incidentPhase1);
        //         $incidentPhase1Step3->setTitle('Incident Information');
        //         $incidentPhase1Step3->setInstructions('');
        //         $incidentPhase1Step3->setStepNumber(3);
        //         $incidentPhase1Step3->setOrdering(3);
        //         $this->em->persist($incidentPhase1Step3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step3FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step3FormField1->setOrdering(1);
        //         $incidentPhase1Step3FormField1->setLabel('');
        //         $incidentPhase1Step3FormField1->setFieldId(null);
        //         $incidentPhase1Step3FormField1->setFieldData([]);
        //         $incidentPhase1Step3FormField1->setFieldRule([]);
        //         $incidentPhase1Step3FormField1->setFieldStyle([]);
        //         $incidentPhase1Step3FormField1->setIncidentQuestion($question4);
        //         $incidentPhase1Step3FormField1->setStep($incidentPhase1Step3);
        //         $incidentPhase1Step3FormField1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step3FormField1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step3FormField2 = new IncidentFormFields();
        //         $incidentPhase1Step3FormField2->setOrdering(2);
        //         $incidentPhase1Step3FormField2->setLabel('');
        //         $incidentPhase1Step3FormField2->setFieldId(null);
        //         $incidentPhase1Step3FormField2->setFieldData([]);
        //         $incidentPhase1Step3FormField2->setFieldRule([]);
        //         $incidentPhase1Step3FormField2->setFieldStyle([]);
        //         $incidentPhase1Step3FormField2->setIncidentQuestion($question5);
        //         $incidentPhase1Step3FormField2->setStep($incidentPhase1Step3);
        //         $incidentPhase1Step3FormField2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step3FormField2);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 4
        //         $incidentPhase1Step4 = new IncidentStep();
        //         $incidentPhase1Step4->setPhase($incidentPhase1);
        //         $incidentPhase1Step4->setTitle('Captures pictures of Damages, Scene, Property Etc.');
        //         $incidentPhase1Step4->setInstructions('For Automobile accidents please take pictures of Damages, the Odometer, and the four corners of the van, and inside the cab.');
        //         $incidentPhase1Step4->setStepNumber(4);
        //         $incidentPhase1Step4->setOrdering(4);
        //         $this->em->persist($incidentPhase1Step4);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField1->setOrdering(1);
        //         $incidentPhase1Step4FormField1->setLabel('');
        //         $incidentPhase1Step4FormField1->setFieldId(null);
        //         $incidentPhase1Step4FormField1->setFieldData([]);
        //         $incidentPhase1Step4FormField1->setFieldRule([]);
        //         $incidentPhase1Step4FormField1->setFieldStyle([]);
        //         $incidentPhase1Step4FormField1->setIncidentQuestion($question6);
        //         $incidentPhase1Step4FormField1->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField2 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField2->setOrdering(2);
        //         $incidentPhase1Step4FormField2->setLabel('');
        //         $incidentPhase1Step4FormField2->setFieldId(null);
        //         $incidentPhase1Step4FormField2->setFieldData([]);
        //         $incidentPhase1Step4FormField2->setFieldRule([]);
        //         $incidentPhase1Step4FormField2->setFieldStyle([]);
        //         $incidentPhase1Step4FormField2->setIncidentQuestion($question7);
        //         $incidentPhase1Step4FormField2->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField3 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField3->setOrdering(3);
        //         $incidentPhase1Step4FormField3->setLabel('');
        //         $incidentPhase1Step4FormField3->setFieldId(null);
        //         $incidentPhase1Step4FormField3->setFieldData([]);
        //         $incidentPhase1Step4FormField3->setFieldRule([]);
        //         $incidentPhase1Step4FormField3->setFieldStyle([]);
        //         $incidentPhase1Step4FormField3->setIncidentQuestion($question8);
        //         $incidentPhase1Step4FormField3->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField3->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField4 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField4->setOrdering(4);
        //         $incidentPhase1Step4FormField4->setLabel('');
        //         $incidentPhase1Step4FormField4->setFieldId(null);
        //         $incidentPhase1Step4FormField4->setFieldData([]);
        //         $incidentPhase1Step4FormField4->setFieldRule([]);
        //         $incidentPhase1Step4FormField4->setFieldStyle([]);
        //         $incidentPhase1Step4FormField4->setIncidentQuestion($question9);
        //         $incidentPhase1Step4FormField4->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField4->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField4);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField5 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField5->setOrdering(5);
        //         $incidentPhase1Step4FormField5->setLabel('');
        //         $incidentPhase1Step4FormField5->setFieldId(null);
        //         $incidentPhase1Step4FormField5->setFieldData([]);
        //         $incidentPhase1Step4FormField5->setFieldRule([]);
        //         $incidentPhase1Step4FormField5->setFieldStyle([]);
        //         $incidentPhase1Step4FormField5->setIncidentQuestion($question10);
        //         $incidentPhase1Step4FormField5->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField5->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField5);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step4FormField6 = new IncidentFormFields();
        //         $incidentPhase1Step4FormField6->setOrdering(6);
        //         $incidentPhase1Step4FormField6->setLabel('');
        //         $incidentPhase1Step4FormField6->setFieldId(null);
        //         $incidentPhase1Step4FormField6->setFieldData([]);
        //         $incidentPhase1Step4FormField6->setFieldRule([]);
        //         $incidentPhase1Step4FormField6->setFieldStyle([]);
        //         $incidentPhase1Step4FormField6->setIncidentQuestion($question11);
        //         $incidentPhase1Step4FormField6->setStep($incidentPhase1Step4);
        //         $incidentPhase1Step4FormField6->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step4FormField6);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 5
        //         $incidentPhase1Step5 = new IncidentStep();
        //         $incidentPhase1Step5->setPhase($incidentPhase1);
        //         $incidentPhase1Step5->setTitle('Record the Other party information');
        //         $incidentPhase1Step5->setInstructions('Gather photo damage of the party involved.');
        //         $incidentPhase1Step5->setStepNumber(5);
        //         $incidentPhase1Step5->setOrdering(5);
        //         $this->em->persist($incidentPhase1Step5);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step5FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step5FormField1->setOrdering(1);
        //         $incidentPhase1Step5FormField1->setLabel('');
        //         $incidentPhase1Step5FormField1->setFieldId(null);
        //         $incidentPhase1Step5FormField1->setFieldData([]);
        //         $incidentPhase1Step5FormField1->setFieldRule([]);
        //         $incidentPhase1Step5FormField1->setFieldStyle([]);
        //         $incidentPhase1Step5FormField1->setIncidentQuestion($question13);
        //         $incidentPhase1Step5FormField1->setStep($incidentPhase1Step5);
        //         $incidentPhase1Step5FormField1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step5FormField1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step5FormField2 = new IncidentFormFields();
        //         $incidentPhase1Step5FormField2->setOrdering(2);
        //         $incidentPhase1Step5FormField2->setLabel('');
        //         $incidentPhase1Step5FormField2->setFieldId(null);
        //         $incidentPhase1Step5FormField2->setFieldData([]);
        //         $incidentPhase1Step5FormField2->setFieldRule([]);
        //         $incidentPhase1Step5FormField2->setFieldStyle([]);
        //         $incidentPhase1Step5FormField2->setIncidentQuestion($question14);
        //         $incidentPhase1Step5FormField2->setStep($incidentPhase1Step5);
        //         $incidentPhase1Step5FormField2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step5FormField2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step5FormField3 = new IncidentFormFields();
        //         $incidentPhase1Step5FormField3->setOrdering(3);
        //         $incidentPhase1Step5FormField3->setLabel('');
        //         $incidentPhase1Step5FormField3->setFieldId(null);
        //         $incidentPhase1Step5FormField3->setFieldData([]);
        //         $incidentPhase1Step5FormField3->setFieldRule([]);
        //         $incidentPhase1Step5FormField3->setFieldStyle([]);
        //         $incidentPhase1Step5FormField3->setIncidentQuestion($question15);
        //         $incidentPhase1Step5FormField3->setStep($incidentPhase1Step5);
        //         $incidentPhase1Step5FormField3->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step5FormField3);
        //         $this->em->flush();
        //
        //         //Incident Phase 1 Steps 6
        //         $incidentPhase1Step6 = new IncidentStep();
        //         $incidentPhase1Step6->setPhase($incidentPhase1);
        //         $incidentPhase1Step6->setTitle('Incident Information');
        //         $incidentPhase1Step6->setInstructions('Provide details of incident including location.');
        //         $incidentPhase1Step6->setStepNumber(6);
        //         $incidentPhase1Step6->setOrdering(6);
        //         $this->em->persist($incidentPhase1Step6);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField1 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField1->setOrdering(1);
        //         $incidentPhase1Step6FormField1->setLabel('');
        //         $incidentPhase1Step6FormField1->setFieldId(null);
        //         $incidentPhase1Step6FormField1->setFieldData([]);
        //         $incidentPhase1Step6FormField1->setFieldRule([]);
        //         $incidentPhase1Step6FormField1->setFieldStyle([]);
        //         $incidentPhase1Step6FormField1->setIncidentQuestion($question16);
        //         $incidentPhase1Step6FormField1->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField2 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField2->setOrdering(2);
        //         $incidentPhase1Step6FormField2->setLabel('');
        //         $incidentPhase1Step6FormField2->setFieldId(null);
        //         $incidentPhase1Step6FormField2->setFieldData([]);
        //         $incidentPhase1Step6FormField2->setFieldRule([]);
        //         $incidentPhase1Step6FormField2->setFieldStyle([]);
        //         $incidentPhase1Step6FormField2->setIncidentQuestion($question17);
        //         $incidentPhase1Step6FormField2->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField3 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField3->setOrdering(3);
        //         $incidentPhase1Step6FormField3->setLabel('');
        //         $incidentPhase1Step6FormField3->setFieldId(null);
        //         $incidentPhase1Step6FormField3->setFieldData([]);
        //         $incidentPhase1Step6FormField3->setFieldRule([]);
        //         $incidentPhase1Step6FormField3->setFieldStyle([]);
        //         $incidentPhase1Step6FormField3->setIncidentQuestion($question18);
        //         $incidentPhase1Step6FormField3->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField3->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField4 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField4->setOrdering(4);
        //         $incidentPhase1Step6FormField4->setLabel('');
        //         $incidentPhase1Step6FormField4->setFieldId(null);
        //         $incidentPhase1Step6FormField4->setFieldData([]);
        //         $incidentPhase1Step6FormField4->setFieldRule([]);
        //         $incidentPhase1Step6FormField4->setFieldStyle([]);
        //         $incidentPhase1Step6FormField4->setIncidentQuestion($question19);
        //         $incidentPhase1Step6FormField4->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField4->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField4);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField5 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField5->setOrdering(5);
        //         $incidentPhase1Step6FormField5->setLabel('');
        //         $incidentPhase1Step6FormField5->setFieldId(null);
        //         $incidentPhase1Step6FormField5->setFieldData([]);
        //         $incidentPhase1Step6FormField5->setFieldRule([]);
        //         $incidentPhase1Step6FormField5->setFieldStyle([]);
        //         $incidentPhase1Step6FormField5->setIncidentQuestion($question20);
        //         $incidentPhase1Step6FormField5->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField5->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField5);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField6 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField6->setOrdering(6);
        //         $incidentPhase1Step6FormField6->setLabel('');
        //         $incidentPhase1Step6FormField6->setFieldId(null);
        //         $incidentPhase1Step6FormField6->setFieldData([]);
        //         $incidentPhase1Step6FormField6->setFieldRule([]);
        //         $incidentPhase1Step6FormField6->setFieldStyle([]);
        //         $incidentPhase1Step6FormField6->setIncidentQuestion($question21);
        //         $incidentPhase1Step6FormField6->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField6->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField6);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField7 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField7->setOrdering(7);
        //         $incidentPhase1Step6FormField7->setLabel('');
        //         $incidentPhase1Step6FormField7->setFieldId(null);
        //         $incidentPhase1Step6FormField7->setFieldData([]);
        //         $incidentPhase1Step6FormField7->setFieldRule([]);
        //         $incidentPhase1Step6FormField7->setFieldStyle([]);
        //         $incidentPhase1Step6FormField7->setIncidentQuestion($question22);
        //         $incidentPhase1Step6FormField7->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField7->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField7);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField8 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField8->setOrdering(8);
        //         $incidentPhase1Step6FormField8->setLabel('');
        //         $incidentPhase1Step6FormField8->setFieldId(null);
        //         $incidentPhase1Step6FormField8->setFieldData([]);
        //         $incidentPhase1Step6FormField8->setFieldRule([]);
        //         $incidentPhase1Step6FormField8->setFieldStyle([]);
        //         $incidentPhase1Step6FormField8->setIncidentQuestion($question23);
        //         $incidentPhase1Step6FormField8->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField8->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField8);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField9 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField9->setOrdering(9);
        //         $incidentPhase1Step6FormField9->setLabel('');
        //         $incidentPhase1Step6FormField9->setFieldId(null);
        //         $incidentPhase1Step6FormField9->setFieldData([]);
        //         $incidentPhase1Step6FormField9->setFieldRule([]);
        //         $incidentPhase1Step6FormField9->setFieldStyle([]);
        //         $incidentPhase1Step6FormField9->setIncidentQuestion($question24);
        //         $incidentPhase1Step6FormField9->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField9->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField9);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField10 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField10->setOrdering(10);
        //         $incidentPhase1Step6FormField10->setLabel('');
        //         $incidentPhase1Step6FormField10->setFieldId(null);
        //         $incidentPhase1Step6FormField10->setFieldData([]);
        //         $incidentPhase1Step6FormField10->setFieldRule([]);
        //         $incidentPhase1Step6FormField10->setFieldStyle([]);
        //         $incidentPhase1Step6FormField10->setIncidentQuestion($question25);
        //         $incidentPhase1Step6FormField10->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField10->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField10);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField11 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField11->setOrdering(11);
        //         $incidentPhase1Step6FormField11->setLabel('');
        //         $incidentPhase1Step6FormField11->setFieldId(null);
        //         $incidentPhase1Step6FormField11->setFieldData([]);
        //         $incidentPhase1Step6FormField11->setFieldRule([]);
        //         $incidentPhase1Step6FormField11->setFieldStyle([]);
        //         $incidentPhase1Step6FormField11->setIncidentQuestion($question26);
        //         $incidentPhase1Step6FormField11->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField11->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField11);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField12 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField12->setOrdering(12);
        //         $incidentPhase1Step6FormField12->setLabel('');
        //         $incidentPhase1Step6FormField12->setFieldId(null);
        //         $incidentPhase1Step6FormField12->setFieldData([]);
        //         $incidentPhase1Step6FormField12->setFieldRule([]);
        //         $incidentPhase1Step6FormField12->setFieldStyle([]);
        //         $incidentPhase1Step6FormField12->setIncidentQuestion($question27);
        //         $incidentPhase1Step6FormField12->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField12->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField12);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField13 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField13->setOrdering(13);
        //         $incidentPhase1Step6FormField13->setLabel('');
        //         $incidentPhase1Step6FormField13->setFieldId(null);
        //         $incidentPhase1Step6FormField13->setFieldData([]);
        //         $incidentPhase1Step6FormField13->setFieldRule([]);
        //         $incidentPhase1Step6FormField13->setFieldStyle([]);
        //         $incidentPhase1Step6FormField13->setIncidentQuestion($question28);
        //         $incidentPhase1Step6FormField13->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField13->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField13);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField14 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField14->setOrdering(14);
        //         $incidentPhase1Step6FormField14->setLabel('');
        //         $incidentPhase1Step6FormField14->setFieldId(null);
        //         $incidentPhase1Step6FormField14->setFieldData([]);
        //         $incidentPhase1Step6FormField14->setFieldRule([]);
        //         $incidentPhase1Step6FormField14->setFieldStyle([]);
        //         $incidentPhase1Step6FormField14->setIncidentQuestion($question29);
        //         $incidentPhase1Step6FormField14->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField14->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField14);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField15 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField15->setOrdering(15);
        //         $incidentPhase1Step6FormField15->setLabel('');
        //         $incidentPhase1Step6FormField15->setFieldId(null);
        //         $incidentPhase1Step6FormField15->setFieldData([]);
        //         $incidentPhase1Step6FormField15->setFieldRule([]);
        //         $incidentPhase1Step6FormField15->setFieldStyle([]);
        //         $incidentPhase1Step6FormField15->setIncidentQuestion($question30);
        //         $incidentPhase1Step6FormField15->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField15->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField15);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField16 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField16->setOrdering(16);
        //         $incidentPhase1Step6FormField16->setLabel('');
        //         $incidentPhase1Step6FormField16->setFieldId(null);
        //         $incidentPhase1Step6FormField16->setFieldData([]);
        //         $incidentPhase1Step6FormField16->setFieldRule([]);
        //         $incidentPhase1Step6FormField16->setFieldStyle([]);
        //         $incidentPhase1Step6FormField16->setIncidentQuestion($question31);
        //         $incidentPhase1Step6FormField16->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField16->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField16);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField17 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField17->setOrdering(17);
        //         $incidentPhase1Step6FormField17->setLabel('');
        //         $incidentPhase1Step6FormField17->setFieldId(null);
        //         $incidentPhase1Step6FormField17->setFieldData([]);
        //         $incidentPhase1Step6FormField17->setFieldRule([]);
        //         $incidentPhase1Step6FormField17->setFieldStyle([]);
        //         $incidentPhase1Step6FormField17->setIncidentQuestion($question32);
        //         $incidentPhase1Step6FormField17->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField17->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField17);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase1Step6FormField18 = new IncidentFormFields();
        //         $incidentPhase1Step6FormField18->setOrdering(18);
        //         $incidentPhase1Step6FormField18->setLabel('');
        //         $incidentPhase1Step6FormField18->setFieldId(null);
        //         $incidentPhase1Step6FormField18->setFieldData([]);
        //         $incidentPhase1Step6FormField18->setFieldRule([]);
        //         $incidentPhase1Step6FormField18->setFieldStyle([]);
        //         $incidentPhase1Step6FormField18->setIncidentQuestion($question33);
        //         $incidentPhase1Step6FormField18->setStep($incidentPhase1Step6);
        //         $incidentPhase1Step6FormField18->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase1Step6FormField18);
        //         $this->em->flush();
        //
        //         //Incident Phase2  Add
        //         $incidentPhase2 = new IncidentPhase();
        //         $incidentPhase2->setTitle('Driver Safety');
        //         $incidentPhase2->setOrdering('2');
        //         // $incidentPhase2->setCompany($company);
        //         $incidentPhase2->setRole($company->getCompanyRoles()[0]);
        //         $incidentPhase2->setIncidentType($incidentType1);
        //         $incidentPhase2->setCreatedDate($currentDateTime);
        //         $incidentPhase2->setIsInApp(true);
        //         $incidentPhase2->setIsInWeb(true);
        //         $incidentPhase2->setAllocatedTime(60);
        //         $this->em->persist($incidentPhase2);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Steps1
        //         $incidentPhase2Step1 = new IncidentStep();
        //         $incidentPhase2Step1->setPhase($incidentPhase2);
        //         $incidentPhase2Step1->setTitle('Driver Safety');
        //         $incidentPhase2Step1->setInstructions('Ensures Driver is safe and out of harm\'s way / Authorities have been contacted
        // Contact Station Manager or Manager on Duty if there is an Accident with Injuries or a fatality to have them go to the scene immediately. NO EXCEPTIONS');
        //         $incidentPhase2Step1->setStepNumber(1);
        //         $incidentPhase2Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase2Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step1Field1 = new IncidentFormFields();
        //         $incidentPhase2Step1Field1->setOrdering(1);
        //         $incidentPhase2Step1Field1->setLabel('');
        //         $incidentPhase2Step1Field1->setFieldId(null);
        //         $incidentPhase2Step1Field1->setFieldData([]);
        //         $incidentPhase2Step1Field1->setFieldRule([]);
        //         $incidentPhase2Step1Field1->setFieldStyle([]);
        //         $incidentPhase2Step1Field1->setIncidentQuestion($question34);
        //         $incidentPhase2Step1Field1->setStep($incidentPhase2Step1);
        //         $incidentPhase2Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Steps2
        //         $incidentPhase2Step2 = new IncidentStep();
        //         $incidentPhase2Step2->setPhase($incidentPhase2);
        //         $incidentPhase2Step2->setTitle('Record Driver Statement');
        //         $incidentPhase2Step2->setInstructions('Records Driver Statement via Ringcentral....... state to the driver " We are on a recorded line');
        //         $incidentPhase2Step2->setStepNumber(2);
        //         $incidentPhase2Step2->setOrdering(2);
        //         $this->em->persist($incidentPhase2Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step2Field1 = new IncidentFormFields();
        //         $incidentPhase2Step2Field1->setOrdering(1);
        //         $incidentPhase2Step2Field1->setLabel('');
        //         $incidentPhase2Step2Field1->setFieldId(null);
        //         $incidentPhase2Step2Field1->setFieldData([]);
        //         $incidentPhase2Step2Field1->setFieldRule([]);
        //         $incidentPhase2Step2Field1->setFieldStyle([]);
        //         $incidentPhase2Step2Field1->setIncidentQuestion($question35);
        //         $incidentPhase2Step2Field1->setStep($incidentPhase2Step2);
        //         $incidentPhase2Step2Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step2Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Steps3
        //         $incidentPhase2Step3 = new IncidentStep();
        //         $incidentPhase2Step3->setPhase($incidentPhase2);
        //         $incidentPhase2Step3->setTitle('Safety and Compliance');
        //         $incidentPhase2Step3->setInstructions('Contact Safety and Compliance office Station Management and Notify them of the Incident:');
        //         $incidentPhase2Step3->setStepNumber(3);
        //         $incidentPhase2Step3->setOrdering(3);
        //         $this->em->persist($incidentPhase2Step3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step3Field1 = new IncidentFormFields();
        //         $incidentPhase2Step3Field1->setOrdering(1);
        //         $incidentPhase2Step3Field1->setLabel('');
        //         $incidentPhase2Step3Field1->setFieldId(null);
        //         $incidentPhase2Step3Field1->setFieldData([]);
        //         $incidentPhase2Step3Field1->setFieldRule([]);
        //         $incidentPhase2Step3Field1->setFieldStyle([]);
        //         $incidentPhase2Step3Field1->setIncidentQuestion($question36);
        //         $incidentPhase2Step3Field1->setStep($incidentPhase2Step3);
        //         $incidentPhase2Step3Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step3Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Step[]s4
        //         $incidentPhase2Step4 = new IncidentStep();
        //         $incidentPhase2Step4->setPhase($incidentPhase2);
        //         $incidentPhase2Step4->setTitle('Transfer Documents');
        //         $incidentPhase2Step4->setInstructions('Transfer all information supporting our Drivers Statement to Safety and Compliance Office  (S&C)');
        //         $incidentPhase2Step4->setStepNumber(3);
        //         $incidentPhase2Step4->setOrdering(3);
        //         $this->em->persist($incidentPhase2Step4);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step4Field1 = new IncidentFormFields();
        //         $incidentPhase2Step4Field1->setOrdering(1);
        //         $incidentPhase2Step4Field1->setLabel('');
        //         $incidentPhase2Step4Field1->setFieldId(null);
        //         $incidentPhase2Step4Field1->setFieldData([]);
        //         $incidentPhase2Step4Field1->setFieldRule([]);
        //         $incidentPhase2Step4Field1->setFieldStyle([]);
        //         $incidentPhase2Step4Field1->setIncidentQuestion($question37);
        //         $incidentPhase2Step4Field1->setStep($incidentPhase2Step4);
        //         $incidentPhase2Step4Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step4Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Steps5
        //         $incidentPhase2Step5 = new IncidentStep();
        //         $incidentPhase2Step5->setPhase($incidentPhase2);
        //         $incidentPhase2Step5->setTitle('Communicate with Station Manager');
        //         $incidentPhase2Step5->setInstructions('Coordinate with Station Manager to determine where the Vehicle should go due to Incident.
        // If vehicle needs to be towed/Coordinate with Station Where and ensure its documented/ If not Ensure the vehicle gets back to the station….');
        //         $incidentPhase2Step5->setStepNumber(3);
        //         $incidentPhase2Step5->setOrdering(3);
        //         $this->em->persist($incidentPhase2Step5);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step5Field1 = new IncidentFormFields();
        //         $incidentPhase2Step5Field1->setOrdering(1);
        //         $incidentPhase2Step5Field1->setLabel('');
        //         $incidentPhase2Step5Field1->setFieldId(null);
        //         $incidentPhase2Step5Field1->setFieldData([]);
        //         $incidentPhase2Step5Field1->setFieldRule([]);
        //         $incidentPhase2Step5Field1->setFieldStyle([]);
        //         $incidentPhase2Step5Field1->setIncidentQuestion($question38);
        //         $incidentPhase2Step5Field1->setStep($incidentPhase2Step5);
        //         $incidentPhase2Step5Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step5Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase2 Steps6
        //         $incidentPhase2Step6 = new IncidentStep();
        //         $incidentPhase2Step6->setPhase($incidentPhase2);
        //         $incidentPhase2Step6->setTitle('Setup Rescure route');
        //         $incidentPhase2Step6->setInstructions('Coordinate Rescue if any. Attempt to get the remaining packages out');
        //         $incidentPhase2Step6->setStepNumber(3);
        //         $incidentPhase2Step6->setOrdering(3);
        //         $this->em->persist($incidentPhase2Step6);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase2Step6Field1 = new IncidentFormFields();
        //         $incidentPhase2Step6Field1->setOrdering(1);
        //         $incidentPhase2Step6Field1->setLabel('');
        //         $incidentPhase2Step6Field1->setFieldId(null);
        //         $incidentPhase2Step6Field1->setFieldData([]);
        //         $incidentPhase2Step6Field1->setFieldRule([]);
        //         $incidentPhase2Step6Field1->setFieldStyle([]);
        //         $incidentPhase2Step6Field1->setIncidentQuestion($question39);
        //         $incidentPhase2Step6Field1->setStep($incidentPhase2Step6);
        //         $incidentPhase2Step6Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase2Step6Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase3  Add
        //         $incidentPhase3 = new IncidentPhase();
        //         $incidentPhase3->setTitle('Dispatch');
        //         $incidentPhase3->setOrdering('3');
        //         // $incidentPhase3->setCompany($company);
        //         $incidentPhase3->setRole($company->getCompanyRoles()[1]);
        //         $incidentPhase3->setIncidentType($incidentType1);
        //         $incidentPhase3->setCreatedDate($currentDateTime);
        //         $incidentPhase3->setIsInApp(false);
        //         $incidentPhase3->setIsInWeb(true);
        //         $incidentPhase3->setAllocatedTime(60);
        //         $this->em->persist($incidentPhase3);
        //         $this->em->flush();
        //
        //         //Incident Phase3 Steps1
        //         $incidentPhase3Step1 = new IncidentStep();
        //         $incidentPhase3Step1->setPhase($incidentPhase3);
        //         $incidentPhase3Step1->setTitle('Scene Review');
        //         $incidentPhase3Step1->setInstructions('Determine whether or not you are needed at the Scene and get there if needed.');
        //         $incidentPhase3Step1->setStepNumber(1);
        //         $incidentPhase3Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase3Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase3Step1Field1 = new IncidentFormFields();
        //         $incidentPhase3Step1Field1->setOrdering(1);
        //         $incidentPhase3Step1Field1->setLabel('');
        //         $incidentPhase3Step1Field1->setFieldId(null);
        //         $incidentPhase3Step1Field1->setFieldData([]);
        //         $incidentPhase3Step1Field1->setFieldRule([]);
        //         $incidentPhase3Step1Field1->setFieldStyle([]);
        //         $incidentPhase3Step1Field1->setIncidentQuestion($question40);
        //         $incidentPhase3Step1Field1->setStep($incidentPhase3Step1);
        //         $incidentPhase3Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase3Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase3 Steps2
        //         $incidentPhase3Step2 = new IncidentStep();
        //         $incidentPhase3Step2->setPhase($incidentPhase3);
        //         $incidentPhase3Step2->setTitle('Review Documents if onsite');
        //         $incidentPhase3Step2->setInstructions('If onsite, document from drivers perspective to obtain what occurred and compare what Central Dispatch received.');
        //         $incidentPhase3Step2->setStepNumber(2);
        //         $incidentPhase3Step2->setOrdering(2);
        //         $this->em->persist($incidentPhase3Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase3Step2Field1 = new IncidentFormFields();
        //         $incidentPhase3Step2Field1->setOrdering(1);
        //         $incidentPhase3Step2Field1->setLabel('');
        //         $incidentPhase3Step2Field1->setFieldId(null);
        //         $incidentPhase3Step2Field1->setFieldData([]);
        //         $incidentPhase3Step2Field1->setFieldRule([]);
        //         $incidentPhase3Step2Field1->setFieldStyle([]);
        //         $incidentPhase3Step2Field1->setIncidentQuestion($question42);
        //         $incidentPhase3Step2Field1->setStep($incidentPhase3Step2);
        //         $incidentPhase3Step2Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase3Step2Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase3 Steps2
        //         $incidentPhase3Step3 = new IncidentStep();
        //         $incidentPhase3Step3->setPhase($incidentPhase3);
        //         $incidentPhase3Step3->setTitle('Review Paperwork');
        //         $incidentPhase3Step3->setInstructions('Ensure all paperwork pertaining to incident is sent to Safety and Compliance office');
        //         $incidentPhase3Step3->setStepNumber(3);
        //         $incidentPhase3Step3->setOrdering(3);
        //         $this->em->persist($incidentPhase3Step3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase3Step3Field1 = new IncidentFormFields();
        //         $incidentPhase3Step3Field1->setOrdering(1);
        //         $incidentPhase3Step3Field1->setLabel('');
        //         $incidentPhase3Step3Field1->setFieldId(null);
        //         $incidentPhase3Step3Field1->setFieldData([]);
        //         $incidentPhase3Step3Field1->setFieldRule([]);
        //         $incidentPhase3Step3Field1->setFieldStyle([]);
        //         $incidentPhase3Step3Field1->setIncidentQuestion($question43);
        //         $incidentPhase3Step3Field1->setStep($incidentPhase3Step3);
        //         $incidentPhase3Step3Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase3Step3Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4  Add
        //         $incidentPhase4 = new IncidentPhase();
        //         $incidentPhase4->setTitle('Safety and Compliance');
        //         $incidentPhase4->setOrdering('4');
        //         // $incidentPhase4->setCompany($company);
        //         $incidentPhase4->setRole($company->getCompanyRoles()[6]);
        //         $incidentPhase4->setIncidentType($incidentType1);
        //         $incidentPhase4->setCreatedDate($currentDateTime);
        //         $incidentPhase4->setIsInApp(false);
        //         $incidentPhase4->setIsInWeb(true);
        //         $incidentPhase4->setAllocatedTime(2880);
        //         $this->em->persist($incidentPhase4);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps1
        //         $incidentPhase4Step1 = new IncidentStep();
        //         $incidentPhase4Step1->setPhase($incidentPhase4);
        //         $incidentPhase4Step1->setTitle('Insurance Claim Review');
        //         $incidentPhase4Step1->setInstructions('Determine Whether or not it is an Insurance claim based on the contents of the incident report and what type of claim.');
        //         $incidentPhase4Step1->setStepNumber(1);
        //         $incidentPhase4Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase4Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step1Field1 = new IncidentFormFields();
        //         $incidentPhase4Step1Field1->setOrdering(1);
        //         $incidentPhase4Step1Field1->setLabel('');
        //         $incidentPhase4Step1Field1->setFieldId(null);
        //         $incidentPhase4Step1Field1->setFieldData([]);
        //         $incidentPhase4Step1Field1->setFieldRule([]);
        //         $incidentPhase4Step1Field1->setFieldStyle([]);
        //         $incidentPhase4Step1Field1->setIncidentQuestion($question44);
        //         $incidentPhase4Step1Field1->setStep($incidentPhase4Step1);
        //         $incidentPhase4Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps2
        //         $incidentPhase4Step2 = new IncidentStep();
        //         $incidentPhase4Step2->setPhase($incidentPhase4);
        //         $incidentPhase4Step2->setTitle('Scan and submit Documents');
        //         $incidentPhase4Step2->setInstructions('Scan and submit all supporting documents to Insurance Carrier Adjuster Contact');
        //         $incidentPhase4Step2->setStepNumber(2);
        //         $incidentPhase4Step2->setOrdering(2);
        //         $this->em->persist($incidentPhase4Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step2Field1 = new IncidentFormFields();
        //         $incidentPhase4Step2Field1->setOrdering(1);
        //         $incidentPhase4Step2Field1->setLabel('');
        //         $incidentPhase4Step2Field1->setFieldId(null);
        //         $incidentPhase4Step2Field1->setFieldData([]);
        //         $incidentPhase4Step2Field1->setFieldRule([]);
        //         $incidentPhase4Step2Field1->setFieldStyle([]);
        //         $incidentPhase4Step2Field1->setIncidentQuestion($question45);
        //         $incidentPhase4Step2Field1->setStep($incidentPhase4Step2);
        //         $incidentPhase4Step2Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step2Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps3
        //         $incidentPhase4Step3 = new IncidentStep();
        //         $incidentPhase4Step3->setPhase($incidentPhase4);
        //         $incidentPhase4Step3->setTitle('Medical Billing');
        //         $incidentPhase4Step3->setInstructions('Send Claim information to Medical facility for billing');
        //         $incidentPhase4Step3->setStepNumber(3);
        //         $incidentPhase4Step3->setOrdering(3);
        //         $this->em->persist($incidentPhase4Step3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step3Field1 = new IncidentFormFields();
        //         $incidentPhase4Step3Field1->setOrdering(1);
        //         $incidentPhase4Step3Field1->setLabel('');
        //         $incidentPhase4Step3Field1->setFieldId(null);
        //         $incidentPhase4Step3Field1->setFieldData([]);
        //         $incidentPhase4Step3Field1->setFieldRule([]);
        //         $incidentPhase4Step3Field1->setFieldStyle([]);
        //         $incidentPhase4Step3Field1->setIncidentQuestion($question46);
        //         $incidentPhase4Step3Field1->setStep($incidentPhase4Step3);
        //         $incidentPhase4Step3Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step3Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps4
        //         $incidentPhase4Step4 = new IncidentStep();
        //         $incidentPhase4Step4->setPhase($incidentPhase4);
        //         $incidentPhase4Step4->setTitle('Touch base with Driver');
        //         $incidentPhase4Step4->setInstructions('Touch base with Driver on status and Condition Day #2 after Incident');
        //         $incidentPhase4Step4->setStepNumber(4);
        //         $incidentPhase4Step4->setOrdering(4);
        //         $this->em->persist($incidentPhase4Step4);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step4Field1 = new IncidentFormFields();
        //         $incidentPhase4Step4Field1->setOrdering(1);
        //         $incidentPhase4Step4Field1->setLabel('');
        //         $incidentPhase4Step4Field1->setFieldId(null);
        //         $incidentPhase4Step4Field1->setFieldData([]);
        //         $incidentPhase4Step4Field1->setFieldRule([]);
        //         $incidentPhase4Step4Field1->setFieldStyle([]);
        //         $incidentPhase4Step4Field1->setIncidentQuestion($question47);
        //         $incidentPhase4Step4Field1->setStep($incidentPhase4Step4);
        //         $incidentPhase4Step4Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step4Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps5
        //         $incidentPhase4Step5 = new IncidentStep();
        //         $incidentPhase4Step5->setPhase($incidentPhase4);
        //         $incidentPhase4Step5->setTitle('Follow up appointment');
        //         $incidentPhase4Step5->setInstructions('Obtain follow up appointment documents if any and submit to Insurance Carrier');
        //         $incidentPhase4Step5->setStepNumber(5);
        //         $incidentPhase4Step5->setOrdering(5);
        //         $this->em->persist($incidentPhase4Step5);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step5Field1 = new IncidentFormFields();
        //         $incidentPhase4Step5Field1->setOrdering(1);
        //         $incidentPhase4Step5Field1->setLabel('');
        //         $incidentPhase4Step5Field1->setFieldId(null);
        //         $incidentPhase4Step5Field1->setFieldData([]);
        //         $incidentPhase4Step5Field1->setFieldRule([]);
        //         $incidentPhase4Step5Field1->setFieldStyle([]);
        //         $incidentPhase4Step5Field1->setIncidentQuestion($question48);
        //         $incidentPhase4Step5Field1->setStep($incidentPhase4Step5);
        //         $incidentPhase4Step5Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step5Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps6
        //         $incidentPhase4Step6 = new IncidentStep();
        //         $incidentPhase4Step6->setPhase($incidentPhase4);
        //         $incidentPhase4Step6->setTitle('Process vehicle');
        //         $incidentPhase4Step6->setInstructions('Process Claim pertaining to the Vehicle involved in Accident');
        //         $incidentPhase4Step6->setStepNumber(6);
        //         $incidentPhase4Step6->setOrdering(6);
        //         $this->em->persist($incidentPhase4Step6);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step6Field1 = new IncidentFormFields();
        //         $incidentPhase4Step6Field1->setOrdering(1);
        //         $incidentPhase4Step6Field1->setLabel('');
        //         $incidentPhase4Step6Field1->setFieldId(null);
        //         $incidentPhase4Step6Field1->setFieldData([]);
        //         $incidentPhase4Step6Field1->setFieldRule([]);
        //         $incidentPhase4Step6Field1->setFieldStyle([]);
        //         $incidentPhase4Step6Field1->setIncidentQuestion($question49);
        //         $incidentPhase4Step6Field1->setStep($incidentPhase4Step6);
        //         $incidentPhase4Step6Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step6Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps7
        //         $incidentPhase4Step7 = new IncidentStep();
        //         $incidentPhase4Step7->setPhase($incidentPhase4);
        //         $incidentPhase4Step7->setTitle('Property or Third Party Damage');
        //         $incidentPhase4Step7->setInstructions('Process any damages to personal property or third party.');
        //         $incidentPhase4Step7->setStepNumber(7);
        //         $incidentPhase4Step7->setOrdering(7);
        //         $this->em->persist($incidentPhase4Step7);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step7Field1 = new IncidentFormFields();
        //         $incidentPhase4Step7Field1->setOrdering(1);
        //         $incidentPhase4Step7Field1->setLabel('');
        //         $incidentPhase4Step7Field1->setFieldId(null);
        //         $incidentPhase4Step7Field1->setFieldData([]);
        //         $incidentPhase4Step7Field1->setFieldRule([]);
        //         $incidentPhase4Step7Field1->setFieldStyle([]);
        //         $incidentPhase4Step7Field1->setIncidentQuestion($question50);
        //         $incidentPhase4Step7Field1->setStep($incidentPhase4Step7);
        //         $incidentPhase4Step7Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step7Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase4 Steps8
        //         $incidentPhase4Step8 = new IncidentStep();
        //         $incidentPhase4Step8->setPhase($incidentPhase4);
        //         $incidentPhase4Step8->setTitle('Contact Repair Shop or Facility');
        //         $incidentPhase4Step8->setInstructions('Contact Repair Shop or facility to Ensure they have Insurance Adjuster or Appraiser information');
        //         $incidentPhase4Step8->setStepNumber(8);
        //         $incidentPhase4Step8->setOrdering(8);
        //         $this->em->persist($incidentPhase4Step8);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase4Step8Field1 = new IncidentFormFields();
        //         $incidentPhase4Step8Field1->setOrdering(1);
        //         $incidentPhase4Step8Field1->setLabel('');
        //         $incidentPhase4Step8Field1->setFieldId(null);
        //         $incidentPhase4Step8Field1->setFieldData([]);
        //         $incidentPhase4Step8Field1->setFieldRule([]);
        //         $incidentPhase4Step8Field1->setFieldStyle([]);
        //         $incidentPhase4Step8Field1->setIncidentQuestion($question51);
        //         $incidentPhase4Step8Field1->setStep($incidentPhase4Step8);
        //         $incidentPhase4Step8Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase4Step8Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase5  Add
        //         $incidentPhase5 = new IncidentPhase();
        //         $incidentPhase5->setTitle('Feedback');
        //         $incidentPhase5->setOrdering('5');
        //         // $incidentPhase5->setCompany($company);
        //         $incidentPhase5->setRole($company->getCompanyRoles()[1]);
        //         $incidentPhase5->setIncidentType($incidentType1);
        //         $incidentPhase5->setCreatedDate($currentDateTime);
        //         $incidentPhase5->setIsInApp(false);
        //         $incidentPhase5->setIsInWeb(true);
        //         $incidentPhase5->setAllocatedTime(1440);
        //         $this->em->persist($incidentPhase5);
        //         $this->em->flush();
        //
        //         //Incident Phase5 Steps1
        //         $incidentPhase5Step1 = new IncidentStep();
        //         $incidentPhase5Step1->setPhase($incidentPhase5);
        //         $incidentPhase5Step1->setTitle('Check back with driver');
        //         $incidentPhase5Step1->setInstructions('Reach out to driver and check on status and condition after incident.');
        //         $incidentPhase5Step1->setStepNumber(1);
        //         $incidentPhase5Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase5Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase5Step1Field1 = new IncidentFormFields();
        //         $incidentPhase5Step1Field1->setOrdering(1);
        //         $incidentPhase5Step1Field1->setLabel('');
        //         $incidentPhase5Step1Field1->setFieldId(null);
        //         $incidentPhase5Step1Field1->setFieldData([]);
        //         $incidentPhase5Step1Field1->setFieldRule([]);
        //         $incidentPhase5Step1Field1->setFieldStyle([]);
        //         $incidentPhase5Step1Field1->setIncidentQuestion($question52);
        //         $incidentPhase5Step1Field1->setStep($incidentPhase5Step1);
        //         $incidentPhase5Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase5Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase6 Add
        //         $incidentPhase6 = new IncidentPhase();
        //         $incidentPhase6->setTitle('Repair');
        //         $incidentPhase6->setOrdering('6');
        //         // $incidentPhase6->setCompany($company);
        //         $incidentPhase6->setRole($company->getCompanyRoles()[1]);
        //         $incidentPhase6->setIncidentType($incidentType1);
        //         $incidentPhase6->setCreatedDate($currentDateTime);
        //         $incidentPhase6->setIsInApp(false);
        //         $incidentPhase6->setIsInWeb(true);
        //         $incidentPhase6->setAllocatedTime(4320);
        //         $this->em->persist($incidentPhase6);
        //         $this->em->flush();
        //
        //         //Incident Phase6 Steps1
        //         $incidentPhase6Step1 = new IncidentStep();
        //         $incidentPhase6Step1->setPhase($incidentPhase6);
        //         $incidentPhase6Step1->setTitle('Provide Estimate');
        //         $incidentPhase6Step1->setInstructions('Prepare and provide an estimate for the Damages pertaining to Incident');
        //         $incidentPhase6Step1->setStepNumber(1);
        //         $incidentPhase6Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase6Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase6Step1Field1 = new IncidentFormFields();
        //         $incidentPhase6Step1Field1->setOrdering(1);
        //         $incidentPhase6Step1Field1->setLabel('');
        //         $incidentPhase6Step1Field1->setFieldId(null);
        //         $incidentPhase6Step1Field1->setFieldData([]);
        //         $incidentPhase6Step1Field1->setFieldRule([]);
        //         $incidentPhase6Step1Field1->setFieldStyle([]);
        //         $incidentPhase6Step1Field1->setIncidentQuestion($question53);
        //         $incidentPhase6Step1Field1->setStep($incidentPhase6Step1);
        //         $incidentPhase6Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase6Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase6 Steps2
        //         $incidentPhase6Step2 = new IncidentStep();
        //         $incidentPhase6Step2->setPhase($incidentPhase6);
        //         $incidentPhase6Step2->setTitle('Returned to fleet?');
        //         $incidentPhase6Step2->setInstructions('Was timely service provided in order to return the vehicle to the fleet?');
        //         $incidentPhase6Step2->setStepNumber(1);
        //         $incidentPhase6Step2->setOrdering(1);
        //         $this->em->persist($incidentPhase6Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase6Step2Field1 = new IncidentFormFields();
        //         $incidentPhase6Step2Field1->setOrdering(1);
        //         $incidentPhase6Step2Field1->setLabel('');
        //         $incidentPhase6Step2Field1->setFieldId(null);
        //         $incidentPhase6Step2Field1->setFieldData([]);
        //         $incidentPhase6Step2Field1->setFieldRule([]);
        //         $incidentPhase6Step2Field1->setFieldStyle([]);
        //         $incidentPhase6Step2Field1->setIncidentQuestion($question54);
        //         $incidentPhase6Step2Field1->setStep($incidentPhase6Step2);
        //         $incidentPhase6Step2Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase6Step2Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase7 Add
        //         $incidentPhase7 = new IncidentPhase();
        //         $incidentPhase7->setTitle('Legal and Financial');
        //         $incidentPhase7->setOrdering('7');
        //         // $incidentPhase7->setCompany($company);
        //         $incidentPhase7->setRole($company->getCompanyRoles()[6]);
        //         $incidentPhase7->setIncidentType($incidentType1);
        //         $incidentPhase7->setCreatedDate($currentDateTime);
        //         $incidentPhase7->setIsInApp(false);
        //         $incidentPhase7->setIsInWeb(true);
        //         $incidentPhase7->setAllocatedTime(4320);
        //         $this->em->persist($incidentPhase7);
        //         $this->em->flush();
        //
        //         //Incident Phase7 Steps1
        //         $incidentPhase7Step1 = new IncidentStep();
        //         $incidentPhase7Step1->setPhase($incidentPhase7);
        //         $incidentPhase7Step1->setTitle('Intake Details');
        //         $incidentPhase7Step1->setInstructions('Process intake and assign claim # with Incident');
        //         $incidentPhase7Step1->setStepNumber(1);
        //         $incidentPhase7Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase7Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step1Field1 = new IncidentFormFields();
        //         $incidentPhase7Step1Field1->setOrdering(1);
        //         $incidentPhase7Step1Field1->setLabel('');
        //         $incidentPhase7Step1Field1->setFieldId(null);
        //         $incidentPhase7Step1Field1->setFieldData([]);
        //         $incidentPhase7Step1Field1->setFieldRule([]);
        //         $incidentPhase7Step1Field1->setFieldStyle([]);
        //         $incidentPhase7Step1Field1->setIncidentQuestion($question55);
        //         $incidentPhase7Step1Field1->setStep($incidentPhase7Step1);
        //         $incidentPhase7Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step1Field2 = new IncidentFormFields();
        //         $incidentPhase7Step1Field2->setOrdering(2);
        //         $incidentPhase7Step1Field2->setLabel('');
        //         $incidentPhase7Step1Field2->setFieldId(null);
        //         $incidentPhase7Step1Field2->setFieldData([]);
        //         $incidentPhase7Step1Field2->setFieldRule([]);
        //         $incidentPhase7Step1Field2->setFieldStyle([]);
        //         $incidentPhase7Step1Field2->setIncidentQuestion($question56);
        //         $incidentPhase7Step1Field2->setStep($incidentPhase7Step1);
        //         $incidentPhase7Step1Field2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step1Field2);
        //         $this->em->flush();
        //
        //         //Incident Phase7 Steps2
        //         $incidentPhase7Step2 = new IncidentStep();
        //         $incidentPhase7Step2->setPhase($incidentPhase7);
        //         $incidentPhase7Step2->setTitle('Assign Adjuster');
        //         $incidentPhase7Step2->setInstructions('Process Claim and Assign Adjuster…..Investigation if Necessary');
        //         $incidentPhase7Step2->setStepNumber(2);
        //         $incidentPhase7Step2->setOrdering(2);
        //         $this->em->persist($incidentPhase7Step2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step2Field1 = new IncidentFormFields();
        //         $incidentPhase7Step2Field1->setOrdering(1);
        //         $incidentPhase7Step2Field1->setLabel('');
        //         $incidentPhase7Step2Field1->setFieldId(null);
        //         $incidentPhase7Step2Field1->setFieldData([]);
        //         $incidentPhase7Step2Field1->setFieldRule([]);
        //         $incidentPhase7Step2Field1->setFieldStyle([]);
        //         $incidentPhase7Step2Field1->setIncidentQuestion($question57);
        //         $incidentPhase7Step2Field1->setStep($incidentPhase7Step2);
        //         $incidentPhase7Step2Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step2Field1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step2Field2 = new IncidentFormFields();
        //         $incidentPhase7Step2Field2->setOrdering(2);
        //         $incidentPhase7Step2Field2->setLabel('');
        //         $incidentPhase7Step2Field2->setFieldId(null);
        //         $incidentPhase7Step2Field2->setFieldData([]);
        //         $incidentPhase7Step2Field2->setFieldRule([]);
        //         $incidentPhase7Step2Field2->setFieldStyle([]);
        //         $incidentPhase7Step2Field2->setIncidentQuestion($question58);
        //         $incidentPhase7Step2Field2->setStep($incidentPhase7Step2);
        //         $incidentPhase7Step2Field2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step2Field2);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step2Field3 = new IncidentFormFields();
        //         $incidentPhase7Step2Field3->setOrdering(3);
        //         $incidentPhase7Step2Field3->setLabel('');
        //         $incidentPhase7Step2Field3->setFieldId(null);
        //         $incidentPhase7Step2Field3->setFieldData([]);
        //         $incidentPhase7Step2Field3->setFieldRule([]);
        //         $incidentPhase7Step2Field3->setFieldStyle([]);
        //         $incidentPhase7Step2Field3->setIncidentQuestion($question59);
        //         $incidentPhase7Step2Field3->setStep($incidentPhase7Step2);
        //         $incidentPhase7Step2Field3->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step2Field3);
        //         $this->em->flush();
        //
        //         //Incident Phase7 Steps3
        //         $incidentPhase7Step3 = new IncidentStep();
        //         $incidentPhase7Step3->setPhase($incidentPhase7);
        //         $incidentPhase7Step3->setTitle('Payments');
        //         $incidentPhase7Step3->setInstructions('Process payment based on determined facts of incident or Return to us to pay out of pocket ');
        //         $incidentPhase7Step3->setStepNumber(3);
        //         $incidentPhase7Step3->setOrdering(3);
        //         $this->em->persist($incidentPhase7Step3);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase7Step3Field1 = new IncidentFormFields();
        //         $incidentPhase7Step3Field1->setOrdering(1);
        //         $incidentPhase7Step3Field1->setLabel('');
        //         $incidentPhase7Step3Field1->setFieldId(null);
        //         $incidentPhase7Step3Field1->setFieldData([]);
        //         $incidentPhase7Step3Field1->setFieldRule([]);
        //         $incidentPhase7Step3Field1->setFieldStyle([]);
        //         $incidentPhase7Step3Field1->setIncidentQuestion($question60);
        //         $incidentPhase7Step3Field1->setStep($incidentPhase7Step3);
        //         $incidentPhase7Step3Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase7Step3Field1);
        //         $this->em->flush();
        //
        //         //Incident Phase8 Add
        //         $incidentPhase8 = new IncidentPhase();
        //         $incidentPhase8->setTitle('Finalization');
        //         $incidentPhase8->setOrdering('8');
        //         // $incidentPhase8->setCompany($company);
        //         $incidentPhase8->setRole($company->getCompanyRoles()[6]);
        //         $incidentPhase8->setIncidentType($incidentType1);
        //         $incidentPhase8->setCreatedDate($currentDateTime);
        //         $incidentPhase8->setIsInApp(false);
        //         $incidentPhase8->setIsInWeb(true);
        //         $incidentPhase8->setAllocatedTime(4320);
        //         $this->em->persist($incidentPhase8);
        //         $this->em->flush();
        //
        //         //Incident Phase8 Steps1
        //         $incidentPhase8Step1 = new IncidentStep();
        //         $incidentPhase8Step1->setPhase($incidentPhase8);
        //         $incidentPhase8Step1->setTitle('Review incident');
        //         $incidentPhase8Step1->setInstructions('');
        //         $incidentPhase8Step1->setStepNumber(1);
        //         $incidentPhase8Step1->setOrdering(1);
        //         $this->em->persist($incidentPhase8Step1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase8Step1Field1 = new IncidentFormFields();
        //         $incidentPhase8Step1Field1->setOrdering(1);
        //         $incidentPhase8Step1Field1->setLabel('');
        //         $incidentPhase8Step1Field1->setFieldId(null);
        //         $incidentPhase8Step1Field1->setFieldData([]);
        //         $incidentPhase8Step1Field1->setFieldRule([]);
        //         $incidentPhase8Step1Field1->setFieldStyle([]);
        //         $incidentPhase8Step1Field1->setIncidentQuestion($question61);
        //         $incidentPhase8Step1Field1->setStep($incidentPhase8Step1);
        //         $incidentPhase8Step1Field1->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase8Step1Field1);
        //         $this->em->flush();
        //
        //         //Incident Form Fields
        //         $incidentPhase8Step1Field2 = new IncidentFormFields();
        //         $incidentPhase8Step1Field2->setOrdering(2);
        //         $incidentPhase8Step1Field2->setLabel('');
        //         $incidentPhase8Step1Field2->setFieldId(null);
        //         $incidentPhase8Step1Field2->setFieldData([]);
        //         $incidentPhase8Step1Field2->setFieldRule([]);
        //         $incidentPhase8Step1Field2->setFieldStyle([]);
        //         $incidentPhase8Step1Field2->setIncidentQuestion($question62);
        //         $incidentPhase8Step1Field2->setStep($incidentPhase8Step1);
        //         $incidentPhase8Step1Field2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentPhase8Step1Field2);
        //         $this->em->flush();

        //inciden type 1 end

        // inciden type 2 start

        //Incident Type Add
        $incidentType2 = new IncidentType();
        $incidentType2->setName('Call In');
        $incidentType2->setPrefix('Cal');
        $incidentType2->setCompany($company);
        $incidentType2->setIsInWeb(true);
        $this->em->persist($incidentType2);
        $this->em->flush();

        // Incident Phase Add
        $incidentType2Phase1 = new IncidentPhase();
        $incidentType2Phase1->setTitle('Call In Report');
        $incidentType2Phase1->setOrdering('1');
        //  $incidentType2Phase1->setCompany($company);
        $incidentType2Phase1->setRole($company->getCompanyRoles()[4]);
        $incidentType2Phase1->setIncidentType($incidentType2);
        $incidentType2Phase1->setIsInApp(false);
        $incidentType2Phase1->setIsInWeb(true);
        $incidentType2Phase1->setCreatedDate($currentDateTime);
        $this->em->persist($incidentType2Phase1);
        $this->em->flush();
        //Incident Phase 1 Steps 1
        // $incidentType2Phase1Step1 = new IncidentStep();
        // $incidentType2Phase1Step1->setPhase($incidentType2Phase1);
        // $incidentType2Phase1Step1->setTitle('Choose Driver');
        // $incidentType2Phase1Step1->setInstructions('');
        // $incidentType2Phase1Step1->setStepNumber(1);
        // $incidentType2Phase1Step1->setOrdering(1);
        // $incidentType2Phase1Step1->setIsInApp(false);
        // $incidentType2Phase1Step1->setIsInWeb(true);
        // $this->em->persist($incidentType2Phase1Step1);
        // $this->em->flush();

        //Incident Form Fields
        // $incidentType2Phase1Step1Field1 = new IncidentFormFields();
        // $incidentType2Phase1Step1Field1->setOrdering(1);
        // $incidentType2Phase1Step1Field1->setLabel('');
        // $incidentType2Phase1Step1Field1->setFieldId(23);
        // $incidentType2Phase1Step1Field1->setFieldData([]);
        // $incidentType2Phase1Step1Field1->setFieldRule([]);
        // $incidentType2Phase1Step1Field1->setFieldStyle([]);
        // $incidentType2Phase1Step1Field1->setIncidentQuestion($question63);
        // $incidentType2Phase1Step1Field1->setStep($incidentType2Phase1Step1);
        // $incidentType2Phase1Step1Field1->setCreatedDate($currentDateTime);
        // $incidentType2Phase1Step1Field1->setIsInApp(false);
        // $incidentType2Phase1Step1Field1->setIsInWeb(true);
        // $this->em->persist($incidentType2Phase1Step1Field1);
        // $this->em->flush();

        //Incident Phase 1 Steps 2
        $incidentType2Phase1Step2 = new IncidentStep();
        $incidentType2Phase1Step2->setPhase($incidentType2Phase1);
        $incidentType2Phase1Step2->setTitle('Reason for call in');
        $incidentType2Phase1Step2->setInstructions('Please provide a reason for the call in.');
        $incidentType2Phase1Step2->setStepNumber(2);
        $incidentType2Phase1Step2->setOrdering(2);
        $incidentType2Phase1Step2->setIsInApp(false);
        $incidentType2Phase1Step2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step2);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase1Step2Field1 = new IncidentFormFields();
        $incidentType2Phase1Step2Field1->setOrdering(1);
        $incidentType2Phase1Step2Field1->setLabel('');
        $incidentType2Phase1Step2Field1->setFieldId(null);
        $incidentType2Phase1Step2Field1->setFieldData([]);
        $incidentType2Phase1Step2Field1->setFieldRule([]);
        $incidentType2Phase1Step2Field1->setFieldStyle([]);
        $incidentType2Phase1Step2Field1->setIncidentQuestion($question64);
        $incidentType2Phase1Step2Field1->setStep($incidentType2Phase1Step2);
        $incidentType2Phase1Step2Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase1Step2Field1->setIsInApp(false);
        $incidentType2Phase1Step2Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step2Field1);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase1Step2Field2 = new IncidentFormFields();
        $incidentType2Phase1Step2Field2->setOrdering(2);
        $incidentType2Phase1Step2Field2->setLabel('open notes');
        $incidentType2Phase1Step2Field2->setFieldId(null);
        $incidentType2Phase1Step2Field2->setFieldData([]);
        $incidentType2Phase1Step2Field2->setFieldRule([]);
        $incidentType2Phase1Step2Field2->setFieldStyle([]);
        $incidentType2Phase1Step2Field2->setIncidentQuestion($question64a);
        $incidentType2Phase1Step2Field2->setStep($incidentType2Phase1Step2);
        $incidentType2Phase1Step2Field2->setCreatedDate($currentDateTime);
        $incidentType2Phase1Step2Field2->setIsInApp(false);
        $incidentType2Phase1Step2Field2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step2Field2);
        $this->em->flush();

        //Incident Phase 1 Steps 3
        $incidentType2Phase1Step3 = new IncidentStep();
        $incidentType2Phase1Step3->setPhase($incidentType2Phase1);
        $incidentType2Phase1Step3->setTitle('Assign To');
        $incidentType2Phase1Step3->setInstructions('');
        $incidentType2Phase1Step3->setStepNumber(3);
        $incidentType2Phase1Step3->setOrdering(3);
        $incidentType2Phase1Step3->setIsInApp(false);
        $incidentType2Phase1Step3->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step3);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase1Step3Field1 = new IncidentFormFields();
        $incidentType2Phase1Step3Field1->setOrdering(1);
        $incidentType2Phase1Step3Field1->setLabel('');
        $incidentType2Phase1Step3Field1->setFieldId(null);
        $incidentType2Phase1Step3Field1->setFieldData([]);
        $incidentType2Phase1Step3Field1->setFieldRule([]);
        $incidentType2Phase1Step3Field1->setFieldStyle([]);
        $incidentType2Phase1Step3Field1->setIncidentQuestion($question65);
        $incidentType2Phase1Step3Field1->setStep($incidentType2Phase1Step3);
        $incidentType2Phase1Step3Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase1Step3Field1->setIsInApp(false);
        $incidentType2Phase1Step3Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step3Field1);
        $this->em->flush();

        $incidentType2Phase1Step3Field2 = new IncidentFormFields();
        $incidentType2Phase1Step3Field2->setOrdering(2);
        $incidentType2Phase1Step3Field2->setLabel('');
        $incidentType2Phase1Step3Field2->setFieldId(null);
        $incidentType2Phase1Step3Field2->setFieldData([]);
        $incidentType2Phase1Step3Field2->setFieldRule([]);
        $incidentType2Phase1Step3Field2->setFieldStyle([]);
        $incidentType2Phase1Step3Field2->setIncidentQuestion($question66);
        $incidentType2Phase1Step3Field2->setStep($incidentType2Phase1Step3);
        $incidentType2Phase1Step3Field2->setCreatedDate($currentDateTime);
        $incidentType2Phase1Step3Field2->setIsInApp(false);
        $incidentType2Phase1Step3Field2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase1Step3Field2);
        $this->em->flush();

        // Incident Phase Add
        $incidentType2Phase2 = new IncidentPhase();
        $incidentType2Phase2->setTitle('Review');
        $incidentType2Phase2->setOrdering(2);
        //$incidentType2Phase2->setCompany($company);
        $incidentType2Phase2->setRole($company->getCompanyRoles()[2]);
        $incidentType2Phase2->setIncidentType($incidentType2);
        $incidentType2Phase2->setCreatedDate($currentDateTime);
        $incidentType2Phase2->setAllocatedTime(1440);
        $incidentType2Phase2->setIsInApp(false);
        $incidentType2Phase2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2);
        $this->em->flush();

        //Incident Phase 2 Steps 1
        $incidentType2Phase2Step1 = new IncidentStep();
        $incidentType2Phase2Step1->setPhase($incidentType2Phase2);
        $incidentType2Phase2Step1->setTitle('Confirmed excused / unexcused');
        $incidentType2Phase2Step1->setInstructions('Was the call in excusable?');
        $incidentType2Phase2Step1->setStepNumber(1);
        $incidentType2Phase2Step1->setOrdering(1);
        $incidentType2Phase2Step1->setIsInApp(false);
        $incidentType2Phase2Step1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step1);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase2Step1Field1 = new IncidentFormFields();
        $incidentType2Phase2Step1Field1->setOrdering(1);
        $incidentType2Phase2Step1Field1->setLabel('');
        $incidentType2Phase2Step1Field1->setFieldId(null);
        $incidentType2Phase2Step1Field1->setFieldData([]);
        $incidentType2Phase2Step1Field1->setFieldRule([]);
        $incidentType2Phase2Step1Field1->setFieldStyle([]);
        $incidentType2Phase2Step1Field1->setIncidentQuestion($question67);
        $incidentType2Phase2Step1Field1->setStep($incidentType2Phase2Step1);
        $incidentType2Phase2Step1Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase2Step1Field1->setIsInApp(false);
        $incidentType2Phase2Step1Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step1Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 2
        $incidentType2Phase2Step2 = new IncidentStep();
        $incidentType2Phase2Step2->setPhase($incidentType2Phase2);
        $incidentType2Phase2Step2->setTitle('Upload documents');
        $incidentType2Phase2Step2->setInstructions('If incident is excusable then pleasep provide proper documentation.');
        $incidentType2Phase2Step2->setStepNumber(2);
        $incidentType2Phase2Step2->setOrdering(2);
        $incidentType2Phase2Step2->setIsInApp(false);
        $incidentType2Phase2Step2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step2);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase2Step2Field1 = new IncidentFormFields();
        $incidentType2Phase2Step2Field1->setOrdering(1);
        $incidentType2Phase2Step2Field1->setLabel('');
        $incidentType2Phase2Step2Field1->setFieldId(null);
        $incidentType2Phase2Step2Field1->setFieldData([]);
        $incidentType2Phase2Step2Field1->setFieldRule([]);
        $incidentType2Phase2Step2Field1->setFieldStyle([]);
        $incidentType2Phase2Step2Field1->setIncidentQuestion($question68);
        $incidentType2Phase2Step2Field1->setStep($incidentType2Phase2Step2);
        $incidentType2Phase2Step2Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase2Step2Field1->setIsInApp(false);
        $incidentType2Phase2Step2Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step2Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 3
        $incidentType2Phase2Step3 = new IncidentStep();
        $incidentType2Phase2Step3->setPhase($incidentType2Phase2);
        $incidentType2Phase2Step3->setTitle('Correction Action');
        $incidentType2Phase2Step3->setInstructions('Is there a need for correction action?');
        $incidentType2Phase2Step3->setStepNumber(3);
        $incidentType2Phase2Step3->setOrdering(3);
        $incidentType2Phase2Step3->setIsInApp(false);
        $incidentType2Phase2Step3->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step3);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase2Step3Field1 = new IncidentFormFields();
        $incidentType2Phase2Step3Field1->setOrdering(1);
        $incidentType2Phase2Step3Field1->setLabel('');
        $incidentType2Phase2Step3Field1->setFieldId(null);
        $incidentType2Phase2Step3Field1->setFieldData([]);
        $incidentType2Phase2Step3Field1->setFieldRule([]);
        $incidentType2Phase2Step3Field1->setFieldStyle([]);
        $incidentType2Phase2Step3Field1->setIncidentQuestion($question69);
        $incidentType2Phase2Step3Field1->setStep($incidentType2Phase2Step3);
        $incidentType2Phase2Step3Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase2Step3Field1->setIsInApp(false);
        $incidentType2Phase2Step3Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step3Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 4
        $incidentType2Phase2Step4 = new IncidentStep();
        $incidentType2Phase2Step4->setPhase($incidentType2Phase2);
        $incidentType2Phase2Step4->setTitle('Notes');
        $incidentType2Phase2Step4->setInstructions('Please provide any notes about this case.');
        $incidentType2Phase2Step4->setStepNumber(4);
        $incidentType2Phase2Step4->setOrdering(4);
        $incidentType2Phase2Step4->setIsInApp(false);
        $incidentType2Phase2Step4->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step4);
        $this->em->flush();

        //Incident Form Fields
        $incidentType2Phase2Step4Field1 = new IncidentFormFields();
        $incidentType2Phase2Step4Field1->setOrdering(1);
        $incidentType2Phase2Step4Field1->setLabel('');
        $incidentType2Phase2Step4Field1->setFieldId(null);
        $incidentType2Phase2Step4Field1->setFieldData([]);
        $incidentType2Phase2Step4Field1->setFieldRule([]);
        $incidentType2Phase2Step4Field1->setFieldStyle([]);
        $incidentType2Phase2Step4Field1->setIncidentQuestion($question70);
        $incidentType2Phase2Step4Field1->setStep($incidentType2Phase2Step4);
        $incidentType2Phase2Step4Field1->setCreatedDate($currentDateTime);
        $incidentType2Phase2Step4Field1->setIsInApp(false);
        $incidentType2Phase2Step4Field1->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step4Field1);
        $this->em->flush();

        $incidentType2Phase2Step4Field2 = new IncidentFormFields();
        $incidentType2Phase2Step4Field2->setOrdering(2);
        $incidentType2Phase2Step4Field2->setLabel('');
        $incidentType2Phase2Step4Field2->setFieldId(null);
        $incidentType2Phase2Step4Field2->setFieldData([]);
        $incidentType2Phase2Step4Field2->setFieldRule([]);
        $incidentType2Phase2Step4Field2->setFieldStyle([]);
        $incidentType2Phase2Step4Field2->setIncidentQuestion($question71);
        $incidentType2Phase2Step4Field2->setStep($incidentType2Phase2Step4);
        $incidentType2Phase2Step4Field2->setCreatedDate($currentDateTime);
        $incidentType2Phase2Step4Field2->setIsInApp(false);
        $incidentType2Phase2Step4Field2->setIsInWeb(true);
        $this->em->persist($incidentType2Phase2Step4Field2);
        $this->em->flush();

        // incident type 2 end

        // inciden type 3 start
        //Incident Type Add
        $incidentType3 = new IncidentType();
        $incidentType3->setName('NCNS');
        $incidentType3->setPrefix('Ncn');
        $incidentType3->setCompany($company);
        $this->em->persist($incidentType3);
        $this->em->flush();

        // Incident Phase Add
        $incidentType3Phase1 = new IncidentPhase();
        $incidentType3Phase1->setTitle('NCNS Report');
        $incidentType3Phase1->setOrdering('1');
        //  $incidentType3Phase1->setCompany($company);
        $incidentType3Phase1->setRole($company->getCompanyRoles()[6]);
        $incidentType3Phase1->setIncidentType($incidentType3);
        $incidentType3Phase1->setIsInApp(false);
        $incidentType3Phase1->setIsInWeb(true);
        $incidentType3Phase1->setCreatedDate($currentDateTime);
        $this->em->persist($incidentType3Phase1);
        $this->em->flush();

        //Incident Phase 1 Steps 1
        // $incidentType3Phase1Step1 = new IncidentStep();
        // $incidentType3Phase1Step1->setPhase($incidentType3Phase1);
        // $incidentType3Phase1Step1->setTitle('Choose Driver');
        // $incidentType3Phase1Step1->setInstructions('Which driver did not show for work today?');
        // $incidentType3Phase1Step1->setStepNumber(1);
        // $incidentType3Phase1Step1->setOrdering(1);
        // $incidentType3Phase1Step1->setIsInApp(false);
        // $incidentType3Phase1Step1->setIsInWeb(true);
        // $this->em->persist($incidentType3Phase1Step1);
        // $this->em->flush();

        //Incident Form Fields
        // $incidentType3Phase1Step1Field1 = new IncidentFormFields();
        // $incidentType3Phase1Step1Field1->setOrdering(1);
        // $incidentType3Phase1Step1Field1->setLabel('');
        // $incidentType3Phase1Step1Field1->setFieldId(null);
        // $incidentType3Phase1Step1Field1->setFieldData([]);
        // $incidentType3Phase1Step1Field1->setFieldRule([]);
        // $incidentType3Phase1Step1Field1->setFieldStyle([]);
        // $incidentType3Phase1Step1Field1->setIncidentQuestion($question72);
        // $incidentType3Phase1Step1Field1->setStep($incidentType3Phase1Step1);
        // $incidentType3Phase1Step1Field1->setCreatedDate($currentDateTime);
        // $incidentType3Phase1Step1Field1->setIsInApp(false);
        // $incidentType3Phase1Step1Field1->setIsInWeb(true);
        // $this->em->persist($incidentType3Phase1Step1Field1);
        // $this->em->flush();

        //Incident Phase 1 Steps 2
        // $incidentType3Phase1Step2 = new IncidentStep();
        // $incidentType3Phase1Step2->setPhase($incidentType3Phase1);
        // $incidentType3Phase1Step2->setTitle('Location of NCNS');
        // $incidentType3Phase1Step2->setInstructions('What station was the reported no call no show for?');
        // $incidentType3Phase1Step2->setStepNumber(2);
        // $incidentType3Phase1Step2->setOrdering(2);
        // $incidentType3Phase1Step2->setIsInApp(false);
        // $incidentType3Phase1Step2->setIsInWeb(true);
        // $this->em->persist($incidentType3Phase1Step2);
        // $this->em->flush();

        //Incident Form Fields
        // $incidentType3Phase1Step2Field1 = new IncidentFormFields();
        // $incidentType3Phase1Step2Field1->setOrdering(1);
        // $incidentType3Phase1Step2Field1->setLabel('');
        // $incidentType3Phase1Step2Field1->setFieldId(null);
        // $incidentType3Phase1Step2Field1->setFieldData([]);
        // $incidentType3Phase1Step2Field1->setFieldRule([
        //     "depend" => ["isDepend" => true, "dependOn" => $incidentType3Phase1Step1Field1->getId()],
        //     "validations" => ["required" => "This fields is required!!!"],
        // ]);
        // $incidentType3Phase1Step2Field1->setFieldStyle([]);
        // $incidentType3Phase1Step2Field1->setIncidentQuestion($question73);
        // $incidentType3Phase1Step2Field1->setStep($incidentType3Phase1Step2);
        // $incidentType3Phase1Step2Field1->setCreatedDate($currentDateTime);
        // $incidentType3Phase1Step2Field1->setIsInApp(false);
        // $incidentType3Phase1Step2Field1->setIsInWeb(true);
        // $this->em->persist($incidentType3Phase1Step2Field1);
        // $this->em->flush();

        //Incident Phase 1 Steps 3
        $incidentType3Phase1Step3 = new IncidentStep();
        $incidentType3Phase1Step3->setPhase($incidentType3Phase1);
        $incidentType3Phase1Step3->setTitle('For which type of Shift did the Driver no call/no show?');
        $incidentType3Phase1Step3->setInstructions('Which shift did the driver not call and show for?');
        $incidentType3Phase1Step3->setStepNumber(3);
        $incidentType3Phase1Step3->setOrdering(3);
        $incidentType3Phase1Step3->setIsInApp(false);
        $incidentType3Phase1Step3->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step3);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase1Step3Field1 = new IncidentFormFields();
        $incidentType3Phase1Step3Field1->setOrdering(1);
        $incidentType3Phase1Step3Field1->setLabel('');
        $incidentType3Phase1Step3Field1->setFieldId(null);
        $incidentType3Phase1Step3Field1->setFieldData([]);
        $incidentType3Phase1Step3Field1->setFieldRule([
            "depend" => ["isDepend" => true, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => 'Station'],
            "validations" => ["required" => "This fields is required!!!"],
        ]);
        $incidentType3Phase1Step3Field1->setFieldStyle([]);
        $incidentType3Phase1Step3Field1->setIncidentQuestion($question74);
        $incidentType3Phase1Step3Field1->setStep($incidentType3Phase1Step3);
        $incidentType3Phase1Step3Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase1Step3Field1->setIsInApp(false);
        $incidentType3Phase1Step3Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step3Field1);
        $this->em->flush();

        //Incident Phase 1 Steps 4
        $incidentType3Phase1Step4 = new IncidentStep();
        $incidentType3Phase1Step4->setPhase($incidentType3Phase1);
        $incidentType3Phase1Step4->setTitle('Notify Driver?');
        $incidentType3Phase1Step4->setInstructions('Selecting this will notify the driver about this incident via text message.');
        $incidentType3Phase1Step4->setStepNumber(4);
        $incidentType3Phase1Step4->setOrdering(4);
        $incidentType3Phase1Step4->setIsInApp(false);
        $incidentType3Phase1Step4->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step4);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase1Step4Field1 = new IncidentFormFields();
        $incidentType3Phase1Step4Field1->setOrdering(1);
        $incidentType3Phase1Step4Field1->setLabel('');
        $incidentType3Phase1Step4Field1->setFieldId(null);
        $incidentType3Phase1Step4Field1->setFieldData([]);
        $incidentType3Phase1Step4Field1->setFieldRule([]);
        $incidentType3Phase1Step4Field1->setFieldStyle([]);
        $incidentType3Phase1Step4Field1->setIncidentQuestion($question75);
        $incidentType3Phase1Step4Field1->setStep($incidentType3Phase1Step4);
        $incidentType3Phase1Step4Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase1Step4Field1->setIsInApp(false);
        $incidentType3Phase1Step4Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step4Field1);
        $this->em->flush();

        //Incident Phase 1 Steps 5
        $incidentType3Phase1Step5 = new IncidentStep();
        $incidentType3Phase1Step5->setPhase($incidentType3Phase1);
        $incidentType3Phase1Step5->setTitle('NCNS Shift Time');
        $incidentType3Phase1Step5->setInstructions('Was the shift during the weekday or on the weekend?');
        $incidentType3Phase1Step5->setStepNumber(4);
        $incidentType3Phase1Step5->setOrdering(4);
        $incidentType3Phase1Step5->setIsInApp(false);
        $incidentType3Phase1Step5->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step5);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase1Step5Field1 = new IncidentFormFields();
        $incidentType3Phase1Step5Field1->setOrdering(1);
        $incidentType3Phase1Step5Field1->setLabel('');
        $incidentType3Phase1Step5Field1->setFieldId(null);
        $incidentType3Phase1Step5Field1->setFieldData([]);
        $incidentType3Phase1Step5Field1->setFieldRule([]);
        $incidentType3Phase1Step5Field1->setFieldStyle([]);
        $incidentType3Phase1Step5Field1->setIncidentQuestion($question76);
        $incidentType3Phase1Step5Field1->setStep($incidentType3Phase1Step5);
        $incidentType3Phase1Step5Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase1Step5Field1->setIsInApp(false);
        $incidentType3Phase1Step5Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step5Field1);
        $this->em->flush();

        $incidentType3Phase1Step5Field2 = new IncidentFormFields();
        $incidentType3Phase1Step5Field2->setOrdering(2);
        $incidentType3Phase1Step5Field2->setLabel('Notes');
        $incidentType3Phase1Step5Field2->setFieldId(null);
        $incidentType3Phase1Step5Field2->setFieldData([]);
        $incidentType3Phase1Step5Field2->setFieldRule([]);
        $incidentType3Phase1Step5Field2->setFieldStyle([]);
        $incidentType3Phase1Step5Field2->setIncidentQuestion($question64a);
        $incidentType3Phase1Step5Field2->setStep($incidentType3Phase1Step5);
        $incidentType3Phase1Step5Field2->setCreatedDate($currentDateTime);
        $incidentType3Phase1Step5Field2->setIsInApp(false);
        $incidentType3Phase1Step5Field2->setIsInWeb(true);
        $this->em->persist($incidentType3Phase1Step5Field2);
        $this->em->flush();

        // Incident Phase Add
        $incidentType3Phase2 = new IncidentPhase();
        $incidentType3Phase2->setTitle('Review');
        $incidentType3Phase2->setOrdering(2);
        //$incidentType3Phase2->setCompany($company);
        $incidentType3Phase2->setRole($company->getCompanyRoles()[2]); //manager
        $incidentType3Phase2->setIncidentType($incidentType3);
        $incidentType3Phase2->setCreatedDate($currentDateTime);
        $incidentType3Phase2->setIsInApp(false);
        $incidentType3Phase2->setIsInWeb(true);
        $incidentType3Phase2->setAllocatedTime(1440);
        $this->em->persist($incidentType3Phase2);
        $this->em->flush();

        //Incident Phase 2 Steps 1
        $incidentType3Phase2Step1 = new IncidentStep();
        $incidentType3Phase2Step1->setPhase($incidentType3Phase2);
        $incidentType3Phase2Step1->setTitle('Was driver contacted?');
        $incidentType3Phase2Step1->setInstructions('Did anyone contact the driver and if so what method?');
        $incidentType3Phase2Step1->setStepNumber(1);
        $incidentType3Phase2Step1->setOrdering(1);
        $incidentType3Phase2Step1->setIsInApp(false);
        $incidentType3Phase2Step1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step1);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase2Step1Field1 = new IncidentFormFields();
        $incidentType3Phase2Step1Field1->setOrdering(1);
        $incidentType3Phase2Step1Field1->setLabel('');
        $incidentType3Phase2Step1Field1->setFieldId(null);
        $incidentType3Phase2Step1Field1->setFieldData([]);
        $incidentType3Phase2Step1Field1->setFieldRule([]);
        $incidentType3Phase2Step1Field1->setFieldStyle([]);
        $incidentType3Phase2Step1Field1->setIncidentQuestion($question77);
        $incidentType3Phase2Step1Field1->setStep($incidentType3Phase2Step1);
        $incidentType3Phase2Step1Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step1Field1->setIsInApp(false);
        $incidentType3Phase2Step1Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step1Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 2
        $incidentType3Phase2Step2 = new IncidentStep();
        $incidentType3Phase2Step2->setPhase($incidentType3Phase2);
        $incidentType3Phase2Step2->setTitle('Excused?');
        $incidentType3Phase2Step2->setInstructions('Is there a reason that the NCNS is excused?');
        $incidentType3Phase2Step2->setStepNumber(2);
        $incidentType3Phase2Step2->setOrdering(2);
        $incidentType3Phase2Step2->setIsInApp(false);
        $incidentType3Phase2Step2->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step2);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase2Step2Field1 = new IncidentFormFields();
        $incidentType3Phase2Step2Field1->setOrdering(1);
        $incidentType3Phase2Step2Field1->setLabel('');
        $incidentType3Phase2Step2Field1->setFieldId(null);
        $incidentType3Phase2Step2Field1->setFieldData([]);
        $incidentType3Phase2Step2Field1->setFieldRule([]);
        $incidentType3Phase2Step2Field1->setFieldStyle([]);
        $incidentType3Phase2Step2Field1->setIncidentQuestion($question78);
        $incidentType3Phase2Step2Field1->setStep($incidentType3Phase2Step2);
        $incidentType3Phase2Step2Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step2Field1->setIsInApp(false);
        $incidentType3Phase2Step2Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step2Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 3
        $incidentType3Phase2Step3 = new IncidentStep();
        $incidentType3Phase2Step3->setPhase($incidentType3Phase2);
        $incidentType3Phase2Step3->setTitle('Upload Material');
        $incidentType3Phase2Step3->setInstructions('What material is to be given if there is an excuse?');
        $incidentType3Phase2Step3->setStepNumber(3);
        $incidentType3Phase2Step3->setOrdering(3);
        $incidentType3Phase2Step3->setIsInApp(false);
        $incidentType3Phase2Step3->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step3);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase2Step3Field1 = new IncidentFormFields();
        $incidentType3Phase2Step3Field1->setOrdering(1);
        $incidentType3Phase2Step3Field1->setLabel('');
        $incidentType3Phase2Step3Field1->setFieldId(null);
        $incidentType3Phase2Step3Field1->setFieldData([]);
        $incidentType3Phase2Step3Field1->setFieldRule([]);
        $incidentType3Phase2Step3Field1->setFieldStyle([]);
        $incidentType3Phase2Step3Field1->setIncidentQuestion($question79);
        $incidentType3Phase2Step3Field1->setStep($incidentType3Phase2Step3);
        $incidentType3Phase2Step3Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step3Field1->setIsInApp(false);
        $incidentType3Phase2Step3Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step3Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 4
        $incidentType3Phase2Step4 = new IncidentStep();
        $incidentType3Phase2Step4->setPhase($incidentType3Phase2);
        $incidentType3Phase2Step4->setTitle('Driver Strikes?');
        $incidentType3Phase2Step4->setInstructions('If not excused how many strikes is this?');
        $incidentType3Phase2Step4->setStepNumber(4);
        $incidentType3Phase2Step4->setOrdering(4);
        $incidentType3Phase2Step4->setIsInApp(false);
        $incidentType3Phase2Step4->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step4);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase2Step4Field1 = new IncidentFormFields();
        $incidentType3Phase2Step4Field1->setOrdering(1);
        $incidentType3Phase2Step4Field1->setLabel('');
        $incidentType3Phase2Step4Field1->setFieldId(null);
        $incidentType3Phase2Step4Field1->setFieldData([]);
        $incidentType3Phase2Step4Field1->setFieldRule([]);
        $incidentType3Phase2Step4Field1->setFieldStyle([]);
        $incidentType3Phase2Step4Field1->setIncidentQuestion($question80);
        $incidentType3Phase2Step4Field1->setStep($incidentType3Phase2Step4);
        $incidentType3Phase2Step4Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step4Field1->setIsInApp(false);
        $incidentType3Phase2Step4Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step4Field1);
        $this->em->flush();

        //Incident Phase 2 Steps 5
        $incidentType3Phase2Step5 = new IncidentStep();
        $incidentType3Phase2Step5->setPhase($incidentType3Phase2);
        $incidentType3Phase2Step5->setTitle('Correction Action');
        $incidentType3Phase2Step5->setInstructions('If strike given should we deliver a correction action form to the driver? If Strike is not given - then correction action is optional.');
        $incidentType3Phase2Step5->setStepNumber(5);
        $incidentType3Phase2Step5->setOrdering(5);
        $incidentType3Phase2Step5->setIsInApp(false);
        $incidentType3Phase2Step5->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step5);
        $this->em->flush();

        //Incident Form Fields
        $incidentType3Phase2Step5Field1 = new IncidentFormFields();
        $incidentType3Phase2Step5Field1->setOrdering(1);
        $incidentType3Phase2Step5Field1->setLabel('');
        $incidentType3Phase2Step5Field1->setFieldId(null);
        $incidentType3Phase2Step5Field1->setFieldData([]);
        $incidentType3Phase2Step5Field1->setFieldRule([]);
        $incidentType3Phase2Step5Field1->setFieldStyle([]);
        $incidentType3Phase2Step5Field1->setIncidentQuestion($question81);
        $incidentType3Phase2Step5Field1->setStep($incidentType3Phase2Step5);
        $incidentType3Phase2Step5Field1->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step5Field1->setIsInApp(false);
        $incidentType3Phase2Step5Field1->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step5Field1);
        $this->em->flush();

        $incidentType3Phase2Step5Field2 = new IncidentFormFields();
        $incidentType3Phase2Step5Field2->setOrdering(2);
        $incidentType3Phase2Step5Field2->setLabel('');
        $incidentType3Phase2Step5Field2->setFieldId(null);
        $incidentType3Phase2Step5Field2->setFieldData([]);
        $incidentType3Phase2Step5Field2->setFieldRule([]);
        $incidentType3Phase2Step5Field2->setFieldStyle([]);
        $incidentType3Phase2Step5Field2->setIncidentQuestion($question82);
        $incidentType3Phase2Step5Field2->setStep($incidentType3Phase2Step5);
        $incidentType3Phase2Step5Field2->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step5Field2->setIsInApp(false);
        $incidentType3Phase2Step5Field2->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step5Field2);
        $this->em->flush();

        $incidentType3Phase2Step5Field3 = new IncidentFormFields();
        $incidentType3Phase2Step5Field3->setOrdering(3);
        $incidentType3Phase2Step5Field3->setLabel('');
        $incidentType3Phase2Step5Field3->setFieldId(null);
        $incidentType3Phase2Step5Field3->setFieldData([]);
        $incidentType3Phase2Step5Field3->setFieldRule([]);
        $incidentType3Phase2Step5Field3->setFieldStyle([]);
        $incidentType3Phase2Step5Field3->setIncidentQuestion($question64a);
        $incidentType3Phase2Step5Field3->setStep($incidentType3Phase2Step5);
        $incidentType3Phase2Step5Field3->setCreatedDate($currentDateTime);
        $incidentType3Phase2Step5Field3->setIsInApp(false);
        $incidentType3Phase2Step5Field3->setIsInWeb(true);
        $this->em->persist($incidentType3Phase2Step5Field3);
        $this->em->flush();

        // inciden type 3 end

        // inciden type 4 start
        //Incident Type Add
        // $incidentType4 = new IncidentType();
        // $incidentType4->setName('Safety And Compliance');
        // $incidentType4->setPrefix('Saf');
        // $incidentType4->setCompany($company);
        // $this->em->persist($incidentType4);
        // $this->em->flush();
        //
        // // Incident Phase Add
        // $incidentType4Phase1 = new IncidentPhase();
        // $incidentType4Phase1->setTitle('Late Attendance Report');
        // $incidentType4Phase1->setOrdering(1);
        // //$incidentType4Phase1->setCompany($company);
        // $incidentType4Phase1->setRole($company->getCompanyRoles()[6]); //owner
        // $incidentType4Phase1->setIncidentType($incidentType4);
        // $incidentType4Phase1->setCreatedDate($currentDateTime);
        // $incidentType4Phase1->setIsInApp(false);
        // $incidentType4Phase1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1);
        // $this->em->flush();
        //
        // //Incident Phase 1 Steps 1
        // $incidentType4Phase1Step1 = new IncidentStep();
        // $incidentType4Phase1Step1->setPhase($incidentType4Phase1);
        // $incidentType4Phase1Step1->setTitle('Choose Driver');
        // $incidentType4Phase1Step1->setInstructions('Which driver was late for work today?');
        // $incidentType4Phase1Step1->setStepNumber(1);
        // $incidentType4Phase1Step1->setOrdering(1);
        // $incidentType4Phase1Step1->setIsInApp(false);
        // $incidentType4Phase1Step1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step1);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase1Step1Field1 = new IncidentFormFields();
        // $incidentType4Phase1Step1Field1->setOrdering(1);
        // $incidentType4Phase1Step1Field1->setLabel('');
        // $incidentType4Phase1Step1Field1->setFieldId(null);
        // $incidentType4Phase1Step1Field1->setFieldData([]);
        // $incidentType4Phase1Step1Field1->setFieldRule([]);
        // $incidentType4Phase1Step1Field1->setFieldStyle([]);
        // $incidentType4Phase1Step1Field1->setIncidentQuestion($question72);
        // $incidentType4Phase1Step1Field1->setStep($incidentType4Phase1Step1);
        // $incidentType4Phase1Step1Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase1Step1Field1->setIsInApp(false);
        // $incidentType4Phase1Step1Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step1Field1);
        // $this->em->flush();
        //
        // //Incident Phase 1 Steps 2
        // $incidentType4Phase1Step2 = new IncidentStep();
        // $incidentType4Phase1Step2->setPhase($incidentType4Phase1);
        // $incidentType4Phase1Step2->setTitle('Location of late arrival');
        // $incidentType4Phase1Step2->setInstructions('What station was the driver reported late to?');
        // $incidentType4Phase1Step2->setStepNumber(1);
        // $incidentType4Phase1Step2->setOrdering(1);
        // $incidentType4Phase1Step2->setIsInApp(false);
        // $incidentType4Phase1Step2->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step2);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase1Step2Field1 = new IncidentFormFields();
        // $incidentType4Phase1Step2Field1->setOrdering(1);
        // $incidentType4Phase1Step2Field1->setLabel('');
        // $incidentType4Phase1Step2Field1->setFieldId(null);
        // $incidentType4Phase1Step2Field1->setFieldData([]);
        // $incidentType4Phase1Step2Field1->setFieldRule([]);
        // $incidentType4Phase1Step2Field1->setFieldStyle([]);
        // $incidentType4Phase1Step2Field1->setIncidentQuestion($question73);
        // $incidentType4Phase1Step2Field1->setStep($incidentType4Phase1Step2);
        // $incidentType4Phase1Step2Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase1Step2Field1->setIsInApp(false);
        // $incidentType4Phase1Step2Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step2Field1);
        // $this->em->flush();
        //
        // //Incident Phase 1 Steps 3
        // $incidentType4Phase1Step3 = new IncidentStep();
        // $incidentType4Phase1Step3->setPhase($incidentType4Phase1);
        // $incidentType4Phase1Step3->setTitle('Did the driver inform station before statrt of shift?');
        // $incidentType4Phase1Step3->setInstructions('Select what method the driver communicated late arrival.');
        // $incidentType4Phase1Step3->setStepNumber(1);
        // $incidentType4Phase1Step3->setOrdering(1);
        // $incidentType4Phase1Step3->setIsInApp(false);
        // $incidentType4Phase1Step3->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step3);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase1Step3Field1 = new IncidentFormFields();
        // $incidentType4Phase1Step3Field1->setOrdering(1);
        // $incidentType4Phase1Step3Field1->setLabel('');
        // $incidentType4Phase1Step3Field1->setFieldId(null);
        // $incidentType4Phase1Step3Field1->setFieldData([]);
        // $incidentType4Phase1Step3Field1->setFieldRule([]);
        // $incidentType4Phase1Step3Field1->setFieldStyle([]);
        // $incidentType4Phase1Step3Field1->setIncidentQuestion($question83);
        // $incidentType4Phase1Step3Field1->setStep($incidentType4Phase1Step3);
        // $incidentType4Phase1Step3Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase1Step3Field1->setIsInApp(false);
        // $incidentType4Phase1Step3Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step3Field1);
        // $this->em->flush();
        //
        // //Incident Phase 1 Steps 4
        // $incidentType4Phase1Step4 = new IncidentStep();
        // $incidentType4Phase1Step4->setPhase($incidentType4Phase1);
        // $incidentType4Phase1Step4->setTitle('Notify Driver?');
        // $incidentType4Phase1Step4->setInstructions('Selecting this will notify the driver about this incident via text message.');
        // $incidentType4Phase1Step4->setStepNumber(1);
        // $incidentType4Phase1Step4->setOrdering(1);
        // $incidentType4Phase1Step4->setIsInApp(false);
        // $incidentType4Phase1Step4->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step4);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase1Step4Field1 = new IncidentFormFields();
        // $incidentType4Phase1Step4Field1->setOrdering(1);
        // $incidentType4Phase1Step4Field1->setLabel('');
        // $incidentType4Phase1Step4Field1->setFieldId(null);
        // $incidentType4Phase1Step4Field1->setFieldData([]);
        // $incidentType4Phase1Step4Field1->setFieldRule([]);
        // $incidentType4Phase1Step4Field1->setFieldStyle([]);
        // $incidentType4Phase1Step4Field1->setIncidentQuestion($question84);
        // $incidentType4Phase1Step4Field1->setStep($incidentType4Phase1Step4);
        // $incidentType4Phase1Step4Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase1Step4Field1->setIsInApp(false);
        // $incidentType4Phase1Step4Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase1Step4Field1);
        // $this->em->flush();
        //
        // // Incident Phase Add
        // $incidentType4Phase2 = new IncidentPhase();
        // $incidentType4Phase2->setTitle('Review');
        // $incidentType4Phase2->setOrdering(1);
        // //$incidentType4Phase2->setCompany($company);
        // $incidentType4Phase2->setRole($company->getCompanyRoles()[2]); //manager
        // $incidentType4Phase2->setIncidentType($incidentType4);
        // $incidentType4Phase2->setCreatedDate($currentDateTime);
        // $incidentType4Phase2->setIsInApp(false);
        // $incidentType4Phase2->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2);
        // $this->em->flush();
        //
        // //Incident Phase 2 Steps 1
        // $incidentType4Phase2Step1 = new IncidentStep();
        // $incidentType4Phase2Step1->setPhase($incidentType4Phase2);
        // $incidentType4Phase2Step1->setTitle('Excused?');
        // $incidentType4Phase2Step1->setInstructions('Is there a reason that the NCNS is excused?');
        // $incidentType4Phase2Step1->setStepNumber(1);
        // $incidentType4Phase2Step1->setOrdering(1);
        // $incidentType4Phase2Step1->setIsInApp(false);
        // $incidentType4Phase2Step1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step1);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase2Step1Field1 = new IncidentFormFields();
        // $incidentType4Phase2Step1Field1->setOrdering(1);
        // $incidentType4Phase2Step1Field1->setLabel('');
        // $incidentType4Phase2Step1Field1->setFieldId(null);
        // $incidentType4Phase2Step1Field1->setFieldData([]);
        // $incidentType4Phase2Step1Field1->setFieldRule([]);
        // $incidentType4Phase2Step1Field1->setFieldStyle([]);
        // $incidentType4Phase2Step1Field1->setIncidentQuestion($question85);
        // $incidentType4Phase2Step1Field1->setStep($incidentType4Phase2Step1);
        // $incidentType4Phase2Step1Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase2Step1Field1->setIsInApp(false);
        // $incidentType4Phase2Step1Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step1Field1);
        // $this->em->flush();
        //
        // //Incident Phase 2 Steps 2
        // $incidentType4Phase2Step2 = new IncidentStep();
        // $incidentType4Phase2Step2->setPhase($incidentType4Phase2);
        // $incidentType4Phase2Step2->setTitle('Upload Material');
        // $incidentType4Phase2Step2->setInstructions('What material is to be given if there is an excuse?');
        // $incidentType4Phase2Step2->setStepNumber(1);
        // $incidentType4Phase2Step2->setOrdering(1);
        // $incidentType4Phase2Step2->setIsInApp(false);
        // $incidentType4Phase2Step2->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step2);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase2Step2Field1 = new IncidentFormFields();
        // $incidentType4Phase2Step2Field1->setOrdering(1);
        // $incidentType4Phase2Step2Field1->setLabel('');
        // $incidentType4Phase2Step2Field1->setFieldId(null);
        // $incidentType4Phase2Step2Field1->setFieldData([]);
        // $incidentType4Phase2Step2Field1->setFieldRule([]);
        // $incidentType4Phase2Step2Field1->setFieldStyle([]);
        // $incidentType4Phase2Step2Field1->setIncidentQuestion($question86);
        // $incidentType4Phase2Step2Field1->setStep($incidentType4Phase2Step2);
        // $incidentType4Phase2Step2Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase2Step2Field1->setIsInApp(false);
        // $incidentType4Phase2Step2Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step2Field1);
        // $this->em->flush();
        //
        // //Incident Phase 2 Steps 3
        // $incidentType4Phase2Step3 = new IncidentStep();
        // $incidentType4Phase2Step3->setPhase($incidentType4Phase2);
        // $incidentType4Phase2Step3->setTitle('Correction Action');
        // $incidentType4Phase2Step3->setInstructions('Was corrective action provided?');
        // $incidentType4Phase2Step3->setStepNumber(1);
        // $incidentType4Phase2Step3->setOrdering(1);
        // $incidentType4Phase2Step3->setIsInApp(false);
        // $incidentType4Phase2Step3->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step3);
        // $this->em->flush();
        //
        // //Incident Form Fields
        // $incidentType4Phase2Step3Field1 = new IncidentFormFields();
        // $incidentType4Phase2Step3Field1->setOrdering(1);
        // $incidentType4Phase2Step3Field1->setLabel('');
        // $incidentType4Phase2Step3Field1->setFieldId(null);
        // $incidentType4Phase2Step3Field1->setFieldData([]);
        // $incidentType4Phase2Step3Field1->setFieldRule([]);
        // $incidentType4Phase2Step3Field1->setFieldStyle([]);
        // $incidentType4Phase2Step3Field1->setIncidentQuestion($question87);
        // $incidentType4Phase2Step3Field1->setStep($incidentType4Phase2Step3);
        // $incidentType4Phase2Step3Field1->setCreatedDate($currentDateTime);
        // $incidentType4Phase2Step3Field1->setIsInApp(false);
        // $incidentType4Phase2Step3Field1->setIsInWeb(true);
        // $this->em->persist($incidentType4Phase2Step3Field1);
        // $this->em->flush();

        // inciden type 4 end

        // // inciden type 5 start
        //         //Incident Type Add
        //         $incidentType5 = new IncidentType();
        //         $incidentType5->setName('Feedback');
        //         $incidentType5->setCompany($company);
        //         $this->em->persist($incidentType5);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType5Phase1 = new IncidentPhase();
        //             $incidentType5Phase1->setTitle('Roadside Incident');
        //             $incidentType5Phase1->setOrdering(1);
        //             $incidentType5Phase1->setCompany($company);
        //             $incidentType5Phase1->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType5Phase1->setIncidentType($incidentType5);
        //             $incidentType5Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType5Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType5Phase1Step1 = new IncidentStep();
        //                 $incidentType5Phase1Step1->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step1->setTitle('Select Drivers');
        //                 $incidentType5Phase1Step1->setInstructions('');
        //                 $incidentType5Phase1Step1->setStepNumber(1);
        //                 $incidentType5Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType5Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step1FormField1->setTitle('');
        //                     $incidentType5Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step1FormField1->setFormType('Select');
        //                     $incidentType5Phase1Step1FormField1->setValue([]);
        //                     $incidentType5Phase1Step1FormField1->setOptions([]);
        //                     $incidentType5Phase1Step1FormField1->setStep($incidentType5Phase1Step1);
        //                     $incidentType5Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 2
        //                 $incidentType5Phase1Step2 = new IncidentStep();
        //                 $incidentType5Phase1Step2->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step2->setTitle('Assistance Type');
        //                 $incidentType5Phase1Step2->setInstructions('');
        //                 $incidentType5Phase1Step2->setStepNumber(2);
        //                 $incidentType5Phase1Step2->setOrdering(2);
        //                 $this->em->persist($incidentType5Phase1Step2);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step2FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step2FormField1->setTitle('');
        //                     $incidentType5Phase1Step2FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step2FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step2FormField1->setFormType('Select');
        //                     $incidentType5Phase1Step2FormField1->setValue(['Flat Tire' => 1, 'Lockout' => 2,'Lost Key' => 3, 'Van Breakdown' => 4]);
        //                     $incidentType5Phase1Step2FormField1->setOptions([]);
        //                     $incidentType5Phase1Step2FormField1->setStep($incidentType5Phase1Step2);
        //                     $incidentType5Phase1Step2FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step2FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 3
        //                 $incidentType5Phase1Step3 = new IncidentStep();
        //                 $incidentType5Phase1Step3->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step3->setTitle('Date Needed');
        //                 $incidentType5Phase1Step3->setInstructions('When is the assistance needed?');
        //                 $incidentType5Phase1Step3->setStepNumber(3);
        //                 $incidentType5Phase1Step3->setOrdering(3);
        //                 $this->em->persist($incidentType5Phase1Step3);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step3FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step3FormField1->setTitle('');
        //                     $incidentType5Phase1Step3FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step3FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step3FormField1->setFormType('date');
        //                     $incidentType5Phase1Step3FormField1->setValue([]);
        //                     $incidentType5Phase1Step3FormField1->setOptions([]);
        //                     $incidentType5Phase1Step3FormField1->setStep($incidentType5Phase1Step3);
        //                     $incidentType5Phase1Step3FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step3FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 4
        //                 $incidentType5Phase1Step4 = new IncidentStep();
        //                 $incidentType5Phase1Step4->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step4->setTitle('Location');
        //                 $incidentType5Phase1Step4->setInstructions('Location of where assistance is needed.');
        //                 $incidentType5Phase1Step4->setStepNumber(4);
        //                 $incidentType5Phase1Step4->setOrdering(4);
        //                 $this->em->persist($incidentType5Phase1Step4);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step4FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step4FormField1->setTitle('');
        //                     $incidentType5Phase1Step4FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step4FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step4FormField1->setFormType('Input');
        //                     $incidentType5Phase1Step4FormField1->setValue([]);
        //                     $incidentType5Phase1Step4FormField1->setOptions([]);
        //                     $incidentType5Phase1Step4FormField1->setStep($incidentType5Phase1Step4);
        //                     $incidentType5Phase1Step4FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step4FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 5
        //                 $incidentType5Phase1Step5 = new IncidentStep();
        //                 $incidentType5Phase1Step5->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step5->setTitle('Photos');
        //                 $incidentType5Phase1Step5->setInstructions('If photos are required please upload');
        //                 $incidentType5Phase1Step5->setStepNumber(5);
        //                 $incidentType5Phase1Step5->setOrdering(5);
        //                 $this->em->persist($incidentType5Phase1Step5);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step5FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step5FormField1->setTitle('');
        //                     $incidentType5Phase1Step5FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step5FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step5FormField1->setFormType('file');
        //                     $incidentType5Phase1Step5FormField1->setValue([]);
        //                     $incidentType5Phase1Step5FormField1->setOptions([]);
        //                     $incidentType5Phase1Step5FormField1->setStep($incidentType5Phase1Step5);
        //                     $incidentType5Phase1Step5FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step5FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 6
        //                 $incidentType5Phase1Step6 = new IncidentStep();
        //                 $incidentType5Phase1Step6->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step6->setTitle('Was dispatched notified?');
        //                 $incidentType5Phase1Step6->setInstructions('Please determine if dispatch was notified.');
        //                 $incidentType5Phase1Step6->setStepNumber(6);
        //                 $incidentType5Phase1Step6->setOrdering(6);
        //                 $this->em->persist($incidentType5Phase1Step6);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step6FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step6FormField1->setTitle('');
        //                     $incidentType5Phase1Step6FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step6FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step6FormField1->setFormType('Radio');
        //                     $incidentType5Phase1Step6FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType5Phase1Step6FormField1->setOptions([]);
        //                     $incidentType5Phase1Step6FormField1->setStep($incidentType5Phase1Step6);
        //                     $incidentType5Phase1Step6FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step6FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 7
        //                 $incidentType5Phase1Step7 = new IncidentStep();
        //                 $incidentType5Phase1Step7->setPhase($incidentType5Phase1);
        //                 $incidentType5Phase1Step7->setTitle('Issue description');
        //                 $incidentType5Phase1Step7->setInstructions('Please describe in as much detail as possible the assistance that is required for this incident.');
        //                 $incidentType5Phase1Step7->setStepNumber(7);
        //                 $incidentType5Phase1Step7->setOrdering(7);
        //                 $this->em->persist($incidentType5Phase1Step7);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType5Phase1Step7FormField1 = new IncidentFormFields();
        //                     $incidentType5Phase1Step7FormField1->setTitle('Issue');
        //                     $incidentType5Phase1Step7FormField1->setOrdering(1);
        //                     $incidentType5Phase1Step7FormField1->setErrorMsg('');
        //                     $incidentType5Phase1Step7FormField1->setFormType('Input');
        //                     $incidentType5Phase1Step7FormField1->setValue([]);
        //                     $incidentType5Phase1Step7FormField1->setOptions([]);
        //                     $incidentType5Phase1Step7FormField1->setStep($incidentType5Phase1Step7);
        //                     $incidentType5Phase1Step7FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType5Phase1Step7FormField1);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType5Phase2 = new IncidentPhase();
        //             $incidentType5Phase2->setTitle('Dispatch Review');
        //             $incidentType5Phase2->setOrdering(2);
        //             $incidentType5Phase2->setCompany($company);
        //             $incidentType5Phase2->setRole($company->getCompanyRoles()[0]); //dispacher
        //             $incidentType5Phase2->setIncidentType($incidentType5);
        //             $incidentType5Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType5Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 // $incidentType5Phase2Step1 = new IncidentStep();
        //                 // $incidentType5Phase2Step1->setPhase($incidentType5Phase2);
        //                 // $incidentType5Phase2Step1->setTitle('');
        //                 // $incidentType5Phase2Step1->setInstructions('');
        //                 // $incidentType5Phase2Step1->setStepNumber(1);
        //                 // $incidentType5Phase2Step1->setOrdering(1);
        //                 // $this->em->persist($incidentType5Phase2Step1);
        //                 // $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     // $incidentType5Phase2Step1FormField1 = new IncidentFormFields();
        //                     // $incidentType5Phase2Step1FormField1->setTitle('');
        //                     // $incidentType5Phase2Step1FormField1->setOrdering(1);
        //                     // $incidentType5Phase2Step1FormField1->setErrorMsg('');
        //                     // $incidentType5Phase2Step1FormField1->setFormType('');
        //                     // $incidentType5Phase2Step1FormField1->setValue([]);
        //                     // $incidentType5Phase2Step1FormField1->setOptions([]);
        //                     // $incidentType5Phase2Step1FormField1->setStep($incidentType5Phase2Step1);
        //                     // $incidentType5Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     // $this->em->persist($incidentType5Phase2Step1FormField1);
        //                     // $this->em->flush();
        //
        // // inciden type 5 end
        //
        // // inciden type 6 start
        //         //Incident Type Add
        //         $incidentType6 = new IncidentType();
        //         $incidentType6->setName('Repair');
        //         $incidentType6->setCompany($company);
        //         $this->em->persist($incidentType6);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType6Phase1 = new IncidentPhase();
        //             $incidentType6Phase1->setTitle('');
        //             $incidentType6Phase1->setOrdering(1);
        //             $incidentType6Phase1->setCompany($company);
        //             $incidentType6Phase1->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType6Phase1->setIncidentType($incidentType6);
        //             $incidentType6Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType6Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType6Phase1Step1 = new IncidentStep();
        //                 $incidentType6Phase1Step1->setPhase($incidentType6Phase1);
        //                 $incidentType6Phase1Step1->setTitle('');
        //                 $incidentType6Phase1Step1->setInstructions('');
        //                 $incidentType6Phase1Step1->setStepNumber(1);
        //                 $incidentType6Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType6Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType6Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField1->setTitle('Party responsible');
        //                     $incidentType6Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType6Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField1->setFormType('Select');
        //                     $incidentType6Phase1Step1FormField1->setValue([]);
        //                     $incidentType6Phase1Step1FormField1->setOptions([]);
        //                     $incidentType6Phase1Step1FormField1->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField2->setTitle('Party disrespected');
        //                     $incidentType6Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType6Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField2->setFormType('Select');
        //                     $incidentType6Phase1Step1FormField2->setValue(['Amazon' => 1, 'Dispatch' => 2,'Fellow Employee' => 3, 'Manager' => 4, 'Customer' => 5, 'Other' => 6]);
        //                     $incidentType6Phase1Step1FormField2->setOptions([]);
        //                     $incidentType6Phase1Step1FormField2->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField3 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField3->setTitle('Assigned To');
        //                     $incidentType6Phase1Step1FormField3->setOrdering(3);
        //                     $incidentType6Phase1Step1FormField3->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField3->setFormType('Input');
        //                     $incidentType6Phase1Step1FormField3->setValue([]);
        //                     $incidentType6Phase1Step1FormField3->setOptions([]);
        //                     $incidentType6Phase1Step1FormField3->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField4 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField4->setTitle('Reviewed By');
        //                     $incidentType6Phase1Step1FormField4->setOrdering(4);
        //                     $incidentType6Phase1Step1FormField4->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField4->setFormType('Input');
        //                     $incidentType6Phase1Step1FormField4->setValue([]);
        //                     $incidentType6Phase1Step1FormField4->setOptions([]);
        //                     $incidentType6Phase1Step1FormField4->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField4);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField5 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField5->setTitle('Record Date of incident');
        //                     $incidentType6Phase1Step1FormField5->setOrdering(5);
        //                     $incidentType6Phase1Step1FormField5->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField5->setFormType('date');
        //                     $incidentType6Phase1Step1FormField5->setValue([]);
        //                     $incidentType6Phase1Step1FormField5->setOptions([]);
        //                     $incidentType6Phase1Step1FormField5->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField5->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField5);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField6 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField6->setTitle('Record time of incident');
        //                     $incidentType6Phase1Step1FormField6->setOrdering(6);
        //                     $incidentType6Phase1Step1FormField6->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField6->setFormType('time');
        //                     $incidentType6Phase1Step1FormField6->setValue([]);
        //                     $incidentType6Phase1Step1FormField6->setOptions([]);
        //                     $incidentType6Phase1Step1FormField6->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField6->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField6);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField7 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField7->setTitle('Describe incident of disrespect');
        //                     $incidentType6Phase1Step1FormField7->setOrdering(7);
        //                     $incidentType6Phase1Step1FormField7->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField7->setFormType('Input');
        //                     $incidentType6Phase1Step1FormField7->setValue([]);
        //                     $incidentType6Phase1Step1FormField7->setOptions([]);
        //                     $incidentType6Phase1Step1FormField7->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField7->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField7);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase1Step1FormField8 = new IncidentFormFields();
        //                     $incidentType6Phase1Step1FormField8->setTitle('Upload files/documentation');
        //                     $incidentType6Phase1Step1FormField8->setOrdering(8);
        //                     $incidentType6Phase1Step1FormField8->setErrorMsg('');
        //                     $incidentType6Phase1Step1FormField8->setFormType('Input');
        //                     $incidentType6Phase1Step1FormField8->setValue([]);
        //                     $incidentType6Phase1Step1FormField8->setOptions([]);
        //                     $incidentType6Phase1Step1FormField8->setStep($incidentType6Phase1Step1);
        //                     $incidentType6Phase1Step1FormField8->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase1Step1FormField8);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType6Phase2 = new IncidentPhase();
        //             $incidentType6Phase2->setTitle('');
        //             $incidentType6Phase2->setOrdering(2);
        //             $incidentType6Phase2->setCompany($company);
        //             $incidentType6Phase2->setRole($company->getCompanyRoles()[2]); //manager
        //             $incidentType6Phase2->setIncidentType($incidentType6);
        //             $incidentType6Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType6Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType6Phase2Step1 = new IncidentStep();
        //                 $incidentType6Phase2Step1->setPhase($incidentType6Phase2);
        //                 $incidentType6Phase2Step1->setTitle('');
        //                 $incidentType6Phase2Step1->setInstructions('');
        //                 $incidentType6Phase2Step1->setStepNumber(1);
        //                 $incidentType6Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType6Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType6Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField1->setTitle('Review Incident Details');
        //                     $incidentType6Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType6Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField1->setFormType('Input');
        //                     $incidentType6Phase2Step1FormField1->setValue([]);
        //                     $incidentType6Phase2Step1FormField1->setOptions([]);
        //                     $incidentType6Phase2Step1FormField1->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase2Step1FormField2 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField2->setTitle('Option to begin corrective action');
        //                     $incidentType6Phase2Step1FormField2->setOrdering(2);
        //                     $incidentType6Phase2Step1FormField2->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField2->setFormType('Input');
        //                     $incidentType6Phase2Step1FormField2->setValue([]);
        //                     $incidentType6Phase2Step1FormField2->setOptions([]);
        //                     $incidentType6Phase2Step1FormField2->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase2Step1FormField3 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField3->setTitle('Date reviewed with employee');
        //                     $incidentType6Phase2Step1FormField3->setOrdering(3);
        //                     $incidentType6Phase2Step1FormField3->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField3->setFormType('Input');
        //                     $incidentType6Phase2Step1FormField3->setValue([]);
        //                     $incidentType6Phase2Step1FormField3->setOptions([]);
        //                     $incidentType6Phase2Step1FormField3->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase2Step1FormField4 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField4->setTitle('Incident Complete');
        //                     $incidentType6Phase2Step1FormField4->setOrdering(4);
        //                     $incidentType6Phase2Step1FormField4->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField4->setFormType('Input');
        //                     $incidentType6Phase2Step1FormField4->setValue([]);
        //                     $incidentType6Phase2Step1FormField4->setOptions([]);
        //                     $incidentType6Phase2Step1FormField4->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField4);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase2Step1FormField5 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField5->setTitle('Approved');
        //                     $incidentType6Phase2Step1FormField5->setOrdering(5);
        //                     $incidentType6Phase2Step1FormField5->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField5->setFormType('Radio');
        //                     $incidentType6Phase2Step1FormField5->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType6Phase2Step1FormField5->setOptions([]);
        //                     $incidentType6Phase2Step1FormField5->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField5->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField5);
        //                     $this->em->flush();
        //
        //                     $incidentType6Phase2Step1FormField6 = new IncidentFormFields();
        //                     $incidentType6Phase2Step1FormField6->setTitle('Reviewed By');
        //                     $incidentType6Phase2Step1FormField6->setOrdering(6);
        //                     $incidentType6Phase2Step1FormField6->setErrorMsg('');
        //                     $incidentType6Phase2Step1FormField6->setFormType('Input');
        //                     $incidentType6Phase2Step1FormField6->setValue([]);
        //                     $incidentType6Phase2Step1FormField6->setOptions([]);
        //                     $incidentType6Phase2Step1FormField6->setStep($incidentType6Phase2Step1);
        //                     $incidentType6Phase2Step1FormField6->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType6Phase2Step1FormField6);
        //                     $this->em->flush();
        //
        //
        // // inciden type 6 end
        //
        // // inciden type 7 start
        //         //Incident Type Add
        //         $incidentType7 = new IncidentType();
        //         $incidentType7->setName('Legal and Financial');
        //         $incidentType7->setCompany($company);
        //         $this->em->persist($incidentType7);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType7Phase1 = new IncidentPhase();
        //             $incidentType7Phase1->setTitle('Lead Driver, Dispatcher, Station Manager');
        //             $incidentType7Phase1->setOrdering(1);
        //             $incidentType7Phase1->setCompany($company);
        //             $incidentType7Phase1->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType7Phase1->setIncidentType($incidentType7);
        //             $incidentType7Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType7Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType7Phase1Step1 = new IncidentStep();
        //                 $incidentType7Phase1Step1->setPhase($incidentType7Phase1);
        //                 $incidentType7Phase1Step1->setTitle('');
        //                 $incidentType7Phase1Step1->setInstructions('');
        //                 $incidentType7Phase1Step1->setStepNumber(1);
        //                 $incidentType7Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType7Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType7Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField1->setTitle('Select date of delay');
        //                     $incidentType7Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType7Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField1->setFormType('date');
        //                     $incidentType7Phase1Step1FormField1->setValue([]);
        //                     $incidentType7Phase1Step1FormField1->setOptions([]);
        //                     $incidentType7Phase1Step1FormField1->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType7Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField2->setTitle('Enter your email address ');
        //                     $incidentType7Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType7Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField2->setFormType('Input');
        //                     $incidentType7Phase1Step1FormField2->setValue([]);
        //                     $incidentType7Phase1Step1FormField2->setOptions([]);
        //                     $incidentType7Phase1Step1FormField2->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField3 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField3->setTitle('Select delivery station code');
        //                     $incidentType7Phase1Step1FormField3->setOrdering(3);
        //                     $incidentType7Phase1Step1FormField3->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField3->setFormType('Select');
        //                     $incidentType7Phase1Step1FormField3->setValue(['DCH4' => 1, 'DML2' => 2]);
        //                     $incidentType7Phase1Step1FormField3->setOptions([]);
        //                     $incidentType7Phase1Step1FormField3->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField4 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField4->setTitle('Select Reason for Delay');
        //                     $incidentType7Phase1Step1FormField4->setOrdering(4);
        //                     $incidentType7Phase1Step1FormField4->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField4->setFormType('Select');
        //                     $incidentType7Phase1Step1FormField4->setValue(['Late Departure' => 1, 'On-Road Delay' => 2,'Amazon Authorized' => 3]);
        //                     $incidentType7Phase1Step1FormField4->setOptions([]);
        //                     $incidentType7Phase1Step1FormField4->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField4);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField5 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField5->setTitle('Enter the email address of the Amazon manager who authorized the delay');
        //                     $incidentType7Phase1Step1FormField5->setOrdering(5);
        //                     $incidentType7Phase1Step1FormField5->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField5->setFormType('Input');
        //                     $incidentType7Phase1Step1FormField5->setValue([]);
        //                     $incidentType7Phase1Step1FormField5->setOptions([]);
        //                     $incidentType7Phase1Step1FormField5->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField5->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField5);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField6 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField6->setTitle('Enter the total number of impacted routes');
        //                     $incidentType7Phase1Step1FormField6->setOrdering(6);
        //                     $incidentType7Phase1Step1FormField6->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField6->setFormType('Input');
        //                     $incidentType7Phase1Step1FormField6->setValue([]);
        //                     $incidentType7Phase1Step1FormField6->setOptions([]);
        //                     $incidentType7Phase1Step1FormField6->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField6->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField6);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField7 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField7->setTitle('Enter the impacted route codes');
        //                     $incidentType7Phase1Step1FormField7->setOrdering(7);
        //                     $incidentType7Phase1Step1FormField7->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField7->setFormType('Select');
        //                     $incidentType7Phase1Step1FormField7->setValue([]);
        //                     $incidentType7Phase1Step1FormField7->setOptions([]);
        //                     $incidentType7Phase1Step1FormField7->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField7->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField7);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase1Step1FormField8 = new IncidentFormFields();
        //                     $incidentType7Phase1Step1FormField8->setTitle('Enter additional information that is pertinent for the evaluation for the UPD request (open notes field up to 16000 characters)');
        //                     $incidentType7Phase1Step1FormField8->setOrdering(8);
        //                     $incidentType7Phase1Step1FormField8->setErrorMsg('');
        //                     $incidentType7Phase1Step1FormField8->setFormType('Input');
        //                     $incidentType7Phase1Step1FormField8->setValue([]);
        //                     $incidentType7Phase1Step1FormField8->setOptions([]);
        //                     $incidentType7Phase1Step1FormField8->setStep($incidentType7Phase1Step1);
        //                     $incidentType7Phase1Step1FormField8->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase1Step1FormField8);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType7Phase2 = new IncidentPhase();
        //             $incidentType7Phase2->setTitle('Station Manager, Director of Dispatch, Regional Manager');
        //             $incidentType7Phase2->setOrdering(2);
        //             $incidentType7Phase2->setCompany($company);
        //             $incidentType7Phase2->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType7Phase2->setIncidentType($incidentType7);
        //             $incidentType7Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType7Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType7Phase2Step1 = new IncidentStep();
        //                 $incidentType7Phase2Step1->setPhase($incidentType7Phase2);
        //                 $incidentType7Phase2Step1->setTitle('');
        //                 $incidentType7Phase2Step1->setInstructions('');
        //                 $incidentType7Phase2Step1->setStepNumber(1);
        //                 $incidentType7Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType7Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     // $incidentType7Phase2Step1FormField1 = new IncidentFormFields();
        //                     // $incidentType7Phase2Step1FormField1->setTitle('');
        //                     // $incidentType7Phase2Step1FormField1->setOrdering(1);
        //                     // $incidentType7Phase2Step1FormField1->setErrorMsg('');
        //                     // $incidentType7Phase2Step1FormField1->setFormType('date');
        //                     // $incidentType7Phase2Step1FormField1->setValue([]);
        //                     // $incidentType7Phase2Step1FormField1->setOptions([]);
        //                     // $incidentType7Phase2Step1FormField1->setStep($incidentType7Phase2Step1);
        //                     // $incidentType7Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     // $this->em->persist($incidentType7Phase2Step1FormField1);
        //                     // $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType7Phase2Step1FormField2 = new IncidentFormFields();
        //                     $incidentType7Phase2Step1FormField2->setTitle('status');
        //                     $incidentType7Phase2Step1FormField2->setOrdering(2);
        //                     $incidentType7Phase2Step1FormField2->setErrorMsg('');
        //                     $incidentType7Phase2Step1FormField2->setFormType('Radio');
        //                     $incidentType7Phase2Step1FormField2->setValue(['Pending AMZL Review' => 1]);
        //                     $incidentType7Phase2Step1FormField2->setOptions([]);
        //                     $incidentType7Phase2Step1FormField2->setStep($incidentType7Phase2Step1);
        //                     $incidentType7Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase2Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType7Phase2Step1FormField3 = new IncidentFormFields();
        //                     $incidentType7Phase2Step1FormField3->setTitle('Amazone rafrence number');
        //                     $incidentType7Phase2Step1FormField3->setOrdering(3);
        //                     $incidentType7Phase2Step1FormField3->setErrorMsg('');
        //                     $incidentType7Phase2Step1FormField3->setFormType('Input');
        //                     $incidentType7Phase2Step1FormField3->setValue([]);
        //                     $incidentType7Phase2Step1FormField3->setOptions([]);
        //                     $incidentType7Phase2Step1FormField3->setStep($incidentType7Phase2Step1);
        //                     $incidentType7Phase2Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType7Phase2Step1FormField3);
        //                     $this->em->flush();
        //
        //
        //         // Incident Phase Add
        //         $incidentType7Phase2 = new IncidentPhase();
        //         $incidentType7Phase2->setTitle('Station Manager, Director of Dispatch, Regional Manager');
        //         $incidentType7Phase2->setOrdering(3);
        //         $incidentType7Phase2->setCompany($company);
        //         $incidentType7Phase2->setRole($company->getCompanyRoles()[6]); //owner
        //         $incidentType7Phase2->setIncidentType($incidentType7);
        //         $incidentType7Phase2->setCreatedDate($currentDateTime);
        //         $this->em->persist($incidentType7Phase2);
        //         $this->em->flush();
        //
        //             //Incident Phase 1 Steps 1
        //             $incidentType7Phase2Step1 = new IncidentStep();
        //             $incidentType7Phase2Step1->setPhase($incidentType7Phase2);
        //             $incidentType7Phase2Step1->setTitle('');
        //             $incidentType7Phase2Step1->setInstructions('');
        //             $incidentType7Phase2Step1->setStepNumber(1);
        //             $incidentType7Phase2Step1->setOrdering(1);
        //             $this->em->persist($incidentType7Phase2Step1);
        //             $this->em->flush();
        //
        //                 //Incident Form Fields
        //                 $incidentType7Phase2Step1FormField1 = new IncidentFormFields();
        //                 $incidentType7Phase2Step1FormField1->setTitle('Update status to');
        //                 $incidentType7Phase2Step1FormField1->setOrdering(1);
        //                 $incidentType7Phase2Step1FormField1->setErrorMsg('');
        //                 $incidentType7Phase2Step1FormField1->setFormType('Select');
        //                 $incidentType7Phase2Step1FormField1->setValue(['Pending AMZL Review' => 1, 'Denied' => 2,'Fully Approved' => 3, 'Partially Approved' => 4]);
        //                 $incidentType7Phase2Step1FormField1->setOptions([]);
        //                 $incidentType7Phase2Step1FormField1->setStep($incidentType7Phase2Step1);
        //                 $incidentType7Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                 $this->em->persist($incidentType7Phase2Step1FormField1);
        //                 $this->em->flush();
        //
        //                 //Incident Form Fields
        //                 $incidentType7Phase2Step1FormField2 = new IncidentFormFields();
        //                 $incidentType7Phase2Step1FormField2->setTitle('Amazon Reviewer Comments');
        //                 $incidentType7Phase2Step1FormField2->setOrdering(2);
        //                 $incidentType7Phase2Step1FormField2->setErrorMsg('');
        //                 $incidentType7Phase2Step1FormField2->setFormType('Input');
        //                 $incidentType7Phase2Step1FormField2->setValue([]);
        //                 $incidentType7Phase2Step1FormField2->setOptions([]);
        //                 $incidentType7Phase2Step1FormField2->setStep($incidentType7Phase2Step1);
        //                 $incidentType7Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                 $this->em->persist($incidentType7Phase2Step1FormField2);
        //                 $this->em->flush();
        // // inciden type 7 end
        //
        // // inciden type 8 start
        //         //Incident Type Add
        //         $incidentType8 = new IncidentType();
        //         $incidentType8->setName('Finalization');
        //         $incidentType8->setCompany($company);
        //         $this->em->persist($incidentType8);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType8Phase1 = new IncidentPhase();
        //             $incidentType8Phase1->setTitle('Amazon or DSP?');
        //             $incidentType8Phase1->setOrdering(1);
        //             $incidentType8Phase1->setCompany($company);
        //             $incidentType8Phase1->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType8Phase1->setIncidentType($incidentType8);
        //             $incidentType8Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType8Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType8Phase1Step1 = new IncidentStep();
        //                 $incidentType8Phase1Step1->setPhase($incidentType8Phase1);
        //                 $incidentType8Phase1Step1->setTitle('');
        //                 $incidentType8Phase1Step1->setInstructions('');
        //                 $incidentType8Phase1Step1->setStepNumber(1);
        //                 $incidentType8Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType8Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType8Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType8Phase1Step1FormField1->setTitle('Amazon or DSP?');
        //                     $incidentType8Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType8Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType8Phase1Step1FormField1->setFormType('Select');
        //                     $incidentType8Phase1Step1FormField1->setValue(['Amazone' => 1, 'Dsp' => 2]);
        //                     $incidentType8Phase1Step1FormField1->setOptions([]);
        //                     $incidentType8Phase1Step1FormField1->setStep($incidentType8Phase1Step1);
        //                     $incidentType8Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType8Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        // // inciden type 8 end
        //
        // // inciden type 9 start
        //
        //         //Incident Type Add
        //         $incidentType9 = new IncidentType();
        //         $incidentType9->setName('Abondoned Route');
        //         $incidentType9->setCompany($company);
        //         $this->em->persist($incidentType9);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType9Phase1 = new IncidentPhase();
        //             $incidentType9Phase1->setTitle('');
        //             $incidentType9Phase1->setOrdering(1);
        //             $incidentType9Phase1->setCompany($company);
        //             $incidentType9Phase1->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType9Phase1->setIncidentType($incidentType9);
        //             $incidentType9Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType9Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType9Phase1Step1 = new IncidentStep();
        //                 $incidentType9Phase1Step1->setPhase($incidentType9Phase1);
        //                 $incidentType9Phase1Step1->setTitle('');
        //                 $incidentType9Phase1Step1->setInstructions('');
        //                 $incidentType9Phase1Step1->setStepNumber(1);
        //                 $incidentType9Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType9Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType9Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType9Phase1Step1FormField1->setTitle('Driver');
        //                     $incidentType9Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType9Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType9Phase1Step1FormField1->setFormType('Select');
        //                     $incidentType9Phase1Step1FormField1->setValue([]);
        //                     $incidentType9Phase1Step1FormField1->setOptions([]);
        //                     $incidentType9Phase1Step1FormField1->setStep($incidentType9Phase1Step1);
        //                     $incidentType9Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType9Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType9Phase1Step1FormField2->setTitle('Type Of Route');
        //                     $incidentType9Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType9Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType9Phase1Step1FormField2->setFormType('Select');
        //                     $incidentType9Phase1Step1FormField2->setValue(['Amazon' => 1, 'Dispatch' => 2,'Fellow Employee' => 3, 'Manager' => 4, 'Customer' => 5, 'Other' => 6]);
        //                     $incidentType9Phase1Step1FormField2->setOptions([]);
        //                     $incidentType9Phase1Step1FormField2->setStep($incidentType9Phase1Step1);
        //                     $incidentType9Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType9Phase1Step1FormField3 = new IncidentFormFields();
        //                     $incidentType9Phase1Step1FormField3->setTitle('Assigned To');
        //                     $incidentType9Phase1Step1FormField3->setOrdering(3);
        //                     $incidentType9Phase1Step1FormField3->setErrorMsg('');
        //                     $incidentType9Phase1Step1FormField3->setFormType('Input');
        //                     $incidentType9Phase1Step1FormField3->setValue([]);
        //                     $incidentType9Phase1Step1FormField3->setOptions([]);
        //                     $incidentType9Phase1Step1FormField3->setStep($incidentType9Phase1Step1);
        //                     $incidentType9Phase1Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase1Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType9Phase1Step1FormField4 = new IncidentFormFields();
        //                     $incidentType9Phase1Step1FormField4->setTitle('Reviewed By');
        //                     $incidentType9Phase1Step1FormField4->setOrdering(4);
        //                     $incidentType9Phase1Step1FormField4->setErrorMsg('');
        //                     $incidentType9Phase1Step1FormField4->setFormType('Input');
        //                     $incidentType9Phase1Step1FormField4->setValue([]);
        //                     $incidentType9Phase1Step1FormField4->setOptions([]);
        //                     $incidentType9Phase1Step1FormField4->setStep($incidentType9Phase1Step1);
        //                     $incidentType9Phase1Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase1Step1FormField4);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType9Phase2 = new IncidentPhase();
        //             $incidentType9Phase2->setTitle('');
        //             $incidentType9Phase2->setOrdering(2);
        //             $incidentType9Phase2->setCompany($company);
        //             $incidentType9Phase2->setRole($company->getCompanyRoles()[6]); //owner
        //             $incidentType9Phase2->setIncidentType($incidentType9);
        //             $incidentType9Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType9Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 2 Steps 1
        //                 $incidentType9Phase2Step1 = new IncidentStep();
        //                 $incidentType9Phase2Step1->setPhase($incidentType9Phase2);
        //                 $incidentType9Phase2Step1->setTitle('');
        //                 $incidentType9Phase2Step1->setInstructions('');
        //                 $incidentType9Phase2Step1->setStepNumber(1);
        //                 $incidentType9Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType9Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType9Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType9Phase2Step1FormField1->setTitle('Approved');
        //                     $incidentType9Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType9Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType9Phase2Step1FormField1->setFormType('Radio');
        //                     $incidentType9Phase2Step1FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType9Phase2Step1FormField1->setOptions([]);
        //                     $incidentType9Phase2Step1FormField1->setStep($incidentType9Phase2Step1);
        //                     $incidentType9Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType9Phase2Step1FormField2 = new IncidentFormFields();
        //                     $incidentType9Phase2Step1FormField2->setTitle('Reviewed By');
        //                     $incidentType9Phase2Step1FormField2->setOrdering(2);
        //                     $incidentType9Phase2Step1FormField2->setErrorMsg('');
        //                     $incidentType9Phase2Step1FormField2->setFormType('Input');
        //                     $incidentType9Phase2Step1FormField2->setValue([]);
        //                     $incidentType9Phase2Step1FormField2->setOptions([]);
        //                     $incidentType9Phase2Step1FormField2->setStep($incidentType9Phase2Step1);
        //                     $incidentType9Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType9Phase2Step1FormField2);
        //                     $this->em->flush();
        //
        // // inciden type 9 end
        //
        // // inciden type 10 start
        //
        //         //Incident Type Add
        //         $incidentType10 = new IncidentType();
        //         $incidentType10->setName('Theft');
        //         $incidentType10->setCompany($company);
        //         $this->em->persist($incidentType10);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType10Phase1 = new IncidentPhase();
        //             $incidentType10Phase1->setTitle('');
        //             $incidentType10Phase1->setOrdering(1);
        //             $incidentType10Phase1->setCompany($company);
        //             $incidentType10Phase1->setRole($company->getCompanyRoles()[2]); //manager
        //             $incidentType10Phase1->setIncidentType($incidentType10);
        //             $incidentType10Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType10Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType10Phase1Step1 = new IncidentStep();
        //                 $incidentType10Phase1Step1->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step1->setTitle('Enter type of property stolen');
        //                 $incidentType10Phase1Step1->setInstructions('');
        //                 $incidentType10Phase1Step1->setStepNumber(1);
        //                 $incidentType10Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType10Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step1FormField1->setTitle('Driver');
        //                     $incidentType10Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step1FormField1->setFormType('Select');
        //                     $incidentType10Phase1Step1FormField1->setValue(['Amazon Property' => 1, 'DSP Property' => 2, 'Emlpoyee Property' => 3, 'Customer Property' => 4]);
        //                     $incidentType10Phase1Step1FormField1->setOptions([]);
        //                     $incidentType10Phase1Step1FormField1->setStep($incidentType10Phase1Step1);
        //                     $incidentType10Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        // //  second step is pending
        //
        //                 //Incident Phase 1 Steps 3
        //                 $incidentType10Phase1Step3 = new IncidentStep();
        //                 $incidentType10Phase1Step3->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step3->setTitle('Enter responsible party');
        //                 $incidentType10Phase1Step3->setInstructions('');
        //                 $incidentType10Phase1Step3->setStepNumber(3);
        //                 $incidentType10Phase1Step3->setOrdering(3);
        //                 $this->em->persist($incidentType10Phase1Step3);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step3FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step3FormField1->setTitle('Party');
        //                     $incidentType10Phase1Step3FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step3FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step3FormField1->setFormType('Select');
        //                     $incidentType10Phase1Step3FormField1->setValue(['Unknown' => 1, 'DSP Employee' => 2, 'Amazon Employee' => 3, 'Customer' => 4, 'Third Party' => 5]);
        //                     $incidentType10Phase1Step3FormField1->setOptions([]);
        //                     $incidentType10Phase1Step3FormField1->setStep($incidentType10Phase1Step3);
        //                     $incidentType10Phase1Step3FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step3FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step3FormField2 = new IncidentFormFields();
        //                     $incidentType10Phase1Step3FormField2->setTitle('name');
        //                     $incidentType10Phase1Step3FormField2->setOrdering(2);
        //                     $incidentType10Phase1Step3FormField2->setErrorMsg('');
        //                     $incidentType10Phase1Step3FormField2->setFormType('Input');
        //                     $incidentType10Phase1Step3FormField2->setValue([]);
        //                     $incidentType10Phase1Step3FormField2->setOptions([]);
        //                     $incidentType10Phase1Step3FormField2->setStep($incidentType10Phase1Step3);
        //                     $incidentType10Phase1Step3FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step3FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step3FormField3 = new IncidentFormFields();
        //                     $incidentType10Phase1Step3FormField3->setTitle('Contact information');
        //                     $incidentType10Phase1Step3FormField3->setOrdering(3);
        //                     $incidentType10Phase1Step3FormField3->setErrorMsg('');
        //                     $incidentType10Phase1Step3FormField3->setFormType('Input');
        //                     $incidentType10Phase1Step3FormField3->setValue([]);
        //                     $incidentType10Phase1Step3FormField3->setOptions([]);
        //                     $incidentType10Phase1Step3FormField3->setStep($incidentType10Phase1Step3);
        //                     $incidentType10Phase1Step3FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step3FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step3FormField4 = new IncidentFormFields();
        //                     $incidentType10Phase1Step3FormField4->setTitle('Open notes');
        //                     $incidentType10Phase1Step3FormField4->setOrdering(4);
        //                     $incidentType10Phase1Step3FormField4->setErrorMsg('');
        //                     $incidentType10Phase1Step3FormField4->setFormType('Input');
        //                     $incidentType10Phase1Step3FormField4->setValue([]);
        //                     $incidentType10Phase1Step3FormField4->setOptions([]);
        //                     $incidentType10Phase1Step3FormField4->setStep($incidentType10Phase1Step3);
        //                     $incidentType10Phase1Step3FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step3FormField4);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 4
        //                 $incidentType10Phase1Step4 = new IncidentStep();
        //                 $incidentType10Phase1Step4->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step4->setTitle('Describe the incident (notes field)');
        //                 $incidentType10Phase1Step4->setInstructions('');
        //                 $incidentType10Phase1Step4->setStepNumber(4);
        //                 $incidentType10Phase1Step4->setOrdering(4);
        //                 $this->em->persist($incidentType10Phase1Step4);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step4FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step4FormField1->setTitle('Option to upload files/evidence/documentation.');
        //                     $incidentType10Phase1Step4FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step4FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step4FormField1->setFormType('file');
        //                     $incidentType10Phase1Step4FormField1->setValue([]);
        //                     $incidentType10Phase1Step4FormField1->setOptions([]);
        //                     $incidentType10Phase1Step4FormField1->setStep($incidentType10Phase1Step4);
        //                     $incidentType10Phase1Step4FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step4FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 5
        //                 $incidentType10Phase1Step5 = new IncidentStep();
        //                 $incidentType10Phase1Step5->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step5->setTitle('Reported to Amazon?');
        //                 $incidentType10Phase1Step5->setInstructions('');
        //                 $incidentType10Phase1Step5->setStepNumber(5);
        //                 $incidentType10Phase1Step5->setOrdering(5);
        //                 $this->em->persist($incidentType10Phase1Step5);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step5FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step5FormField1->setTitle('');
        //                     $incidentType10Phase1Step5FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step5FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step5FormField1->setFormType('Radio');
        //                     $incidentType10Phase1Step5FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType10Phase1Step5FormField1->setOptions([]);
        //                     $incidentType10Phase1Step5FormField1->setStep($incidentType10Phase1Step5);
        //                     $incidentType10Phase1Step5FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step5FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step5FormField2 = new IncidentFormFields();
        //                     $incidentType10Phase1Step5FormField2->setTitle('Name');
        //                     $incidentType10Phase1Step5FormField2->setOrdering(2);
        //                     $incidentType10Phase1Step5FormField2->setErrorMsg('');
        //                     $incidentType10Phase1Step5FormField2->setFormType('Input');
        //                     $incidentType10Phase1Step5FormField2->setValue([]);
        //                     $incidentType10Phase1Step5FormField2->setOptions([]);
        //                     $incidentType10Phase1Step5FormField2->setStep($incidentType10Phase1Step5);
        //                     $incidentType10Phase1Step5FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step5FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step5FormField2 = new IncidentFormFields();
        //                     $incidentType10Phase1Step5FormField2->setTitle('email of Amazon contact');
        //                     $incidentType10Phase1Step5FormField2->setOrdering(3);
        //                     $incidentType10Phase1Step5FormField2->setErrorMsg('');
        //                     $incidentType10Phase1Step5FormField2->setFormType('Input');
        //                     $incidentType10Phase1Step5FormField2->setValue([]);
        //                     $incidentType10Phase1Step5FormField2->setOptions([]);
        //                     $incidentType10Phase1Step5FormField2->setStep($incidentType10Phase1Step5);
        //                     $incidentType10Phase1Step5FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step5FormField2);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 6
        //                 $incidentType10Phase1Step6 = new IncidentStep();
        //                 $incidentType10Phase1Step6->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step6->setTitle('Reported to police?');
        //                 $incidentType10Phase1Step6->setInstructions('');
        //                 $incidentType10Phase1Step6->setStepNumber(6);
        //                 $incidentType10Phase1Step6->setOrdering(6);
        //                 $this->em->persist($incidentType10Phase1Step6);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step6FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step6FormField1->setTitle('');
        //                     $incidentType10Phase1Step6FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step6FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step6FormField1->setFormType('Radio');
        //                     $incidentType10Phase1Step6FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType10Phase1Step6FormField1->setOptions([]);
        //                     $incidentType10Phase1Step6FormField1->setStep($incidentType10Phase1Step6);
        //                     $incidentType10Phase1Step6FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step6FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step6FormField2 = new IncidentFormFields();
        //                     $incidentType10Phase1Step6FormField2->setTitle('Report number');
        //                     $incidentType10Phase1Step6FormField2->setOrdering(2);
        //                     $incidentType10Phase1Step6FormField2->setErrorMsg('');
        //                     $incidentType10Phase1Step6FormField2->setFormType('Input');
        //                     $incidentType10Phase1Step6FormField2->setValue([]);
        //                     $incidentType10Phase1Step6FormField2->setOptions([]);
        //                     $incidentType10Phase1Step6FormField2->setStep($incidentType10Phase1Step6);
        //                     $incidentType10Phase1Step6FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step6FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step6FormField3 = new IncidentFormFields();
        //                     $incidentType10Phase1Step6FormField3->setTitle('Officer name');
        //                     $incidentType10Phase1Step6FormField3->setOrdering(3);
        //                     $incidentType10Phase1Step6FormField3->setErrorMsg('');
        //                     $incidentType10Phase1Step6FormField3->setFormType('Input');
        //                     $incidentType10Phase1Step6FormField3->setValue([]);
        //                     $incidentType10Phase1Step6FormField3->setOptions([]);
        //                     $incidentType10Phase1Step6FormField3->setStep($incidentType10Phase1Step6);
        //                     $incidentType10Phase1Step6FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step6FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType10Phase1Step6FormField4 = new IncidentFormFields();
        //                     $incidentType10Phase1Step6FormField4->setTitle('Officer contact info');
        //                     $incidentType10Phase1Step6FormField4->setOrdering(4);
        //                     $incidentType10Phase1Step6FormField4->setErrorMsg('');
        //                     $incidentType10Phase1Step6FormField4->setFormType('Input');
        //                     $incidentType10Phase1Step6FormField4->setValue([]);
        //                     $incidentType10Phase1Step6FormField4->setOptions([]);
        //                     $incidentType10Phase1Step6FormField4->setStep($incidentType10Phase1Step6);
        //                     $incidentType10Phase1Step6FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step6FormField4);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 7
        //                 $incidentType10Phase1Step7 = new IncidentStep();
        //                 $incidentType10Phase1Step7->setPhase($incidentType10Phase1);
        //                 $incidentType10Phase1Step7->setTitle('Property recovered?');
        //                 $incidentType10Phase1Step7->setInstructions('');
        //                 $incidentType10Phase1Step7->setStepNumber(7);
        //                 $incidentType10Phase1Step7->setOrdering(7);
        //                 $this->em->persist($incidentType10Phase1Step7);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase1Step7FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase1Step7FormField1->setTitle('');
        //                     $incidentType10Phase1Step7FormField1->setOrdering(1);
        //                     $incidentType10Phase1Step7FormField1->setErrorMsg('');
        //                     $incidentType10Phase1Step7FormField1->setFormType('Radio');
        //                     $incidentType10Phase1Step7FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType10Phase1Step7FormField1->setOptions([]);
        //                     $incidentType10Phase1Step7FormField1->setStep($incidentType10Phase1Step7);
        //                     $incidentType10Phase1Step7FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase1Step7FormField1);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType10Phase2 = new IncidentPhase();
        //             $incidentType10Phase2->setTitle('');
        //             $incidentType10Phase2->setOrdering(2);
        //             $incidentType10Phase2->setCompany($company);
        //             $incidentType10Phase2->setRole($company->getCompanyRoles()[2]); //manager
        //             $incidentType10Phase2->setIncidentType($incidentType10);
        //             $incidentType10Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType10Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType10Phase2Step1 = new IncidentStep();
        //                 $incidentType10Phase2Step1->setPhase($incidentType10Phase2);
        //                 $incidentType10Phase2Step1->setTitle('');
        //                 $incidentType10Phase2Step1->setInstructions('');
        //                 $incidentType10Phase2Step1->setStepNumber(1);
        //                 $incidentType10Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType10Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType10Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType10Phase2Step1FormField1->setTitle('Date Review');
        //                     $incidentType10Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType10Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType10Phase2Step1FormField1->setFormType('date');
        //                     $incidentType10Phase2Step1FormField1->setValue([]);
        //                     $incidentType10Phase2Step1FormField1->setOptions([]);
        //                     $incidentType10Phase2Step1FormField1->setStep($incidentType10Phase2Step1);
        //                     $incidentType10Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType10Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        // // inciden type 10 end
        //
        // // inciden type 11 end
        //
        //         //Incident Type Add
        //         $incidentType11 = new IncidentType();
        //         $incidentType11->setName('Customer Complaint');
        //         $incidentType11->setCompany($company);
        //         $this->em->persist($incidentType11);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType11Phase1 = new IncidentPhase();
        //             $incidentType11Phase1->setTitle('Enter complaint details');
        //             $incidentType11Phase1->setOrdering(1);
        //             $incidentType11Phase1->setCompany($company);
        //             $incidentType11Phase1->setRole($company->getCompanyRoles()[4]); //owner
        //             $incidentType11Phase1->setIncidentType($incidentType11);
        //             $incidentType11Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType11Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType11Phase1Step1 = new IncidentStep();
        //                 $incidentType11Phase1Step1->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step1->setTitle('Who is the source of the complaint?');
        //                 $incidentType11Phase1Step1->setInstructions('');
        //                 $incidentType11Phase1Step1->setStepNumber(1);
        //                 $incidentType11Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType11Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step1FormField1->setTitle('Customer name');
        //                     $incidentType11Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step1FormField1->setFormType('Input');
        //                     $incidentType11Phase1Step1FormField1->setValue([]);
        //                     $incidentType11Phase1Step1FormField1->setOptions([]);
        //                     $incidentType11Phase1Step1FormField1->setStep($incidentType11Phase1Step1);
        //                     $incidentType11Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType11Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType11Phase1Step1FormField2->setTitle('Customer email');
        //                     $incidentType11Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType11Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType11Phase1Step1FormField2->setFormType('Input');
        //                     $incidentType11Phase1Step1FormField2->setValue([]);
        //                     $incidentType11Phase1Step1FormField2->setOptions([]);
        //                     $incidentType11Phase1Step1FormField2->setStep($incidentType11Phase1Step1);
        //                     $incidentType11Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType11Phase1Step1FormField3 = new IncidentFormFields();
        //                     $incidentType11Phase1Step1FormField3->setTitle('Customer phone number');
        //                     $incidentType11Phase1Step1FormField3->setOrdering(3);
        //                     $incidentType11Phase1Step1FormField3->setErrorMsg('');
        //                     $incidentType11Phase1Step1FormField3->setFormType('Input');
        //                     $incidentType11Phase1Step1FormField3->setValue([]);
        //                     $incidentType11Phase1Step1FormField3->setOptions([]);
        //                     $incidentType11Phase1Step1FormField3->setStep($incidentType11Phase1Step1);
        //                     $incidentType11Phase1Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step1FormField3);
        //                     $this->em->flush();
        //
        //
        //                 //Incident Phase 1 Steps 2
        //                 $incidentType11Phase1Step2 = new IncidentStep();
        //                 $incidentType11Phase1Step2->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step2->setTitle('Type of complaint');
        //                 $incidentType11Phase1Step2->setInstructions('');
        //                 $incidentType11Phase1Step2->setStepNumber(2);
        //                 $incidentType11Phase1Step2->setOrdering(2);
        //                 $this->em->persist($incidentType11Phase1Step2);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step2FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step2FormField1->setTitle('');
        //                     $incidentType11Phase1Step2FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step2FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step2FormField1->setFormType('Select');
        //                     $incidentType11Phase1Step2FormField1->setValue(['Verbal'=>1,'Email'=>2,'Phone Call'=>3,'Facebook'=>4,'Website'=>5]);
        //                     $incidentType11Phase1Step2FormField1->setOptions([]);
        //                     $incidentType11Phase1Step2FormField1->setStep($incidentType11Phase1Step2);
        //                     $incidentType11Phase1Step2FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step2FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 3
        //                 $incidentType11Phase1Step3 = new IncidentStep();
        //                 $incidentType11Phase1Step3->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step3->setTitle('Complaint details');
        //                 $incidentType11Phase1Step3->setInstructions('');
        //                 $incidentType11Phase1Step3->setStepNumber(3);
        //                 $incidentType11Phase1Step3->setOrdering(3);
        //                 $this->em->persist($incidentType11Phase1Step3);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step3FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step3FormField1->setTitle('Open notes');
        //                     $incidentType11Phase1Step3FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step3FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step3FormField1->setFormType('Input');
        //                     $incidentType11Phase1Step3FormField1->setValue([]);
        //                     $incidentType11Phase1Step3FormField1->setOptions([]);
        //                     $incidentType11Phase1Step3FormField1->setStep($incidentType11Phase1Step3);
        //                     $incidentType11Phase1Step3FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step3FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType11Phase1Step3FormField2 = new IncidentFormFields();
        //                     $incidentType11Phase1Step3FormField2->setTitle('Option to upload files: documentation/photos/videos/audio clip');
        //                     $incidentType11Phase1Step3FormField2->setOrdering(1);
        //                     $incidentType11Phase1Step3FormField2->setErrorMsg('');
        //                     $incidentType11Phase1Step3FormField2->setFormType('file');
        //                     $incidentType11Phase1Step3FormField2->setValue([]);
        //                     $incidentType11Phase1Step3FormField2->setOptions([]);
        //                     $incidentType11Phase1Step3FormField2->setStep($incidentType11Phase1Step3);
        //                     $incidentType11Phase1Step3FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step3FormField2);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 4
        //                 $incidentType11Phase1Step4 = new IncidentStep();
        //                 $incidentType11Phase1Step4->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step4->setTitle('Complaint details');
        //                 $incidentType11Phase1Step4->setInstructions('');
        //                 $incidentType11Phase1Step4->setStepNumber(3);
        //                 $incidentType11Phase1Step4->setOrdering(3);
        //                 $this->em->persist($incidentType11Phase1Step4);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step4FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step4FormField1->setTitle('');
        //                     $incidentType11Phase1Step4FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step4FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step4FormField1->setFormType('Select');
        //                     $incidentType11Phase1Step4FormField1->setValue(['Verbal'=>1,'Email'=>2,'Phone Call'=>3,'Facebook'=>4,'Website'=>5]);
        //                     $incidentType11Phase1Step4FormField1->setOptions([]);
        //                     $incidentType11Phase1Step4FormField1->setStep($incidentType11Phase1Step4);
        //                     $incidentType11Phase1Step4FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step4FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 4
        //                 $incidentType11Phase1Step4 = new IncidentStep();
        //                 $incidentType11Phase1Step4->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step4->setTitle('Driver involved?');
        //                 $incidentType11Phase1Step4->setInstructions('');
        //                 $incidentType11Phase1Step4->setStepNumber(4);
        //                 $incidentType11Phase1Step4->setOrdering(4);
        //                 $this->em->persist($incidentType11Phase1Step4);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step4FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step4FormField1->setTitle('Enter name');
        //                     $incidentType11Phase1Step4FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step4FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step4FormField1->setFormType('Input');
        //                     $incidentType11Phase1Step4FormField1->setValue([]);
        //                     $incidentType11Phase1Step4FormField1->setOptions([]);
        //                     $incidentType11Phase1Step4FormField1->setStep($incidentType11Phase1Step4);
        //                     $incidentType11Phase1Step4FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step4FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 5
        //                 $incidentType11Phase1Step5 = new IncidentStep();
        //                 $incidentType11Phase1Step5->setPhase($incidentType11Phase1);
        //                 $incidentType11Phase1Step5->setTitle('Route involved');
        //                 $incidentType11Phase1Step5->setInstructions('');
        //                 $incidentType11Phase1Step5->setStepNumber(4);
        //                 $incidentType11Phase1Step5->setOrdering(4);
        //                 $this->em->persist($incidentType11Phase1Step5);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase1Step5FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase1Step5FormField1->setTitle('Route code');
        //                     $incidentType11Phase1Step5FormField1->setOrdering(1);
        //                     $incidentType11Phase1Step5FormField1->setErrorMsg('');
        //                     $incidentType11Phase1Step5FormField1->setFormType('Input');
        //                     $incidentType11Phase1Step5FormField1->setValue([]);
        //                     $incidentType11Phase1Step5FormField1->setOptions([]);
        //                     $incidentType11Phase1Step5FormField1->setStep($incidentType11Phase1Step5);
        //                     $incidentType11Phase1Step5FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase1Step5FormField1);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType11Phase2 = new IncidentPhase();
        //             $incidentType11Phase2->setTitle('Manager review');
        //             $incidentType11Phase2->setOrdering(2);
        //             $incidentType11Phase2->setCompany($company);
        //             $incidentType11Phase2->setRole($company->getCompanyRoles()[2]); //manager
        //             $incidentType11Phase2->setIncidentType($incidentType11);
        //             $incidentType11Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType11Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType11Phase2Step1 = new IncidentStep();
        //                 $incidentType11Phase2Step1->setPhase($incidentType11Phase2);
        //                 $incidentType11Phase2Step1->setTitle('Confirm that complaint has been reviewed');
        //                 $incidentType11Phase2Step1->setInstructions('');
        //                 $incidentType11Phase2Step1->setStepNumber(1);
        //                 $incidentType11Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType11Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase2Step1FormField1->setTitle('optional notes');
        //                     $incidentType11Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType11Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType11Phase2Step1FormField1->setFormType('Input');
        //                     $incidentType11Phase2Step1FormField1->setValue([]);
        //                     $incidentType11Phase2Step1FormField1->setOptions([]);
        //                     $incidentType11Phase2Step1FormField1->setStep($incidentType11Phase2Step1);
        //                     $incidentType11Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 2
        //                 $incidentType11Phase2Step2 = new IncidentStep();
        //                 $incidentType11Phase2Step2->setPhase($incidentType11Phase2);
        //                 $incidentType11Phase2Step2->setTitle('Follow up with complaining party?');
        //                 $incidentType11Phase2Step2->setInstructions('');
        //                 $incidentType11Phase2Step2->setStepNumber(2);
        //                 $incidentType11Phase2Step2->setOrdering(2);
        //                 $this->em->persist($incidentType11Phase2Step2);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType11Phase2Step2FormField1 = new IncidentFormFields();
        //                     $incidentType11Phase2Step2FormField1->setTitle('');
        //                     $incidentType11Phase2Step2FormField1->setOrdering(1);
        //                     $incidentType11Phase2Step2FormField1->setErrorMsg('');
        //                     $incidentType11Phase2Step2FormField1->setFormType('Radio');
        //                     $incidentType11Phase2Step2FormField1->setValue([]);
        //                     $incidentType11Phase2Step2FormField1->setOptions(['yes' => 1, 'no' => 2]);
        //                     $incidentType11Phase2Step2FormField1->setStep($incidentType11Phase2Step2);
        //                     $incidentType11Phase2Step2FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType11Phase2Step2FormField1);
        //                     $this->em->flush();
        //
        // // inciden type 11 end
        //
        // // inciden type 12 end
        //
        //         //Incident Type Add
        //         $incidentType12 = new IncidentType();
        //         $incidentType12->setName('Incomplete Route');
        //         $incidentType12->setCompany($company);
        //         $this->em->persist($incidentType12);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType12Phase1 = new IncidentPhase();
        //             $incidentType12Phase1->setTitle('Information');
        //             $incidentType12Phase1->setOrdering(1);
        //             $incidentType12Phase1->setCompany($company);
        //             $incidentType12Phase1->setRole($company->getCompanyRoles()[4]); //owner
        //             $incidentType12Phase1->setIncidentType($incidentType12);
        //             $incidentType12Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType12Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType12Phase1Step1 = new IncidentStep();
        //                 $incidentType12Phase1Step1->setPhase($incidentType12Phase1);
        //                 $incidentType12Phase1Step1->setTitle('');
        //                 $incidentType12Phase1Step1->setInstructions('');
        //                 $incidentType12Phase1Step1->setStepNumber(1);
        //                 $incidentType12Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType12Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType12Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField1->setTitle('Select Day of incomplete route');
        //                     $incidentType12Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType12Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField1->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField1->setValue([]);
        //                     $incidentType12Phase1Step1FormField1->setOptions([]);
        //                     $incidentType12Phase1Step1FormField1->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField2->setTitle('multi-select route code or codes from that day');
        //                     $incidentType12Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType12Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField2->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField2->setValue([]);
        //                     $incidentType12Phase1Step1FormField2->setOptions([]);
        //                     $incidentType12Phase1Step1FormField2->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField3 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField3->setTitle('Log driver name(s)');
        //                     $incidentType12Phase1Step1FormField3->setOrdering(3);
        //                     $incidentType12Phase1Step1FormField3->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField3->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField3->setValue([]);
        //                     $incidentType12Phase1Step1FormField3->setOptions([]);
        //                     $incidentType12Phase1Step1FormField3->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField4 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField4->setTitle('Reason');
        //                     $incidentType12Phase1Step1FormField4->setOrdering(4);
        //                     $incidentType12Phase1Step1FormField4->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField4->setFormType('Select');
        //                     $incidentType12Phase1Step1FormField4->setValue(['Out of Delivery Time'=>1,'Injury'=>2,'Accident'=>3,'Van issue'=>4,'Inclement weather'=>5]);
        //                     $incidentType12Phase1Step1FormField4->setOptions([]);
        //                     $incidentType12Phase1Step1FormField4->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField4);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField5 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField5->setTitle('How many packages are returning?');
        //                     $incidentType12Phase1Step1FormField5->setOrdering(5);
        //                     $incidentType12Phase1Step1FormField5->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField5->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField5->setValue([]);
        //                     $incidentType12Phase1Step1FormField5->setOptions([]);
        //                     $incidentType12Phase1Step1FormField5->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField5->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField5);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField6 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField6->setTitle('How many packages were attempted/delivered?');
        //                     $incidentType12Phase1Step1FormField6->setOrdering(6);
        //                     $incidentType12Phase1Step1FormField6->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField6->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField6->setValue([]);
        //                     $incidentType12Phase1Step1FormField6->setOptions([]);
        //                     $incidentType12Phase1Step1FormField6->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField6->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField6);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase1Step1FormField7 = new IncidentFormFields();
        //                     $incidentType12Phase1Step1FormField7->setTitle('Optional: upload files/documentation');
        //                     $incidentType12Phase1Step1FormField7->setOrdering(7);
        //                     $incidentType12Phase1Step1FormField7->setErrorMsg('');
        //                     $incidentType12Phase1Step1FormField7->setFormType('Input');
        //                     $incidentType12Phase1Step1FormField7->setValue([]);
        //                     $incidentType12Phase1Step1FormField7->setOptions([]);
        //                     $incidentType12Phase1Step1FormField7->setStep($incidentType12Phase1Step1);
        //                     $incidentType12Phase1Step1FormField7->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase1Step1FormField7);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType12Phase2 = new IncidentPhase();
        //             $incidentType12Phase2->setTitle('Manager Review');
        //             $incidentType12Phase2->setOrdering(2);
        //             $incidentType12Phase2->setCompany($company);
        //             $incidentType12Phase2->setRole($company->getCompanyRoles()[2]); //manager
        //             $incidentType12Phase2->setIncidentType($incidentType12);
        //             $incidentType12Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType12Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType12Phase2Step1 = new IncidentStep();
        //                 $incidentType12Phase2Step1->setPhase($incidentType12Phase2);
        //                 $incidentType12Phase2Step1->setTitle('');
        //                 $incidentType12Phase2Step1->setInstructions('');
        //                 $incidentType12Phase2Step1->setStepNumber(1);
        //                 $incidentType12Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType12Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType12Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType12Phase2Step1FormField1->setTitle('Excused/Unexcused');
        //                     $incidentType12Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType12Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType12Phase2Step1FormField1->setFormType('Input');
        //                     $incidentType12Phase2Step1FormField1->setValue([]);
        //                     $incidentType12Phase2Step1FormField1->setOptions([]);
        //                     $incidentType12Phase2Step1FormField1->setStep($incidentType12Phase2Step1);
        //                     $incidentType12Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase2Step1FormField2 = new IncidentFormFields();
        //                     $incidentType12Phase2Step1FormField2->setTitle('Option to start corrective coaching action');
        //                     $incidentType12Phase2Step1FormField2->setOrdering(2);
        //                     $incidentType12Phase2Step1FormField2->setErrorMsg('');
        //                     $incidentType12Phase2Step1FormField2->setFormType('Input');
        //                     $incidentType12Phase2Step1FormField2->setValue([]);
        //                     $incidentType12Phase2Step1FormField2->setOptions([]);
        //                     $incidentType12Phase2Step1FormField2->setStep($incidentType12Phase2Step1);
        //                     $incidentType12Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase2Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType12Phase2Step1FormField3 = new IncidentFormFields();
        //                     $incidentType12Phase2Step1FormField3->setTitle('complete');
        //                     $incidentType12Phase2Step1FormField3->setOrdering(3);
        //                     $incidentType12Phase2Step1FormField3->setErrorMsg('');
        //                     $incidentType12Phase2Step1FormField3->setFormType('Input');
        //                     $incidentType12Phase2Step1FormField3->setValue([]);
        //                     $incidentType12Phase2Step1FormField3->setOptions([]);
        //                     $incidentType12Phase2Step1FormField3->setStep($incidentType12Phase2Step1);
        //                     $incidentType12Phase2Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType12Phase2Step1FormField3);
        //                     $this->em->flush();
        //
        //
        //         //Incident Type Add
        //         $incidentType13 = new IncidentType();
        //         $incidentType13->setName('Device Damage');
        //         $incidentType13->setCompany($company);
        //         $this->em->persist($incidentType13);
        //         $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType13Phase1 = new IncidentPhase();
        //             $incidentType13Phase1->setTitle('Report device damage/malfunction');
        //             $incidentType13Phase1->setOrdering(1);
        //             $incidentType13Phase1->setCompany($company);
        //             $incidentType13Phase1->setRole($company->getCompanyRoles()[4]); //owner
        //             $incidentType13Phase1->setIncidentType($incidentType13);
        //             $incidentType13Phase1->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType13Phase1);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType13Phase1Step1 = new IncidentStep();
        //                 $incidentType13Phase1Step1->setPhase($incidentType13Phase1);
        //                 $incidentType13Phase1Step1->setTitle('');
        //                 $incidentType13Phase1Step1->setInstructions('');
        //                 $incidentType13Phase1Step1->setStepNumber(1);
        //                 $incidentType13Phase1Step1->setOrdering(1);
        //                 $this->em->persist($incidentType13Phase1Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType13Phase1Step1FormField1 = new IncidentFormFields();
        //                     $incidentType13Phase1Step1FormField1->setTitle('Device ID');
        //                     $incidentType13Phase1Step1FormField1->setOrdering(1);
        //                     $incidentType13Phase1Step1FormField1->setErrorMsg('');
        //                     $incidentType13Phase1Step1FormField1->setFormType('Input');
        //                     $incidentType13Phase1Step1FormField1->setValue([]);
        //                     $incidentType13Phase1Step1FormField1->setOptions([]);
        //                     $incidentType13Phase1Step1FormField1->setStep($incidentType13Phase1Step1);
        //                     $incidentType13Phase1Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase1Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType13Phase1Step1FormField2->setTitle('Device model');
        //                     $incidentType13Phase1Step1FormField2->setOrdering(2);
        //                     $incidentType13Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType13Phase1Step1FormField2->setFormType('Input');
        //                     $incidentType13Phase1Step1FormField2->setValue([]);
        //                     $incidentType13Phase1Step1FormField2->setOptions([]);
        //                     $incidentType13Phase1Step1FormField2->setStep($incidentType13Phase1Step1);
        //                     $incidentType13Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase1Step1FormField2 = new IncidentFormFields();
        //                     $incidentType13Phase1Step1FormField2->setTitle('Lost, damaged, or stolen?');
        //                     $incidentType13Phase1Step1FormField2->setOrdering(3);
        //                     $incidentType13Phase1Step1FormField2->setErrorMsg('');
        //                     $incidentType13Phase1Step1FormField2->setFormType('Select');
        //                     $incidentType13Phase1Step1FormField2->setValue(['Lost' => 1, 'damaged' => 2, 'stolen' => 3]);
        //                     $incidentType13Phase1Step1FormField2->setOptions([]);
        //                     $incidentType13Phase1Step1FormField2->setStep($incidentType13Phase1Step1);
        //                     $incidentType13Phase1Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase1Step1FormField2);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType13Phase2 = new IncidentPhase();
        //             $incidentType13Phase2->setTitle('Inventory Manager');
        //             $incidentType13Phase2->setOrdering(3);
        //             $incidentType13Phase2->setCompany($company);
        //             $incidentType13Phase2->setRole($company->getCompanyRoles()[4]); //owner
        //             $incidentType13Phase2->setIncidentType($incidentType13);
        //             $incidentType13Phase2->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType13Phase2);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType13Phase2Step1 = new IncidentStep();
        //                 $incidentType13Phase2Step1->setPhase($incidentType13Phase2);
        //                 $incidentType13Phase2Step1->setTitle('');
        //                 $incidentType13Phase2Step1->setInstructions('');
        //                 $incidentType13Phase2Step1->setStepNumber(1);
        //                 $incidentType13Phase2Step1->setOrdering(1);
        //                 $this->em->persist($incidentType13Phase2Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType13Phase2Step1FormField1 = new IncidentFormFields();
        //                     $incidentType13Phase2Step1FormField1->setTitle('Is this a warranty repair?');
        //                     $incidentType13Phase2Step1FormField1->setOrdering(1);
        //                     $incidentType13Phase2Step1FormField1->setErrorMsg('');
        //                     $incidentType13Phase2Step1FormField1->setFormType('Input');
        //                     $incidentType13Phase2Step1FormField1->setValue([]);
        //                     $incidentType13Phase2Step1FormField1->setOptions([]);
        //                     $incidentType13Phase2Step1FormField1->setStep($incidentType13Phase2Step1);
        //                     $incidentType13Phase2Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase2Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase2Step1FormField2 = new IncidentFormFields();
        //                     $incidentType13Phase2Step1FormField2->setTitle('File claim for replacement device');
        //                     $incidentType13Phase2Step1FormField2->setOrdering(2);
        //                     $incidentType13Phase2Step1FormField2->setErrorMsg('');
        //                     $incidentType13Phase2Step1FormField2->setFormType('date');
        //                     $incidentType13Phase2Step1FormField2->setValue([]);
        //                     $incidentType13Phase2Step1FormField2->setOptions([]);
        //                     $incidentType13Phase2Step1FormField2->setStep($incidentType13Phase2Step1);
        //                     $incidentType13Phase2Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase2Step1FormField2);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase2Step1FormField3 = new IncidentFormFields();
        //                     $incidentType13Phase2Step1FormField3->setTitle('Return broken device (skip if lost/stolen)');
        //                     $incidentType13Phase2Step1FormField3->setOrdering(3);
        //                     $incidentType13Phase2Step1FormField3->setErrorMsg('');
        //                     $incidentType13Phase2Step1FormField3->setFormType('date');
        //                     $incidentType13Phase2Step1FormField3->setValue([]);
        //                     $incidentType13Phase2Step1FormField3->setOptions([]);
        //                     $incidentType13Phase2Step1FormField3->setStep($incidentType13Phase2Step1);
        //                     $incidentType13Phase2Step1FormField3->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase2Step1FormField3);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase2Step1FormField4 = new IncidentFormFields();
        //                     $incidentType13Phase2Step1FormField4->setTitle('Update serial #/device inventory info');
        //                     $incidentType13Phase2Step1FormField4->setOrdering(4);
        //                     $incidentType13Phase2Step1FormField4->setErrorMsg('');
        //                     $incidentType13Phase2Step1FormField4->setFormType('Input');
        //                     $incidentType13Phase2Step1FormField4->setValue([]);
        //                     $incidentType13Phase2Step1FormField4->setOptions([]);
        //                     $incidentType13Phase2Step1FormField4->setStep($incidentType13Phase2Step1);
        //                     $incidentType13Phase2Step1FormField4->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase2Step1FormField4);
        //                     $this->em->flush();
        //
        //             // Incident Phase Add
        //             $incidentType13Phase3 = new IncidentPhase();
        //             $incidentType13Phase3->setTitle('Receipt of New Device (Station Manager)');
        //             $incidentType13Phase3->setOrdering(3);
        //             $incidentType13Phase3->setCompany($company);
        //             $incidentType13Phase3->setRole($company->getCompanyRoles()[4]); //owner
        //             $incidentType13Phase3->setIncidentType($incidentType13);
        //             $incidentType13Phase3->setCreatedDate($currentDateTime);
        //             $this->em->persist($incidentType13Phase3);
        //             $this->em->flush();
        //
        //                 //Incident Phase 1 Steps 1
        //                 $incidentType13Phase3Step1 = new IncidentStep();
        //                 $incidentType13Phase3Step1->setPhase($incidentType13Phase3);
        //                 $incidentType13Phase3Step1->setTitle('');
        //                 $incidentType13Phase3Step1->setInstructions('');
        //                 $incidentType13Phase3Step1->setStepNumber(1);
        //                 $incidentType13Phase3Step1->setOrdering(1);
        //                 $this->em->persist($incidentType13Phase3Step1);
        //                 $this->em->flush();
        //
        //                     //Incident Form Fields
        //                     $incidentType13Phase3Step1FormField1 = new IncidentFormFields();
        //                     $incidentType13Phase3Step1FormField1->setLabel('Device Received?');
        //                     $incidentType13Phase3Step1FormField1->setOrdering(1);
        //                     $incidentType13Phase3Step1FormField1->setFieldId('');
        //                     $incidentType13Phase3Step1FormField1->setFieldData([]);
        //                     $incidentType13Phase3Step1FormField1->setFieldRule('');
        //                     $incidentType13Phase3Step1FormField1->setFieldStyle('');
        //                     $incidentType13Phase3Step1FormField1->setFormType('Radio');
        //                     $incidentType13Phase3Step1FormField1->setValue(['Yes' => true, 'No' => false]);
        //                     $incidentType13Phase3Step1FormField1->setOptions([]);
        //                     $incidentType13Phase3Step1FormField1->setStep($incidentType13Phase3Step1);
        //                     $incidentType13Phase3Step1FormField1->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase3Step1FormField1);
        //                     $this->em->flush();
        //
        //                     $incidentType13Phase3Step1FormField2 = new IncidentFormFields();
        //                     $incidentType13Phase3Step1FormField2->setLabel('Set Up New Device');
        //                     $incidentType13Phase3Step1FormField2->setOrdering(1);
        //                     $incidentType13Phase3Step1FormField2->setFieldId('');
        //                     $incidentType13Phase3Step1FormField2->setFieldData('');
        //                     $incidentType13Phase3Step1FormField2->setFieldRule('');
        //                     $incidentType13Phase3Step1FormField2->setFieldStyle('');
        //                     $incidentType13Phase3Step1FormField2->setFormType('Input');
        //                     $incidentType13Phase3Step1FormField2->setValue([]);
        //                     $incidentType13Phase3Step1FormField2->setOptions([]);
        //                     $incidentType13Phase3Step1FormField2->setStep($incidentType13Phase3Step1);
        //                     $incidentType13Phase3Step1FormField2->setCreatedDate($currentDateTime);
        //                     $this->em->persist($incidentType13Phase3Step1FormField2);
        //                     $this->em->flush();

        // Information

        $io->success('Company Incident Added Successfully!!');

        return 0;
    }
}
