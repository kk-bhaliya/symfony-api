<?php

namespace App\Command;

use App\Criteria\Common;
use App\Entity\DriverRoute;
use App\Entity\PaycomSettings;
use App\Entity\Station;
use App\Utils\EventEmitter;
use App\Utils\GlobalUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CrawlPaycomCommand extends Command
{
    protected static $defaultName = 'dsp:crawl:paycom';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EventEmitter
     */
    private $eventEmitter;

    /**
     * @var ParameterBagInterface
     */
    private $bag;

    /**
     * CrawlPaycomCommand constructor.
     * @param EntityManagerInterface $em
     * @param EventEmitter $eventEmitter
     * @param ParameterBagInterface $bag
     */
    public function __construct(
        EntityManagerInterface $em,
        EventEmitter $eventEmitter,
        ParameterBagInterface $bag
    ) {
        $this->em = $em;
        $this->eventEmitter = $eventEmitter;
        $this->bag = $bag;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Crawl Paycom account for a specific company')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit how many companies to run')
            ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'First company to be run (offset, not ID)')
            ->addOption('companies', 'c', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Specific company IDs to run', []);
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $opts = (object) $input->getOptions();

        /* @var $repo \App\Repository\PaycomSettingsRepository */
        $accounts = new ArrayCollection(
            $this->em->getRepository(PaycomSettings::class)
                ->findEnabledAccounts()
        );

        if ( count( $opts->companies ) )
            $accounts = $accounts->filter(function ($account) use ( $opts ) {
                return in_array( $account->getCompany()->getId(), $opts->companies );
            });

        if ( !$accounts->count() )
            return $io->comment('No active accounts');

        /* @var $account \App\Entity\PaycomSettings */
        foreach ($accounts as $k => $account) {
            $company = $account->getCompany();

            $stations = $this->em->createQueryBuilder()
                ->select('s')
                ->from(Station::class, 's')
                ->addCriteria(Common::notArchived('s'))
                ->andWhere('s.company = :company')
                ->setParameter('company', $company)
                ->getQuery()
                ->getResult();

            foreach ($stations as $station) {
                $stationTz  = new \DateTimeZone($station->getTimeZoneName());
                $stationNow = new \DateTime('now', $stationTz);
                $utcNow     = GlobalUtility::newUTCDateTime()->format('Y-m-d H:i:00');
                $offset     = $stationNow->format('P');

                $driverRoutes = $this->em->createQueryBuilder()
                    ->select('dr')
                    ->from(DriverRoute::class, 'dr')
                    ->leftJoin('dr.driver', 'd')
                    ->addCriteria(Common::notArchived('dr'))
                    ->addCriteria(Common::notArchived('d'))
                    ->andWhere('d.paycomId is not null')
                    ->andWhere("dr.dateCreated = DATE(CONVERT_TZ(:utcNow, '+00:00', :offset))")
                    ->setParameter('utcNow', $utcNow)
                    ->setParameter('offset', $offset)
                    ->andWhere('d.company = :company')
                    ->setParameter('company', $company)
                    ->andWhere('dr.station = :station')
                    ->setParameter('station', $station)
                    ->getQuery()
                    ->getResult()
                ;

                if( count($driverRoutes) === 0 )
                    continue;

                $paycomIds = array_map(function ($dr) {
                    return $dr->getDriver()->getPaycomId();
                }, $driverRoutes);

                $payload = json_encode([
                    'apiHost'    => $this->bag->get('api_host'),
                    'company'    => $company->getId(),
                    'clientCode' => $account->getClientCode(),
                    'username'   => $account->getUsername(),
                    'password'   => $account->getPassword(),
                    'pin1'       => $account->getPin1(),
                    'pin2'       => $account->getPin2(),
                    'pin3'       => $account->getPin3(),
                    'pin4'       => $account->getPin4(),
                    'pin5'       => $account->getPin5(),
                    'ids'        => $paycomIds,
                    'date'       => $stationNow->format('U'),
                    'timezone'   => $station->getTimeZoneName()
                ]);

                $event = json_encode([
                    'version' => '2.0',
                    'routeKey' => '$default',
                    'rawPath' => '/',
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ],
                    'requestContext' => [
                        'http' => [
                            'method' => 'POST',
                        ],
                    ],
                    'body' => $payload,
                    'isBase64Encoded' => false,
                ]);

                $client = new \Aws\Lambda\LambdaClient([
                    'version' => 'latest',
                    'region' => $this->bag->get('aws_region'),
                    'credentials' => [
                        'key' => $this->bag->get('aws_key'),
                        'secret' => $this->bag->get('aws_secret')
                    ]
                ]);

                $result = $client->invoke([
                    'FunctionName' => 'paycom',
                    'InvocationType' => 'Event',
                    'Payload' => $event
                ]);

                $io->note("Company {$company->getId()} - Station {$station->getId()} : {$result->get('StatusCode')}");
                $io->newLine();
            }
        }

        $io->newLine();
        $io->success('Done');
    }
}
