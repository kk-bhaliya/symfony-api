<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Entity\Driver;

class ScheduleAddDriverRouteCommand extends Command
{
    protected static $defaultName = 'schedule:add:driverroute';
    private $em;
    private $kernel;

    public function __construct(
        EntityManagerInterface $em,
        KernelInterface $kernel
    ) {
        $this->em = $em;
        $this->kernel = $kernel;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Schedule add driver route')
            ->addArgument('scheduleId', InputArgument::OPTIONAL, 'Pass Schedule Id');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $canCreateOpenShift = false;
        $scheduleId = $input->getArgument('scheduleId');

        // Fetch all Schedule
        if ($scheduleId) {
            $schedule = $this->em->getRepository('App\Entity\Schedule')->findOneBy(['id' => $scheduleId, 'isArchive' => false]);
        } else {
            $schedule = $this->em->getRepository('App\Entity\Schedule')->getLatestAllSchedule();
        }

        $weekFunctionArray = ['getSunday', 'getMonday', 'getTuesday', 'getWednesday', 'getThursday', 'getFriday', 'getSaturday'];
        $dateArray=$driverArray=[];
        // Loop all schedule
        //foreach ($schedules as $schedule) {
        if($schedule){
            $this->em->createQueryBuilder()
                 ->update('App\Entity\UpdatedSchedule', 'us')
                 ->set('us.status', 1)
                 ->where('us.schedule = :schedule')
                 ->setParameter('schedule', $schedule->getId())
                 ->getQuery()
                 ->execute();

            $today = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $today->modify('+1 day');
            $today = ($today)->format('Y-m-d');

            foreach ($schedule->getUpdatedSchedules()->filter(function ($us) {
                return $us->getStatus();
            }) as $sud) {
                if(!empty($sud->getScheduleDate())){
                    $dateObj = $sud->getScheduleDate();
                    $dateObj->modify('+1 day');
                    $today = $dateObj->format('Y-m-d');
                }
            }

            $companyId = $schedule->getCompany()->getId();

            $driverSkills = $scheduleDriver = $scheduleWeekArr = [];

            foreach ($schedule->getDrivers()->filter(function ($dr) {
               if( $dr->getEmployeeStatus() == Driver::STATUS_ACTIVE )
                    return true;
                else
                    return false;
            }) as $driver) {
                $scheduleDriver[] = $driver->getId();
                // Get driver's all skill
                foreach ($driver->getDriverSkills()->filter(function ($skill) {
                    return !$skill->getIsArchive();
                }) as $driverSkill) {
                    $driverSkills[$driver->getId()][] = $driverSkill->getSkill()->getId();
                }
            }

            // Get updated Driver Routes
            $updatedDriverRoutesArray = $this->em->getRepository('App\Entity\TempDriverRoute')->getUpdatedDriverRouteBySchedule($schedule->getId());

            // Unassign driver route from driver which does exist in current schedule and also does not updated yet.
            $this->em->getRepository('App\Entity\DriverRoute')->updateDriverNullInDriverRoute($schedule->getId(), $scheduleDriver, $updatedDriverRoutesArray);

            // Get current and future week schedule design data
            $scheduleDesigns = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignShiftLatestWeek($schedule->getId());

            $gendate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));

            $isRecurring = $schedule->getIsRecurring();
            $startDateOfSchedule = $schedule->getStartDate();
            $endDateOfSchedule = $schedule->getEndDate();
            $lastDateOfSchedule = $schedule->getScheduleLastDate();
            $startDateOfSchedule = $startDateOfSchedule->format('Y-m-d');
            if($endDateOfSchedule) {
                $endDateOfSchedule = $endDateOfSchedule->format('Y-m-d');
            }

            if($startDateOfSchedule > $today ) {
                $today = $startDateOfSchedule;
            }
            if($isRecurring) {
                if($lastDateOfSchedule) {
                    $lastDateOfSchedule = $lastDateOfSchedule->format('Y-m-d');
                }
                if(empty($endDateOfSchedule)) {
                    $endDateOfSchedule = $lastDateOfSchedule;
                }
            }

            foreach ($scheduleDesigns as $scheduleDesign) {
                $scheduleWeekArr[$scheduleDesign->getWeek()] = ["week" => $scheduleDesign->getWeek(), "year" => $scheduleDesign->getYear(), "company" => $companyId];

                // Check week day is true or not
                $weekDayArray = [];

                foreach ($weekFunctionArray as $key => $weekDay) {
                    if ($scheduleDesign->$weekDay() == 1) {
                        $generatedDate = $gendate->setISODate($scheduleDesign->getYear(), $scheduleDesign->getWeek(), $key)->format('Y-m-d');
                        if (($generatedDate >= $today) && $generatedDate <= $endDateOfSchedule) {
                            $weekDayArray[$scheduleDesign->getWeek()][] = $generatedDate;
                        }
                    }
                }

                $shift = !empty($scheduleDesign->getShifts()) ? $scheduleDesign->getShifts()[0] : '';

                // Soft Delete all future driver route which date removed from schedule design
                if (!empty($shift)) {
                    $this->em->getRepository('App\Entity\DriverRoute')->shiftDateChangeRemoveDriverRoute($shift->getId(), $schedule->getId(), isset($weekDayArray[$scheduleDesign->getWeek()]) ? $weekDayArray[$scheduleDesign->getWeek()] : [], $scheduleDesign->getWeek(), $scheduleDesign->getYear(), $updatedDriverRoutesArray, $today);
                }

                // Get shift id array from schedule design week data
                $scheduleAllShifts = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignShiftByWeek($schedule->getId(), $scheduleDesign->getWeek(), $scheduleDesign->getYear());
                $scheduleShifts = array_column($scheduleAllShifts, 'id');

                // Soft Delete all future driver route which shift removed from schedule design
                $this->em->getRepository('App\Entity\DriverRoute')->shiftChangeRemoveDriverRoute($schedule->getId(), $scheduleShifts, $scheduleDesign->getWeek(), $scheduleDesign->getYear(), $updatedDriverRoutesArray, $today);

                if (!empty($scheduleDriver)) {

                    // Driver skill loop
                    foreach ($driverSkills as $driverId => $driverSkill) {

                        // Check shift's skill match with driver's skill array
                        if (!empty($shift) && !empty($weekDayArray) && in_array($shift->getSkill()->getId(), $driverSkill)) {

                            foreach ($weekDayArray[$scheduleDesign->getWeek()] as $weekDate) {

                                // In Schedule "Return drivers to previous assigned schedule after end date" has true then make isActive false for all driver route which schedule has above flag is false
                                $isActive = 1;
                                if ($schedule->getPreviousAssignSchedule() == 1) {
                                    $this->em->getRepository('App\Entity\DriverRoute')->setScheduleDriverRouteActive($weekDate, $driverId, $schedule->getId(), $today);
                                } else {
                                    $bResultData = $this->em->getRepository('App\Entity\DriverRoute')->setCurrentScheduleDriverRouteFalse($weekDate, $driverId, $schedule->getId(), $today);
                                    $isActive = ($bResultData > 0) ? 0 : 1;
                                }

                                // Add or Update Driver Route By Date
                                $this->em->getRepository('App\Entity\DriverRoute')->addOrUpdateScheduleDriverRoute($weekDate, $shift->getId(), $schedule->getId(), $isActive, $updatedDriverRoutesArray, $today, $driverId);
                                $dateArray[]=$weekDate;
                                $driverArray[]=$driverId;
                            }
                        }
                    }
                } else if ($canCreateOpenShift) {
                    // Check shift's skill match with driver's skill array
                    if (!empty($shift) && !empty($weekDayArray)) {

                        foreach ($weekDayArray[$scheduleDesign->getWeek()] as $weekDate) {
                            // Add or Update Driver Route By Date
                            $this->em->getRepository('App\Entity\DriverRoute')->addOrUpdateScheduleDriverRoute($weekDate, $shift->getId(), $schedule->getId(), true, $updatedDriverRoutesArray,$today);
                        }
                    }
                }
            }

            // Soft Delete all future driver route which not match with schedule start and end date.
            $this->em->getRepository('App\Entity\DriverRoute')->datesChangeRemoveDriverRoute($schedule->getId(), $startDateOfSchedule, $endDateOfSchedule, $updatedDriverRoutesArray);

            // Inactive all conflict route of another schedule of selected schedule
            if($schedule->getEffectiveDate()){
                $this->em->getRepository('App\Entity\DriverRoute')->disableConflictRoutes($schedule->getEffectiveDate(),array_unique($dateArray),array_unique($driverArray),$schedule->getId());
            }

            $this->em->createQueryBuilder()
                ->delete('App\Entity\UpdatedSchedule', 'us')
                ->where('us.schedule = :schedule')
                ->setParameter('schedule', $schedule->getId())
                ->getQuery()
                ->execute();
        }

        $io->success('Schedule Driver Route Added Successfully.');
        return 0;
    }
}
