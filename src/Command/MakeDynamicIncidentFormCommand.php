<?php

namespace App\Command;

use App\Criteria\Common;
use App\Entity\Company;
use App\Entity\IncidentFormFields;
use App\Entity\IncidentPhase;
use App\Entity\IncidentQuestion;
use App\Entity\IncidentStep;
use App\Entity\IncidentType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MakeDynamicIncidentFormCommand extends Command
{
    protected static $defaultName = 'dsp:make:incidentform';

    private $em;
    private $io;
    private $remove = false;
    private $delete = false;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Make Dynamic Incident Form')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit how many companies to run')
            ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'First company to be run (offset, not ID)')
            ->addOption('companies', 'c', InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Specific company IDs to run', [])
            ->addOption('remove', 'r', InputOption::VALUE_NONE, 'First soft delete old data and then insert')
            ->addOption('delete', 'd', InputOption::VALUE_NONE, 'First hard delete old data and then insert');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $opts = (object) $input->getOptions();
        $currentDateTime = new \DateTime();
        $this->remove = false; //($opts->remove) ? true : false;
        $this->delete = false; //($opts->delete) ? true : false;
        if (!$opts->companies) {
            return $this->io->error('No company provided');
        }

        /* @var $repo CompanyRepository */
        $companiesQb = $this->em->createQueryBuilder()
            ->select('c')
            ->from(Company::class, 'c')
            ->addCriteria(Common::notArchived('c'));

            if ($opts->limit) {
                $companiesQb->setMaxResults($opts->limit);
            }
            if ($opts->start) {
                $companiesQb->setFirstResult($opts->start);
            }
            if ($opts->companies && count($opts->companies) > 0) {
                $companiesQb->andWhere($companiesQb->expr()->in('c.id', ':companies'))->setParameter('companies', $opts->companies);
            }
        $companiesObject = $companiesQb->getQuery()->getResult();
        $companies = new ArrayCollection($companiesObject);

        if (!$companies->count()) {
            return $this->io->error('No companies found !!!');
        }

        foreach ($companies as $company) {
            $roles = [];

            foreach ($company->getCompanyRoles()->filter(function ($roleObj) {
                return !$roleObj->getIsArchive();
            }) as $role) {
                $roles[$role->getName()] = $role;
            }

            $this->io->newLine();
            $this->io->title('Company ' . $company->getName());
            $this->io->newLine();

            //Global Questions
            $this->io->comment('Global Questions');
            $globalQuestions = $this->createGlobalQuestions($company);
            $this->io->newLine();

            // Incident Types
            $this->io->comment('Incident Types');
            $incidentTypes = $this->createIncidentTypes($company);
            $this->io->newLine();

            // Incident Phases
            $this->io->comment('Incident Phases');
            $incidentPhases = $this->createIncidentPhases($company, $globalQuestions, $incidentTypes, $roles);
            $this->io->newLine();

            // Incident Steps
            $this->io->comment('Incident Steps');
            $incidentSteps = $this->createIncidentSteps($company, $incidentPhases);
            $this->io->newLine();

            // Incident Fields
            $this->io->comment('Incident Fields');
            $incidentFields = $this->createIncidentFields($company, $globalQuestions, $incidentSteps);
            $this->io->newLine();

        }
        $this->io->newLine();
        $this->io->comment('Done');
        return $this->io->success('Dynamic Incident Created Successfully!!');
    }

    /**
     * Global Question Section.
     */
    public function removeIncidentQuestions($company)
    {

        $this->io->note('Removing incident questions for "' . $company->getName() . '" company');

        $this->io->progressStart();
        $this->io->newLine();

        $this->io->progressAdvance();
        $this->io->newLine();

        $this->em->getRepository(IncidentQuestion::class)->removeAllIncidentQuestionsByCompany($company);
        $this->io->progressFinish();
        $this->io->newLine();

        return true;
    }

    public function deleteIncidentQuestions($company)
    {

        $this->io->note('Deleting incident questions for "' . $company->getName() . '" company');

        $this->io->progressStart();
        $this->io->newLine();

        $this->io->progressAdvance();
        $this->io->newLine();

        $this->em->getRepository(IncidentQuestion::class)->deleteAllIncidentQuestionsByCompany($company);
        $this->io->progressFinish();
        $this->io->newLine();

        return true;
    }

    public function createGlobalQuestions($company)
    {

        if ($this->remove) {
            $this->removeIncidentQuestions($company);
        }

        if ($this->delete) {
            $this->deleteIncidentQuestions($company);
        }

        $this->io->note('Starting create dynamic questions for "' . $company->getName() . '" company');

        $globalQuestions = [];

        $fieldRule = [
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null, "dependOnStep" => null, "dependOnPhase" => null],
            "validations" => ["required" => false],
        ];

        $fieldStyle = ["class" => "", "style" => "", "data-attr" => ""];

        $this->io->progressStart();
        $this->io->newLine();

        /**
         * Entity Type Questions.
         */

        // 1. Driver Selection
        $globalQuestions['enitity_driver'] = $this->insertGlobalQuestion('enitity_driver', $company, null, IncidentQuestion::TYPE_ENTITY, ["entity" => "Driver", "repoFunction" => "form_getDrivers", "fieldType" => IncidentQuestion::TYPE_SELECT], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 2. Station Selection
        $globalQuestions['enitity_station'] = $this->insertGlobalQuestion('enitity_station', $company, null, IncidentQuestion::TYPE_ENTITY, ["entity" => "Station", "repoFunction" => "form_getStations", "fieldType" => IncidentQuestion::TYPE_SELECT], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 3. Shift Selection
        $globalQuestions['enitity_shift'] = $this->insertGlobalQuestion('enitity_shift', $company, null, IncidentQuestion::TYPE_ENTITY, ["entity" => "Shift", "repoFunction" => "form_getShifts", "fieldType" => IncidentQuestion::TYPE_SELECT], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 4. Asign Driver Selection
        $globalQuestions['enitity_assign_driver'] = $this->insertGlobalQuestion('enitity_assign_driver', $company, null, IncidentQuestion::TYPE_ENTITY, ["entity" => "Driver", "repoFunction" => "form_getDriversForAsign", "fieldType" => IncidentQuestion::TYPE_SELECT], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        /**
         * Global Field Type Questions
         */

        // 1. Hidden Field Question
        $globalQuestions['type_hidden'] = $this->insertGlobalQuestion('type_hidden', $company, null, IncidentQuestion::TYPE_HIDDEN, [], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 2. File Upload
        $globalQuestions['type_file'] = $this->insertGlobalQuestion('type_file', $company, null, IncidentQuestion::TYPE_FILE, [], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 3. Textarea field
        $globalQuestions['type_textarea'] = $this->insertGlobalQuestion('type_textarea', $company, null, IncidentQuestion::TYPE_TEXTAREA, [], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 4. Numeric Field
        $globalQuestions['type_numeric'] = $this->insertGlobalQuestion('type_numeric', $company, null, IncidentQuestion::TYPE_NUMBER, [], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        /**
         * Custom Select Type
         */

        // 1. Reason for call in
        $globalQuestions['reason_for_call'] = $this->insertGlobalQuestion('reason_for_call', $company, null, IncidentQuestion::TYPE_SELECT, ['sick' => 'Sick', 'injury' => 'Injury', 'pets' => 'Pets', 'vehicle-issue' => 'Vehicle Issue', 'family-emergency' => 'Family Emergency', 'overslept' => 'Overslept', 'other' => 'Other', 'quit' => 'Quit','abandoned' => 'Abandoned', 'kids' => 'Kids', 'sent-home' => 'Sent Home', 'preventable' => 'Preventable','attitude' => 'Attitude', 'not-at-fault' => 'Not At Fault','car' => 'Car'], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 2. Dropdown for yes/no
        $globalQuestions['dropdown_yes_no'] = $this->insertGlobalQuestion('dropdown_yes_no', $company, null, IncidentQuestion::TYPE_SELECT, ['yes' => 'Yes', 'no' => 'No'], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 3. shift time for ncns
        $globalQuestions['shift_times'] = $this->insertGlobalQuestion('shift_times', $company, null, IncidentQuestion::TYPE_SELECT, ['weekday' => 'Weekday', 'weekend' => 'Weekend'], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 4. driver contacted for ncns
        $globalQuestions['driver_contacted'] = $this->insertGlobalQuestion('driver_contacted', $company, null, IncidentQuestion::TYPE_CHECKBOX, ['call' => 'Call', 'text' => 'Text', 'email' => 'Email', 'nocontacted' => 'No contacted'], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        // 5. driver strikes for ncns
        $globalQuestions['driver_strikes'] = $this->insertGlobalQuestion('driver_strikes', $company, null, IncidentQuestion::TYPE_SELECT, ['0' => '0', '1' => '1', '2' => '2', '3' => '3'], $fieldRule, $fieldStyle);
        $this->io->progressAdvance();
        $this->io->newLine();

        $this->io->progressFinish();
        $this->io->newLine();

        return $globalQuestions;
    }

    public function insertGlobalQuestion($token, $company, $label, $incidentType, $fieldData, $fieldRule, $fieldStyle)
    {

        $incidentQuestion = $this->em->getRepository('App\Entity\IncidentQuestion')->findOneBy(['token'=>$token,"company"=>$company->getId()]);
        if(empty($incidentQuestion)){
            $incidentQuestion = new IncidentQuestion();
            $incidentQuestion->setToken($token);
        }
        $incidentQuestion->setCompany($company);
        $incidentQuestion->setLabel($label);
        $incidentQuestion->setFieldId($incidentType);
        $incidentQuestion->setFieldData($fieldData);
        $incidentQuestion->setFieldRule($fieldRule);
        $incidentQuestion->setFieldStyle($fieldStyle);
        $incidentQuestion->setIsArchive(false);

        $this->em->persist($incidentQuestion);
        $this->em->flush();

        return $incidentQuestion;
    }

    /**
     * Incident Type Section.
     */

    public function removeIncidentTypes($company)
    {

        $this->io->note('Removing incident types for "' . $company->getName() . '" company');

        $this->io->progressStart();
        $this->io->newLine();

        $this->io->progressAdvance();
        $this->io->newLine();

        $this->em->getRepository(IncidentType::class)->removeAllIncidentTypesByCompany($company);
        $this->io->progressFinish();
        $this->io->newLine();

        return true;
    }

    public function deleteIncidentTypes($company)
    {

        $this->io->note('Deleting incident types for "' . $company->getName() . '" company');

        $this->io->progressStart();
        $this->io->newLine();

        $this->io->progressAdvance();
        $this->io->newLine();

        $this->em->getRepository(IncidentType::class)->deleteAllIncidentTypesByCompany($company);
        $this->io->progressFinish();
        $this->io->newLine();

        return true;
    }

    public function createIncidentTypes($company)
    {

        if ($this->remove) {
            $this->removeIncidentTypes($company);
        }

        if ($this->delete) {
            $this->deleteIncidentTypes($company);
        }

        $incidentTypes = [];
        $this->io->note('Starting create incident types for "' . $company->getName() . '" company');
        $this->io->progressStart();
        $this->io->newLine();

        $incidentTypesArray = [
            'Acc' => 'Accident/Injury',
            'Cal' => 'Call In',
            'Ncn' => 'NCNS',
            'Lat' => 'Late',
            'Roa' => 'Roadside',
            'Res' => 'Resignation',
            'Dis' => 'Disrespect',
            'Upd' => 'UPD',
            'Dro' => 'Drop Route',
            'Abo' => 'Abondoned Route',
            'The' => 'Theft',
            'Cus' => 'Customer Complaint',
            'Inc' => 'Incomplete Route',
            'Dev' => 'Device Damage',
        ];

        $avaibaleInWeb = [
            'Acc' => true,
            'Cal' => true,
            'Ncn' => true,
            'Lat' => true,
            'Roa' => true,
            'Res' => true,
            'Dis' => true,
            'Upd' => true,
            'Dro' => true,
            'Abo' => true,
            'The' => true,
            'Cus' => true,
            'Inc' => true,
            'Dev' => true,
        ];

        $avaibaleInApp = [
            'Acc' => true,
            'Cal' => false,
            'Ncn' => true,
            'Lat' => true,
            'Roa' => false,
            'Res' => true,
            'Dis' => false,
            'Upd' => false,
            'Dro' => false,
            'Abo' => false,
            'The' => false,
            'Cus' => false,
            'Inc' => false,
            'Dev' => false,
        ];

        foreach ($incidentTypesArray as $prefix => $incidentType) {

            $incidentTypes[$prefix] = $this->em->getRepository('App\Entity\IncidentType')->findOneBy(['token'=>$prefix,"company"=>$company->getId()]);
            if(empty($incidentTypes[$prefix])){
                $incidentTypes[$prefix] = new IncidentType();
                $incidentTypes[$prefix]->setToken($prefix);
            }
            $incidentTypes[$prefix]->setName($incidentType);
            $incidentTypes[$prefix]->setCompany($company);
            $incidentTypes[$prefix]->setPrefix($prefix);
            $incidentTypes[$prefix]->setIsInWeb($avaibaleInWeb[$prefix]);
            $incidentTypes[$prefix]->setIsInApp($avaibaleInApp[$prefix]);

            $this->em->persist($incidentTypes[$prefix]);
            $this->em->flush();
            $this->io->progressAdvance();
            $this->io->newLine();
        }

        $this->io->progressFinish();
        $this->io->newLine();

        return $incidentTypes;
    }

    /**
     * Incident Phase Section.
     */

    public function createIncidentPhases($company, $globalQuestions, $incidentTypes, $roles)
    {

        $this->io->note('Starting create incident phases for "' . $company->getName() . '" company');

        $incidentPhases = [];

        $this->io->progressStart();
        $this->io->newLine();
        $this->io->newLine();

        /**
         * Type Call In
         */

        // 1. Phase 'Call In Report'
        $incidentPhases['call-in-report'] = $this->insertIncidentPhase('call-in-report', 'Call In Report', isset($roles['ROLE_DELIVERY_ASSOCIATE']) ? $roles['ROLE_DELIVERY_ASSOCIATE'] : null, false, $incidentTypes['Cal'], 0, true, false, 0, true);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentTypes['Cal']->getName() . '" incident types.');

        // 2. Phase 'Review'
        $incidentPhases['call-in-review'] = $this->insertIncidentPhase('call-in-review', 'Review', null, true, $incidentTypes['Cal'], 1440, true, false, 1, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentTypes['Cal']->getName() . '" incident types.');

        /**
         * Type NCNS
         */

        // 1. Phase 'NCNS Report'
        $incidentPhases['ncns-report'] = $this->insertIncidentPhase('ncns-report', 'NCNS Report', isset($roles['ROLE_DELIVERY_ASSOCIATE']) ? $roles['ROLE_DELIVERY_ASSOCIATE'] : null, false, $incidentTypes['Ncn'], 0, true, false, 0, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentTypes['Ncn']->getName() . '" incident types.');

        // 2. Phase 'Review'
        $incidentPhases['ncns-review'] = $this->insertIncidentPhase('ncns-review', 'Review', null, true, $incidentTypes['Ncn'], 1440, true, false, 1, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentTypes['Ncn']->getName() . '" incident types.');

        $this->io->progressFinish();
        $this->io->newLine();

        return $incidentPhases;
    }

    public function insertIncidentPhase($token, $title, $role, $onlyAccessToManagerOfDriver, $incidentType, $allocatedTime, $isInWeb, $isInApp, $ordering, $hasReview)
    {
        $incidentPhase = $this->em->getRepository('App\Entity\IncidentPhase')->findOneBy(['token'=>$token, "incidentType"=>$incidentType->getId()]);
        if(empty($incidentPhase)){
            $incidentPhase = new IncidentPhase();
            $incidentPhase->setToken($token);
        }
        $incidentPhase->setTitle($title);
        $incidentPhase->setRole($role);
        $incidentPhase->setOnlyAccessToManagerOfDriver($onlyAccessToManagerOfDriver);
        $incidentPhase->setIncidentType($incidentType);
        $incidentPhase->setAllocatedTime($allocatedTime);
        $incidentPhase->setIsInWeb($isInWeb);
        $incidentPhase->setIsInApp($isInApp);
        $incidentPhase->setOrdering($ordering);
        $incidentPhase->setHasReview($hasReview);

        $this->em->persist($incidentPhase);
        $this->em->flush();

        return $incidentPhase;
    }

    /**
     * Incident Step Section.
     */

    public function createIncidentSteps($company, $incidentPhases)
    {

        $this->io->note('Starting create incident steps for "' . $company->getName() . '" company');

        $incidentSteps = [];

        $this->io->progressStart();
        $this->io->newLine();

        /**
         * Type Call In
         */

        // 1. Steps of Phase 'Call In Report'
        $incidentSteps['call-in-report-step1'] = $this->insertIncidentStep('call-in-report-step1', $incidentPhases['call-in-report'], 'Reason for call in', '', 2, 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-report']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['call-in-report-step2'] = $this->insertIncidentStep('call-in-report-step2', $incidentPhases['call-in-report'], 'Assigned To', '', 3, 3, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-report']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        // 2. Steps of Phase 'Review'
        $incidentSteps['call-in-review-step1'] = $this->insertIncidentStep('call-in-review-step1', $incidentPhases['call-in-review'], 'Confirmed excused / unexcused', 'Was the call-in excused?', 1, 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['call-in-review-step2'] = $this->insertIncidentStep('call-in-review-step2', $incidentPhases['call-in-review'], 'Upload documents', 'If incident was excusable then please upload any required documentation.', 2, 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['call-in-review-step3'] = $this->insertIncidentStep('call-in-review-step3', $incidentPhases['call-in-review'], 'Corrective Action', 'Is there a need for corrective action?', 3, 3, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['call-in-review-step4'] = $this->insertIncidentStep('call-in-review-step4', $incidentPhases['call-in-review'], 'Notes', 'Please provide any notes about this case.', 4, 4, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['call-in-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        /**
         * Type NCNS
         */

        // 1. NCNS Report phase steps
        $incidentSteps['ncns-report-step1'] = $this->insertIncidentStep('ncns-report-step1', $incidentPhases['ncns-report'], 'For which type of Shift did the Driver no call/no show?', 'Which shift did the driver not call and show for?', 3, 3, true, false, true);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-report']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-report-step2'] = $this->insertIncidentStep('ncns-report-step2', $incidentPhases['ncns-report'], 'Notify Driver?', 'Selecting this will notify the driver about this incident via text message.', 4, 4, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-report']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-report-step3'] = $this->insertIncidentStep('ncns-report-step3', $incidentPhases['ncns-report'], 'NCNS Shift Time', 'Was the shift during the weekday or on the weekend?', 5, 5, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-report']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        // 2. NCNS In Report phase steps
        $incidentSteps['ncns-review-step1'] = $this->insertIncidentStep('ncns-review-step1', $incidentPhases['ncns-review'], 'Was driver contacted?', 'Did anyone contact the driver and if so what method?', 1, 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-review-step2'] = $this->insertIncidentStep('ncns-review-step2', $incidentPhases['ncns-review'], 'Excused?', 'Is there a reason that the NCNS is excused?', 2, 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-review-step3'] = $this->insertIncidentStep('ncns-review-step3', $incidentPhases['ncns-review'], 'Upload Material', 'What material is to be given if there is an excuse?', 3, 3, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-review-step4'] = $this->insertIncidentStep('ncns-review-step4', $incidentPhases['ncns-review'], 'Driver Strikes?', 'If not excused how many strikes is this?', 4, 4, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $incidentSteps['ncns-review-step5'] = $this->insertIncidentStep('ncns-review-step5', $incidentPhases['ncns-review'], 'Correction Action ', 'If strike given should we deliver a correction action form to the driver? If Strike is not given - then correction action is optional.', 5, 5, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentPhases['ncns-review']->getTitle() . '" incident Phase.');
        $this->io->newLine();

        $this->io->progressFinish();
        $this->io->newLine();

        return $incidentSteps;
    }

    public function insertIncidentStep($token, $phase, $title, $instruction, $stepNumber, $ordering, $isInWeb, $isInApp, $isArchive)
    {

        $incidentStep = $this->em->getRepository('App\Entity\IncidentStep')->findOneBy(['token'=>$token,'phase'=>$phase->getId()]);
        if(empty($incidentStep)){
            $incidentStep = new IncidentStep();
            $incidentStep->setToken($token);
        }

        $incidentStep->setPhase($phase);
        $incidentStep->setTitle($title);
        $incidentStep->setInstructions($instruction);
        $incidentStep->setStepNumber($stepNumber);
        $incidentStep->setOrdering($ordering);
        $incidentStep->setIsInApp($isInApp);
        $incidentStep->setIsInWeb($isInWeb);
        $incidentStep->setIsArchive($isArchive);
        $this->em->persist($incidentStep);
        $this->em->flush();

        return $incidentStep;
    }

    /**
     * Incident field Section.
     */

    public function createIncidentfields($company, $globalQuestions, $incidentSteps)
    {

        $this->io->note('Starting create incident fields for "' . $company->getName() . '" company');

        $incidentFields = [];

        $this->io->progressStart();
        $this->io->newLine();

        $fieldRuleRequireTrue = [
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null, "dependOnStep" => null, "dependOnPhase" => null],
            "validations" => ["required" => "this field is required"],
        ];

        $fieldRuleAsignTo = [
            "depend" => ["isDepend" => false, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => null, "dependOnStep" => null, "dependOnPhase" => null],
            "validations" => [],
            "hasAsingn" => true
        ];

        /**
         * Type Call In
         */

        // 1. Call In step1 fields
        $incidentFields['call-in-report-step1-field1'] = $this->insertIncidentFields('call-in-report-step1-field1', $incidentSteps['call-in-report-step1'], $globalQuestions['reason_for_call'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-report-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['call-in-report-step1-field2'] = $this->insertIncidentFields('call-in-report-step1-field2', $incidentSteps['call-in-report-step1'], $globalQuestions['type_textarea'], 'Notes', null, [], [], [], 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-report-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 2. Call In step2 fields
        $incidentFields['call-in-report-step2-field1'] = $this->insertIncidentFields('call-in-report-step2-field1', $incidentSteps['call-in-report-step2'], $globalQuestions['enitity_assign_driver'], 'Assigned To', null, [], $fieldRuleAsignTo, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-report-step2']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['call-in-report-step2-field2'] = $this->insertIncidentFields('call-in-report-step2-field2', $incidentSteps['call-in-report-step2'], $globalQuestions['type_hidden'], 'Reviewed By ', null, [], [], [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-report-step2']->getTitle() . '" incident Step.');
        $this->io->newLine();


        // 1. Review In step1 fields
        $incidentFields['call-in-review-step1-field1'] = $this->insertIncidentFields('call-in-review-step1-field1', $incidentSteps['call-in-review-step1'], $globalQuestions['dropdown_yes_no'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-review-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 2. Review In step2 fields
        $incidentFields['call-in-review-step2-field1'] = $this->insertIncidentFields('call-in-review-step2-field1', $incidentSteps['call-in-review-step2'], $globalQuestions['type_file'], '', null, [], [], [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-review-step2']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 3. Review In step3 fields
        $incidentFields['call-in-review-step3-field1'] = $this->insertIncidentFields('call-in-review-step3-field1', $incidentSteps['call-in-review-step3'], $globalQuestions['dropdown_yes_no'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-review-step3']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 4. Review In step4 fields
        $incidentFields['call-in-review-step4-field1'] = $this->insertIncidentFields('call-in-review-step4-field1', $incidentSteps['call-in-review-step4'], $globalQuestions['type_textarea'], '', null, [], [], [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-review-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['call-in-review-step5-field2'] = $this->insertIncidentFields('call-in-review-step5-field2', $incidentSteps['call-in-review-step4'], $globalQuestions['type_hidden'], '', null, [], [], [], 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['call-in-review-step4']->getTitle() . '" incident Step.');
        $this->io->newLine();

        /**
         * Type NCNS
         */

        // 1. NCNS In step1 fields
        $incidentFields['ncns-report-step1-field1'] = $this->insertIncidentFields('ncns-report-step1-field1', $incidentSteps['ncns-report-step1'], $globalQuestions['enitity_shift'], 'Select Station Shift', null, [], [
            "depend" => ["isDepend" => true, "dependOn" => null, "dependOnValue" => null, "dependOnIncident" => 'station', "dependOnStep" => null, "dependOnPhase" => null],
            "validations" => ["required" => false],
        ], [], 1, true, false, true);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-report-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 2. NCNS In step2 fields
        $incidentFields['ncns-report-step2-field1'] = $this->insertIncidentFields('ncns-report-step2-field1', $incidentSteps['ncns-report-step2'], $globalQuestions['dropdown_yes_no'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-report-step2']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 3. NCNS In step3 fields
        $incidentFields['ncns-report-step3-field1'] = $this->insertIncidentFields('ncns-report-step3-field1', $incidentSteps['ncns-report-step3'], $globalQuestions['shift_times'], 'Select shift time', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-report-step3']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['ncns-report-step3-field2'] = $this->insertIncidentFields('ncns-report-step3-field2', $incidentSteps['ncns-report-step3'], $globalQuestions['type_textarea'], 'Notes', null, [], [], [], 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-report-step3']->getTitle() . '" incident Step.');
        $this->io->newLine();


        // 1. Review In step1 fields
        $incidentFields['ncns-review-step1-field1'] = $this->insertIncidentFields('ncns-review-step1-field1', $incidentSteps['ncns-review-step1'], $globalQuestions['driver_contacted'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step1']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 2. Review In step2 fields
        $incidentFields['ncns-review-step2-field1'] = $this->insertIncidentFields('ncns-review-step2-field1', $incidentSteps['ncns-review-step2'], $globalQuestions['dropdown_yes_no'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step2']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 3. Review In step3 fields
        $incidentFields['ncns-review-step3-field1'] = $this->insertIncidentFields('ncns-review-step3-field1', $incidentSteps['ncns-review-step3'], $globalQuestions['type_file'], '', null, [], [], [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step3']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 4. Review In step4 fields
        $incidentFields['ncns-review-step4-field1'] = $this->insertIncidentFields('ncns-review-step4-field1', $incidentSteps['ncns-review-step4'], $globalQuestions['driver_strikes'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step4']->getTitle() . '" incident Step.');
        $this->io->newLine();

        // 5. Review In step5 fields
        $incidentFields['ncns-review-step5-field1'] = $this->insertIncidentFields('ncns-review-step5-field1', $incidentSteps['ncns-review-step5'], $globalQuestions['dropdown_yes_no'], '', null, [], $fieldRuleRequireTrue, [], 1, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step5']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['ncns-review-step5-field2'] = $this->insertIncidentFields('ncns-review-step5-field2', $incidentSteps['ncns-review-step5'], $globalQuestions['type_hidden'], '', null, [], [], [], 2, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step5']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $incidentFields['ncns-review-step5-field3'] = $this->insertIncidentFields('ncns-review-step5-field3', $incidentSteps['ncns-review-step5'], $globalQuestions['type_textarea'], 'Notes', null, [], [], [], 3, true, false, false);
        $this->io->progressAdvance();
        $this->io->note('For "' . $incidentSteps['ncns-review-step5']->getTitle() . '" incident Step.');
        $this->io->newLine();

        $this->io->progressFinish();
        $this->io->newLine();

        return $incidentFields;
    }

    public function insertIncidentFields($token, $step, $question, $label, $fieldId, $fieldData, $fieldRule, $fieldStyle, $ordering, $isInWeb, $isInApp, $isArchive)
    {
        $currentDateTime = new \DateTime();

        $incidentFormField = $this->em->getRepository('App\Entity\IncidentFormFields')->findOneBy(['token'=>$token,'step'=>$step->getId()]);
        if(empty($incidentFormField)){
            $incidentFormField = new IncidentFormFields();
            $incidentFormField->setToken($token);
        }
        $incidentFormField->setOrdering($ordering);
        $incidentFormField->setLabel($label);
        $incidentFormField->setFieldId($fieldId);
        $incidentFormField->setFieldData($fieldData);
        $incidentFormField->setFieldRule($fieldRule);
        $incidentFormField->setFieldStyle($fieldStyle);
        $incidentFormField->setIncidentQuestion($question);
        $incidentFormField->setCreatedDate($currentDateTime);
        $incidentFormField->setStep($step);
        $incidentFormField->setIsInWeb($isInWeb);
        $incidentFormField->setIsInApp($isInApp);
        $incidentFormField->setIsArchive($isArchive);
        $this->em->persist($incidentFormField);
        $this->em->flush();

        return $incidentFormField;
    }

}
