<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

class TempDriverRemoveCommand extends Command
{
    protected static $defaultName = 'app:temp-driver-remove';

    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $currentDate = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $this->em->getRepository('App\Entity\TempDriverRoute')->getAllTempDriverRemove();

        $io->success('Remove temp driver route...');

        return 0;
    }
}
