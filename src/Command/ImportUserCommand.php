<?php

namespace App\Command;

use App\Entity\ImportUser;
use App\Entity\EmergencyContact;
use App\Entity\User;
use App\Entity\Driver;
use App\Entity\DriverSkill;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Services\AmazonS3Service;
use Doctrine\ORM\EntityManagerInterface;
use App\Services\EmailService;
use App\Utils\GlobalUtility;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Twig\Environment;
use App\Services\BotService;

class ImportUserCommand extends Command
{
    protected static $defaultName = 'dsp:import-user';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var EmailService
     */
    protected $emailService;

    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var AmazonS3Service
     */
    protected $s3;

    /**
     * @var ContainerBagInterface
     */
    private $params;

    /**
     * @var GlobalUtility
     */
    private $globalUtils;

    /**
     * @var BotService
     */
    protected $botService;

    private $io;

    public function __construct(
        EntityManagerInterface $em,
        EmailService $emailService,
        Environment $twig,
        AmazonS3Service $s3,
        ContainerBagInterface $params,
        GlobalUtility $globalUtils,
        BotService $botService
    )
    {
        $this->em = $em;
        $this->emailService = $emailService;
        $this->twig = $twig;
        $this->s3 = $s3;
        $this->params = $params;
        $this->globalUtils = $globalUtils;
        $this->botService = $botService;
        parent::__construct();
    }

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $importUserRecords = $this->em->getRepository('App\Entity\ImportUser')->findBy(['status'=>0,'isArchive'=>false],[],2);
        $dateFormatValidation = '/^(19|20)\d\d([-.])(0[1-9]|1[012])\2(0[1-9]|[12][0-9]|3[01])$/';

        foreach ($importUserRecords as $importUserRecord) {
            $importUserRecord->setStatus(1);
            $this->em->persist($importUserRecord);
            $this->em->flush($importUserRecord);
        }
        foreach ($importUserRecords as $importUserRecord) {

            $company = $importUserRecord->getCompanyId();
            $email = $importUserRecord->getEmail();
            $userId = $importUserRecord->getUserId();
            $parentUser = $this->em->getRepository('App\Entity\User')->find($userId);
            $fileName = $importUserRecord->getFilePath();
            $sendInvitation = $importUserRecord->getSendInvitation();

            $newDrivers = [];
            $log = '';
            if ($fileName != '') {
                $file = fopen($fileName, "r");
                $firstRow = true;
                $log = '';
                $cnt = $editCount = $addCount = 0;
                while (($column = fgetcsv($file, 10000, ",")) !== false) {
                    $validate = true;
                    if ($cnt == 0) {
                        if (trim($column[0]) != 'Firstname*') {
                            $log = 'csv column Firstname not there';
                            break;
                        } elseif (trim($column[1]) != 'Middle_Name') {
                            $log = 'csv column Middle_Name not there';
                            break;
                        } elseif (trim($column[2]) != 'Lastname*') {
                            $log = 'csv column Lastname not there';
                            break;
                        } elseif (trim($column[3]) != 'Nickname') {
                            $log = 'csv column Nickname not there';
                            break;
                        } elseif (trim($column[4]) != 'Emergency_1_Contact') {
                            $log = 'csv column Emergency_1_Contact not there';
                            break;
                        } elseif (trim($column[5]) != 'Emergency_1_Phone') {
                            $log = 'csv column Emergency_1_Phone not there';
                            break;
                        } elseif (trim($column[6]) != 'Emergency_1_Relationship') {
                            $log = 'csv column Emergency_1_Relationship not there';
                            break;
                        } elseif (trim($column[7]) != 'Emergency_2_Contact') {
                            $log = 'csv column Emergency_2_Contact not there';
                            break;
                        } elseif (trim($column[8]) != 'Emergency_2_Phone') {
                            $log = 'csv column Emergency_2_Phone not there';
                            break;
                        } elseif (trim($column[9]) != 'Emergency_2_Relationship') {
                            $log = 'csv column Emergency_2_Relationship not there';
                            break;
                        } elseif (trim($column[10]) != 'Emergency_3_Contact') {
                            $log = 'csv column Emergency_3_Contact not there';
                            break;
                        } elseif (trim($column[11]) != 'Emergency_3_Phone') {
                            $log = 'csv column Emergency_3_Phone not there';
                            break;
                        } elseif (trim($column[12]) != 'Emergency_3_Relationship') {
                            $log = 'csv column Emergency_3_Relationship not there';
                            break;
                        } elseif (trim($column[13]) != 'Delivery_Station_Code_Code*') {
                            $log = 'csv column Delivery_Station_Code_Code not there';
                            break;
                        } elseif (trim($column[14]) != 'Gender*') {
                            $log = 'csv column Gender not there';
                            break;
                        } elseif (trim($column[15]) != 'Street*') {
                            $log = 'csv column Street not there';
                            break;
                        } elseif (trim($column[16]) != 'City') {
                            $log = 'csv column City not there';
                            break;
                        } elseif (trim($column[17]) != 'State') {
                            $log = 'csv column State not there';
                            break;
                        } elseif (trim($column[18]) != 'Zipcode') {
                            $log = 'csv column Zipcode not there';
                            break;
                        } elseif (trim($column[19]) != 'Primary_Phone*') {
                            $log = 'csv column Primary_Phone not there';
                            break;
                        } elseif (trim($column[20]) != 'Act._Marital_Status') {
                            $log = 'csv column Act._Marital_Status not there';
                            break;
                        } elseif (trim($column[21]) != 'Personal_Email*') {
                            $log = 'csv column Personal_Email not there';
                            break;
                        } elseif (trim($column[22]) != 'Hire_Date_(Y-M-D)*') {
                            $log = 'csv column Hire_Date not there';
                            break;
                        } elseif (trim($column[23]) != 'Termination_Date') {
                            $log = 'csv column Termination_Date not there';
                            break;
                        } elseif (trim($column[24]) != 'Rehire_Date_(Y-M-D)') {
                            $log = 'csv column Rehire_Date not there';
                            break;
                        } elseif (trim($column[25]) != 'PantSize') {
                            $log = 'csv column PantSize not there';
                            break;
                        } elseif (trim($column[26]) != 'ShoeSize') {
                            $log = 'csv column ShoeSize not there';
                            break;
                        } elseif (trim($column[27]) != 'T-ShirtSize') {
                            $log = 'csv column T-ShirtSize not there';
                            break;
                        } elseif (trim($column[28]) != 'DLState') {
                            $log = 'csv column DLState not there';
                            break;
                        } elseif (trim($column[29]) != 'DriversLicense#') {
                            $log = 'csv column DriversLicense# not there';
                            break;
                        } elseif (trim($column[30]) != 'VeteranStatus') {
                            $log = 'csv column VeteranStatus not there';
                            break;
                        } elseif (trim($column[31]) != 'Employee_Status*') {
                            $log = 'csv column Employee_Status not there';
                            break;
                        } elseif (trim($column[32]) != 'Supervisor_Primary*') {
                            $log = 'csv column Supervisor_Primary not there';
                            break;
                        } elseif (trim($column[33]) != 'Work_Email') {
                            $log = 'csv column Work_Email not there';
                            break;
                        } elseif (trim($column[34]) != 'Birth_Date_(Y-M-D)*') {
                            $log = 'csv column Birth_Date_(MM/DD/YYYY) not there';
                            break;
                        } elseif (trim($column[35]) != 'DLExpiration_(Y-M-D)') {
                            $log = 'csv column DLExpiration not there';
                            break;
                        } elseif (trim($column[36]) != 'Employee_Code') {
                            $log = 'csv column Employee_Code not there';
                            break;
                        } elseif (trim($column[37]) != 'SS_Number*') {
                            $log = 'csv column SS_Number not there';
                            break;
                        } elseif (trim($column[38]) != 'Employment_Status*') {
                            $log = 'csv column Employment_Status not there';
                            break;
                        } elseif (trim($column[39]) != 'Role*') {
                            $log = 'csv column Role not there';
                            break;
                        } elseif (trim($column[40]) != 'Transporter_ID') {
                            $log = 'csv column Transporter_ID not there';
                            break;
                        } elseif (trim($column[41]) != 'Skill*') {
                            $log = 'csv column Skill not there';
                            break;
                        } elseif (trim($column[42]) != 'Rate') {
                            $log = 'csv column Rate not there';
                            break;
                        }
                    }
                    if ($cnt > 0) {

                        $userStatus = false;
                        $tempLog = '';
                        if (strlen(trim(trim($column[39]))) > 0) {
                            $roleName = '';
                            if (trim($column[39]) === 'Delivery Associate') {
                                $roleName = 'ROLE_DELIVERY_ASSOCIATE';
                            } else if (trim($column[39]) == 'Station Manager') {
                                $roleName = 'ROLE_STATION_MANAGER';
                            } else if (trim($column[39]) == 'Operations Manager') {
                                $roleName = 'ROLE_OPERATIONS_MANAGER';
                            } else if (trim($column[39]) == 'Lead Driver') {
                                $roleName = 'ROLE_LEAD_DRIVER';
                            } else if (trim($column[39]) == 'Assistant Station Manager') {
                                $roleName = 'ROLE_ASSISTANT_STATION_MANAGER';
                            } else if (trim($column[39]) == 'Recruiter') {
                                $roleName = 'ROLE_RECRUITER';
                            } else if (trim($column[39]) == 'Dispatcher') {
                                $roleName = 'ROLE_DISPATCH';
                            } else if (trim($column[39]) == 'Administrative Assistant') {
                                $roleName = 'ROLE_ADMINISTRATIVE_ASSISTANT';
                            } else if (trim($column[39]) == 'Safety Champion') {
                                $roleName = 'ROLE_SAFETY_CHAMP';
                            } else if (trim($column[39]) == 'Benefits Specialist') {
                                $roleName = 'ROLE_SAFETY_CHAMP';
                            } else {
                                $roleName = 'ROLE_DELIVERY_ASSOCIATE';
                            }

                            $role = $this->em->getRepository('App\Entity\Role')->findOneBy(['company' => $company, 'name' => $roleName, 'isArchive' => false]);
                            if (!$role) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please fill the correct Role';
                                } else {
                                    $tempLog = $tempLog . 'please fill the correct Role';
                                }
                                $validate = false;
                            }
                        } else {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Role';
                            } else {
                                $tempLog = $tempLog . 'please fill the Role';
                            }
                            $validate = false;
                        }
                        // check validation
                        if (trim($column[0]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the First Name';
                            } else {
                                $tempLog = $tempLog . 'please fill the First Name';
                            }
                            $validate = false;
                        }
                        if (trim($column[2]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Last Name';
                            } else {
                                $tempLog = $tempLog . 'please fill the Last Name';
                            }
                            $validate = false;
                        }
                        if (trim($column[21]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Email Id';
                            } else {
                                $tempLog = $tempLog . 'please fill the Email Id';
                            }
                            $validate = false;
                        } elseif (!preg_match("/^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/", trim($column[21]))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill valid Email Id';
                            } else {
                                $tempLog = $tempLog . 'please fill valid Email Id';
                            }
                            $validate = false;
                        }
                        $birthDate = null;
                        if ($column[34] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Birthdate';
                            } else {
                                $tempLog = $tempLog . 'please fill the Birthdate';
                            }
                            $validate = false;
                        } else {

                            if (!preg_match($dateFormatValidation, $column[34])) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please fill the Birthdate Format validation';
                                } else {
                                    $tempLog = $tempLog . 'please fill the Birthdate Format validation';
                                }
                                $validate = false;
                            }else {
                                $birthDate = strlen(trim($column[34])) > 0 ? new \DateTime(trim($column[34])) : null;
                                if(!is_a($birthDate, 'DateTime')){
                                    if ($tempLog != '') {
                                        $tempLog = $tempLog . ', please fill the Birthdate Format validation';
                                    } else {
                                        $tempLog = $tempLog . 'please fill the Birthdate Format validation';
                                    }
                                    $validate = false;
                                }
                            }
                        }
                        $hireDate = null;
                        if ($column[22] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Hire_Date';
                            } else {
                                $tempLog = $tempLog . 'please fill the Hire_Date';
                            }
                            $validate = false;
                        } else {
                            if (!preg_match($dateFormatValidation, $column[22])) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please fill the Hire_Date Format validation';
                                } else {
                                    $tempLog = $tempLog . 'please fill the Hire_Date Format validation';
                                }
                                $validate = false;
                            } else {
                                $hireDate = strlen(trim($column[22])) > 0 ? new \DateTime(trim($column[22])) : null;
                                if(!is_a($hireDate, 'DateTime')) {
                                    if ($tempLog != '') {
                                        $tempLog = $tempLog . ', please fill the Hire_Date Format validation';
                                    } else {
                                        $tempLog = $tempLog . 'please fill the Hire_Date Format validation';
                                    }
                                    $validate = false;
                                }
                            }
                        }
                        $DriversLicenseNumberExpiration = null;
                        if ($column[35] != '') {
                            if (!preg_match($dateFormatValidation, $column[35])) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please fill the DLExpiration Format validation';
                                } else {
                                    $tempLog = $tempLog . 'please fill the DLExpiration Format validation';
                                }
                                $validate = false;
                            } else {
                                $DriversLicenseNumberExpiration = strlen(trim($column[35])) > 0 ? new \DateTime(trim($column[35])) : null;
                                if (!is_a($DriversLicenseNumberExpiration, 'DateTime')) {
                                    if ($tempLog != '') {
                                        $tempLog = $tempLog . ', please fill the DLExpiration Format validation';
                                    } else {
                                        $tempLog = $tempLog . 'please fill the DLExpiration Format validation';
                                    }
                                    $validate = false;
                                }
                            }
                        }
                        if ($column[37] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the SS_Number';
                            } else {
                                $tempLog = $tempLog . 'please fill the SS_Number';
                            }
                            $validate = false;
                        }
                        if ($column[19] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Primary_Phone';
                            } else {
                                $tempLog = $tempLog . 'please fill the Primary_Phone';
                            }
                            $validate = false;
                        }

                        if (!in_array(trim($column[20]), array('Domestic Partnership','Single', 'Married', 'Divorced', 'Widowed', ''))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Act._Marital_Status with options like (Single, Married, Divorced, Widowed, Domestic Partnership)';
                            } else {
                                $tempLog = $tempLog . 'please fill the Act._Marital_Status with options like (Single, Married, Divorced, Widowed, Domestic Partnership)';
                            }
                            $validate = false;
                        }elseif(trim($column[20]) == 'Domestic Partnership' ){
                            $column[20] = 'domestic-partnership';
                        }
                        if ($column[14] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Gender';
                            } else {
                                $tempLog = $tempLog . 'please fill the Gender';
                            }
                            $validate = false;
                        } elseif (!in_array(strtolower(trim($column[14])), array('male', 'female', 'non-binary', 'do not wish to disclose'))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Gender with options like (Male, Female, Non-binary, Do not wish to disclose)';
                            } else {
                                $tempLog = $tempLog . 'please fill the Gender with options like (Male, Female, Non-binary, Do not wish to disclose)';
                            }
                            $validate = false;
                        }
                        if (!in_array(strtolower(trim($column[30])), array('y', 'n', ''))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the VeteranStatus with options like (y, n)';
                            } else {
                                $tempLog = $tempLog . 'please fill the VeteranStatus with options like (y, n)';
                            }
                            $validate = false;
                        }
                        if (trim($column[31]) == '' || (trim($column[31]) != '' && !in_array(strtolower(trim($column[31])), array('active', 'disabled', 'archived')))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Employee_Status with options like (active, disabled, archived)';
                            } else {
                                $tempLog = $tempLog . 'please fill the Employee_Status with options like (active, disabled, archived)';
                            }
                            $validate = false;
                        } else {
                            if (strtolower(trim($column[31])) == 'active') {
                                $column[31] = 0;
                            } elseif (strtolower(trim($column[31])) == 'disabled') {
                                $column[31] = 1;
                            } elseif (strtolower(trim($column[31])) == 'archive') {
                                $column[31] = 2;
                            }
                        }
                        if (trim($column[38]) == '' || !in_array(strtolower(trim($column[38])), array('exempt', 'non-exempt'))) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Employment_Status with options like (exempt, non-exempt)';
                            } else {
                                $tempLog = $tempLog . 'please fill the Employment_Status with options like (exempt, non-exempt)';
                            }
                            $validate = false;
                        }
                        if (trim($column[15]) == '' && trim($column[16]) == '' && trim($column[17]) == '' && trim($column[18]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Street';
                            } else {
                                $tempLog = $tempLog . 'please fill the Street';
                            }
                            $validate = false;
                        }
                        $station = null;
                        if (trim($column[13]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Station';
                            } else {
                                $tempLog = $tempLog . 'please fill the Station';
                            }
                            $validate = false;
                        } else {
                            $station = $this->em->getRepository('App\Entity\Station')->findOneBy(['company' => $company, 'code' => trim($column[13]), 'isArchive' => false]);
                            if (!$station) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please enter correct the Station';
                                } else {
                                    $tempLog = $tempLog . 'please enter correct the Station';
                                }
                                $validate = false;
                            }
                        }

                        $assignManager = null;

                        if (trim($column[32]) == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the Supervisor_Primary';
                            } else {
                                $tempLog = $tempLog . 'please fill the Supervisor_Primary';
                            }
                            $validate = false;
                        } else {
                            $superVisorArr = explode(',', $column[32]);

                            if (isset($superVisorArr[0]) && isset($superVisorArr[1])) {
                                if (!$station) {
                                    if ($tempLog != '') {
                                        $tempLog = $tempLog . ', please first fill the Station';
                                    } else {
                                        $tempLog = $tempLog . 'please first fill the Station';
                                    }
                                    $validate = false;
                                } else {
                                    $assignManager = $this->em->getRepository('App\Entity\User')->findManagers(['company' => (int)$company, 'firstName' => trim(explode(' ', ltrim($superVisorArr[1]))[0]), 'lastName' => trim($superVisorArr[0])]);

                                    if (empty($assignManager)) {
                                        if ($tempLog != '') {
                                            $tempLog = $tempLog . ', please enter correct the Supervisor_Primary';
                                        } else {
                                            $tempLog = $tempLog . 'please enter correct the Supervisor_Primary';
                                        }
                                        $validate = false;
                                    }
                                }
                            } else {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', please enter correct the Supervisor_Primary';
                                } else {
                                    $tempLog = $tempLog . 'please enter correct the Supervisor_Primary';
                                }
                                $validate = false;
                            }
                        }

                        if ($column[41] == '') {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please fill the skill';
                            } else {
                                $tempLog = $tempLog . 'please fill the skill';
                            }
                            $validate = false;
                        }
                        $user = $this->em->getRepository('App\Entity\User')->findOneBy(['email' => trim($column[21])]);
                        if ($user) {
                            if($user->getCompany()->getId() != $company){

                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', user already exist in other company';
                                } else {
                                    $tempLog = $tempLog . 'user already exist in other company';
                                }
                                $validate = false;
                            }
                        }
                        if ($tempLog != '') {
                            $log = $log . '(' . $cnt . ')' . implode(" | ", $column) . '
                            Error - ' . $tempLog . '

                            ';
                        }
                        if ($validate == true) {
                            if (!$user && trim($column[21]) != '') {
                                if ($importUserRecord->getPassword()) {
                                    $password = $importUserRecord->getPassword();
                                } else {
                                    $password = $this->globalUtils->plainPassword();
                                }
                                $newUser = new User();
                                $newUser->setEmail(strtolower(trim($column[21])));
                                $newUser->setPassword($password);
                                $newUser->setFirstName(trim($column[0]));
                                $newUser->setLastName(trim($column[2]));
                                $newUser->setMiddleName(trim($column[1]));
                                if (trim($column[3]) != '') {
                                    $newUser->setFriendlyName(trim($column[3]) . ' ' . trim($column[2]));
                                } else {
                                    $newUser->setFriendlyName(trim($column[0]) . ' ' . trim($column[2]));
                                }
                                $companyObj = $this->em->getRepository('App\Entity\Company')->find((int)$company);
                                $newUser->setCompany($companyObj);
                                $newUser->setIsEnabled(true);
                                $newUser->setIsArchive($column[31]);
                                $newUser->addUserRole($role);
                                $this->em->persist($newUser);
                                $this->em->flush();

                                $userStatus = true;
                                $user = $newUser;
                                if ($sendInvitation == true) {
                                    $content = $this->twig->render('emails/employee-create.twig', ['company' => $companyObj, 'name' => trim($column[0]) . ' ' . trim($column[2]), 'password' => $password]);
                                    $this->emailService->sendEmail(strtolower(trim($column[21])), 'Your Employee Account Created', $content);
                                }
                            } elseif ($user) {
                                $user->setFirstName(trim($column[0]));
                                $user->setLastName(trim($column[2]));
                                $user->setMiddleName(trim($column[1]));
                                $user->setEmail(strtolower(trim($column[21])));
                                if (trim($column[3]) != '') {
                                    $user->setFriendlyName(trim($column[3]) . ' ' . trim($column[2]));
                                } else {
                                    $user->setFriendlyName(trim($column[0]) . ' ' . trim($column[2]));
                                }
                                $user->setIsArchive($column[31]);
                                $this->em->persist($user);
                                $this->em->flush();
                                $userStatus = false;
                            }

                            if ($userStatus == true) {
                                // $superVisorArr = explode(',',$column[32]);
                                // $assignManager = $this->em->getRepository('App\Entity\User')->findOneBy(['firstName' => trim(explode(' ',ltrim($superVisorArr[1]))[0]),'lastName' => trim($superVisorArr[0])]);
                                $driver = new Driver();
                                $driver->setJobTitle(trim($column[39]));
                                $driver->setSsn(sprintf("%04s", substr(trim($column[37]), -4)));
                                $employeementStatus = $this->em->getRepository('App\Entity\EmploymentStatus')->findOneBy(['companyId' => $company, 'name' => strtolower(trim($column[38])), 'isArchive' => false]);
                                if (!empty($employeementStatus)) {
                                    $driver->setEmploymentStatus($employeementStatus);
                                }
                                $driver->setPersonalPhone(trim($column[19]));
                                if (strtolower(trim($column[20]) == '')) {
                                    $driver->setMaritalStatus(NULL);
                                } else {
                                    $driver->setMaritalStatus(strtolower(trim($column[20])));
                                }
                                $driver->setGender(strtolower(trim($column[14])));

                                if (strtolower(trim($column[30])) == 'n') {
                                    $column[30] = 0;
                                } elseif (strtolower(trim($column[30])) == 'y') {
                                    $column[30] = 1;
                                } else {
                                    $column[30] = NULL;
                                }
                                $driver->setVeteranStatus($column[30]);
                                $driver->setSecondaryEmail(strtolower(trim($column[33])));
                                $driver->setFullAddress(trim($column[15]) . ' ' . trim($column[16]) . ' ' . trim($column[17]) . ' ' . trim($column[18]));
                                $driver->setNote('');

                                if (strlen(trim($column[27])) > 0) {
                                    $shirtSize = trim($column[27]);
                                } else {
                                    $shirtSize = 'M - 2XL';
                                }
                                $driver->setShirtSize(trim($shirtSize));
                                if (strlen(trim($column[26])) > 0) {
                                    $shoeSize = trim($column[26]);
                                } else {
                                    $shoeSize = 'M - US 11/EU 44';
                                }
                                $driver->setShoeSize($shoeSize);
                                if (strlen(trim($column[25])) > 0) {
                                    $pentSize = trim($column[25]);
                                } else {
                                    $pentSize = 'M - 2XL';
                                }
                                $driver->setPantSize($pentSize);
                                $driver->setTransporterID(trim($column[40]));
                                $driver->setDriversLicenseNumber(trim($column[29]));
                                $driver->setLicenseState(trim($column[28]));
                                $driver->setPaycomId(sprintf("%04s", trim($column[36])));
                                $driver->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
//                                $driver->setUser($user);
                                $driver->addStation($station);
                                $driver->addAssignedManager($assignManager);

                                $driver->setIsArchive($column[31]);
                                if (is_a($birthDate, 'DateTime')) {
                                    $driver->setBirthday($birthDate);
                                }
                                if (is_a($hireDate, 'DateTime')) {
                                    $driver->setHireDate($hireDate);
                                }
                                if (is_a($DriversLicenseNumberExpiration, 'DateTime')) {
                                    $driver->setDriversLicenseNumberExpiration($DriversLicenseNumberExpiration);
                                }
                                $driver->setUser($user);
                                $this->em->persist($driver);
                                $this->em->flush();
                                $user->setDriver($driver);
                                $this->em->flush();
                                $newDrivers[] = $driver;

                                if ($driver) {
                                    if (strlen(trim($column[4])) > 0 && strlen(trim($column[5])) > 0) {
                                        $emergencyContact = new EmergencyContact();
                                        $emergencyContact->setDriver($driver);
                                        $emergencyContact->setName(trim($column[4]));
                                        $emergencyContact->setNumber(trim($column[5]));
                                        $emergencyContact->setRelation(trim($column[6]));
                                        $this->em->persist($emergencyContact);
                                        $this->em->flush();
                                    }
                                    if (strlen(trim($column[7])) > 0 && strlen(trim($column[8])) > 0) {
                                        $emergencyContact = new EmergencyContact();
                                        $emergencyContact->setDriver($driver);
                                        $emergencyContact->setName(trim($column[7]));
                                        $emergencyContact->setNumber(trim($column[8]));
                                        $emergencyContact->setRelation(trim($column[9]));
                                        $this->em->persist($emergencyContact);
                                        $this->em->flush();
                                    }
                                    if (strlen(trim($column[10])) > 0 && strlen(trim($column[11])) > 0) {
                                        $emergencyContact = new EmergencyContact();
                                        $emergencyContact->setDriver($driver);
                                        $emergencyContact->setName(trim($column[10]));
                                        $emergencyContact->setNumber(trim($column[11]));
                                        $emergencyContact->setRelation(trim($column[12]));
                                        $this->em->persist($emergencyContact);
                                        $this->em->flush();
                                    }

                                    $skill = new DriverSkill();
                                    $skill->setDriver($driver);
                                    $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => $column[41], 'isArchive' => false]);
                                    if (!empty($companySkill)) {
                                        $skill->setSkill($companySkill);
                                        $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                        if (!empty($skillRate)) {
                                            $skill->setSkillRate($skillRate);
                                        }
                                        if ($column[38] == 'non-exempt') {
                                            if ($column[42] != '' && $column[42] != 0) {
                                                $skill->setHourlyRate($column[42]);
                                            } else {
                                                if (!empty($skillRate)) {
                                                    $skill->setHourlyRate($skillRate->getDefaultRate());
                                                }else{
                                                    $skill->setHourlyRate(0);
                                                }
                                            }
                                        } else {
                                            if (!empty($skillRate)) {
                                               $skill->setHourlyRate($skillRate->getDefaultRate());
                                            }else{
                                                $skill->setHourlyRate(0);
                                            }
                                        }
                                    } else {
                                        $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => 'Driver', 'isArchive' => false]);
                                        $skill->setSkill($companySkill);
                                        $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                        if (!empty($skillRate)) {
                                            $skill->setSkillRate($skillRate);
                                        }
                                        if ($column[38] == 'non-exempt') {
                                            if ($column[42] != '' && $column[42] != 0) {
                                                $skill->setHourlyRate($column[42]);
                                            } else {
                                                if (!empty($skillRate)) {
                                                    $skill->setHourlyRate($skillRate->getDefaultRate());
                                                }else{
                                                    $skill->setHourlyRate(0);
                                                }
                                            }
                                        } else {
                                            if (!empty($skillRate)) {
                                                $skill->setHourlyRate($skillRate->getDefaultRate());
                                            }else{
                                                $skill->setHourlyRate(0);

                                            }
                                        }
                                    }
                                    $this->em->persist($skill);
                                    $this->em->flush();
                                    $addCount++;
                                }
                            } elseif ($userStatus == false) {
                                if ($user) {
                                    $driverObj = $user->getDriver();
                                    if ($driverObj) {
                                        // $superVisorArr = explode(',',$column[32]);
                                        // $assignManager = $this->em->getRepository('App\Entity\User')->findOneBy(['firstName' => trim(explode(' ',ltrim($superVisorArr[1]))[0]),'lastName' => trim($superVisorArr[0])]);
                                        $oldAssignManager = $driverObj->getAssignedManagers()->first();
                                        $driverObj->removeAssignedManager($oldAssignManager);
                                        $driverObj->addAssignedManager($assignManager);

                                        $oldStation = $driverObj->getStations()->first();
                                        $driverObj->removeStation($oldStation);
                                        $driverObj->addStation($station);

                                        $this->em->persist($driverObj);
                                        $this->em->flush();

                                        $user->setDriver($driverObj);
                                        $this->em->flush();
                                        $newDrivers[] = $driverObj;

                                        $qb = $this->em->createQueryBuilder();
                                        $qb->update('App\Entity\Driver', 'd');
                                        if (is_a($birthDate, 'DateTime')) {
                                            $qb->set('d.birthday', ':birthDate')
                                                ->setParameter('birthDate', $birthDate);
                                        }
                                        if (is_a($hireDate, 'DateTime')) {
                                            $qb->set('d.hireDate', ':hireDate')
                                                ->setParameter('hireDate', $hireDate);
                                        }
                                        if (is_a($DriversLicenseNumberExpiration, 'DateTime')) {
                                            $qb->set('d.driversLicenseNumberExpiration', ':driversLicenseNumberExpiration')
                                                ->setParameter('driversLicenseNumberExpiration', $DriversLicenseNumberExpiration);
                                        }
                                        if (strtolower(trim($column[30])) == 'n') {
                                            $column[30] = 0;
                                        } elseif (strtolower(trim($column[30])) == 'y') {
                                            $column[30] = 1;
                                        } else {
                                            $column[30] = NULL;
                                        }
                                        $maritalStatus = NULL;
                                        if (strtolower(trim($column[20]) == '')) {
                                            $maritalStatus = NULL;
                                        } else {
                                            $maritalStatus = strtolower(trim($column[20]));
                                        }

                                        $result = $qb->set('d.jobTitle', ':jobTitle')
                                            ->setParameter('jobTitle', trim($column[39]))
                                            ->set('d.ssn', ':ssn')
                                            ->setParameter('ssn', sprintf("%04s", substr(trim($column[37]), -4)))
                                            ->set('d.personalPhone', ':personalPhone')
                                            ->setParameter('personalPhone', trim($column[19]))
                                            ->set('d.maritalStatus', ':maritalStatus')
                                            ->setParameter('maritalStatus', $maritalStatus)
                                            ->set('d.gender', ':gender')
                                            ->setParameter('gender', strtolower(trim($column[14])))
                                            ->set('d.veteranStatus', ':veteranStatus')
                                            ->setParameter('veteranStatus', $column[30])
                                            ->set('d.secondaryEmail', ':secondaryEmail')
                                            ->setParameter('secondaryEmail', strtolower(trim($column[33])))
                                            ->set('d.fullAddress', ':fullAddress')
                                            ->setParameter('fullAddress', trim($column[15]) . ' ' . trim($column[16]) . ' ' . trim($column[17]) . ' ' . trim($column[18]))
                                            ->set('d.shirtSize', ':shirtSize')
                                            ->setParameter('shirtSize', strlen(trim($column[27])) > 0 ? trim($column[27]) : 'M - 2XL')
                                            ->set('d.shoeSize', ':shoeSize')
                                            ->setParameter('shoeSize', strlen(trim($column[26])) > 0 ? trim($column[26]) : 'M - US 11/EU 44')
                                            ->set('d.pantSize', ':pantSize')
                                            ->setParameter('pantSize', strlen(trim($column[25])) > 0 ? trim($column[25]) : 'M - 2XL')
                                            ->set('d.transporterID', ':transporterID')
                                            ->setParameter('transporterID', trim($column[40]))
                                            ->set('d.driversLicenseNumber', ':driversLicenseNumber')
                                            ->setParameter('driversLicenseNumber', trim($column[29]))
                                            ->set('d.licenseState', ':licenseState')
                                            ->setParameter('licenseState', trim($column[28]))
                                            ->set('d.paycomId', ':paycomId')
                                            ->setParameter('paycomId', sprintf("%04s", trim($column[36])))
                                            ->set('d.isArchive', ':IsArchive')
                                            ->setParameter('IsArchive', $column[31])
                                            ->where('d.id = :id')
                                            ->setParameter('id', $driverObj)
                                            ->getQuery()
                                            ->execute();

                                        if (!empty($driverObj->getEmergencyContacts())) {
                                            $qb = $this->em->createQueryBuilder();
                                            $qb->delete('App\Entity\EmergencyContact', 'e')
                                                ->where('e.driver = :driverId')
                                                ->setParameter('driverId', $driverObj->getId())
                                                ->getQuery()
                                                ->execute();
                                        }
                                        if (strlen(trim($column[4])) > 0 && strlen(trim($column[5])) > 0) {
                                            $emergencyContact = new EmergencyContact();
                                            $emergencyContact->setDriver($driverObj);
                                            $emergencyContact->setName(trim($column[4]));
                                            $emergencyContact->setNumber(trim($column[5]));
                                            $emergencyContact->setRelation(trim($column[6]));
                                            $this->em->persist($emergencyContact);
                                            $this->em->flush();
                                        }
                                        if (strlen(trim($column[7])) > 0 && strlen(trim($column[8])) > 0) {
                                            $emergencyContact = new EmergencyContact();
                                            $emergencyContact->setDriver($driverObj);
                                            $emergencyContact->setName(trim($column[7]));
                                            $emergencyContact->setNumber(trim($column[8]));
                                            $emergencyContact->setRelation(trim($column[9]));
                                            $this->em->persist($emergencyContact);
                                            $this->em->flush();
                                        }
                                        if (strlen(trim($column[10])) > 0 && strlen(trim($column[11])) > 0) {
                                            $emergencyContact = new EmergencyContact();
                                            $emergencyContact->setDriver($driverObj);
                                            $emergencyContact->setName(trim($column[10]));
                                            $emergencyContact->setNumber(trim($column[11]));
                                            $emergencyContact->setRelation(trim($column[12]));
                                            $this->em->persist($emergencyContact);
                                            $this->em->flush();
                                        }
                                        $oldSkills = $driverObj->getDriverSkills();
                                        if ($oldSkills) {
                                            $qb = $this->em->createQueryBuilder();
                                            $qb->delete('App\Entity\DriverSkill', 'ds')
                                                ->where('ds.driver = :driverId')
                                                ->setParameter('driverId', $driverObj->getId())
                                                ->getQuery()
                                                ->execute();
                                        }

                                        $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => $column[41], 'isArchive' => false]);
                                        if (!empty($companySkill)) {
                                            $skill = new DriverSkill();
                                            $skill->setDriver($driverObj);
                                            $skill->setSkill($companySkill);
                                            $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                            if(!empty($skillRate))
                                               $skill->setSkillRate($skillRate);
                                            if ($column[38] == 'non-exempt') {
                                                if ($column[42] != '' && $column[42] != 0) {
                                                    $skill->setHourlyRate($column[42]);
                                                } else {
                                                    if(!empty($skillRate)){
                                                        $skill->setHourlyRate($skillRate->getDefaultRate());
                                                    }else{
                                                        $skill->setHourlyRate(0);
                                                    }
                                                }
                                            } else {
                                                if(!empty($skillRate))
                                                    $skill->setHourlyRate(0);
                                            }
                                        } else {
                                            $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => 'Driver', 'isArchive' => false]);
                                            $skill = new DriverSkill();
                                            $skill->setDriver($driverObj);
                                            $skill->setSkill($companySkill);
                                            $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                            if(!empty($skillRate))
                                                $skill->setSkillRate($skillRate);
                                            if ($column[38] == 'non-exempt') {
                                                if ($column[42] != '' && $column[42] != 0) {
                                                    $skill->setHourlyRate($column[42]);
                                                } else {
                                                    if(!empty($skillRate)){
                                                        $skill->setHourlyRate($skillRate->getDefaultRate());
                                                    }else{
                                                        $skill->setHourlyRate(0);
                                                    }
                                                }
                                            } else {
                                                if(!empty($skillRate)){
                                                    $skill->setHourlyRate($skillRate->getDefaultRate());
                                                }else{
                                                    $skill->setHourlyRate(0);
                                                }
                                            }
                                        }
                                        $this->em->persist($skill);
                                        $this->em->flush();
                                        if ($result > 0) {
                                            $editCount++;
                                        }
                                    } else {
                                      $driver = new Driver();
                                      $driver->setJobTitle(trim($column[39]));
                                      $driver->setSsn(sprintf("%04s", substr(trim($column[37]), -4)));
                                      $employeementStatus = $this->em->getRepository('App\Entity\EmploymentStatus')->findOneBy(['companyId' => $company, 'name' => strtolower(trim($column[38])), 'isArchive' => false]);
                                      if (!empty($employeementStatus)) {
                                          $driver->setEmploymentStatus($employeementStatus);
                                      }
                                      $driver->setPersonalPhone(trim($column[19]));
                                      if (strtolower(trim($column[20]) == '')) {
                                          $driver->setMaritalStatus(NULL);
                                      } else {
                                          $driver->setMaritalStatus(strtolower(trim($column[20])));
                                      }
                                      $driver->setGender(strtolower(trim($column[14])));

                                      if (strtolower(trim($column[30])) == 'n') {
                                          $column[30] = 0;
                                      } elseif (strtolower(trim($column[30])) == 'y') {
                                          $column[30] = 1;
                                      } else {
                                          $column[30] = NULL;
                                      }
                                      $driver->setVeteranStatus($column[30]);
                                      $driver->setSecondaryEmail(strtolower(trim($column[33])));
                                      $driver->setFullAddress(trim($column[15]) . ' ' . trim($column[16]) . ' ' . trim($column[17]) . ' ' . trim($column[18]));
                                      $driver->setNote('');

                                      if (strlen(trim($column[27])) > 0) {
                                          $shirtSize = trim($column[27]);
                                      } else {
                                          $shirtSize = 'M - 2XL';
                                      }
                                      $driver->setShirtSize(trim($shirtSize));
                                      if (strlen(trim($column[26])) > 0) {
                                          $shoeSize = trim($column[26]);
                                      } else {
                                          $shoeSize = 'M - US 11/EU 44';
                                      }
                                      $driver->setShoeSize($shoeSize);
                                      if (strlen(trim($column[25])) > 0) {
                                          $pentSize = trim($column[25]);
                                      } else {
                                          $pentSize = 'M - 2XL';
                                      }
                                      $driver->setPantSize($pentSize);
                                      $driver->setTransporterID(trim($column[40]));
                                      $driver->setDriversLicenseNumber(trim($column[29]));
                                      $driver->setLicenseState(trim($column[28]));
                                      $driver->setPaycomId(sprintf("%04s", trim($column[36])));
                                      $driver->setCompany($this->em->getRepository('App\Entity\Company')->find($company));

                                      $driver->addStation($station);
                                      $driver->addAssignedManager($assignManager);

                                      $driver->setIsArchive($column[31]);
                                      if (is_a($birthDate, 'DateTime')) {
                                          $driver->setBirthday($birthDate);
                                      }
                                      if (is_a($hireDate, 'DateTime')) {
                                          $driver->setHireDate($hireDate);
                                      }
                                      if (is_a($DriversLicenseNumberExpiration, 'DateTime')) {
                                          $driver->setDriversLicenseNumberExpiration($DriversLicenseNumberExpiration);
                                      }
                                      $driver->setUser($user);
                                      $this->em->persist($driver);
                                      $this->em->flush();
                                      $user->setDriver($driver);
                                      $this->em->flush();
                                      $newDrivers[] = $driver;
                                      if ($driver) {
                                          if (strlen(trim($column[4])) > 0 && strlen(trim($column[5])) > 0) {
                                              $emergencyContact = new EmergencyContact();
                                              $emergencyContact->setDriver($driver);
                                              $emergencyContact->setName(trim($column[4]));
                                              $emergencyContact->setNumber(trim($column[5]));
                                              $emergencyContact->setRelation(trim($column[6]));
                                              $this->em->persist($emergencyContact);
                                              $this->em->flush();
                                          }
                                          if (strlen(trim($column[7])) > 0 && strlen(trim($column[8])) > 0) {
                                              $emergencyContact = new EmergencyContact();
                                              $emergencyContact->setDriver($driver);
                                              $emergencyContact->setName(trim($column[7]));
                                              $emergencyContact->setNumber(trim($column[8]));
                                              $emergencyContact->setRelation(trim($column[9]));
                                              $this->em->persist($emergencyContact);
                                              $this->em->flush();
                                          }
                                          if (strlen(trim($column[10])) > 0 && strlen(trim($column[11])) > 0) {
                                              $emergencyContact = new EmergencyContact();
                                              $emergencyContact->setDriver($driver);
                                              $emergencyContact->setName(trim($column[10]));
                                              $emergencyContact->setNumber(trim($column[11]));
                                              $emergencyContact->setRelation(trim($column[12]));
                                              $this->em->persist($emergencyContact);
                                              $this->em->flush();
                                          }

                                          $skill = new DriverSkill();
                                          $skill->setDriver($driver);
                                          $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => $column[41], 'isArchive' => false]);
                                          if (!empty($companySkill)) {
                                              $skill->setSkill($companySkill);
                                              $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                              if (!empty($skillRate)) {
                                                  $skill->setSkillRate($skillRate);
                                              }
                                              if ($column[38] == 'non-exempt') {
                                                  if ($column[42] != '' && $column[42] != 0) {
                                                      $skill->setHourlyRate($column[42]);
                                                  } else {
                                                      if (!empty($skillRate)) {
                                                          $skill->setHourlyRate($skillRate->getDefaultRate());
                                                      }else{
                                                          $skill->setHourlyRate(0);
                                                      }
                                                  }
                                              } else {
                                                  if (!empty($skillRate)) {
                                                     $skill->setHourlyRate($skillRate->getDefaultRate());
                                                  }else{
                                                      $skill->setHourlyRate(0);
                                                  }
                                              }
                                          } else {
                                              $companySkill = $this->em->getRepository('App\Entity\Skill')->findOneBy(['company' => $company, 'name' => 'Driver', 'isArchive' => false]);
                                              $skill->setSkill($companySkill);
                                              $skillRate = $this->em->getRepository('App\Entity\SkillRate')->findOneBy(['station' => $station->getId(), 'skill' => $companySkill->getId(), 'isArchive' => false]);
                                              if (!empty($skillRate)) {
                                                  $skill->setSkillRate($skillRate);
                                              }
                                              if ($column[38] == 'non-exempt') {
                                                  if ($column[42] != '' && $column[42] != 0) {
                                                      $skill->setHourlyRate($column[42]);
                                                  } else {
                                                      if (!empty($skillRate)) {
                                                          $skill->setHourlyRate($skillRate->getDefaultRate());
                                                      }else{
                                                          $skill->setHourlyRate(0);
                                                      }
                                                  }
                                              } else {
                                                  if (!empty($skillRate)) {
                                                      $skill->setHourlyRate($skillRate->getDefaultRate());
                                                  }else{
                                                      $skill->setHourlyRate(0);

                                                  }
                                              }
                                          }
                                          $this->em->persist($skill);
                                          $this->em->flush();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $cnt++;
                }
            }

            $twilioResult = [];
            foreach ($newDrivers as $newDriver) {
                $twilioResult[] = $this->botService->driverCreationHandlerForImport($newDriver);
            }


            $message = '';
            $logS3FilePath = '';
            $uploadedFileName = $fileName;
            if($log != ''){

                // log file create
                $publicDirectory = $this->params->get('kernel.project_dir') . '/public';
                $fileName = $userId.'_'.time().'_logfile.txt';
                $logFilepath = $publicDirectory . '/excelsheet/' . $fileName;

                $logfile = fopen($logFilepath, "w") or die("Unable to open file!");
                // $logTxt = "log test";
                fwrite($logfile, $log);
                fclose($logfile);

                // log file upload
                $s3result = $this->s3->uploadFile($logFilepath, $fileName, 'dsp.data.storage.general');

                if (is_string($s3result)) {
                    throw new Exception($s3result);
                }
                $logS3FilePath = $s3result['ObjectURL'];
                unlink($logFilepath);

                $message = 'Your CSV file: ' . $uploadedFileName . ' imported successfully but had some errors. Please check the attached logfile. ';
            } else {
                $message = 'Your CSV ' . $uploadedFileName . ' imported successfully.';
            }
            $this->io->note('log file path '.$logS3FilePath);
            // send mail
            $content = $this->twig->render('emails/employee-import.twig', ['logPath' => $logS3FilePath, 'message' => $message]);
            $this->emailService->sendEmail(strtolower(trim($email)), 'dspworkplace - Employee CSV Import Completed - ' . $uploadedFileName, $content);

            // isArchive true
            $importUserRecord->setIsArchive(true);
            $this->em->persist($importUserRecord);
            $this->em->flush($importUserRecord);
        }

        $this->io->success('Import User successfully.');

        return true;
    }
}
