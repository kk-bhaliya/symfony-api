<?php

namespace App\Command;

use App\Entity\ScheduleDesign;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Services\EmailService;
use Twig\Environment;

class ScheduleRecurringCommand extends Command
{
    protected static $defaultName = 'app:schedule-recurring';

    private $em;
    private $kernel;
    private $emailService;
    private $twig;

    public function __construct(EntityManagerInterface $em, KernelInterface $kernel, EmailService $emailService, Environment $twig)
    {
        $this->em = $em;
        $this->kernel = $kernel;
        $this->emailService = $emailService;
        $this->twig = $twig;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Schedule add driver route')
            ->addArgument('scheduleId', InputArgument::OPTIONAL, 'Pass Schedule Id');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $scheduleId = $input->getArgument('scheduleId');

        if($scheduleId){
            $schedules = $this->em->getRepository('App\Entity\Schedule')->findBy(['id'=>$scheduleId]);
        } else {
            $schedules = $this->em->getRepository('App\Entity\Schedule')->getRecurringSchedule();
        }

        $scheduleMailDataArr = [];
        foreach ($schedules as $schedule) {
            $currentStartWeekAccordingSchedule = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $currentStartWeekAccordingSchedule->setISODate($schedule->getYear(), $schedule->getStartWeek());
            $currentStartDateAccordingSchedule = $currentStartWeekAccordingSchedule->format('Y-m-d');


            $scheduleMailDataArr[$schedule->getId()]['before']['startDate'] = $schedule->getStartDate()->format('Y-m-d');
            $scheduleMailDataArr[$schedule->getId()]['before']['endDate'] = $schedule->getEndDate()->format('Y-m-d');
            $scheduleMailDataArr[$schedule->getId()]['before']['startWeek'] = $schedule->getStartWeek();
            $scheduleMailDataArr[$schedule->getId()]['before']['endWeek'] = $schedule->getEndWeek();
            $scheduleMailDataArr[$schedule->getId()]['before']['scheduleLastDate'] = $schedule->getScheduleLastDate()->format('Y-m-d');

            if($currentStartDateAccordingSchedule <= (new \DateTime('now'))->format('Y-m-d')) {
                $currentTimeZone = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            } else {
                $currentTimeZone = (new \DateTime($currentStartDateAccordingSchedule))->setTimezone(new \DateTimeZone('UTC'));
            }
            if ($currentTimeZone->format('l') == 'Sunday') {
                $currentTimeZone = $currentTimeZone->modify('1 days');
            }

            $currentDate = $currentTimeZone->format('Y-m-d');
            $currentYear = $currentTimeZone->format('Y');

            $currentTimeZone = $currentTimeZone->modify('14 weeks');
            $lastWeekDate = $currentTimeZone->setISODate((int)$currentTimeZone->format('o'), (int)$currentTimeZone->format('W'), 6);
            $lastWeekDate = $lastWeekDate->format('Y-m-d');

            $currentScheduleStartWeek = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $currentScheduleStartWeek->setISODate($schedule->getYear(), $schedule->getStartWeek());
            $currentScheduleStartDate = $currentScheduleStartWeek->format('Y-m-d');
            $currentScheduleEndWeek = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $currentScheduleEndWeek->setISODate($schedule->getWeekEndYear(), $schedule->getEndWeek());
            $currentScheduleEndDate = $currentScheduleEndWeek->format('Y-m-d');

        //    dd($currentDate,$currentScheduleEndDate);
            //startweek to and week diff
             $currentWeekArray = $this->getWeekDifferenceArray($currentDate,$currentScheduleEndDate,$currentYear);
            //current to to and week diff
             $newWeekArray = $this->getWeekDifferenceArray($currentDate,$lastWeekDate,$currentYear);
             $scheduleStartEndWeek = $this->getWeekDifferenceArray($currentScheduleStartDate,$currentScheduleEndDate,$schedule->getYear());

            $oldWeeks = $scheduleStartEndWeek['weeks'];

            if((count($currentWeekArray['weeks']) != count($newWeekArray['weeks'])) && $schedule->getOriginalWeekCount() > 0){
                for ($i = count($currentWeekArray['weeks']); $i < count($newWeekArray['weeks']); $i++) {
                    $newStartWeek = (count($oldWeeks) % $schedule->getOriginalWeekCount());
                    $newStartWeek = $oldWeeks[$newStartWeek];

                    $findOldWeekAndYear = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
                    $findOldWeekAndYear->setISODate($schedule->getYear(), $newStartWeek);

                    $oldWeekNumber = $findOldWeekAndYear->format('W');
                    $oldYearNumber = $findOldWeekAndYear->format('Y');

                    $findNewWeekAndYear = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
                    $findNewWeekAndYear->setISODate($schedule->getWeekEndYear(), ((int)end($oldWeeks)+1));

                    $week = $findNewWeekAndYear->format('W');
                    $year = $findNewWeekAndYear->format('Y');

                    $scheduleDesigns = $this->em->getRepository('App\Entity\ScheduleDesign')->findBy(['schedule' => $schedule->getId(), 'week' => $oldWeekNumber, 'year' => $oldYearNumber, 'isArchive' => 0]);
                    $checkExistScheduleDesign = $this->em->getRepository('App\Entity\ScheduleDesign')->findBy(['schedule' => $schedule->getId(), 'week' => $week, 'year' => $year, 'isArchive' => 0]);

                   if(!$checkExistScheduleDesign && $scheduleDesigns ){
                        foreach ($scheduleDesigns as $scheduleDesign) {
                          $sd = new ScheduleDesign();
                          $sd->setSchedule($scheduleDesign->getSchedule());
                          $sd->setWeek($week);
                          $sd->setSunday($scheduleDesign->getSunday());
                          $sd->setMonday($scheduleDesign->getMonday());
                          $sd->setTuesday($scheduleDesign->getTuesday());
                          $sd->setWednesday($scheduleDesign->getWednesday());
                          $sd->setThursday($scheduleDesign->getThursday());
                          $sd->setFriday($scheduleDesign->getFriday());
                          $sd->setSaturday($scheduleDesign->getSaturday());
                          $sd->setOrdering($scheduleDesign->getOrdering());
                          $sd->setYear($year);
                          $this->em->persist($sd);
                          $this->em->flush();
                          if ($scheduleDesign->getShifts()) {
                              foreach ($scheduleDesign->getShifts() as $shift) {
                                  $shift->addScheduleDesign($sd);
                                  $this->em->persist($shift);
                                  $this->em->flush();
                              }
                          }
                      }
                    }
                    array_shift($oldWeeks);
                    $lastWeekNumber = (int) end($oldWeeks);
                    array_push($oldWeeks,(string)($lastWeekNumber+1));
                }
                 $newEndDateOfSchedule = $this->getLastWeekDate($year, $week);
                 $newLastDateOfSchedule = $this->getLastWeekDate($year, $week)->modify('-1 days');
                 $schedule->setStartWeek($currentWeekArray['weeks'][0]);
                 $schedule->setEndWeek($week);
                 $schedule->setYear($currentWeekArray['years'][0]);
                 $schedule->setWeekEndYear($year);
                 $schedule->setScheduleLastDate($newLastDateOfSchedule);
                 $schedule->setEndDate($newEndDateOfSchedule);
                 $this->em->persist($schedule);
                 $this->em->flush();
            }
            $scheduleMailDataArr[$schedule->getId()]['after']['startDate'] = $schedule->getStartDate()->format('Y-m-d');
            $scheduleMailDataArr[$schedule->getId()]['after']['endDate'] = $schedule->getEndDate()->format('Y-m-d');
            $scheduleMailDataArr[$schedule->getId()]['after']['startWeek'] = $schedule->getStartWeek();
            $scheduleMailDataArr[$schedule->getId()]['after']['endWeek'] = $schedule->getEndWeek();
            $scheduleMailDataArr[$schedule->getId()]['after']['scheduleLastDate'] = $schedule->getScheduleLastDate()->format('Y-m-d');
            $scheduleMailDataArr[$schedule->getId()]['station'] = $schedule->getStation()->getName();
            $scheduleMailDataArr[$schedule->getId()]['scheduleId'] = $schedule->getId();
            $scheduleMailDataArr[$schedule->getId()]['scheduleName'] = $schedule->getName();
            $scheduleMailDataArr[$schedule->getId()]['stationId'] = $schedule->getStation()->getId();
        }

        if ($_ENV['APP_ENV'] == 'prod') {
            $content = $this->twig->render('emails/recurringDetail.twig', ['scheduleMailDataArr'=>$scheduleMailDataArr]);
            $this->emailService->sendEmail(['parth@corephp.com','yogesh.bhatt2009@gmail.com','trivedibrijesh2135@gmail.com'], 'Recurring Schedule', $content);
        }

        $io->success('Recurring data successfully inserted.');
    }

    public function getWeekDifferenceArray($startDate, $endDate,$year = null)
    {
       // dd($startDate, $endDate);
        $startDateObj = (new \DateTime($startDate))->setTimezone(new \DateTimeZone('UTC'));
        $startWeekNo =  ltrim($startDateObj->format('W'), '0');
        $endDateObj = (new \DateTime($endDate))->setTimezone(new \DateTimeZone('UTC'));
        $endWeekNo =  ltrim($endDateObj->format('W'), '0');

        $dd = (new \DateTime($startDate))->setTimezone(new \DateTimeZone('UTC'));
        $thisYearLastWeek = $dd->setISODate($dd->format('Y'),'53');
        $totalWeeks = $thisYearLastWeek->format('W') == 53 ? 53 : 52;

        if($startWeekNo > $endWeekNo){
            $endWeekNo = $totalWeeks + $endWeekNo;
        }



        $weeks=[];
        $startDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $currYear = $startDateObj->format('Y');
        $year = $currYear > $year ? $year : $currYear;
        $oldYear = $year;

        for($i = $startWeekNo; $i<= $endWeekNo;$i++){

            $startDateObj = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
            $newDateObj = $startDateObj->setISODate($oldYear, $i);
            $newDateObj->format('W').'**';
            $weeks['weeks'][] = $newDateObj->format('W');
            if($currYear == $year){
                $weeks['years'][] = (string)$year;
            } else {
                $weeks['years'][] = (string)($year);
                $year++;
            }
        }
        return $weeks;
    }

    public function getLastWeekDate($year, $week)
    {
        $endFrom = (new \DateTime())->setTimezone(new \DateTimeZone('UTC'));
        $endFrom->setISODate($year, $week);
        $endFrom->modify('+5 days');
        return $endFrom;
    }
}
