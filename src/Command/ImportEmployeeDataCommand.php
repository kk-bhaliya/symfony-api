<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Driver;
use App\Entity\User;
use App\Entity\Image;
use App\Entity\EmergencyContact;
use App\Entity\DriverSkill;


class ImportEmployeeDataCommand  extends Command
{
    protected static $defaultName = 'app:import-employee';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('file', InputArgument::OPTIONAL, 'Enter file name days')
        ->addArgument('company', InputArgument::OPTIONAL, 'Enter company ID')
        ->addArgument('station', InputArgument::OPTIONAL, 'Enter station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $fileName = 'public/csv/employee/'.$input->getArgument('file');
        $company = $input->getArgument('company');
        $station = $input->getArgument('station');

        if (file_exists($fileName)) {
            try {
                $file = fopen($fileName, "r");
                $firstRow = true;
                $cnt = $editCount = $addCount = 0;            

                while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if($cnt > 0 ){
                        $user = $this->em->getRepository('App\Entity\User')->findOneBy(['email'=>$column[21]]);

                        $userStatus=false;                        
                        if(strlen($column[39]) > 0 ){
                            $roleName='';
                            if($column[39] === 'Delivery Associate'  ){
                                $roleName='ROLE_DELIVERY_ASSOCIATE';
                            } else if( $column[39] == 'Station Manager' ){
                                $roleName='ROLE_STATION_MANAGER';
                            } else if( $column[39] == 'Operations Manager' ){
                                $roleName='ROLE_OPERATIONS_MANAGER';
                            } else if( $column[39] == 'Lead Driver' ){
                                $roleName='ROLE_LEAD_DRIVER';
                            } else if( $column[39] == 'Assistant Station Manager' ){    
                                $roleName='ROLE_ASSISTANT_STATION_MANAGER';
                            } else if( $column[39] == 'Recruiter' ){    
                                $roleName='ROLE_RECRUITER';                            
                            } else if( $column[39] == 'Dispatcher' ){    
                                $roleName='ROLE_DISPATCH';  
                            } else if( $column[39] == 'Administrative Assistant' ){    
                                $roleName='ROLE_ADMINISTRATIVE_ASSISTANT';   
                            } else if( $column[39] == 'Safety Champion' ){    
                                $roleName='ROLE_SAFETY_CHAMP';    
                            } else if( $column[39] == 'Benefits Specialist' ){    
                                $roleName='ROLE_SAFETY_CHAMP';                                              
                            } else {  
                                $roleName='ROLE_DELIVERY_ASSOCIATE';
                            }    

                            $role = $this->em->getRepository('App\Entity\Role')->findOneBy(['company'=>$company,'name'=>$roleName]) ; 
                        } else {
                            $role = $this->em->getRepository('App\Entity\Role')->findOneBy(['company'=>$company,'name'=>'ROLE_DELIVERY_ASSOCIATE'])  ;
                        }

                        if( !$user && $column[21] !='' ){
                            $checkUserExist = $this->em->getRepository('App\Entity\User')->findOneBy(['firstName'=>$column[0],'middleName'=>$column[1],'lastName'=>$column[2]]);

                            if ( !$checkUserExist ) {  
                                $newUser = new User();
                                $newUser->setEmail(strtolower($column[21]));
                                $newUser->setPassword( $this->plainPassword());  
                                $newUser->setFirstName($column[0]);
                                $newUser->setLastName($column[2]);
                                $newUser->setMiddleName($column[1]);
                                if($column[3] != '' ){
                                    $newUser->setFriendlyName($column[3]);    
                                } else {
                                    $newUser->setFriendlyName($column[0].' '.$column[2]);    
                                }                        
                                $newUser->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
                                $newUser->setIsEnabled(true);                         
                                $newUser->setIsArchive($column[31] == 'Active' ? false : true);
                                $newUser->addUserRole($role);                        
                                $this->em->persist($newUser);
                                $this->em->flush();   
                                $userStatus = true;
                                $user = $newUser;
                            } else {
                                $checkUserExist->setFirstName($column[0]);
                                $checkUserExist->setLastName($column[2]);
                                $checkUserExist->setMiddleName($column[1]);
                                $checkUserExist->setEmail(strtolower($column[21]));
                                if($column[3] != '' ){
                                    $checkUserExist->setFriendlyName($column[3]);    
                                } else {
                                    $checkUserExist->setFriendlyName($column[0].' '.$column[2]);  
                                }       
                                $checkUserExist->setIsArchive($column[31] == 'Active' ? false : true);                                      
                                $this->em->persist($checkUserExist);
                                $this->em->flush();
                                $userStatus = false; 
                                $user = $checkUserExist;
                            }                            
                        } else {
                                $user->setFirstName($column[0]);
                                $user->setLastName($column[2]);
                                $user->setMiddleName($column[1]);
                                if($column[3] != '' ){
                                    $user->setFriendlyName($column[3]);    
                                } else {
                                    $user->setFriendlyName($column[0].' '.$column[2]);
                                }    
                                $user->setIsArchive($column[31] == 'Active' ? false : true);                                         
                                $this->em->persist($user);
                                $this->em->flush();
                                $userStatus = false; 
                        }

                        if( $userStatus == true ){

                            $driver = new Driver();

                            $driver->setBirthday(strlen($column[34]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[34]))) : null);
                            $driver->setJobTitle($column[39]);
                            $driver->setSsn(substr($column[37],-4));
                            $driver->setEmploymentStatus($this->em->getRepository('App\Entity\EmploymentStatus')->findOneBy(['companyId'=>$company,'name'=>'Non-Exempt']));        
                            $driver->setPersonalPhone($column[19]);
                            $driver->setMaritalStatus(lcfirst($column[20]));
                            $driver->setGender(lcfirst($column[14]));

                            if($column[30] == 'Not a Veteran'){
                                $column[30] = 0;
                            }else {
                                $column[30] = 1;
                            }
                            $driver->setVeteranStatus($column[30]);
                            $driver->setSecondaryEmail(strtolower($column[33]));
                            $driver->setFullAddress($column[15].' '.$column[16].' '.$column[17].' '.$column[18] );
                            $driver->setNote('');
                            $driver->setHireDate(strlen($column[22]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[22]))) : null);
                            if(strlen($column[27]) > 0 ){
                                $shirtSize= $column[27];
                            } else {
                                $shirtSize= 'M - 2XL';
                            }
                            $driver->setShirtSize($shirtSize);
                            if(strlen($column[26]) > 0 ){
                                $shoeSize= $column[26];
                            } else {
                                $shoeSize= 'M - US 11/EU 44';
                            }
                            $driver->setShoeSize($shoeSize);
                            if(strlen($column[25]) > 0 ){
                                $pentSize= $column[25];
                            } else {
                                $pentSize= 'M - 2XL';
                            }
                            $driver->setPantSize($pentSize);
                            $driver->setTransporterID($column[40]);
                            $driver->setDriversLicenseNumber($column[29]);
                            $driver->setDriversLicenseNumberExpiration(strlen($column[35]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[35]))) : null);
                            $driver->setLicenseState($column[28]);                      
                            $driver->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
                            $driver->setUser($user);
                            $driver->addStation($this->em->getRepository('App\Entity\Station')->find($station));
                            $driver->addAssignedManager($this->em->getRepository('App\Entity\User')->find(169));
                            $driver->setIsArchive($column[31] == 'Active' ? false : true);                                                
                            $this->em->persist($driver);
                            $this->em->flush();  

                            if( $driver ){
                                if(strlen($column[4]) > 0 && strlen($column[5]) > 0 ){
                                    $emergencyContact = new EmergencyContact();
                                    $emergencyContact->setDriver($driver);
                                    $emergencyContact->setName($column[4]);
                                    $emergencyContact->setNumber($column[5]);
                                    $emergencyContact->setRelation($column[6]);
                                    $this->em->persist($emergencyContact);
                                    $this->em->flush();   
                                } 
                                if(strlen($column[7]) > 0 && strlen($column[8]) > 0 ){
                                    $emergencyContact = new EmergencyContact();
                                    $emergencyContact->setDriver($driver);
                                    $emergencyContact->setName($column[7]);
                                    $emergencyContact->setNumber($column[8]);
                                    $emergencyContact->setRelation($column[9]);
                                    $this->em->persist($emergencyContact);
                                    $this->em->flush();   
                                } 
                                if(strlen($column[10]) > 0 && strlen($column[11]) > 0 ){
                                    $emergencyContact = new EmergencyContact();
                                    $emergencyContact->setDriver($driver);
                                    $emergencyContact->setName($column[10]);
                                    $emergencyContact->setNumber($column[11]);
                                    $emergencyContact->setRelation($column[12]);
                                    $this->em->persist($emergencyContact);
                                    $this->em->flush();   
                                }   

                                $skill = new DriverSkill();
                                $skill->setDriver($driver);
                                $skill->setSkill($this->em->getRepository('App\Entity\Skill')->find(209));
                                $skill->setHourlyRate(17);
                                $this->em->persist($skill);
                                $this->em->flush(); 
                                $addCount++;
                                $io->success('User with '.$column[21].' email id has inserted');   
                            } 
                        } else {
                            if ($user) {
                                $driverObj = $user->getDriver();
                                if($driverObj){
                                    $qb = $this->em->createQueryBuilder();

                                    $result = $qb->update('App\Entity\Driver','d')
                                    ->set('d.birthday', ':birthDate')
                                    ->setParameter( 'birthDate', strlen($column[34]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[34]))) : null )
                                    ->set('d.jobTitle', ':jobTitle')
                                    ->setParameter( 'jobTitle',$column[39])
                                    ->set('d.ssn', ':ssn')
                                    ->setParameter( 'ssn',substr($column[37],-4))
                                    ->set('d.personalPhone', ':personalPhone')
                                    ->setParameter( 'personalPhone',$column[19])
                                    ->set('d.maritalStatus', ':maritalStatus')
                                    ->setParameter( 'maritalStatus',lcfirst($column[20]))
                                    ->set('d.gender', ':gender')
                                    ->setParameter( 'gender',lcfirst($column[14]))
                                    ->set('d.veteranStatus', ':veteranStatus')
                                    ->setParameter( 'veteranStatus',$column[30] == 'Not a Veteran' ? 0 :1 )
                                    ->set('d.secondaryEmail', ':secondaryEmail')
                                    ->setParameter( 'secondaryEmail',strtolower($column[33]))
                                    ->set('d.fullAddress', ':fullAddress')
                                    ->setParameter( 'fullAddress',$column[15].' '.$column[16].' '.$column[17].' '.$column[18])
                                    ->set('d.hireDate', ':hireDate')
                                    ->setParameter( 'hireDate',strlen($column[22]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[22]))) : null)
                                    ->set('d.shirtSize', ':shirtSize')
                                    ->setParameter( 'shirtSize',strlen($column[27]) > 0 ? $column[27] : 'M - 2XL')
                                    ->set('d.shoeSize', ':shoeSize')
                                    ->setParameter( 'shoeSize',strlen($column[26]) > 0 ? $column[26] : 'M - US 11/EU 44')
                                    ->set('d.pantSize', ':pantSize')
                                    ->setParameter( 'pantSize',strlen($column[25]) > 0 ? $column[25] : 'M - 2XL')
                                    ->set('d.transporterID', ':transporterID')
                                    ->setParameter( 'transporterID',$column[40])
                                    ->set('d.driversLicenseNumber', ':driversLicenseNumber')
                                    ->setParameter( 'driversLicenseNumber',$column[29])
                                    ->set('d.driversLicenseNumberExpiration', ':driversLicenseNumberExpiration')
                                    ->setParameter( 'driversLicenseNumberExpiration',strlen($column[35]) > 0 ? new \DateTime(date("Y-m-d", strtotime($column[35]))) : null)
                                    ->set('d.licenseState', ':licenseState')
                                    ->setParameter( 'licenseState',$column[28])
                                    ->set('d.isArchive', ':IsArchive')
                                    ->setParameter( 'IsArchive',$column[31] == 'Active' ? false : true)
                                    ->where('d.id = :id')
                                    ->setParameter( 'id', $driverObj )
                                    ->getQuery()
                                    ->execute();                                            
                                    if($result > 0){
                                        $editCount++;
                                        $io->success('User with '.$user->getEmail().' email id has updated');
                                    }

                                }
                            }
                        }
                    }
                    $cnt++; 
                }   
                $io->success('Total '.$addCount.' records has been inserted and ' .$editCount.' Updated');
            } catch (Exception $e) {
                $io->warning('Error.'.$e);
            }
        } else {
            $io->warning('file not found.');
        }    

    }

    public function plainPassword()
    {
        $numbers = array_rand(range(0, 9), rand(2, 2));
        $uppercase = array_rand(array_flip(range('A', 'Z')), rand(2, 2));
        $lowercase = array_rand(array_flip(range('a', 'z')), rand(2, 2));
        $special = array_rand(array_flip(['@', '#', '$', '!', '%', '*', '?', '&']), rand(2, 2));
        $password = array_merge(
            $numbers,
            $uppercase,
            $lowercase,
            $special
        );
        shuffle($password);
        return implode($password);
    }
}
