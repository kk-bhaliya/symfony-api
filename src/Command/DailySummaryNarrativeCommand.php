<?php

namespace App\Command;

use App\Criteria\Common;
use App\Entity\Company;
use App\Entity\DailyDriverSummary;
use App\Entity\DriverRoute;
use App\Entity\Station;
use App\Services\EmailService;
use App\Utils\EventEmitter;
use App\Utils\GlobalUtility;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Twig\Environment;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Services\AmazonS3Service;

class DailySummaryNarrativeCommand extends Command
{
    protected static $defaultName = 'dsp:dailysummary:email';

    private $em;
    private $eventEmitter;
    private $twig;
    private $globalUtils;
    private $path;
    private $dompdf;

    /**
     * @var EmailService
     */
    protected $emailService;
    protected $s3;


    public function __construct(
        EntityManagerInterface $em,
        EventEmitter $eventEmitter,
        Environment $twig,
        EmailService $emailService,
        GlobalUtility $globalUtils,
        AmazonS3Service $s3,
        string $path
    ) {
        $this->twig = $twig;
        $this->em = $em;
        $this->eventEmitter = $eventEmitter;
        $this->emailService = $emailService;
        $this->globalUtils = $globalUtils;
        $this->s3 = $s3;
        $this->path = $path;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Send Email to management at the end of the night')
            ->addOption('date', 'd', InputOption::VALUE_OPTIONAL, 'Date for narrative')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit how many companies to run')
            ->addOption('start', 's', InputOption::VALUE_OPTIONAL, 'First company to be run (offset, not ID)')
            ->addOption('companies', 'c', InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Specific company IDs to run', [])
            ->addOption('station', 'st', InputOption::VALUE_OPTIONAL, 'Specific station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        if(isset($_SERVER['NO_EMAIL'])) {
            return;
        }

        $io = new SymfonyStyle($input, $output);
        $opts = (object) $input->getOptions();

        /*if (!$opts->date) {
            return $io->comment('No date provided');
        }*/
        $stationParam = '';
        if ($opts->station) {
            $stationParam = $opts->station;
        }
        /* @var $repo CompanyRepository */
        $companiesQb = $this->em->createQueryBuilder()
            ->select('c')
            ->from(Company::class, 'c')
            ->addCriteria(Common::notArchived('c'));
        if ($opts->limit) {
            $companiesQb->setMaxResults($opts->limit);
        }
        if ($opts->start) {
            $companiesQb->setFirstResult($opts->start);
        }
        if ($opts->companies && count($opts->companies) > 0) {
            $companiesQb->andWhere($companiesQb->expr()->in('c.id', ':companies'))->setParameter('companies', $opts->companies);
        }
        $companiesObject = $companiesQb->getQuery()->getResult();
        $companies = new ArrayCollection($companiesObject);

        if (!$companies->count()) {
            return $io->comment('No active companies');
        }

        $io->comment('Starting emails');

        foreach ($companies as $company) {
            /* @var $company Company */
            $owners = $company->getOwners();

            $stationManagers = $company->getStationManager();

            $operationManagers = $company->getOperationManager();

            $operationAccountManagers = $company->getOperationAccountManager();

            $assistantStationManagers = $company->getAssistantStationManager();
      
            if($stationParam){
                $stations = $this->em->getRepository(Station::class)->findBy(['id'=>$stationParam, 'company' => $company->getId()]);
            }else{
                $stations = $this->em->getRepository(Station::class)->getStationByCompany($company);
            }

            if ($owners->count() || $stationManagers->count() || $operationManagers->count() || $operationAccountManagers->count() || $assistantStationManagers->count()) {

                foreach ($stations as $station) {
                    // Email
                    $emails = [];
                    // Main company owner and owner of current STATION
                    foreach($owners as $owner) {
                        // if(empty($owner->getDriver())) {
                            // $emails[] = $owner->getEmail();
                        // } else {
                            // if ($station->getId() == $owner->getDriver()->getStations()->first()->getId()) {
                            // }
                        // }
                        $emails[] = $owner->getEmail();
                    }

                    // Station manager of current STATION
                    foreach ($stationManagers->filter(function ($stationManagerObj) use ($station) {
                        return (!empty($stationManagerObj->getDriver()) && $station->getId() == $stationManagerObj->getDriver()->getStations()->first()->getId());
                    }) as $stationManager) {
                        $emails[] = $stationManager->getEmail();
                    }

                    // Operation manager of current STATION
                    // foreach ($operationManagers->filter(function ($operationManagerObj) use ($station) {
                    //     return (!empty($operationManagerObj->getDriver()) && $station->getId() == $operationManagerObj->getDriver()->getStations()->first()->getId());
                    // }) as $operationManager) {
                    //     $emails[] = $operationManager->getEmail();
                    // }
                    foreach ($operationManagers as $operationManager) {
                        $emails[] = $operationManager->getEmail();
                    }

                    // Operation account manager of current STATION
                    // foreach ($operationAccountManagers->filter(function ($operationAccountManagerObj) use ($station) {
                    //     return (!empty($operationAccountManagerObj->getDriver()) && $station->getId() == $operationAccountManagerObj->getDriver()->getStations()->first()->getId());
                    // }) as $operationAccountManager) {
                    //     $emails[] = $operationAccountManager->getEmail();
                    // }

                    foreach ($operationAccountManagers as $operationAccountManager) {
                        $emails[] = $operationAccountManager->getEmail();
                    }
                    
                    foreach ($assistantStationManagers as $assistantStationManager) {
                        $emails[] = $assistantStationManager->getEmail();
                    }
                    
                    if (count($emails) > 0) {

                        $stationTz = new \DateTimeZone($station->getTimeZoneName());
                        $stationNow = new \DateTime('now', $stationTz);
                        $offset = $stationNow->format('P');
                        $now =  new \DateTime('now');
                        $today = GlobalUtility::newUTCDateTime()->format('Y-m-d');
                        if(!$opts->date) {
                            $passedDate = $stationNow;
                            $opts->date = $stationNow->format('Y-m-d');
                        } else {
                            $passedDate = new \DateTime($opts->date);
                        }

                        $passedDate->setTime($passedDate->format('H'), $passedDate->format('i'), $passedDate->format('s'));
                        $passedNow = $passedDate->format('Y-m-d H:i:00');
                        if ($passedDate->format('l') == 'Sunday') {
                            $currentWeek = $passedDate->format('W') + 1;
                        } else {
                            $currentWeek = $passedDate->format('W');
                        }

                        $pastWeek = $currentWeek - 1;
                        $currentWeekStartEndDay = $this->globalUtils->getStartAndEndDate($currentWeek);
                        $pastWeekStartEndDay = $this->globalUtils->getStartAndEndDate($pastWeek);
                        $lastTwoWeekDataStartEndDay = ['start' => $pastWeekStartEndDay['start'], 'end' => $currentWeekStartEndDay['end']];
                        $lastSevenDays = $this->globalUtils->getDatesOfQuarter('byday', [6, 0], null, 'Y-m-d');
                        $lastFourtinDays = $this->globalUtils->getDatesOfQuarter('byday', [13, 0], null, 'Y-m-d');
                        $data = [];
                        $routeDetails = $quickReview = $employeeDetails = [];

                        // Get daily driver summary  data for the station
                        $dailyDriverSummaries = $this->em->getRepository(DailyDriverSummary::class)->getDailyDriverSummaryByStation($station, $opts->date);

                        $cnt = 0;
                        if (count($dailyDriverSummaries) > 0) {
                            foreach ($dailyDriverSummaries as $dailyDriverSummary) {

                                $data['stationDailyNarrative'] = $dailyDriverSummary->getSummary() . "\n\r";

                                if ($cnt == count($dailyDriverSummaries) - 1) {
                                    $data['stationDailyNarrative'] = $data['stationDailyNarrative'] . "\n\r";
                                }

                                $quickReview['scheduledInterviews'] = (isset($quickReview['scheduledInterviews']) ? $quickReview['scheduledInterviews'] : 0) + $dailyDriverSummary->getScheduledInterviews();

                                $quickReview['interviewsCompleted'] = (isset($quickReview['interviewsCompleted']) ? $quickReview['interviewsCompleted'] : 0) + $dailyDriverSummary->getInterviewsCompleted();

                                $quickReview['driversHired'] = (isset($quickReview['driversHired']) ? $quickReview['driversHired'] : 0) + $dailyDriverSummary->getDriversHired();

                                $quickReview['quit'] = (isset($quickReview['quit']) ? $quickReview['quit'] : 0) + $dailyDriverSummary->getDriversQuit();

                                $quickReview['terminated'] = (isset($quickReview['terminated']) ? $quickReview['terminated'] : 0) + $dailyDriverSummary->getDriversFired();

                                $cnt++;
                            }
                        } else {
                            $data['stationDailyNarrative'] = "No summary found.";
                            $quickReview['scheduledInterviews'] = 0;
                            $quickReview['interviewsCompleted'] = 0;
                            $quickReview['driversHired'] = 0;
                            $quickReview['quit'] = 0;
                            $quickReview['terminated'] = 0;
                        }

                        // Get route data for the station
                        $driverRoutes = $this->em->getRepository(DriverRoute::class)->getTodayDriverRouteByStation($station, $opts->date);

                        $lastSevenDayDriverRoutes = $this->em->getRepository(DriverRoute::class)->getRouteCountByDriverAndScheduleBetweenParticularDates($station, $lastSevenDays);

                        $currentWeekDriverRoutes = $this->em->getRepository(DriverRoute::class)->getDriverRoutesBetweenTwoDatesByStation($station, $currentWeekStartEndDay);

                        $lastTwoWeekriverRoutes = $this->em->getRepository(DriverRoute::class)->getDriverRoutesBetweenTwoDatesByStation($station, $lastTwoWeekDataStartEndDay);
                        if (count($driverRoutes) > 0) {
                            $cnt = 0;
                            $routeDetails['totalRoutesForToday'] = 0;
                            $invoiceTypeShift = $backupShiftMsg = $routeCodeUnique = $noRouteCode = $duplicateRouteCode = $uniueRouteCode = [];
                            foreach ($driverRoutes as $driverRoute) {
                                $isBackup = ($driverRoute->getShiftType()->getCategory() == \App\Entity\Shift::BACKUP);
                                $driverRouteCodes = $driverRoute->getDriverRouteCodes()->filter(function ($routeCode) {
                                    return !$routeCode->getIsArchive();
                                });
                                if ((!empty($driverRouteCodes->first()) && !empty($driverRoute->getShiftInvoiceType())) && (!$isBackup || ($isBackup && empty($driverRoute->getDatetimeEnded())))) {

                                    foreach ($driverRouteCodes as $routeCodeObj) {
                                        if (!in_array($routeCodeObj->getCode(),$routeCodeUnique)) {
                                            $routeDetails['totalRoutesForToday'] = (isset($routeDetails['totalRoutesForToday']) ? $routeDetails['totalRoutesForToday'] : 0) + 1;
                                        } else {
                                            if (!in_array($routeCodeObj->getCode(),$uniueRouteCode)) {
                                                $duplicateRouteCode['duplicateRouteCode'][$driverRoute->getShiftType()->getName()] = (isset($duplicateRouteCode['duplicateRouteCode'][$driverRoute->getShiftType()->getName()]) ? $duplicateRouteCode['duplicateRouteCode'][$driverRoute->getShiftType()->getName()] : 0) + 1;
                                            }
                                            $uniueRouteCode[] = $routeCodeObj->getCode();
                                        }
                                        $routeCodeUnique[] = $routeCodeObj->getCode();
                                    }
                                }
                                if ($isBackup && !empty($driverRoute->getDatetimeEnded())) {
                                    $backupShiftMsg['backup'][$driverRoute->getShiftType()->getName()] = (isset($backupShiftMsg['backup'][$driverRoute->getShiftType()->getName()]) ? $backupShiftMsg['backup'][$driverRoute->getShiftType()->getName()] : 0) + 1;
                                }

                                if (empty($driverRoute->getShiftInvoiceType())) {
                                    $invoiceTypeShift['invoiceType'][$driverRoute->getShiftType()->getName()] = (isset($invoiceTypeShift['invoiceType'][$driverRoute->getShiftType()->getName()]) ? $invoiceTypeShift['invoiceType'][$driverRoute->getShiftType()->getName()] : 0) + 1;
                                }

                                if (empty($driverRouteCodes->first())) {
                                    $noRouteCode['noRouteCode'][$driverRoute->getShiftType()->getName()] = (isset($noRouteCode['noRouteCode'][$driverRoute->getShiftType()->getName()]) ? $noRouteCode['noRouteCode'][$driverRoute->getShiftType()->getName()] : 0) + 1;
                                }

                                $routeDetails['shifts'][$driverRoute->getShiftType()->getName()] = (isset($routeDetails['shifts'][$driverRoute->getShiftType()->getName()]) ? $routeDetails['shifts'][$driverRoute->getShiftType()->getName()] : 0) + 1;

                                $scheduledDrivers = isset($routeDetails['scheduledDrivers']) ? $routeDetails['scheduledDrivers'] : 0;
                                $routeDetails['scheduledDrivers'] = !empty($driverRoute->getDriver()) ? ($scheduledDrivers + 1) : $scheduledDrivers;

                                $backupShifts = isset($routeDetails['backupShifts']) ? $routeDetails['backupShifts'] : 0;

                                $routeDetails['backupShifts'] = ($isBackup) ? ($backupShifts + 1) : $backupShifts;

                                $backupsSentHome = isset($routeDetails['backupsSentHome']) ? $routeDetails['backupsSentHome'] : 0;
                                $routeDetails['backupsSentHome'] = (!empty($driverRoute->getDatetimeEnded())) ? ($backupsSentHome + 1) : $backupsSentHome;

                                $rescueShift = isset($quickReview['rescuesShifts']) ? $quickReview['rescuesShifts'] : 0;
                                $isRescue = $driverRoute->getIsRescuer();
                                $quickReview['rescuesShifts'] = ($isRescue) ? ($rescueShift + 1) : $rescueShift;

                                $devices = isset($quickReview['totalDevices']) ? $quickReview['totalDevices'] : 0;
                                $hasDevice = !empty($driverRoute->getDevice()) ? true : false;
                                $quickReview['totalDevices'] = ($hasDevice) ? ($devices + count($driverRoute->getDevice())) : $devices;

                                if ($hasDevice) {
                                    foreach ($driverRoute->getDevice() as $device) {
                                        $quickReview['devices'][$device->getManufacturer().'-'.$device->getModel()] = (isset($quickReview['devices'][$device->getManufacturer().'-'.$device->getModel()]) ? $quickReview['devices'][$device->getManufacturer().'-'.$device->getModel()] : 0) + 1;
                                    }
                                }

                                if (!empty($driverRoute->getDriver()) && empty($driverRoute->getPunchIn()) && empty($driverRoute->getPunchOut(1))) {
                                    $employeeDetails['ncnsDrivers'][] = $driverRoute->getDriver()->getFullName();
                                } else {
                                    $employeeDetails['ncnsDrivers'] = [];
                                }

                                if (!empty($driverRoute->getDriver())) {
                                    $lunch = 'No';
                                    $minutes = 0;
                                    if (!empty($driverRoute->getBreakPunchIn(1)) && !empty($driverRoute->getBreakPunchOut())) {
                                        $interval = date_diff($driverRoute->getBreakPunchIn(1), $driverRoute->getBreakPunchOut());
                                        $minutes = (($interval->h * 60) + $interval->i);
                                        $lunch = $minutes.' minutes';
                                    }

                                    if (!empty($driverRoute->getPunchIn()) && (empty($driverRoute->getPunchOut(1)) || empty($driverRoute->getBreakPunchIn(1)) || empty($driverRoute->getBreakPunchOut()) || (!empty($driverRoute->getBreakPunchIn(1)) && !empty($driverRoute->getBreakPunchOut()) && $minutes < 30))) {
                                        $npDPunchedInTime = $driverRoute->getPunchIn();
                                        $npDPunchedInTime->setTimezone(new \DateTimeZone($station->getTimeZoneName()));

                                        $npDPunchedOutTime = 'No';
                                        $punchInOutStatus = false;
                                        $duration = 0;
                                        if (!empty($driverRoute->getPunchOut(1))) {
                                            $npDPunchedOutTime = $driverRoute->getPunchOut(1);
                                            $npDPunchedOutTime->setTimezone(new \DateTimeZone($station->getTimeZoneName()));

                                            $since_start = $npDPunchedInTime->diff($npDPunchedOutTime);
                                            $duration = $since_start->days * 24 * 60;
                                            $duration += $since_start->h * 60;
                                            $duration += $since_start->i;
                                            if( $lunch != 'No' && $minutes > 0 ){
                                                $duration = $duration - $minutes;
                                            }
                                            if($duration < 240){
                                                $punchInOutStatus = true;
                                            }
                                            $npDPunchedOutTime = $npDPunchedOutTime->format('g:ia');
                                        }
                                        if($punchInOutStatus == false){
                                            $employeeDetails['notPunchedDrivers'][$cnt]['driver'] = $driverRoute->getDriver()->getFullName();
                                            $employeeDetails['notPunchedDrivers'][$cnt]['punchIn'] = $npDPunchedInTime->format('g:ia');
                                            $employeeDetails['notPunchedDrivers'][$cnt]['punchOut'] = $npDPunchedOutTime;
                                            $employeeDetails['notPunchedDrivers'][$cnt]['lunch'] = $lunch;
                                        }
                                        $cnt++;
                                    }
                                }

                                $vehicles = isset($quickReview['totalVans']) ? $quickReview['totalVans'] : 0;
                                $hasVehicles = !empty($driverRoute->getVehicle()) ? true : false;
                                $quickReview['totalVans'] = ($hasVehicles) ? ($vehicles + 1) : $vehicles;
                                $quickReview['vans']['active'] = isset($quickReview['vans']['active']) ? $quickReview['vans']['active'] : 0;
                                $quickReview['vans']['inactive'] = isset($quickReview['vans']['inactive']) ? $quickReview['vans']['inactive'] : 0;
                                $quickReview['vans']['underRepair'] = isset($quickReview['vans']['underRepair']) ? $quickReview['vans']['underRepair'] : 0;
                                $quickReview['vans']['beingServiced'] = isset($quickReview['vans']['beingServiced']) ? $quickReview['vans']['beingServiced'] : 0;
                                if ($hasVehicles) {
                                    $quickReview['vans']['active'] = ($driverRoute->getVehicle()->getStatus() == 'Active') ? ($quickReview['vans']['active'] + 1) : $quickReview['vans']['active'];
                                    $quickReview['vans']['inactive'] = ($driverRoute->getVehicle()->getStatus() == 'Inactive') ? ($quickReview['vans']['inactive'] + 1) : $quickReview['vans']['inactive'];
                                    $quickReview['vans']['underRepair'] = ($driverRoute->getVehicle()->getStatus() == 'Under Repair') ? ($quickReview['vans']['underRepair'] + 1) : $quickReview['vans']['underRepair'];
                                    $quickReview['vans']['beingServiced'] = ($driverRoute->getVehicle()->getStatus() == 'Being Serviced') ? ($quickReview['vans']['beingServiced'] + 1) : $quickReview['vans']['beingServiced'];
                                }
                            }

                            if (!isset($employeeDetails['notPunchedDrivers'])) {
                                $employeeDetails['notPunchedDrivers'] = [];
                            }

                        } else {
                            $routeDetails['totalRoutesForToday'] = 0;
                            $routeDetails['scheduledDrivers'] = 0;
                            $routeDetails['backupShifts'] = 0;
                            $routeDetails['backupsSentHome'] =  0;
                            $quickReview['rescuesShifts'] = 0;
                            $quickReview['totalDevices'] = 0;
                            $employeeDetails['notPunchedDrivers'] = [];
                            $employeeDetails['ncnsDrivers'] = [];
                            $quickReview['totalVans'] = 0;
                            $quickReview['vans']['active'] = 0;
                            $quickReview['vans']['inactive'] = 0;
                            $quickReview['vans']['underRepair'] = 0;
                            $quickReview['vans']['beingServiced'] = 0;
                        }

                        if (count($lastSevenDayDriverRoutes) > 0) {
                            foreach ($lastSevenDayDriverRoutes as $lastSevenDayDriverRoute) {
                                if ($lastSevenDayDriverRoute['betweenDatesDriverRoutesCount'] > 3) {
                                    $routeDetails['regularlyScheduledDrivers'] = (isset($routeDetails['regularlyScheduledDrivers']) ? $routeDetails['regularlyScheduledDrivers'] : 0) + 1;
                                } else {
                                    $routeDetails['regularlyScheduledDrivers'] = 0;
                                }
                            }
                        } else {
                            $routeDetails['regularlyScheduledDrivers'] = 0;
                        }

                        $routeDetails['droppedRoutes']['amazonLateCancellations'] = 0;
                        $routeDetails['droppedRoutes']['stationDroppedRoutes'] = 0;

                        $totalMinutesWorkedForToday = $totalMinutesWorkedForWeek = $amazonBillableMinutesForToday = $amazonBillableMinutesForWeek = $totalOverTimeForToday = $totalOverTimeForWeek = $backupMinutesForToday = $backupMinutesForWeek = $rescueMinutesForToday = $rescueMinutesForWeek = 0;
                        $aDailySummeryStationBudgets = $todayRouteCodeUnique = $weekRouteCodeUnique = [];

                        foreach ($currentWeekDriverRoutes as $currentWeekDriverRoute) {
                            if ($currentWeekDriverRoute->getDateCreated()->format('Y-m-d') <= $passedDate->format('Y-m-d')) {
                                $isPunched = (!empty($currentWeekDriverRoute->getPunchIn()) && !empty($currentWeekDriverRoute->getPunchOut(1)));
                                $isBackup = ($currentWeekDriverRoute->getShiftType()->getCategory() == \App\Entity\Shift::BACKUP);
                                $isRescue = $currentWeekDriverRoute->getIsRescuer();

                                $minuteWorked = $totalMinutesWorked = 0;

                                $shiftStartTime = $currentWeekDriverRoute->getStartTime();
                                $shiftEndTime = $currentWeekDriverRoute->getEndTime();
                                $punchedInTime = $currentWeekDriverRoute->getPunchIn();
                                $punchedOutTime = $currentWeekDriverRoute->getPunchOut(1);

                                $billableHours = $shiftEndTime->diff($shiftStartTime);
                                $totalBillableMinutes = (($billableHours->h * 60) + $billableHours->i) - $currentWeekDriverRoute->getUnpaidBreak();
                                $isWeekViewCriteriaUnique = false;
                                $currentWeekDriverRouteCodes = $currentWeekDriverRoute->getDriverRouteCodes()->filter(function ($routeCode) {
                                    return !$routeCode->getIsArchive();
                                });
                                if ((!empty($currentWeekDriverRouteCodes->first()) && !empty($currentWeekDriverRoute->getShiftInvoiceType())) && (!$isBackup || ($isBackup && empty($currentWeekDriverRoute->getDatetimeEnded())))) {
                                    foreach ($currentWeekDriverRouteCodes as $routeCodeObj) {
                                            if (!in_array($routeCodeObj->getCode(),$weekRouteCodeUnique)) {
                                                $amazonBillableMinutesForWeek = $amazonBillableMinutesForWeek + $totalBillableMinutes;
                                            }
                                            $weekRouteCodeUnique[] = $routeCodeObj->getCode();
                                    }
                                    $isWeekViewCriteriaUnique = true;
                                }

                                $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['amazonBillableMinuteToday'] = isset($aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['amazonBillableMinuteToday']) ? $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['amazonBillableMinuteToday'] + $totalBillableMinutes : $totalBillableMinutes;

                                if ($isRescue) {
                                    $rescueBillableHour = (($billableHours->h * 60) + $billableHours->i);
                                    $rescueMinutesForWeek = $rescueMinutesForWeek + $rescueBillableHour;
                                    $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['rescueMinutesForToday'] = isset($aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['rescueMinutesForToday']) ? $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['rescueMinutesForToday'] + $rescueBillableHour : $rescueBillableHour;
                                }
                                if ($isPunched) {
                                    $minuteWorked = $punchedOutTime->diff($punchedInTime);
                                    $totalMinutesWorked = (($minuteWorked->h * 60) + $minuteWorked->i) - $currentWeekDriverRoute->getUnpaidBreak();

                                    $totalMinutesWorkedForWeek = $totalMinutesWorkedForWeek + $totalMinutesWorked;

                                    if ($totalMinutesWorked > 480) {
                                        $totalOverTimeWorked = ($totalMinutesWorked - 480);
                                        $totalOverTimeForWeek = $totalOverTimeForWeek + $totalOverTimeWorked;
                                        $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['overTimeForToday'] = isset($aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['overTimeForToday']) ? $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['overTimeForToday'] + $totalOverTimeWorked : $totalOverTimeWorked;
                                    }

                                    if ($isBackup) {
                                        $backupMinutesForWeek = $backupMinutesForWeek + $totalMinutesWorked;
                                        $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['backupMinutesForToday'] = isset($aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['backupMinutesForToday']) ? $aDailySummeryStationBudgets[$currentWeekDriverRoute->getDateCreated()->format('Y-m-d')]['backupMinutesForToday'] + $totalMinutesWorked : $totalMinutesWorked;
                                    }
                                }
                                if ($currentWeekDriverRoute->getDateCreated()->format('Y-m-d') == $opts->date) {
                                    if ($isWeekViewCriteriaUnique) {
                                        foreach ($currentWeekDriverRouteCodes as $routeCodeObj) {
                                                if (!in_array($routeCodeObj->getCode(),$todayRouteCodeUnique)) {
                                                    $amazonBillableMinutesForToday = $amazonBillableMinutesForToday + $totalBillableMinutes;
                                                }
                                                $todayRouteCodeUnique[] = $routeCodeObj->getCode();
                                        }
                                    }

                                    if ($isRescue) {
                                        $rescueMinutesForToday = $rescueMinutesForToday + (($billableHours->h * 60) + $billableHours->i);
                                    }

                                    if ($isPunched) {

                                        $totalMinutesWorkedForToday = $totalMinutesWorkedForToday + $totalMinutesWorked;

                                        if ($totalMinutesWorked > 480) {
                                            $totalOverTimeForToday = $totalOverTimeForToday + ($totalMinutesWorked - 480);
                                        }

                                        if ($isBackup) {
                                            $backupMinutesForToday = $backupMinutesForToday + $totalMinutesWorked;
                                        }
                                    }
                                }
                            }
                        }
                        $tempAmazonBillableHourForToday = $tempAmazonBillableHoursForWeek  = $totalTodayOverTimeBudget = $totalTodayRescueBudget = $totalTodayBackupBudget = $totalWeekRescueBudget = $totalWeekBackupBudget = $totalWeekOverTimeBudget = $priorTodayOverTime = $priorTodayBackupHours = $priorTodayRescueHours = $priorWeekbackupHours = $priorWeekOverTimeHours = $priorWeekRescueHours = $tempRescueHoursForWeek = $tempBackupHoursForWeek = $tempTotalOverTimeForWeek = 0;
                        foreach ($aDailySummeryStationBudgets as $keyDate => $aDailySummeryStationBudget) {

                            $tempAmazonBillableHoursForWeek = (float) ((floor($aDailySummeryStationBudget['amazonBillableMinuteToday'] / 60)) . ((($aDailySummeryStationBudget['amazonBillableMinuteToday'] % 60) > 0) ? '.' . round((($aDailySummeryStationBudget['amazonBillableMinuteToday'] % 60) / 60) * 100) : '')) + $tempAmazonBillableHoursForWeek;

                            $overTimeForTodayBudget = isset($aDailySummeryStationBudget['overTimeForToday']) ? $aDailySummeryStationBudget['overTimeForToday'] : 0;
                            $backupMinutesForTodayBudget = isset($aDailySummeryStationBudget['backupMinutesForToday']) ? $aDailySummeryStationBudget['backupMinutesForToday'] : 0;
                            $rescueMinutesForTodayBudget = isset($aDailySummeryStationBudget['rescueMinutesForToday']) ? $aDailySummeryStationBudget['rescueMinutesForToday'] : 0;

                            if ($keyDate == $opts->date) {
                                $tempAmazonBillableHoursForToday = (float) ((floor($aDailySummeryStationBudget['amazonBillableMinuteToday'] / 60)) . ((($aDailySummeryStationBudget['amazonBillableMinuteToday'] % 60) > 0) ? '.' . round((($aDailySummeryStationBudget['amazonBillableMinuteToday'] % 60) / 60) * 100) : ''));

                                //totalOverTimeForToday
                                $tempTotalOverTimeForToday = (float) ((floor($overTimeForTodayBudget / 60)) . ((($overTimeForTodayBudget % 60) > 0) ? '.' . round((($overTimeForTodayBudget % 60) / 60) * 100) : ''));
                                $canUseDayHourOverTime = $priorWeekOverTimeHours + round($tempAmazonBillableHoursForToday * ($company->getOvertimeHours() / 100));
                                $totalTodayOverTimeBudget = ($canUseDayHourOverTime > 0 ) ? (($tempTotalOverTimeForToday *  100) / $canUseDayHourOverTime) : 0;

                                //backupHoursForToday
                                $tempBackupHoursForToday = (float) ((floor($backupMinutesForTodayBudget / 60)) . ((($backupMinutesForTodayBudget % 60) > 0) ? '.' . round((($backupMinutesForTodayBudget % 60) / 60) * 100) : ''));
                                $canUseBackupHourOverTime = $priorWeekbackupHours + round($tempAmazonBillableHoursForToday * ($company->getBackupDriverHours() / 100));
                                $totalTodayBackupBudget = ($canUseBackupHourOverTime > 0) ? (($tempBackupHoursForToday * 100) / $canUseBackupHourOverTime) : 0;

                                //rescueHoursForToday
                                $tempRescueHoursForToday = (float) ((floor($rescueMinutesForTodayBudget / 60)) . ((($rescueMinutesForTodayBudget % 60) > 0) ? '.' . round((($rescueMinutesForTodayBudget % 60) / 60) * 100) : ''));
                                $canUseRescueHourOverTime = $priorWeekRescueHours + round($tempAmazonBillableHoursForToday * ($company->getRescueHours() / 100));
                                $totalTodayRescueBudget = ($canUseRescueHourOverTime > 0) ? (($tempRescueHoursForToday * 100) / $canUseRescueHourOverTime) : 0;
                            }

                            //totalOverTimeForWeek
                            $tempTotalOverTimeForWeek = (float) ((floor($overTimeForTodayBudget / 60)) . ((($overTimeForTodayBudget % 60) > 0) ? '.' . round((($overTimeForTodayBudget % 60) / 60) * 100) : '')) + $tempTotalOverTimeForWeek;
                            $canUseWeekHourOverTime = $priorWeekOverTimeHours + round($tempAmazonBillableHoursForWeek * ($company->getOvertimeHours() / 100));
                            $totalWeekOverTimeBudget = ($canUseWeekHourOverTime > 0) ? (($tempTotalOverTimeForWeek * 100) / $canUseWeekHourOverTime) : 0;
                            $priorWeekOverTimeHours = $canUseWeekHourOverTime - $tempTotalOverTimeForWeek;

                            //backupHoursForWeek
                            $tempBackupHoursForWeek = (float) ((floor($backupMinutesForTodayBudget / 60)) . ((($backupMinutesForTodayBudget % 60) > 0) ? '.' . round((($backupMinutesForTodayBudget % 60) / 60) * 100) : '')) + $tempBackupHoursForWeek;
                            $canUseWeekHourBackupHours = $priorWeekbackupHours + round($tempAmazonBillableHoursForWeek * ($company->getBackupDriverHours() / 100));
                            $totalWeekBackupBudget = ($canUseWeekHourBackupHours > 0) ? (($tempBackupHoursForWeek * 100) / $canUseWeekHourBackupHours) : 0;
                            $priorWeekbackupHours = $canUseWeekHourBackupHours - $tempBackupHoursForWeek;

                            //rescueHoursForWeek
                            $tempRescueHoursForWeek = (float) ((floor($rescueMinutesForTodayBudget / 60)) . ((($rescueMinutesForTodayBudget % 60) > 0) ? '.' . round((($rescueMinutesForTodayBudget % 60) / 60) * 100) : '')) + $tempRescueHoursForWeek;
                            $canUseWeekHourRescueHours = $priorWeekRescueHours + (round($tempAmazonBillableHoursForWeek * ($company->getRescueHours() / 100)));
                            $totalWeekRescueBudget = ($canUseWeekHourRescueHours > 0) ? (($tempRescueHoursForWeek * 100) / $canUseWeekHourRescueHours) : 0;
                            $priorWeekRescueHours = $canUseWeekHourRescueHours - $tempRescueHoursForWeek;
                        }

                        $routeDetails['totalHoursWorkedForToday'] = (float) ((floor($totalMinutesWorkedForToday / 60)) . ((($totalMinutesWorkedForToday % 60) > 0) ? '.' . round((($totalMinutesWorkedForToday % 60) / 60) * 100) : ''));

                        $routeDetails['totalHoursWorkedForWeek'] = (float) ((floor($totalMinutesWorkedForWeek / 60)) . ((($totalMinutesWorkedForWeek % 60) > 0) ? '.' . round((($totalMinutesWorkedForWeek % 60) / 60) * 100) : ''));

                        $routeDetails['amazonBillableHoursForToday'] = (float) ((floor($amazonBillableMinutesForToday / 60)) . ((($amazonBillableMinutesForToday % 60) > 0) ? '.' . round((($amazonBillableMinutesForToday % 60) / 60) * 100) : ''));

                        $routeDetails['amazonBillableHoursForWeek'] = (float) ((floor($amazonBillableMinutesForWeek / 60)) . ((($amazonBillableMinutesForWeek % 60) > 0) ? '.' . round((($amazonBillableMinutesForWeek % 60) / 60) * 100) : ''));

                        $routeDetails['totalOverTimeForToday'] = (float) ((floor($totalOverTimeForToday / 60)) . ((($totalOverTimeForToday % 60) > 0) ? '.' . round((($totalOverTimeForToday % 60) / 60) * 100) : ''));

                        $routeDetails['totalOverTimeForWeek'] = (float) ((floor($totalOverTimeForWeek / 60)) . ((($totalOverTimeForWeek % 60) > 0) ? '.' . round((($totalOverTimeForWeek % 60) / 60) * 100) : ''));

                        /* TODO: Update with actual data */
                        $routeDetails['overTimePercentageOfWeeklyBudgetUsedForToday'] = number_format($totalTodayOverTimeBudget,2).'%';
                        $routeDetails['overTimePercentageOfWeeklyBudgetUsedForWeek'] = number_format($totalWeekOverTimeBudget,2).'%';

                        $routeDetails['backupHoursForToday'] = (float) ((floor($backupMinutesForToday / 60)) . ((($backupMinutesForToday % 60) > 0) ? '.' . round((($backupMinutesForToday % 60) / 60) * 100) : ''));

                        $routeDetails['backupHourssForWeek'] = (float) ((floor($backupMinutesForWeek / 60)) . ((($backupMinutesForWeek % 60) > 0) ? '.' . round((($backupMinutesForWeek % 60) / 60) * 100) : ''));

                        /* TODO: Update with actual data */
                        $routeDetails['backupPercentageOfWeeklyBudgetUsedForToday'] = number_format($totalTodayBackupBudget,2).'%';
                        $routeDetails['backupPercentageOfWeeklyBudgetUsedForWeek'] = number_format($totalWeekBackupBudget,2).'%';

                        $routeDetails['rescueHoursForToday'] = (float) ((floor($rescueMinutesForToday / 60)) . ((($rescueMinutesForToday % 60) > 0) ? '.' . round((($rescueMinutesForToday % 60) / 60) * 100) : ''));

                        $routeDetails['rescueHoursForWeek'] = (float) ((floor($rescueMinutesForWeek / 60)) . ((($rescueMinutesForWeek % 60) > 0) ? '.' . round((($rescueMinutesForWeek % 60) / 60) * 100) : ''));

                        /* TODO: Update with actual data */
                        $routeDetails['rescuePercentageOfWeeklyBudgetUsedForToday'] = number_format($totalTodayRescueBudget,2).'%';
                        $routeDetails['rescuePercentageOfWeeklyBudgetUsedForWeek'] = number_format($totalWeekRescueBudget,2).'%';


                        /* TODO: Update accident Injuries */
                        //$quickReview['accidentsInjuries'] = ['Rex Cars – T-bone accident (not at fault)','Peg Legge – Sprained ankle','Tim Burr – Knocked over mailbox'];
                        $quickReview['accidentsInjuries'] = '';

                        $rescuedDriverInLastTwoWeeks = $totalDaysWorked = $totalHourWorked = $totalHourSchedule = $remainHours = $totalDaysWorkedByWeek = $totalHourWorkedByWeek = $overTimeDrivers = [];
                        $rescuedIds=[];
                        foreach ($lastTwoWeekriverRoutes as $lastTwoWeekriverRoute) {

                            if ($lastTwoWeekriverRoute->getDateCreated()->format('l') == 'Sunday') {
                                $routeWeek = $lastTwoWeekriverRoute->getDateCreated()->format('W') + 1;
                            } else {
                                $routeWeek = $lastTwoWeekriverRoute->getDateCreated()->format('W');
                            }

                            if (!empty($lastTwoWeekriverRoute->getPunchIn()) && !empty($lastTwoWeekriverRoute->getPunchOut(1))) {
                                $totalHourWorkedByWeekByDriver = $this->getHoursBetweenTwoTime($lastTwoWeekriverRoute->getPunchIn(), $lastTwoWeekriverRoute->getPunchOut(1));

                                $totalHourWorkedByWeek['totalHourWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek] = isset($totalHourWorkedByWeek['totalHourWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek]) ? $totalHourWorkedByWeek['totalHourWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek] + $totalHourWorkedByWeekByDriver : $totalHourWorkedByWeekByDriver;

                                $overTimeHourPerWeek = $totalHourWorkedByWeek['totalHourWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek];

                                $overTimeHourPerWeek = ($overTimeHourPerWeek > 40) ? ($overTimeHourPerWeek - 40) : 0;

                                $totalOverTimeHourWorkedByWeek['totalOverTimeHourWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek] = $overTimeHourPerWeek;

                                if ($overTimeHourPerWeek > 0) {
                                    $overTimeDrivers[$lastTwoWeekriverRoute->getDriver()->getId()] = $lastTwoWeekriverRoute->getDriver()->getFullName();
                                }
                            }

                            if (!empty($lastTwoWeekriverRoute->getStartTime()) && !empty($lastTwoWeekriverRoute->getEndTime())) {
                                $totalDaysWorkedByWeek['totalDaysWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek] = isset($totalDaysWorkedByWeek['totalDaysWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek]) ? $totalDaysWorkedByWeek['totalDaysWorkedByWeek'][$lastTwoWeekriverRoute->getDriver()->getId()][$routeWeek] + 1 : 1;
                            }

                            if ($lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d') <= $passedDate->format('Y-m-d')) {
                                if($currentWeekStartEndDay['start'] <= $lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d')){
                                    if (!empty($lastTwoWeekriverRoute->getPunchIn()) && !empty($lastTwoWeekriverRoute->getPunchOut(1))) {
                                        $totalDaysWorked['totalDaysWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] = isset($totalDaysWorked['totalDaysWorked'][$lastTwoWeekriverRoute->getDriver()->getId()]) ? $totalDaysWorked['totalDaysWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] + 1 : 1;

                                        $totalHourWorkedByDriver = $this->getHoursBetweenTwoTime($lastTwoWeekriverRoute->getPunchIn(), $lastTwoWeekriverRoute->getPunchOut(1));

                                        $totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] = isset($totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()]) ? $totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] + $totalHourWorkedByDriver : $totalHourWorkedByDriver;
                                    }
                                }

                                if($lastTwoWeekriverRoute->getIsRescued() == true) {
                                    $codeArr=[];
                                    foreach ($lastTwoWeekriverRoute->getDriverRouteCodes()->filter(function ($routeCode) {
                                        return !$routeCode->getIsArchive();
                                    }) as $code) {
                                        $codeArr[] = $code->getCode();
                                    }
                                    $rescuerLastTwoWeeks=[];
                                    if(!in_array($lastTwoWeekriverRoute->getDriver()->getId(),$rescuedIds)){
                                        $rescuerLastTwoWeeks = $this->em->getRepository(DriverRoute::class)->getRescuerLastTwoWeeks($station, $lastTwoWeekDataStartEndDay,$lastTwoWeekriverRoute->getDriver()->getId(),'week' );
                                        if(count($rescuerLastTwoWeeks) > 2 ){
                                            foreach($rescuerLastTwoWeeks as $rescuer){
                                                $rescuerRouteCodes = $rescuer->getDriverRouteCodes()->filter(function ($routeCode) {
                                                    return !$routeCode->getIsArchive();
                                                });
                                                $difference = $rescuer->getStartTime() ->diff($rescuer->getEndTime() );
                                                $employeeDetails['rescuedInLastTwoWeeks'][$lastTwoWeekriverRoute->getDriver()->getId()]['rescuer'][$rescuerRouteCodes[0]->getCode()][] = [
                                                    'name'=>$rescuer->getDriver()->getFullName(),
                                                    'date'=>$rescuer->getDateCreated(),
                                                    'time'=>$rescuer->getStartTime()->format('h:i A').' - '.$rescuer->getEndTime()->format('h:i A'),
                                                    'duration'=> $return_time = $difference ->format('%H:%I').' Hrs',
                                                    'package'=>$rescuer->getNumberOfPackage(),
                                                    'note'=>$rescuer->getNewNote(),
                                                    'code'=>$rescuerRouteCodes[0]->getCode()
                                                ];
                                            }
                                            $employeeDetails['rescuedInLastTwoWeeks'][$lastTwoWeekriverRoute->getDriver()->getId()]['name'] = $lastTwoWeekriverRoute->getDriver()->getFullName();
                                            $employeeDetails['rescuedInLastTwoWeeks'][$lastTwoWeekriverRoute->getDriver()->getId()]['id'] = $lastTwoWeekriverRoute->getDriver()->getId();
                                        }
                                    }
                                    $rescuedIds[] = $lastTwoWeekriverRoute->getDriver()->getId();

                                    if( $lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d') == $passedDate->format('Y-m-d') ){
                                        $rescuerLastTwoWeeks = $this->em->getRepository(DriverRoute::class)->getRescuerLastTwoWeeks($station, $passedDate->format('Y-m-d'),$lastTwoWeekriverRoute->getDriver()->getId(),'today' );
                                        if(count($rescuerLastTwoWeeks) > 0 ){
                                            foreach($rescuerLastTwoWeeks as $rescuer){
                                                $rescuerRouteCodes = $rescuer->getDriverRouteCodes()->filter(function ($routeCode) {
                                                    return !$routeCode->getIsArchive();
                                                });
                                                $difference = $rescuer->getStartTime() ->diff($rescuer->getEndTime() );
                                                $employeeDetails['rescuedInToday'][$lastTwoWeekriverRoute->getDriver()->getId()]['rescuer'][$rescuerRouteCodes[0]->getCode()][] = [
                                                    'name'=>$rescuer->getDriver()->getFullName(),
                                                    'date'=>$rescuer->getDateCreated(),
                                                    'time'=>$rescuer->getStartTime()->format('h:i A').' - '.$rescuer->getEndTime()->format('h:i A'),
                                                    'duration'=> $return_time = $difference ->format('%H:%I').' Hrs',
                                                    'package'=>$rescuer->getNumberOfPackage(),
                                                    'note'=>$rescuer->getNewNote(),
                                                    'code'=>$rescuerRouteCodes[0]->getCode()
                                                ];
                                            }
                                            $employeeDetails['rescuedInToday'][$lastTwoWeekriverRoute->getDriver()->getId()]['name'] = $lastTwoWeekriverRoute->getDriver()->getFullName();
                                        }
                                    }
                                }
                            }

                            if(($currentWeekStartEndDay['start'] <= $lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d')) && ($currentWeekStartEndDay['end'] >= $lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d'))){
                                if (!empty($lastTwoWeekriverRoute->getStartTime()) && !empty($lastTwoWeekriverRoute->getEndTime())) {
                                    $totalHourScheduleByDriver = $this->getHoursBetweenTwoTime($lastTwoWeekriverRoute->getStartTime(), $lastTwoWeekriverRoute->getEndTime());

                                    $totalHourSchedule['totalHourSchedule'][$lastTwoWeekriverRoute->getDriver()->getId()] = isset($totalHourSchedule['totalHourSchedule'][$lastTwoWeekriverRoute->getDriver()->getId()]) ? $totalHourSchedule['totalHourSchedule'][$lastTwoWeekriverRoute->getDriver()->getId()] + $totalHourScheduleByDriver : $totalHourScheduleByDriver;

                                    if(
                                        ($passedDate->format('Y-m-d') <= $lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d')) &&
                                        ($lastTwoWeekriverRoute->getDateCreated()->format('Y-m-d') <= $currentWeekStartEndDay['end'])
                                    ){
                                        $remainHours['remainHours'][$lastTwoWeekriverRoute->getDriver()->getId()] = isset($remainHours['remainHours'][$lastTwoWeekriverRoute->getDriver()->getId()]) ? $remainHours['remainHours'][$lastTwoWeekriverRoute->getDriver()->getId()] + $totalHourScheduleByDriver : $totalHourScheduleByDriver;
                                    }
                                }
                            }

                            if(isset($totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()])) {
                                if($totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] > 40) {
                                    $employeeDetails['driverInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['name'] =  $lastTwoWeekriverRoute->getDriver()->getFullName();
                                    $employeeDetails['driverInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['hoursSchedule'] = $totalHourSchedule['totalHourSchedule'][$lastTwoWeekriverRoute->getDriver()->getId()].' hours';
                                    $employeeDetails['driverInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['hoursWorked'] = $totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()].' hours';
                                    $employeeDetails['driverInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['daysWorked'] = $totalDaysWorked['totalDaysWorked'][$lastTwoWeekriverRoute->getDriver()->getId()];
                                }
                                $remainHoursByDriver = isset($remainHours['remainHours'][$lastTwoWeekriverRoute->getDriver()->getId()]) ? $remainHours['remainHours'][$lastTwoWeekriverRoute->getDriver()->getId()] : 0;
                                if(($totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()] + $remainHoursByDriver) > 40) {
                                    $employeeDetails['driverEnterInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['name'] =  $lastTwoWeekriverRoute->getDriver()->getFullName();
                                    $employeeDetails['driverEnterInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['hoursSchedule'] = $totalHourSchedule['totalHourSchedule'][$lastTwoWeekriverRoute->getDriver()->getId()].' hours';
                                    $employeeDetails['driverEnterInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['hoursWorked'] = $totalHourWorked['totalHourWorked'][$lastTwoWeekriverRoute->getDriver()->getId()].' hours';
                                    $employeeDetails['driverEnterInOverTime'][$lastTwoWeekriverRoute->getDriver()->getId()]['daysWorked'] = $totalDaysWorked['totalDaysWorked'][$lastTwoWeekriverRoute->getDriver()->getId()];
                                }
                            }
                        }

                        foreach($overTimeDrivers as $key => $overTimeDriver) {
                            $totalHourWorkedByWeekArray = $totalHourWorkedByWeek['totalHourWorkedByWeek'][$key];
                            $totalDaysWorkedByWeekArray = $totalDaysWorkedByWeek['totalDaysWorkedByWeek'][$key];
                            $totalOverTimeHourWorkedByWeekArray = $totalOverTimeHourWorkedByWeek['totalOverTimeHourWorkedByWeek'][$key];

                            $employeeDetails['employeeWithOvertimeTrend'][$key]['name'] = $overTimeDriver;
                            $employeeDetails['employeeWithOvertimeTrend'][$key]['overTimeNotes'] = array_sum($totalOverTimeHourWorkedByWeekArray)." overtime hours in last 2 weeks";
                            $employeeDetails['employeeWithOvertimeTrend'][$key]['avgHoursPerWeek'] = number_format(array_sum($totalHourWorkedByWeekArray) / 2,2);
                            $employeeDetails['employeeWithOvertimeTrend'][$key]['avgScheduleDayPerWeek'] = number_format(array_sum($totalDaysWorkedByWeekArray) / 2,0);
                        }

                        if (!isset($employeeDetails['rescuedInLastTwoWeeks'])) {
                            $employeeDetails['rescuedInLastTwoWeeks'] = [];
                        }
                        if (!isset($employeeDetails['rescuedInToday'])) {
                            $employeeDetails['rescuedInToday'] = [];
                        }

                        if (!isset($employeeDetails['driverInOverTime'])) {
                            $employeeDetails['driverInOverTime'] = [];
                        }
                        if (!isset($employeeDetails['driverEnterInOverTime'])) {
                            $employeeDetails['driverEnterInOverTime'] = [];
                        }
                        if (!isset($employeeDetails['employeeWithOvertimeTrend'])) {
                            $employeeDetails['employeeWithOvertimeTrend'] = [];
                        }
                        if (isset($routeDetails['shifts'])) {
                            foreach($routeDetails['shifts'] as $keyShift => $routeShift) {
                                $message = (isset($backupShiftMsg['backup']) && isset($backupShiftMsg['backup'][$keyShift])) ? '('.$backupShiftMsg['backup'][$keyShift].') were sent home ' : '';
                                $message .= (isset($invoiceTypeShift['invoiceType']) && isset($invoiceTypeShift['invoiceType'][$keyShift])) ? '('.$invoiceTypeShift['invoiceType'][$keyShift].') no inovice type ' : '';
                                $message .= (isset($noRouteCode['noRouteCode']) && isset($noRouteCode['noRouteCode'][$keyShift])) ? '('.$noRouteCode['noRouteCode'][$keyShift].') no route code ' : '';
                                $message .= (isset($duplicateRouteCode['duplicateRouteCode']) && isset($duplicateRouteCode['duplicateRouteCode'][$keyShift])) ? '('.$duplicateRouteCode['duplicateRouteCode'][$keyShift].') duplicate code ' : '';
                                $routeDetails['shifts'][$keyShift] = $routeShift. ' '.$message;
                            }
                        }

                        $routeDetails['date'] = $opts->date;
                        $data['routeDetails'] = $routeDetails;
                        $data['quickReview'] = $quickReview;
                        $data['employeeDetails'] = $employeeDetails;
                        $data['station'] = $station;
                        $data['stationParam'] = $stationParam;
                        $html = $this->twig->render('emails/dailySummary.twig',$data);
                        
                        // if station not then send email
                        if ($stationParam == '') {
                            foreach ($emails as $email) {
                                // @var $email BaseController
                                $this->emailService->sendEmail($email, 'Daily Summary for Station - '.$station->getName() . ' (Company - '.$company->getName().') - ' . $opts->date, $html);
                                $io->success('Daily Summary for Station "'.$station->getName() . '" (Company "'.$company->getName().'") sent successfully', true);
                            }
                        }

                        $dailySummary = $this->em->getRepository(DailyDriverSummary::class)->findOneBy( ['station'=> $station->getId(),'summaryDate'=>new \DateTime($opts->date)]);
                        if ($dailySummary) {

                            $data['stationParam'] = $station->getId();
                            $pdfTwig = $this->twig->render('emails/dailySummary.twig',$data);
                            // Generate PDF File
                            $pdfOptions = new Options();
                            $pdfOptions->set('defaultFont', 'Arial');
                            $dompdf = new Dompdf($pdfOptions);

                            $dompdf->loadHtml($pdfTwig);
                            $dompdf->setPaper('A4', 'portrait');
                            $dompdf->render();

                            // Save PDF File
                            $output = $dompdf->output();
                            $fileName = $station->getId().'-'.time().'_daily-summary.pdf';
                            $publicDirectory = $this->path . '/public/excelsheet/'.$fileName;

                            file_put_contents($publicDirectory, $output);

                            // Upload PDF File
                            $s3result = $this->s3->uploadFile($publicDirectory, $fileName, 'dsp.data.storage.general');
                            if (is_string($s3result)) {
                                throw new Exception($s3result);
                            }
                            // Get uploaded file url
                            $logS3FilePath = $s3result['ObjectURL'];

                            //Update Url in database
                            if($logS3FilePath){
                                $dailySummary->setReportUrl($logS3FilePath);
                                $this->em->persist($dailySummary);
                                $this->em->flush($dailySummary);
                            }
                            // Delete PDF File
                            unlink($publicDirectory);
                        }
                    }
                }
            }
        }

        $io->newLine();
        $io->success('Done', true);
    }

    public function getHoursBetweenTwoTime($startTime, $endTime) {
        $interval = date_diff($startTime, $endTime);
        $minutes = (($interval->h * 60) + $interval->i);

        $hourWorked = floor($minutes / 60);
        $minutesWorked = ($hourWorked % 60);
        $totalHours = (float) $hourWorked . (($minutesWorked > 0) ? '.' . round(($minutesWorked / 60) * 100) : '');
        $totalHours = (float) sprintf('%g', $totalHours);

        return $totalHours;
    }
}
