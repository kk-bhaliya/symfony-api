<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Device;


class ImportDeviceDataCommand  extends Command
{
    protected static $defaultName = 'app:import-device';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('file', InputArgument::OPTIONAL, 'Enter file name days')
            ->addArgument('company', InputArgument::OPTIONAL, 'Enter company ID')
            ->addArgument('station', InputArgument::OPTIONAL, 'Enter station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $fileName = 'public/csv/device/'.$input->getArgument('file');
        $company = $input->getArgument('company');
        $station = $input->getArgument('station');

        if (file_exists($fileName)) {
            try {
                $file = fopen($fileName, "r");
                $firstRow = true;
                $cnt = 0;
                while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                    if($cnt > 1){
                        $device = new Device();
                        $device->setCompany($this->em->getRepository('App\Entity\Company')->find($company));
                        $device->setStation($this->em->getRepository('App\Entity\Station')->find($station));
                        $device->setName($column[0]);
                        $device->setCarrier($column[1]);
                        $device->setUsername($column[2]);
                        $device->setPlatform(strtolower($column[3]));
                        $device->setAssetTag($column[4]);
                        $device->setManufacturer(ucfirst($column[5]));                        
                        $device->setModel($column[6]);
                        $device->setOsVersion($column[7]);
                        $device->setMdmClient($column[8] == 'Yes' ? true : false);
                        $device->setDeviceId($column[9]);
                        $device->setDeviceUid( isset($column[9]) ? $column[9] : 0);                         
                        $device->setNumber($column[10]);
                        $device->setImei($column[11]);                        
                        $device->setDeviceCondition($column[16]);
                        $device->setIsArchive(false);
                        $device->setAllowKickoff(false);
                        $device->setIsPersonalDevice(false);
                        $this->em->persist($device);
                        $this->em->flush();
                    }                        
                    $cnt++;  
                }   
                $io->success($cnt.' record has been inserted successfully'); 
            } catch (Exception $e) {
                $io->warning('Error.'.$e);
            }
        } else {
            $io->warning('file not found.');
        }    
        
    }
}
