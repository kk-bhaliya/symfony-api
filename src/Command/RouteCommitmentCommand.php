<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\DriverRoute;
use App\Entity\RouteCommitment;
use Doctrine\ORM\Query;

class RouteCommitmentCommand extends Command
{
    protected static $defaultName = 'app:route-commitment';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('week',InputArgument::OPTIONAL,'Enter week')
             ->addArgument('year',InputArgument::OPTIONAL,'Enter year')
             ->addArgument('company',InputArgument::OPTIONAL,'Enter company');
    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $company = $input->getArgument('company');
        $week = $input->getArgument('week');
        $year = $input->getArgument('year');
        //$shiftArrayByWeek = [];
        $routeCommitments = $this->em->getRepository('App\Entity\RouteCommitment')->getRouteCommitmentCount($week, $year, $company);
        
        foreach ($routeCommitments as $routeCommitment) {

            //[$routeCommitment->getWeek().'-'.$routeCommitment->getYear()][$routeCommitment->getShiftType()->getId()] = $routeCommitment->getShiftType()->getId();

            $driverRouteWithDriver = $this->em->getRepository('App\Entity\DriverRoute')->getDriverRouteWithDriverCount($routeCommitment->getCompany()->getId(), $routeCommitment->getShiftType()->getId(), $routeCommitment->getWeekDate());

            $driverRouteWithoutDriver = $this->em->getRepository('App\Entity\DriverRoute')->getDriverRouteWithoutDriverCount($routeCommitment->getCompany()->getId(), $routeCommitment->getShiftType()->getId(), $routeCommitment->getWeekDate());

            $tempDriverWeekDate = $routeCommitment->getWeekDate()->format('Y-m-d') .' 00:00:00';

            $driverWeekDate = $routeCommitment->getWeekDate()->format('Y-m-d');
            
            //if ((int)$routeCommitment->getRoute() < (int)$driverRouteWithDriver) {
            //    $this->em->getRepository('App\Entity\TempDriverRoute')->removeDriverRoute($tempDriverWeekDate, $driverWeekDate, $routeCommitment->getShiftType()->getId(), $routeCommitment->getCompany()->getId(), $limit = '');
            //} else {
                if (((int)$routeCommitment->getRoute() - $driverRouteWithDriver - $driverRouteWithoutDriver) > 0) {
                    $countRoute = (int)$routeCommitment->getRoute() - $driverRouteWithDriver - $driverRouteWithoutDriver;
                    for($nRouteCount = 0; $nRouteCount < $countRoute; $nRouteCount++) {
                        $currentDate = date("Y-m-d");
                        if($routeCommitment->getWeekDate()->format('Y-m-d') >= $currentDate){
                            $driverRoute = new DriverRoute();
                            $driverRoute->setDateCreated(new \DateTime($routeCommitment->getWeekDate()->format('Y-m-d')));
                            $driverRoute->setStation($routeCommitment->getStation());
                            $driverRoute->setShiftType($routeCommitment->getShiftType());
                            $driverRoute->setSkill($routeCommitment->getShiftType()->getSkill());
                            $driverRoute->setIsOpenShift(1);
                            $this->em->persist($driverRoute);
                            $this->em->flush($driverRoute);
                        }
                    }

                } 
                //elseif (((int)$routeCommitment->getRoute() - $driverRouteWithDriver - $driverRouteWithoutDriver) < 0) {
                //        $limit = abs($routeCommitment->getRoute() - $driverRouteWithDriver - $driverRouteWithoutDriver);
                //        $this->em->getRepository('App\Entity\TempDriverRoute')->removeDriverRoute($tempDriverWeekDate, $driverWeekDate, $routeCommitment->getShiftType()->getId(), $routeCommitment->getCompany()->getId(), $limit);
                //}
            //}
        }
        
        // foreach ($shiftArrayByWeek as $week => $shiftAsWeek) {
        //     $weekYear = explode("-", $week);
        //     $driverRouteByShifts = $this->em->getRepository('App\Entity\DriverRoute')->getDriverRouteByWeekUnmatchShift($weekYear[0], array_values($shiftAsWeek), $weekYear[1]);
        //     foreach ($driverRouteByShifts as $driverRouteByShift) {
        //         $removeDriverRouteNotInTempDriver = $this->em->getRepository('App\Entity\TempDriverRoute')->findBy(["routeId" => $driverRouteByShift->getId(), "isNew" => 1]);
        //         if (empty($removeDriverRouteNotInTempDriver)) {
        //             $this->em->remove($driverRouteByShift);
        //             $this->em->flush();
        //         }
        //     }
        // }
        $io->success('Route commitment command run successfully.');
    }
}
