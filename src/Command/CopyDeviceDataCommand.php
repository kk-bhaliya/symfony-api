<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Device;


class CopyDeviceDataCommand  extends Command
{
    protected static $defaultName = 'app:copy-device';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('oldId', InputArgument::OPTIONAL, 'Enter old company ID')
            ->addArgument('newId', InputArgument::OPTIONAL, 'Enter new company ID')
            ->addArgument('stationId', InputArgument::OPTIONAL, 'Enter new station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $oldId = $input->getArgument('oldId');
        $newId = $input->getArgument('newId');
        $stationId = $input->getArgument('stationId');
        
        $oldCompany = $this->em->getRepository('App\Entity\Company')->find($oldId);
        $newCompany = $this->em->getRepository('App\Entity\Company')->find($newId);
        $station = $this->em->getRepository('App\Entity\Station')->find($stationId);

        if($oldCompany && $newCompany){
            $cnt=1;
            foreach ($oldCompany->getDevices() as $device) {            
                $newDevice = new Device();
                $newDevice->setCompany($newCompany);
                $newDevice->setStation($station);
                $newDevice->setName($device->getName());
                $newDevice->setCarrier($device->getCarrier());
                $newDevice->setUsername($device->getUsername());
                $newDevice->setPlatform($device->getPlatform());
                $newDevice->setAssetTag($device->getAssetTag());
                $newDevice->setManufacturer($device->getManufacturer());  
                $newDevice->setModel($device->getModel());
                $newDevice->setOsVersion($device->getOsVersion());
                $newDevice->setMdmClient($device->getMdmClient());
                $newDevice->setDeviceId($device->getDeviceId());
                $newDevice->setDeviceUid($device->getDeviceUid());
                $newDevice->setNumber($device->getNumber());
                $newDevice->setImei($device->getImei());  
                $newDevice->setDeviceCondition($device->getDeviceCondition());
                $newDevice->setIsArchive($device->getIsArchive());
                $newDevice->setAllowKickoff($device->getAllowKickoff());
                $newDevice->setIsPersonalDevice($device->getIsPersonalDevice());
                $this->em->persist($newDevice);
                $this->em->flush();
                $cnt++;
            }
            $io->success($cnt.' record has been copied successfully'); 
        } else {
            $io->warning('Company Not found.');
        }      
    }
}
