<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Vehicle;


class CopyVehicleDataCommand  extends Command
{
    protected static $defaultName = 'app:copy-vehicle';

    private $em;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->addArgument('oldId', InputArgument::OPTIONAL, 'Enter old company ID')
            ->addArgument('newId', InputArgument::OPTIONAL, 'Enter new company ID')
            ->addArgument('stationId', InputArgument::OPTIONAL, 'Enter new station ID');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $oldId = $input->getArgument('oldId');
        $newId = $input->getArgument('newId');
        $stationId = $input->getArgument('stationId');
        
        $oldCompany = $this->em->getRepository('App\Entity\Company')->find($oldId);
        $newCompany = $this->em->getRepository('App\Entity\Company')->find($newId);
        $station = $this->em->getRepository('App\Entity\Station')->find($stationId);

        if($oldCompany && $newCompany){
            $cnt=1;
            foreach ($oldCompany->getVehicles() as $vehicle) { 
                 $newVehicle = new Vehicle();
                 $newVehicle->setStation($station);
                 $newVehicle->setCompany($newCompany);
                 $newVehicle->setStatus($vehicle->getStatus());
                 $newVehicle->setVehicleId($vehicle->getVehicleId());
                 $newVehicle->setLocation($vehicle->getLocation());   
                 $newVehicle->setNotes($vehicle->getNotes());
                 $newVehicle->setVin($vehicle->getVin());
                 $newVehicle->setState($vehicle->getState());
                 $newVehicle->setLicensePlate($vehicle->getLicensePlate());                         
                 $newVehicle->setLicensePlateExpiration($vehicle->getLicensePlateExpiration());
                 $newVehicle->setMake($vehicle->getMake());
                 $newVehicle->setModel($vehicle->getModel());
                 $newVehicle->setModelType($vehicle->getModelType());
                 $newVehicle->setYear($vehicle->getYear());
                 $newVehicle->setHeight($vehicle->getHeight());
                 $newVehicle->setFuelCardNumber($vehicle->getFuelCardNumber());
                 $newVehicle->setDateEnd($vehicle->getDateEnd());
                 $newVehicle->setInsuranceCompany($vehicle->getInsuranceCompany());
                 $newVehicle->setPolicyNumber($vehicle->getPolicyNumber());
                 $newVehicle->setTankCapacity($vehicle->getTankCapacity());
                 $newVehicle->setGasType($vehicle->getGasType());
                 $newVehicle->setIsOwned($vehicle->getIsOwned());
                 $newVehicle->setIsArchive($vehicle->getIsArchive());
                 $newVehicle->setIsEnabled($vehicle->getIsEnabled());                 
                 $this->em->persist($newVehicle);
                 $this->em->flush(); 
                 $cnt++;
            }
            $io->success($cnt.' record has been copied successfully');    
        } else {
            $io->warning('Company Not found.');
        }   
        
    }
}
