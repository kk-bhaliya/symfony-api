<?php


namespace App\Services;


class SingleResult
{
    /**
     * @var bool
     */
    private $success;

    /**
     * @var string
     */
    private $data;

    /**
     * @var string
     */
    private $nameOfResult;


    public function __construct(bool $success = false, string $data = '', $nameOfResult = 'result')
    {
        $this->success = $success;
        $this->data = $data;
        $this->nameOfResult = $nameOfResult;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @return string
     */
    public function getData(): string
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData(string $data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getFormatData()
    {
        return ['success'=> $this->isSuccess(), $this->nameOfResult => $this->data];
    }




}