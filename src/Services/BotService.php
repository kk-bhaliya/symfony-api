<?php


namespace App\Services;


use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverSkill;
use App\Entity\Event;
use App\Entity\Station;
use App\Entity\Task;
use App\Entity\User;
use App\Worker\BotServiceWorker;
use Aws\Lambda\LambdaClient;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class BotService
{
    /**
     * @var TwilioAccountService
     */
    private $twilioAccountService;
    /**
     * @var TokenStorageInterface
     */
    private $token;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var PlivoService
     */
    private $plivoService;
    /**
     * @var Result
     */
    private $result;
    /**
     * @var DriverService
     */
    private $driverService;
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var StationService
     */
    private $stationService;
    /**
     * @var TwilioUtil
     */
    private $twilioUtil;
    /**
     * @var ParameterBagInterface
     */
    private $bag;
    /**
     * @var EntityManagerInterface
     */
    private $em;


    /**
     * BotService constructor.
     * @param EntityManagerInterface $manager
     * @param TwilioAccountService $twilioAccountService
     * @param TokenStorageInterface $token
     * @param PlivoService $plivoService
     * @param DriverService $driverService
     * @param UserService $userService
     * @param StationService $stationService
     * @param TwilioUtil $twilioUtil
     * @param Result $result
     * @param ParameterBagInterface $bag
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $manager,
        TwilioAccountService $twilioAccountService,
        TokenStorageInterface $token,
        PlivoService $plivoService,
        DriverService $driverService,
        UserService $userService,
        StationService $stationService,
        TwilioUtil $twilioUtil,
        Result $result,
        ParameterBagInterface $bag,
        EntityManagerInterface $em

    )
    {

        $this->twilioAccountService = $twilioAccountService;
        $this->token = $token;
        $this->manager = $manager;
        $this->plivoService = $plivoService;
        $this->result = $result;
        $this->driverService = $driverService;
        $this->userService = $userService;
        $this->stationService = $stationService;
        $this->twilioUtil = $twilioUtil;
        $this->bag = $bag;
        $this->em = $em;

    }

    /**
     * @param Company $company
     * @return mixed
     * @throws Exception
     */
    public function dataForTwilioAuthentication(Company $company)
    {
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        if (empty($data['accountSid'])) {
            throw new Exception('accountSid field is missing in Company Entity');
        }
        if (empty($data['authToken'])) {
            throw new Exception('authToken field is missing in Company Entity');
        }
        if (empty($data['serviceSid'])) {
            throw new Exception('serviceSid field is missing in Company Entity');
        }
        return $data;
    }

    /**
     * @param array $data
     * @param array $requestData
     * @return array
     */
    public function getData(array $data, array $requestData)
    {
        try {
            $previousRecord = empty($requestData['previousRecord']) ? null : $requestData['previousRecord'];
            $updatedRecord = empty($requestData['updatedRecord']) ? null : $requestData['updatedRecord'];
            $notification = empty($requestData['notification']) ? null : $requestData['notification'];
            $result = [];
            if (!empty($data['created'])) {
                foreach ($data['created'] as $entityUniqueIdentifier => $valueArray) {
                    if (!empty($valueArray['entityName']) && !empty($valueArray['entityId'])) {
                        if ($valueArray['entityName'] === 'Station') {
                            $result['stationHandler'] = $this->stationHandler($valueArray['entityId']);
                        }
                        if ($valueArray['entityName'] === 'Driver') {
                            $result['driverCreationHandler'] = $this->driverCreationHandler($valueArray['entityId']);
                        }
                    }
                }
            }

            if (!empty($data['updates']) && $data['code'] === 200) {
                foreach ($data['updates'] as $key => $value) {
                    if ($value['entityName'] === 'User') {
                        $result['userUpdateHandler'] = $this->userUpdateHandler($value['entityId'], $value['fields']);
                    }
                }
            }

            return $result;
        } catch (Exception $exception) {
            $exceptionResult = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
            return $exceptionResult->getFormatData();
        }
    }

    /**
     * @param int $entityId
     * @param array $fields
     * @return array
     * @throws Exception
     */
    public function userUpdateHandler(int $entityId, array $fields)
    {
        try {
            if (count($fields) < 1) {
                return [];
            }
            $result['updateUser'] = $this->userService->updateUserForTwilio($entityId, $this->twilioUtil, $this->twilioAccountService);
            $result['input'] = $fields;
            return $result;
        } catch (Exception $exception) {
            $exceptionResult = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
            return $exceptionResult->getFormatData();
        } catch (Error $error) {
            $errorResult = new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
            return $errorResult->getFormatData();
        }
    }

    /**
     * @param int $entityId
     * @return mixed
     */
    public function driverCreationHandler(int $entityId)
    {
        try {
            $driver = $this->manager->find(Driver::class, $entityId);
            $user = $driver->getUser();
            $userAndDriverVerification = $this->userAndDriverVerification($user, $driver);
            if ($driver instanceof Driver && $user instanceof User) {
                $this->syncUserDriver($user, $driver);
                $driverResults['userCreation'] = $this->userService->createUserForChat($user, $this->twilioAccountService)->getFormatData();
                $workplaceBotChannelCreation = $this->userService->setUpWorkplaceBotChannel($user, $this->twilioAccountService, $this->twilioUtil);
                $driverResults['workplaceBotChannelCreation'] = $workplaceBotChannelCreation->getFormatData();
                $driverResults['managerChannelCreation'] = $this->userService->createChannelWithManager($user, $this->twilioAccountService, $this->twilioUtil)->getFormatData();
                return [$driverResults, $userAndDriverVerification];
            }
            $issue = new Result(false,
                [
                    'isDriver' => $driver instanceof Driver,
                    'isUser' => $driver->getUser() instanceof User,
                    'entityIdReceived' => $entityId,
                    'userAndDriverVerification' => $userAndDriverVerification,
                ], 'error', 'Error in driverCreationHandler()', true);
            return $issue->getFormatData();
        } catch (Exception $exception) {
            $e = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
            return $e->getFormatData();
        } catch (Error $error) {
            $e = new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
            return $e->getFormatData();
        }
    }

    public function syncUserDriver(User $user, Driver $driver)
    {
        if (!($user->getDriver() instanceof Driver)) {
            $user->setDriver($driver);
            $this->manager->flush();
        }
    }

    public function userAndDriverVerification(User $user, Driver $driver)
    {
        return [
            'userId' => $user instanceof User ? $user->getId() : $user,
            'driverFromUser' => $user instanceof User && $user->getDriver() instanceof Driver ? $user->getDriver()->getId() : null,
            'driverId' => $driver instanceof Driver ? $driver->getId() : $driver,
            'userFromDriver' => $driver instanceof Driver && $driver->getUser() instanceof User ? $driver->getUser()->getId() : $user,
        ];
    }

    /**
     * @param Driver $driver
     * @return array
     */
    public function driverCreationHandlerForImport(Driver $driver)
    {
        try {
            $user = $driver->getUser();
            $userAndDriverVerification = $this->userAndDriverVerification($user, $driver);
            if ($driver instanceof Driver && $user instanceof User) {
                $this->syncUserDriver($user, $driver);
                $driverResults['userCreation'] = $this->userService->createUserForChat($user, $this->twilioAccountService)->getFormatData();
                $workplaceBotChannelCreation = $this->userService->setUpWorkplaceBotChannel($user, $this->twilioAccountService, $this->twilioUtil);
                $driverResults['workplaceBotChannelCreation'] = $workplaceBotChannelCreation->getFormatData();
                $userAndDriverVerification['chatIdentifier'] = $user->getChatIdentifier();
                $driverResults['managerChannelCreation'] = $this->userService->createChannelWithManager($user, $this->twilioAccountService, $this->twilioUtil)->getFormatData();
                return [$driverResults, $userAndDriverVerification];
            }
            $issue = new Result(false,
                [
                    'isDriver' => $driver instanceof Driver,
                    'isUser' => $driver->getUser() instanceof User,
                    'entityIdReceived' => $driver->getId(),
                    'userAndDriverVerification' => $userAndDriverVerification,
                ], 'error', 'Error in driverCreationHandler()', true);
            return $issue->getFormatData();
        } catch (Exception $exception) {
            $e = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
            return $e->getFormatData();
        } catch (Error $error) {
            $e = new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
            return $e->getFormatData();
        }
    }

    /**
     * @return string
     * @throws Exception
     */
    public function createUniqueIdentifier()
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function getIdentity(User $user)
    {
        $identity = $user->getIdentifier();
        if ($user->getChatIdentifier() === null) {
            $user->setChatIdentifier($identity . '-' . $this->createUniqueIdentifier());
            $this->manager->flush();
        }
        return $user->getChatIdentifier();
    }

    /**
     * @param User $user
     * @return Result
     * @throws Exception
     */
    public function createUserForChat(User $user)
    {
        $dataAuthentication = $this->dataForTwilioAuthentication($user->getCompany());
        $dataUser = [];
        $dataUser['identity'] = $this->getIdentity($user);
        $dataUser['options']['friendlyName'] = $user->getFriendlyName();
        $dataUser['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'email' => $user->getEmail(),
                'role' => $user->getRoleForChat()
            ];
        $dataUser = array_merge($dataUser, $dataAuthentication);
        $resultUser = $this->twilioAccountService->createUserResource($dataUser);
        if ($resultUser->isSuccess()) {
            if ($resultUser->getData()['sid'] === null) {
                throw new Exception('UserSid is null, error in user chat creation');
            }
            $user->setUserSid($resultUser->getData()['sid']);
            $this->manager->flush();
        }
        return $resultUser;
    }

    /**
     * @param Driver $driver
     * @return Result
     */
    public function createChannelWithManager(Driver $driver)
    {
        try {
            $duoChannels = $this->driverService->dataForDuoChannelManager($driver);
            $channelResult = [];
            foreach ($duoChannels as $duoChannelData) {
                $channelResult[] = $this->createChannelWithUsers($duoChannelData)->getFormatData();
            }
            return new Result(true,
                [
                    'createChannelWithManager' => $channelResult
                ], 'channelWithManager', 'Result for duo channel with Manager');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'error', 'Error in createChannelWithManager()');
        }
    }

    /**
     * @param int $entityId
     * @return Result
     */
    public function stationHandler(int $entityId)
    {
        try {
            $station = $this->manager->find(Station::class, $entityId);
            /** @var User $user */
            $user = $this->token->getToken()->getUser();
            $stationChannel = $this->stationService->createStationChannel($station, $user, $this->twilioAccountService, $this->twilioUtil);
            $stationMemberResult = $this->stationService->addDriverToStationChannel($station, $user, $this->twilioAccountService, $this->twilioUtil);
            return new Result(true, ['stationChannel' => $stationChannel, 'stationMemberResult' => $stationMemberResult,], 'station');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
        } catch (Error $error) {
            return new Result(false, $error->getTrace(), 'error', $error->getMessage());
        }
    }

    /**
     * @param $data
     * @param string $key
     * @return Company
     * @throws Exception
     */
    public function getCompanyEntity($data, $key = 'companyId')
    {
        if (is_array($data) && empty($data[$key])) {
            throw new Exception('data and/or key field missing to find Company');
        }
        if (!is_array($data) && empty($data)) {
            throw new Exception('data and fields missing');
        }
        $value = is_array($data) ? $data[$key] : $data;
        if ($key === 'companyId') {
            $key = 'id';
        }

        $company = $this->manager->getRepository(Company::class)->findOneBy([$key => $value]);
        if ($company instanceof Company) {
            return $company;
        }

        throw new Exception('Company does not exist');
    }


    /**
     * @param $data
     * @param string $key
     * @return User
     * @throws Exception
     */
    public function getUserEntity($data, $key = 'userId')
    {
        if (is_array($data) && empty($data[$key])) {
            throw new Exception('data and key field missing');
        }
        if (!is_array($data) && empty($data)) {
            throw new Exception('data field missing');
        }
        $value = is_array($data) ? $data[$key] : $data;
        if ($key === 'userId') {
            $key = 'id';
        }
        $user = $this->manager->getRepository(User::class)->findOneBy([$key => $value]);
        if ($user instanceof User) {
            return $user;
        }

        throw new Exception('User does not exist');
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function dataForChannel(array $data)
    {
        if (empty($data['options']['friendlyName'])) {
            if (!is_string($data['friendlyName'])) {
                throw new Exception('friendlyName input is not valid');
            }
            throw new Exception('friendlyName input is not valid');
        }

        return $data;
    }

    public function dataForMember(User $user, string $channelSid)
    {
        $data['channelSid'] = $channelSid;
        $data['identity'] = $user->getChatIdentifier();
        $data['options'] = [];
        $data['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat(),
            ];
        return $data;
    }

    /**
     * @param User $user1
     * @param User $user2
     * @return array
     * @throws Exception
     */
    public function dataForDuoChannel(User $user1, User $user2)
    {
        if ($user1->getCompany()->getId() !== $user2->getCompany()->getId()) {
            throw new Exception('Users are not in the same company');
        }
        $duoChannelData['companyId'] = $user1->getCompany()->getId();
        $duoChannelData['options']['attributes'] = [
            "memberCount" => 2,
            "kind" => "Duo",
            "nameOfChannel" => $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier(),
        ];
        $duoChannelData['options']['friendlyName'] = $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier();
        $duoChannelData['members'] = [
            [
                'userId' => $user1->getId(),
            ],
            [
                'userId' => $user2->getId()
            ]
        ];
        return $duoChannelData;
    }

    /**
     * @param array $data
     * @return Result
     * @throws Exception
     */
    public function createChannelWithUsers(array $data)
    {
        $company = $this->getCompanyEntity($data);
        $dataAuth = $this->dataForTwilioAuthentication($company);

        $validMembers = $this->getValidMembers($data);
        $issues = $validMembers['issues'];
        $members = $validMembers['members'];

        $dataChannel = $this->dataForChannel($data);
        $dataChannel = array_merge($dataChannel, $dataAuth);

        $resultChannel = $this->twilioAccountService->createChannelResource($dataChannel);
        if ($resultChannel->isSuccess() === false) {
            return $resultChannel;
        }
        $channelSid = $resultChannel->getData()['sid'];

        $membersResult = [];
        foreach ($members as $member) {
            $dataMember = $this->dataForMember($member, $channelSid);
            $dataMember = array_merge($dataMember, $dataAuth);
            $memberResult = $this->twilioAccountService->createMemberResource($dataMember);
            $membersResult[] = $memberResult->getFormatData();
        }

        return new Result(true,
            [
                'channelSid' => $channelSid,
                'channel' => $resultChannel->getFormatData(),
                'members' => $membersResult,
                'issues' => $issues
            ], 'channel', 'Channel with members created.', true);

    }

    public function addDriverToNotificationChannel(Driver $driver)
    {
        try {
            $channelData = $this->driverService->addDriverToNotificationChannelData($driver);
            $channelResult = $this->createChannelWithUsers($channelData);
            if ($channelResult->isSuccess()) {
                $driver->getUser()->setNotificationsChannelSid($channelResult->getFormatData()['channel']['channelSid']);
                $this->manager->flush();
            }
            return $channelResult;
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'error', 'Error in addDriverToNotificationChannel()');
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getValidMembers(array $data)
    {
        $issues = [];
        $members = [];
        foreach ($data['members'] as $member) {
            $user = $this->getUserEntity($member);
            if ($user->getChatIdentifier() !== null) {
                $members[] = $user;
            } else {
                $issues[] = ['userId' => $user->getId()];
            }
        }

        return ['members' => $members, 'issues' => $issues];
    }

    /**
     * @param array $data
     * @return Result
     * @throws Exception
     */
    public function addUsersToExistingChannel(array $data)
    {
        if (empty($data['channelSid'])) {
            throw new Exception('channelSid field missing or empty');
        }
        $channelSid = $data['channelSid'];

        $company = $this->getCompanyEntity($data);
        $dataAuth = $this->dataForTwilioAuthentication($company);

        $validMembers = $this->getValidMembers($data);
        $issues = $validMembers['issues'];
        $members = $validMembers['members'];

        $membersResult = [];
        foreach ($members as $member) {
            $dataMember = $this->dataForMember($member, $channelSid);
            $dataMember = array_merge($dataMember, $dataAuth);
            $memberResult = $this->twilioAccountService->createMemberResource($dataMember);
            $membersResult[] = $memberResult->getFormatData();
        }

        return new Result(true,
            [
                'channel' => $channelSid,
                'members' => $membersResult,
                'issues' => $issues
            ], 'channel', 'Added users to channel', true);

    }

    /**
     * @param Driver $driver
     * @return Result
     * @throws Exception
     */
    public function addDriverToStationChannel(Driver $driver)
    {
        if ($driver->getStations()[0] instanceof Station) {
            $userId = $driver->getUser()->getId();
            $data['companyId'] = $driver->getCompany()->getId();
            $data['channelSid'] = $driver->getStations()[0]->getChannelSid();
            $data['members'] = [['userId' => $userId]];
            $data['accountSid'] = $driver->getCompany()->getAccountSid();
            $data['authToken'] = $driver->getCompany()->getAuthToken();
            $data['serviceSid'] = $driver->getCompany()->getServiceSid();
            return $this->addUsersToExistingChannel($data);
        }
        throw new Exception('Station not found');
    }


    /**
     * @param array $data
     * @return Result
     * @throws Exception
     */
    public function fetchMembersByChannel(array $data)
    {
        $company = $this->getCompanyEntity($data);
        $dataAuth = $this->dataForTwilioAuthentication($company);
        $data = array_merge($data, $dataAuth);
        return $this->twilioAccountService->readMultipleMembersResources($data);
    }

    /**
     * @param array $data
     * @return Result
     * @throws Exception
     */
    public function fetchUsersByChannel(array $data)
    {
        $members = $this->fetchMembersByChannel($data)->getData();
        $users = [];
        foreach ($members as $member) {
            $user = $this->manager->getRepository(User::class)->findOneBy(['chatIdentifier' => $member['identity']]);
            if ($user instanceof User &&
                $user->getChatIdentifier() !== null &&
                $user->getDriver() !== null &&
                $user->getDriver()->getStations()->count() > 0 &&
                $user->getDriver()->getDriverSkills()->count() > 0
            ) {
                $users[] = $this->userInformationToSend($user);
            }
        }

        return new Result(true, $users, 'users', 'Users fetched by channel', true);
    }

    /**
     * @param array $data
     * @param string $version
     * @return Result
     * @throws Exception
     */
    public function fetchTwilioUsers(array $data, string $version = "1")
    {
        if (empty($data['userId'])) {
            throw new Exception('userId field missing');
        }
        if (isset($data['orderBy'])) {
            $data['orderBy'] = json_decode($data['orderBy'], true, JSON_THROW_ON_ERROR);
        }
        if (empty($data['orderBy'])) {
            $data['orderBy'] = ['id' => 'DESC'];
        }
        if (empty($data['offset'])) {
            $data['offset'] = 0;
        }
        if (empty($data['limit'])) {
            $data['limit'] = 3000;
        }

        $user = $this->getUserEntity($data);
        $company = $this->getCompanyEntity($data);
        $this->manager->beginTransaction();
        if ($version === "2") {
            $users = $this->getTwilioUsersV2($company->getId(), $user->getId(), $data['orderBy'], $data['limit'], $data['offset']);
        } else {
            $users = $this->getTwilioUsers($company->getId(), $user->getId(), $data['orderBy'], $data['limit'], $data['offset']);
        }

        $this->manager->commit();
        return new Result(true, $users, 'users', 'Twilio users fetched', true);

    }

    public function getTwilioUsers(int $companyId, int $currentUserId, ?array $orderBy = ['id' => 'DESC'], ?int $limit = 3000, ?int $offset = 0)
    {
        if (is_null($orderBy)) {
            $orderBy = ['id' => 'ASC'];
        }
        if (is_null($limit)) {
            $limit = 3000;
        }
        if (is_null($offset)) {
            $offset = 0;
        }

        $users = $this->manager
            ->getRepository(User::class)
            ->findBy(['company' => $companyId, 'isArchive' => false], $orderBy, $limit, $offset);
        $list = [];
        foreach ($users as $user) {
            /** @var $user User */
            if ($user->getId() !== $currentUserId &&
                $user->getChatIdentifier() !== null &&
                $user->getDriver() !== null &&
                $user->getDriver()->getStations()->count() > 0 &&
                $user->getDriver()->getDriverSkills()->count() > 0
            ) {
                $list[] = $this->userInformationToSend($user);
            }
        }
        return $list;
    }

    public function getTwilioUsersV2(int $companyId, int $currentUserId, ?array $orderBy = ['id' => 'DESC'], ?int $limit = 3000, ?int $offset = 0)
    {
        if (is_null($orderBy)) {
            $orderBy = ['id' => 'ASC'];
        }
        if (is_null($limit)) {
            $limit = 3000;
        }
        if (is_null($offset)) {
            $offset = 0;
        }

        $users = $this->manager
            ->getRepository(User::class)
            ->findBy(['company' => $companyId, 'isArchive' => false], $orderBy, $limit, $offset);
        $list = [];
        foreach ($users as $user) {
            /** @var $user User */
            if ($user->getId() !== $currentUserId &&
                $user->getChatIdentifier() !== null &&
                $user->getDriver() !== null &&
                $user->getDriver()->getStations()->count() > 0 &&
                $user->getDriver()->getDriverSkills()->count() > 0
            ) {
                $list[] = $this->userSmallInformationToSend($user);
            }
        }
        return $list;
    }


    public function userInformationToSend(User $user)
    {
        return [
            'userId' => $user->getId(),
            'identity' => $user->getChatIdentifier(),
            'role' => $user->getRoleForChat(),
            'roleUnderscore' => str_replace(" ", "_", $user->getRoleForChat()),
            'skills' => array_map(function (DriverSkill $skill) {
                return $skill->getSkill()->getSkillName();
            }, $user->getDriver()->getDriverSkills()->toArray()),
            'email' => $user->getEmail(),
            'friendlyName' => $user->getFriendlyName(),
            'image' => $user->getProfileImage(),
            'stations' => array_map(function (Station $station) {
                return $station->getCode();
            }, $user->getDriver()->getStations()->toArray())
        ];
    }

    public function userSmallInformationToSend(User $user)
    {
        return [
            'userId' => $user->getId(),
            'identity' => $user->getChatIdentifier(),
            'role' => $user->getRoleForChat(),
            'friendlyName' => $user->getFriendlyName(),
            'image' => $user->getProfileImage(),
        ];
    }

    /**
     * @param $data
     * @return Result
     * @throws Exception
     */
    public function deleteMessage($data)
    {
        $company = $this->getCompanyEntity($data);
        $dataAuth = $this->dataForTwilioAuthentication($company);
        $data = array_merge($data, $dataAuth);
        if (empty($data['channelSid'])) {
            throw new Exception('channelSid field missing or empty');
        }
        return $this->twilioAccountService->deleteMessageResource($data);

    }

    /**
     * @param Company $company
     * @param string $message
     * @return array
     * @throws Exception
     */
    public function setDataForMessageBot(Company $company, string $message)
    {
        $data = [];
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        $data['channelSid'] = $company->getWorkplaceBotChannelSid();
        $data['body'] = $message;
        return $data;
    }

    public function getDrivers($entity)
    {
        if (method_exists($entity, 'getDrivers')) {
            return $entity->getDrivers();
        }

        return [];
    }


    /**
     * @param Company $company
     * @param string $channelSid
     * @param string $message
     * @return array
     * @throws Exception
     */
    public function setDataForMessage(Company $company, string $channelSid, string $message)
    {
        $data = $this->dataForTwilioAuthentication($company);
        $data['channelSid'] = $channelSid;
        $data['body'] = $message;
        return $data;
    }


    /**
     * @param Company $company
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function dataForMessage(Company $company, array $data)
    {
        $dataAuth = $this->dataForTwilioAuthentication($company);
        $data = array_merge($data, $dataAuth);

        if (!empty($data['channelSid'])) {
            $data['channelIdentifier'] = $data['channelSid'];
        }

        if (!empty($data['uniqueName'])) {
            $data['channelIdentifier'] = $data['uniqueName'];
        }

        return $data;
    }

    /**
     * @param array $data
     * @param bool $useLambda
     * @return array
     * @throws Exception
     */
    public function sendTwilioMessage(array $data, bool $useLambda = false)
    {
        if (empty($data['companyId'])) {
            throw new Exception('companyId field missing.');
        }
        $company = $this->em->find(Company::class, $data['companyId']);
        $body = $this->dataForMessage($company, $data);
        if ($useLambda) {
            return $this->sendToLambda($body, 'sendMessage-dev');
        }
        return $this->sendTwilioTextMessage($body)->getFormatData();
    }

    /**
     * @param array $data
     * @param string $functionName
     * @return array
     */
    public function sendToLambda(array $data, string $functionName)
    {

        $client = new LambdaClient([
            'version' => 'latest',
            'region' => $this->bag->get('aws_region'),
            'credentials' => [
                'key' => $this->bag->get('aws_key'),
                'secret' => $this->bag->get('aws_secret')
            ]
        ]);


        $payload = json_encode([
            'version' => '2.0',
            'routeKey' => '$default',
            'rawPath' => '/',
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'requestContext' => [
                'http' => [
                    'method' => 'POST',
                ],
            ],
            'body' => $data,
            'isBase64Encoded' => false,
        ]);

        $result = $client->invoke([
            'FunctionName' => $functionName,
            'InvocationType' => 'Event',
            'Payload' => $payload,
        ]);

        $result = $result->toArray();
        $result['body'] = $data;
        return $result;
    }

    /**
     * @param array $data
     * @return array
     */
    public function createTask(array $data)
    {
        try {
            if (!isset($data['create'])) {
                throw new Exception('create field missing.');
            }
            if (isset($data['create']) && $data['create'] === false) {
                return $data;
            }
            if (empty($data['name'])) {
                throw new Exception('name field missing.');
            }
            if (empty($data['type'])) {
                throw new Exception('type field missing.');
            }
            if (empty($data['stationId'])) {
                throw new Exception('stationId field missing.');
            }
            $stationId = $data['stationId'];
            $name = $data['name'];
            $type = $data['type'];
            $description = empty($data['description']) ? null : $data['description'];
            $taskData = empty($data['data']) ? [] : $data['data'];


            $assignees = $this->em->getRepository(User::class)->getManagersInChargeByStation($stationId, true);
            $task = $this->em->getRepository(Task::class)->createTask($name, $assignees, $type, $description, $taskData);

            return [
                'task' => [
                    'id' => $task->getId()
                ],
                'assignees' => $assignees,
                'data' => $data,
            ];

        } catch (Exception $exception) {
            return [
                'message' => $exception->getMessage(),
                'exception' => $exception->getTrace(),
                'data' => $data,
            ];
        }

    }

    public function createEvent(array $data)
    {
        $name = "N/A";
        if (!empty($data['name'])) {
            $name = $data['name'];
        }
        $eventName = "N/A";
        if (!empty($data['eventName'])) {
            $eventName = $data['eventName'];
        }
        $message = "N/A";
        if (!empty($data['message'])) {
            $message = $data['message'];
        }
        $location = null;
        if (!empty($data['location'])) {
            $location = $data['location'];
        }
        $driverRoute = null;
        if (!empty($data['driverRoute'])) {
            $driverRoute = $data['driverRoute'];
        }
        $vehicle = null;
        if (!empty($data['vehicle'])) {
            $vehicle = $data['vehicle'];
        }
        $device = null;
        if (!empty($data['device'])) {
            $device = $data['device'];
        }
        $user = null;
        if (!empty($data['user'])) {
            $user = $data['user'];
        }

        $event = $this->em->getRepository(Event::class)
            ->createEvent($name, $eventName, $message, $location, $driverRoute, $vehicle, $device, $user);

        return [
            'event' => $event
        ];
    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function getWorkplaceBotChannelByUser(User $user)
    {
        $chatIdentifier = $user->getChatIdentifier();
        if (is_string($chatIdentifier)) {
            return 'workplace_bot|' . $chatIdentifier;
        }
        throw new Exception('The chat identifier is not a string.');
    }

    /**
     * @param $stationId
     * @return array
     */
    public function getManagersInChargeByStation($stationId)
    {
        return $this->em->getRepository(User::class)->getManagersInChargeByStation($stationId);
    }


    /**
     * @param array $data
     * @return Result
     */
    public function sendTwilioTextMessage(array $data)
    {
        return $this->twilioAccountService->createMessageResource($data);
    }

    /**
     * @param array $data
     * @param bool $useLambda
     * @return array
     */
    public function sendMessagesToStationManagers(array $data, bool $useLambda = false)
    {
        try {
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            if (empty($data['stationId'])) {
                throw new Exception('stationId field missing.');
            }
            if (empty($data['body'])) {
                throw new Exception('body field missing.');
            }
            $body = $data['body'];
            $company = $this->em->find(Company::class, $data['companyId']);
            $dataMessage = $this->dataForMessage($company, $data);
            $message = $body;
            $managers = $this->getManagersInChargeByStation($data['stationId']);
            $body = [];
            foreach ($managers as $manager) {
                $dataMessage['channelIdentifier'] = 'workplace_bot|' . $manager['chatIdentifier'];
                $dataMessage['body'] = 'Hi ' . $manager['friendlyName'] . '. ' . $message;
                $body[] = $dataMessage;
            }
            if ($useLambda) {
                $result = $this->sendToLambda($body, 'sendMessage-dev');
                $body = [];
                foreach ($result['body'] as $item) {
                    if (isset($item['accountSid'])) {
                        unset($item['accountSid']);
                    }
                    if (isset($item['authToken'])) {
                        unset($item['authToken']);
                    }
                    $body[] = $item;
                }
                $result['body'] = $body;
                return $result;
            }
            $result = [];
            foreach ($body as $messageData) {
                $result[] = $this->sendTwilioTextMessage($messageData)->getFormatData();
            }
            return $result;
        } catch (Exception $exception) {
            return [
                'exception' => [
                    'message' => $exception->getMessage(),
                    'line' => $exception->getLine(),
                    'file' => $exception->getFile()
                ]
            ];
        }
    }

    /**
     * @param $stationId
     * @return array
     */
    public function getChannelsManagersInChargeByStation($stationId)
    {
        $managers = $this->getManagersInChargeByStation($stationId);
        $channels = [];
        foreach ($managers as $item) {
            $channels[] = 'workplace_bot|' . $item['chatIdentifier'];
        }
        return $channels;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function dataForPlivoSMS(array $data)
    {
        if (empty($data['companyId'])) {
            throw new Exception('companyId field missing.');
        }
        if (empty($data['destinationPhoneNumber'])) {
            throw new Exception('destinationPhoneNumber field missing.');
        }
        if (empty($data['text'])) {
            throw new Exception('text field missing.');
        }

        $destinationPhoneNumber = $this->plivoService->checkPhoneNumber($data['destinationPhoneNumber']);
        $this->plivoService->checkText($data['text']);
        $company = $this->em->find(Company::class, $data['companyId']);
        $authId = $company->getPlivoAuthId();
        $authToken = $company->getPlivoAuthToken();
        $phoneNumber = $company->getPlivoMainSrcNumber();

        $plivoData = $data;
        $plivoData['authId'] = $authId;
        $plivoData['authToken'] = $authToken;
        $plivoData['destinationPhoneNumber'] = $destinationPhoneNumber;
        $plivoData['text'] = $data['text'];
        $plivoData['sourceNumber'] = $phoneNumber;

        return $plivoData;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function sendPlivoSMSByLambda(array $data)
    {
        $plivoData = $this->dataForPlivoSMS($data);
        return $this->sendToLambda($plivoData, 'plivo-dev');
    }

    public function sendPlivoSMSDrivers(array $data, Company $company)
    {
        try {
            $authId = $company->getPlivoAuthId();
            $authToken = $company->getPlivoAuthToken();
            $phoneNumber = $company->getPlivoMainSrcNumber();
            $plivoData = [];
            foreach ($data as $item) {
                $data = $item;
                $data['authId'] = $authId;
                $data['authToken'] = $authToken;
                $destinationPhoneNumber = $this->plivoService->checkPhoneNumber($item['destinationPhoneNumber']);
                $data['destinationPhoneNumber'] = $destinationPhoneNumber;
                $data['text'] = $item['text'];
                $data['sourceNumber'] = $phoneNumber;
                $plivoData[] = $data;
            }
            $result = $this->sendToLambda($plivoData, 'plivo-dev');
            $tmp = $result['body'];
            $body = [];
            foreach ($tmp as $item) {
                unset($item['authId']);
                unset($item['authToken']);
                $body[] = $item;
            }
            $result['body'] = $body;
            return $result;
        } catch (Exception $exception) {
            return ['exception' => [
                'message' => $exception->getMessage(),
                'line' => $exception->getLine(),
                'file' => $exception->getFile()
            ]];
        }
    }

    /**
     * @param array $data
     * @param DriverRoute $driverRoute
     * @param bool $useLambda
     * @return array
     */
    public function sendPlivoBreakReminder(array $data, DriverRoute $driverRoute, bool $useLambda = false)
    {
        try {
            $plivoData = [];
            $driver = $driverRoute->getDriver();
            $personalPhone = $driver ? $driver->getPersonalPhone() : null;
            if (is_null($driverRoute->getDBBreakPunchIn()) && is_string($personalPhone)) {
                $plivoData['getDBBreakPunchIn'] = $driverRoute->getDBBreakPunchIn();
                $plivoData['companyId'] = $data['companyId'];
                $plivoData['destinationPhoneNumber'] = $personalPhone; // $personalPhone; // '+12699673367'
                $driverName = $driver->getFullName();
                $driverId = $driverName . " ID => " . $driver->getId() . " DriverRoute ID => " . $driverRoute->getId();
                $plivoData['text'] = "Hello " . $driverId . ". Please remember to take your required 30 minute break.";
                if ($useLambda) {
                    $result = $this->sendPlivoSMSByLambda($plivoData);
                    unset($result['body']['authId']);
                    unset($result['body']['authToken']);
                    return $result;
                }
                $plivoData = $this->dataForPlivoSMS($data);
                $result = $this->sendSMSV2($plivoData['authId'], $plivoData['authToken'], $plivoData['sourceNumber'], $plivoData['destinationPhoneNumber'], $plivoData['text']);
                return $result->getData();
            }

            return [
                'message' => 'This route has driver with personal phone attached.',
                'personalPhone' => $personalPhone,
                'driverRoute' =>
                    [
                        'id' => $driverRoute->getId(),
                        'punchIn' => $driverRoute->getPunchIn(),
                        'punchOut' => $driverRoute->getDBPunchOut(),
                        'breakPunchIn' => $driverRoute->getBreakPunchIn(),
                        'breakPunchOut' => $driverRoute->getBreakPunchOut(),
                    ]
            ];
        } catch (Exception $e) {
            return [
                'exception' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'data' => $plivoData,
                    'file' => $e->getFile()
                ]
            ];
        }
    }

    /**
     * @param array $data
     * @param DriverRoute $driverRoute
     * @param bool $useLambda
     * @param string $case
     * @return array
     */
    public function sendPlivoPunchPaycomMessage(array $data, DriverRoute $driverRoute, bool $useLambda = false, string $case = 'PunchIn')
    {
        try {
            $plivoData = [];
            $punchInData = [];
            $punchOutData = [];
            $driver = $driverRoute->getDriver();
            $personalPhone = $driver ? $driver->getPersonalPhone() : null;
            $driverName = $driver ? $driver->getFullName() : "N/A";
            if ($case === 'PunchOut') {
                if (is_null($driverRoute->getDBPunchOut()) && is_string($personalPhone)) {
                    $punchOutData['getDBPunchOut'] = $driverRoute->getDBPunchOut();
                    $punchOutData['companyId'] = $data['companyId'];
                    $punchOutData['destinationPhoneNumber'] = $personalPhone; // $personalPhone; // '+12699673367';
                    $driverId = $driverName . " ID => " . $driver->getId() . " DriverRoute ID => " . $driverRoute->getId();
                    $punchOutData['text'] = "Hello " . $driverId . ". Please make sure you have successfully punched out Paycom. If you have already punched out Paycom, please ignore this message. \nAndroid - https://play.google.com/store/apps/details?id=com.paycom.mobile.ess \nIOS - https://apps.apple.com/us/app/paycom/id1207929487.";
                    $plivoData[] = $this->dataForPlivoSMS($punchOutData);
                }
            }

            if (is_null($driverRoute->getPunchIn()) && is_string($personalPhone)) {
                $punchInData['companyId'] = $data['companyId'];
                $punchInData['destinationPhoneNumber'] = $personalPhone; // $personalPhone; // '+12699673367';
                $driverId = $driverName . " ID => " . $driver->getId() . " DriverRoute ID => " . $driverRoute->getId();
                $punchInData['text'] = "Hello " . $driverId . ". Please make sure you have successfully punched into Paycom. If you have already punched into Paycom, please ignore this message. \nAndroid - https://play.google.com/store/apps/details?id=com.paycom.mobile.ess \nIOS - https://apps.apple.com/us/app/paycom/id1207929487.";
                $plivoData[] = $this->dataForPlivoSMS($punchInData);
            }

            if (count($plivoData) === 0) {
                return [
                    'message' => 'This route has driver with personal phone attached.',
                    'personalPhone' => $personalPhone,
                    'case' => $case,
                    'driverRoute' =>
                        [
                            'id' => $driverRoute->getId(),
                            'punchIn' => $driverRoute->getPunchIn(),
                            'punchOut' => $driverRoute->getDBPunchOut(),
                        ]
                ];
            }
            if ($useLambda) {
                return $this->sendToLambda($plivoData, 'plivo-dev');
            }

            return $this->sendArrayOfSMS($plivoData);
        } catch (Exception $e) {
            return [
                'exception' => [
                    'message' => $e->getMessage(),
                    'line' => $e->getLine(),
                    'data' => $plivoData,
                    'file' => $e->getFile()
                ]
            ];
        }
    }

    /**
     * @param array $data
     * @return Company
     * @throws Exception
     */
    public function obtainCompany(array $data)
    {
        if (empty($data['companyId'])) {
            throw new Exception('companyId field missing.');
        }
        return $this->em->find(Company::class, $data['companyId']);
    }

    /**
     * @param string $destinationPhoneNumber
     * @param string $message
     * @param Company $company
     * @return Result
     */
    public function sendSMS(string $destinationPhoneNumber, string $message, Company $company)
    {
        $authId = $company->getPlivoAuthId();
        $authToken = $company->getPlivoAuthToken();
        $sourcePhoneNumber = $company->getPlivoMainSrcNumber();
        $plivo = new PlivoService($authId, $authToken);
        return $plivo->sendMessage($destinationPhoneNumber, $message, $sourcePhoneNumber);
    }

    /**
     * @param string $authId
     * @param string $authToken
     * @param string $sourcePhoneNumber
     * @param string $destinationPhoneNumber
     * @param string $message
     * @return Result
     */
    public function sendSMSV2(string $authId, string $authToken, string $sourcePhoneNumber, string $destinationPhoneNumber, string $message)
    {
        $plivo = new PlivoService($authId, $authToken);
        return $plivo->sendMessage($destinationPhoneNumber, $message, $sourcePhoneNumber);
    }

    public function sendArrayOfSMS(array $list)
    {
        $result = [];
        foreach ($list as $item) {
            $resultSMS = $this->sendSMSV2($item['authId'], $item['authToken'], $item['sourceNumber'], $item['destinationPhoneNumber'], $item['text']);
            $result[] = $resultSMS->getData();
        }
        return $result;
    }

    /**
     * @param Company $company
     * @param string $channelSid
     * @param string $body
     * @return array
     * @throws Exception
     */
    public function setDataForMessageWorkplaceBot(Company $company, string $channelSid, string $body)
    {
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        $data['channelSid'] = $channelSid;
        $data['body'] = $body;
        return $data;
    }


}
