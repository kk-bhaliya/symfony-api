<?php

namespace App\Services;

use App\Entity\InvoiceType;
use App\Entity\Rate;
use App\Entity\Shift;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\SkillRate;
use App\Entity\BalanceGroup;
use App\Entity\Schedule;
use App\Entity\ScheduleDesign;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Description of EmailService.
 */
class DummyDataService
{
    protected $container;
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
      $this->em = $em;
      //parent::__construct();
    }

    public function importData($manager,$companyId)
    {
        $records = 0;
        $company= $manager->getRepository('App\Entity\Company')->find($companyId);

        $records = $records + count($manager->getRepository('App\Entity\Station')->findByCompany($companyId));
        $records = $records + count($manager->getRepository('App\Entity\Skill')->findByCompany($companyId));

        //Add station
        $station = new Station();
        $station->setName('Template Station');
        $station->setNote('');
        $station->setCompany($company);
        $station->setCode('DTMP');
        $station->setStationAddress('2811 Beverly Rd, Eagan, MN 55121, USA');
        $station->setStationLatitude('44.85315504351624');
        $station->setStationLongitude('-93.13636771719665');
        $station->setStationGeofence('85');
        $station->setParkingLotAddress('1000 Blue Gentian Rd, Eagan, MN 55121');
        $station->setParkingLotLatitude('44.858253560563064');
        $station->setParkingLotLongitude('-93.14035021639405');
        $station->setParkingLotGeofence('110');
        $station->setTimezone('CT');
        $manager->persist($station);
        $manager->flush();

        //Add company skill
        $skill1 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Bulk', $company);
        $skill2 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Cosmos', $company);
        $skill3 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Dispatcher', $company);
        $skill4 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Driver', $company);
        $skill5 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Driver Trainer', $company);
        $skill6 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Lead Driver', $company);
        $skill7 = $this->em->getRepository('App\Entity\Skill')->getSkillImportData('Manager', $company);

        //Skill Rate add for skill
        $skillRate1 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill1, 15);
        $skillRate2 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill2, 15);
        $skillRate3 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill3, 15);
        $skillRate4 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill4, 15);
        $skillRate5 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill5, 15);
        $skillRate6 = $this->em->getRepository('App\Entity\SkillRate')->getSkillRateImportData($station, $skill6, 15);


        //Rate added
        $rate1 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Hourly Base Rate - Standard', $station, 'Hourly', '24', NULL, $company);
        $rate2 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Hourly Base Rate - Same Day', $station, 'Hourly', '24', NULL, $company);
        $rate3 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Hourly Base Rate - Secure Parcel', $station, 'Hourly', '24', NULL, $company);
        $rate4 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Flat Rate - $200', $station, 'Daily', '200', NULL, $company);
        $rate5 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Flate Rate - $50', $station, 'Daily', '50', NULL, $company);
        $rate6 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Flate Rate - $100', $station, 'Daily', '100', NULL, $company);
        $rate7 = $this->em->getRepository('App\Entity\Rate')->getRateImportData('Training Day', $station, 'Hourly', '20', NULL, $company);

        //Add Invoice Type
        $invoiceType1 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('AMZL Weekday Cancel', $station, $rate4, 'flatRate', 'AMZL Late Cancellation');
        $invoiceType2 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('AMZL Weekend Cancel', $station, $rate4, 'flatRate', 'AMZL Late Cancellation');
        $invoiceType3 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('DSP Weekday Cancel', $station, $rate5, 'flatRate', 'DSP Late Cancellation');
        $invoiceType4 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('DSP Weekend Cancell', $station, $rate6, 'flatRate', 'DSP Late Cancellation');
        $invoiceType5 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Standard Parcel 10', $station, $rate1, '10', 'Service Type');
        $invoiceType6 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Standard Parcel 8', $station, $rate1, '8', 'Service Type');
        $invoiceType7 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Secure Parcel 8', $station, $rate3, '8', 'Service Type');
        $invoiceType8 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Secure Parcel 4', $station, $rate3, '4', 'Service Type');
        $invoiceType9 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('10 Hr Nursery Level 2', $station, $rate1, '10', 'Service Type');
        $invoiceType10 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('10 Hr Nursery Level 1', $station, $rate1, '10', 'Service Type');
        $invoiceType11 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Training Day', $station, $rate1, '10', 'Service Type');
        $invoiceType12 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('8 Hr Nursery Level 2', $station, $rate1, '8', 'Service Type');
        $invoiceType13 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('8 Hr Nursery Level 1', $station, $rate1, '8', 'Service Type');
        $invoiceType14 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Rescue 10', $station, $rate1, '10', 'Service Type');
        $invoiceType15 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Rescue 5', $station, $rate1, '5', 'Service Type');
        $invoiceType16 = $this->em->getRepository('App\Entity\InvoiceType')->getInvoiceTypeImportData('Box Truck Parcel 10', $station, $rate1, '10', 'Service Type');

        //Add balance group station
        $balancegroup1 = $this->em->getRepository('App\Entity\BalanceGroup')->getBalanceGroupImportData($station, $company, 'Box Truck Parcel');
        $balancegroup2 = $this->em->getRepository('App\Entity\BalanceGroup')->getBalanceGroupImportData($station, $company, 'Secure Parcel');
        $balancegroup3 = $this->em->getRepository('App\Entity\BalanceGroup')->getBalanceGroupImportData($station, $company, 'Standard Parcel');

        //Add shift
        $shift1 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('10 Hr - Lvl 1', $station, $balancegroup3, $invoiceType10, $company, $skill4, '#516F90', '12:15', '23:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 3, '#ffffff' );
        $shift2 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('10 Hr - Lvl 2', $station, $balancegroup3, $invoiceType9, $company, $skill4, '#516F90', '12:15', '23:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 3, '#ffffff' );
        $shift3 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('10 Hr - Standard', $station, $balancegroup3, $invoiceType5, $company, $skill4, '#BD37A4', '12:15', '22:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#ffffff' );
        $shift4 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('8 Hr - Standard', $station, $balancegroup3, $invoiceType6, $company, $skill4, '#853E78', '14:15', '22:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#ffffff' );
        $shift5 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('AM Cosmos', $station, $balancegroup2, $invoiceType6, $company, $skill2, '#f03063', '12:15', '22:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#ffffff' );
        $shift6 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Backup Driver C1', $station, $balancegroup3, NULL, $company, $skill4, '#516F90', '12:15', '15:15', 30, 'Come prepared to run a full route. If all routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.', 2, '#ffffff' );
        $shift7 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Bulk', $station, $balancegroup1, $invoiceType16, $company, $skill1, '#f59fb7', '11:45', '22:15', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#333333' );
        $shift8 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Classroom Training', $station, NULL, $invoiceType11, $company, $skill4, '#DBAE60', '13:00', '22:00', 30, 'Please arrive at least 15 minutes early to have time to find parking.', 3, '#ffffff' );
        $shift9 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Dispatcher', $station, NULL, NULL, $company, $skill3, '#cc2957', '11:00', '22:00', 30, '', 1, '#ffffff' );
        $shift10 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Lead Driver', $station, $balancegroup3, $invoiceType5, $company, $skill6, '#00A38D', '11:15', '22:45', 30, 'Please come prepared to coordinate morning load-out; please reach out to your station manager with any questions.', 1, '#ffffff' );
        $shift11 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('PM Cosmos', $station, NULL, $invoiceType8, $company, $skill2, '#0091AE', '21:15', '01:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#ffffff' );
        $shift12 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Sweeper', $station, NULL, $invoiceType14, $company, $skill4, '#7FD1DE', '14:15', '22:45', 30, 'Please come prepared with your badge, safety vest, and Amazon uniform.', 1, '#333333' );
        $shift13 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Rescue', $station, NULL, NULL, $company, $skill4, '#0040FF', '12:15', '23:45', 0, '', 4, '#ffffff' );
        $shift14 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Add Train', $station, NULL, NULL, $company, $skill4, '#FFC300', '12:15', '23:45', 0, '', 5, '#ffffff' );
        $shift15 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Light Duty', $station, NULL, NULL, $company, $skill4, '#27AE60', '12:15', '23:45', 0, '', 6, '#ffffff' );
        $shift16 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Unscheduled Driver', $station, $balancegroup3, NULL, $company, $skill4, '#428BCA', '12:15', '15:15', 30, 'Come prepared to run a full route. If all routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.', 7, '#ffffff' );
        $shift17 = $this->em->getRepository('App\Entity\Shift')->getShiftImportData('Backup Driver C2', $station, $balancegroup3, NULL, $company, $skill4, '#516F90', '12:15', '15:15', 30, 'Come prepared to run a full route. If all routes are covered and there are no routes available to pick up, you will assist at the station and be sent home.', 2, '#ffffff' );

        //Schedule
        $schedule1 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Custom Schedule 1', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 4, '2020-07-17', 0);

        $scheduleDesign1 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 21, 0, 1, 1, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign2 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift4, 21, 1, 0, 0, 1, 0, 0, 0, 1, '2020', 0);

        $scheduleDesign3 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift7, 22, 1, 1, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign4 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift12, 22, 0, 0, 1, 0, 0, 1, 0, 1, '2020', 0);

        $scheduleDesign5 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift5, 23, 0, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign6 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift9, 23, 0, 0, 0, 1, 1, 1, 0, 1, '2020', 0);

        $scheduleDesign7 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 24, 1, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign8 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift4, 24, 0, 1, 0, 0, 0, 0, 0, 1, '2020', 0);

        $scheduleDesign9 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift5, 24, 0, 0, 1, 0, 0, 0, 0, 2, '2020', 0);

        $scheduleDesign10 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift7, 24, 0, 0, 0, 1, 0, 0, 0, 3, '2020', 0);

        $scheduleDesign11 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift9, 24, 0, 0, 0, 0, 1, 0, 0, 4, '2020', 0);

        $scheduleDesign12 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift10, 24, 0, 0, 0, 0, 0, 1, 0, 5, '2020', 0);

        $scheduleDesign13 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift12, 24, 0, 0, 0, 0, 0, 0, 1, 6, '2020', 0);

        $scheduleDesign14 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 25, 0, 1, 1, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign15 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 25, 1, 0, 0, 1, 0, 0, 0, 1, '2020', 0);

        $scheduleDesign16 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift7, 26, 1, 1, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign17 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift7, 26, 0, 0, 1, 0, 0, 1, 0, 1, '2020', 0);

        $scheduleDesign18 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift5, 27, 0, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign19 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift9, 27, 0, 0, 0, 1, 1, 1, 0, 1, '2020', 0);

        $scheduleDesign20 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 28, 1, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign21 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 28, 0, 1, 0, 0, 0, 0, 0, 1, '2020', 0);

        $scheduleDesign22 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift5, 28, 0, 0, 1, 0, 0, 0, 0, 2, '2020', 0);

        $scheduleDesign23 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift7, 28, 0, 0, 0, 1, 0, 0, 0, 3, '2020', 0);

        $scheduleDesign24 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift9, 28, 0, 0, 0, 0, 1, 0, 0, 4, '2020', 0);

        $scheduleDesign25 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift10, 28, 0, 0, 0, 0, 0, 1, 0, 5, '2020', 0);


        $scheduleDesign26 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 28, 0, 0, 0, 0, 0, 0, 1, 6, '2020', 0);

        $scheduleDesign27 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift3, 29, 0, 1, 1, 1, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign28 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule1, $shift4, 29, 1, 0, 0, 1, 0, 0, 0, 1, '2020', 0);

        $schedule2 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Every Other Weekend', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 2, '2020-07-17', 0);

        $scheduleDesign29 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, $shift3, 21, 1, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign30 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, '', 22, 0, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign31 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, $shift3, 23, 1, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign32 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, '', 24, 0, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign33 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, $shift3, 25, 1, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign34 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, '', 26, 0, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign35 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, $shift3, 27, 1, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $scheduleDesign36 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, '', 28, 0, 0, 0, 0, 0, 0, 0, 0, '2020', 0);

        $scheduleDesign37 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule2, $shift3, 29, 1, 0, 0, 0, 0, 0, 1, 0, '2020', 0);

        $schedule3 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Lead Driver (Schedule A)', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 2, '2020-07-17', 0);

        $scheduleDesign37 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 21, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign38 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 22, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign39 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 23, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign40 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 24, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign41 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 25, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign42 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 26, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign43 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 27, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign44 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 28, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign45 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule3, $shift3, 29, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $schedule4 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Monday-Friday', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 1, '2020-07-17', 0);

        $scheduleDesign46 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 21, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign47 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 22, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign48 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 23, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign49 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 24, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign50 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 25, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign51 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 26, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign52 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 27, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign53 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 28, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign54 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule4, $shift3, 29, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $schedule5 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Rotating Waterfall', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 3, '2020-07-17', 0);

        $scheduleDesign55 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 21, 1, 1, 1, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign56 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 22, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign57 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 23, 0, 0, 1, 1, 1, 1, 1, 0, '2020', 0);

        $scheduleDesign58 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 24, 1, 1, 1, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign59 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 25, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign60 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 26, 0, 0, 1, 1, 1, 1, 1, 0, '2020', 0);

        $scheduleDesign61 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 27, 1, 1, 1, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign62 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 28, 0, 1, 1, 1, 1, 1, 0, 0, '2020', 0);

        $scheduleDesign63 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule5, $shift3, 29, 0, 0, 1, 1, 1, 1, 1, 0, '2020', 0);

        $schedule6 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Schedule A (2-2-3)', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 2, '2020-07-17', 0);

        $scheduleDesign64 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 21, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign65 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 22, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign66 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 23, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign67 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 24, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign68 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 25, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign69 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 26, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign70 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 27, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign71 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 28, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign72 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule6, $shift3, 29, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $schedule7 = $this->em->getRepository('App\Entity\Schedule')->getScheduleImportData('Schedule B (2-2-3)', $company, '2020-05-18', '2020-07-18', 1, $station, 0, 21, 29, '2020', '2020', 2, '2020-07-17', 0);

        $scheduleDesign73 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 21, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign74 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 22, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign75 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 23, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign76 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 24, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign77 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 25, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign78 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 26, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign79 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 27, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        $scheduleDesign80 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 28, 0, 1, 1, 0, 0, 1, 1, 0, '2020', 0);

        $scheduleDesign80 = $this->em->getRepository('App\Entity\ScheduleDesign')->getScheduleDesignImportData($schedule7, $shift3, 29, 1, 0, 0, 1, 1, 0, 0, 0, '2020', 0);

        //Working on..........................

        if($records > 0){
            return ['status'=>false,'message'=>'Failed'];
        } else {
            return ['status'=>true,'message'=>'Success'];
        }

    }
}
