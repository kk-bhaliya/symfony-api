<?php


namespace App\Services;


use App\Entity\Station;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class StationService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * DriverService constructor.
     * @param EntityManagerInterface $manager
     * @param TokenStorageInterface $token
     */
    public function __construct(EntityManagerInterface $manager, TokenStorageInterface $token)
    {
        $this->manager = $manager;
        $this->token = $token;
    }

    /**
     * @param Station $station
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function createStationChannel(Station $station, User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        if(!($station instanceof Station)){
            throw new Exception('Station is null.');
        }
        if(!is_string($station->getCode())){
            throw new Exception('Station Code is not string.');
        }
        $dataChannel = $twilioUtil->getTwilioAuthenticationData($user->getCompany());
        $dataChannel['options']['friendlyName'] = 'Station-' . $station->getCode();
        $dataChannel['options']['attributes'] =
            [
                "kind" => "Group",
                "type" => "Group",
            ];
        $stationChannelResult = $twilioAccountService->createChannelResource($dataChannel);
        if ($stationChannelResult->isSuccess()) {
            $channelSid = $stationChannelResult->getData()['sid'];
            $station->setChannelSid($channelSid);
            $this->manager->flush();
            return $stationChannelResult;
        }
        return $stationChannelResult;
    }

    /**
     * @param Station $station
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function addDriverToStationChannel(Station $station, User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        $dataMember = $twilioUtil->getTwilioAuthenticationData($user->getCompany());
        $dataMember['identity'] = $user->getChatIdentifier();
        $dataMember['channelSid'] = $station->getChannelSid();
        $dataMember['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat()
            ];
        $resultMember = $twilioAccountService->createMemberResource($dataMember);
        if (!$resultMember->isSuccess()) {
            return $resultMember;
        }
        return new Result(true,
            [
                'channelSid' => $dataMember['channelSid'],
                'member' => $resultMember->getFormatData(),
                'user' => $user->getId(),
            ],
            'stationChannel', 'Station ' . $station->getCode() . ' channel.', true);
    }
}