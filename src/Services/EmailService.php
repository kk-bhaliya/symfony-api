<?php

namespace App\Services;

use Exception;

/**
 * Description of EmailService.
 */
class EmailService
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function sendEmail($email, $subject, $message)
    {
        $mail = (new \Swift_Message($subject))
        ->setFrom('support@dspworkplace.com', 'dsp_workplace')
        ->setTo($email)
        ->setBody($message,'text/html');
        try {
            $this->container->get('mailer')->send($mail);
        } catch (Exception $e) {
            echo $e;
        }
    }
}