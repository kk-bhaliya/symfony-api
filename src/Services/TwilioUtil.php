<?php


namespace App\Services;


use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\Station;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class TwilioUtil
{
    private $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * DriverService constructor.
     * @param EntityManagerInterface $entityManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EntityManagerInterface $entityManager, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param $company
     * @throws Exception
     */
    public function checkCompany($company)
    {
        if (!($company instanceof Company)) {
            throw new Exception('Company entity missing.');
        }
    }

    /**
     * @param Company $company
     * @return array
     * @throws Exception
     */
    public function getTwilioAuthenticationData(Company $company)
    {
        $this->checkCompany($company);
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        if (empty($data['accountSid'])) {
            throw new Exception('accountSid missing.');
        }
        if (empty($data['authToken'])) {
            throw new Exception('authToken missing.');
        }
        if (empty($data['serviceSid'])) {
            throw new Exception('serviceSid missing.');
        }
        return $data;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getTwilioAuthData(array $data)
    {
        $company = null;
        if (isset($data['company'])) {
            if (is_string($data['company']) || is_numeric($data['company'])) {
                $company = $this->entityManager->find(Company::class, $data['company']);
            }
            if (isset($data['company']['id'])) {
                $company = $this->entityManager->find(Company::class, $data['company']['id']);
            }

        } elseif (isset($data['companyId'])) {
            $company = $this->entityManager->find(Company::class, $data['companyId']);
        }
        return $this->getTwilioAuthenticationData($company);
    }

    /**
     * @param User $user1
     * @param User $user2
     * @return array
     * @throws Exception
     */
    public function dataForDuoChannel(User $user1, User $user2)
    {
        if ($user1->getCompany()->getId() !== $user2->getCompany()->getId()) {
            throw new Exception('Users are not in the same company.');
        }
        if (!(is_string($user1->getChatIdentifier()))) {
            throw new Exception('User #1 has null chat identifier.');
        }
        if (!(is_string($user2->getChatIdentifier()))) {
            throw new Exception('User #2 has null chat identifier.');
        }
        $identifier1 = $user1->getChatIdentifier();
        $identifier2 = $user2->getChatIdentifier();
        $name1 = $user1->getFriendlyName();
        $name2 = $user2->getFriendlyName();
        $duoChannelData['companyId'] = $user1->getCompany()->getId();
        $duoChannelData['options']['attributes'] = [
            "memberCount" => 2,
            "kind" => "Duo",
            "type" => "Duo",
            "identities" => $identifier1 . "|" . $identifier2,
            "friendlyName" => $name1 . "|" . $name2,
        ];
        $duoChannelData['options']['friendlyName'] = $identifier1 . "|" . $identifier2;
        $duoChannelData['members'] = [
            [
                'userId' => $user1->getId(),
            ],
            [
                'userId' => $user2->getId()
            ]
        ];
        return $duoChannelData;
    }

    /**
     * @param $data
     * @return User
     * @throws Exception
     */
    public function findUser(array $data)
    {
        $user = $this->entityManager->find(User::class, $data['user']['id']);
        if (!($user instanceof User)) {
            throw new Exception('User not found.');
        }
        return $user;
    }

    /**
     * @param array $data
     * @return Station
     * @throws Exception
     */
    public function findStation(array $data)
    {
        $station = $this->entityManager->find(Station::class, $data['station']['id']);
        if (!($station instanceof Station)) {
            throw new Exception('Station not found.');
        }
        return $station;
    }

    /**
     * @param TwilioAccountService $twilioAccountService
     * @param array $dataAuth
     * @param string $identifier1
     * @param string $identifier2
     * @return Result
     */
    public function checkChannelExist(TwilioAccountService $twilioAccountService, array $dataAuth, string $identifier1, string $identifier2)
    {
        $dataAuth['uniqueName'] = $identifier1 . "|" . $identifier2;
        $resultExist = $twilioAccountService->fetchChannelResource($dataAuth);
        if ($resultExist->isSuccess()) {
            return new Result(true, $resultExist->getData(), 'error', 'Channel already exist', true);
        }
        $dataAuth['uniqueName'] = $identifier2 . "|" . $identifier1;
        $resultExist = $twilioAccountService->fetchChannelResource($dataAuth);
        if ($resultExist->isSuccess()) {
            return new Result(true, $resultExist->getData(), 'error', 'Channel already exist', true);
        }
        return $resultExist;
    }

    /**
     * @param User $user1
     * @param User $user2
     * @param TwilioAccountService $twilioAccountService
     * @return Result
     */
    public function createDuoChannel(User $user1, User $user2, TwilioAccountService $twilioAccountService)
    {
        try {
            $companyId_1 = $user1->getCompany()->getId();
            $companyId_2 = $user2->getCompany()->getId();
            if ($companyId_1 !== $companyId_2) {
                throw new Exception('company IDs does not match.');
            }
            $identifier1 = $user1->getChatIdentifier();
            $identifier2 = $user2->getChatIdentifier();
            $name1 = $user1->getFriendlyName();
            $name2 = $user2->getFriendlyName();
            if (!is_string($identifier1)) {
                throw new Exception('$identifier1 not string');
            }
            if (!is_string($identifier2)) {
                throw new Exception('$identifier2 not string');
            }
            if (!is_string($name1)) {
                throw new Exception('$name1 not string');
            }
            if (!is_string($name2)) {
                throw new Exception('$name2 not string');
            }
            $friendlyName = $name1 . '|' . $name2;
            $uniqueName = $identifier1 . '|' . $identifier2;

            $data = $this->getTwilioAuthenticationData($user1->getCompany());
            $dataAuth = $data;
            $resultExist = $this->checkChannelExist($twilioAccountService, $dataAuth, $identifier1, $identifier2);
            if ($resultExist->isSuccess()) {
                return $resultExist;
            }
            $data['options']['friendlyName'] = $friendlyName;
            $data['options']['uniqueName'] = $uniqueName;
            $data['options']['attributes'] =
                [
                    "type" => "Duo",
                    "kind" => "Duo",
                    "friendlyName" => $friendlyName,
                    "identities" => $uniqueName,
                ];
            return $twilioAccountService->createChannelResource($data);
        } catch (Exception $exception) {
            return new Result(false, ['u3'], 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, ['u4'], 'error', $error->getMessage(), true);
        }
    }

    /**
     * @param TwilioAccountService $twilioAccountService
     * @param array $data
     * @param array $dataAuth
     * @return Result
     */
    public function createDuoChannelV2(TwilioAccountService $twilioAccountService, array $data, array $dataAuth)
    {
        try {
            $dataChannel = $dataAuth;
            $dataChannel['options'] = $data['options'];
            return $twilioAccountService->createChannelResource($dataChannel);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
        }
    }

    /**
     * @param string $channelSid
     * @param int $userId
     * @param string $identity
     * @param array $dataAuth
     * @return array
     */
    public function dataForMemberV2(string $channelSid, int $userId, string $identity, array $dataAuth)
    {
        $data = $dataAuth;
        $data['channelSid'] = $channelSid;
        $data['identity'] = $identity;
        $data['options']['attributes'] =
            [
                'id' => $userId,
            ];
        return $data;
    }

    /**
     * @param TwilioAccountService $twilioAccountService
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function createDuoChannelWithMembersV2(TwilioAccountService $twilioAccountService, array $data)
    {
        $company = $this->entityManager->find(Company::class, $data['companyId']);
        $dataAuth = $this->getTwilioAuthenticationData($company);
        $channelResult = $this->createDuoChannelV2($twilioAccountService, $data, $dataAuth);
        $channelSid = $channelResult->getData()['sid'];
        $user1 = $data['members'][0];
        $user2 = $data['members'][1];
        $data1 = $this->dataForMemberV2($channelSid, $user1['userId'], $user1['identity'], $dataAuth);
        $members['member1'] = $twilioAccountService->createMemberResource($data1)->getFormatData();
        if ($user1['userId'] !== $user2['userId']) {
            $data2 = $this->dataForMemberV2($channelSid, $user2['userId'], $user2['identity'], $dataAuth);
            $members['member2'] = $twilioAccountService->createMemberResource($data2)->getFormatData();
        }
        return [
            'channelSid' => $channelSid,
            'members' => $members,
            'channel' => $channelResult->getData(),
        ];
    }

    /**
     * @param User $user1
     * @param User $user2
     * @param TwilioAccountService $twilioAccountService
     * @return array
     * @throws Exception
     */
    public function createDuoChannelWithMembers(User $user1, User $user2, TwilioAccountService $twilioAccountService)
    {
        $channelResult = $this->createDuoChannel($user1, $user2, $twilioAccountService);
        $channelSid = $channelResult->getData()['sid'];
        $membersResult = $this->addUsersToDuoChannel($user1, $user2, $channelSid, $twilioAccountService);
        return [
            'channelSid' => $channelSid,
            'members' => $membersResult,
        ];
    }

    /**
     * @param User $user
     * @param string $channelSid
     * @return array
     * @throws Exception
     */
    public function dataForMember(User $user, string $channelSid)
    {
        if (!($user instanceof User)) {
            throw new Exception('User received is not instance of User.');
        }
        if (!is_string($user->getChatIdentifier())) {
            throw new Exception('Chat Identifier is null.');
        }
        $company = $user->getCompany();
        $data = $this->getTwilioAuthenticationData($company);
        $data['channelSid'] = $channelSid;
        $data['identity'] = $user->getChatIdentifier();
        $data['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat(),
                'image' => $user->getProfileImage(),
                'id' => $user->getId()
            ];
        return $data;
    }

    /**
     * @param User $user1
     * @param User $user2
     * @param string $channelSid
     * @param TwilioAccountService $twilioAccountService
     * @return array
     * @throws Exception
     */
    public function addUsersToDuoChannel(User $user1, User $user2, string $channelSid, TwilioAccountService $twilioAccountService)
    {
        $data1 = $this->dataForMember($user1, $channelSid);
        $members['member1'] = $twilioAccountService->createMemberResource($data1)->getFormatData();
        $data2 = $this->dataForMember($user2, $channelSid);
        $members['member2'] = $twilioAccountService->createMemberResource($data2)->getFormatData();
        return $members;
    }

    /**
     * @param Company $company
     * @param string $channelName
     * @param TwilioAccountService $twilioAccountService
     * @param $image
     * @return Result
     */
    public function createGroupChannel(Company $company, string $channelName, TwilioAccountService $twilioAccountService, $image = null)
    {
        try {
            $data = $this->getTwilioAuthenticationData($company);
            $data['options']['friendlyName'] = $channelName;
            $data['options']['attributes'] =
                [
                    "type" => "Group",
                    "kind" => "Group",
                    "image" => $image,
                ];
            return $twilioAccountService->createChannelResource($data);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
        }
    }

    /**
     * @param User[] $users
     * @param string $channelSid
     * @param TwilioAccountService $twilioAccountService
     * @return array
     * @throws Exception
     */
    public function addUsersToGroupChannel(array $users, string $channelSid, TwilioAccountService $twilioAccountService)
    {
        $members = [];
        foreach ($users as $user) {
            $data = $this->dataForMember($user, $channelSid);
            $members[] = $twilioAccountService->createMemberResource($data)->getFormatData();
        }
        return $members;
    }


    /**
     * @param Company $company
     * @param array $users
     * @param string $channelName
     * @param TwilioAccountService $twilioAccountService
     * @param null $image
     * @return Result
     */
    public function createGroupChannelWithUsers(Company $company, array $users, string $channelName, TwilioAccountService $twilioAccountService, $image = null)
    {
        if (count($users) < 1) {
            return new Result(false, [], 'error', 'The number of users is less than one.', true);
        }
        $usersToAdd = [];
        $issuesUsersIdToAdd = [];
        foreach ($users as $userId) {
            $userToAdd = $this->entityManager->getRepository(User::class)->find($userId);
            if ($userToAdd instanceof User && is_string($userToAdd->getChatIdentifier())) {
                $usersToAdd[] = $userToAdd;
            } else {
                $issuesUsersIdToAdd[] = $userId;
            }
        }
        if (count($usersToAdd) < 1) {
            return new Result(false, [], 'error', 'The number of users is less than one.', true);
        }
        $groupChannelResult = $this->createGroupChannel($company, $channelName, $twilioAccountService, $image);
        if ($groupChannelResult->isSuccess()) {
            $channelSid = $groupChannelResult->getData()['sid'];
            try {
                $memberResult[] = $this->addUsersToGroupChannel($usersToAdd, $channelSid, $twilioAccountService);
            } catch (Exception $exception) {
                $memberResult[] = ['exception' => $exception->getTrace(), 'message' => $exception->getMessage()];
            } catch (Error $error) {
                $memberResult[] = ['error' => $error->getTrace(), 'message' => $error->getMessage()];
            }
            $data['channelSid'] = $channelSid;
            $data['members'] = $memberResult;
            $data['issues'] = $issuesUsersIdToAdd;
            return new Result(true, $data, 'result', 'Group Channel Created.', true);
        } else {
            return $groupChannelResult;
        }
    }

    public function getMembersOfChannelV2(array $data, UserService $userService)
    {
        try {
            $identities = $data['identities'];
            /** @var User $currentUser */
            $currentUser = $this->tokenStorage->getToken()->getUser();
            $users = [];
            foreach ($identities as $identity) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['chatIdentifier' => $identity]);
                if($user instanceof User){
                    $users[] = $userService->dataUserFilterV2($user, $user->getDriver());
                }
            }
            $stations = $userService->dataStationByCompany($currentUser);
            $skills = $userService->dataSkillsByCompany($currentUser);
            $roles = $userService->dataRolesByCompany($currentUser);
            $resultData = [
                'users' => $users,
                'stations' => $stations,
                'skills' => $skills,
                'roles' => $roles,
            ];
            return new Result(true, $resultData, 'users', 'Users fetched by channel', true);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @param TwilioAccountService $twilioAccountService
     * @param UserService $userService
     * @return Result
     */
    public function getMembersOfChannel(array $data, TwilioAccountService $twilioAccountService, UserService $userService)
    {
        try {
            $company = $this->entityManager->find(Company::class, $data['company']['id']);
            $currentUser = $this->entityManager->find(User::class, $data['user']['id']);
            $dataAuth = $this->getTwilioAuthenticationData($company);
            $data = array_merge($data, $dataAuth);
            $result = $twilioAccountService->readMultipleMembersResources($data);
            if ($result->isSuccess()) {
                $members = $result->getData();
                $users = [];
                foreach ($members as $member) {
                    $user = $this->entityManager->getRepository(User::class)->findOneBy(['chatIdentifier' => $member['identity']]);
                    $driver = $user->getDriver();
                    if ($user instanceof User && $driver instanceof Driver) {
                        $users[] = $userService->dataUserFilterV2($user, $driver);
                    }
                }
                $stations = $userService->dataStationByCompany($currentUser);
                $skills = $userService->dataSkillsByCompany($currentUser);
                $roles = $userService->dataRolesByCompany($currentUser);
                $resultData = [
                    'users' => $users,
                    'stations' => $stations,
                    'skills' => $skills,
                    'roles' => $roles,
                ];
            } else {
                return $result;
            }
            return new Result(true, $resultData, 'result', 'Users fetched by channel', true);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @param TwilioAccountService $twilioAccountService
     * @return Result
     */
    public function readMembersOfChannel(array $data, TwilioAccountService $twilioAccountService)
    {
        try {
            if (empty($data['channelSid'])) {
                throw new Exception('channelSid field missing.');
            }
            $company = $this->entityManager->find(Company::class, $data['company']['id']);
            $dataAuth = $this->getTwilioAuthenticationData($company);
            $data = array_merge($data, $dataAuth);
            $result = $twilioAccountService->readMultipleMembersResources($data);
            if ($result->isSuccess()) {
                $members = $result->getData();
                $users = [];
                foreach ($members as $member) {
                    $user = $this->entityManager->getRepository(User::class)->findOneBy(['chatIdentifier' => $member['identity']]);
                    if ($user instanceof User) {
                        $users[] = $user->getId();
                    }
                }
                return new Result(true, $users, 'users', 'Users of channel.', true);
            } else {
                return $result;
            }
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        }
    }

    public function readMembersOfChannelIdentities(array $data, TwilioAccountService $twilioAccountService)
    {
        try {
            if (empty($data['channelSid'])) {
                throw new Exception('channelSid field missing.');
            }
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            $dataAuth = $this->getTwilioAuthenticationData($company);
            $data = array_merge($data, $dataAuth);
            $result = $twilioAccountService->readMultipleMembersResources($data);
            $membersSid = [];
            if ($result->isSuccess()) {
                $members = $result->getData();
                foreach ($members as $member) {
                    $membersSid[] = $member['identity'];
                }
                return new Result(true, $membersSid, 'members', 'Members of channel.', true);
            } else {
                return $result;
            }
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        }
    }
}