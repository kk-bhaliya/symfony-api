<?php


namespace App\Services;


use Error;
use Exception;
use Plivo\Exceptions\PlivoResponseException;
use Plivo\Exceptions\PlivoRestException;
use Plivo\Resources\Message\MessageCreateResponse;
use Plivo\Resources\PhoneNumber\PhoneNumber;
use Plivo\Resources\PhoneNumber\PhoneNumberBuyResponse;
use Plivo\Resources\ResourceList;
use Plivo\Resources\ResponseUpdate;
use Plivo\Resources\SubAccount\SubAccount;
use Plivo\Resources\SubAccount\SubAccountCreateResponse;
use Plivo\RestClient;

class PlivoService
{

    /**
     * @var string
     */
    private $authId;
    /**
     * @var string
     */
    private $authToken;
    /**
     * @var string
     */
    private $mainSrcNumber;

    /** @var RestClient $client */
    private $client;


    public function __construct(string $authId, string $authToken)
    {
        $this->authId = $authId;
        $this->authToken = $authToken;
        $this->client = new RestClient($this->authId, $this->authToken);
    }

    /**
     * @return RestClient
     */
    public function getClient(): RestClient
    {
        return $this->client;
    }

    public function createSubAccount(string $accountName)
    {
        try {
            /** @var SubAccountCreateResponse $response */
            $response = $this->client->subAccounts->create($accountName, true);
            return new Result(true,
                [
                    'authId' => $response->getAuthId(),
                    'authToken' => $response->getAuthToken(),
                    'statusCode' => $response->getStatusCode(),
                    'apiId' => $response->getApiId(),
                    'message' => $response->getMessage(),
                ],
                'plivo', 'Account ' . $accountName . ' created');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in creating plivo sub account with name: ' . $accountName);
        } catch (Error $exception) {
            if (empty($exception->getTrace()[0]['args'][0]['error'])) {
                return new Result(false, $exception->getTrace(),
                    'error', 'Error in creating plivo sub account with name: ' . $accountName);
            }
            return new Result(false, $exception->getTrace(),
                'error',
                'Error in creating plivo sub account with name: ' . $accountName . ', ' . $exception->getTrace()[0]['args'][0]['error']);
        }
    }

    /**
     * @param string $authId
     * @return Result
     */
    public function retrieveSubAccount(string $authId)
    {
        try {
            /** @var SubAccount $response */
            $response = $this->client->subAccounts->get($authId);
            return new Result(true,
                [
                    'id' => $response->getId(),
                    'authId' => $response->authId,
                    'authToken' => $response->authToken,
                    'enabled' => $response->enabled,
                    'name' => $response->name,
                    'params' => $response->properties,
                ],
                'plivo', 'Account retrieved');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in retrieving plivo sub account');
        } catch (Error $exception) {
            return new Result(false, $exception->getTrace(),
                'error',
                'Error in retrieving plivo sub account');
        }
    }

    public function findNumbers(array $criteria)
    {
        try {
            /** @var ResourceList $response */
            $response = $this->client->phonenumbers->list(
                'US', ['type' => 'local', 'pattern' => $criteria['areaCode']]
            );
            return new Result(true,
                [
                    'firstPhoneNumber' => $response->resources[0]->properties['number'],
                    'meta' => $response->meta(),
                    'resources' => $response->resources,
                ],
                'plivo', 'Phone numbers retrieved by area code: ' . $criteria['areaCode']);

        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in finding numbers');
        } catch (Error $exception) {
            return new Result(false, $exception->getTrace(),
                'error',
                'Error in finding numbers');
        }
    }

    public function buyNumber(string $phoneNumber)
    {
        try {
            /** @var PhoneNumberBuyResponse $response */
            $response = $this->client->phonenumbers->buy($phoneNumber);
            return new Result(true,
                [
                    'message' => $response->getMessage(),
                    'statusCode' => $response->getStatusCode(),
                    'apiId' => $response->getApiId(),
                    'number' => $phoneNumber,
                ],
                'plivo', 'Phone number ' . $phoneNumber . ' has been bought');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in buying phone number');
        } catch (Error $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in buying phone number');
        }
    }

    public function updateBuyNumber(string $phoneNumber, string $alias, string $subAccountAuthId = '')
    {
        try {
            /** @var ResponseUpdate $response */
            $response = $this->client->numbers->update($phoneNumber,
                ['alias' => $alias, 'subaccount' => $subAccountAuthId]);

            return new Result(true,
                [
                    'message' => $response->getMessage(),
                    'statusCode' => $response->getStatusCode(),
                    'apiId' => $response->getApiId(),
                    'number' => $phoneNumber,
                ],
                'plivo',
                'Phone number ' . $phoneNumber . ' has been updated with alias as ' . $alias . ' and subAccount as ' . $subAccountAuthId);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in updating phone number');
        } catch (Error $exception) {
            return new Result(false, $exception->getTrace(),
                'error', 'Error in updating phone number');
        }
    }

    /**
     * @param string $destinationNumber
     * @param string $text
     * @param string $sourceNumber
     * @return Result
     */
    public function sendMessage(string $destinationNumber, string $text, string $sourceNumber)
    {
        try {
            $client = new RestClient($this->authId, $this->authToken);
            /** @var $response MessageCreateResponse */
            $response = $client->messages->create($sourceNumber,
                [$destinationNumber],
                $text
            );
            return new Result(true,
                [
                    'statusCode' => $response->getStatusCode(),
                    'apiId' => $response->getApiId(),
                    'message' => $response->getMessage(),
                    'uuid' => $response->getMessageUuid(),
                    'length' => strlen($text),
                ],
                'sms', 'SMS sent');
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'error', 'Error in sending SMS');
        } catch (Error $exception) {
            return new Result(false, $exception->getTrace(), 'error', 'Error in sending SMS');
        }
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function checkAuthenticationFields(array $data)
    {
        if (empty($data['authId'])) {
            throw new Exception('authId field missing');
        }
        if (empty($data['authToken'])) {
            throw new Exception('authToken field missing');
        }
        if (empty($data['phoneNumber'])) {
            throw new Exception('phoneNumber field missing');
        }
    }

    /**
     * @param string $phoneNumber
     * @return string|string[]
     * @throws Exception
     */
    public function checkPhoneNumber(string $phoneNumber)
    {
        $number = $phoneNumber;
        if (strpos($number, '+1') === false) {
            $number = '+1' . $number;
        }
        $number = str_replace("-", "", $number);
        $number = str_replace(" ", "", $number);
        if (strlen($number) !== strlen('+11231231234')) {
            throw new Exception('Phone number has invalid string length. Phone number received: ' . $number);
        }
        return $number;
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function checkAreaCode(array $data)
    {
        if (empty($data['areaCode'])) {
            throw new Exception('Area Code is not found');
        }
    }

    /**
     * @param array $data
     * @param string $key
     * @throws Exception
     */
    public function checkDataByKey(array $data, string $key)
    {
        if (empty($data[$key])) {
            throw new Exception('The data[' . $key . '] is not found or empty');
        }
    }

    /**
     * @param $text
     * @throws Exception
     */
    public function checkText($text)
    {
        if (!is_string($text)) {
            throw new Exception('Text has invalid format, is not string');
        }
        if (strlen(trim($text)) < 1) {
            throw new Exception('Text has invalid string length, < 1');
        }
    }

    /**
     * @return string
     */
    public function getMainSrcNumber(): string
    {
        return $this->mainSrcNumber;
    }

    /**
     * @param string $mainSrcNumber
     */
    public function setMainSrcNumber(string $mainSrcNumber): void
    {
        $this->mainSrcNumber = $mainSrcNumber;
    }


}