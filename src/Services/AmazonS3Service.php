<?php


namespace App\Services;

use Aws\Result;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Exception;

class AmazonS3Service
{
    /**
     * @var S3Client
     */
    private $s3client;
    /**
     * @var string
     */
    private $bucket_key_id;

    /**
     * @var string
     */
    private $bucket_access_key;


    public function __construct(string $bucket_key_id, string $bucket_access_key)
    {
        $this->bucket_key_id = $bucket_key_id;
        $this->bucket_access_key = $bucket_access_key;
        $this->s3client = new S3Client([
            'version' => 'latest',
            'region' => 'us-east-1',
            'credentials' => [
                'key' => $bucket_key_id,
                'secret' => $bucket_access_key,
            ]
        ]);
    }

    /**
     * @param $filepath
     * @param string $filename
     * @param string $bucketId
     * @return Result|string|array
     */
    public function uploadFile($filepath, string $filename, string $bucketId)
    {
        try {
            if ($filepath === null || empty($filepath) || $filepath === '') {
                throw new Exception('File path is empty or null.');
            }
            if (!file_exists($filepath)) {
                throw new Exception('File does not exist.');
            }

            return $this->getS3client()->putObject([
                'Bucket' => $bucketId,
                'Key' => $filename,
                'SourceFile' => $filepath,
                'ACL' => 'public-read'
            ]);
        } catch (S3Exception $e) {
            return $e->getMessage();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    /**
     * @param string $file
     * @param string $bucketId
     * @return Result|string
     */
    public function deleteFile(string $file, string $bucketId)
    {
        return $this->getS3client()->deleteObject([
            'Bucket' => $bucketId,
            'Key' => $file
        ]);
    }


    /**
     * @return S3Client
     */
    public function getS3client(): S3Client
    {
        return $this->s3client;
    }

    /**
     * @param S3Client $s3client
     */
    public function setS3client(S3Client $s3client): void
    {
        $this->s3client = $s3client;
    }

    /**
     * @return string
     */
    public function getBucketKeyId(): string
    {
        return $this->bucket_key_id;
    }

    /**
     * @param string $bucket_key_id
     */
    public function setBucketKeyId(string $bucket_key_id): void
    {
        $this->bucket_key_id = $bucket_key_id;
    }

    /**
     * @return string
     */
    public function getBucketAccessKey(): string
    {
        return $this->bucket_access_key;
    }

    /**
     * @param string $bucket_access_key
     */
    public function setBucketAccessKey(string $bucket_access_key): void
    {
        $this->bucket_access_key = $bucket_access_key;
    }
}
