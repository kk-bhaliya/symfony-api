<?php


namespace App\Services;


use App\Entity\Driver;
use App\Entity\DriverSkill;
use App\Entity\Role;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\User;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserService
{

    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * DriverService constructor.
     * @param EntityManagerInterface $manager
     * @param TokenStorageInterface $token
     */
    public function __construct(EntityManagerInterface $manager, TokenStorageInterface $token)
    {
        $this->manager = $manager;
        $this->token = $token;
    }

    /**
     * @return User|object|string
     * @throws Exception
     */
    public function getCurrentUser()
    {
        $user = $this->token->getToken()->getUser();
        if ($user instanceof User) {
            return $user;
        }
        throw new Exception('User not available.');
    }

    /**
     * @return string|null
     */
    public function getCurrentUserFriendlyName()
    {
        $user = $this->token->getToken()->getUser();
        if ($user instanceof User) {
            return $user->getFriendlyName();
        }
        return "N/A";
    }

    /**
     * @param string $key
     * @param $value
     * @return User
     * @throws Exception
     */
    public function findUser(string $key, $value)
    {
        $user = $this->manager->getRepository(User::class)->findOneBy([$key => $value]);
        if ($user instanceof User) {
            return $user;
        }
        throw new Exception('User entity not found.');
    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function getIdentity(User $user)
    {
        if (!($user instanceof User)) {
            throw new Exception('User entity is null.');
        }
        $identity = $user->getIdentifier() . '-' . bin2hex(random_bytes(7));
        if ($user->getChatIdentifier() === null) {
            $user->setChatIdentifier($identity);
            $this->manager->flush();
        }
        return $user->getChatIdentifier();
    }

    /**
     * @param User $user
     * @return string
     * @throws Exception
     */
    public function generateIdentity(User $user)
    {
        if (!($user instanceof User)) {
            throw new Exception('User entity is null.');
        }
        return $user->getIdentifier() . '-' . bin2hex(random_bytes(7));
    }

    /**
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @return Result
     * @throws Exception
     */
    public function createUserForChat(User $user, TwilioAccountService $twilioAccountService)
    {
        $company = $user->getCompany();
        $dataUser['accountSid'] = $company->getAccountSid();
        $dataUser['authToken'] = $company->getAuthToken();
        $dataUser['serviceSid'] = $company->getServiceSid();
        $dataUser['identity'] = $this->generateIdentity($user);
        $dataUser['options']['friendlyName'] = $user->getFriendlyName();
        $dataUser['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat(),
                'image' => $user->getProfileImage(),
                'id' => $user->getId(),
            ];
        $resultUser = $twilioAccountService->createUserResource($dataUser);
        if ($resultUser->isSuccess()) {
            $userSid = $resultUser->getData()['sid'];
            $identity = $resultUser->getData()['identity'];
            $user->setUserSid($userSid);
            $user->setChatIdentifier($identity);
            $this->manager->flush();
        }
        return $resultUser;
    }

    /**
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function createWorkplaceBotChannel(User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            if (!is_string($user->getChatIdentifier())) {
                throw new Exception('User #' . $user->getIdentifier() . ' does not have identity for twilio');
            }
            $company = $user->getCompany();
            $data = $twilioUtil->getTwilioAuthenticationData($company);
            $data['options']['friendlyName'] = 'workplace_bot';
            $data['options']['uniqueName'] = 'workplace_bot|' . $user->getChatIdentifier();
            $data['options']['attributes'] =
                [
                    "kind" => "Bot",
                    "type" => "Bot",
                ];
            $result = $twilioAccountService->createChannelResource($data);
            if ($result->isSuccess()) {
                $channelSid = $result->getData()['sid'];
                if (is_string($channelSid)) {
                    $user->setNotificationsChannelSid($channelSid);
                    $this->manager->flush();
                }
            }
            return $result;
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
        }
    }

    /**
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function addUserToWorkplaceBotChannel(User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        $channelSid = $user->getNotificationsChannelSid();
        if (!is_string($channelSid)) {
            throw new Exception('channelSid is not string.');
        }
        $company = $user->getCompany();
        $data = $twilioUtil->getTwilioAuthenticationData($company);
        $data['channelSid'] = $channelSid;
        $data['identity'] = $user->getChatIdentifier();
        $data['options']['attributes'] =
            [
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat(),
                'image' => $user->getProfileImage(),
            ];
        return $twilioAccountService->createMemberResource($data);
    }

    /**
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     */
    public function setUpWorkplaceBotChannel(User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $workplaceBotChannel = $this->createWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
            if ($workplaceBotChannel->isSuccess()) {
                $resultMember = $this->addUserToWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
                if ($resultMember->isSuccess()) {
                    $user->setNotificationsChannelSid($workplaceBotChannel->getData()['sid']);
                    $this->manager->flush();
                }
                return new Result(true, [
                    'workplaceBotChannel' => $workplaceBotChannel->getData(),
                    'memberChannel' => $resultMember->getData(),
                ]);
            }
            return $workplaceBotChannel;
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'result', $e->getMessage());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'result', $e->getMessage());
        }
    }

    /**
     * @param User $user
     * @param string $channelSid
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function addUserToChannel(User $user, string $channelSid, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        $data = $twilioUtil->dataForMember($user, $channelSid);
        return $twilioAccountService->createMemberResource($data);
    }

    /**
     * @param User $user
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return Result
     * @throws Exception
     */
    public function createChannelWithManager(User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            if (!($user instanceof User)) {
                throw new Exception('The user received does not exists.');
            }
            if ($user->getIsArchive()) {
                throw new Exception('The user received is archived.');
            }
            $driver = $user->getDriver();
            if (!($driver instanceof Driver)) {
                throw new Exception('The user received does not have a driver.');
            }

            $manager = $driver->getMainManager();
            if (!($manager instanceof User)) {
                throw new Exception('No managers assigned found.');
            }
            $userId = $user->getId();
            $managerId = $manager->getId();
            if ($userId === $managerId) {
                throw new Exception('Manager is the same as User.');
            }
            $result['user']['id'] = $userId;
            $result['manager']['id'] = $managerId;
            $channelResult = $twilioUtil->createDuoChannel($user, $manager, $twilioAccountService);
            if ($channelResult->isSuccess() && $channelResult->getNameOfResult() === 'channel') {
                $result['channel'] = $channelResult->getFormatData();
                $channelSid = $channelResult->getData()['sid'];
                $result['members'] = $twilioUtil->addUsersToDuoChannel($user, $manager, $channelSid, $twilioAccountService);
            } else {
                return $channelResult;
            }
            return new Result(true, $result, 'managerChannel', 'Channel with manager', true);
        } catch (Exception $exception) {
            return new Result(false, [$exception->getTraceAsString()], 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, [$error->getTraceAsString()], 'error', $error->getMessage(), true);
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    private function containsChatIdentifier(User $user)
    {
        return is_string($user->getChatIdentifier());
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getTwilioUsersInformation(array $data)
    {
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }

        $users = $this->manager->getRepository(User::class)->getTwilioUsersByCompany(['company' => $data['companyId']]);
        $list = [];
        foreach ($users as $user) {
            /** @var User $user */
            $driver = $user->getDriver();
            if ($driver instanceof Driver) {
                $list[] = $this->dataUserFilterV2($user, $driver);
            }
        }

        $stations = $this->manager->getRepository(Station::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $stations = array_map(function (Station $station) {
            return $station->getCode();
        }, $stations);

        $skills = $this->manager->getRepository(Skill::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $skills = array_map(function (Skill $skill) {
            return $skill->getSkillName();
        }, $skills);

        $roles = $this->manager->getRepository(Role::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $roles = array_map(function (Role $role) {
            return $role->getRoleName();
        }, $roles);

        return [
            'users' => $list,
            'roles' => $roles,
            'skills' => $skills,
            'stations' => $stations,
            'count' => sizeof($list),
        ];
    }


    public function validateUser(User $user)
    {
        return
            $user instanceof User &&
            $user->getChatIdentifier() !== null &&
            $user->getDriver() !== null &&
            $user->getDriver()->getStations()->count() > 0 &&
            $user->getDriver()->getDriverSkills()->count() > 0;
    }

    /**
     * @param User $user
     * @return array
     */
    public function validateUserV2(User $user)
    {
        $driver = $user->getDriver();
        if ($driver) {
            return [
                'valid' => true,
                'data' => [
                    'userId' => $user->getId(),
                    'identity' => $user->getChatIdentifier(),
                    'role' => $user->getRoleForChat(),
                    'skills' => array_map(function (DriverSkill $skill) {
                        return $skill->getSkill()->getSkillName();
                    }, $driver->getDriverSkills()->toArray()),
                    'email' => $user->getEmail(),
                    'friendlyName' => $user->getFriendlyName(),
                    'image' => $user->getProfileImage(),
                    'stations' => array_map(function (Station $station) {
                        return $station->getCode();
                    }, $driver->getStations()->toArray())
                ]
            ];
        }
        return ['valid' => false, 'data' => null];
    }

    /**
     * @param array $user
     * @return array
     */
    public function validateUserMobile(array $user)
    {
        return [
            'userId' => $user['userId'],
            'driverId' => $user['driverId'],
            'identity' => $user['identity'],
            'email' => $user['email'],
            'friendlyName' => $this->getFriendlyName($user['friendlyName'], $user['firstName'], $user['lastName']),
            'image' => $user['image'],
        ];
    }

    /**
     * @param array $user
     * @return array
     */
    public function validateDriverMobile(array $user)
    {
        return [
            'userId' => $user['userId'],
            'driverId' => $user['driverId'],
            'identity' => $user['identity'],
            'email' => $user['email'],
            'friendlyName' => $this->getFriendlyName($user['friendlyName'], $user['firstName'], $user['lastName']),
            'image' => $user['image'],
        ];
    }

    public function getFriendlyName($friendlyName, $firstName, $lastName): string
    {
        if (is_string($friendlyName)) {
            return $friendlyName;
        }
        if (is_string($firstName . ' ' . $lastName)) {
            return $firstName . ' ' . $lastName;
        }
        if (is_string($firstName)) {
            return $firstName;
        }
        if (is_string($lastName)) {
            return $lastName;
        }
        return "N/A";
    }

    /**
     * @param User $user
     * @return array
     */
    public function dataUserFilter(User $user)
    {
        $driver = $user->getDriver();
        return [
            'userId' => $user->getId(),
            'identity' => $user->getChatIdentifier(),
            'role' => $user->getRoleForChat(),
            'skills' => array_map(function (DriverSkill $skill) {
                return $skill->getSkill()->getSkillName();
            }, $driver->getDriverSkills()->toArray()),
            'email' => $user->getEmail(),
            'friendlyName' => $user->getFriendlyName(),
            'image' => $user->getProfileImage(),
            'stations' => array_map(function (Station $station) {
                return $station->getCode();
            }, $driver->getStations()->toArray())
        ];
    }

    /**
     * @param User $user
     * @param Driver $driver
     * @return array
     */
    public function dataUserFilterV2(User $user, Driver $driver)
    {
        $stationsCollection = $driver->getStations();
        $stations = [];
        foreach ($stationsCollection as $station) {
            $stations[] = $station->getCode();
        }
        $skillsCollection = $driver->getDriverSkills();
        $skills = [];
        foreach ($skillsCollection as $skill) {
            $skills[] = $skill->getSkill()->getName();
        }

        return [
            'userId' => $user->getId(),
            'identity' => $user->getChatIdentifier(),
            'role' => $user->getRoleForChat(),
            'skills' => $skills,
            'email' => $user->getEmail(),
            'friendlyName' => $user->getFriendlyName(),
            'image' => $user->getProfileImage(),
            'stations' => $stations
        ];
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function getTwilioUsersInformationTest(array $data)
    {
        if (empty($data['companyId'])) {
            throw new Exception('companyId missing.');
        }

        $drivers = $this->manager->getRepository(Driver::class)->findBy(['company' => $data['companyId'], 'isArchive' => false]);
        $list = [];
        foreach ($drivers as $driver) {
            $list[] = $this->dataDriverFilter($driver);
        }

        $stations = $this->manager->getRepository(Station::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $stations = array_map(function (Station $station) {
            return $station->getCode();
        }, $stations);

        $skills = $this->manager->getRepository(Skill::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $skills = array_map(function (Skill $skill) {
            return $skill->getSkillName();
        }, $skills);

        $roles = $this->manager->getRepository(Role::class)
            ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

        $roles = array_map(function (Role $role) {
            return $role->getRoleName();
        }, $roles);

        return [
            'drivers' => $list,
            'roles' => $roles,
            'skills' => $skills,
            'stations' => $stations,
            'count' => sizeof($list),
        ];
    }

    public function dataDriverFilter(Driver $driver)
    {
        return [
            'jobTitle' => $driver->getJobTitle(),
            'id' => $driver->getId(),
            'personalPhone' => $driver->getPersonalPhone()
        ];
    }

    public function dataStationByCompany(User $user)
    {
        $company = $user->getCompany();
        $stations = $this->manager->getRepository(Station::class)
            ->findBy(['company' => $company->getId(), 'isArchive' => false]);
        if (count($stations) > 0) {
            $stations = array_map(function (Station $station) {
                return $station->getCode();
            }, $stations);
        }
        return $stations;
    }

    public function dataSkillsByCompany(User $user)
    {
        $company = $user->getCompany();
        $skills = $this->manager->getRepository(Skill::class)
            ->findBy(['company' => $company->getId(), 'isArchive' => false]);
        if (count($skills) > 0) {
            $skills = array_map(function (Skill $skill) {
                return $skill->getSkillName();
            }, $skills);
        }
        return $skills;
    }

    public function dataRolesByCompany(User $user)
    {
        $company = $user->getCompany();
        $roles = $this->manager->getRepository(Role::class)
            ->findBy(['company' => $company->getId(), 'isArchive' => false]);
        if (count($roles) > 0) {
            $roles = array_map(function (Role $role) {
                return $role->getRoleName();
            }, $roles);
        }
        return $roles;
    }

    public function createTwilioUser(User $user, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil, UserService $userService)
    {
        try {
            if (!($user instanceof User)) {
                throw new Exception('User is not valid.');
            }
            $driver = $user->getDriver();
            if (!($user->getDriver() instanceof Driver)) {
                $user->setDriver($driver);
                $this->manager->flush();
            }
            $results['userCreation'] = $this->createUserForChat($user, $twilioAccountService)->getFormatData();
            $workplaceBotChannelCreation = $this->setUpWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
            $results['workplaceBotChannelCreation'] = $workplaceBotChannelCreation->getFormatData();
            $results['user'] = [
                'identity' => $user->getChatIdentifier(),
                'userId' => $user->getId(),
                'driverId' => $user->getDriver()->getId(),
                'friendlyName' => $user->getFriendlyName(),
                'role' => $user->getRoleForChat(),
            ];
            return new Result(true, $results, 'users', 'Twilio Users creation', true);
        } catch (Exception $exception) {
            return new Result(false, $exception->getTrace(), 'exception', $exception->getMessage(), true);
        } catch (Error $error) {
            return new Result(false, $error->getTrace(), 'error', $error->getMessage(), true);
        }
    }


    /**
     * @param $entityId
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return array
     * @throws Exception
     */
    public function updateUserForTwilio($entityId, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        $user = $this->findUser("id", $entityId);
        $data = $twilioUtil->getTwilioAuthenticationData($user->getCompany());
        $data['userSid'] = $user->getUserSid();
        $data['options'] = [];
        $data['options']['friendlyName'] = $user->getFriendlyName();
        $data['options']['attributes']['image'] = $user->getProfileImage();
        $role = $user->getRoleName();
        $data['options']['attributes']['role'] = $role;
        $data['options']['attributes']['position'] = $role;
        $result = $twilioAccountService->updateUserResource($data);
        return $result->getData();
    }

    public function generalFieldsUser(?User $user){
        if($user){
            return [
                'id' => $user->getId(),
                'friendlyName' => $user->getFriendlyName(),
                'email' => $user->getEmail(),
                'role' => $user->getRoleName(),
                'identity' => $user->getChatIdentifier(),
                'notificationsChannelSid' => $user->getNotificationsChannelSid(),
            ];
        }
        return null;
    }

    public function fieldsUser(User $user)
    {
        $updatedBy = $user->getUpdatedBy();
        return [
            'id' => $user->getId(),
            'friendlyName' => $user->getFriendlyName(),
            'email' => $user->getEmail(),
            'role' => $user->getRoleName(),
            'identity' => $user->getChatIdentifier(),
            'notificationsChannelSid' => $user->getNotificationsChannelSid(),
            'updatedAt' => $user->getUpdatedAt(),
            'createdAt' => $user->getCreatedAt(),
            'updatedBy' => $this->generalFieldsUser($user->getUpdatedBy()),
        ];
    }


}