<?php


namespace App\Services;


use Exception;
use Stripe\Customer;
use Stripe\Exception\ApiErrorException;
use Stripe\Plan;
use Stripe\Product;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\SubscriptionItem;

class StripeService
{


    /**
     * @var string
     */
    private $stripe_auth_api_secret_key;
    /**
     * @var string
     */
    private $stripe_auth_api_publish_key;

    public function __construct(string $stripe_auth_api_secret_key, string $stripe_auth_api_publish_key)
    {

        $this->stripe_auth_api_secret_key = $stripe_auth_api_secret_key;
        $this->stripe_auth_api_publish_key = $stripe_auth_api_publish_key;
    }

    public function setApiKey(){
        Stripe::setApiKey($this->stripe_auth_api_secret_key);

    }

    public function getProducts($params = null, $opts = null){
        try {
            return Product::all($params, $opts);
        } catch (ApiErrorException $e) {
            return $e;
        }
    }

    public function getPlans($params = null, $opts = null){
        try {
            return Plan::all($params, $opts);
        } catch (ApiErrorException $e) {
            return $e;
        }
    }


    public function createCustomer($params = null, $options = null)
    {
        try {
            return Customer::create($params, $options);
        } catch (ApiErrorException $e) {
            return $e;
        }
    }

    public function getCustomer($params = null, $options = null)
    {
        try {
            return Customer::retrieve($params, $options);
        } catch (ApiErrorException $e) {
            return $e;
        }
    }

    public function deleteCustomer($params = null, $options = null)
    {
        try {
            $customer = Customer::retrieve($params, $options);
            return $customer->delete();
        } catch (ApiErrorException $e) {
            return $e;
        }
    }

    public function createSubscription($params = null, $options = null){
        try {
            return Subscription::create($params, $options);
        } catch (ApiErrorException $e) {
            return $e;
        }

    }

    public function createUsageRecord($subscriptionItemId, $params = null, $opts = null){
        try {
            return SubscriptionItem::createUsageRecord($subscriptionItemId, $params, $opts);
        } catch (ApiErrorException $e) {
            return $e;
        }
    }
}