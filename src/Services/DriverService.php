<?php


namespace App\Services;


use App\Entity\Driver;
use App\Entity\Station;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DriverService
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;
    /**
     * @var TokenStorageInterface
     */
    private $token;

    /**
     * DriverService constructor.
     * @param EntityManagerInterface $manager
     * @param TokenStorageInterface $token
     */
    public function __construct(EntityManagerInterface $manager, TokenStorageInterface $token)
    {
        $this->manager = $manager;
        $this->token = $token;
    }

    /**
     * @param $data
     * @param string $key
     * @return Driver
     * @throws Exception
     */
    public function findDriver(array $data, $key = 'driverId')
    {
        if (empty($data[$key])) {
            throw new Exception('data and key: ' . $key . ' field missing');
        }
        if (empty($data)) {
            throw new Exception('data field missing');
        }
        $value = is_array($data) ? $data[$key] : $data;
        if ($key === 'driverId') {
            $key = 'id';
        }
        $driver = $this->manager->getRepository(Driver::class)->findOneBy([$key => $value]);
        if ($driver instanceof Driver) {
            return $driver;
        }

        throw new Exception('Driver does not exist');
    }

    public function dataForMember(Driver $driver, string $channelSid)
    {
        $data['channelSid'] = $channelSid;
        $data['identity'] = $driver->getUser()->getChatIdentifier();
        $data['options'] = [];
        $data['options']['attributes'] =
            [
                'friendlyName' => $driver->getUser()->getFriendlyName(),
                'role' => $driver->getUser()->getRoleForChat(),
            ];
        return $data;
    }

    /**
     * @param Driver $driver
     * @param TwilioAccountService $twilioAccountService
     * @return Result
     * @throws Exception
     */
    public function addDriverToStationChannel(Driver $driver, TwilioAccountService $twilioAccountService)
    {
        if (!($driver instanceof Driver)) {
            throw new Exception('User has no Driver entity attached.');
        }
        $station = $driver->getStations()->first();
        if ($station instanceof Station) {
            if (!is_string($station->getChannelSid())) {
                throw new Exception('Station has no channel.');
            }
            $company = $driver->getCompany();
            $user = $driver->getUser();
            $data['accountSid'] = $company->getAccountSid();
            $data['authToken'] = $company->getAuthToken();
            $data['serviceSid'] = $company->getServiceSid();
            $data['channelSid'] = $driver->getStations()[0]->getChannelSid();
            $data['identity'] = $driver->getUser()->getChatIdentifier();
            $data['options']['attributes'] =
                [
                    'friendlyName' => $user->getFriendlyName(),
                    'role' => $user->getRoleForChat(),
                ];
            return $twilioAccountService->createMemberResource($data);
        }
        throw new Exception('Station not found.');
    }

    /**
     * @param Driver $driver
     * @return array
     * @throws Exception
     */
    public function addDriverToNotificationChannelData(Driver $driver)
    {
        $user = $driver->getUser();

        if (!($user instanceof User)) {
            throw new Exception('User not found by driver');
        }
        if ($user->getIsArchive()) {
            throw new Exception('User is archived');
        }
        if ($user->getChatIdentifier() === null) {
            throw new Exception('User does not have chat identifier');
        }
        if ($user->getNotificationsChannelSid() !== null) {
            throw new Exception('User has already have a channelSid as ' . $user->getNotificationsChannelSid());
        }

        $channelData['companyId'] = $user->getCompany()->getId();
        $channelData['options']['friendlyName'] = 'workplace_bot';
        $channelData['options']['attributes']['kind'] = 'Bot';
        $channelData['members'][0] = ['userId' => $user->getId()];
        return $channelData;
    }

    /**
     * @param Driver $driver
     * @return array
     * @throws Exception
     */
    public function dataForDuoChannelManager(Driver $driver)
    {
        if (empty($driver->getCompany()->getId())) {
            throw new Exception('companyId missing');
        }
        $duoChannelsData = [];
        $criteria['companyId'] = $driver->getCompany()->getId();
        $user1 = $driver->getUser();
        $user2 = $driver->getAssignedManagers()->count() > 0 ? $driver->getAssignedManagers()->first() : null;
        if ($driver->getAssignedManagers()->count() < 1) {
            throw new Exception('No User Manager assigned.');
        }
        foreach ($driver->getAssignedManagers() as $user) {
            if ($user instanceof User) {
                $criteria['options']['attributes'] = [
                    "kind" => "Duo",
                    "type" => "Duo",
                    "nameOfChannel" => $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier(),
                    "identities" => $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier(),
                ];
                $criteria['options']['friendlyName'] = $user1->getFriendlyName() . "|" . $user2->getFriendlyName();
                $criteria['options']['uniqueName'] = $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier();
                $criteria['members'] = [
                    [
                        'userId' => $user1->getId(),
                    ],
                    [
                        'userId' => $user2->getId()
                    ]
                ];
                $duoChannelsData[] = $criteria;
            }
        }

        return $duoChannelsData;
    }

    public function getManagers(Driver $driver)
    {
        $managers = [];
        $assignedManagers = $driver->getAssignedManagers();
        foreach ($assignedManagers as $item) {
            if ($item instanceof User) {
                $managers[] = [
                    'userId' => $item->getId(),
                    'friendlyName' => $item->getFriendlyName(),
                    'chatIdentifier' => $item->getChatIdentifier(),
                    'isArchive' => $item->getIsArchive()
                ];
            }
        }
        return $managers;
    }

}