<?php


namespace App\Services;

use App\Entity\Company;
use Error;
use Exception;
use phpDocumentor\Reflection\Types\Boolean;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\ChatGrant;
use Twilio\Rest\Client;

class TwilioAccountService
{
    /**
     * @var string
     */
    private $firebaseSecret;

    public function __construct($firebaseSecret)
    {
        $this->firebaseSecret = $firebaseSecret;
    }

    /**
     * @param Exception $e
     * @return Result
     */
    public function exceptionResult($e)
    {
        if ($e instanceof ConfigurationException) {
            return new Result(false, $e->getTrace(), 'configurationException', $e->getMessage(), true);
        }
        if ($e instanceof TwilioException) {
            return new Result(false, $e->getTrace(), 'twilioException', $e->getMessage(), true);
        }
        if ($e instanceof Error) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
        if ($e instanceof Exception) {
            return new Result(false, $e->getTrace(), 'exception', $e->getMessage(), true);
        }
        return new Result(false, $e->getTrace(), 'errorOther', $e->getMessage());
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function checkAuthentication(array $data)
    {
        if (empty($data['accountSid'])) {
            throw new Exception('accountSid field is missing');
        }

        if (empty($data['authToken'])) {
            throw new Exception('authToken field is missing');
        }

        if (empty($data['serviceSid'])) {
            throw new Exception('serviceSid field is missing');
        }
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function checkOptionsForUser(array $data)
    {
        $options = [];
        if (empty($data['options'])) {
            throw new Exception('options fields missing');
        }
        if (array_key_exists('attributes', $data['options'])) {
            $options['attributes'] = json_encode($data['options']['attributes'], JSON_THROW_ON_ERROR);
        }
        if (array_key_exists('roleSid', $data['options']) && is_string($data['options']['roleSid'])) {
            $options['roleSid'] = $data['options']['roleSid'];
        }
        if (array_key_exists('friendlyName', $data['options']) && !is_string($data['options']['friendlyName'])) {
            throw new Exception('friendlyName is not a string');
        }
        if (array_key_exists('friendlyName', $data['options']) && is_string($data['options']['friendlyName'])) {
            $options['friendlyName'] = $data['options']['friendlyName'];
        }

        return $options;
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkChannelSid(array $data)
    {
        if (empty($data['channelSid'])) {
            throw new Exception('channelSid field missing.');
        }
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkChannelUniqueName(array $data)
    {
        if (empty($data['uniqueName'])) {
            throw new Exception('uniqueName field missing.');
        }
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkIdentity(array $data)
    {
        if (empty($data['identity'])) {
            throw new Exception('identity field missing.');
        }
    }

    /**
     * @param array $data
     * @throws Exception
     */
    private function checkUserSid(array $data)
    {
        if (empty($data['userSid'])) {
            throw new Exception('userSid field is missing');
        }
    }

    private function unsetFields(array $data)
    {
        unset($data['accountSid']);
        unset($data['serviceSid']);
        if (isset($data['links'])) {
            unset($data['links']);
        }
        unset($data['url']);
        return $data;
    }

    /**
     * @param array $data
     * @return Result
     */
    public function createUserResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkIdentity($data);
            $options = $this->checkOptionsForUser($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $user = $twilio->chat->v2->services($data['serviceSid'])->users->create($data['identity'], $options);
            $user = $user->toArray();
            unset($user['accountSid']);
            unset($user['serviceSid']);
            unset($user['links']);
            unset($user['url']);
            return new Result(true, $user, 'user', 'User created', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function updateUserResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkUserSid($data);
            $options = $this->checkOptionsForUser($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $user = $twilio->chat->v2->services($data['serviceSid'])
                ->users($data['userSid'])
                ->update($options);
            $user = $user->toArray();
            unset($user['accountSid']);
            unset($user['serviceSid']);
            unset($user['links']);
            unset($user['url']);
            return new Result(true, $user, 'user', 'User Updated', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    private function getTrace(array $trace)
    {
        if (sizeof($trace) > 0) {
            $trace = $trace[0];
            unset($trace['args']);
            return $trace;
        }
        return [];
    }

    public function deleteUserResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkUserSid($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $user = $twilio->chat->v2->services($data['serviceSid'])->users($data['userSid'])->delete();
            return new Result(true, ['user' => $data['userSid'], 'isUserDeleted' => $user, 'userDeleted' => $user], 'user', 'User deleted');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function fetchUserResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkUserSid($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $user = $twilio->chat->v2->services($data['serviceSid'])->users($data['userSid'])->fetch();
            $user = $user->toArray();
            unset($user['accountSid']);
            unset($user['serviceSid']);
            unset($user['links']);
            unset($user['url']);
            return new Result(true, $user, 'user', 'User fetched', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage());
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function readMultipleUsers(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $users = $twilio->chat->v2->services($data['serviceSid'])->users->read(1000);
            $result = [];
            foreach ($users as $user) {
                $user = $user->toArray();
                unset($user['accountSid']);
                unset($user['serviceSid']);
                unset($user['links']);
                unset($user['url']);
                $result[] = $user;
            }
            return new Result(true, $result, 'user', 'Users fetched', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }


    /**
     * @param array $data
     * @return Result
     */
    public function createApiKey(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $new_key = $twilio->newKeys->create(['friendlyName' => $data['keyName']]);
            return new Result(true, $new_key->toArray(), 'apiKey', 'API KEY created');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function createAuthTokenToChat(array $data)
    {
        $token = new AccessToken($data['accountSid'], $data['apiKey'], $data['apiSecret'], $data['ttl'], $data['identity']);
        $chatGrant = new ChatGrant();
        $chatGrant->setServiceSid($data['serviceSid']);
        $chatGrant->setPushCredentialSid($data['credentialSid']);
        $token->addGrant($chatGrant);
        $tokenJWT = $token->toJWT();
        return new Result(true,
            [
                'token' => $tokenJWT,
                'credentialSid' => $data['credentialSid'],
                'data' => isset($data['platform']) && $data['platform'] === "ios" ? $data['apnCredentialSid'] : $data['fcmCredentialSid'],
            ], 'twilioToken');

    }

    public function fetchServiceResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $service = $twilio->chat->v2->services($data['serviceSid'])->fetch();
            return new Result(true, $service->toArray(), 'service', 'Service Fetched');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function getMultipleServiceResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $services = $twilio->chat->v2->services->read($data['limit'], $data['pageSize']);
            return new Result(true, $services);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function updateServiceResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $service = $twilio->chat->v2->services($data['serviceSid'])->update($data['options']);
            return new Result(true, $service->toArray(), 'service', 'Service Updated');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function deleteServiceResource($accountSid, $authToken, $serviceSid)
    {
        try {
            $twilio = new Client($accountSid, $authToken);
            $service = $twilio->chat->v2->services($serviceSid)->delete();
            return new SingleResult(true, $service, 'serviceDeleted');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function createCredentialResource(array $data)
    {
        try {
            $data['options']['secret'] = $this->firebaseSecret;
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $credential = $twilio->chat->v2->credentials->create($data['type'], $data['options']);
            return new Result(true, $credential->toArray(), 'credential', 'Credential Created');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    private function dataForChannel(array $data)
    {
        $options = [];
        if (array_key_exists('attributes', $data['options'])) {
            $options['attributes'] = json_encode($data['options']['attributes']);
        }
        if (array_key_exists('friendlyName', $data['options'])) {
            $options['friendlyName'] = $data['options']['friendlyName'];
        }
        if (array_key_exists('uniqueName', $data['options'])) {
            $options['uniqueName'] = $data['options']['uniqueName'];
        }
        if (array_key_exists('type', $data['options'])) {
            $options['type'] = $data['options']['type'];
        }

        return $options;
    }

    /**
     * @param array $data
     * @return Result
     */
    public function createChannelResource(array $data)
    {
        try {
            $options = $this->dataForChannel($data);
            $this->checkAuthentication($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $channel = $twilio->chat->v2->services($data['serviceSid'])->channels->create($options);
            $channel = $channel->toArray();
            unset($channel['accountSid']);
            unset($channel['serviceSid']);
            unset($channel['url']);
            unset($channel['links']);
            return new Result(true, $channel, 'channel', 'Channel Created', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'configurationException', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'exception', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function fetchChannelResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $channel = null;
            if (!empty($data['channelSid'])) {
                $channel = $twilio->chat->v2->services($data['serviceSid'])->channels($data['channelSid'])->fetch();
            } elseif (!empty($data['uniqueName'])) {
                $channel = $twilio->chat->v2->services($data['serviceSid'])->channels($data['uniqueName'])->fetch();
            } else {
                throw new Exception('channelSid or uniqueName is missing.');
            }
            $channel = $channel->toArray();
            unset($channel['accountSid']);
            unset($channel['serviceSid']);
            unset($channel['links']);
            unset($channel['url']);
            return new Result(true, $channel, 'channel', 'Channel fetched.', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function fetchMultipleChannelResources(array $data)
    {
        try {
            if (!array_key_exists('options', $data)) {
                $data['options'] = array();
            }
            if (!array_key_exists('limit', $data)) {
                $data['limit'] = null;
            }
            if (!array_key_exists('pageSize', $data)) {
                $data['pageSize'] = null;
            }

            $twilio = new Client($data['accountSid'], $data['authToken']);
            $channelsResult = $twilio->chat->v2->services($data['serviceSid'])->channels->read($data['options'], $data['limit'], $data['pageSize']);
            $channels = [];
            if (array_key_exists('filter', $data)) {
                $filter = $data['filter'];
                if ($filter !== null) {
                    foreach ($channelsResult as $channel) {
                        $channels[] = [$filter => $channel->$filter, 'sid' => $channel->sid];
                    }
                    return new Result(true, $channels, 'channels', $filter, true);
                }

            }

            foreach ($channelsResult as $channel) {
                $arr = $channel->toArray();
                unset($arr['accountSid']);
                unset($arr['serviceSid']);
                unset($arr['url']);
                unset($arr['links']);
                $channels[] = $arr;

            }
            return new Result(true, $channels, 'channels', 'All data for each channel', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'configurationException', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function updateChannelResource(array $data)
    {
        try {
            if (!array_key_exists('channelSid', $data)) {
                throw new Exception('channelSid field is missing');
            }
            $options = $this->dataForChannel($data);
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $channel = $twilio->chat->v2->services($data['serviceSid'])->channels($data['channelSid'])->update($options);
            $channel = $channel->toArray();
            unset($channel['accountSid']);
            unset($channel['serviceSid']);
            unset($channel['links']);
            unset($channel['url']);
            return new Result(true, $channel, 'channel', 'Channel Updated', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function deleteChannelResource(array $data)
    {
        try {
            if (!array_key_exists('channelSid', $data)) {
                throw new Exception('channelSid field is missing');
            }
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $channel = $twilio->chat->v2->services($data['serviceSid'])->channels($data['channelSid'])->delete();
            $channelDeleted['isChannelDeleted'] = $channel;
            $channelDeleted['sid'] = $data['channelSid'];
            return new Result(true, $channelDeleted, 'channel', 'Channel deleted', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function readMultipleUserChannels(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $userChannels = $twilio->chat->v2
                ->services($data['serviceSid'])
                ->users($data['userSid'])
                ->userChannels
                ->read();
            $result = [];
            if (!empty($data['simple']) && $data['simple']) {
                foreach ($userChannels as $record) {
                    $result[] = $record->channelSid;
                }
            } else {
                foreach ($userChannels as $record) {
                    $temp = $record->toArray();
                    unset($temp['accountSid']);
                    unset($temp['serviceSid']);
                    unset($temp['url']);
                    unset($temp['links']);
                    $result[] = $temp;
                }
            }

            return new Result(true, $result, 'channel', 'There are ' . sizeof($result) . ' channels ', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function createMemberResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkIdentity($data);
            $this->checkChannelSid($data);
            $options['attributes'] = [];
            if (isset($data['options']) && array_key_exists('attributes', $data['options'])) {
                $options['attributes'] = json_encode($data['options']['attributes']);
            } else {
                if (isset($data['friendlyName'])) {
                    $options['attributes'] = json_encode(['friendlyName' => $data['friendlyName']]);
                }
            }
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $member = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->members->create($data['identity'], $options);
            $member = $member->toArray();
            unset($member['accountSid']);
            unset($member['serviceSid']);
            unset($member['url']);
            return new Result(true, $member, 'member', 'Member created', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'configurationException', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'twilioException', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'exception', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function readMultipleMembersResources(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $this->checkChannelSid($data);

            $twilio = new Client($data['accountSid'], $data['authToken']);
            $members = $twilio->chat->v2->services($data['serviceSid'])->channels($data['channelSid'])
                ->members->read([], 1000);
            $result = [];
            foreach ($members as $member) {
                $member = $member->toArray();
                unset($member['accountSid']);
                unset($member['serviceSid']);
                unset($member['url']);
                $result[] = $member;
            }
            return new Result(true, $result, 'members', 'Members fetched by channel', true);
        } catch (ConfigurationException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (TwilioException $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Exception $e) {
            return new Result(false, $this->getTrace($e->getTrace()), 'error', $e->getMessage(), true);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function addUserToChannel(string $accountSid, string $authToken, $serviceSid, $channelSid, $options)
    {
        try {
            $twilio = new Client($accountSid, $authToken);
            $member = $twilio->chat->v2->services($serviceSid)
                ->channels($channelSid)
                ->members
                ->create($options['identity']);
            return new Result(true, $member->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function createUserChannel($accountSid, $authToken, $serviceSid, $channelSid, $user1, $user2)
    {
        $r1 = $this->addUserToChannel($accountSid, $authToken, $serviceSid, $channelSid, ['identity' => $user1]);
        $r2 = $this->addUserToChannel($accountSid, $authToken, $serviceSid, $channelSid, ['identity' => $user2]);
        return [$r1, $r2];
    }


    public function fetchRoleResource($data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $role = $twilio->chat->v2->services($data['serviceSid'])->roles($data['roleSid'])->fetch();
            return new Result(true, $role->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, ['message' => $e->getMessage(), 'trace' => $e->getTrace()], 'ConfigurationException');
        } catch (TwilioException $e) {
            return new Result(false, ['message' => $e->getMessage(), 'trace' => $e->getTrace()], 'TwilioException');
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function createRoleResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $role = $twilio->chat->v2->services($data['serviceSid'])->roles
                ->create($data['friendlyName'], $data['type'], $data['permission']);
            return new Result(true, $role->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function updateRoleResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $role = $twilio->chat->v2->services($data['serviceSid'])->roles($data['roleSid'])
                ->update($data['permission']);

            return new Result(true, $role->toArray(), 'role', 'Role updated');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function getUserChannelResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $userChannel = $twilio->chat->v2->services($data['serviceSid'])
                ->users($data['userSid'])->userChannels($data['channelSid'])->fetch();
            return new Result(true, $userChannel->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function getMemberResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $member = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->members($data['memberSid'])->fetch();
            return new Result(true, $member->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function updateMemberResource(array $data)
    {
        $options = [];
        if (!empty($data['attributes'])) {
            $options['attributes'] = json_encode($data['attributes']);
        }
        if (!empty($data['roleSid'])) {
            $options['roleSid'] = $data['roleSid'];
        }
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $member = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->members($data['memberSid'])
                ->update($options);

            return new Result(true, $member->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    public function getRoleResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $role = $twilio->chat->v2->services($data['serviceSid'])->roles($data['roleSid'])->fetch();
            return new Result(true, $role->toArray(), 'role', 'Role Founded');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param Company $company
     * @param string $message
     * @return array
     * @throws Exception
     */
    public function setDataForMessageBot(Company $company, string $message)
    {
        $data = [];
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        $data['channelSid'] = $company->getWorkplaceBotChannelSid();
        $this->checkAuthentication($data);
        $this->checkChannelSid($data);
        $data['body'] = $message;
        return $data;
    }

    /**
     * @param array $data
     * @return Result
     */
    public function createMessageResource(array $data)
    {
        try {
            $this->checkAuthentication($data);
            $options = [];
            if (!empty($data['attributes'])) {
                $options['attributes'] = json_encode($data['attributes']);
            }
            /** The identity of the new message's author. The default value is system.*/
            if (!empty($data['from']) && is_string($data['from'])) {
                $options['from'] = $data['from'];
            }
            if (empty($data['body'])) {
                throw new Exception('body field is missing.');
            }
            $options['body'] = $data['body'];
            $channelIdentifier = null;
            if (!empty($data['channelSid'])) {
                $channelIdentifier = $data['channelSid'];
            } else if (!empty($data['uniqueName'])) {
                $channelIdentifier = $data['uniqueName'];
            } else if (!empty($data['channelIdentifier'])) {
                $channelIdentifier = $data['channelIdentifier'];
            }
            if (is_null($channelIdentifier)) {
                throw new Exception('Channel Identifier is missing.');
            }
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $message = $twilio->chat->v2->services($data['serviceSid'])->channels($channelIdentifier)->messages->create($options);
            $messageData = $this->unsetFields($message->toArray());
            return new Result(true, $messageData, 'message', 'Message created.');
        } catch (ConfigurationException $e) {
            return $this->exceptionResult($e);
        } catch (TwilioException $e) {
            return $this->exceptionResult($e);
        } catch (Exception $e) {
            return $this->exceptionResult($e);
        } catch (Error $e) {
            return $this->exceptionResult($e);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function fetchMessageResource(array $data)
    {
        $expand = false;
        if (isset($data['expand']) && is_bool($data['expand'])) {
            $expand = $data['expand'];
        }
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $message = $twilio->chat->v2->services($data['serviceSid'])->channels($data['channelSid'])
                ->messages($data['messageSid'])->fetch();
            return new Result(true, $message->toArray(), 'Message', 'Message Fetched', $expand);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'Message', $e->getMessage(), $expand);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'Message', $e->getMessage(), $expand);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function readMultipleMessageResources(array $data)
    {
        $expand = false;
        if (isset($data['expand']) && is_bool($data['expand'])) {
            $expand = $data['expand'];
        }
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $messages = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->messages->read();
            $arrayMessage['length'] = sizeof($messages);
            $arrayMessage['data'] = [];
            foreach ($messages as $message) {
                $arrayMessage['data'][] = $message->toArray();
            }
            return new Result(true, $arrayMessage, 'Messages', 'Message Fetched', $expand);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'Messages', $e->getMessage(), $expand);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'Messages', $e->getMessage(), $expand);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function deleteMessageResource(array $data)
    {
        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $message = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->messages($data['messageSid'])->delete();
            return new Result(true, ['deleted' => $message], 'message', 'Message Deleted');
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage());
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function deleteMessagesResources(array $data)
    {
        try {
            $result = $this->readMultipleMessageResources($data);
            if ($result->isSuccess()) {
                foreach ($result->getData()['data'] as $message) {
                    $data['messageSid'] = $message['sid'];
                    $this->deleteMessageResource($data);
                }
            }
            return new Result(true, [sizeof($result->getData()['data'])], 'Messages', 'Messages Deleted');
        } catch (Exception $e) {
            return new Result(false, $e->getTrace(), 'Message', $e->getMessage());
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }

    }

    /**
     * @param array $data
     * @return Result
     */
    public function updateMessageResource(array $data)
    {
        $expand = false;
        if (isset($data['expand']) && is_bool($data['expand'])) {
            $expand = $data['expand'];
        }
        if (!empty($data['attributes'])) {
            $data['attributes'] = json_encode($data['attributes']);
        }

        try {
            $twilio = new Client($data['accountSid'], $data['authToken']);
            $message = $twilio->chat->v2->services($data['serviceSid'])
                ->channels($data['channelSid'])
                ->messages($data['messageSid'])
                ->update($data);
            return new Result(true, $message->toArray(), 'Message', 'Message Updated', $expand);
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace(), 'Message', $e->getMessage(), $expand);
        } catch (TwilioException $e) {
            return new Result(false, $e->getTrace(), 'Message', $e->getMessage(), $expand);
        } catch (Error $e) {
            return new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        }
    }


}