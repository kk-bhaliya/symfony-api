<?php


namespace App\Services;


use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class Result
{
    /**
     * @var bool
     */
    private $success;

    private $data;

    /**
     * @var string
     */
    private $nameOfResult;

    /**
     * @var bool
     */
    private $expand;
    /**
     * @var string
     */
    private $message;

    /**
     * @var array
     */
    private $formatData;

    /**
     * Result constructor.
     * @param bool $success
     * @param $data
     * @param string $nameOfResult
     * @param string $message
     * @param bool $expand
     * @param array $formatData
     */
    public function __construct(bool $success = false, $data = [], $nameOfResult = 'result', string $message = 'OK', bool $expand = true, array $formatData = [])
    {
        $this->success = $success;
        $this->data = $data;
        $this->nameOfResult = $nameOfResult;
        $this->expand = $expand;
        $this->message = $message;
        $this->formatData = $formatData;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getNameOfResult(): string
    {
        return $this->nameOfResult;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }


    public function getData()
    {
        return $this->data;
    }


    public function setData($data): void
    {
        $this->data = $data;
    }


    /**
     * @param string $key
     * @param $information
     * @return array
     */
    public function addToFormatData(string $key, $information)
    {
        $this->formatData = $this->getFormatData();
        $this->formatData[$key] = $information;
        return $this->formatData;
    }

    /**
     * @return array
     */
    public function getFormatData()
    {
        if ($this->expand) {
            $this->formatData['success'] = $this->isSuccess();
            $this->formatData['message'] = $this->message;
            $this->formatData[$this->nameOfResult] = $this->data;
            return $this->formatData;
        }

        $this->formatData['success'] = $this->isSuccess();
        $this->formatData['message'] = $this->message;
        $this->formatData[$this->nameOfResult] = reset($this->data);
        return $this->formatData;
    }

    /**
     * @return JsonResponse
     */
    public function jsonError()
    {
        return new JsonResponse($this->getFormatData(), Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param bool $data
     * @return JsonResponse
     */
    public function jsonResponse(bool $data = false)
    {
        if ($data) {
            return new JsonResponse($this->getData(), Response::HTTP_OK);
        }
        return new JsonResponse($this->getFormatData(), Response::HTTP_OK);
    }

    /**
     * @param $e
     * @param bool $expand
     * @return JsonResponse
     */
    public function jsonException($e, bool $expand = true)
    {
        if ($e instanceof Error) {
            $result = new Result(false, $e->getTrace(), 'error', $e->getMessage(), true);
        } else if ($e instanceof Exception) {
            $result = new Result(false, $e->getTrace(), 'exception', $e->getMessage(), true);
        } else {
            $result = new Result(false, $e->getTrace(), 'errorOther', $e->getMessage(), $expand);
        }
        return $result->jsonError();
    }

    /**
     * @param $exception
     * @param bool $expand
     * @return Result
     */
    public function getExceptionResult($exception, bool $expand = true): Result
    {
        return new Result(false, $exception->getTrace(), 'error', $exception->getMessage(), $expand);
    }


}