<?php

namespace App\Services;

use App\Entity\Company;
use App\Entity\CortexLog;
use App\Entity\CortexSettings;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\Role;
use App\Entity\Shift;
use App\Entity\Station;
use App\Utils\EventEmitter;
use App\Utils\GlobalUtility;
use Doctrine\ORM\EntityManagerInterface;

class CortexService {
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    )
    {
        $this->entityManager = $entityManager;
    }

    public function getStationDetailsFromFlex($searchFor, $searchField = 'defaultStationCode')
    {
        $regions = json_decode(file_get_contents('https://logistics.amazon.com/flex/api/getOperationalRegions'));
        $station = false;
        foreach ($regions as $region)
            foreach ($region->basicServiceAreas as $area)
                if ($area->{$searchField} == $searchFor)
                    $station = $area;

        return $station;
    }

    public function processLog( $json )
    {
        if (!$json->log->routeSummaries)
            throw new \Exception('Invalid json log: ' . print_r($json, true));

        $dspId = $json->log->routeSummaries[0]->companyId;

        /** @var CortexSettings $cortexSettings */
        $cortexSettings = $this->entityManager->getRepository(CortexSettings::class)->findOneByCortexDspId($dspId);

        if ( !$cortexSettings )
            throw new \Exception('Company not found');

        /** @var Station $station */
        $stationDetails = self::getStationDetailsFromFlex($json->serviceAreaId, 'serviceAreaID');
        $station = $this->entityManager->getRepository(Station::class)->findOneBy([
            'code' => $stationDetails->defaultStationCode,
            'company' => $cortexSettings->getCompany()
        ]);

        if ( !$station )
            throw new \Exception('Station not found');

        // save log to future reports
        $log = new CortexLog();
        $log->setCompany($cortexSettings->getCompany());
        $log->setStation($station);
        $log->setRecord((array)$json);
        $this->entityManager->persist($log);
        $this->entityManager->flush();

        $routes = $json->log->routeSummaries;

        $allRouteCodes = array_map(function ($route) {
            return $route->routeCode;
        }, $routes);

        $allRouteCodesQuery = implode(',', array_map(function ($code) {
            return "'{$code}'";
        }, $allRouteCodes));

        return (object)[
            'cortexSettings' => $cortexSettings,
            'company' => $cortexSettings->getCompany(),
            'station' => $station,
            'routes' => $routes,
            'codes'  => $allRouteCodes,
            'codesQuery' => $allRouteCodesQuery
        ];
    }
}