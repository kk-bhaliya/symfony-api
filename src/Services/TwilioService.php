<?php


namespace App\Services;

use Exception;
use phpDocumentor\Reflection\Types\This;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class TwilioService
{
    private $twilioAccountSid;
    private $twilioApiKey;
    private $twilioApiSecret;
    public $authToken;

    public function __construct(string $twilioAccountSid, string $twilioApiKey, string $twilioApiSecret, string $authToken)
    {
        $this->twilioAccountSid = $twilioAccountSid;
        $this->twilioApiKey = $twilioApiKey;
        $this->twilioApiSecret = $twilioApiSecret;
        $this->authToken = $authToken;
    }

    /**
     * @param Exception $e
     * @return Result
     */
    public function exceptionResult(Exception $e)
    {
        return new Result(false, $e->getTrace(), 'error', $e->getMessage());
    }

    public function createSubAccount(string $accountName)
    {
        try {
            if (empty($accountName)) {
                throw new Exception('Empty or Null String Received in accountName');
            }
            $twilio = new Client($this->twilioAccountSid, $this->authToken);
            $account = $twilio->api->v2010->accounts->create(array("friendlyName" => $accountName));
            return new Result(true, $account->toArray(), 'subAccount', 'Sub Account created');
        } catch (Exception $e) {
            return $this->exceptionResult($e);
        }
    }

    public function createChatService(string $accountName)
    {
        try {
            $result = $this->createSubAccount($accountName);
            if ($result->isSuccess()) {
                $subAccount = $result->getData();

                $twilio = new Client($subAccount['sid'], $subAccount['authToken']);
                $service = $twilio->chat->v2->services->create($accountName . '-Chat-Service');
                $result = ['serviceId' => $service->sid, 'service' => $service->toArray(), 'subAccount' => $subAccount];
                return new Result(true, $result, 'chatService', 'Chat service created');
            }
            return $result;
        } catch (Exception $e) {
            return $this->exceptionResult($e);
        }
    }

    public function findSubAccount(string $accountName)
    {
        try {
            $twilio = new Client($this->twilioAccountSid, $this->authToken);
            $accounts = $twilio->api->v2010->accounts->read(["friendlyName" => $accountName]);
            $subAccount = [];
            foreach ($accounts as $account) {
                if ($account->status === 'active') {
                    $subAccount = $account;
                }
            }
            if (is_array($subAccount)) {
                return new Result(false, [], 'subAccount', 'No subAccount found');
            }
            return new Result(true,
                ['lastSubAccountFound' => $subAccount->toArray(), 'numOfSubAccounts' => count($accounts)],
                'subAccount',
                'Last subAccount found'
            );

        } catch (Exception $e) {
            return $this->exceptionResult($e);
        }
    }

    public function fetchSubAccountInformation(string $accountName)
    {
        try {
            $twilio = new Client($this->twilioAccountSid, $this->authToken);
            $accounts = $twilio->api->v2010->accounts->read(array("friendlyName" => $accountName));
            $subAccount = [];
            $subAccounts = [];
            foreach ($accounts as $account) {
                $subAccount = $account;
                $subAccounts[] = $account->toArray();
            }
            if (count($accounts) > 1) {
                return new Result(true,
                    ['numOfSubAccounts' => count($accounts), 'subAccounts' => $subAccounts]
                    , 'subAccounts', 'Showing all subAccounts');
            }
            return new Result(true, $subAccount->toArray());
        } catch (ConfigurationException $e) {
            return new Result(false, $e->getTrace());
        }
    }

    /**
     * @param array $data
     * @return Result
     */
    public function updateSubAccount(array $data)
    {
        try {
            $checkArray = $this->checkKeys(['accountSid', 'status'], array_keys($data));
            if ($checkArray['count'] > 0) {
                return new Result(false, $checkArray['keys'], 'Use the following key(s)');
            }
            $twilio = new Client($this->twilioAccountSid, $this->authToken);
            $account = $twilio->api->v2010->accounts($data['accountSid'])->update(['status' => $data['status']]);
            return new Result(true, $account->toArray(), 'Account' . ucfirst($data['status']), 'Account updated');
        } catch (Exception $e) {
            $errorArray = [];
            if (isset($data['debug']) && $data['debug'] === true) {
                $errorArray['trace'] = $e->getTrace();
            }
            return new Result(false, $errorArray, 'subAccount', $e->getMessage());
        }
    }

    /**
     * @param array $correctKeys
     * @param array $givenKeys
     * @return array
     */
    private function checkKeys(array $correctKeys, array $givenKeys)
    {
        $result = array_diff($correctKeys, $givenKeys);
        return ['count' => count($result), 'keys' => $result];
    }

    /**
     * @param array $data
     * @throws Exception
     */
    public function checkAuthenticationFields(array $data)
    {
        if (empty($data['service']['accountSid'])) {
            throw new Exception('accountSid field missing');
        }
        if (empty($data['subAccount']['authToken'])) {
            throw new Exception('authToken field missing');
        }
        if (empty($data['service']['sid'])) {
            throw new Exception('service sid field missing');
        }
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function getChannelRoleFields(array $data)
    {
        $this->checkAuthenticationFields($data);
        if (empty($data['service']['defaultChannelRoleSid'])) {
            throw new Exception('defaultChannelRoleSid field missing');
        }

        $updateRoleData['accountSid'] = $data['service']['accountSid'];
        $updateRoleData['authToken'] = $data['subAccount']['authToken'];
        $updateRoleData['serviceSid'] = $data['service']['sid'];
        $updateRoleData['roleSid'] = $data['service']['defaultChannelRoleSid'];
        $updateRoleData['permission'] =
            [
                "editOwnMessage",
                "deleteAnyMessage",
                "addMember",
                "editChannelAttributes",
                "editAnyMemberAttributes",
                "sendMessage",
                "editAnyMessage",
                "leaveChannel",
                "editChannelName",
                "editAnyMessageAttributes",
                "deleteOwnMessage",
                "editOwnMessageAttributes",
                "removeMember",
                "inviteMember",
                "sendMediaMessage",
                "editOwnMemberAttributes",
                "destroyChannel"
            ];
        return $updateRoleData;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function getServiceRoleFields(array $data)
    {
        if (empty($data['service']['defaultServiceRoleSid'])) {
            throw new Exception('defaultServiceRoleSid field missing');
        }
        $updateRoleData['accountSid'] = $data['service']['accountSid'];
        $updateRoleData['authToken'] = $data['subAccount']['authToken'];
        $updateRoleData['serviceSid'] = $data['service']['sid'];
        $updateRoleData['roleSid'] = $data['service']['defaultServiceRoleSid'];
        $updateRoleData['permission'] =
            [
                "createChannel",
                "joinChannel",
                "editOwnUserInfo",
                "editAnyUserInfo",
                "destroyChannel",
                "inviteMember",
                "removeMember",
                "editChannelName",
                "editChannelAttributes",
                "addMember",
                "editAnyMemberAttributes",
                "editOwnMemberAttributes",
                "editAnyMessage",
                "editOwnMessage",
                "editAnyMessageAttributes",
                "editOwnMessageAttributes",
                "deleteAnyMessage",
                "deleteOwnMessage"
            ];
        return $updateRoleData;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function getServiceOptionsFields(array $data)
    {
        $this->checkAuthenticationFields($data);
        $updateServiceData['accountSid'] = $data['service']['accountSid'];
        $updateServiceData['authToken'] = $data['subAccount']['authToken'];
        $updateServiceData['serviceSid'] = $data['service']['sid'];
        $updateServiceData['options'] =
            [
                'reachabilityEnabled' => true,
                'readStatusEnabled' => true,
                'notificationsNewMessageEnabled' => true,
                'notificationsAddedToChannelEnabled' => true,
                'notificationsNewMessageBadgeCountEnabled' => true,
                'limitsChannelMembers' => 1000,
                'limitsUserChannels' => 1000,
                'mediaCompatibilityMessage' => 'Sending Files ...',
                'notificationsLogEnabled' => true,
                'notificationsInvitedToChannelEnabled' => true,
                'notificationsRemovedFromChannelEnabled' => true,
            ];

        return $updateServiceData;
    }


}