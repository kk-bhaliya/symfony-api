<?php

namespace App\Criteria;

use Doctrine\Common\Collections\Criteria;

class Common {
    public static function notArchived($alias = ''): Criteria
    {
        $alias = strlen($alias) > 0 ? $alias .= '.' : '';
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq($alias.'isArchive', 0))
        ;
    }
}