<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\Location;
use App\Entity\Station;
use App\Entity\User;
use DateTime;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Driver Controller
 * @Route("/api",name="api_")
 */
class DriverRouteController extends BaseController
{
    /**
     * @Rest\Post("/driver_route/create")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function createDriverRouteAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/driver_route/update")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function updateDriverRouteAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/driver_route/get")
     * @return JsonResponse
     * @throws Exception
     */
    public function getDriverRouteAction(): JsonResponse
    {
        return $this->json($this->getAllValues());
    }
}
