<?php

namespace App\Controller;

use App\Entity\Location;
use App\Entity\Station;
use App\Entity\Vehicle;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Vehicle Controller
 * @Route("/api",name="api_")
 */
class VehicleController extends BaseController
{
    /**
     * @Rest\Post("/vehicle/create")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createVehicleAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/vehicle/update")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function updateVehicleAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/vehicle/get")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getVehicleAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/vehicle/fetch/locations/form")
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function fetchLocationsForm(Request $request): Response
    {
        $data = $request->request->all();
        $response = $this
            ->entityManager
            ->getRepository(Location::class)
            ->findBy(['company' => $data['company']]);
        $result = [];
        foreach ($response as $location) {
            $tmp = [];
            if ($location->getStation()) {
                $tmp['name'] = $location->getStation()->getName();
            } else {
                $tmp['name'] = $location->getName();
            }
            $tmp['value'] = $location->getId();
            $result[] = $tmp;
        }
        $other = [];
        $other['name'] = 'Other';
        $other['value'] = 'Other';
        $result[] = $other;
        return new JsonResponse($result);
    }

    /**
     * @Rest\Post("/vehicle/fetch/locations")
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     * @throws ExceptionInterface
     */
    public function fetchLocations(Request $request): Response
    {
        $data = $request->request->all();
        $response = $this
            ->entityManager
            ->getRepository(Location::class)
            ->findBy(['company' => $data['company']]);
        $encoder = new JsonEncoder();
        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::CALLBACKS => [
                'station' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                    if ($innerObject instanceof Station) {
                        return [
                            'id' => $innerObject->getId(),
                            'name' => $innerObject->getName(),
                            'code' => $innerObject->getCode(),
                        ];
                    }
                    return null;
                }
            ],
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);

        $serializer = new Serializer([$normalizer], [$encoder]);
        $result = $serializer->serialize($response, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => ['company', 'vehicles'], AbstractObjectNormalizer::ENABLE_MAX_DEPTH => false]);
        return new Response(
            $result,
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
    }

    /**
     * @Rest\Post("/vehicle/fetch/list")
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     */
    public function fetchVehicles(Request $request): Response
    {
        $data = $request->request->all();

        $response = $this
            ->entityManager
            ->getRepository(Vehicle::class)
            ->findBy(['company' => $data['company']], null, $data['limit'], $data['offset']);

        $defaultContext = [
            AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                return $object->getId();
            },
            AbstractNormalizer::CALLBACKS => [
                'station' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                    if ($innerObject instanceof Station) {
                        return [
                            'id' => $innerObject->getId(),
                            'name' => $innerObject->getName(),
                            'code' => $innerObject->getCode(),
                        ];
                    }
                    return null;
                },
            ],
        ];
        $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);
        $result = $serializer->serialize(
            $response,
            'json',
            [
                AbstractNormalizer::IGNORED_ATTRIBUTES =>
                [
                    'company',
                    'vehicleDriverRecords',
                    'driverRoutes',
                    'tempDriverRoutes',
                    'vehicles',
                ],
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => false,
            ]
        );
        return new Response(
            $result,
            Response::HTTP_OK,
            ['Content-type' => 'application/json']
        );
    }

    /**
     * @Rest\Post("/vehicle/csv/upload")
     * @param Request $request
     * @param ObjectNormalizer $objectNormalizer
     * @return JsonResponse
     */
    public function uploadCsv(Request $request, EntityManagerInterface $em)
    {
        $companyId = $request->request->get('company');
        $company = $em->getRepository('App\Entity\Company')->find((int) $companyId);
        $userId = $request->request->get('user');
        $parentUser = $em->getRepository('App\Entity\User')->find($userId);
        $fileName = $request->request->get('filePath');

        if ($fileName != '') {
            $file = fopen($fileName, "r");
            $firstRow = true;
            $log = '';
            $cnt = $editCount = $addCount = 0;
            while (($column = fgetcsv($file, 10000, ",")) !== false) {
                $validate = true;

                if ($cnt == 0) {
                    if (trim($column[0]) != 'Station*') {
                        $log = 'csv column Station not there';
                        break;
                    } elseif (trim($column[1]) != 'Vehicle ID*') {
                        $log = 'csv column Vehicle ID not there';
                        break;
                    } elseif (trim($column[2]) != 'Status*') {
                        $log = 'csv column Status not there';
                        break;
                    } elseif (trim($column[3]) != 'Notes') {
                        $log = 'csv column Notes not there';
                        break;
                    } elseif (trim($column[4]) != 'Make*') {
                        $log = 'csv column Make not there';
                        break;
                    } elseif (trim($column[5]) != 'Model*') {
                        $log = 'csv column Model not there';
                        break;
                    } elseif (trim($column[6]) != 'Model Type') {
                        $log = 'csv column Model Type not there';
                        break;
                    } elseif (trim($column[7]) != 'Year*') {
                        $log = 'csv column Year not there';
                        break;
                    } elseif (trim($column[8]) != 'Height (inches)') {
                        $log = 'csv column Height (inches) not there';
                        break;
                    } elseif (trim($column[9]) != 'Current Overnight Parking Location') {
                        $log = 'csv column Current Overnight Parking Location not there';
                        break;
                    } elseif (trim($column[10]) != 'License Plate State') {
                        $log = 'csv column License Plate State not there';
                        break;
                    } elseif (trim($column[11]) != 'License Plate #') {
                        $log = 'csv column License Plate # not there';
                        break;
                    } elseif (trim($column[12]) != 'License Plate Exp_(Y-M-D)') {
                        $log = 'csv column License Plate Exp. not there';
                        break;
                    } elseif (trim($column[13]) != 'Toll Card ID') {
                        $log = 'csv column Toll Card ID not there';
                        break;
                    } elseif (trim($column[14]) != 'Date - Start_(Y-M-D)') {
                        $log = 'csv column Date - Start not there';
                        break;
                    } elseif (trim($column[15]) != 'Date - End_(Y-M-D)') {
                        $log = 'csv column Date - End not there';
                        break;
                    } elseif (trim($column[16]) != 'VIN*') {
                        $log = 'csv column VIN not there';
                        break;
                    } elseif (trim($column[17]) != 'Tank Capacity (gallons)') {
                        $log = 'csv column Tank Capacity (gallons) not there';
                        break;
                    } elseif (trim($column[18]) != 'Gasoline Type') {
                        $log = 'csv column Gasoline Type not there';
                        break;
                    } elseif (trim($column[19]) != 'Fuel Card #') {
                        $log = 'csv column Fuel Card # not there';
                        break;
                    } elseif (trim($column[20]) != 'Insurance Company') {
                        $log = 'csv column Insurance Company not there';
                        break;
                    } elseif (trim($column[21]) != 'Policy Number') {
                        $log = 'csv column Policy Number not there';
                        break;
                    } elseif (trim($column[22]) != 'Policy Expiration_(Y-M-D)') {
                        $log = 'csv column Policy Expiration not there';
                        break;
                    } elseif (trim($column[23]) != 'Upload of Insurance Card') {
                        $log = 'csv column Upload of Insurance Card not there';
                        break;
                    } elseif (trim($column[24]) != 'Ownership Type') {
                        $log = 'csv column Ownership Type not there';
                        break;
                    }
                }
                if ($cnt == 1) {
                    if (trim($column[25]) != 'Leasing Company') {
                        $log = 'csv column Leasing Company not there';
                        break;
                    } elseif (trim($column[26]) != 'Agreement #') {
                        $log = 'csv column Agreement # not there';
                        break;
                    } elseif (trim($column[27]) != 'Price/mo') {
                        $log = 'csv column Price/mo not there';
                        break;
                    } elseif (trim($column[28]) != 'Deposit') {
                        $log = 'csv column Deposit not there';
                        break;
                    } elseif (trim($column[29]) != 'Start Date_(Y-M-D)') {
                        $log = 'csv column Leasing start date not there';
                        break;
                    } elseif (trim($column[30]) != 'End Date_(Y-M-D)') {
                        $log = 'csv column leasing end date not there';
                        break;
                    } elseif (trim($column[36]) != 'Rental Agreement #') {
                        $log = 'csv column Rental agreement not there';
                        break;
                    } elseif (trim($column[37]) != 'Price/mo') {
                        $log = 'csv column Price/mo not there';
                        break;
                    } elseif (trim($column[38]) != 'Deposit') {
                        $log = 'csv column Rental Deposit not there';
                        break;
                    } elseif (trim($column[39]) != 'Contract Start Date_(Y-M-D)') {
                        $log = 'csv column Contract start date not there';
                        break;
                    } elseif (trim($column[40]) != 'Contract End Date_(Y-M-D)') {
                        $log = 'csv column Contract end date not there';
                        break;
                    }
                }
                if ($cnt == 2) {
                    if (trim($column[31]) != 'Odometer') {
                        $log = 'csv column leased initial codition odometer not there';
                        break;
                    } elseif (trim($column[32]) != 'Date_(Y-M-D)') {
                        $log = 'csv column leased initial codition date not there';
                        break;
                    } elseif (trim($column[33]) != 'Odometer') {
                        $log = 'csv column leased return condition odometer not there';
                        break;
                    } elseif (trim($column[34]) != 'Date_(Y-M-D)') {
                        $log = 'csv column leased return condition date not there';
                        break;
                    } elseif (trim($column[42]) != 'Odometer') {
                        $log = 'csv column Rental initial condition odometer not there';
                        break;
                    } elseif (trim($column[43]) != 'Date_(Y-M-D)') {
                        $log = 'csv column Rental initial condition date not there';
                        break;
                    } elseif (trim($column[44]) != 'Odometer') {
                        $log = 'csv column Rental return condition odometer not there';
                        break;
                    } elseif (trim($column[45]) != 'Date_(Y-M-D)') {
                        $log = 'csv column Rental return condition date not there';
                        break;
                    } elseif (trim($column[46]) != 'Price') {
                        $log = 'csv column Owned purchase condition price not there';
                        break;
                    } elseif (trim($column[47]) != 'Odometer') {
                        $log = 'csv column Owned purchase condition odometer not there';
                        break;
                    } elseif (trim($column[48]) != 'Date_(Y-M-D)') {
                        $log = 'csv column Owned purchase condition date not there';
                        break;
                    } elseif (trim($column[49]) != 'Price') {
                        $log = 'csv column Owned sold condition price not there';
                        break;
                    } elseif (trim($column[50]) != 'Odometer') {
                        $log = 'csv column Owned sold condition odometer not there';
                        break;
                    } elseif (trim($column[51]) != 'Date_(Y-M-D)') {
                        $log = 'csv column Owned sold condition date not there';
                        break;
                    }
                }
                if ($cnt > 2) {
                    $tempLog = '';
                    $vehicle = null;
                    if (trim($column[1]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Vehicle ID';
                        } else {
                            $tempLog = $tempLog . 'please fill the Vehicle ID';
                        }
                        $validate = false;
                    } else {
                        $vehicle = $em->getRepository('App\Entity\Vehicle')->findOneBy(['vehicleId' => trim($column[1]), 'isArchive'=>false]);
                        if ($vehicle) {
                            if($vehicle->getCompany()->getId() != $companyId){
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ',Vehicle ID already exist in other company';
                                } else {
                                    $tempLog = $tempLog . 'Vehicle ID already exist in other company';
                                }
                                $validate = false;
                            }
                        }
                    }

                    $station = null;
                    if (trim($column[0]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Station';
                        } else {
                            $tempLog = $tempLog . 'please fill the Station';
                        }
                        $validate = false;
                    } else {
                        $station = $em->getRepository('App\Entity\Station')->findOneBy(['company' => $companyId, 'code' => trim($column[0]), 'isArchive' => false]);
                        if (!$station) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please enter correct the Station';
                            } else {
                                $tempLog = $tempLog . 'please enter correct the Station';
                            }
                            $validate = false;
                        }
                    }
                    if (trim($column[2]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Status';
                        } else {
                            $tempLog = $tempLog . 'please fill the Status';
                        }
                        $validate = false;
                    } elseif (!in_array(trim($column[2]), array('Active', 'Inactive', 'Under Repair', 'Being Serviced'))) {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Status with (Active, Inactive, Under Repair, Being Serviced)';
                        } else {
                            $tempLog = $tempLog . 'please fill the Status with (Active, Inactive, Under Repair, Being Serviced)';
                        }
                        $validate = false;
                    }
                    if (trim($column[16]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the VIN';
                        } else {
                            $tempLog = $tempLog . 'please fill the VIN';
                        }
                        $validate = false;
                    }
                    if (trim($column[4]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Make';
                        } else {
                            $tempLog = $tempLog . 'please fill the Make';
                        }
                        $validate = false;
                    }
                    if (trim($column[5]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Model';
                        } else {
                            $tempLog = $tempLog . 'please fill the Model';
                        }
                        $validate = false;
                    }
                    if (trim($column[6]) != '' && !in_array(strtolower(trim($column[6])), array('cargo van', 'xl cargo van', 'box', 'step van'))) {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the model type with (Cargo Van, XL Cargo Van, Box, Step Van)';
                        } else {
                            $tempLog = $tempLog . 'please fill the model type with (Cargo Van, XL Cargo Van, Box, Step Van)';
                        }
                        $validate = false;
                    }
                    if (trim($column[7]) == '') {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the Year';
                        } else {
                            $tempLog = $tempLog . 'please fill the Year';
                        }
                        $validate = false;
                    }
                    if (trim($column[24]) != '' && !in_array(strtolower(trim($column[24])), array('owned', 'leased', 'rented'))) {
                        if ($tempLog != ''){
                            $tempLog = $tempLog . ', please fill the ownership type with (Owned, Leased, Rented)';
                        } else {
                            $tempLog = $tempLog . 'please fill the ownership type with (Owned, Leased, Rented)';
                        }
                        $validate = false;
                    }
                    $licensePlateExpiration = null;
                    $tollCardStartDate = null;
                    if ($column[14] != '') {
                        $tollCardStartDate = strlen(trim($column[14])) > 0 ? new \DateTime(trim($column[14])) : null;
                        if (!is_a($tollCardStartDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Toll card start Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Toll card start Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $tollCardEndDate = null;
                    if ($column[15] != '') {
                        $tollCardEndDate = strlen(trim($column[15])) > 0 ? new \DateTime(trim($column[15])) : null;
                        if (!is_a($tollCardEndDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Toll card end date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Toll card end date format validation';
                            }
                            $validate = false;
                        }
                    }
                    $leasedStartDate = null;
                    if ($column[29] != '') {
                        $leasedStartDate = strlen(trim($column[29])) > 0 ? new \DateTime(trim($column[29])) : null;
                        if (!is_a($leasedStartDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the leased start Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the leased start Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $leasedEndDate = null;
                    if ($column[30] != '') {
                        $leasedEndDate = strlen(trim($column[30])) > 0 ? new \DateTime(trim($column[30])) : null;
                        if (!is_a($leasedEndDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the leased end date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the leased end date format validation';
                            }
                            $validate = false;
                        }
                    }
                    $leasedInitialConditionDate = null;
                    if ($column[32] != '') {
                        $leasedInitialConditionDate = strlen(trim($column[32])) > 0 ? new \DateTime(trim($column[32])) : null;
                        if (!is_a($leasedInitialConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Initial Condition Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Initial Condition Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $leasedReturnConditionDate = null;
                    if ($column[34] != '') {
                        $leasedReturnConditionDate = strlen(trim($column[34])) > 0 ? new \DateTime(trim($column[34])) : null;
                        if (!is_a($leasedReturnConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Return Condition Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Return Condition Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $rentedStartDate = null;
                    if ($column[39] != '') {
                        $rentedStartDate = strlen(trim($column[39])) > 0 ? new \DateTime(trim($column[39])) : null;
                        if (!is_a($rentedStartDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Rented Start Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Rented Start Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $rentedEndDate = null;
                    if ($column[40] != '') {
                        $rentedEndDate = strlen(trim($column[40])) > 0 ? new \DateTime(trim($column[40])) : null;
                        if (!is_a($rentedEndDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Rented End Date Format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Rented End Date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $rentalInitialConditionDate = null;
                    if ($column[43] != '') {
                        $rentalInitialConditionDate = strlen(trim($column[43])) > 0 ? new \DateTime(trim($column[43])) : null;
                        if (!is_a($rentalInitialConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Rental initial condition date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Rental initial condition date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $rentalReturnConditionDate = null;
                    if ($column[45] != '') {
                        $rentalReturnConditionDate = strlen(trim($column[45])) > 0 ? new \DateTime(trim($column[45])) : null;
                        if (!is_a($rentalReturnConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Rental return condition date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Rental return condition date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $ownedPurchaseConditionDate = null;
                    if ($column[48] != '') {
                        $ownedPurchaseConditionDate = strlen(trim($column[48])) > 0 ? new \DateTime(trim($column[48])) : null;
                        if (!is_a($ownedPurchaseConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Owned purchase condition date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Owned purchase condition date Format validation';
                            }
                            $validate = false;
                        }
                    }
                    $ownedSoldConditionDate = null;
                    if ($column[51] != '') {
                        $ownedSoldConditionDate = strlen(trim($column[51])) > 0 ? new \DateTime(trim($column[51])) : null;
                        if (!is_a($ownedSoldConditionDate, 'DateTime')) {
                            if ($tempLog != ''){
                                $tempLog = $tempLog . ', please fill the Owned sold condition date format validation';
                            } else {
                                $tempLog = $tempLog . 'please fill the Owned sold condition date Format validation';
                            }
                            $validate = false;
                        }
                    }

                    if ($tempLog != '') {
                        $log = $log . '(' . $cnt . ')' . implode(" | ", $column) . '
                              Error - ' . $tempLog . '

                              ';
                    }

                    if ($validate == true) {
                        if (empty($vehicle)) {
                            
                            /*if (strlen(trim($column[9])) > 0) {
                                $location = $em->getRepository('App\Entity\Location')->findOneBy(['station' => trim($column[0]), 'isArchive' => false]);
                                if (!$location) {
                                    $location = new Location();
                                    $location->setAddress(trim($column[9]));
                                    $location->setCompany($company);
                                    $em->persist($location);
                                    $em->flush();
                                }
                            } else {
                                $location = null;
                            }*/

                            $noteVal = htmlspecialchars(trim($column[3]));
                            $vehicle = new Vehicle();
                            $vehicle->setStation($station);
                            $vehicle->setCompany($company);
                            $vehicle->setStatus(ucfirst(trim($column[2])));
                            $vehicle->setVehicleId(trim($column[1]));
                            $vehicle->setOvernightParkingLocation(trim($column[9]));
                          /* if ($location) {
                                $vehicle->setLocation($location);
                            }*/

                            $vehicle->setNotes($noteVal);
                            $vehicle->setVin(trim($column[16]));
                            $vehicle->setState(trim($column[10]));
                            $vehicle->setLicensePlate(trim($column[11]));
                            $vehicle->setLicensePlateExpiration(trim($column[12]));
                            $vehicle->setTollCardNumber(trim($column[13]));
                            $vehicle->setMake(trim($column[4]));
                            $vehicle->setModel(trim($column[5]));
                            $vehicle->setModelType(str_replace(' ', '_', strtolower(trim($column[6]))));
                            $vehicle->setYear(strtolower(trim($column[7])));
                            $vehicle->setHeight(trim($column[8]));
                            $vehicle->setFuelCardNumber(trim($column[19]));
                            if (is_a($tollCardStartDate, 'DateTime')) {
                                $vehicle->setTollCardStartDate($tollCardStartDate);
                            }
                            if (is_a($tollCardEndDate, 'DateTime')) {
                                $vehicle->setTollCardEndDate($tollCardEndDate);
                            }
                            $vehicle->setInsuranceCompany(trim($column[20]));
                            $vehicle->setPolicyNumber(trim($column[21]));
                            $vehicle->setTankCapacity(trim($column[17]));
                            $vehicle->setGasType(trim($column[18]));
                            $vehicle->setType(ucfirst(trim($column[24])));
                            $vehicle->setLeasingCompany($column[25] ? trim($column[25]) : NULL);
                            $vehicle->setLeasingAgreementNumber($column[26] ? trim($column[26]) : NULL);
                            $vehicle->setLeasingPricePerMonth($column[27] ? trim($column[27]) : NULL);
                            $vehicle->setLeasingDeposit($column[28] ? trim($column[28]) : NULL);
                            if (is_a($leasedStartDate, 'DateTime')) {
                                $vehicle->setLeasingStartDate($leasedStartDate);
                            }
                            if (is_a($leasedEndDate, 'DateTime')) {
                                $vehicle->setLeasingEndDate($leasedEndDate);
                            }
                            $vehicle->setLeasingInitialConditionOdometer($column[31] ? trim($column[31]) : NULL);
                            if (is_a($leasedInitialConditionDate, 'DateTime')) {
                                $vehicle->setLeasingInitialConditionDate($leasedInitialConditionDate);
                            }
                            $vehicle->setLeasingReturnConditionOdometer($column[33] ? trim($column[33]) : NULL);
                            if (is_a($leasedReturnConditionDate, 'DateTime')) {
                                $vehicle->setLeasingReturnConditionDate($leasedReturnConditionDate);
                            }
                            $vehicle->setRentalAgreementNumber($column[36] ? trim($column[36]) : NULL);
                            $vehicle->setRentalPricePerMonth($column[37] ? trim($column[37]) : NULL);
                            $vehicle->setRentalDeposit($column[38] ? trim($column[38]) : NULL);
                            if (is_a($rentedStartDate, 'DateTime')) {
                                $vehicle->setRentalStartDate($rentedStartDate);
                            }
                            if (is_a($rentedEndDate, 'DateTime')) {
                                $vehicle->setRentalEndDate($rentedEndDate);
                            }
                            $vehicle->setRentalInitialConditionOdometer($column[42] ? trim($column[42]) : NULL);
                            if (is_a($rentalInitialConditionDate, 'DateTime')) {
                                $vehicle->setRentalInitialConditionDate($rentalInitialConditionDate);
                            }
                            $vehicle->setRentalReturnConditionOdometer($column[44] ? trim($column[44]) : NULL);
                            if (is_a($rentalReturnConditionDate, 'DateTime')) {
                                $vehicle->setRentalReturnConditionDate($rentalReturnConditionDate);
                            }
                            $vehicle->setOwnedPurchaseConditionPrice($column[46] ? trim($column[46]) : NULL);
                            $vehicle->setOwnedPurchaseConditionOdometer($column[47] ? trim($column[47]) : NULL);
                            if (is_a($ownedPurchaseConditionDate, 'DateTime')) {
                                $vehicle->setOwnedPurchaseConditionDate($ownedPurchaseConditionDate);
                            }
                            $vehicle->setOwnedSoldConditionPrice($column[49] ? trim($column[49]) : NULL);
                            $vehicle->setOwnedSoldConditionOdometer($column[50] ? trim($column[50]) : NULL);
                            if (is_a($ownedSoldConditionDate, 'DateTime')) {
                                $vehicle->setOwnedSoldConditionDate($ownedSoldConditionDate);
                            }
                            $em->persist($vehicle);
                            $em->flush();
                        } else {
                            $noteVal = htmlspecialchars(trim($column[3]));
                           /* if (strlen(trim($column[9])) > 0) {
                                $location = $em->getRepository('App\Entity\Location')->findOneBy(['station' => trim($column[0]), 'isArchive' => false]);
                                if (!$location) {
                                    $location = new Location();
                                    $location->setAddress(trim($column[9]));
                                    $location->setCompany($company);
                                    $em->persist($location);
                                    $em->flush();
                                }
                            } else {
                                $location = null;
                            }*/

                            $vehicle->setStation($station);
                            $vehicle->setCompany($company);
                            $vehicle->setStatus(ucfirst(trim($column[2])));
                            $vehicle->setVehicleId(trim($column[1]));
                            $vehicle->setOvernightParkingLocation(trim($column[9]));
                            /*if ($location) {
                                $vehicle->setLocation($location);
                            }*/

                            $vehicle->setNotes($noteVal);
                            $vehicle->setVin(trim($column[16]));
                            $vehicle->setState(trim($column[10]));
                            $vehicle->setLicensePlate(trim($column[11]));
                            $vehicle->setLicensePlateExpiration(trim($column[12]));
                            $vehicle->setTollCardNumber(trim($column[13]));
                            $vehicle->setMake(trim($column[4]));
                            $vehicle->setModel(trim($column[5]));
                            $vehicle->setModelType(str_replace(' ', '_', strtolower(trim($column[6]))));
                            $vehicle->setYear(strtolower(trim($column[7])));
                            $vehicle->setHeight(trim($column[8]));
                            $vehicle->setFuelCardNumber(trim($column[19]));
                            if (is_a($tollCardStartDate, 'DateTime')) {
                                $vehicle->setTollCardStartDate($tollCardStartDate);
                            }
                            if (is_a($tollCardEndDate, 'DateTime')) {
                                $vehicle->setTollCardEndDate($tollCardEndDate);
                            }
                            $vehicle->setInsuranceCompany(trim($column[20]));
                            $vehicle->setPolicyNumber(trim($column[21]));
                            $vehicle->setTankCapacity(trim($column[17]));
                            $vehicle->setGasType(trim($column[18]));
                            $vehicle->setType(ucfirst(trim($column[24])));
                            $vehicle->setLeasingCompany($column[25] ? trim($column[25]) : NULL);
                            $vehicle->setLeasingAgreementNumber($column[26] ? trim($column[26]) : NULL);
                            $vehicle->setLeasingPricePerMonth($column[27] ? trim($column[27]) : NULL);
                            $vehicle->setLeasingDeposit($column[28] ? trim($column[28]) : NULL);
                            if (is_a($leasedStartDate, 'DateTime')) {
                                $vehicle->setLeasingStartDate($leasedStartDate);
                            }
                            if (is_a($leasedEndDate, 'DateTime')) {
                                $vehicle->setLeasingEndDate($leasedEndDate);
                            }
                            $vehicle->setLeasingInitialConditionOdometer($column[31] ? trim($column[31]) : NULL);
                            if (is_a($leasedInitialConditionDate, 'DateTime')) {
                                $vehicle->setLeasingInitialConditionDate($leasedInitialConditionDate);
                            }
                            $vehicle->setLeasingReturnConditionOdometer($column[33] ? trim($column[33]) : NULL);
                            if (is_a($leasedReturnConditionDate, 'DateTime')) {
                                $vehicle->setLeasingReturnConditionDate($leasedReturnConditionDate);
                            }
                            $vehicle->setRentalAgreementNumber($column[36] ? trim($column[36]) : NULL);
                            $vehicle->setRentalPricePerMonth($column[37] ? trim($column[37]) : NULL);
                            $vehicle->setRentalDeposit($column[38] ? trim($column[38]) : NULL);
                            if (is_a($rentedStartDate, 'DateTime')) {
                                $vehicle->setRentalStartDate($rentedStartDate);
                            }
                            if (is_a($rentedEndDate, 'DateTime')) {
                                $vehicle->setRentalEndDate($rentedEndDate);
                            }
                            $vehicle->setRentalInitialConditionOdometer($column[42] ? trim($column[42]) : NULL);
                            if (is_a($rentalInitialConditionDate, 'DateTime')) {
                                $vehicle->setRentalInitialConditionDate($rentalInitialConditionDate);
                            }
                            $vehicle->setRentalReturnConditionOdometer($column[44] ? trim($column[44]) : NULL);
                            if (is_a($rentalReturnConditionDate, 'DateTime')) {
                                $vehicle->setRentalReturnConditionDate($rentalReturnConditionDate);
                            }
                            $vehicle->setOwnedPurchaseConditionPrice($column[46] ? trim($column[46]) : NULL);
                            $vehicle->setOwnedPurchaseConditionOdometer($column[47] ? trim($column[47]) : NULL);
                            if (is_a($ownedPurchaseConditionDate, 'DateTime')) {
                                $vehicle->setOwnedPurchaseConditionDate($ownedPurchaseConditionDate);
                            }
                            $vehicle->setOwnedSoldConditionPrice($column[49] ? trim($column[49]) : NULL);
                            $vehicle->setOwnedSoldConditionOdometer($column[50] ? trim($column[50]) : NULL);
                            if (is_a($ownedSoldConditionDate, 'DateTime')) {
                                $vehicle->setOwnedSoldConditionDate($ownedSoldConditionDate);
                            }
                            $em->persist($vehicle);
                            $em->flush();
                        }
                    }
                }
                $cnt++;
            }
        }

        return $this->jsonResponse(true, '', 200, 'user', $log);
    }

}
