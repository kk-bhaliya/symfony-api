<?php
namespace App\Controller;

use App\Criteria\Common;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\Shift;
use App\Entity\User;
use App\Services\CortexService;
use App\Utils\GlobalUtility;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * CortexSchedule Controller
 * @Route("/cortex", name="cortex_")
 */
class CortexLog extends BaseController
{
    /**
     * @Route(
     *     name="processCortexLog",
     *     path="/log",
     *     methods={"POST"},
     * )
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function processCortexLog(Request $request, CortexService $cortexService)
    {
        try {
            $json = json_decode($request->getContent());
        } catch (\Exception $e) {
            return $this->jsonException($e);
        }

        try {
            $process = $cortexService->processLog($json);
            $routes = $process->routes;
            $allRouteCodesQuery = $process->codesQuery;
            $cortexSettings = $process->cortexSettings;
            $company = $process->company;
            $station = $process->station;
        } catch (\Exception $e) {
            return $this->jsonException($e);
        }

        if ( isset($json->cookies) ) {
            $cortexSettings->setCortexCookies(json_encode($json->cookies));
            $this->entityManager->persist($cortexSettings);
            $this->entityManager->flush();
        }

        // we require a backup shift type
        /** @var Shift $shift */
        try {
            $qb = $this->entityManager->createQueryBuilder();
            $shift = $qb->select('st')
                ->from(Shift::class, 'st')
                ->addCriteria(\App\Criteria\Common::notArchived('st'))
                ->andWhere('st.station = :station')
                ->setParameter('station', $station)
                ->andWhere('st.category = 7')
                ->orderBy('st.isArchive', 'ASC')
                ->setMaxResults(1)
                ->getQuery()
                ->getSingleResult();
        } catch (\Exception $e) {
            return $this->jsonException(new \Exception('No system shift available. Make sure you have at least one active system shift type'));
        }

        // bootstrap
        $response = [
            'success' => true,
            'new' => 0,
            'assigned' => 0,
            'open' => 0,
        ];

        $qb = $this->entityManager->createQueryBuilder();
        $owner = $qb->select('u')
            ->from(User::class, 'u')
            ->join('u.userRoles', 'ur')
            ->addCriteria(Common::notArchived('u'))
            ->andWhere('u.company = :company')
            ->setParameter('company', $company)
            ->andWhere('ur.name = :owner')
            ->setParameter('owner', 'ROLE_OWNER')
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult()
        ;

        // delete existing DriverRoutesCodes
        $dql = $this->entityManager->createQuery("
            UPDATE
                \App\Entity\DriverRouteCode rc
            SET
                rc.isArchive = 1
            WHERE
                rc.code IN ({$allRouteCodesQuery})
                AND rc.dateCreated LIKE ?1
                AND rc.station = ?2
        ");
        $dql->execute([
            1 => GlobalUtility::newUTCDateTime()->format('Y-m-d') . '%',
            2 => $station
        ]);

        // add to scheduled DriverRoutes
        $qb = $this->entityManager->createQueryBuilder();
        $scheduledDrivers = $qb
            ->select('dr')
            ->from(DriverRoute::class, 'dr')
            ->leftJoin('dr.driver', 'd')
            ->addSelect('d')
            ->leftJoin('d.user', 'u')
            ->leftJoin('u.userRoles', 'ur')
            ->addCriteria(\App\Criteria\Common::notArchived('dr'))
            ->addCriteria(\App\Criteria\Common::notArchived('d'))
            ->andWhere('dr.dateCreated = CURRENT_DATE()')
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->andWhere(
                $qb->expr()->notIn(
                    'ur.name',
                    ['ROLE_STATION_MANAGER', 'ROLE_ASSISTANT_MANAGER', 'ROLE_OWNER']
                )
            )
            ->getQuery()
            ->getResult()
        ;

        $openShifts = $this->entityManager->createQueryBuilder()
            ->select('dr')
            ->from(DriverRoute::class, 'dr')
            ->addCriteria(\App\Criteria\Common::notArchived('dr'))
            ->andWhere('dr.dateCreated = CURRENT_DATE()')
            ->andWhere('dr.station = :station')
            ->setParameter('station', $station)
            ->andWhere('dr.driver IS NULL')
            ->getQuery()
            ->getResult()
        ;

        // create new DriverRoutesCodes
        foreach ($routes as $route) {
            $code = new DriverRouteCode();
            $code->setCode($route->routeCode);
            $code->setIsArchive(0);
            $code->setCompany($company);
            $code->setStation($station);
            $this->entityManager->persist($code);

            $highest = max(array_column((array)$route->transporterDeliveryProgress, 'countOfTotalPackages'));
            $transporterId = array_keys(array_filter((array)$route->transporterDeliveryProgress, function ($driver) use ($highest) {
                return $driver->countOfTotalPackages === $highest;
            }));
            $transporterId = $transporterId[0];

            $scheduled = array_filter(array_merge($scheduledDrivers , $openShifts), function ($dr) use ($route) {
                /** @var DriverRoute $dr */
                return
                    (
                        // has driver with transporter id
                        !empty($dr->getDriver())
                        && in_array($dr->getDriver()->getTransporterID(), $route->transporterIdList)
                    )
                    ||
                    (
                        // open route
                        $dr->getIsOpenShift()
                        && $dr->getDriverRouteCodes()->filter(function ($routeCode) use ($route) {
                            /** @var DriverRouteCode $routeCode */
                            return $routeCode->getCode() === $route->routeCode;
                        })->isEmpty() === false
                    )
                ;
            });

            if (!empty($scheduled)) {
                foreach ($scheduled as $dr) {
                    /** @var DriverRoute $dr */
                    // shared code, but main driver does not have a shift
                    if (!empty($dr->getDriver()) && $dr->getDriver()->getTransporterID() !== $transporterId) {
                        $driver = $this->entityManager
                            ->getRepository(Driver::class)
                            ->findOneBy([
                                'transporterID' => $transporterId,
                                'isArchive' => false
                            ]);

                        $new = $this->newDriverRoute($shift, $driver, $code);

                        if ($driver)
                            ++$response['assigned'];
                        else
                            ++$response['open'];

                        $this->entityManager->persist($new);
                        continue;
                    }

                    $dr->addDriverRouteCode($code);
                    $dr->setRouteCode($route->routeCode);
                    $this->entityManager->persist($dr);
                    ++$response['assigned'];
                }
            } else {
                /** @var Driver $driver */
                $drivers = $this->entityManager
                    ->createQueryBuilder()
                    ->select('d')
                    ->from(Driver::class, 'd')
                    ->leftJoin('d.user', 'u')
                    ->leftJoin('u.userRoles', 'ur')
                    ->andWhere(
                        $qb->expr()->notIn(
                            'ur.name',
                            ['ROLE_STATION_MANAGER', 'ROLE_ASSISTANT_MANAGER', 'ROLE_OWNER']
                        )
                    )
                    ->andWhere(
                        $qb->expr()->in(
                            'd.transporterID',
                            $route->transporterIdList
                        )
                    )
                    ->addCriteria(\App\Criteria\Common::notArchived('d'))
                    ->getQuery()
                    ->getResult()
                ;

                $driver = array_filter($drivers, function ($d) use ($transporterId) {
                    return $d->getTransporterID() === $transporterId;
                });

                if (empty($driver)) {
                    ++$response['open'];
                    $driver = null;
                } else {
                    ++$response['new'];
                    $driver = array_shift($driver);
                }

                $new = $this->newDriverRoute($shift, $driver, $code);

                $this->entityManager->persist($new);
            }
        }

        $this->entityManager->flush();

        $this->eventEmitter->create()
            ->setTopic([
                'company' => $company->getId(),
                'station' => $station->getId(),
                'topic'   => 'loadout'
            ])
            ->setEvent('REFRESH')
            ->setEmitter($owner->getId())
            ->setData(true)
            ->dispatch();

        return $this->json($response);
    }

    private function newDriverRoute(Shift $shift, ?Driver $driver, DriverRouteCode $code, ?\DateTimeInterface $date = null)
    {
        $date = $date ?? GlobalUtility::newUTCDateTime();

        return $this->entityManager
            ->getRepository(DriverRoute::class)
            ->addDriverRouteForCortex(
                $shift,
                $date,
                $driver,
                $code
            );
    }
}