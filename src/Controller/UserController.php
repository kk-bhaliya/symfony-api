<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Entity\DriverSkill;
use App\Entity\EmergencyContact;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use http\Env\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * User Controller
 * @Route("/api",name="api_")
 */
class UserController extends BaseController
{
    /**
     * @Rest\Post("/user/create")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     */
    public function createUserAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/user/update")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     */
    public function updateUserAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/user/get")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getUserAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/user/check/email/exist")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws Exception
     */
    public function checkEmailExitsAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $entityManager = $this->getDoctrine()->getManager();
        $userId = isset($data['userId']) ? $data['userId'] : 'null';
        $user = $entityManager->getRepository('App:User')->getEmailExist($data['email'], $userId);

        if ($user) {
            $status['status'] = true;
            $status['success'] = true;
        } else {
            $status['status'] = false;
            $status['success'] = false;
            $status['message'] = 'Email does not exist in our records';
        }
        return $this->json($status);
    }

    /**
     * @Rest\Post("/user/check_password")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function userCheckPassword(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $this->findUser($data['value'], $data['key']);
            if (!($user instanceof User)) {
                throw new Exception('User not found');
            }

            $encoded = $encoder->encodePassword($user, $data['password']);
            if ($user->getPassword() !== $encoded) {
                throw new Exception('Password does not match');
            }

            return $this->jsonResponse(true, [], 200, 'user', 'Password match');
        } catch (Exception $e) {
            return $this->jsonException($e);
        } catch (Error $e) {
            return $this->jsonException($e);
        }
    }

    /**
     * @Rest\Post("/user/update_password")
     * @param Request $request
     *
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @throws Exception
     */
    public function updatePasswordUser(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $this->findUser($data['value'], $data['key']);
            if (!($user instanceof User)) {
                throw new Exception('User not found');
            }
            $encoded = $encoder->encodePassword($user, $data['password']);
            $user->setPassword($encoded);
            $user->setIsResetPassword(false);
            $this->entityManager->flush();
            return $this->jsonResponse(true, [], 200, 'user', 'User password updated');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @param $value
     * @param $key
     * @return User|object|null
     * @throws Exception
     */
    public function findUser($value, $key)
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([$key => $value]);
        if ($user instanceof User) {
            return $user;
        }

        throw new Exception('Error in finding user');
    }

    /**
     * @Rest\Post("/user/fetch/")
     * @param Request $request
     * @param ObjectNormalizer $objectNormalizer
     * @return JsonResponse
     */
    public function fetchUserAction(Request $request, ObjectNormalizer $objectNormalizer): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $this->findUser($data['value'], $data['key']);
            $normalizeEntity = $this->serialize($data, $user);
            if ($normalizeEntity !== null) {
                $user = $normalizeEntity;
            }
            return $this->jsonResponse(true, $user, 200, 'user', 'User information fetched');
        } catch (Exception $e) {
            return $this->jsonException($e);
        } catch (ExceptionInterface $e) {
            return $this->jsonException($e);
        }
    }

    /**
     * @Rest\Post("/fetch/user")
     * @param Request $request
     * @param ObjectNormalizer $objectNormalizer
     * @return JsonResponse
     */
    public function getUserEntity(Request $request, ObjectNormalizer $objectNormalizer): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $this->findUser($data['value'], $data['key']);
            $context[AbstractNormalizer::ATTRIBUTES] = empty($data['includes']) ? [] : $data['includes'];
            $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = empty($data['excludes']) ? [] : $data['excludes'];
            $user = $objectNormalizer->normalize($user, null, $context);
            return $this->jsonResponse(true, $user, 200, 'user', 'User information fetched');
        } catch (Exception $e) {
            return $this->jsonException($e);
        } catch (ExceptionInterface $e) {
            return $this->jsonException($e);
        }
    }

    public function plainPassword()
    {
        $numbers = array_rand(range(0, 9), rand(2, 2));
        $uppercase = array_rand(array_flip(range('A', 'Z')), rand(2, 2));
        $lowercase = array_rand(array_flip(range('a', 'z')), rand(2, 2));
        $special = array_rand(array_flip(['@', '#', '$', '!', '%', '*', '?', '&']), rand(2, 2));
        $password = array_merge(
            $numbers,
            $uppercase,
            $lowercase,
            $special
        );
        shuffle($password);
        return implode($password);
    }

}
