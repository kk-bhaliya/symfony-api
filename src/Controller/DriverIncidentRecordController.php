<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Driver Incident Record Controller
 * @Route("/api",name="api_")
 */
class DriverIncidentRecordController extends BaseController
{
    /**
     * @Rest\Post("/driver_incident_record/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createDriverIncidentRecordAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/driver_incident_record/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateDriverIncidentRecordAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/driver_incident_record/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getDriverIncidentRecordAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }
}
