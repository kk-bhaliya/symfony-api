<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\Event;
use App\Entity\Station;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Device Controller
 * @Route("/api",name="api_")
 */
class DeviceController extends BaseController
{
    /**
     * @Rest\Post("/device/create")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createDeviceAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/device/update")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function updateDeviceAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/device/get")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getDeviceAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/devices/csv/upload")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function uploadCsv(Request $request, EntityManagerInterface $em)
    {
        $companyId = $request->request->get('company');
        $company = $em->getRepository('App\Entity\Company')->find((int)$companyId);
        $userId = $request->request->get('user');
        $parentUser = $em->getRepository('App\Entity\User')->find($userId);
        $fileName = $request->request->get('filePath');

        if ($fileName != '') {
            $file = fopen($fileName, "r");
            $firstRow = true;
            $log = '';
            $cnt = $editCount = $addCount = 0;
            while (($column = fgetcsv($file, 10000, ",")) !== false) {
                $validate = true;
                if ($cnt == 0) {
                    if (trim($column[0]) != 'Station*') {
                        $log = 'csv column Station not there';
                        break;
                    } elseif (trim($column[1]) != 'Device Name (Name)*') {
                        $log = 'csv column Device Name (Name) not there';
                        break;
                    } elseif (trim($column[2]) != 'Carrier*') {
                        $log = 'csv column Carrier not there';
                        break;
                    } elseif (trim($column[3]) != 'Username*') {
                        $log = 'csv column Username not there';
                        break;
                    } elseif (trim($column[4]) != 'Platform*') {
                        $log = 'csv column Platform not there';
                        break;
                    } elseif (trim($column[5]) != 'Asset Tag ID*') {
                        $log = 'csv column Asset Tag ID not there';
                        break;
                    } elseif (trim($column[6]) != 'Manufacturer*') {
                        $log = 'csv column Manufacturer not there';
                        break;
                    } elseif (trim($column[7]) != 'Model*') {
                        $log = 'csv column Model not there';
                        break;
                    } elseif (trim($column[8]) != 'OS Version*') {
                        $log = 'csv column OS Version not there';
                        break;
                    } elseif (trim($column[9]) != 'MDM Client*') {
                        $log = 'csv column MDM Client not there';
                        break;
                    } elseif (trim($column[10]) != 'UID*') {
                        $log = 'csv column UID not there';
                        break;
                    } elseif (trim($column[11]) != 'Phone Number') {
                        $log = 'csv column Phone Number not there';
                        break;
                    } elseif (trim($column[12]) != 'IMEI/MEID (Internal Device ID)*') {
                        $log = 'csv column IMEI/MEID (Internal Device ID) not there';
                        break;
                    } elseif (trim($column[13]) != 'Condition*') {
                        $log = 'csv column Condition not there';
                        break;
                    } elseif (trim($column[14]) != 'Run Loadout') {
                        $log = 'csv column Run Loadout not there';
                        break;
                    }
                }
                if ($cnt > 0) {
                    $userStatus = false;
                    $tempLog = '';
                    $device = null;

                    // check validation

                    $station = null;
                    if (trim($column[0]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Station';
                        } else {
                            $tempLog = $tempLog . 'please fill the Station';
                        }
                        $validate = false;
                    } else {
                        $station = $em->getRepository('App\Entity\Station')->findOneBy(['company' => $companyId, 'code' => trim($column[0]), 'isArchive' => false]);
                        if (!$station) {
                            if ($tempLog != '') {
                                $tempLog = $tempLog . ', please enter correct the Station';
                            } else {
                                $tempLog = $tempLog . 'please enter correct the Station';
                            }
                            $validate = false;
                        }
                    }
                    if (trim($column[1]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Device Name';
                        } else {
                            $tempLog = $tempLog . 'please fill the Device Name';
                        }
                        $validate = false;
                    }
                    if (trim($column[4]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Platform';
                        } else {
                            $tempLog = $tempLog . 'please fill the PlatForm';
                        }
                        $validate = false;
                    } elseif (!in_array(trim($column[4]), array('Android', 'ios', 'Other'))) {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Platform with (Android, ios, Other)';
                        } else {
                            $tempLog = $tempLog . 'please fill the Platform with (Android, ios, Other)';
                        }
                        $validate = false;
                    }
                    if (trim($column[5]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Asset Tag ID';
                        } else {
                            $tempLog = $tempLog . 'please fill the Asset Tag ID';
                        }
                        $validate = false;
                    }
                    if (trim($column[3]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Username';
                        } else {
                            $tempLog = $tempLog . 'please fill the Username';
                        }
                        $validate = false;
                    }
                    if (trim($column[8]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the OS Version';
                        } else {
                            $tempLog = $tempLog . 'please fill the OS Version';
                        }
                        $validate = false;
                    }
                    if (trim($column[9]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the MDM Client';
                        } else {
                            $tempLog = $tempLog . 'please fill the MDM Client';
                        }
                        $validate = false;
                    }
                    if (trim($column[6]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Manufacturer';
                        } else {
                            $tempLog = $tempLog . 'please fill the Manufacturer';
                        }
                        $validate = false;
                    }
                    if (trim($column[7]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Model';
                        } else {
                            $tempLog = $tempLog . 'please fill the Model';
                        }
                        $validate = false;
                    }
                    if (trim($column[10]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the UID';
                        } else {
                            $tempLog = $tempLog . 'please fill the UID';
                        }
                        $validate = false;
                    }
                    if (trim($column[11]) != '' && !preg_match('/^[02-9][0-9]{9}$/', $column[11])) {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', Phone number as wrong format';
                        } else {
                            $tempLog = $tempLog . 'Phone number as wrong format';
                        }
                        $validate = false;
                    }
                    if (trim($column[12]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the IMEI/MEID';
                        } else {
                            $tempLog = $tempLog . 'please fill the IMEI/MEID';
                        }
                        $validate = false;
                    }
                    if (trim($column[13]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Condition';
                        } else {
                            $tempLog = $tempLog . 'please fill the Condition';
                        }
                        $validate = false;
                    } elseif (!in_array(trim($column[13]), array('Good', 'Cracked Screen', 'Short Battery', 'Stolen'))) {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Condition with (Good, Cracked Screen, Short Battery, Stolen)';
                        } else {
                            $tempLog = $tempLog . 'please fill the Condition with (Good, Cracked Screen, Short Battery, Stolen)';
                        }
                        $validate = false;
                    }
                    if (trim($column[2]) == '') {
                        if ($tempLog != '') {
                            $tempLog = $tempLog . ', please fill the Carrier';
                        } else {
                            $tempLog = $tempLog . 'please fill the Carrier';
                        }
                        $validate = false;
                    }

                    if (trim($column[10]) != '' && trim($column[12]) != '') {
                        $device = $em->getRepository('App\Entity\Device')->findOneBy(
                            [
                                'imei' => trim($column[12]) != '' ? trim($column[12]) : null,
                                'deviceId' => trim($column[10]) != '' ? trim($column[10]) : null,
                                'isArchive' => false
                            ]
                        );
                        if ($device) {
                            if ($device->getCompany()->getId() != $companyId) {
                                if ($tempLog != '') {
                                    $tempLog = $tempLog . ', UID and IMEI/MEID already exist in other company';
                                } else {
                                    $tempLog = $tempLog . 'UID and IMEI/MEID already exist in other company';
                                }
                                $validate = false;
                            }
                        }
                    }

                    if ($tempLog != '') {
                        $log = $log . '(' . $cnt . ')' . implode(" | ", $column) . '
                        Error - ' . $tempLog . '

                        ';
                    }

                    if ($validate == true) {
                        if (empty($device)) {
                            $device = new Device();
                            $device->setCompany($company);
                            $device->setStation($station);
                            $device->setName(trim($column[1]));
                            $device->setCarrier(trim($column[2]));
                            $device->setUsername(trim($column[3]));
                            $device->setPlatform(trim($column[4]));
                            $device->setAssetTag(trim($column[5]));
                            $device->setManufacturer(ucfirst(trim($column[6])));
                            $device->setModel(trim($column[7]));
                            $device->setOsVersion(trim($column[8]));
                            $device->setMdmClient(strtolower(trim($column[9])) == 'yes' ? true : false);
                            $device->setDeviceId(trim($column[10]));
                            $device->setDeviceUid(trim($column[10]) != '' ? trim($column[10]) : 0);
                            $device->setNumber($column[11] ? trim($column[11]) : '');
                            $device->setImei(trim($column[12]) != '' ? trim($column[12]) : null);
                            $device->setDeviceCondition(trim($column[13]));
                            $device->setIsArchive(false);
                            $device->setAllowKickoff(trim($column[14]));
                            $device->setIsPersonalDevice(false);
                            $device->setAllowLoadOutProcess(trim($column[14]));
                            $em->persist($device);
                            $em->flush();
                        } else {
                            $device->setCompany($company);
                            $device->setStation($station);
                            $device->setName(trim($column[1]));
                            $device->setCarrier(trim($column[2]));
                            $device->setUsername(trim($column[3]));
                            $device->setPlatform(trim($column[4]));
                            $device->setAssetTag(trim($column[5]));
                            $device->setManufacturer(ucfirst(trim($column[6])));
                            $device->setModel(trim($column[7]));
                            $device->setOsVersion(trim($column[8]));
                            $device->setMdmClient(strtolower(trim($column[9])) == 'yes' ? true : false);
                            $device->setDeviceId(trim($column[10]));
                            $device->setDeviceUid(trim($column[10]) != '' ? trim($column[10]) : 0);
                            $device->setNumber($column[11] ? trim($column[11]) : '');
                            $device->setImei(trim($column[12]) != '' ? trim($column[12]) : null);
                            $device->setDeviceCondition(trim($column[13]));
                            $device->setIsArchive(false);
                            $device->setAllowKickoff(trim($column[14]));
                            $device->setIsPersonalDevice(false);
                            $device->setAllowLoadOutProcess(trim($column[14]));
                            $em->persist($device);
                            $em->flush();
                        }
                    }
                }
                $cnt++;
            }
        }
        return $this->jsonResponse(true, '', 200, 'user', $log);
    }

    /**
     * @Route(
     *     name="checkPhoneNumber",
     *     path="/check_phone_number",
     *     methods={"POST"},
     * )
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function checkPhoneNumber(Request $request): JsonResponse
    {
        $device = $this->entityManager->getRepository(Device::class)
            ->findOneBy(['number' => $request->get('number'), 'isArchive' => false]);
        $success = true;
        $status = 200;
        $result = $device ? $device->getId() : null;
        return $this->jsonResponse($success, $result, $status, 'device');
    }

    /**
     * @Route(
     *     name="addPersonalDevice",
     *     path="/add_personal_device",
     *     methods={"POST"},
     * )
     * @param Request $request
     *
     * @param EntityManagerInterface $em
     * @return JsonResponse
     */
    public function addPersonalDevice(Request $request, EntityManagerInterface $em)
    {
        $deviceInfo = $request->request->all();
        $device = new Device();

        foreach ($deviceInfo as $field => $value) {
            $setter = 'set' . $field;
            $device->$setter($value);
        }
        $device->setIsEnabled(true);
        $device->setIsEnabled(false);
        $device->setDeviceCondition('Good');
        $device->setMdmClient(false);
        $device->setAllowKickoff(false);
        $device->setIsPersonalDevice(true);
        $device->setIsOwned(false);

        $em->persist($device);
        $em->flush();

        return $this->jsonResponse(200, $device->getId());
    }

    /**
     * @Route(
     *     name="updateDevicesByCompany",
     *     path="/update_devices_by_company",
     *     methods={"POST"},
     * )
     * @param Request $request
     * @return JsonResponse
     */
    public function updateDevicesByCompany(Request $request)
    {
        $data = $request->request->all();
        $companyId = $data['company']['id'];
        /** @var Station $station */
        $station = $this->entityManager->getRepository(Station::class)->find($data['station']['id']);
        $devices = $this->entityManager->getRepository(Device::class)->findBy(['company' => $companyId, 'isArchive' => false]);
        foreach ($devices as $device) {
            if ($device instanceof Device) {
                $device->setName($station->getCode() . '-#' . $device->getId() . '-' . $device->getModel());
            }

        }
        $this->entityManager->flush();
        return $this->jsonResponse(true, $devices);
    }
}
