<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\Station;
use App\Entity\User;
use App\Entity\Vehicle;
use Doctrine\Common\Persistence\Mapping\AbstractClassMetadataFactory;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Station Controller
 * @Route("/api",name="api_")
 */
class StationController extends BaseController
{
    /**
     * @Rest\Post("/station/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createStationAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/station/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateStationAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/station/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getStationAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/fetchDriversByStation")
     * @param Request $request
     *
     * @return Response
     * @throws Exception
     */
    public function fetchDriversByStation(Request $request): Response
    {
        try {
            $data = $request->request->all();
            $companyId = $data['company'];
            $company = $this->entityManager->find(Company::class, $companyId);
            $stations = $company->getStations()->filter(function (Station $item) {
                return $item->getIsArchive() === false;
            });
            $result = [];
            $defaultContext = [
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object, $format, $context) {
                    if (method_exists($object, 'getId')) {
                        return $object->getId();
                    }
                    return null;
                },
                AbstractNormalizer::CALLBACKS => [
                    'user' => function ($innerObject, $outerObject, string $attributeName, string $format = null, array $context = []) {
                        if ($innerObject instanceof User && !$innerObject->getIsArchive()) {
                            return [
                                'id' => $innerObject->getId(),
                                'name' => $innerObject->getFriendlyName(),
                                'email' => $innerObject->getEmail(),
                                'isArchive' => $innerObject->getIsArchive()
                            ];
                        }
                        return null;
                    },
                ],
                AbstractNormalizer::ATTRIBUTES =>
                    [
                        'user',
                    ],
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => false,
                AbstractObjectNormalizer::SKIP_NULL_VALUES => true,
            ];

            $normalizer = new ObjectNormalizer(null, null, null, null, null, null, $defaultContext);
            $encoder = new JsonEncoder();
            $serializer = new Serializer([$normalizer], [$encoder]);
            foreach ($stations as $station) {
                $result[] =
                    [
                        'name' => $station->getName(),
                        'code' => $station->getCode(),
                        'drivers' => $station->getDrivers()->filter(function (Driver $item) {
                            return $item->getIsArchive() === false;
                        })
                    ];
            }

            return new Response(
                $serializer->serialize($result, 'json'),
                Response::HTTP_OK,
                ['Content-type' => 'application/json']
            );
        } catch (Exception $exception) {
            return new Response(
                ['message' => $exception->getMessage()],
                Response::HTTP_BAD_REQUEST,
                ['Content-type' => 'application/json']
            );
        }

    }
}
