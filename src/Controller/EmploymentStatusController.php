<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Employment Status Controller
 * @Route("/api",name="api_")
 */
class EmploymentStatusController extends BaseController
{
    /**
     * @Rest\Post("/employment_status/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createEmploymentStatusAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/employment_status/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateEmploymentStatusAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/employment_status/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getEmploymentStatusAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }
}

