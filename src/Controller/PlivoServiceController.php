<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\User;
use App\Services\PlivoService;
use App\Services\Result;
use App\Services\SingleResult;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Throwable;


/**
 * @Route("/api/plivo", name="api_")
 */
class PlivoServiceController extends BaseController
{
    /**
     * @Rest\Post("/set/sub_account/in/company")
     * @param Request $request
     * @return JsonResponse
     */
    public function setSubAccountInCompany(Request $request)
    {
        try {
            $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $this->plivoService->checkAuthenticationFields($data);
            $this->plivoService->checkPhoneNumber($data['sourceNumber']);


            $company->setPlivoAuthId($data['authId']);
            $company->setPlivoAuthToken($data['authToken']);
            $company->setPlivoMainSrcNumber($data['sourceNumber']);
            $this->entityManager->flush();
            $result = new Result(true, [], 'plivo', 'Setting up of authentication plivo fields');
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create/sub/account/in/company")
     * @param Request $request
     * @return JsonResponse
     */
    public function createSubAccountInCompany(Request $request)
    {
        try {
            $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $alias = $company->getName() . ' ID: ' . $company->getId();
            $subAccountResult = $this->plivoService->createSubAccount($alias);
            $phoneNumbersResult = null;
            $buyNumberResult = null;
            $updateNumberResult = null;
            $authId = null;
            $authToken = null;
            $phoneNumber = null;
            if ($subAccountResult->isSuccess()) {
                $authId = $subAccountResult->getData()['authId'];
                $authToken = $subAccountResult->getData()['authToken'];
                $criteria['areaCode'] = empty($company->getAreaCode()) ? '651' : $company->getAreaCode();
                $phoneNumbersResult = $this->plivoService->findNumbers($criteria);
            } else {
                $result = new Result(false, $subAccountResult->getData(), 'subAccountResult', 'Sub Account failed');
                return $result->jsonResponse();
            }
            if ($phoneNumbersResult->isSuccess()) {
                $this->plivoService->checkDataByKey($phoneNumbersResult->getData(), 'firstPhoneNumber');
                $phoneNumber = $phoneNumbersResult->getData()['firstPhoneNumber'];
                $buyNumberResult = $this->plivoService->buyNumber($phoneNumber);
            } else {
                $result = new Result(false,
                    [
                        $subAccountResult->getData(),
                        $phoneNumbersResult->getData()
                    ], 'subAccountResult', 'Getting phone number failed');
                return $result->jsonResponse();
            }
            if ($buyNumberResult->isSuccess() && $phoneNumber !== null && $authId !== null && $authToken !== null) {
                $updateNumberResult = $this->plivoService->updateBuyNumber($phoneNumber, $alias, $authId);
            } else {
                $result = new Result(false, [
                    $subAccountResult->getData(),
                    $phoneNumbersResult->getData(),
                    $buyNumberResult->getData()
                ], 'subAccountResult', 'Buy phone number failed');
                return $result->jsonResponse();
            }
            if ($updateNumberResult->isSuccess()) {
                $company->setPlivoMainSrcNumber($phoneNumber);
                $company->setPlivoAuthId($authId);
                $company->setPlivoAuthToken($authToken);
                $this->entityManager->flush();
            } else {
                $result = new Result(false,
                    [
                        $subAccountResult->getData(),
                        $phoneNumbersResult->getData(),
                        $buyNumberResult->getData(),
                        $updateNumberResult->getData()
                    ], 'subAccountResult', 'Update phone number failed');
                return $result->jsonResponse();
            }

            $result = new Result(true,
                [
                    $subAccountResult->getData(),
                    $phoneNumbersResult->getData(),
                    $buyNumberResult->getData(),
                    $updateNumberResult->getData()
                ], 'subAccountResult', 'Sub Account created');
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create/sub/account")
     * @param Request $request
     * @return JsonResponse
     */
    public function createSubAccount(Request $request)
    {
        try {
            $data = $request->request->all();
            $subAccountResult = $this->plivoService->createSubAccount($data['name']);
            return $subAccountResult->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/search/phone/numbers")
     * @param Request $request
     * @return JsonResponse
     */
    public function searchPhoneNumbers(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkAreaCode($data);
            $result = $this->plivoService->findNumbers($data);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/buy/phone/number")
     * @param Request $request
     * @return JsonResponse
     */
    public function buyPhoneNumber(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkAreaCode($data);
            $result = $this->plivoService->findNumbers($data);
            $this->plivoService->checkDataByKey($result->getData(), 'firstPhoneNumber');
            $phoneNumber = $result->getData()['firstPhoneNumber'];
            $result = $this->plivoService->buyNumber($phoneNumber);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/update/phone/number/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePhoneNumberResource(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkDataByKey($data, 'phoneNumber');
            $this->plivoService->checkDataByKey($data, 'alias');
            $this->plivoService->checkDataByKey($data, 'subAccountAuthId');
            $result = $this->plivoService->updateBuyNumber($data['phoneNumber'], $data['alias'], $data['subAccountAuthId']);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/add/phone/number/to/sub_account")
     * @param Request $request
     * @return JsonResponse
     */
    public function addPhoneNumberToSubAccount(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkDataByKey($data, 'companyId');
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company instanceof Company) {
                if (empty($company->getName())) {
                    throw new Exception('Company name cannot be null');
                }
                $alias = $company->getName();
                $subAccountAuthId = $company->getPlivoAuthId();
                $phoneNumber = $company->getPlivoMainSrcNumber();
                $this->plivoService->checkDataByKey($data, 'subAccountAuthId');
                $result = $this->plivoService->updateBuyNumber($phoneNumber, $alias, $subAccountAuthId);
                return $result->jsonResponse();
            }
            throw new Exception('Company not found');
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/retrieve/sub/account")
     * @param Request $request
     * @return JsonResponse
     */
    public function retrieveSubAccount(Request $request)
    {
        $data = $request->request->all();
        $result = $this->plivoService->retrieveSubAccount($data['authId']);
        return $result->jsonResponse();
    }

    /**
     * @Rest\Post("/send/sms")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendSMS(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkPhoneNumber($data['destinationPhoneNumber']);
            $this->plivoService->checkText($data['text']);
            $company = $this->botService->getCompanyEntity($data);
            $authId = $company->getPlivoAuthId();
            $authToken = $company->getPlivoAuthToken();
            $phoneNumber = $company->getPlivoMainSrcNumber();
            $plivo = new PlivoService($authId, $authToken);
            $result = $plivo->sendMessage($data['destinationPhoneNumber'], $data['text'], $phoneNumber);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/test/send/sms")
     * @param Request $request
     * @return JsonResponse
     */
    public function testSendSMS(Request $request)
    {
        try {
            $data = $request->request->all();
            $this->plivoService->checkPhoneNumber($data['phoneNumber']);
            $this->plivoService->checkPhoneNumber($data['destinationPhoneNumber']);
            $this->plivoService->checkText($data['text']);
            $this->plivoService->checkAuthenticationFields($data);
            $authId = $data['authId'];
            $authToken = $data['authToken'];
            $phoneNumber = $data['phoneNumber'];
            $plivo = new PlivoService($authId, $authToken);
            $result = $plivo->sendMessage($data['destinationPhoneNumber'], $data['text'], $phoneNumber);
            return $this->json($result->getFormatData(), Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }

    }

}