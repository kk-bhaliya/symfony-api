<?php

namespace App\Controller;

use App\Entity\Media;
use Exception;
use phpDocumentor\Reflection\Types\Object_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * ImageNormalizer Controller
 * @Route("/api",name="api_")
 */
class ImageController extends BaseController
{
    /**
     * @Rest\Post("/image/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createImageAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/image/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function updateImageAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/image/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getImageAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/upload")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function upload(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $files = $request->files->all();
            $result = [];

            foreach ($files as $key => $value) {
                $file = $value;
                $result['isFile'] = is_file($file);
                $ext = pathinfo($data['name'], PATHINFO_EXTENSION);
                $ext = !empty(strtolower($ext)) ? strtolower($ext) : 'jpg';
                if($ext === 'HEIC'){
                    $ext = 'png';
                }
                $tmpFileName = 'media.' . $ext;
                $result['is_readable'] = is_readable($file);
                $result['is_uploaded_file'] = is_uploaded_file($file);
                $source = fopen($file, 'r');
                $destination = fopen($tmpFileName, 'w');
                stream_copy_to_stream($source, $destination);
                fclose($source);
                fclose($destination);
                $filename = $this->getUniqueIdentifier() . '_' . $data['name'];
                $s3result = $this->s3->uploadFile('./' . $tmpFileName, $filename, 'dsp.data.storage.general');
                if (is_string($s3result)) {
                    throw new Exception($s3result);
                }
                $result['ObjectURL'] = $s3result['ObjectURL'];
                unlink($tmpFileName);
            }
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }

        return $this->json($result);
    }

    /**
     * @Rest\Post("/uploadFile")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadFile(Request $request): JsonResponse
    {

        // TODO: Improve performance
        try {
            $files = $request->files->get('file');
            $data = $request->request->all();
            //return $this->json($files);
            $result = [];

            foreach ($files as $key => $file) {

                $fileName = $file->getClientOriginalName();
                $ext = pathinfo($fileName, PATHINFO_EXTENSION);
                $filename = $this->getUniqueIdentifier() . '_' . $fileName;


                $entityManager = $this->getDoctrine()->getManager();

                if(isset($data['id']) && isset($data['id'][$key])) {
                    $Image = $entityManager->getRepository(Media::class)->find($data['id'][$key]);
                }else{
                    $Image = new Media();
                }

                $exist_file = $Image->getPath();
                if(!empty($exist_file)){
                    $exist_file = explode('/',$exist_file);
                    $filename = $exist_file[count($exist_file)-1];
                }

                $s3result = $this->s3->uploadFile($file->getRealPath(), $filename, 'dsp.data.storage.general');
                if (is_string($s3result)) {
                    throw new Exception($s3result);
                }


                $Image->setPath($s3result['ObjectURL']);
                $Image->setType($data['type']);
                $Image->setEntityId($data['entityId']);
                $entityManager->persist($Image);
                $entityManager->flush();

                $result[] = $Image;

            }
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }

        return $this->json($result);
    }

    /**
     * @Rest\Post("/deleteFile")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteFile(Request $request): JsonResponse
    {
        $data = $request->request->all();

        $exist_file = explode('/',$data['file']);
        $filename = $exist_file[count($exist_file)-1];

        $s3result = $this->s3->deleteFile($filename, 'dsp.data.storage.general');
        if(!empty($s3result)){
            $entityManager = $this->getDoctrine()->getManager();
            $Image = $entityManager->getRepository(Media::class)->find($data['id']);
            $entityManager->remove($Image);
            $entityManager->flush();

            return $this->json(['message'=>'image deleted']);
        }

    }
}
