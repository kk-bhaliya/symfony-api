<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Stripe\Exception\ApiErrorException;

/**
 * Status Controller
 * @Route("/api",name="api_")
 */

class StripeController extends BaseController
{
    /**
     * @Rest\Get("/stripe/get/packages")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getPackagesAction(Request $request)
    {
        $this->getStripeService()->setApiKey();
        $products = $this->getStripeService()->getProducts(['limit' => 10,'active' => true]);
        $products['data'] = array_reverse($products['data']);

        $productArray = [];
        foreach ($products['data'] as $product) {
            $price=0;
            $plans = $this->getStripeService()->getPlans(['limit' => 20, 'product' => $product['id'], 'active' => true]);
            $productArray[$product->metadata->package_type]['name'] = $product['name'];
            $productArray[$product->metadata->package_type]['id'] = $product['id'];
            $productArray[$product->metadata->package_type]['type'] = $product->metadata->package_type;
            $productArray[$product->metadata->package_type]['meta'] = $product->metadata;

            $packageArray = [];
            $addonArray = [];
            
            foreach ($plans['data'] as $plan) {
                $child = [
                    'id' => $plan['id'],
                    'name' => $plan['nickname'],
                    'amount' => $plan['amount_decimal']/100,
                    'type' => $plan['usage_type'],
                    'usage' => $plan['usage_type'],
                    'standard' => ($plan->metadata->is_standard)?$plan->metadata->is_standard:0,
                    'category' => ($plan->metadata->category == 'hosting')?$plan->metadata->category:0,
                    'meta' => $plan->metadata,
                    'unit' => $plan->billing_scheme,
                    'order' => $plan->metadata->order,
                ];

                if( $plan->metadata->is_addon == 0 ){
                    $packageArray[] = $child;
                    $price = $price + $plan['amount']/100;
                }
                if( $plan->metadata->is_addon == 1 ){
                    $addonArray[] = $child;
                }
            }

            $order = array_column($packageArray, 'order');
            array_multisort($order, SORT_ASC, $packageArray);

            $order = array_column($addonArray, 'order');
            array_multisort($order, SORT_ASC, $addonArray);

            $productArray[$product->metadata->package_type]['packages'] = $packageArray;
            $productArray[$product->metadata->package_type]['addon'] = $addonArray;
            $productArray[$product->metadata->package_type]['price'] = $price;
            $productArray[$product->metadata->package_type]['description'] = $product->metadata->description;
        }

        return $this->json($productArray, 200);
    }

    /**
     * @Rest\Get("/stripe/get/addon/packages")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getAddonPackageAction(Request $request)
    {
        $this->getStripeService()->setApiKey();
        //$data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $plans = $this->getStripeService()->getPlans(['limit' => 20, 'product' => $request->query->get('product_id'), 'active' => true]);
        $planArray = [];
        foreach ($plans['data'] as $plan) {
            if( $plan->metadata->is_addon == 1 ){
                $planArray[] = ['id' => $plan['id'], 'name' => $plan['nickname'], 'amount' => $plan['amount']/100, 'type' => $plan['usage_type']];
            }
        }
        return $this->json($planArray, 200);
    }

    /**
     * @Rest\Get("/stripe/create/customer")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createCustomerAction(Request $request)
    {
        $this->getStripeService()->setApiKey();
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $customer = $this->getStripeService()->createCustomer(
            [
                'email' => $data['email'],
                'source' => $data['payment_token'],
                'name' => $data['name']

            ]
        );
        $customerArray=[];
        if ($customer instanceof ApiErrorException) {
            $customerArray=['status'=>0,'data'=>[],'message'=>'failed'];
        } else {
            $customerArray=['status'=>0,'data'=>$customer,'message'=>'failed'];
        }
        return $this->json($customerArray, 200);
    }
}
