<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Event;
use App\Entity\Role;
use App\Entity\User;
use App\Services\AmazonS3Service;
use App\Services\BotService;
use App\Services\PlivoService;
use App\Services\Result;
use App\Services\TwilioService;
use App\Services\TwilioAccountService;
use App\Utils\EventEmitter;
use App\Worker\BotServiceWorker;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use JMS\Serializer\SerializerInterface;
use Lankerd\GroundworkBundle\Handler\DataHandler;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use App\Services\StripeService;
use App\Services\DummyDataService;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use App\Services\EmailService;

/**
 * Base controller
 *
 * @Route("/api/lazy",name="lazy_api")
 *
 * @author Julian Lankerd <julian@corephp.com>
 */
class BaseController extends AbstractFOSRestController
{
    /**
     * @var AdapterInterface
     */
    protected $cache;

    protected $serializer;

    protected $companyKey;

    protected $dataHandler;

    protected $twilioService;

    protected $twilioAccountService;

    /**
     * @var StripeService
     */
    protected $stripeService;

    /**
     * @var DummyDataService
     */
    private $dummyDataService;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * @var BotService
     */
    protected $botService;
    /**
     * @var AmazonS3Service
     */
    protected $s3;

    /**
     * @var EmailService
     */
    protected $emailService;
    /**
     * @var PlivoService
     */
    protected $plivoService;
    /**
     * @var Result
     */
    protected $result;

    /**
     * @var EventEmitter
     */
    protected $eventEmitter;

    /**
     * @var BotServiceWorker
     */
    protected $botServiceWorker;

    /**
     * BaseController constructor.
     * @param RequestStack $requestStack
     * @param AdapterInterface $cache
     * @param SerializerInterface $serializer
     * @param DataHandler $dataHandler
     * @param StripeService $stripeService
     * @param DummyDataService $dummyDataService
     * @param TwilioService $twilioService
     * @param TwilioAccountService $twilioAccountService
     * @param EntityManagerInterface $entityManager
     * @param BotService $botService
     * @param AmazonS3Service $s3
     * @param EmailService $emailService
     * @param PlivoService $plivoService
     * @param Result $result ,
     * @param EventEmitter $eventEmitter
     * @param BotServiceWorker $botServiceWorker
     */
    public function __construct(
        RequestStack $requestStack,
        AdapterInterface $cache,
        SerializerInterface $serializer,
        DataHandler $dataHandler,
        StripeService $stripeService,
        DummyDataService $dummyDataService,
        TwilioService $twilioService,
        TwilioAccountService $twilioAccountService,
        EntityManagerInterface $entityManager,
        BotService $botService,
        AmazonS3Service $s3,
        EmailService $emailService,
        PlivoService $plivoService,
        Result $result,
        EventEmitter $eventEmitter,
        BotServiceWorker $botServiceWorker
    )
    {
        /**
         * Grab the secret key, and make sure it is correct.
         **/

        /**
         * Grab the company ApiKey. This will
         * allow us to do searches based company information
         **/
        $this->companyKey = $requestStack->getCurrentRequest()->headers->get('companyKey');

        /**
         * Set a Serializer property so that we can correctly serialize data for insertion to Redis.
         **/
        $this->serializer = $serializer;

        /**
         * Set a Cache property so that we can begin to interact with Redis.
         **/
        $this->cache = $cache;

        /**
         * Initialize GroundworkBundle object handler into the controller.
         */
        $this->dataHandler = $dataHandler;
        /**
         * Initialize Stripe service.
         */
        $this->stripeService = $stripeService;

        /**
         * Initialize dummy data service.
         */
        $this->dummyDataService = $dummyDataService;

        $this->twilioService = $twilioService;

        $this->twilioAccountService = $twilioAccountService;

        $this->entityManager = $entityManager;
        $this->botService = $botService;

        $this->s3 = $s3;

        $this->emailService = $emailService;
        $this->plivoService = $plivoService;
        $this->result = $result;

        $this->eventEmitter = $eventEmitter;
        $this->botServiceWorker = $botServiceWorker;
    }

    /**
     * @Rest\Post("/manage/data")
     * @param Request $request
     * @return JsonResponse
     */
    public function manageData(Request $request): JsonResponse
    {
        try {
            $result = $this->dataHandler->requestHandler($request);
            $botResult = $this->botService->getData($result, $request->request->all());
            $result['botService'] = $botResult;
            return $this->json($result);
        } catch (Exception $exception) {
            $result['exception'] = [
                'message' => $exception->getMessage(),
                'trace' => $exception->getTrace(),
                'line' => $exception->getLine(),
                'filename' => $exception->getFile(),
            ];
            return $this->json($result, 400);
        } catch (Error $error) {
            $result['error'] = [
                'message' => $error->getMessage(),
                'trace' => $error->getTrace(),
                'line' => $error->getLine(),
                'filename' => $error->getFile(),
            ];
            return $this->json($result, 400);
        }
    }

    /**
     * @Rest\Post("/get_timeline_events")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getTimelineEventsByVehicle(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $result = $this->entityManager->getRepository(Event::class)->getTimelineEvents($data);
        return $this->json(array_slice($result , 0, 31));
    }

    /**
     * @param array $data
     * @param $entity
     * @return array|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     */
    public function serialize(array $data, $entity)
    {
        if (array_key_exists('includes', $data) || array_key_exists('excludes', $data)) {
            $context = [];
            if (!empty($data['includes'])) {
                $context[AbstractNormalizer::ATTRIBUTES] = $data['includes'];
            }
            if (!empty($data['excludes'])) {
                $context[AbstractNormalizer::IGNORED_ATTRIBUTES] = $data['excludes'];
            }
            $serializer = new Serializer([new ObjectNormalizer()]);
            return $serializer->normalize($entity, null, $context);
        }
        return null;

    }

    /**
     * @return string
     * @throws Exception
     */
    public function getUniqueIdentifier()
    {
        return bin2hex(random_bytes(16));
    }

    /**
     * @param User $user
     * @param Company $company
     * @param EntityManagerInterface $entityManager
     * @return Result
     */
    public function createPreDesignedRoles(User $user, Company $company, EntityManagerInterface $entityManager)
    {
        $roles = ['ROLE_DISPATCHER', 'ROLE_STATION_MANAGER', 'ROLE_ASSISTANT_MANAGER', 'ROLE_LEAD_DRIVER', 'ROLE_CUSTOMER_ADMIN', 'ROLE_DELIVERY_ASSOCIATE', 'ROLE_OWNER'];
        $results = [];
        try {
            foreach ($roles as $name) {
                $role = new Role();
                $role->setName($name);
                $role->setCompany($company);
                $role->addUser($user);
                $entityManager->persist($role);
                $results[$role->getId()] = $this->toArray($role, ['users', 'company', 'id']);
            }
            $entityManager->flush();
            return new Result(true, $this->toArray($results), 'preDesignedRoles', 'Roles created', true);
        } catch (Exception $exception) {
            return new Result(true, $exception->getTrace(), 'preDesignedRoles', $exception->getMessage(), true);
        }

    }

    /**
     * @param Company $company
     * @return mixed
     * @throws Exception
     */
    public function dataForTwilioAuthentication(Company $company)
    {
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        if (empty($data['accountSid'])) {
            throw new Exception('accountSid field is missing');
        }
        if (empty($data['authToken'])) {
            throw new Exception('authToken field is missing');
        }
        if (empty($data['serviceSid'])) {
            throw new Exception('serviceSid field is missing');
        }
        return $data;
    }

    /**
     * @param bool $success
     * @param $result
     * @param int $status
     * @param string $resultName
     * @param string $message
     * @return JsonResponse
     */
    public function jsonResponse(bool $success, $result, $status = 200, $resultName = 'result', $message = 'OK')
    {
        return $this->json(['success' => $success, $resultName => $result, 'message' => $message], $status);
    }

    /**
     * @param string $role
     * @return string
     */
    public function getPosition(string $role)
    {
        $roleName = str_replace("ROLE_", "", $role);
        $roleName = str_replace("_", " ", $roleName);
        $roleName = ucwords(strtolower($roleName));
        $roleName = str_replace('Hr ', 'HR ', $roleName);
        return $roleName;
    }

    /**
     * @param bool $success
     * @param $result
     * @param string $message
     * @param int $status
     * @return JsonResponse
     */
    public function jsonResponseV2(bool $success, $result, $message = 'OK', $status = 200)
    {
        $data = $result;
        if (isset($result['success'])) {
            $data['success'] = $result['success'];
        } else {
            $data['success'] = $success;
        }
        if (isset($result['message'])) {
            $data['message'] = $result['message'];
        } else {
            $data['message'] = $message;
        }
        return $this->json($data, $status);
    }

    /**
     * @param Exception|Error $exception
     * @return JsonResponse
     */
    public function jsonExceptionV2($exception)
    {
        $result['success'] = false;
        $result['message'] = $exception->getMessage();
        $result['line'] = $exception->getLine();
        $result['file'] = $exception->getFile();
        return $this->json($result, Response::HTTP_BAD_REQUEST);
    }

    /**
     * @param Exception|Error $exception
     * @param bool $expand
     * @return JsonResponse
     */
    public function jsonException($exception, bool $expand = true)
    {
        $result = new Result(false, $exception->getTrace(), 'error', $exception->getMessage(), $expand);
        return $result->jsonError();
    }


    public function toArray($object, $ignoreAttributes)
    {
        if (is_array($ignoreAttributes)) {
            $normalizer = new ObjectNormalizer();
            $encoder = new JsonEncoder();

            $serializer = new Serializer([$normalizer], [$encoder]);

            return json_decode($serializer->serialize($object, 'json', [
                AbstractNormalizer::IGNORED_ATTRIBUTES => $ignoreAttributes
            ]), true);
        }

        return json_decode($this->serializer->serialize($object, 'json'), true);
    }

    /**
     * Warning! This helps to determine if the old
     * REST method is being used in the case
     * that nothing is passed through the
     * default route in the BaseController!
     * This is done in order to accommodate
     * for using the legacy controllers.
     *
     * @param $className
     *
     * @return void
     * @throws ReflectionException
     * @author julian@corephp.com
     */
    private function setClass($className): void
    {
        $dataHandler = $this->dataHandler;

        if (empty($className)) {
            $dataHandler->setClass((new ReflectionClass($this))->getShortName());
        } else {
            $dataHandler->setClass($className);
        }
    }

    /**
     * @Rest\Post("/create/{entityName}")
     * @param Request $request
     * @param string $entityName
     *
     * @return JsonResponse
     * @throws ReflectionException|\JsonException
     */
    public function createRecord(Request $request, string $entityName = null): JsonResponse
    {
        $this->setClass($entityName);
        return $this->json($this->dataHandler->createRecord($request));
    }

    /**
     * @Rest\Post("/update/{entityName}")
     * @param Request $request
     * @param string $entityName
     *
     * @return JsonResponse
     * @throws ReflectionException
     * @throws Exception
     */
    public function updateRecord(Request $request, string $entityName = null): JsonResponse
    {
        $this->setClass($entityName);
        return $this->json($this->dataHandler->updateRecord($request), 200);
    }

    /**
     * @Rest\Post("/get/{entityName}")
     * @param Request $request
     * @param $entityName
     *
     * @return JsonResponse
     * @throws ReflectionException
     * @throws Exception
     */
    public function getAllValues(Request $request, $entityName = null): Response
    {
        $this->setClass($entityName);
        return $this->json($this->dataHandler->getAllValues($request), 200);
    }

    /**
     * @Rest\Post("/delete/{entityName}")
     * @param Request $request
     * @param string $entityName
     *
     * @return JsonResponse
     * @throws ReflectionException
     * @throws Exception
     */
    public function deleteRecord(Request $request, string $entityName = null): JsonResponse
    {
        $this->setClass($entityName);
        return $this->json($this->dataHandler->deleteRecord($request));
    }


    /**
     * @Rest\Post("/import/demo/data")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function imnportData(Request $request)
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $manager = $this->getDoctrine()->getManager();

        return $this->json($this->dummyDataService->importData($manager, $data['company']));
    }

    /**
     * @Rest\Post("/check/company/data")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkCompanyData(Request $request)
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $records = 0;
        $manager = $this->getDoctrine()->getManager();
        $company = $manager->getRepository('App\Entity\Company')->find($data['company']);
        if (@$company->getDevices()) {
            $records = $records + count($company->getDevices());
        }
        if (@$company->getSkills()) {
            $records = $records + count($company->getSkills());
        }
        if (@$company->getStations()) {
            $records = $records + count($company->getStations());
        }
        // if(@$company->getEmploymentStatuses()){
        //     $records = $records + count($company->getEmploymentStatuses());
        // }
        if ($records > 0) {
            return $this->json(['status' => false, 'message' => 'Failed']);
        } else {
            return $this->json(['status' => true, 'message' => 'Success']);
        }
    }

    /**
     * @return StripeService
     */
    public function getStripeService(): StripeService
    {
        return $this->stripeService;
    }

    /**
     * @return EmailService
     */
    public function getEmailService(): EmailService
    {
        return $this->emailService;
    }

    /**
     * Return in KM
     * @param float $latitude1
     * @param float $longitude1
     * @param float $latitude2
     * @param float $longitude2
     * @return float
     */
    public function findRadius(float $latitude1, float $longitude1, float $latitude2, float $longitude2)
    {
        $latitude1 = $this->toRadians($latitude1);
        $longitude1 = $this->toRadians($longitude1);
        $latitude2 = $this->toRadians($latitude2);
        $longitude2 = $this->toRadians($longitude2);
        return 6371.0 * acos(
                sin($latitude1) * sin($latitude2)
                + cos($latitude1) * cos($latitude2) * cos($longitude2 - $longitude1));
    }

    /**
     * @param float $value
     * @return float
     */
    public function toRadians(float $value)
    {
        return $value * pi() / 180.0;
    }
}
