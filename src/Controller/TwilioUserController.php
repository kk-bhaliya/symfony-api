<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Role;
use App\Entity\Schedule;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\User;
use App\Services\DriverService;
use App\Services\Result;
use App\Services\StationService;
use App\Services\TwilioAccountService;
use App\Services\TwilioUtil;
use App\Services\UserService;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Coupler Controller
 * @Route("/api/twilio", name="api_")
 */
class TwilioUserController extends BaseController
{
    /**
     * @Rest\POST("/create_twilio_account")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws Exception
     */
    public function createTwilioSubAccount(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $user = $this->botService->getUserEntity($data);
            $accountName = 'COMPANY->' . $company->getName() . '|ID->' . $company->getId() . '|USER-ID->' . $user->getId();

            /** Creating Service */
            $chatServiceResult = $this->twilioService->createChatService($accountName);
            $finalResponseBoolean = $chatServiceResult->isSuccess();
            $finalResponseData['createdService'] = $chatServiceResult->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }
            $data = $chatServiceResult->getData();

            /** Updating Service Channel for complete functionality on Twilio JS SKD */
            $updateRoleData = $this->twilioService->getChannelRoleFields($data);
            $updateRoleResultChannel = $this->twilioAccountService->updateRoleResource($updateRoleData);
            $finalResponseBoolean &= $updateRoleResultChannel->isSuccess();
            $finalResponseData['roleChannel'] = $updateRoleResultChannel->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }

            /** Updating Service Role for complete functionality on Twilio JS SKD */
            $updateRoleData = $this->twilioService->getServiceRoleFields($data);
            $updateRoleResultService = $this->twilioAccountService->updateRoleResource($updateRoleData);
            $finalResponseBoolean &= $updateRoleResultService->isSuccess();
            $finalResponseData['roleService'] = $updateRoleResultService->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }

            /** Updating Service Specifications */
            $updateServiceData = $this->twilioService->getServiceOptionsFields($data);
            $updateServiceResult = $this->twilioAccountService->updateServiceResource($updateServiceData);
            $finalResponseBoolean &= $updateServiceResult->isSuccess();
            $finalResponseData['updatedService'] = $updateServiceResult->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }

            /** Creating Credential ID to set up Notifications */
            $credentialData['accountSid'] = $data['service']['accountSid'];
            $credentialData['authToken'] = $data['subAccount']['authToken'];
            $credentialData['serviceSid'] = $data['service']['sid'];
            $credentialData['type'] = 'fcm';
            $credentialData['options']['friendlyName'] = 'fcm-push-credentials-' . random_int(1, PHP_INT_MAX);
            $credentialResult = $this->twilioAccountService->createCredentialResource($credentialData);
            $finalResponseBoolean &= $credentialResult->isSuccess();
            $finalResponseData['createdCredential'] = $credentialResult->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }

            /** Creating User for the Company */
            $userData['accountSid'] = $data['service']['accountSid'];
            $userData['authToken'] = $data['subAccount']['authToken'];
            $userData['serviceSid'] = $data['service']['sid'];
            $userData['identity'] = $this->botService->getIdentity($user);
            $userData['options']['friendlyName'] = $user->getFriendlyName();
            $userData['options']['attributes'] = [
                'friendlyName' => $user->getFriendlyName(),
                'email' => $user->getEmail(),
                'role' => $user->getRoleForChat()
            ];
            $userResult = $this->twilioAccountService->createUserResource($userData);
            $finalResponseBoolean &= $userResult->isSuccess();
            $finalResponseData['createdUser'] = $userResult->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }
            if ($userResult->isSuccess()) {
                $user->setUserSid($userResult->getData()['sid']);
                $entityManager->flush();
            }

            /** Creating Channel */
            $channelData['accountSid'] = $data['service']['accountSid'];
            $channelData['authToken'] = $data['subAccount']['authToken'];
            $channelData['serviceSid'] = $data['service']['sid'];
            $channelData['options']['friendlyName'] = "General";
            $channelData['options']['attributes'] =
                [

                    "kind" => "Group",
                ];
            $channelResult = $this->twilioAccountService->createChannelResource($channelData);
            $finalResponseBoolean &= $channelResult->isSuccess();
            $finalResponseData['createdChannel'] = $channelResult->getFormatData();
            if (!$finalResponseBoolean) {
                return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
            }

            /** Saving information for User and Company */
            if ($finalResponseBoolean) {
                $data = $chatServiceResult->getData();
                $apiKeyData['accountSid'] = $data['service']['accountSid'];
                $apiKeyData['authToken'] = $data['subAccount']['authToken'];
                $apiKeyData['keyName'] = $apiKeyData['accountSid'] . '-apiKey-' . rand(0, PHP_INT_MAX);
                $apiKeyResult = $this->twilioAccountService->createApiKey($apiKeyData);
                $finalResponseBoolean &= $apiKeyResult->isSuccess();
                $finalResponseData['createdApiKey'] = $apiKeyResult->getFormatData();
                if (!$finalResponseBoolean) {
                    return $this->jsonResponse($finalResponseBoolean, $finalResponseData);
                }

                $apiKeyResultData = $apiKeyResult->getData();
                $company->setTwilioApiKey($apiKeyResultData['sid']);
                $company->setTwilioApiKeySecret($apiKeyResultData['secret']);
                $company->setAuthToken($data['subAccount']['authToken']);
                $company->setAccountSid($data['service']['accountSid']);
                $company->setServiceSid($data['service']['sid']);
                $company->setChannelRoleSid($data['service']['defaultChannelRoleSid']);
                $company->setServiceRoleSid($data['service']['defaultServiceRoleSid']);
                $company->setCredentialSid($credentialResult->getData()['sid']);
                $entityManager->flush();

                /** Adding User to Channel */
                $userChannelData['channelSid'] = $channelResult->getData()['sid'];
                $userChannelData['accountSid'] = $company->getAccountSid();
                $userChannelData['authToken'] = $company->getAuthToken();
                $userChannelData['serviceSid'] = $company->getServiceSid();
                $userChannelData['identity'] = $user->getUsername();
                $userChannelData['options']['attributes'] = ['friendlyName' => $user->getFriendlyName()];
                $userChannelData['options']['attributes'] =
                    [
                        'friendlyName' => $user->getFriendlyName(),
                        'email' => $user->getEmail(),
                        'role' => $user->getRoleForChat()
                    ];
                $addUserToChannelResult = $this->twilioAccountService->createMemberResource($userChannelData);
                $finalResponseBoolean &= $addUserToChannelResult->isSuccess();
                $finalResponseData['createdMember'] = $addUserToChannelResult->getFormatData();
            }

            return $this->jsonResponse($finalResponseBoolean, $finalResponseData, Response::HTTP_CREATED);
        } catch (Exception $exception) {
            return $this->jsonResponse(false, $exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }
    }


    /**
     * @Rest\POST("/create_channel_members")
     * @param Request $request
     * @return JsonResponse
     */
    public function createChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $dataChannel = $this->dataForChannel($dataTwilio, $data);
            $members = [];
            foreach ($data['members'] as $member) {
                if (array_key_exists('id', $member)) {
                    $newUser = $this->entityManager->find(User::class, $member['id']);
                    if (!is_null($newUser->getEmail())) {
                        $members[] = ['email' => $newUser->getEmail(), 'name' => $newUser->getFriendlyName(), 'role' => $newUser->getRoleForChat()];
                    } else {
                        throw new Exception('User ' . $member['id'] . ' does not contain valid email');
                    }
                } else {
                    throw new Exception('The identifier id does not exists in members array');
                }
            }

            $channelResult = $this->twilioAccountService->createChannelResource($dataChannel);
            if (!$channelResult->isSuccess()) {
                return $channelResult->jsonError();
            }
            $channelSid = $channelResult->getData()['sid'];
            $membersResult = [];
            foreach ($members as $member) {
                $dataMember = $this
                    ->dataForMember($dataTwilio, $channelSid, $member['email'], $member['name'], $member['role']);
                $memberResult = $this->twilioAccountService->createMemberResource($dataMember);
                if (!$memberResult->isSuccess()) {
                    return $memberResult->jsonError();
                }
                $membersResult[] = $memberResult->getFormatData();
            }
            $result = ['channelSid' => $channelSid, 'membersResult' => $membersResult];
            return $this->jsonResponse(true, $result, Response::HTTP_CREATED);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create_single_channel_company")
     * @param Request $request
     * @return JsonResponse
     */
    public function createSingleUserChannelsForAllUsers(Request $request)
    {
        try {
            $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            $newUser = $this->entityManager->find(User::class, $data['userId']);
            $users = $this->entityManager->getRepository(User::class)->findTwilioUsers($company->getId(), $newUser->getId());
            $results = [];
            /** @var User $user */
            foreach ($users as $user) {
                $dataTwilio = $this->dataForTwilioAuthentication($company);
                $dataChannel = $this->dataForSingleChannel($dataTwilio, $newUser, $user);
                $channelResult = $this->twilioAccountService->createChannelResource($dataChannel);
                $channelSid = $channelResult->getData()['sid'];
                $dataMember = $this
                    ->dataForMember($dataTwilio, $channelSid, $user->getEmail(), $user->getFriendlyName(), $user->getRoleForChat());
                $dataNewMember = $this
                    ->dataForMember($dataTwilio, $channelSid, $newUser->getEmail(), $newUser->getFriendlyName(), $newUser->getRoleForChat());
                $results[]['user'] = $user->getId();
                $results[]['channelResult'] = $channelResult->getFormatData();
                $memberResult = $this->twilioAccountService->createMemberResource($dataMember);
                $results[]['memberResult'] = $memberResult->getFormatData();
                $newMemberResult = $this->twilioAccountService->createMemberResource($dataNewMember);
                $results[]['newMemberResult'] = $newMemberResult->getFormatData();
            }
            $result = new Result(true, $results, 'channels', 'Single channels created');
            return $this->json($result->getData(), Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'error', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Get("/get_twilio_users")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchTwilioUsers(Request $request)
    {
        try {
            $data['userId'] = $request->get('userId');
            $data['companyId'] = $request->get('companyId');
            $data['orderBy'] = $request->get('orderBy');
            $data['offset'] = $request->get('offset');
            $data['limit'] = $request->get('limit');

            if (!empty($request->get('version'))) {
                $data['version'] = $request->get('version');
                $users = $this->botService->fetchTwilioUsers($data, $data['version']);
            } else {
                $users = $this->botService->fetchTwilioUsers($data);
            }

            return $users->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    public function dataForSingleChannel(array $data, User $currentUser, User $user)
    {
        $data['options'] = [];
        $data['options']['friendlyName'] = $currentUser->getFriendlyName() . '|' . $user->getFriendlyName();
        $data['options']['uniqueName'] = $currentUser->getEmail() . '|' . $user->getEmail();
        $data['options']['attributes'] = ['kind' => 'Single', 'memberCount' => 2];
        return $data;
    }

    /**
     * @param array $data
     * @param array $dataReceived
     * @return array
     * @throws Exception
     */
    public function dataForChannel(array $data, array $dataReceived)
    {
        if (empty($dataReceived['friendlyName'])) {
            if (!is_string($dataReceived['friendlyName'])) {
                throw new Exception('friendlyName input is not valid');
            }
            throw new Exception('friendlyName input is not valid');
        }
        if (empty($dataReceived['uniqueName'])) {
            if (!is_string($dataReceived['uniqueName'])) {
                throw new Exception('uniqueName input is not valid');
            }
            throw new Exception('uniqueName input is not valid');
        }

        $subtitle = null;
        $channelName = null;
        $description = null;
        if (isset($dataReceived['subtitle'])) {
            if (!is_string($dataReceived['subtitle']) or empty($dataReceived['subtitle'])) {
                throw new Exception('subtitle input is not valid');
            }
            $subtitle = $dataReceived['subtitle'];
        }

        if (isset($dataReceived['channelName'])) {
            if (!is_string($dataReceived['channelName']) || empty($dataReceived['subtitle'])) {
                throw new Exception('channelName input is not valid');
            }
            $channelName = $dataReceived['channelName'];
        }
        if (isset($dataReceived['description'])) {
            if (!is_string($dataReceived['description'])) {
                throw new Exception('description input is not valid');
            }
            $description = $dataReceived['description'];
        }

        $data['options'] = [];
        $data['options']['friendlyName'] = $dataReceived['friendlyName'];
        $data['options']['uniqueName'] = $dataReceived['uniqueName'];
        $data['options']['attributes'] =
            [
                'type' => 'Group',
                'subtitle' => $subtitle,
                'channelName' => $channelName,
                'description' => $description,
            ];
        return $data;
    }

    public function dataForMember(array $data, string $channelSid, string $identity, string $friendlyName, string $role)
    {
        $data['channelSid'] = $channelSid;
        $data['identity'] = $identity;
        $data['options'] = [];
        $data['options']['attributes'] = ['friendlyName' => $friendlyName, 'role' => $role];
        return $data;
    }

    /**
     * @Rest\Post("/create_station_channel_with_user")
     * @param Request $request
     * @param StationService $stationService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function createStationChannelWithUser(Request $request, StationService $stationService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            $user = $twilioUtil->findUser($data);
            $station = $twilioUtil->findStation($data);
            $channelResult = $stationService->createStationChannel($station, $user, $twilioAccountService, $twilioUtil);
            if (!$channelResult->isSuccess()) {
                return $channelResult->jsonError();
            }
            $memberResult = $stationService->addDriverToStationChannel($station, $user, $twilioAccountService, $twilioUtil);
            if (!$memberResult->isSuccess()) {
                return $memberResult->jsonError();
            }
            return $this->json(
                [
                    'stationChannelResult' => $channelResult->getFormatData(),
                    'stationMemberResult' => $memberResult->getFormatData()
                ], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/add_user_to_station_channel")
     * @param Request $request
     * @param StationService $stationService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function addUserToStationChannel(Request $request, StationService $stationService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            $user = $twilioUtil->findUser($data);
            $station = $twilioUtil->findStation($data);
            if (!$station->getChannelSid()) {
                throw new Exception('Station Channel not found.');
            }
            $memberResult = $stationService->addDriverToStationChannel($station, $user, $twilioAccountService, $twilioUtil);
            if (!$memberResult->isSuccess()) {
                return $memberResult->jsonError();
            }
            return $this->json(
                [
                    'stationMemberResult' => $memberResult->getFormatData()
                ], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_twilio_users_information")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getTwilioUsersInformation(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $result = $userService->getTwilioUsersInformation($data);
            return $this->json($result, Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_twilio_users_information_v2")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getTwilioUsersInformationV2(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(User::class)->getTwilioUsersV2(['company' => $data['companyId']]);
            $users = [];
            foreach ($result as $user) {
                $users[$user['id']][] = $user;
            }
            foreach ($users as $user) {
                $users[$user[0]['id']] = [
                    'userId' => $user[0]['id'],
                    'driverId' => $user[0]['driverId'],
                    'identity' => $user[0]['identity'],
                    'email' => $user[0]['email'],
                    'role' => array_unique(array_map(function ($item) {
                        $role = str_replace('ROLE_', '', $item['roleName']);
                        $role = str_replace("_", " ", $role);
                        $role = ucwords(strtolower($role));
                        $role = str_replace('Hr ', 'HR ', $role);
                        return $role;
                    }, $user))[0],
                    'friendlyName' => $this->getFriendlyName($user[0]['friendlyName'], $user[0]['firstName'], $user[0]['lastName']),
                    'image' => $user[0]['profileImage'],
                    'skills' => array_values(array_unique(array_map(function ($item) {
                        return $item['skillName'];
                    }, $user))),
                    'stations' => array_unique(array_map(function ($item) {
                        return $item['stationCode'];
                    }, $user)),
                    'schedules' => $this->entityManager->getRepository(Schedule::class)->getSchedulesNamesByDriver($user[0]['driverId'])
                ];
            }
            $stations = $this->entityManager->getRepository(Station::class)
                ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

            $stations = array_map(function (Station $station) {
                return $station->getCode();
            }, $stations);

            $skills = $this->entityManager->getRepository(Skill::class)
                ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

            $skills = array_map(function (Skill $skill) {
                return $skill->getSkillName();
            }, $skills);

            $roles = $this->entityManager->getRepository(Role::class)
                ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

            $roles = array_map(function (Role $role) {
                return $role->getRoleName();
            }, $roles);

            $schedules = $this->entityManager->getRepository(Schedule::class)
                ->findBy(['company' => $data['companyId'], 'isArchive' => false]);

            $schedules = array_map(function (Schedule $schedule) {
                return $schedule->getName();
            }, $schedules);
            return $this->json([
                'users' => array_values($users),
                'roles' => $roles,
                'skills' => $skills,
                'stations' => $stations,
                'schedules' => $schedules,
                'count' => sizeof($users),
            ], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    public function getFriendlyName($friendlyName, $firstName, $lastName): string
    {
        if (is_string($friendlyName)) {
            return $friendlyName;
        }
        if (is_string($firstName . ' ' . $lastName)) {
            return $firstName . ' ' . $lastName;
        }
        if (is_string($firstName)) {
            return $firstName;
        }
        if (is_string($lastName)) {
            return $lastName;
        }
        return "N/A";
    }

    /**
     * @Rest\Post("/get_twilio_users_information_test")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getTwilioUsersInformationTest(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $result = $userService->getTwilioUsersInformationTest($data);
            return $this->json($result, Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_twilio_users_information_load_out")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getTwilioUsersLoadOut(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $user = $this->entityManager->find(User::class, $data['user']);
            $driverRoutes = $this->entityManager->getRepository(DriverRoute::class)->getDriverRoutesOfToday($data);
            $users = [];
            foreach ($driverRoutes as $driverRoute) {
                /** @var DriverRoute $driverRoute */
                if ($driverRoute->getDriver() instanceof Driver && $driverRoute->getDriver()->getUser() instanceof User) {
                    $user = $driverRoute->getDriver()->getUser();
                    $dataUser = $userService->dataUserFilter($user);
                    $dataRoutes = [
                        'punchIn' => $driverRoute->getPunchIn(),
                        'shiftType' => $driverRoute->getShiftType()->getName()
                    ];
                    $dataUser = array_merge($dataUser, $dataRoutes);
                    $users[] = $dataUser;
                }
            }
            return $this->json([
                    'users' => $users,
                    'roles' => $userService->dataSkillsByCompany($user),
                    'skills' => $userService->dataSkillsByCompany($user),
                    'stations' => $userService->dataStationByCompany($user),
                ]
                , Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_twilio_new_users_information")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getNewTwilioUsersInformation(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $result = $userService->getTwilioUsersInformation($data);
            return $this->json($result, Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_users_by_channel")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getUsersByChannel(Request $request, TwilioUtil $twilioUtil, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $result = $twilioUtil->getMembersOfChannelV2($data, $userService);
            if ($result->isSuccess()) {
                return $result->jsonResponse(true);
            }
            return $result->jsonError();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/get_users_to_add_by_channel")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getUsersToAddByChannel(Request $request, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $usersData = $userService->getTwilioUsersInformation($data);
            $users = $usersData['users'];
            $result = $twilioUtil->readMembersOfChannelIdentities($data, $twilioAccountService);
            $identities = $result->getData();
            if ($result->isSuccess()) {
                $list = [];
                foreach ($users as $user) {
                    if (!in_array($user['identity'], $identities)) {
                        $list[] = $user;
                    }
                }
                return $this->json([
                    'identities' => $identities,
                    'users' => $users,
                    'count' => sizeof($list),
                    'roles' => $usersData["roles"],
                    'skills' => $usersData["skills"],
                    'stations' => $usersData["stations"],
                ], Response::HTTP_OK);
            }
            return $result->jsonError();
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/get_users_to_create_duo_channel")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getUsersToCreateDuoChannels(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $companyId = $data['company']['id'];
            $identities = $data['identities'];
            $users = $this->entityManager->getRepository(User::class)->getTwilioUsers(['company' => $companyId]);
            $usersResult = [];
            if (count($users) < 1) {
                throw new Exception('No twilio users found.');
            }
            foreach ($users as $user) {
                $tmp = $userService->validateUserV2($user);
                if ($tmp['valid']) {
                    if (!in_array($tmp['data']['identity'], $identities)) {
                        $usersResult[] = $tmp['data'];
                    }
                }
            }
            $stations = $this->entityManager->getRepository(Station::class)->getStationCodesByCompany($companyId);
            $roles = $this->entityManager->getRepository(Role::class)->getRolesNameByCompany($companyId);
            $skills = $this->entityManager->getRepository(Skill::class)->getSkillsNameByCompany($companyId);
            $result = new Result(true, [
                'users' => $usersResult,
                'roles' => $roles,
                'skills' => $skills,
                'stations' => $stations,
            ], 'users');
            return $result->jsonResponse(true);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/get_search_twilio_users_mobile")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function getSearchTwilioUsersMobile(Request $request, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $identities = !empty($data['identities']) ? $data['identities'] : [];
            $users = $this->entityManager->getRepository(User::class)->getTwilioUsersV1($data);
            $usersResult = [];
            if(count($identities) > 0){
                foreach ($users as $user) {
                    if (!in_array($user['identity'], $identities)) {
                        $usersResult[] = $userService->validateUserMobile($user);
                    }
                }
                return $this->json(['users' => $usersResult, 'count' => count($usersResult)], Response::HTTP_OK);
            }
            return $this->json(['users' => $users, 'count' => count($users)], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

}