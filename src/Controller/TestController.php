<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\DriverRouteCode;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\Task;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Services\Result;
use DateTime;
use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Test Controller
 *
 * @Route("/api",name="api_")
 */
class TestController extends BaseController
{

    /**
     * @Rest\Post("/test")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function test(Request $request): JsonResponse
    {
        $route = $this->entityManager->find(DriverRoute::class, 15483);
        return $this->jsonResponse(true, [
            $route
        ]);
    }

    /**
     * @Rest\Post("/random/twilio/create/channel/all")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function twilioCreateChannel(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $company = $this->botService->getCompanyEntity($data);
        $dataAuth = $this->botService->dataForTwilioAuthentication($company);
        $this->twilioAccountService->createChannelResource($data);
        return $this->jsonResponse(true, []);
    }

    /**
     * @Rest\Post("/random/update/password/all")
     * @param Request $request
     *
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @throws Exception
     */
    public function updatePassword(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        set_time_limit(180);
        $data = $request->request->all();
        $company = $this->botService->getCompanyEntity($data);
        $users = $company->getUsers();
        foreach ($users as $user) {
            if ($user->getIsArchive() === false) {
                $encoded = $encoder->encodePassword($user, 'Private1!');
                $user->setPassword($encoded);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
            }
        }

        return $this->jsonResponse(true, []);
    }

    /**
     * @Rest\Post("/update/password/user")
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @throws Exception
     */
    public function updatePasswordUser(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $this->entityManager->getRepository(User::class)->findOneBy([$data['key'] => $data['value']]);
            if (!($user instanceof User)) {
                throw new Exception('User not found.');
            }
            $encoded = $encoder->encodePassword($user, $data['password']);
            $user->setPassword($encoded);
            $this->entityManager->flush();
            return $this->jsonResponse(true, $user, 200, 'user', 'User password updated');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/test/get_driver_shifts")
     * @param Request $request
     * @return JsonResponse
     */
    public function getDriverShifts(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            if (empty($data['driverId'])) {
                throw new Exception('Driver ID not found');
            }
            /** @var Driver $driver */
            $driver = $this->entityManager
                ->getRepository(Driver::class)
                ->findOneBy(['id' => $data['driverId'], 'isArchive' => false]);

            if ($driver === null) {
                throw new Exception('Driver not found');
            }

            $driverRoutes = $driver->getDriverRoutes();
            $result = [];
            foreach ($driverRoutes as $driverRoute) {
                $result['shifts'][$driverRoute->getId()] =
                    [
                        'dateCreated' => $driverRoute->getDateCreated(),
                        'shift' => $driverRoute->getShiftType(),
                    ];
            }
            return $this->jsonResponse(true, $result);
        } catch (Exception $e) {
            return $this->jsonException($e);
        }
    }

    /**
     * @Rest\Post("/test/get_routes_by_driver_by_day")
     * @param Request $request
     * @return JsonResponse
     */
    public function getRoutesByDriverByDay(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            if (empty($data['driverId'])) {
                throw new Exception('Driver ID not found');
            }
            if (empty($data['date'])) {
                throw new Exception('Date not found');
            }
            /** @var Driver $driver */
            $driver = $this->entityManager
                ->getRepository(Driver::class)
                ->findOneBy(['id' => $data['driverId'], 'isArchive' => false]);
            if ($driver === null) {
                throw new Exception('Driver not found');
            }
            $dateTime = new DateTime($data['date']);
            $routes = $this
                ->entityManager
                ->getRepository(DriverRoute::class)
                ->findBy(
                    [
                        'driver' => $data['driverId'],
                        'dateCreated' => $dateTime,
                        'isArchive' => false,
                    ], ['startTime' => 'DESC']);

            $result = [];
            /** @var DriverRoute $route */
            foreach ($routes as $route) {
                $routeCodes = array_map(function (DriverRouteCode $item) {
                    return $item->getCode();
                }, $route->getDriverRouteCodes()->toArray());
                $result[] =
                    [
                        'route' => $route,
                        'dateCreated' => $route->getDateCreated(),
                        'shift' => $route->getShiftType(),
                        'routeCodes' => $routeCodes,
                    ];
            }

            return $this->jsonResponse(true, $result);
        } catch (Exception $e) {
            return $this->jsonException($e);
        }
    }

    /**
     * @Rest\Post("/test/add/driver/to/notification")
     * @param Request $request
     * @return JsonResponse
     */
    public function testAddDriverToNotificationChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $driver = $this->entityManager->find(Driver::class, $data['id']);
            $result = $this->botService->addDriverToNotificationChannel($driver);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/add_mileage_vehicles")
     * @return JsonResponse
     */
    public function addMileageVehicles()
    {
        try {
            $kickoffLogs = $this->entityManager
                ->getRepository(KickoffLog::class)
                ->findBy(['isArchive' => false]);

            $result = [];
            foreach ($kickoffLogs as $kickoffLog) {
                $vehicle = $kickoffLog->getVehicle();
                $gasTankLevel = $kickoffLog->getGasTankLevel();
                $mileage = $kickoffLog->getMileage();
                if (($vehicle instanceof Vehicle) && $gasTankLevel && $mileage) {
                    $vehicle->setCurrentGasTankLevel($gasTankLevel);
                    $vehicle->setCurrentMileage($mileage);
                    $this->entityManager->flush();
                    $result[] = [
                        'kickoffLog' => ['id' => $kickoffLog->getId()],
                        'vehicle' => ['id' => $vehicle->getId()]
                    ];
                }
            }
            $result = new Result(true, $result);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/update_user_driver_connection")
     * @return JsonResponse
     */
    public function updateUserDriverConnection()
    {
        try {
            $users = $this->entityManager->getRepository(User::class)
                ->findBy(['isArchive' => false]);
            $resultUsers = [];
            foreach ($users as $user) {
                $resultUsers[] = [
                    'getId' => $user->getId(),
                    'getFriendlyName' => $user->getFriendlyName(),
                    'driverFromUser' => $user->getDriver() ? $user->getDriver()->getId() : null,
                    'userFromDriver' => $user->getDriver() ? $user->getDriver()->getUser()->getId() : null,
                ];
            }

            $drivers = $this->entityManager
                ->getRepository(Driver::class)->findBy(['isArchive' => false]);
            $resultDrivers = [];
            $syncUsers = [];
            foreach ($drivers as $driver) {
                $user = $driver->getUser();
                if ($user instanceof User) {
                    if (!($user->getDriver() instanceof Driver)) {
                        $user->setDriver($driver);
                        $this->entityManager->flush();
                        $syncUsers[] = [
                            'user' => $user instanceof User ? $user->getId() : $user,
                            'driverFromUser' => $user->getDriver() instanceof Driver ? $user->getDriver()->getId() : $user->getDriver(),
                            'driver' => $driver instanceof Driver ? $driver->getId() : $driver,
                            'userFromDriver' => $driver->getUser() instanceof User ? $driver->getUser()->getId() : $user,
                        ];
                    }
                }
                $resultDrivers[] = [
                    'user' => $user instanceof User ? $user->getId() : $user,
                    'driverFromUser' => $user->getDriver() instanceof Driver ? $user->getDriver()->getId() : $user->getDriver(),
                    'driver' => $driver instanceof Driver ? $driver->getId() : $driver,
                    'userFromDriver' => $driver->getUser() instanceof User ? $driver->getUser()->getId() : $user,
                ];
            }
            $result = new Result(true, [
                'countUsers' => count($resultUsers),
                'resultUsers' => $resultUsers,
                'countDrivers' => count($resultDrivers),
                'resultDrivers' => $resultDrivers,
                'countSyncUsers' => count($syncUsers),
                'syncUsers' => $syncUsers,
            ]);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/test/get_tasks_by_user")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getTasksByUser(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $result = $this->entityManager->getRepository(User::class)->getTasksByUser($data['userId']);
        return $this->json($result);
    }

    /**
     * @Rest\Post("/test/get_timeline_events")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function getTimelineEventsByDevice(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $result = $this->entityManager->getRepository(Event::class)->getTimelineEvents($data);
        return $this->json(array_slice($result, 0, 31));
    }

}
