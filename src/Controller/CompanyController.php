<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Company Controller
 * @Route("/api",name="api_")
 */
class CompanyController extends BaseController
{
    /**
     * @Rest\Post("/company/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function createCompanyAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/company/create/twilio")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function createteilio(Request $request): JsonResponse
    {
        $companyId =  $this->createRecord($request);
        $subAccount = $this->twilioService->createChatService($companyId);
        return new JsonResponse([$companyId, $subAccount], Response::HTTP_CREATED);
    }

    /**
     * @Rest\Post("/company/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateCompanyAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/company/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCompanyAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }
}
