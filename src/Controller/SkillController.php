<?php

namespace App\Controller;

use App\Entity\DriverSkill;
use App\Entity\Skill;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Skill Controller
 * @Route("/api",name="api_")
 */
class SkillController extends BaseController
{
    /**
     * @Rest\Post("/skill/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createSkillAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/skill/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function updateSkillAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/skill/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function getSkillAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/skill/get_skills")
     * @param Request $request
     * @return JsonResponse
     */
    public function getSkills(Request $request)
    {
        try {
            $data = $request->request->all();
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            $skills = $this->entityManager->getRepository(Skill::class)->findBy(['company' => $data['companyId'], 'isArchive' => false]);
            return $this->jsonResponse(true, $skills, 200, 'skills');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/skill/get_driver_skills")
     * @param Request $request
     * @return JsonResponse
     */
    public function getDriverSkills(Request $request)
    {
        try {
            $data = $request->request->all();
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            $driverSkills = $this->entityManager->getRepository(DriverSkill::class)->findBy(['isArchive' => false]);
            $driverSkills = array_map(function (DriverSkill $item){
                return [
                    'id' => $item->getId(),
                    'skill' => $item->getSkill(),
                ];
            }, $driverSkills);
            return $this->jsonResponse(true, $driverSkills, 200, 'skills');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }
}
