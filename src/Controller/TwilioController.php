<?php

namespace App\Controller;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\DriverSkill;
use App\Entity\Station;
use App\Entity\User;
use App\Services\DriverService;
use App\Services\Result;
use App\Services\SingleResult;
use App\Services\StationService;
use App\Services\TwilioAccountService;
use App\Services\TwilioUtil;
use App\Services\UserService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * @Route("/api/twilio", name="api_")
 */
class TwilioController extends BaseController
{
    /**
     * @param $result
     * @param int $status
     * @return JsonResponse
     */
    public function newResponse($result, $status = 200)
    {
        if ($result instanceof Result) {
            return $this->json($result->getFormatData(), $status);
        }
        if ($result instanceof SingleResult) {
            return $this->json($result->getFormatData(), $status);
        }
        return $this->json($result, $status);
    }

    /**
     * @Rest\POST("/find_subaccount")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function findSubAccount(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $result = $this->twilioService->findSubAccount($data['accountName']);
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/fetch_subaccount")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function fetchSubAccount(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $result = $this->twilioService->fetchSubAccountInformation($data['accountName']);
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/update_subaccount")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateSubAccount(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'updateSubAccount');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/create_api_key")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createApiKey(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $user = $entityManager->getRepository(User::class)->find($data['userId']);
        /** @var Company $company */
        $company = $user->getCompany();
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['keyName'] = array_key_exists('keyName', $data) ? $data['keyName'] : $data['accountSid'] . '-apiKey-' . rand(0, PHP_INT_MAX);
        $result = $this->twilioAccountService->createApiKey($data);
        if ($result->isSuccess()) {
            $apiKeyData = $result->getData();
            $company->setTwilioApiKey($apiKeyData['sid']);
            $company->setTwilioApiKeySecret($apiKeyData['secret']);
            $entityManager->flush();
        }
        return $this->newResponse($result, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Post("/fetch_auth_token")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createAccessAuthToken(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $user = $entityManager->find(User::class, $data['userId']);
        /** @var Company $company */
        $company = $user->getCompany();
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        $data['identity'] = $user->getChatIdentifier();
        $data['apiKey'] = $company->getTwilioApiKey();
        $data['apiSecret'] = $company->getTwilioApiKeySecret();
        if(!empty($data['platform'])){
            if($data['platform'] === "ios"){
                $data['credentialSid'] = $company->getApnCredentialSid();
                if(is_null($data['credentialSid'])){
                    $data['credentialSid'] = $company->getCredentialSid();
                }
                $data['apnCredentialSid'] = $company->getApnCredentialSid();
            }else{
                $data['credentialSid'] = $company->getCredentialSid();
                $data['fcmCredentialSid'] = $company->getFcmCredentialSid();
            }
        }else{
            $data['credentialSid'] = $company->getCredentialSid();
            $data['fcmCredentialSid'] = $company->getFcmCredentialSid();
        }
        $data['ttl'] = array_key_exists('ttl', $data) ? $data['ttl'] : 64000;
        $result = $this->twilioAccountService->createAuthTokenToChat($data);
        return $this->json($result->getData(), Response::HTTP_CREATED);
    }

    /**
     * @param array $correctKeys
     * @param array $givenKeys
     * @return array
     */
    private function checkKeys(array $correctKeys, array $givenKeys)
    {
        $result = array_diff($correctKeys, $givenKeys);
        return ['count' => count($result), 'keys' => $result];
    }

    /**
     * @Rest\Post("/fetch_service")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function fetchServiceResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'fetchServiceResource');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/update_service")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateServiceResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'updateServiceResource');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/create/user/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function createUserResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $newUser = $this->botService->getUserEntity($data);
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $identity = $newUser->getId();
            $newUser->setChatIdentifier($identity);
            $data['identity'] = $identity;
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->createUserResource($data);
            if ($result->isSuccess()) {
                $newUser->setUserSid($result->getData()['sid']);
                $this->entityManager->flush();
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/update/user/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateUserResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $user = $this->botService->getUserEntity($data);
            $dataTwilio = $this->dataForTwilioAuthentication($user->getCompany());
            $data['userSid'] = $user->getUserSid();
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->updateUserResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\POST("/delete/user/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function deleteUserResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $user = $this->entityManager->getRepository(User::class)->findOneBy(['userSid' => $data['userSid']]);
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->deleteUserResource($data);
            if ($result->isSuccess()) {
                $user->setUserSid(null);
                $user->setChatIdentifier(null);
                $user->setIsArchive(true);
                $this->entityManager->flush();
                return $this->json($result->getFormatData(), Response::HTTP_OK);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create/channel/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function createChannelResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->createChannelResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\POST("/update/channel/resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateChannelResource(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->updateChannelResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\POST("/replace_attributes/channel/resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function replaceAttributesChannelResource(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $dataTwilio['channelSid'] = $data['channelSid'];
            $resultChannel = $this->twilioAccountService->fetchChannelResource($dataTwilio);
            $resultChannelAttributes = json_decode($resultChannel->getData()['attributes'], true);
            $attributes = $data['attributes'];
            $updatedChannelAttributes = $resultChannelAttributes;
            foreach ($attributes as $key => $value) {
                if (isset($updatedChannelAttributes[$key])) {
                    $updatedChannelAttributes[$key] = $value;
                }
            }
            $dataChannel = array_merge($data, $dataTwilio);
            $dataChannel['options']['attributes'] = $updatedChannelAttributes;
            $result = $this->twilioAccountService->updateChannelResource($dataChannel);
            if ($result->isSuccess()) {
                return $result->jsonResponse();
            }
            return $this->json($result->getData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/delete/channel/resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function deleteChannel(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->deleteChannelResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/delete/channel/resource/multiple")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function deleteChannels(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $results = [];
            foreach ($data['channels'] as $channel) {
                $data = array_merge(['channelSid' => $channel], $dataTwilio);
                $result = $this->twilioAccountService->deleteChannelResource($data);
                $results[] = $result;
            }

            return $this->json($results, 200);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/fetch/channel/resource")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchChannelResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->fetchChannelResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\GET("/fetch_multiple_channel")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function fetchMultipleChannelResources(Request $request, EntityManagerInterface $entityManager)
    {
        $userId = $request->get('userId');
        $data = $this->twilioArrayInformation($entityManager, $userId);
        if (count($data) > 0) {
            $data['filter'] = $request->get('filter') ? $request->get('filter') : null;
            $result = $this->twilioAccountService->fetchMultipleChannelResources($data);
            return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
        }

        return $this->json(['success' => false], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\Post("/company/fetch_multiple_channels")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchMultipleChannels(Request $request)
    {
        $data = $request->request->all();
        $company = $this->entityManager->find(Company::class, $data['companyId']);
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        $result = $this->twilioAccountService->fetchMultipleChannelResources($data);
        if (!$result->isSuccess()) {
            return $result->jsonError();
        }
        $dataChannels = $result->getData();
        $dataResult = [];
        $channelSids = [];
        if (!empty($data['channelKind']) && is_string($data['channelKind'])) {
            foreach ($dataChannels as $dataChannel) {
                $attr = json_decode($dataChannel['attributes'], true);
                if (array_key_exists('kind', $attr) && $attr['kind'] === $data['channelKind']) {
                    $dataResult[] = [
                        'type' => $data['channelKind'],
                        'attributes' => $attr,
                        'channel' => $dataChannel,
                    ];
                    $channelSids[] = $dataChannel['sid'];
                }
            }
        } elseif (array_key_exists('notKey', $data) && isset($data['notKey'])) {
            foreach ($dataChannels as $dataChannel) {
                $attr = json_decode($dataChannel['attributes'], true);
                if (!array_key_exists($data['notKey'], $attr)) {
                    $dataResult[] = [
                        'type' => 'notKey',
                        'attributes' => $attr,
                        'channel' => $dataChannel,
                    ];
                    $channelSids[] = $dataChannel['sid'];
                }
            }
        } elseif (array_key_exists('noKind', $data)) {
            foreach ($dataChannels as $dataChannel) {
                $attr = json_decode($dataChannel['attributes'], true);
                if (!array_key_exists('kind', $attr)) {
                    $dataResult[] = [
                        'type' => 'noKind',
                        'attributes' => $attr,
                        'channel' => $dataChannel,
                    ];
                    $channelSids[] = $dataChannel['sid'];
                }
            }
        } else {
            foreach ($dataChannels as $dataChannel) {
                $dataResult[] = [
                    'type' => 'normal',
                    'attributes' => json_decode($dataChannel['attributes'], true),
                    'channel' => $dataChannel,
                ];
                $channelSids[] = $dataChannel['sid'];
            }
        }

        return $this->json(['count' => count($dataResult), 'channels' => $dataResult, 'channelSids' => $channelSids], Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/company/update_multiple_channels")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateCustomChannels(Request $request)
    {
        $data = $request->request->all();
        $company = $this->entityManager->find(Company::class, $data['companyId']);
        $data['accountSid'] = $company->getAccountSid();
        $data['authToken'] = $company->getAuthToken();
        $data['serviceSid'] = $company->getServiceSid();
        if (count($data) > 0) {
            $result = $this->twilioAccountService->fetchMultipleChannelResources($data);
            $dataChannels = $result->getData();
            $dataResult = [];
            foreach ($dataChannels as $dataChannel) {
                $attributes = json_decode($dataChannel['attributes'], true);
                $updateChannel['channelSid'] = $dataChannel['sid'];
                $updateChannel['accountSid'] = $data['accountSid'];
                $updateChannel['authToken'] = $data['authToken'];
                $updateChannel['serviceSid'] = $data['serviceSid'];
                if (isset($attributes['kind']) && !array_key_exists('type', $attributes)) {
                    $updateChannel['options']['attributes'] = $attributes;
                    $updateChannel['options']['attributes']['type'] = $attributes['kind'];
                    $updateChannelResult = $this->twilioAccountService->updateChannelResource($updateChannel);
                    $dataResult[] = $updateChannelResult->getData();
                }
            }
            $result = $result->getFormatData();
            $dataResult['length'] = count($result['channels']);
            return $this->json($dataResult, Response::HTTP_ACCEPTED);
        }

        return $this->json(['success' => false], Response::HTTP_BAD_REQUEST);
    }

    /**
     * @Rest\POST("/read_multiple_user_channels")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @param UserService $userService
     * @return JsonResponse
     */
    public function readMultipleUserChannels(Request $request, TwilioUtil $twilioUtil, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $user = $userService->findUser('id', $data['user']['id']);
            /** @var Company $company */
            $company = $user->getCompany();
            $data = $twilioUtil->getTwilioAuthenticationData($company);
            $data['userSid'] = $user->getUserSid();
            $resultUserChannels = $this->twilioAccountService->readMultipleUserChannels($data);
            $resultData = [];
            if ($resultUserChannels->isSuccess()) {
                $channels = $resultUserChannels->getData();
                foreach ($channels as $channel) {
                    $data['channelSid'] = $channel['channelSid'];
                    $resultChannel = $this->twilioAccountService->fetchChannelResource($data);
                    $resultChannelData = $resultChannel->getData();
                    $resultChannelData['resource'] = $channel;
                    $resultData['channelResources'][] = $resultChannelData;
                    if (strpos($resultChannel->getData()['attributes'], 'Duo') !== false) {
                        $resultData['channelsDuo'][] = $channel['channelSid'];
                    }
                    if (strpos($resultChannel->getData()['attributes'], 'Group') !== false) {
                        $resultData['channelsGroup'][] = $channel['channelSid'];
                    }
                    if (strpos($resultChannel->getData()['attributes'], 'Bot') !== false) {
                        $resultData['channelsBot'][] = $channel['channelSid'];
                    }
                }
                foreach ($channels as $channel) {
                    $resultData['channels'][] = $channel['channelSid'];
                }
            }
            $result = new Result(true, $resultData, 'result');
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create/channel/members")
     * @param Request $request
     * @return JsonResponse
     */
    public function createChannelWithMembers(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->createChannelWithUsers($data);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create_duo_channel")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return JsonResponse
     */
    public function createDuoChannel(Request $request, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        try {
            $data = $request->request->all();
            $result = $twilioUtil->createDuoChannelWithMembersV2($twilioAccountService, $data);
            return $this->jsonResponse(true, $result);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    public function dataForDuoChannel(array $data)
    {
        $criteria['companyId'] = $data['companyId'];
        $user1 = $this->entityManager->find(User::class, $data['userId1']);
        $user2 = $this->entityManager->find(User::class, $data['userId2']);
        $criteria['options']['attributes'] = [
            "kind" => "Duo",
            "type" => "Duo",
            "nameOfChannel" => $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier(),
        ];
        $criteria['options']['friendlyName'] = $user1->getChatIdentifier() . "|" . $user2->getChatIdentifier();
        $criteria['members'] = [
            [
                'userId' => $user1->getId(),
            ],
            [
                'userId' => $user2->getId()
            ]
        ];

        return $criteria;
    }

    /**
     * @Rest\Post("/create/duo/channel/members")
     * @param Request $request
     * @return JsonResponse
     */
    public function createDuoChannelWithMembers(Request $request)
    {
        try {
            $data = $request->request->all();
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            if (empty($data['userId1'])) {
                throw new Exception('userId1 field missing.');
            }
            if (empty($data['userId2'])) {
                throw new Exception('userId2 field missing.');
            }
            if ($data['userId2'] === $data['userId1']) {
                throw new Exception('userId1 is equal userId2 and that should not be.');
            }
            $criteria = $this->dataForDuoChannel($data);
            return $criteria;
//            $result = $this->botService->createChannelWithUsers($criteria);
//            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/fetch/multiple/members/by/channel")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchMembersByChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->fetchMembersByChannel($data);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/fetch/users/by/channel")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchUsersByChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->fetchUsersByChannel($data);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/add/members/channel")
     * @param Request $request
     * @return JsonResponse
     */
    public function addMembersToChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->addUsersToExistingChannel($data);
            if ($result->isSuccess()) {
                return $result->jsonResponse();
            }
            return $result->jsonError();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/add/one/member/to/channel")
     * @param Request $request
     * @return JsonResponse
     */
    public function createMemberResource(Request $request)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['companyId']);
            if ($company === null) {
                throw new Exception('Company does not exist');
            }
            $user = $this->entityManager->find(User::class, $data['userId']);
            if ($user === null) {
                throw new Exception('User does not exist');
            }
            $dataTwilio = $this->dataForTwilioAuthentication($company);
            $identity = $user->getUniqueIdentifier();
            if ($user->getChatIdentifier() === null) {
                $user->setChatIdentifier($identity);
                $this->entityManager->flush();
            } else {
                $identity = $user->getChatIdentifier();
            }
            $data['identity'] = $identity;
            $data = array_merge($data, $dataTwilio);
            $result = $this->twilioAccountService->createMemberResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/get_user_channel_resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function getUserChannelResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'getUserChannelResource');
        return $this->jsonResponse($result->isSuccess(), $result->getData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/get_member_resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function getMemberResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'getMemberResource');
        return $this->newResponse($result, Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/update_member_resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateMemberResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'updateMemberResource');
        return $this->newResponse($result, Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/get_role_resource")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function getRoleResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'getRoleResource');
        return $this->jsonResponse($result->isSuccess(), $result->getData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\Post("/create_credential")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createCredentialResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'createCredentialResource');
        return $this->jsonResponse($result->isSuccess(), $result->getData(), Response::HTTP_CREATED);
    }

    /**
     * @Rest\Post("/create_role")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createRoleResource(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'createRoleResource');
        return $this->jsonResponse($result->isSuccess(), $result->getData(), Response::HTTP_CREATED);
    }

    /**
     * @Rest\POST("/fetch_message")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function fetchMessage(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'fetchMessageResource');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/read_multiple_messages")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function readMultipleMessage(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'readMultipleMessageResources');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/delete/message")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function deleteMessage(Request $request, EntityManagerInterface $entityManager)
    {
        try {
            $result = $this->botService->deleteMessage($request->request->all());
            return $result->jsonResponse();
        } catch (Exception $exception) {
            $result = new Result();
            return $result->jsonException($exception);
        }
    }

    /**
     * @Rest\POST("/delete_all_messages")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function deleteAllMessage(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'deleteMessagesResources');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/update_message")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateMessage(Request $request, EntityManagerInterface $entityManager)
    {
        $result = $this->postHandler($request, $entityManager, 'updateMessageResource');
        return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
    }

    /**
     * @Rest\POST("/create_message")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function createMessage(Request $request, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            $data = array_merge($data, $twilioUtil->getTwilioAuthData($data));
            $result = $twilioAccountService->createMessageResource($data);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    private function postHandler(Request $request, EntityManagerInterface $entityManager, string $functionName)
    {
        try {
            $data = $request->request->all();
            if (isset($data['twilio'])) {
                if (method_exists($this->twilioService, $functionName)) {
                    return $this->twilioService->$functionName($data);
                }

            }
            if (count($data) > 0) {
                if (!array_key_exists('userId', $data)) {
                    if (method_exists($this->twilioAccountService, $functionName)) {
                        return $this->twilioAccountService->$functionName($data);
                    }
                }
                /** @var User $user */
                $user = $entityManager->find(User::class, $data['userId']);
                if ($user !== null) {
                    $data['userSid'] = $user->getUserSid();
                    $data['identity'] = $user->getEmail();
                    $data['friendlyName'] = isset($data['friendlyName']) ? $data['friendlyName'] : $user->getFriendlyName();

                    /** @var Company $company */
                    $company = $user->getCompany();
                    $data['accountSid'] = $company->getAccountSid();
                    $data['authToken'] = $company->getAuthToken();
                    $data['serviceSid'] = $company->getServiceSid();
                    return $this->twilioAccountService->$functionName($data);
                }
            }
            return new Result(false,
                [],
                'Error', 'Not User Found', false);


        } catch (Exception $exception) {
            return new Result(false,
                [
                    'exception' => $exception->getMessage()
                ],
                'Error', 'Error in function : ' . $functionName . '()', false);
        }
    }

    private function twilioArrayInformation(EntityManagerInterface $entityManager, string $userId)
    {
        /** @var User $user */
        $user = $entityManager->find(User::class, $userId);
        if ($user instanceof User) {
            $data['userSid'] = $userId;
            $data['identity'] = $user->getEmail();
            $company = $user->getCompany();
            $data['accountSid'] = $company->getAccountSid();
            $data['authToken'] = $company->getAuthToken();
            $data['serviceSid'] = $company->getServiceSid();
            return $data;
        } else {
            return [];
        }
    }

    private function getHandler(Request $request, EntityManagerInterface $entityManager, string $functionName)
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        try {
            if (isset($data['twilio'])) {
                if (method_exists($this->twilioService, $functionName)) {
                    return $this->twilioService->$functionName($data);
                }
            }
            if (count($data) > 0) {
                if (!array_key_exists('userId', $data)) {
                    if (method_exists($this->twilioAccountService, $functionName)) {
                        return $this->twilioAccountService->$functionName($data);
                    }
                }
                /** @var User $user */
                $user = $entityManager->find(User::class, $data['userId']);
                if ($user !== null) {
                    $data['userSid'] = $user->getUserSid();
                    $data['identity'] = $user->getEmail();
                    /** @var Company $company */
                    $company = $user->getCompany();
                    $data['accountSid'] = $company->getAccountSid();
                    $data['authToken'] = $company->getAuthToken();
                    $data['serviceSid'] = $company->getServiceSid();
                    return $this->twilioAccountService->$functionName($data);
                }
            }
        } catch (Exception $exception) {
            return new Result(false, ['exception' => $exception->getMessage()], 'Error', 'Error in function : ' . $functionName . '()', false);
        }
        return new Result(false, [], 'Error', 'Not User Found', false);
    }

    /**
     * @Rest\Post("/members/creation")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function twilioUsersCreation(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $company = $this->botService->getCompanyEntity($data);
        $users = $company->getUsers();
        $results = [];
        foreach ($users as $user) {
            if ($user->getIsArchive() === false && $user->getChatIdentifier() === null) {
                $results[] = $this->botService->createUserForChat($user)->getFormatData();
            }
        }
        return $this->jsonResponse(true, $results);
    }

    /**
     * @Rest\Post("/set/station/channel/sid")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function setStationChannel(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $station = $this->entityManager->find(Station::class, $data['stationId']);
            $station->setChannelSid($data['channelSid']);
            $this->entityManager->flush();
            return $this->jsonResponse(true, $station, 200, 'station', 'Station channelSid updated');
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create/notification/channel/user")
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function createNotificationChannelPerUser(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $company = $this->botService->getCompanyEntity($data);
            $users = $company->getUsers();
            $result = [];
            foreach ($users as $user) {
                if ($user->getIsArchive() === false && $user->getChatIdentifier() !== null && $user->getNotificationsChannelSid() === null) {
                    $channelData['companyId'] = $data['companyId'];
                    $channelData['options']['friendlyName'] = 'workplace_bot';
                    $channelData['options']['attributes']['kind'] = 'Bot';
                    $channelData['members'][0] = ['userId' => $user->getId()];
                    $channelResult = $this->botService->createChannelWithUsers($channelData)->getFormatData();
                    $result[] = $channelResult;
                    $user->setNotificationsChannelSid($channelResult['channel']['channelSid']);
                    $this->entityManager->flush();
//                    $channelData = $this->botService->dataForTwilioAuthentication($company);
//                    $channelData['channelSid'] = $user->getNotificationsChannelSid();
//                    $result[] = $this->twilioAccountService->deleteChannelResource($channelData);
                }
            }
            return $this->jsonResponse(true, $result, 200, 'station', 'Station channelSid updated');
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create_channel_with_manager_all")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createChannelWithManagerAll(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result['dataReceived'] = $data;
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            $data['name'] = 'Manager';


            $drivers = $this->entityManager->getRepository(Driver::class)
                ->findBy(['company' => $data['companyId'], 'isArchive' => false], ['id' => 'ASC']);
            $managers = $this->entityManager->getRepository(DriverSkill::class)->getDriversBySkillName($data);
            if (sizeof($managers) < 0) {
                $result['drivers'] = [];
                $result['message'] = 'No drivers found';
                return $this->jsonResponse(true, $result);
            }
            $result = [];
            $managersIds = array_map(function (Driver $driver) {
                return $driver->getUser()->getId();
            }, $managers);

            /** @var  Driver $driver */
            foreach ($drivers as $driver) {
                foreach ($managers as $manager) {
                    if ($driver->getUser() instanceof User && $manager->getUser() instanceof User) {
                        if (in_array($driver->getUser()->getId(), $managersIds)) {
                            if ($driver->getUser()->getId() < $manager->getUser()->getId()) {
                                $duoData['companyId'] = $data['companyId'];
                                $duoData['userId1'] = $driver->getUser()->getId();
                                $duoData['userId2'] = $manager->getUser()->getId();
                                $result[] = $this->dataForDuoChannel($duoData);
                            } else {
                                continue;
                            }
                        } else if ($driver->getUser()->getId() !== $manager->getUser()->getId()) {
                            $duoData['companyId'] = $data['companyId'];
                            $duoData['userId1'] = $driver->getUser()->getId();
                            $duoData['userId2'] = $manager->getUser()->getId();
                            $result[] = $this->dataForDuoChannel($duoData);
                        }
                    }
                }
            }
            $channelResult = [];
            foreach ($result as $duoDataSubmit) {
                try {
                    $channelResult[] = $this->botService->createChannelWithUsers($duoDataSubmit);
                } catch (Exception $exception) {
                    $channelResult[] = ['type' => 'exception', 'message' => $exception->getMessage(), 'trace' => $exception->getTrace()];
                } catch (Error $exception) {
                    $channelResult[] = ['type' => 'error', 'message' => $exception->getMessage(), 'trace' => $exception->getTrace()];
                }
            }
            return $this->jsonResponse(true, $channelResult);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create_channel_with_managers")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function createChannelWithManagers(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result['dataReceived'] = $data;
            if (empty($data['userId'])) {
                throw new Exception('userId field missing.');
            }

            /** @var User $user */
            $user = $this->entityManager->find(User::class, $data['userId']);
            if (!($user instanceof User)) {
                $result['dataReceived'] = $data;
                $result['message'] = 'The user received does not exists.';
                return $this->jsonResponse(false, $result, 400);
            }

            if (!($user->getDriver() instanceof Driver)) {
                $result['dataReceived'] = $data;
                $result['message'] = 'The user received does not have a driver.';
                return $this->jsonResponse(false, $result, 400);
            }

            /** @var Collection|User[] $managers */
            $managers = $user->getDriver()->getAssignedManagers();

            if ($managers->count() < 1) {
                $result['dataReceived'] = $data;
                $result['message'] = 'No managers assigned found.';
                return $this->jsonResponse(false, $result, 400);
            }

            $dataForDuoChannels = [];
            foreach ($managers as $manager) {
                $dataForDuoChannels[] = $this->botService->dataForDuoChannel($user, $manager);
            }

            $channelResult = [];
            foreach ($dataForDuoChannels as $duoDataSubmit) {
                try {
                    $channelResult[] = $this->botService->createChannelWithUsers($duoDataSubmit);
                } catch (Exception $exception) {
                    $channelResult[] = ['type' => 'exception', 'message' => $exception->getMessage(), 'trace' => $exception->getTrace()];
                } catch (Error $error) {
                    $channelResult[] = ['type' => 'error', 'message' => $error->getMessage(), 'trace' => $error->getTrace()];
                }
            }

            return $this->jsonResponse(true, $channelResult);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_workplace_bot_channel")
     * @param Request $request
     * @param UserService $userService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function createWorkplaceBotChannel(Request $request, UserService $userService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            /** @var User $user */
            $user = $this->entityManager->find(User::class, $data['user']['id']);
            if (!($user instanceof User)) {
                throw new Exception('User entity not found.');
            }
            $result = $userService->createWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/add_user_to_workplace_bot_channel")
     * @param Request $request
     * @param UserService $userService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function addUserToWorkplaceBotChannel(Request $request, UserService $userService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            /** @var User $user */
            $user = $this->entityManager->find(User::class, $data['user']['id']);
            if (!($user instanceof User)) {
                throw new Exception('User entity not found.');
            }
            $result = $userService->addUserToWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/set_up_workplace_bot_channel")
     * @param Request $request
     * @param UserService $userService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function setUpWorkplaceBotChannel(Request $request, UserService $userService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            $user = $userService->findUser('id', $data['user']['id']);
            if (!($user instanceof User)) {
                throw new Exception('User entity not found.');
            }
            $result = $userService->setUpWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
            return $result->jsonResponse();
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/set_up_multiple_workplace_bot_channel")
     * @param Request $request
     * @param UserService $userService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function setUpMultipleWorkplaceBotChannel(Request $request, UserService $userService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil)
    {
        try {
            set_time_limit(50000);
            $data = $request->request->all();
            $users = $this->entityManager->getRepository(User::class)->getTwilioUsersFilter(['company' => $data['companyId']]);
            $channels = [];
            foreach ($users as $user) {
                $result = $userService->setUpWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
                $channels[] = $result->getFormatData();
            }
            return $this->jsonResponse(true, $channels, 200, 'channels');
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/check_multiple_workplace_bot_channel")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkMultipleWorkplaceBotChannel(Request $request)
    {
        try {
            $data = $request->request->all();
            $users = $this->entityManager->getRepository(User::class)->getTwilioUsersFilter($data);
            return $this->jsonResponseV2(true, ['count' => count($users), 'users' => $users]);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/sync_channel_to_db")
     * @param Request $request
     * @return JsonResponse
     */
    public function syncChannelsToProductionDB(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = [];
            foreach ($data as $item) {
                $user = $this->entityManager->find(User::class, $item['u_id']);
                $user->setNotificationsChannelSid($item['u_notificationsChannelSid']);
                $this->entityManager->flush();
                $result[] = $user;
            }
            return $this->jsonResponseV2(true, ['count' => count($result), 'users' => $result]);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_channel_with_manager")
     * @param Request $request
     * @param UserService $userService
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function createChannelWithManager(Request $request, UserService $userService, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result['dataReceived'] = $data;
            $user = $userService->findUser('id', $data['user']['id']);
            $result['channelWithManager'] = $userService->createChannelWithManager($user, $twilioAccountService, $twilioUtil);
            return $this->jsonResponse(true, $result);
        } catch (Exception $exception) {
            return $this->result->jsonException($exception);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_twilio_user")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function createTwilioUser(Request $request, TwilioAccountService $twilioAccountService, UserService $userService): JsonResponse
    {
        try {
            $data = $request->request->all();
            $user = $userService->findUser('id', $data['user']['id']);
            $result = $userService->createUserForChat($user, $twilioAccountService);
            return $result->jsonResponse();
        } catch (Exception $e) {
            return $this->result->jsonException($e);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/sync_manager_channels_by_company")
     * @param Request $request
     * @param DriverService $driverService
     * @param UserService $userService
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return JsonResponse
     */
    public function syncManagerChannelsByCompany(Request $request, DriverService $driverService, UserService $userService, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        set_time_limit(180000000);
        try {
            $data = $request->request->all();
            /** @var User[] $users */
            $users = $this->entityManager->getRepository(User::class)->getTwilioUsersFilter($data);
            if (count($users) < 1) {
                throw new Exception('Users not found.');
            }
            $result = [];
            $countUsers = count($users);
            foreach ($users as $user) {
                $managerChannel = $userService->createChannelWithManager($user, $twilioAccountService, $twilioUtil);
                $driver = $user->getDriver();
                $managerChannelFormatData = $managerChannel->getFormatData();
                $mainManagerChannelSid = null;
                if (isset($managerChannelFormatData['error']) && isset($managerChannelFormatData['error']['sid']) && is_string($managerChannelFormatData['error']['sid'])) {
                    $mainManagerChannelSid = $managerChannelFormatData['error']['sid'];
                    $user->setMainManagerChannelSid($mainManagerChannelSid);
                    $this->entityManager->flush();
                }
                $result[] = [
                    'driverId' => $driver->getId(),
                    'userId' => $user->getId(),
                    'managers' => $driverService->getManagers($driver),
                    'managerChannel' => $managerChannelFormatData,
                    'mainManagerChannelSid' => $mainManagerChannelSid
                ];
            }
            return $this->jsonResponseV2(true, [
                'count' => $countUsers,
                'countResult' => count($result),
                'result' => $result,
            ], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Error $error) {
            $result = new Result(false, $error->getTrace(), 'error', $error->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/create_manager_channels_by_company")
     * @param Request $request
     * @param DriverService $driverService
     * @param UserService $userService
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return JsonResponse
     */
    public function createManagerChannelsByCompany(Request $request, DriverService $driverService, UserService $userService, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        set_time_limit(180000000);
        try {
            $data = $request->request->all();
            $companyId = $data['companyId'];
            $drivers = $this->entityManager->getRepository(Driver::class)->getTwilioUsersV1(['company' => $companyId]);
            if (count($drivers) < 1) {
                throw new Exception('Drivers not found.');
            }
            $result = [];
            $countDrivers = count($drivers);
            foreach ($drivers as $driver) {
                $user = $driver->getUser();
                if ($user instanceof User) {
                    $managerChannel = $userService->createChannelWithManager($user, $twilioAccountService, $twilioUtil);
                    $result[] = [
                        'driverId' => $driver->getId(),
                        'userId' => $user->getId(),
                        'managers' => $driverService->getManagers($driver),
                        'managerChannel' => $managerChannel->getMessage()
                    ];
                }
            }
            return $this->json([
                'countDrivers' => $countDrivers,
                'countResult' => count($result),
                'result' => $result,
            ], Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Error $error) {
            $result = new Result(false, $error->getTrace(), 'error', $error->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/create_manager_channel")
     * @param Request $request
     * @param DriverService $driverService
     * @param UserService $userService
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return JsonResponse
     */
    public function createManagerChannel(Request $request, DriverService $driverService, UserService $userService, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        try {
            $data = $request->request->all();
            /** @var Driver $driver */
            $driver = $this->entityManager->getRepository(Driver::class)->find($data['driverId']);
            $managerChannel = $userService->createChannelWithManager($driver->getUser(), $twilioAccountService, $twilioUtil);
            $result = [
                'driverId' => $driver->getId(),
                'managers' => $driverService->getManagers($driver),
                'managerChannel' => $managerChannel->getFormatData()
            ];
            return $this->json($result, Response::HTTP_OK);
        } catch (Exception $exception) {
            $result = new Result(false, $exception->getTrace(), 'exception', $exception->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Error $error) {
            $result = new Result(false, $error->getTrace(), 'error', $error->getMessage());
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * @Rest\Post("/add_user_to_station_channel")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param DriverService $driverService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function addUserToStationChannel(Request $request, TwilioAccountService $twilioAccountService, DriverService $driverService, UserService $userService)
    {
        try {
            $data = $request->request->all();
            $user = $userService->findUser('id', $data['user']['id']);
            $result = $driverService->addDriverToStationChannel($user->getDriver(), $twilioAccountService);
            return $result->jsonResponse();
        } catch (Exception $e) {
            return $this->result->jsonException($e);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_station_channel")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param StationService $stationService
     * @param UserService $userService
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function createStationChannel(Request $request, TwilioAccountService $twilioAccountService, StationService $stationService, UserService $userService, TwilioUtil $twilioUtil)
    {
        try {
            $data = $request->request->all();
            $user = $userService->findUser('id', $data['user']['id']);
            $station = $user->getDriver()->getStations()->first();
            $result = $stationService->createStationChannel($station, $user, $twilioAccountService, $twilioUtil);
            return $result->jsonResponse();
        } catch (Exception $e) {
            return $this->result->jsonException($e);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_multiple_twilio_users")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function createMultipleTwilioUser(Request $request, TwilioAccountService $twilioAccountService, UserService $userService): JsonResponse
    {
        try {
            $data = $request->request->all();
            $results = [];
            foreach ($data['emails'] as $email) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
                if ($user instanceof User) {
                    try {
                        $result = $userService->createUserForChat($user, $twilioAccountService);
                        $results[] = $result->getFormatData();
                    } catch (Exception $e) {
                        $results[] = [$e->getMessage(), $email];
                    } catch (Error $e) {
                        $results[] = [$e->getMessage(), $email];
                    }
                }
            }
            return $this->jsonResponse(true, $results);
        } catch (Exception $e) {
            return $this->result->jsonException($e);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/add_workplacebot_channel_multiple_twilio_users")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param UserService $userService
     * @return JsonResponse
     */
    public function addWorkplaceBotChannelMultipleTwilioUser(Request $request, TwilioAccountService $twilioAccountService, UserService $userService, TwilioUtil $twilioUtil): JsonResponse
    {
        try {
            $data = $request->request->all();
            $results = [];
            foreach ($data['emails'] as $email) {
                $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $email]);
                if ($user instanceof User) {
                    try {
                        $result = $userService->setUpWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
                        $results[] = $result->getFormatData();
                    } catch (Exception $e) {
                        $results[] = [$e->getMessage(), $email];
                    } catch (Error $e) {
                        $results[] = [$e->getMessage(), $email];
                    }
                }
            }
            return $this->jsonResponse(true, $results);
        } catch (Exception $e) {
            return $this->result->jsonException($e);
        } catch (Error $error) {
            return $this->result->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/create_group_channel")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @param TwilioAccountService $twilioAccountService
     * @return JsonResponse
     */
    public function createGroupChannelWithUsers(Request $request, TwilioUtil $twilioUtil, TwilioAccountService $twilioAccountService)
    {
        try {
            $data = $request->request->all();
            $company = $this->entityManager->getRepository(Company::class)->find($data['company']['id']);
            if (!empty($data['image'])) {
                $result = $twilioUtil->createGroupChannelWithUsers($company, $data['users'], $data['channelName'], $twilioAccountService, $data['image']);
            } else {
                $result = $twilioUtil->createGroupChannelWithUsers($company, $data['users'], $data['channelName'], $twilioAccountService);
            }
            if ($result->isSuccess()) {
                return $result->jsonResponse();
            }
            return $result->jsonError();
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $error) {
            return $this->jsonException($error);
        }
    }

    /**
     * @Rest\Post("/sync_user")
     * @param Request $request
     * @param TwilioUtil $twilioUtil
     * @return JsonResponse
     */
    public function syncUserResource(Request $request, TwilioUtil $twilioUtil)
    {
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['company']['id']);
            $user = $this->entityManager->find(User::class, $data['user']['id']);
            $dataTwilio = $twilioUtil->getTwilioAuthenticationData($company);
            $data['userSid'] = $user->getUserSid();
            $data = array_merge($data, $dataTwilio);
            $data['friendlyName'] = $user->getFriendlyName();
            $data['attributes']['image'] = $user->getProfileImage();
            $data['attributes']['role'] = $user->getRoleForChat();
            $data['attributes']['id'] = $user->getId();
            $result = $this->twilioAccountService->updateUserResource($data);
            if ($result->isSuccess()) {
                return $this->json($result->getFormatData(), Response::HTTP_ACCEPTED);
            }
            return $this->json($result->getFormatData(), Response::HTTP_BAD_REQUEST);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/create_twilio_users_missing")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @param UserService $userService
     * @return JsonResponse
     */
    public function createTwilioUsers(Request $request, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil, UserService $userService)
    {
        try {
            $data = $data = $request->request->all();
            $users = $this->entityManager->getRepository(User::class)->getNotTwilioUsers(['company' => $data['company']['id']]);
            $results = [];
            foreach ($users as $user) {
                if ($user instanceof User) {
                    $result = $userService->createTwilioUser($user, $twilioAccountService, $twilioUtil, $userService);
                    $results[] = $result->getFormatData();
                }
            }
            return $this->json($results, Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/update_user_with_notification_channel_sid")
     * @param Request $request
     * @param TwilioAccountService $twilioAccountService
     * @param TwilioUtil $twilioUtil
     * @param UserService $userService
     * @return JsonResponse
     */
    public function updateUserWithNotificationChannelSid(Request $request, TwilioAccountService $twilioAccountService, TwilioUtil $twilioUtil, UserService $userService)
    {
        set_time_limit(10000000000);
        try {
            $data = $data = $request->request->all();
            $company = $this->entityManager->find(Company::class, $data['company']);
            /** @var User[] $users */
            $users = $this->entityManager->getRepository(User::class)->getUserWithNullNotificationSid(['company' => $data['company']]);
            $count = count($users);
            $results = [];
            foreach ($users as $user) {
                $data = $twilioUtil->getTwilioAuthenticationData($company);
                $data['uniqueName'] = 'workplace_bot|' . $user->getChatIdentifier();
                $channelData = $twilioAccountService->fetchChannelResource($data);
                if(isset($channelData->getData()['sid'])){
                    $user->setNotificationsChannelSid($channelData->getData()['sid']);
                    $this->entityManager->flush();
                    $channelData = $channelData->getData();
                    $channelData['user'] = $userService->fieldsUser($user);
                    $results[] = $channelData;
                }
                else{
                    $channelData = $userService->createWorkplaceBotChannel($user, $twilioAccountService, $twilioUtil);
                    $channelData = $channelData->getData();
                    $channelData['user'] = $userService->fieldsUser($user);
                    $results[] = $channelData;
                }
            }
            return $this->json(['count' => $count, 'channel' => $results], Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/test")
     * @param UserService $userService
     * @return JsonResponse
     */
    public function test(UserService $userService)
    {
        try {
            $user = $this->entityManager->find(User::class, 285);
            $user->setEmail('augustine.sanders@gmail.com');
            $this->entityManager->flush();
            return $this->json($userService->fieldsUser($user), Response::HTTP_OK);
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }
}
