<?php

namespace App\Controller;

use Doctrine\ORM\Query;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * Skill Controller
 * @Route("/api",name="api_")
 */
class SkillRateController extends BaseController
{
    /**
     * @Rest\Post("/skill/rate/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createSkillAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/skill/rate/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateSkillAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/skill/rate/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getSkillAction(Request $request): JsonResponse
    {
        //return $this->getAllValues();
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $station = $qb->select('sr.id,sr.defaultRate,s.name,s.id AS skillId')
            ->from('App:SkillRate','sr')
            ->leftJoin('sr.skill', 's')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
        return new JsonResponse($station);
    }
}
