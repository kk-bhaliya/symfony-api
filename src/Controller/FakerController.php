<?php


namespace App\Controller;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\KickoffLog;
use App\Entity\Role;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Faker Controller
 *
 * @Route("/api/faker",name="api_")
 */
class FakerController extends BaseController
{
    private const DRIVER = 'DRIVER';
    private const LEAD_DRIVER = 'LEAD DRIVER';
    private const MANAGER = 'MANAGER';
    private const SUPERVISOR = 'SUPERVISOR';
    private const DISPATCH = 'DISPATCH';
    private const ROLES = [self::DRIVER, self::LEAD_DRIVER, self::MANAGER, self::SUPERVISOR, self::DISPATCH];
    /**
     * @var Generator
     */
    private $faker;

    public function serialize( $object, $excludes = [], $includes = [] )
    {
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);

        if(!empty($includes)) {
            return json_decode($serializer->serialize($object, 'json', [AbstractNormalizer::ATTRIBUTES => $includes]), true);
        } elseif( !empty($excludes)) {
            return json_decode($serializer->serialize($object, 'json', [AbstractNormalizer::IGNORED_ATTRIBUTES => $excludes]), true);
        } else {
            return json_decode($serializer->serialize($object, 'json'), true);
        }
    }

    /**
     * @Rest\POST("/create_driver_route")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createDriverRoute(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $driver = $entityManager->find(Driver::class, $data['driverId']);
//        $driverRoute = new DriverRoute();
//        $driverRoute->setDriver($driver);
//        $entityManager->persist($driverRoute);
//        $entityManager->flush();
        $normalizer = new ObjectNormalizer();
        $encoder = new JsonEncoder();
        $serializer = new Serializer([$normalizer], [$encoder]);


        $driverRoute = $entityManager->find(DriverRoute::class, 1);
//        $kickOff = new KickoffLog();
//        $kickOff->setDriverRoute($driverRoute);
//        $entityManager->persist($kickOff);
//        $entityManager->flush();
//        $d = json_decode($serializer->serialize($driverRoute, 'json', [AbstractNormalizer::ATTRIBUTES => ['id']]), true);
//        dump($d);
//        die;

        //$this->serialize($driverRoute, [], ['id', 'dateCreated'])

        $kickOff = $entityManager->find(KickoffLog::class, 1);
        // 'id', 'driverRoutes'=>['id']]
//        $d = json_decode($serializer->serialize($driver, 'json', [AbstractNormalizer::ATTRIBUTES => $data['includes']]), true);
//        dump($d);
//        die;
        return $this->json([], Response::HTTP_OK);
    }

    // find route of today
    // assume it is already created while creating route
    // get route & kickoff
    //


    /**
     * @Rest\POST("/create_users")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function createUsers(Request $request, EntityManagerInterface $entityManager)
    {
        $data = $request->request->all();
        $number = $data['numberOfUsers'];
        $companyId = $data['companyId'];
        $this->faker = Factory::create('en_US');
        $company = $entityManager->find(Company::class, $companyId);
        $role = new Role();
        $role->setName(self::ROLES[0]);
        $role->setCompany($company);
        $entityManager->persist($role);
        $entityManager->flush();
        for ($i=0; $i < $number; $i++){
            $user = new User();
            $user->setEmail($this->faker->unique()->email);
            $user->setFirstname($this->faker->firstName);
            $user->setLastname($this->faker->lastName);
            $user->setPassword("Private1!");
            $user->addUserRole($role);
            $user->setCompany($company);
            $entityManager->persist($user);
            $entityManager->flush();



            $driver = new Driver();
            $driver->setCompany($company);
            $driver->setBirthday($this->faker->dateTime);
            $driver->setCompanyPhone($this->faker->phoneNumber);

        }

        return $this->json($data, Response::HTTP_OK);
    }

    /**
     * @Rest\POST("/create_driver")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     * @throws Exception
     */
    public function createDriver(Request $request, EntityManagerInterface $entityManager){
//        "jobTitle": "Driver",
//                    "personalPhone": "555-555-5555",
//                    "companyPhone": "555-555-5555",
//                    "driversLicenseNumber": "123456789",
//                    "driversLicenseNumberExpiration": "2019-10-10 17:00:00",
//                    "hireDate": "2019-10-10 17:00:00",
//                    "birthday": "2019-10-01 17:00:00",
//                    "gender": "Male",
//                    "note": "The quick, brown fox jumps over a lazy dog.",
//                    "shirtSize": "3XL",
//                    "shoeSize": "14 wide",
//                    "pantSize": "20X36",
//                    "discretionaryBonus": "$15",
//                    "ssn": "8888",
//                    "veteranStatus": "12",
//                    "secondaryEmail": "none@gmail.com",
//                    "fullAddress": "123 test road.",
//                    "transporterID": "99134801380413",
//                    "licenseState": "none",
//                    "expectedFirstDayOnTheRoad": "none",
//                    "needsTraining": "1",
//                    "maritalStatus": "yes",
//                    "drivingPreference": "none"
        $data = $request->request->all();
        $user = $entityManager->find(User::class, $data['userId']);
        $driver = new Driver();
        $driver->setUser($user);
        $entityManager->persist($driver);
        $user->setDriver($driver);
        $entityManager->flush();


        return $this->json([], Response::HTTP_OK);
    }

}