<?php


namespace App\Controller;

use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Bot Controller
 * @Route("/api",name="api_")
 */
class BotController extends BaseController
{
    /**
     * @Rest\Post("/send_message", name="sendMessage")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendMessage(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->sendTwilioMessage($data);
            return $this->json($result);
        } catch (Exception $e) {
            return $this->jsonExceptionV2($e);
        } catch (Error $e) {
            return $this->jsonExceptionV2($e);
        }
    }

    /**
     * @Rest\Post("/send_message_station", name="sendMessageStation")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendMessageStation(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->sendMessagesToStationManagers($data);
            return $this->json($result);
        } catch (Exception $e) {
            return $this->jsonExceptionV2($e);
        } catch (Error $e) {
            return $this->jsonExceptionV2($e);
        }
    }

    /**
     * @Rest\Post("/send_sms", name="sendSMS")
     * @param Request $request
     * @return JsonResponse
     */
    public function sendSMS(Request $request)
    {
        try {
            $data = $request->request->all();
            $result = $this->botService->sendPlivoSMSByLambda($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $exception) {
            return $this->jsonExceptionV2($exception);
        }
    }

    /**
     * @Rest\Post("/post_event_hook")
     * @param Request $request
     * @return JsonResponse
     */
    public function postEventHook(Request $request)
    {
        try {
            $data = $request->request->all();
            if (isset($data['EventType']) && $data['EventType'] === "onMessageSent") {
                return $this->json(["onMessageSent" => $data]);
            }
            return $this->json($data);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $exception) {
            return $this->jsonExceptionV2($exception);
        }
    }
}