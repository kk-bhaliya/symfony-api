<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * Kickoff Log Controller
 * @Route("/api",name="api_")
 */
class KickoffLogController extends BaseController
{
    /**
     * @Rest\Post("/kickoff_log/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createKickoffLogAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/kickoff_log/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateKickoffLogAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/kickoff_log/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getKickoffLogAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }
}
