<?php

namespace App\Controller;

use App\Entity\Device;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\KickoffLog;
use App\Entity\Location;
use App\Entity\ReturnToStation;
use App\Entity\Station;
use App\Entity\User;
use App\Entity\Vehicle;
use App\Services\UserService;
use DateTime;
use Error;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * Mobile Controller
 * @Route("/api",name="api_")
 */
class MobileController extends BaseController
{
    /**
     * @Rest\Post("/mobile_version")
     * @param Request $request
     * @return JsonResponse
     */
    public function mobileVersion(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $platform = $data['platform'];
            $version = $data['version'];
            $result['data'] = $data;
            if ($platform === 'android') {
                if ($version !== "1.0.0.96") {
                    $result['android']['update'] = true;
                }
            } elseif ($platform === 'ios') {
                if ($version !== "1.0.3") {
                    $result['ios']['update'] = true;
                }
            }
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/driver_route/add_locations")
     * @param Request $request
     * @return JsonResponse
     */
    public function addGeofenceLocations(Request $request): JsonResponse
    {
        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR)[0];

            $driverRoute = $this->entityManager->find(DriverRoute::class, $data['driverRouteId']);
            $device = $this->entityManager->find(Device::class, $data['deviceId']);
            $user = $this->entityManager->find(User::class, $data['userId']);

            if (!($driverRoute instanceof DriverRoute)) {
                throw new Exception('DriverRoute not found.');
            }
            if (!($device instanceof Device)) {
                throw new Exception('Device not found.');
            }
            if (!($user instanceof User)) {
                throw new Exception('User not found.');
            }

            /** @var Station $station */
            $station = $driverRoute->getStation();

            if (!($station instanceof Station)) {
                throw new Exception('Station not found.');
            }

            $stationLatitude = floatval($station->getStationLatitude());
            $stationLongitude = floatval($station->getStationLongitude());
            $parkingLotLatitude = floatval($station->getParkingLotLatitude());
            $parkingLotLongitude = floatval($station->getParkingLotLongitude());
            $stationRadius = floatval($station->getStationGeofence()) / 1000.0;
            $parkingLotRadius = floatval($station->getParkingLotGeofence()) / 1000.0;

            $location = new Location();

            // check lunch break

            $location->setLongitude($data['longitude']);
            $location->setLatitude($data['latitude']);
            $location->setName('LOCATION');
            $location->setCreatedAt(new DateTime());
            $this->entityManager->persist($location);
            $this->entityManager->flush();
            $driverRoute->addLocation($location);
            $location->setDriverRoute($driverRoute);
            $this->entityManager->flush();

            $threshold = 8.25;
            $eventStation = null;
            $currentStationRadius = $this->findRadius($data['latitude'], $data['longitude'], $stationLatitude, $stationLongitude);
            if ($currentStationRadius <= $threshold * $stationRadius && $currentStationRadius > $stationRadius) {
                $eventStation = $this->createGeofenceEvent($location, "OUTSIDE_STATION", "Outside Station", 'Outside Station !!!');
            } else if ($currentStationRadius <= $stationRadius) {
                $eventStation = $this->createGeofenceEvent($location, "INSIDE_STATION", "Inside Station", 'Inside Station !!!');
            }

            $currentParkingLotRadius = $this->findRadius($data['latitude'], $data['longitude'], $parkingLotLatitude, $parkingLotLongitude);

            $eventParkingLot = null;
            if ($currentParkingLotRadius <= $threshold * $parkingLotRadius && $currentParkingLotRadius > $parkingLotRadius) {
                $eventParkingLot = $this->createGeofenceEvent($location, "OUTSIDE_PARKING_LOT", "Outside Parking Lot", "Outside Parking Lot !!!");
            } else if ($currentParkingLotRadius <= $parkingLotRadius) {
                $eventParkingLot = $this->createGeofenceEvent($location, "INSIDE_PARKING_LOT", "Inside Parking Lot", "Inside Parking Lot !!!");
            }


            if ($eventStation instanceof Event) {
                $driverRoute->addEvent($eventStation);
                $eventStation->setUser($user);
                $eventStation->setDevice($device);
                $this->entityManager->flush();
            }

            if ($eventParkingLot instanceof Event) {
                $driverRoute->addEvent($eventParkingLot);
                $eventParkingLot->setUser($user);
                $eventParkingLot->setDevice($device);
                $this->entityManager->flush();
            }

            // check punch in
            // check punch out


            if (isset($data['debug']) && $data['debug'] === true) {
                return $this->jsonResponse(true,
                    [
                        'location' => $location,
                        'eventStation' => $eventStation,
                        'eventParkingLot' => $eventParkingLot,
                        'currentStationRadius' => $currentStationRadius,
                        'stationRadius' => $stationRadius,
                        'threshold-stationRadius' => $threshold * $stationRadius,
                        'currentParkingLotRadius' => $currentParkingLotRadius,
                        'parkingLotRadius' => $parkingLotRadius,
                        'threshold-parkingLotRadius' => $threshold * $parkingLotRadius,
                        'threshold' => $threshold
                    ],
                    200, 'location', 'Location added.');
            }

            return $this->jsonResponse(true,
                [
                    'location' => $location->getId(),
                    'eventStation' => $eventStation,
                    'eventParkingLot' => $eventParkingLot,
                ],
                200, 'location', 'Location added.');

        } catch (Exception $e) {
            return $this->jsonException($e);
        }
    }

    /**
     * @param Location $location
     * @param string $name
     * @param string $title
     * @param string $message
     * @return Event
     */
    private function createGeofenceEvent(Location $location, string $name, string $title, string $message)
    {
        $event = new Event();
        $event->setLocation($location);
        $event->setName($name);
        $event->setEventName($title);
        $event->setMessage($message);
        $event->setCreatedAt(new DateTime());
        $this->entityManager->persist($event);
        $this->entityManager->flush();
        return $event;
    }

    /**
     * @Rest\Post("/test_location")
     * @param Request $request
     * @return JsonResponse
     */
    public function testLocation(Request $request): JsonResponse
    {
        $data = $request->request->all();
        $location = $this->entityManager->find(Location::class, $data['locationId']);
        $station = $this->entityManager->find(Station::class, $data['stationId']);
        $stationRadius = floatval($station->getStationGeofence()) / 1000.0;
        $parkingLotRadius = floatval($station->getParkingLotGeofence()) / 1000.0;
        $threshold = $data['threshold'];
        $latitude = floatval($location->getLatitude());
        $longitude = floatval($location->getLongitude());

        $result['threshold'] = $threshold;

        $currentParkingLotRadius = $this->executeParkingLotRadius($latitude, $longitude, $station);
        $result['parkingLotRadius'] = $parkingLotRadius;
        $result['currentParkingLotRadius'] = $currentParkingLotRadius;
        if ($currentParkingLotRadius <= $threshold * $parkingLotRadius && $currentParkingLotRadius > $parkingLotRadius) {
            $eventParkingLotName = 'Outside Parking Lot !!!';
            $result['eventParkingLot'] = [
                'eventParkingLotName' => $eventParkingLotName,
                'parkingLotRadius' => $parkingLotRadius,
                'currentParkingLotRadius' => $currentParkingLotRadius,
                '$threshold * $parkingLotRadius' => $threshold * $parkingLotRadius,
            ];
        } else if ($currentParkingLotRadius <= $parkingLotRadius) {
            $eventParkingLotName = 'Inside Parking Lot !!!';
            $result['eventParkingLot'] = [
                'eventParkingLotName' => $eventParkingLotName,
                'parkingLotRadius' => $parkingLotRadius,
                'currentParkingLotRadius' => $currentParkingLotRadius,
                '$threshold * $parkingLotRadius' => $threshold * $parkingLotRadius,
            ];
        }

        $currentStationRadius = $this->executeStationRadius($latitude, $longitude, $station);
        $result['stationRadius'] = $stationRadius;
        $result['currentStationRadius'] = $currentStationRadius;
        $threshold = $data['threshold'];
        if ($currentStationRadius <= $threshold * $stationRadius && $currentStationRadius > $stationRadius) {
            $eventStationName = 'Outside Station !!!';
            $result['eventStation'] = [
                'eventStationName' => $eventStationName,
                'stationRadius' => $stationRadius,
                'currentStationRadius' => $currentStationRadius,
                '$threshold * $stationRadius' => $threshold * $stationRadius,
            ];
        } else if ($currentStationRadius <= $stationRadius) {
            $eventStationName = 'Inside Station !!!';
            $result['eventStation'] = [
                'eventStationName' => $eventStationName,
                'stationRadius' => $stationRadius,
                'currentStationRadius' => $currentStationRadius,
                '$threshold * $stationRadius' => $threshold * $stationRadius,
            ];
        }
        $result['location'] = $location;

        return $this->jsonResponseV2(true, $result);
    }

    private function executeStationRadius(float $latitude, float $longitude, Station $station)
    {
        $stationLatitude = floatval($station->getStationLatitude());
        $stationLongitude = floatval($station->getStationLongitude());
        return $this->findRadius($latitude, $longitude, $stationLatitude, $stationLongitude);
    }

    private function executeParkingLotRadius(float $latitude, float $longitude, Station $station)
    {
        $parkingLotLatitude = floatval($station->getParkingLotLatitude());
        $parkingLotLongitude = floatval($station->getParkingLotLongitude());
        return $this->findRadius($latitude, $longitude, $parkingLotLatitude, $parkingLotLongitude);
    }

    /**
     * @Rest\Post("/driver_information")
     * @param Request $request
     * @return JsonResponse
     */
    public function driverInformation(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(User::class)->userInformation($data);
            return $this->json($result);
        } catch (Exception $e) {
            return $this->jsonExceptionV2($e);
        } catch (Error $e) {
            return $this->jsonExceptionV2($e);
        }
    }

    /**
     * @Rest\Post("/fetch_driver_route_events")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchDriverRouteEvents(Request $request): JsonResponse
    {
        $data = $request->request->all();
        /** @var DriverRoute $driverRoute */
        $driverRoute = $this->entityManager->find(DriverRoute::class, $data['driverRouteId']);
        $events = $driverRoute->getEvents()->filter(function (Event $item) {
            return $item->getIsActive() === true;
        });
        $result['count'] = $events->count();
        $result['events'] = $events;
        return $this->jsonResponseV2(true, $result);
    }

    /**
     * @Rest\Post("/allow_load_out")
     * @param Request $request
     * @return JsonResponse
     */
    public function allowLoadOut(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(Device::class)->allowLoadOutProcess($data);
            return $this->jsonResponseV2(true, $result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/check_driver_route_existence")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkDriverRouteExistence(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->checkIfDriverRoutesExists($data);
            return $this->jsonResponseV2(true, $result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/create_back_up_driver_route")
     * @param Request $request
     * @return JsonResponse
     */
    public function createBackUpDriverRoute(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->createUnscheduledDriverRoute($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/create_unscheduled_driver_route")
     * @param Request $request
     * @return JsonResponse
     */
    public function createUnscheduledDriverRoute(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->createUnscheduledDriverRoute($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/set_driver_route")
     * @param Request $request
     * @return JsonResponse
     */
    public function setDriverRoute(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->setDriverRoute($data);
//            if ($result['init']) {
            // Check after 20 min The Punch In once started Load Out
//                $this->botServiceWorker->later(20 * 60)->sendPlivoPunchPaycomMessageToWorker($result, $result['driverRoute']);
            // Check after 4.5 hours The Break once started Load Out
//                $this->botServiceWorker->later(4.5 * 60 * 60)->sendPlivoBreakReminderToWorker($result, $result['driverRoute']);
//            }
            return $this->json($result['response']);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/get_load_out", name="getLoadOut")
     * @param Request $request
     * @return JsonResponse
     */
    public function getLoadOut(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->getLoadOut($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/complete_load_out", name="completeLoadOut")
     * @param Request $request
     * @param UserService $userService
     * @return JsonResponse
     */
    public function completeLoadOut(Request $request, UserService $userService): JsonResponse
    {
        try {
            $data = $request->request->all();
            if (empty($data['kickoffLogId'])) {
                throw new Exception('kickoffLogId field missing.');
            }
            if (empty($data['companyId'])) {
                throw new Exception('companyId field missing.');
            }
            if (empty($data['platform'])) {
                throw new Exception('platform field missing.');
            }
            $dateTime = new DateTime('now');
            if (!empty($data['loggedTouchFlexAt'])) {
                $dateTime = new DateTime($data['loggedTouchFlexAt']);
            }
            $device = null;
            if (!empty($data['deviceId'])) {
                $device = $this->entityManager->find(Device::class, $data['deviceId']);
            }
            $kickoffLog = $this->entityManager->find(KickoffLog::class, $data['kickoffLogId']);
            $kickoffLog->setProcessCompleted(true);
            $kickoffLog->setLoggedTouchFlexAt($dateTime);
            $kickoffLog->setCompletedAt($dateTime);
            $this->entityManager->flush();
            /** @var DriverRoute $driverRoute */
            $driverRoute = $kickoffLog->getDriverRoute();
            $title = "Load Out completed.";
            $message = $userService->getCurrentUserFriendlyName() . " completed the Load Out process.";
            $event = $this->entityManager->getRepository(Event::class)->createEvent('LOAD_OUT', $title, $message, null, $driverRoute, null, $device);
            $driverRoute->addEvent($event);
            $this->entityManager->flush();

            // Check after 60 min The Punch In once Finished Load Out
//            $this->botServiceWorker->later(60 * 60)->sendPlivoPunchPaycomMessageToWorker($data, $driverRoute);

            return $this->json([
                'success' => true,
                'data' => $data,
                'event' => $event
            ]);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/get_rts")
     * @param Request $request
     * @return JsonResponse
     */
    public function getRTS(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->getRTS($data);
            return $this->jsonResponseV2(true, $result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/complete_rts", name="completeRTS")
     * @param Request $request
     * @return JsonResponse
     */
    public function completeRTS(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(ReturnToStation::class)->completeRTS($data);
            // Check after 25 min The Punch Out once completed RTS
//            $this->botServiceWorker->later(25 * 60)->sendPlivoPunchPaycomMessageToWorker($data, $driverRoute, false, 'PunchOut');
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/validate_vin_scanner")
     * @param Request $request
     * @return JsonResponse
     */
    public function validateVinScanner(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(KickoffLog::class)->setUpVinBasedOnScanner($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/validate_partial_vin")
     * @param Request $request
     * @return JsonResponse
     */
    public function validatePartialVinScanner(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(KickoffLog::class)->partialSearchVan($data);
            if (!empty($result['taskData'])) {
                $taskData = $result['taskData'];
//                $this->botServiceWorker->later()->createTaskToWorker($taskData);
                $result['task'] = $this->botService->createTask($taskData);
            }
            $this->botServiceWorker->later()->sendMessagesToStationManagersToWorker($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/validate_full_vin")
     * @param Request $request
     * @return JsonResponse
     */
    public function validateFullVinScanner(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(KickoffLog::class)->fullSearchVan($data);
            if ($result['vehicle']['isPending']) {
//                $result['notificationResult'] = $this->botService->sendMessagesToStationManagers($data);
                $this->botServiceWorker->later()->sendMessagesToStationManagersToWorker($data);
            }
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/current_mileage_and_gas")
     * @param Request $request
     * @return JsonResponse
     */
    public function currentMileageAndGas(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->currentMileageAndGas($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/load_out_set_mileage")
     * @param Request $request
     * @return JsonResponse
     */
    public function loadOutUpdateMileage(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(KickoffLog::class)->setMileage($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/rts_set_mileage")
     * @param Request $request
     * @return JsonResponse
     */
    public function rtsUpdateMileage(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(ReturnToStation::class)->updateMileage($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/search_device")
     * @param Request $request
     * @return JsonResponse
     */
    public function checkPhoneNumberAndImei(Request $request)
    {
        try {
            $number = $request->get('number');
            $imei = $request->get('imei');
            $deviceUid = $request->get('deviceUid');
            $deviceRepository = $this->entityManager->getRepository(Device::class);
            if (!empty($number)) {
                $device = $deviceRepository->searchOneDevice(['number' => $number]);
                return $this->json($device);
            }
            if (!empty($imei)) {
                $device = $deviceRepository->searchOneDevice(['imei' => $imei]);
                return $this->json($device);
            }
            if (!empty($deviceUid)) {
                $device = $deviceRepository->searchOneDevice(['deviceUid' => $deviceUid]);
                return $this->json($device);
            }
            throw new Exception('Missing input fields such as number, imei, and deviceUid.');
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/attach_device")
     * @param Request $request
     * @return JsonResponse
     */
    public function attachDevice(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(Device::class)->attachDeviceWithUser($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/add_device")
     * @param Request $request
     * @return JsonResponse
     */
    public function addDevice(Request $request)
    {
        try {
            $data = $request->request->all();
            $device = new Device();
            foreach ($data as $field => $value) {
                $setter = 'set' . $field;
                $device->$setter($value);
            }
            if ($data['isPersonalDevice'] === true) {
                $device->setIsPersonalDevice(true);
                $device->setIsOwned(false);
                $device->setIsRented(false);
                $device->setMdmClient(false);
            } else {
                $device->setIsPersonalDevice(false);
                $device->setIsPending(true);
                $device->setIsOwned(true);
                $device->setIsRented(false);
                $device->setMdmClient(true);
                $device->setAllowKickoff(true);
                $device->setAllowLoadOutProcess(true);
                $device->setIsEnabled(true);
            }

            $device->setDeviceCondition('Good');
            $this->entityManager->persist($device);
            $this->entityManager->flush();
            $device->setName('NEW - ID: ' . strval($device->getId()) . ' - ' . strval($device->getModel()));
            $this->entityManager->flush();
            return $this->json(
                [
                    'id' => $device->getId(),
                    'number' => $device->getNumber(),
                    'imei' => $device->getImei(),
                    'deviceUid' => $device->getDeviceUid(),
                    'isPersonalDevice' => $device->getIsPersonalDevice(),
                    'isOwned' => $device->getIsOwned(),
                    'isPending' => $device->getIsPending(),
                ]);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/update_kickoff_log")
     * @param Request $request
     * @return JsonResponse
     */
    public function updateLoadOut(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $kickoffLog = $this->entityManager->find(KickoffLog::class, $data['kickoffLogId']);
            unset($data['kickoffLogId']);
            $class = $this->entityManager->getClassMetadata(KickoffLog::class);
            foreach ($data as $field => $value) {
                $setter = 'set' . $field;
                if (!$class->hasAssociation($field)) {
                    $fieldArray = $class->getFieldMapping($field);
                    if ($fieldArray['type'] === 'datetime') {
                        if ($value) {
                            $kickoffLog->$setter(new DateTime($value));
                        } else {
                            $kickoffLog->$setter($value);
                        }
                    } else {
                        $kickoffLog->$setter($value);
                    }
                }
            }
            $this->entityManager->flush();

            if (!empty($data['debug'])) {
                return $this->json([
                    'success' => true,
                    'kickoffLog' => $kickoffLog
                ]);
            }
            return $this->json([
                'success' => true
            ]);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }

    /**
     * @Rest\Post("/open_shifts", name="openShifts")
     * @param Request $request
     * @return JsonResponse
     */
    public function openShifts(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $result = $this->entityManager->getRepository(DriverRoute::class)->getOpenShiftsForDriver($data);
            return $this->json($result);
        } catch (Exception $exception) {
            return $this->jsonExceptionV2($exception);
        } catch (Error $error) {
            return $this->jsonExceptionV2($error);
        }
    }
}