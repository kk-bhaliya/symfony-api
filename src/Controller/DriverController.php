<?php

namespace App\Controller;

use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\Collection;
use Error;
use Exception;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

/**
 * Driver Controller
 * @Route("/api",name="api_")
 */
class DriverController extends BaseController
{
    /**
     * @Rest\Post("/driver/create")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createDriverAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/driver/update")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateDriverAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/driver/get")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getDriverAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/driver/fetch/")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchDriverAction(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $driver = $this->entityManager->find(Driver::class, $data['id']);
            $custom = $this->serialize($data, $driver);
            if ($custom !== null) {
                $driver = $custom;
            }
            return $this->jsonResponse(true, $driver, 200, 'driver', 'Driver fetched');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (ExceptionInterface $e) {
            return $this->jsonException($e);
        }
    }

    const DRIVER_ROUTE_CHECK = "DRIVER_ROUTE_CHECK";
    const LOAD_OUT = "LOAD_OUT";
    const UNSCHEDULED_DRIVER_ROUTE = "UNSCHEDULED_DRIVER_ROUTE";
    const VEHICLE_ATTACHED = "VEHICLE_ATTACHED";
    const INSIDE_STATION = "INSIDE_STATION";
    const OUTSIDE_STATION = "OUTSIDE_STATION";
    const INSIDE_PARKING_LOT = "INSIDE_PARKING_LOT";
    const OUTSIDE_PARKING_LOT = "OUTSIDE_PARKING_LOT";
    const GAS = "GAS";
    const MILEAGE = "MILEAGE";
    const RTS = "RTS";
    const SEND_HOME = "SEND_HOME";
    const UNSEND_HOME = "UNSEND_HOME";
    const DEVICE_ADD = "DEVICE_ADD";
    const DEVICE_EDIT = "DEVICE_EDIT";
    const VEHICLE_ADD = "VEHICLE_ADD";
    const VEHICLE_EDIT = "VEHICLE_EDIT";
    const SECOND_SHIFT_ADD = "SECOND_SHIFT_ADD";
    const SECOND_SHIFT_START_TIME_EDIT = "SECOND_SHIFT_START_TIME_EDIT";
    const SECOND_SHIFT_END_TIME_EDIT = "SECOND_SHIFT_END_TIME_EDIT";
    const SECOND_SHIFT_INVOICE_TYPE_EDIT = "SECOND_SHIFT_INVOICE_TYPE_EDIT";
    const SECOND_SHIFT_NOTE_EDIT = "SECOND_SHIFT_NOTE_EDIT";
    const SECOND_SHIFT_DRIVER_NOTE_EDIT = "SECOND_SHIFT_DRIVER_NOTE_EDIT";
    const SECOND_SHIFT_PACKAGE_EDIT = "SECOND_SHIFT_PACKAGE_EDIT";
    const INCIDENT_CREATE = "INCIDENT_CREATE";
    const INCIDENT_DELETE = "INCIDENT_DELETE";
    const INCIDENT_PHASE_SAVE = "INCIDENT_PHASE_SAVE";
    const INCIDENT_PHASE_UPDATE = "INCIDENT_PHASE_UPDATE";
    const SHIFT_START_TIME_EDIT = "SHIFT_START_TIME_EDIT";
    const SHIFT_END_TIME_EDIT = "SHIFT_END_TIME_EDIT";
    const EDIT_DRIVER_SHIFT_NOTE = "EDIT_DRIVER_SHIFT_NOTE";
    const EDIT_SHIFT_NOTE = "EDIT_SHIFT_NOTE";
    const EDIT_SHIFT_NAME = "EDIT_SHIFT_NAME";
    const EDIT_SHIFT_INVOICE_TYPE = "EDIT_SHIFT_INVOICE_TYPE";
    const ROUTE_CODE_ADD = "ROUTE_CODE_ADD";
    const ROUTE_ASSIGNED = "ROUTE_ASSIGNED";
    const ROUTE_UNASSIGNED = "ROUTE_UNASSIGNED";
    const ADD_TRAIN = "ADD_TRAIN";
    const EDIT_TRAIN_START_TIME = "EDIT_TRAIN_START_TIME";
    const EDIT_TRAIN_END_TIME = "EDIT_TRAIN_END_TIME";
    const ADD_DUTY = "ADD_DUTY";
    const EDIT_DUTY_START_TIME = "EDIT_DUTY_START_TIME";
    const EDIT_DUTY_END_TIME = "EDIT_DUTY_END_TIME";
    const ROUTE_CODE_EDIT = "ROUTE_CODE_EDIT";
    const RESCUE_SHIFT_ADD = "RESCUE_SHIFT_ADD";
    const RESCUE_SHIFT_START_TIME_EDIT = "RESCUE_SHIFT_START_TIME_EDIT";
    const RESCUE_SHIFT_END_TIME_EDIT = "RESCUE_SHIFT_END_TIME_EDIT";
    const RESCUE_SHIFT_INVOICE_TYPE_EDIT = "RESCUE_SHIFT_INVOICE_TYPE_EDIT";
    const RESCUE_SHIFT_NOTE_EDIT = "RESCUE_SHIFT_NOTE_EDIT";
    const RESCUE_SHIFT_DRIVER_NOTE_EDIT = "RESCUE_SHIFT_DRIVER_NOTE_EDIT";
    const RESCUE_SHIFT_PACKAGE_EDIT = "RESCUE_SHIFT_PACKAGE_EDIT";
    const SHIFT_DELETE = "SHIFT_DELETE";
    const SHIFT_ADD = "SHIFT_ADD";
    const SHIFT_DRAG = "SHIFT_DRAG";
    const ASSIGN_OPENROUTE_TO_DRIVER = "ASSIGN_OPENROUTE_TO_DRIVER";
    const UNASSIGN_AND_POST_TO_OPEN_SHIFT = "UNASSIGN_AND_POST_TO_OPEN_SHIFT";
    const UNASSIGN_AND_DELETE_SHIFT = 'UNASSIGN_AND_DELETE_SHIFT';

    public static $eventsInMobileIcons = [
        self::DRIVER_ROUTE_CHECK => 'Play',
        self::LOAD_OUT => 'Play',
        self::RTS => 'Play',
        self::UNSCHEDULED_DRIVER_ROUTE => 'Backup',
        self::VEHICLE_ATTACHED => 'Backup',
        self::INSIDE_STATION => 'GeoferenceIn',
        self::OUTSIDE_STATION => 'GeoferenceOut',
        self::INSIDE_PARKING_LOT => 'ParkingGeoIn',
        self::OUTSIDE_PARKING_LOT => 'ParkingGeoOut',
        self::GAS => 'VehicleOil',
        self::MILEAGE => 'VehicleOil'
    ];

    public static $editEventsIcons = [
        self::SEND_HOME => 'Send Home',
        self::UNSEND_HOME => 'Unsend Home',
        self::DEVICE_ADD => 'Device Add',
        self::DEVICE_EDIT => 'Device Edit',
        self::VEHICLE_ADD => 'Vehicle Add',
        self::VEHICLE_EDIT => 'Vehicle Edit',
        self::SECOND_SHIFT_ADD => "Second Shift Add",
        self::SECOND_SHIFT_START_TIME_EDIT => "Second Shift Start Time Edit",
        self::SECOND_SHIFT_END_TIME_EDIT => "Second Shift End Time Edit",
        self::SECOND_SHIFT_INVOICE_TYPE_EDIT => "Second Shift Invoice Type Edit",
        self::SECOND_SHIFT_NOTE_EDIT => "Second Shift Note Edit",
        self::SECOND_SHIFT_DRIVER_NOTE_EDIT => "Second Shift Driver Note Edit",
        self::SECOND_SHIFT_PACKAGE_EDIT => "Second Shift Package Edit",
        self::INCIDENT_CREATE => 'Incident Create',
        self::INCIDENT_DELETE => 'Incident Delete',
        self::INCIDENT_PHASE_SAVE => 'Incident Phase Save',
        self::INCIDENT_PHASE_UPDATE => 'Incident Phase Update',
        self::SHIFT_START_TIME_EDIT => 'Shift Start Time Edit',
        self::SHIFT_END_TIME_EDIT => 'Shift End_Time Edit',
        self::EDIT_DRIVER_SHIFT_NOTE => 'Edit Driver Shift Note',
        self::EDIT_SHIFT_NOTE => 'Edit Shift Note',
        self::EDIT_SHIFT_NAME => 'Edit Shift Name',
        self::EDIT_SHIFT_INVOICE_TYPE => 'Edit Shift Invoice Type',
        self::ROUTE_CODE_ADD => "Route Code Add",
        self::ROUTE_ASSIGNED => "Route Assigned",
        self::ROUTE_UNASSIGNED => "Route Unassigned",
        self::ADD_TRAIN => "Add Train",
        self::EDIT_TRAIN_START_TIME => "Edit Train Start Time",
        self::EDIT_TRAIN_END_TIME => "Edit Train End Time",
        self::ADD_DUTY => "Add Light Duty",
        self::EDIT_DUTY_START_TIME => "Edit duty Start Time",
        self::EDIT_DUTY_END_TIME => "Edit duty End Time",
        self::ROUTE_CODE_EDIT => "ROUTE_CODE_EDIT",
        self::RESCUE_SHIFT_ADD => "RESCUE_SHIFT_ADD",
        self::RESCUE_SHIFT_START_TIME_EDIT => "RESCUE_SHIFT_START_TIME_EDIT",
        self::RESCUE_SHIFT_END_TIME_EDIT => "RESCUE_SHIFT_END_TIME_EDIT",
        self::RESCUE_SHIFT_INVOICE_TYPE_EDIT => "RESCUE_SHIFT_INVOICE_TYPE_EDIT",
        self::RESCUE_SHIFT_NOTE_EDIT => "RESCUE_SHIFT_NOTE_EDIT",
        self::RESCUE_SHIFT_DRIVER_NOTE_EDIT => "RESCUE_SHIFT_DRIVER_NOTE_EDIT",
        self::RESCUE_SHIFT_PACKAGE_EDIT => "RESCUE_SHIFT_PACKAGE_EDIT",
        self::SHIFT_DELETE => "SHIFT_DELETE",
        self::SHIFT_ADD => "SHIFT_ADD",
        self::SHIFT_DRAG => "SHIFT_DRAG",
        self::ASSIGN_OPENROUTE_TO_DRIVER => "ASSIGN_OPENROUTE_TO_DRIVER",
        self::UNASSIGN_AND_POST_TO_OPEN_SHIFT =>"UNASSIGN_AND_POST_TO_OPEN_SHIFT",
        self::UNASSIGN_AND_DELETE_SHIFT=>"UNASSIGN_AND_DELETE_SHIFT"
    ];

    /**
     * @param int $driverRouteId
     * @param int $timezoneOffset
     * @return array
     */
    public function lineFormatFromEvents(int $driverRouteId, int $timezoneOffset)
    {
        $events = $this->entityManager->getRepository(Event::class)->employeeTimeline(['driverRoute' => $driverRouteId]);
        $result = [];
        foreach ($events as $event) {
            $name = $event->getName();
            $createdAt = $event->getCreatedAt();
            if (isset(self::$eventsInMobileIcons[$name])) {
                $result[] = [
                    'type' => self::$eventsInMobileIcons[$name],
                    'status' => $event->getStatus(),
                    'information' => $event->getMessage(),
                    'iconType' => self::$eventsInMobileIcons[$name],
                    'createdAt' => $createdAt,
                    'title' => $event->getEventName(),
                    'id' => $event->getId(),
                    'driverRouteId' => $driverRouteId
                ];
            } else if (isset(self::$editEventsIcons[$name])) {
                $result[] = [
                    'type' => self::$editEventsIcons[$name],
                    'status' => '',
                    'information' => $this->entityManager->getRepository(Event::class)->getMessageByType($event->getMessage(), $event->getData(),$timezoneOffset),
                    'iconType' => self::$editEventsIcons[$name],
                    'createdAt' => $createdAt,
                    'title' => $event->getEventName(),
                    'id' => $event->getId(),
                    'driverRouteId' => $driverRouteId
                ];
            } else {
                $result[] = [
                    'type' => '',
                    'status' => '',
                    'information' => $event->getMessage(),
                    'iconType' => '',
                    'createdAt' => $createdAt,
                    'title' => $event->getEventName(),
                    'id' => $event->getId(),
                    'driverRouteId' => $driverRouteId
                ];
            }

        }

        usort($result, function ($a, $b) {
            return $b['createdAt'] > $a['createdAt'];
        });

        return $result;
    }

    /**
     * @Rest\Post("/driver/get_day_timelines")
     * @param Request $request
     * @return JsonResponse
     */
    public function getDayTimelines(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            if (empty($data['driverId'])) {
                throw new Exception('Driver Id empty.');
            }
            $driverRouteId = null;
            if (isset($data['driverRouteId'])) {
                $driverRouteId = $data['driverRouteId'];
            }
            $timezoneOffset = 0;
            if (isset($data['timezoneOffset'])) {
                $timezoneOffset = $data['timezoneOffset'];
            }

            $routes = [];
            $driverRoutes = $this->entityManager->getRepository(DriverRoute::class)->getDriverRoutes($data['driverId'], $data['timezone'], 30, $driverRouteId);
            /** @var DriverRoute $driverRoute */
            foreach ($driverRoutes as $driverRoute) {
                $driverRouteId = $driverRoute->getId();
                $events = $this->lineFormatFromEvents($driverRouteId, $timezoneOffset);
                $lines = [
                    [
                        'type' => 'LunchOut',
                        'status' => '',
                        'information' => 'N/A',
                        'iconType' => 'LunchOut',
                        'createdAt' => $driverRoute->getBreakPunchOut(),
                        'title' => 'Break Out',
                        'id' => $this->getUniqueIdentifier(),
                        'driverRouteId' => $driverRouteId
                    ],
                    [
                        'type' => 'LunchIn',
                        'status' => '',
                        'information' => 'N/A',
                        'iconType' => 'LunchIn',
                        'createdAt' => $driverRoute->getBreakPunchIn(),
                        'title' => 'Break In',
                        'id' => $this->getUniqueIdentifier(),
                        'driverRouteId' => $driverRouteId
                    ],
                    [
                        'type' => 'PunchIn',
                        'status' => '',
                        'information' => 'N/A',
                        'iconType' => 'PunchIn',
                        'createdAt' => $driverRoute->getPunchIn(),
                        'title' => 'Punch In',
                        'id' => $this->getUniqueIdentifier(),
                        'driverRouteId' => $driverRouteId
                    ],
                    [
                        'type' => 'PunchOut',
                        'status' => '',
                        'information' => 'N/A',
                        'iconType' => 'PunchOut',
                        'createdAt' => $driverRoute->getPunchOut(),
                        'title' => 'Punch Out',
                        'id' => $this->getUniqueIdentifier(),
                        'driverRouteId' => $driverRouteId
                    ]
                ];

                $lines = array_merge($lines, $events);

                $lines = array_filter($lines, function ($l) {
                    return !empty($l['createdAt']);
                });

                if (count($lines))
                    $routes[] = [
                        'id' => $driverRoute->getId(),
                        'date' => $driverRoute->getDateCreated(),
                        'timezone' => !empty($data['timezone']) ? $data['timezone'] : 'UTC',
                        'lines' => $lines,
                    ];
            }

            $routes = array_reduce($routes, function ($all, $next) {
                $date = $next['date']->format('c');
                $all[$date] = $all[$date] ?? [
                    'date' => $next['date'],
                    'lines' => [],
                    'timezone' => $next['timezone']
                ];

                $all[$date]['lines'] = array_merge($all[$date]['lines'], $next['lines']);

                usort($all[$date]['lines'], function ($a, $b) {
                    return $b['createdAt'] > $a['createdAt'];
                });

                return $all;
            }, []);

            $routes = array_values($routes);

            $driver = $this->entityManager->find(Driver::class, $data['driverId']);
            /** @var User $user */
            $user = $driver->getUser();
            $driverData = [
                'profileImage' => $user ? $user->getProfileImage() : null,
                'role' => $user ? $user->getRoleName() : null,
                'fullName' => $driver->getFullName()
            ];

            return $this->jsonResponse(true, ['driver' => $driverData, 'driverRoutes' => $routes], 200, 'routes', 'Routes fetched.');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/driver/getDriversBySkillName")
     * @param Request $request
     * @return JsonResponse
     */
    public function getDriversBySkillName(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $drivers = $this->entityManager->getRepository(Driver::class)->getDriversBySkillName($data);
            $drivers = array_map(function (Driver $item) {
                return [
                    'id' => $item->getId(),
                    'friendlyName' => $item->getFriendlyName(),
                    'email' => $item->getEmail(),
                    'driverSkills' => $item->getDriverSkills()
                ];
            }, $drivers);
            return $this->jsonResponse(true, $drivers, 200, 'drivers');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $exception) {
            return $this->jsonException($exception);
        }
    }

    /**
     * @Rest\Post("/driver/custom_replace_manager")
     * @param Request $request
     * @return JsonResponse
     */
    public function customReplaceManager(Request $request): JsonResponse
    {
        try {
            $data = $request->request->all();
            $drivers = $this->entityManager->getRepository(Driver::class)
                ->findBy(['isArchive' => false, 'company' => $data['companyId']]);
            $manager = $this->entityManager->getRepository(User::class)->find($data['managerId']);
            $result = [];
            foreach ($drivers as $driver) {
                /** @var Collection|User[] $managers */
                $managers = $driver->getAssignedManagers();
                if ($managers->count() > 0) {
                    if ($managers[0] instanceof User && !$managers[0]->getDriver()) {
                        $driver->removeAssignedManager($managers[0]);
                        $this->entityManager->flush();
                        $driver->addAssignedManager($manager);
                        $this->entityManager->flush();
                        $result[] = $driver;
                    }
                }
            }
            return $this->jsonResponse(true, $result, 200, 'drivers');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        } catch (Error $exception) {
            return $this->jsonException($exception);
        }
    }
}
