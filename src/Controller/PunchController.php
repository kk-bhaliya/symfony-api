<?php

namespace App\Controller;

use App\Criteria\Common;
use App\Entity\Company;
use App\Entity\DriverRoute;
use App\Entity\Event;
use Doctrine\ORM\Query\QueryException;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PunchController
 * @Route("/punch", name="punch_")
 */
class PunchController extends BaseController
{
    /**
     * @Route(
     *     name="set",
     *     path="/set",
     *     methods={"POST"}
     * )
     * @param Request $request
     * @return JsonResponse
     * @throws QueryException
     * @throws Exception
     */
    public function set(Request $request)
    {
        try {
            $record = json_decode($request->getContent());
        } catch (Exception $e) {
            return $this->jsonException($e);
        }

        if (!isset($record->paycomId) || !isset($record->company))
            return $this->jsonException(new Exception('Missing Paycom ID or Company ID'));

        $company = $this->entityManager->getRepository(Company::class)->find($record->company);

        if (empty($company))
            return $this->jsonException(new Exception("Company {$record->company} not found"));

        $stationToday = new \DateTime('now', new \DateTimeZone($record->timezone));
        $driverRoutes = $this->entityManager->createQueryBuilder()
            ->select('dr')
            ->from(DriverRoute::class, 'dr')
            ->leftJoin('dr.driver', 'd')
            ->where('d.paycomId = :paycomId')
            ->andWhere('dr.dateCreated = :date')
            ->andWhere('d.company = :company')
            ->setParameter('paycomId', $record->paycomId)
            ->setParameter('date', $stationToday->format('Y-m-d'))
            ->setParameter('company', $company)
            ->addCriteria(Common::notArchived('dr'))
            ->addCriteria(Common::notArchived('d'))
            ->getQuery()
            ->getResult();

        if (!count($driverRoutes))
            return $this->jsonException(new Exception('No route found for ' . $record->paycomId));

        $punchDriverRoutes = [];
        $punchDRs = [];
        $dr = $field = null;
        foreach ($record->punches as $punch) {
            $utcPunchTime = \DateTime::createFromFormat(
                DATE_RFC3339_EXTENDED,
                $punch->punchTime
            );

            $dr = $field = null;
            switch ($punch->punchType) {
                case 'ID':
                    $dr = $driverRoutes[0];
                    $field = 'PunchIn';
                    break;

                case 'OL':
                    $dr = $driverRoutes[0];
                    $field = 'BreakPunchIn';
                    break;

                case 'IL':
                    $dr = $driverRoutes[0];
                    $field = 'BreakPunchOut';
                    $utcPunchTime->modify('+1 second');
                    break;

                case 'OD':
                    $dr = $driverRoutes[count($driverRoutes) - 1];
                    $field = 'PunchOut';
                    break;
            }

            if (!$dr || $dr->{"get{$field}"}() === $utcPunchTime)
                continue;

            /** @var DriverRoute $dr */
            $dr->{"set{$field}"}($utcPunchTime);
            $this->entityManager->persist($dr);
            $punchDriverRoutes[] = [
                'driverRoute' => $dr,
                'punchType' => $field
            ];
            $punchDRs[] = [
                'driverRouteId' => $dr->getId(),
                'punchType' => $field
            ];
        }

        if (!$dr)
            return $this->json(
                [
                    'message' => 'Nothing to update',
                    'punchDRs' => $punchDRs
                ]);

        $this->entityManager->flush();

        $event = $this->eventEmitter->create();
        $data = [
            'driverRoute' => $dr->getId(),
            'punchIn' => $dr->getPunchIn(),
            'punchOut' => $dr->getPunchOut(),
            'breakPunchIn' => $dr->getBreakPunchIn(),
            'breakPunchOut' => $dr->getBreakPunchOut()
        ];
        $event
            ->setTopic([
                'company' => $company->getId(),
                'station' => $dr->getStation()->getId(),
                'topic' => 'loadout'
            ])
            ->setEvent('PUNCH')
            ->setEmitter($company->getOwners()->first()->getId())
            ->setData($data)
            ->dispatch();

        foreach ($punchDriverRoutes as $item) {
            $this->entityManager->getRepository(Event::class)->createPunchEvents($item['punchType'], $item['driverRoute']);
        }

        return $this->json([
            'message' => "DriverRoute #{$dr->getId()} {$field} updated",
            'driverRoute' => $data,
            'punchDRs' => $punchDRs
        ]);
    }
}