<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * Vehicle Driver Record Controller
 * @Route("/api",name="api_")
 */
class VehicleDriverRecordController extends BaseController
{
    /**
     * @Rest\Post("/vehicle_driver_record/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createVehicleDriverRecordAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/vehicle_driver_record/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateDriverRecordAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/vehicle_driver_record/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getVehicleDriverRecordAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }
}

