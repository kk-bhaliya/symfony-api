<?php

namespace App\Controller;

use App\Entity\Position;
use App\Form\PositionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Position Controller
 *
 * @Route("/api",name="api_")
 */

class PositionController extends BaseController
{
    /**
     * @Rest\Post("/position/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function createPositionAction(Request $request): JsonResponse
    {
       return $this->createRecord($request);
    }

    /**
     * @Rest\Get("/position/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Exception
     */
    public function getPositionAction(Request $request): JsonResponse
    {
        return $this->json($this->getAllValues());
    }
}

