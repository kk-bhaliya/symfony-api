<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Company;
use App\Entity\Role;
use App\Entity\GradingSettings;
use App\Entity\EmploymentStatus;
use Exception;
use Stripe\Exception\ApiErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * User Controller
 * @Route("/api",name="api_")
 */
class RegistrationController extends BaseController
{
    /**
     * @Rest\Post("/user/registration")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function index(Request $request, KernelInterface $kernel)
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $em = $this->getDoctrine()->getManager();
        $conn = $this->getDoctrine()->getConnection();

        $conn->beginTransaction();

        try {
            $company = new Company();
            $company->setName($data['company']['name']);
            $company->setApiKey($data['company']['apiKey']);
            $company->setNumberOfUser($data['company']['numberOfUser']);

            if (isset($data['company']['domain']))
                $company->setDomain($data['company']['domain']);

            $company->setAmazonAbbreviation($data['company']['amazonAbbreviation']);
            $em->persist($company);
            $em->flush();
            $data['id'] = $company->getId();

            $role = new Role();
            $role->setCompany($company);
            $role->setName('ROLE_DISPATCHER');
            $em->persist($role);
            $em->flush();
            $company->addCompanyRole($role);
            $em->persist($company);
            $em->flush();

            $role1 = new Role();
            $role1->setCompany($company);
            $role1->setName('ROLE_STATION_MANAGER');
            $em->persist($role1);
            $em->flush();
            $company->addCompanyRole($role1);
            $em->persist($company);
            $em->flush();

            $role2 = new Role();
            $role2->setCompany($company);
            $role2->setName('ROLE_LEAD_DRIVER');
            $em->persist($role2);
            $em->flush();
            $company->addCompanyRole($role2);
            $em->persist($company);
            $em->flush();

            $role3 = new Role();
            $role3->setCompany($company);
            $role3->setName('ROLE_OWNER');
            $em->persist($role3);
            $em->flush();
            $company->addCompanyRole($role3);
            $em->persist($company);
            $em->flush();

            $role4 = new Role();
            $role4->setCompany($company);
            $role4->setName('ROLE_DELIVERY_ASSOCIATE');
            $em->persist($role4);
            $em->flush();
            $company->addCompanyRole($role4);
            $em->persist($company);
            $em->flush();

            $role5 = new Role();
            $role5->setCompany($company);
            $role5->setName('ROLE_OPERATIONS_MANAGER');
            $em->persist($role5);
            $em->flush();
            $company->addCompanyRole($role5);
            $em->persist($company);
            $em->flush();

            $role6 = new Role();
            $role6->setCompany($company);
            $role6->setName('ROLE_ASSISTANT_STATION_MANAGER');
            $em->persist($role6);
            $em->flush();
            $company->addCompanyRole($role6);
            $em->persist($company);
            $em->flush();

            $role7 = new Role();
            $role7->setCompany($company);
            $role7->setName('ROLE_SAFETY_MANAGER');
            $em->persist($role7);
            $em->flush();
            $company->addCompanyRole($role7);
            $em->persist($company);
            $em->flush();

            $role8 = new Role();
            $role8->setCompany($company);
            $role8->setName('ROLE_FLEET_MANAGER');
            $em->persist($role8);
            $em->flush();
            $company->addCompanyRole($role8);
            $em->persist($company);
            $em->flush();

            $role9 = new Role();
            $role9->setCompany($company);
            $role9->setName('ROLE_HR_ADMINISTRATOR');
            $em->persist($role9);
            $em->flush();
            $company->addCompanyRole($role9);
            $em->persist($company);
            $em->flush();

            $role10 = new Role();
            $role10->setCompany($company);
            $role10->setName('ROLE_CENTRAL_DISPATCHER');
            $em->persist($role10);
            $em->flush();
            $company->addCompanyRole($role10);
            $em->persist($company);
            $em->flush();

            $role11 = new Role();
            $role11->setCompany($company);
            $role11->setName('ROLE_OPERATIONS_ACCOUNT_MANAGER');
            $em->persist($role11);
            $em->flush();
            $company->addCompanyRole($role11);
            $em->persist($company);
            $em->flush();

            $role12 = new Role();
            $role12->setCompany($company);
            $role12->setName('ROLE_SAFETY_CHAMPION');
            $em->persist($role12);
            $em->flush();
            $company->addCompanyRole($role12);
            $em->persist($company);
            $em->flush();

            $user = new User();
            $user->setEmail($data['email']);
            $user->setCompany($company);
            $user->addUserRole($role3);
            $user->setPassword($data['password']);
            $user->setPhoneNumber($data['phoneNumber']);
            $user->setFirstName($data['firstName']);
            if (array_key_exists('lastName', $data)) {
                $user->setLastName($data['lastName']);
            } else {
                $user->setLastName($data['firstName']);
            }
            $em->persist($user);
            $em->flush();
            $data['userId'] = $user->getId();

            $empStatus1 = new EmploymentStatus();
            $empStatus1->setCompanyId($company);
            $empStatus1->setName('Exempt');
            $empStatus1->setIsArchive(0);
            $em->persist($empStatus1);
            $em->flush();

            $empStatus2 = new EmploymentStatus();
            $empStatus2->setCompanyId($company);
            $empStatus2->setName('Non-Exempt');
            $empStatus1->setIsArchive(0);
            $em->persist($empStatus2);
            $em->flush();

            $gradingSetting1 = new GradingSettings();
            $gradingSetting1->setCompany($company);
            $gradingSetting1->setColor('#F2545B');
            $gradingSetting1->setMin('0');
            $gradingSetting1->setMax('99');
            $gradingSetting1->setDescription('Requires more drivers!');
            $gradingSetting1->setCreatedDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
            $em->persist($gradingSetting1);
            $em->flush();

            $gradingSetting2 = new GradingSettings();
            $gradingSetting2->setCompany($company);
            $gradingSetting2->setColor('#F5C26B');
            $gradingSetting2->setMin('100');
            $gradingSetting2->setMax('109');
            $gradingSetting2->setDescription('Be sure you have enough backup drivers.');
            $gradingSetting2->setCreatedDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
            $em->persist($gradingSetting2);
            $em->flush();

            $gradingSetting3 = new GradingSettings();
            $gradingSetting3->setCompany($company);
            $gradingSetting3->setColor('#7FDED2');
            $gradingSetting3->setMin('110');
            $gradingSetting3->setMax('114');
            $gradingSetting3->setDescription('Looking good!');
            $gradingSetting3->setCreatedDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
            $em->persist($gradingSetting3);
            $em->flush();

            $gradingSetting4 = new GradingSettings();
            $gradingSetting4->setCompany($company);
            $gradingSetting4->setColor('#B4BBE8');
            $gradingSetting4->setMin('115');
            $gradingSetting4->setDescription('This might be too many...');
            $gradingSetting4->setCreatedDate((new \DateTime())->setTimezone(new \DateTimeZone('UTC')));
            $em->persist($gradingSetting4);
            $em->flush();

            $application = new Application($kernel);
            $application->setAutoExit(false);
            $input = new ArrayInput(['command' => 'dsp:make:incidentform', '--companies' => array($company->getId())]);
            $output = new BufferedOutput();
            $application->run($input, $output);

            if ($user->getId() && $company->getId()) {
                $request->request->set('id', $company->getId());
                $request->request->set('userId', $user->getId());
                $request->request->set('companyId', $company->getId());
                $response = $this->forward('App\Controller\TwilioUserController::createTwilioSubAccount', ['request' => $request]);
            } else {
                $status['status'] = false;
                $status['message'] = "Something went wrong in twilio registration !!";
                $e = new \Exception("Something went wrong in twilio registration !!");

                $conn->rollback();
                return $this->jsonException($e);
            }

            if (!array_key_exists('stripe', $data)) {
                $this->getStripeService()->setApiKey();

                if($user){
                    try {
                        $customer = $this->getStripeService()->createCustomer([
                            'email' => $data['email'],
                            'source' => $data['token'],
                            'name' => $data['company']['name']
                        ]);

                        try {
                            // split product and addons subscriptions
                            $planSubscription = $this->getStripeService()->createSubscription([
                                'customer' => $customer->id,
                                'trial_end' => $data['plan']['trial'] ?? strtotime('+30 day'),
                                'items' => array_map(function ($plan) use ($data) {
                                    return [
                                        'price' => $plan,
                                        'quantity' => $data['plan']['quantity'],
                                    ];
                                }, $data['plan']['planId']),
                            ]);

                            if ($planSubscription instanceof ApiErrorException)
                                throw $planSubscription;

                            $addonSubscription = count($data['plan']['addonPlan'])
                            ? $this->getStripeService()->createSubscription([
                                'customer' => $customer->id,
                                'items' => array_map(function ($plan) {
                                    return ['plan' => $plan];
                                }, $data['plan']['addonPlan']),
                            ]) : new \stdClass();

                            if ($addonSubscription instanceof ApiErrorException)
                                throw $addonSubscription;

                            $company->setStripeData($this->json(($this->getStripeService()->getCustomer($customer->id)))->getContent());
                            $em->persist($company);
                            $em->flush();

                            $status['status'] = true;
                            $status['message'] = "User Stripe Registration Successfully";
                            $status['userId'] = $user->getId();

                            $conn->commit();

                            return $this->json($status);

                        } catch (\Exception $e) {
                            $this->getStripeService()->deleteCustomer($customer->id);

                            $conn->rollback();

                            $em->transactional(function ($em) use ($company, $user) {
                                /** @var User $user */
                                $em->remove($company);
                                $em->remove($user);
                                $em->flush();
                            });

                            $status['status']=false;
                            $status['message']= "user not registration issue with packages stripe";


                            return $this->jsonException($e);
                        }
                    } catch (\Exception $e) {
                        /** @var User $user */

                        $conn->rollback();

                        $em->transactional(function ($em) use ($company, $user) {
                            /** @var User $user */
                            $em->remove($company);
                            $em->remove($user);
                            $em->flush();
                        });

                        $status['status']=false;
                        $status['message']= "user not registration issue with stripe ";

                        return $this->jsonException($e);
                    }
                }
            }
        } catch (\Exception $exception) {
            $conn->rollback();
            return $this->jsonException($exception);
        }

        return $this->json($data);
    }


    /**
     * @Rest\Post("/company/check/domain/exist")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function checkDomainExitsAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $entityManager = $this->getDoctrine()->getManager();
        $user = $entityManager->getRepository('App:Company')->findOneBy(['domain' => $data['domain']]);

        if( $user ){
            $status['status']=true;
        } else {
            $status['status']=false;
        }
        return $this->json($status);
    }

    /**
     * @Rest\Post("/forgot-password")
     * @param Request $request
     *
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     * @throws Exception
     */
    public function forgotPasswordAction(Request $request, UserPasswordEncoderInterface $encoder): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('App:User')->findOneBy(['email' => $data['email'], 'isArchive' => false]);
        $password = $this->plainPassword();

        if( $user ){
            $password = $this->plainPassword();
            $encoded = $encoder->encodePassword($user, $password);
            $user->setPassword($encoded);
            $em->persist($user);
            $em->flush();

            $content = $this->renderView('emails/password-reset.twig', ['password'=>$password,'name'=>$user->getFirstName()]);
            $this->getEmailService()->sendEmail($data['email'], 'Your New Password', $content);

            $status['data']=true;
            $status['success'] = true;
            $status['message'] = 'An email has been sent to reset your password.';
        } else {
            $status['data']=false;
            $status['success'] = false;
            $status['message'] = 'The provided email has not been found in our records.';

        }
        return $this->json($status);
    }

    public function plainPassword()
    {
        $numbers = array_rand(range(0, 9), rand(2, 2));
        $uppercase = array_rand(array_flip(range('A', 'Z')), rand(2, 2));
        $lowercase = array_rand(array_flip(range('a', 'z')), rand(2, 2));
        $special = array_rand(array_flip(['@', '#', '$', '!', '%', '*', '?', '&']), rand(2, 2));
        $password = array_merge(
            $numbers,
            $uppercase,
            $lowercase,
            $special
        );
        shuffle($password);
        return implode($password);
    }

    /**
     * @Rest\Post("/send-employee-registration-mail")
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function sendMailAction(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('App:Company')->find( $data['company']);

        if (isset($data['isLoadout']) && $data['isLoadout'] == true) {
            $content = $this->renderView('emails/loadout-password-reset.twig', ['name'=>$data['name'],'password'=>$data['password']]);
            $subject = 'Password Changed';
        } else {
            $content = $this->renderView('emails/employee-create.twig', ['company'=>$company,'name'=>$data['name'],'password'=>$data['password']]);
            $subject = 'Your Employee Account Created';
        }
        $this->getEmailService()->sendEmail($data['email'], $subject, $content);

        return $this->json(["status"=>true]);
    }

}
