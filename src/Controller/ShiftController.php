<?php

namespace App\Controller;

use App\Entity\Shift;
use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * Shift Controller
 * @Route("/api",name="api_")
 */
class ShiftController extends BaseController
{
    /**
     * @Rest\Post("/shift/create")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function createShiftAction(Request $request): JsonResponse
    {
        return $this->createRecord($request);
    }

    /**
     * @Rest\Post("/shift/update")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function updateShiftAction(Request $request): JsonResponse
    {
        return $this->updateRecord($request);
    }

    /**
     * @Rest\Get("/shift/get")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function getShiftAction(Request $request): JsonResponse
    {
        return $this->getAllValues();
    }

    /**
     * @Rest\Post("/shift/fetch/")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchShiftAction(Request $request): JsonResponse
    {
        try {
            $data= $request->request->all();
            $shift = $this->entityManager->find(Shift::class, $data['id']);
            return $this->jsonResponse(true, $shift, 200, 'shift', 'Shift fetched');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }

    }

    /**
     * @Rest\Post("/shift/fetch/with/drivers")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchShiftWithDrivers(Request $request): JsonResponse
    {
        try {
            $data= $request->request->all();
            $shift = $this->entityManager->find(Shift::class, $data['id']);
            $criteria['shiftId'] = $data['id'];
            $drivers = $this->entityManager->getRepository(Shift::class)->getFutureDrivers($criteria);
            $result = [
                'shift' => $shift,
                'drivers' => $drivers,
            ];
            return $this->jsonResponse(true, $result, 200, 'shift', 'Shift with drivers fetched');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }

    }

    /**
     * @Rest\Post("/shift/fetch/with/driver_routes")
     * @param Request $request
     * @return JsonResponse
     */
    public function fetchShiftWithDriverRoutes(Request $request): JsonResponse
    {
        try {
            $data= $request->request->all();
            $shift = $this->entityManager->find(Shift::class, $data['id']);
            $routes = $this->entityManager->getRepository(Shift::class)->getFutureDriverRoutes($data);
            $result = [
                'shift' => $shift,
                'routes' => $routes,
            ];
            return $this->jsonResponse(true, $result, 200, 'shift', 'Shift with drivers fetched');
        } catch (Exception $exception) {
            return $this->jsonException($exception);
        }
    }
}
