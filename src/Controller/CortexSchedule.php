<?php
namespace App\Controller;

use App\Entity\CortexLog;
use App\Entity\CortexSchedule as Schedule;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\Route;

/**
 * CortexSchedule Controller
 * @Route("/cortex",name="cortex_")
 */
class CortexSchedule extends BaseController
{
    /**
     * @Route(
     *     name="createSchedule",
     *     path="/create/schedule",
     *     methods={"POST"},
     * )
     * @param Request $request
     * @return bool
     */
    public function createScheduleAction(Request $request)
    {
        $doctrine = $this->getDoctrine();
        $em = $doctrine->getManager();
        $records = json_decode($request->getContent(), true);

        if(empty($records['data'])) return true; // Nothing is returned so lets just exit.

        //Cortex Log
        if($records['type'] == 'log') {
            $cortexLog = new CortexLog();
            $cortexLog->setRecord($records['data']);
            $cortexLog->setStation($records['station']);

            $company = $cortexLog->getCompany($records['company']);
            if(!is_null($company)){
                $cortexLog->setCompany($company);
            }

            $dateCaptured = \DateTime::createFromFormat('U', strtotime('now'));
            $cortexLog->setCreated($dateCaptured);

            if(!isset($records['routeCode'])) {
                $cortexLog->setRouteCode($records['routeCode']);
            }

            $em->persist($cortexLog);
            $em->flush();
        }

        //Cortex Route Summeries
        // Prolly should move to its own route
        if($records['type'] == 'routeSummaries') {
            if($records['data']['routeSummaries']) {
                foreach($records['data']['routeSummaries'] as $routeSummary ) {
                    $cortexSchedule = new Schedule();
                    $cortexSchedule->setStation($records['station']);
                    $cortexSchedule->setRouteSummeries($routeSummary);

                    $dateCaptured = \DateTime::createFromFormat('U', strtotime('now'));
                    $cortexSchedule->setCreated($dateCaptured);

                    $em->persist($cortexSchedule);
                    $em->flush();
                }
            }
        }
        
        return true;
    }
}