<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Route;
/**
 * Coupler Controller
 * @Route("/api",name="api_")
 */
class ConnectorController extends BaseController
{
    /**
     * @Rest\Post("/connector")
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @throws \Exception
     */
    public function couplerAction(Request $request): JsonResponse
    {
        $this->dataHandler->connectRecords($request);
        return $this->json('Successful Request!', 200);
    }

}
