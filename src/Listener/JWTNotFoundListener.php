<?php


namespace App\Listener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTNotFoundEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;


class JWTNotFoundListener
{

    public function onJWTNotFound(JWTNotFoundEvent $event)
    {
        $data['message'] = $event->getException()->getMessage();
        $response = new JsonResponse($data,Response::HTTP_UNAUTHORIZED);
        $event->setResponse($response);
    }
}
