<?php


namespace App\Listener;

use App\Entity\Company;
use App\Entity\Driver;
use App\Entity\Station;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Response\JWTAuthenticationSuccessResponse;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class AuthenticationSuccessListener
{

    /**
     * @var RequestStack
     */
    private $requestStack;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;


    public function __construct(RequestStack $requestStack, EntityManagerInterface $entityManager)
    {
        $this->requestStack = $requestStack;

        $this->entityManager = $entityManager;
    }

    /**
     * @param $userId
     * @param User $user
     * @param Company $company
     * @return mixed
     */
    private function userDataFirst($userId, User $user, Company $company)
    {
        $users = $this->entityManager->getRepository(User::class)->loginUser(['userId' => $userId]);
        if (count($users) > 0) {
            $userData = $users[0];
            $roleName = str_replace("ROLE_", "", $userData['role']);
            $roleName = str_replace("_", " ", $roleName);
            $roleName = ucwords(strtolower($roleName));
            $roleName = str_replace('Hr ', 'HR ', $roleName);
            $userData['position'] = $roleName;
            $station = [
                'id' => $userData['stationId'],
                'code' => $userData['stationCode'],
                'name' => $userData['stationName']
            ];
            $userData['station'] = $station;
            $userData['companyId'] = $userData['company'];
            $userData['selectedStation'] = $userData['stationId'];
            $userData['currentUserRole'] = $userData['role'];
            $userData['userRoleId'] = $userData['userRoleId'];
            $userData['isDriver'] = true;
            $userData['managers'] = [];
            return $userData;
        }
        $station = $this->entityManager->getRepository(Station::class)->findOneBy(['company' => $company->getId(), 'isArchive' => false]);
        $data['userId'] = $user->getId();
        $data['friendlyName'] = $user->getFriendlyName();
        $data['role'] = $user->getUserFirstRole();
        $data['chatIdentifier'] = $user->getChatIdentifier();
        $data['currentUserRole'] = $user->getUserFirstRole();
        $data['isEnabled'] = $user->getIsEnabled();
        $data['position'] = $user->getRoleName();
        $data['profileImage'] = $user->getProfileImage();
        $data['isResetPassword'] = $user->getIsResetPassword();
        $data['station'] = $station;
        $data['selectedStation'] = $station instanceof Station ? $station->getId() : null;
        $data['company'] = $company->getId();
        $data['companyId'] = $data['company'];
        $driver = $user->getDriver();
        $data['driverId'] = $driver ? $driver->getId() : null;
        $userData['isDriver'] = false;
        $data['managers'] = [];
        return $data;
    }

    /**
     * @param AuthenticationSuccessEvent $event
     * @return JWTAuthenticationSuccessResponse
     * @throws Exception
     */
    public function onAuthenticationSuccess(AuthenticationSuccessEvent $event)
    {
        /** @var JWTAuthenticationSuccessResponse $response */
        $response = $event->getResponse();
        $data = $event->getData();
        /** @var User $user */
        $user = $event->getUser();
        $company = $user->getCompany();
        $email = $user->getUsername();
        $data['email'] = $email;
        if ($user->getIsArchive()) {
            $event->setData(['message' => $email . ' is not found!!!']);
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
        if (!($company instanceof Company)) {
            $event->setData(['message' => 'Company Not registered.']);
            $response->setStatusCode(Response::HTTP_FORBIDDEN);
            return $response;
        }
        $userData = $this->userDataFirst($user->getId(), $user, $company);
        $data = array_merge($data, $userData);
        $tokenJWT = $data['token'];
        $response->headers->add(['HTTP_Authorization' => 'Bearer ' . $tokenJWT]);
        $event->setData($data);
        return $response;
    }
}