<?php

namespace App\Listener;

use App\Entity\BalanceGroup;
use App\Entity\Driver;
use App\Entity\DriverRoute;
use App\Entity\Event;
use App\Entity\InvoiceType;
use App\Entity\IncidentType;
use App\Entity\Rate;
use App\Entity\Schedule;
use App\Entity\Shift;
use App\Entity\ShiftTemplate;
use App\Entity\Skill;
use App\Entity\Station;
use App\Entity\TempDriverRoute;
use App\Entity\User;
use App\Entity\VehicleDriverRecord;
use App\Entity\UpdatedSchedule;
use App\Entity\Device;
use App\Entity\Vehicle;
use App\Utils\EventEmitter;
use DateTime;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class PostUpdateEntityListener implements EventSubscriber
{
    /**
     *
     * @var ObjectManager
     */
    private $em;

    /**
     *
     * @var Object
     */
    private $entityObject;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EventEmitter
     */
    private $eventEmitter;

    /**
     * DataHandler constructor.
     *
     * @param SessionInterface $session
     * @param TokenStorageInterface $tokenStorage
     * @param EventEmitter $eventEmitter
     */
    public function __construct(
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        EventEmitter $eventEmitter
    )
    {
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->eventEmitter = $eventEmitter;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preUpdate,
        ];
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entityObject = $args->getObject();
        $this->updatedByTokenUser($entityObject);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entityObject = $args->getObject();
        $this->updatedByTokenUser($entityObject);
        $this->runUpdateScheduleEvent($args);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->entityObject = $args->getObject();
        $this->em = $args->getObjectManager();

        $this->runUpdateScheduleEvent($args);
        if ((!empty($this->session->get('soft-delete-enable')) && $this->session->get('soft-delete-enable')) || (method_exists($this->entityObject, 'getIsArchive') && $this->entityObject->getIsArchive() == true)) {
            $this->postDeleteAfterSoftDelete();
        }

    }

    public function runUpdateScheduleEvent(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Schedule) {
            if ($entity->getIsArchive() === false) {
                $updatedSchedule = new UpdatedSchedule();
                $updatedSchedule->setSchedule($entity);
                $updatedSchedule->setStation($entity->getStation());
                $entityManager = $args->getObjectManager();
                $entityManager->persist($updatedSchedule);
                $entityManager->flush();
            }
        }

    }

    private function postDeleteAfterSoftDelete()
    {

        //Incident Type soft delete
        if ($this->entityObject instanceof IncidentType) {
            $this->em->getRepository('App\Entity\IncidentType')->updateSetIsArchiveByIncidentType($this->entityObject);
            $this->em->getRepository('App\Entity\Incident')->updateIncidentIsArchiveByIncidentType($this->entityObject);
        }

        //Station soft delete
        if ($this->entityObject instanceof Station) {
            $this->em->getRepository('App\Entity\DriverRoute')->updateStationIsArchiveInDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\VehicleDriverRecord')->updateStationIsArchiveInVehicleDriverRecord($this->entityObject->getId());
            $this->em->getRepository('App\Entity\InvoiceType')->updateStationIsArchiveInInvoiceType($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Shift')->updateStationIsArchiveInShift($this->entityObject->getId());
            $this->em->getRepository('App\Entity\SkillRate')->updateStationIsArchiveInSkillRate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Rate')->updateStationIsArchiveInRate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Schedule')->updateStationIsArchiveInSchedule($this->entityObject->getId());
            $this->em->getRepository('App\Entity\RouteCommitment')->updateStationIsArchiveInRouteCommitment($this->entityObject->getId());
            $this->em->getRepository('App\Entity\GradingSettings')->updateStationIsArchiveInGradingSettings($this->entityObject->getId());
            $this->em->getRepository('App\Entity\BalanceGroup')->updateStationIsArchiveInBalanceGroup($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Vehicle')->updateStationIsArchiveInVehicle($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Device')->updateStationIsArchiveInDevice($this->entityObject->getId());
            $this->em->getRepository('App\Entity\TempDriverRoute')->deleteTempRouteWhereIsNewOne($this->entityObject->getId());
        }

        //Driver soft delete make open shift
        if ($this->entityObject instanceof Driver) {
            //Driver route and tempdriver route changes
            $this->em->getRepository('App\Entity\DriverRoute')->updateDriverOpenShiftInDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\TempDriverRoute')->updateDriverIsArchiveInTempDriverRoute($this->entityObject->getId());
        }

        //Rate soft delete
        if ($this->entityObject instanceof Rate) {
            //Invoice Type Rate set Null
            $this->em->getRepository('App\Entity\InvoiceType')->updateRateNullInInvoiceType($this->entityObject->getId());
        }

        //BalanceGroup soft delete
        if ($this->entityObject instanceof BalanceGroup) {
            //Balance Group set null in shift
            $this->em->getRepository('App\Entity\Shift')->updateBalanceGroupNullInShift($this->entityObject->getId());
            $this->em->getRepository('App\Entity\DriverRoute')->updateBalanceGroupNullInDriverRouteAndTempDriverRoute($this->entityObject->getId());
        }

        //DriverRoute soft delete
        if ($this->entityObject instanceof DriverRoute) {
            if ($this->entityObject->getIsRescuer())
                $this->em->getRepository('App\Entity\DriverRoute')->movePunchTimesFromArchivedRescueShift($this->entityObject->getId());

            $this->em->getRepository('App\Entity\TempDriverRoute')->updateDriverRouteIsArchiveInTempDriverRoute($this->entityObject->getId());
        }

        //TempDriverRoute
        if ($this->entityObject instanceof TempDriverRoute) {
            $data = [
                'from' => [
                    'driverId' => '0.0.0',
                    'week' => $this->entityObject->getDateCreated()->format('N'),
                    'id' => $this->entityObject->getId(),
                ],
                'item' => $this->entityObject,
                'hasAlert' => true,
            ];

            if (!empty($this->entityObject->getDriver())) {
                $data['from']['driverId'] = $this->entityObject->getDriver()->getId();
            }

            $event = $this->eventEmitter->create();
            $event
                ->setTopic([
                    'company' => $this->entityObject->getStation()->getCompany()->getId(),
                    'station' => $this->entityObject->getStation()->getId(),
                    'topic' => 'scheduler'
                ])
                ->setEmitter($this->tokenStorage->getToken()->getUser()->getId())
                ->setEvent('REMOVED')
                ->setData($data)
                ->dispatch();
        }

        //InvoiceType soft delete
        if ($this->entityObject instanceof InvoiceType) {
            $this->em->getRepository('App\Entity\Shift')->updateInvoiceTypeNullInShifts($this->entityObject->getId());
            $this->em->getRepository('App\Entity\ShiftTemplate')->updateInvoiceTypeNullInShiftTemplate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\ShiftTemplate')->updateInvoiceTypeNullInShiftTemplate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\DriverRoute')->updateInvoiceTypeNullInDriverRouteAndTempDriverRoute($this->entityObject->getId());

        }

        //ShiftTemplate soft delete
        if ($this->entityObject instanceof ShiftTemplate) {
            $this->em->getRepository('App\Entity\Shift')->updateShiftTemplateNullInShifts($this->entityObject->getId());
        }

        //VehicleDriverRecord soft delete
        if ($this->entityObject instanceof VehicleDriverRecord) {
            // $this->callStation();
            // $this->callDriverRoutes();
            // $this->callTempDriverRoutes();
        }

        //Shift soft delete
        if ($this->entityObject instanceof Shift) {
            $this->em->getRepository('App\Entity\DriverRoute')->updateShiftIsArchiveInDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\TempDriverRoute')->updateShiftIsArchiveInTempDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\RouteCommitment')->updateShiftIsArchiveInRouteCommitment($this->entityObject->getId());
        }

        //Schedule soft delete
        if ($this->entityObject instanceof Schedule) {
            $this->em->getRepository('App\Entity\ScheduleDesign')->updateScheduleIsArchiveInScheduleDesign($this->entityObject->getId());
            $this->em->getRepository('App\Entity\DriverRoute')->updateScheduleIsArchiveInDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\TempDriverRoute')->updateScheduleIsArchiveInTempDriverRoute($this->entityObject->getId());
        }

        //Schedule soft delete
        if ($this->entityObject instanceof Skill) {
            $this->em->getRepository('App\Entity\Skill')->updateSkillNullInSkill($this->entityObject->getId());
            $this->em->getRepository('App\Entity\DriverRoute')->updateSkillNullInDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\PayRate')->updateSkillNullInPayRate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\SkillRate')->updateSkillNullInSkillRate($this->entityObject->getId());
            $this->em->getRepository('App\Entity\Shift')->updateSkillNullInShift($this->entityObject->getId());
            $this->em->getRepository('App\Entity\DriverSkill')->updateSkillNullInDriverSkill($this->entityObject->getId());
            $this->em->getRepository('App\Entity\TempDriverRoute')->updateSkillNullInTempDriverRoute($this->entityObject->getId());
            $this->em->getRepository('App\Entity\ShiftTemplate')->updateSkillNullInShiftTemplate($this->entityObject->getId());
        }

        //Vehicle soft delete
        if ($this->entityObject instanceof Vehicle) {
            $this->em->getRepository('App\Entity\DriverRoute')->updateVehicleNullInDriverRoute($this->entityObject->getId());
        }

        //Device soft delete
        if ($this->entityObject instanceof Device) {
            $this->em->getRepository('App\Entity\DriverRoute')->deleteDeviceFromDriverRoute($this->entityObject->getId());
        }
    }

    public function updatedByTokenUser($entityObject)
    {
        if (method_exists($entityObject, 'setUpdatedBy') && $this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if ($user instanceof User) {
                $entityObject->setUpdatedBy($user);
            }
        }
    }

    public function createdAtHandler($entityObject)
    {
        if (method_exists($entityObject, 'setCreatedAt')) {
            if (is_null($entityObject->getCreatedAt())) {
                $entityObject->setCreatedAt(new DateTime());
            }
        }
    }
}
