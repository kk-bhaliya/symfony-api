<?php


namespace App\Listener;


use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTExpiredEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class JWTExpiredListener
{
    public function onJWTExpired(JWTExpiredEvent $event)
    {
        $data['message'] = 'Your session has expired. Please login again.';
        $response = new JsonResponse($data,Response::HTTP_UNAUTHORIZED);
        $event->setResponse($response);
    }
}
