<?php


namespace App\Listener;


use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationFailureListener
{

    public function onAuthenticationFailure(AuthenticationFailureEvent $event){

        $data['message'] = 'Authentication Failed. ' .  $event->getException()->getMessage();
        $data['exception'] = $event->getException()->getMessage();
        $response = new JsonResponse($data,Response::HTTP_FORBIDDEN);
        $event->setResponse($response);
    }

}