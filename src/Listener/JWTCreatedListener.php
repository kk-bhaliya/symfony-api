<?php


namespace App\Listener;

use DateTimeZone;
use Exception;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class JWTCreatedListener
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    /**
     * @param JWTCreatedEvent $event
     * @throws Exception
     */
    public function onJWTCreated(JWTCreatedEvent $event)
    {
        $request = $this->requestStack->getCurrentRequest();
        $payload = $event->getData();
        $payload['ip'] = $request->getClientIp();
        $data = json_decode($this->requestStack->getCurrentRequest()->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $timezone = "America/Detroit";

        if (!empty($data['timezone']) && is_string($data['timezone'])) {
            $timezone = $data['timezone'];
            $payload['timezone'] = $data['timezone'];
        }

        $expiration = new \DateTime('+2 day', new DateTimeZone($timezone));
        $expiration->setTime(2, 0, 0);

        if (isset($data['isCorporate']) && $data['isCorporate'] === false) {
            $expiration = new \DateTime('+90 day', new DateTimeZone($timezone));
            $payload['isCorporate'] = $data['isCorporate'];
        }

        $payload['exp'] = $expiration->getTimestamp();
        $payload['tz'] = $expiration->getTimezone();
        $payload['offset'] = $expiration->getOffset();
        $payload['string_exp'] = $expiration->format('Y-m-d H:i:s P T');

        $payload['locale'] = $request->getLocale();
        $payload['timestamp'] = $expiration->getTimestamp();
        $payload['date'] = $expiration->format('l, F jS Y-m-d H:i:sa P T');
        $payload['expiration'] = $expiration->format('Y-m-d H:i:sa P T');

        $event->setData($payload);
        $header = $event->getHeader();
        $header['cty'] = 'JWT';
        $event->setHeader($header);
    }
}
