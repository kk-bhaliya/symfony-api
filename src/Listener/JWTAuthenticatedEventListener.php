<?php


namespace App\Listener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTAuthenticatedEvent;
use Symfony\Component\HttpFoundation\JsonResponse;


class JWTAuthenticatedEventListener
{

    public function onAuthenticated(JWTAuthenticatedEvent $event)
    {
      $data['payload'] = $event->getPayload();
      $data['token']= $event->getToken();

      return new JsonResponse($data);
    }
}