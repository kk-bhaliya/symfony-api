<?php


namespace App\Worker;


use App\Entity\DriverRoute;
use App\Services\BotService;
use Dtc\QueueBundle\Model\Worker;
use Exception;

class BotServiceWorker extends Worker
{

    /**
     * @var BotService
     */
    protected $botService;

    public function __construct(BotService $botService)
    {

        $this->botService = $botService;
    }


    public function sendPlivoPunchPaycomMessageToWorker(array $data, DriverRoute $driverRoute, bool $useLambda = false, string $case = 'PunchIn')
    {
        $result = $this->botService->sendPlivoPunchPaycomMessage($data, $driverRoute, $useLambda, $case);
        file_put_contents("/tmp/plivo_punch_paycom.txt", "\n\n" . json_encode($result) . "\n\n", FILE_APPEND);
    }

    public function sendPlivoBreakReminderToWorker(array $data, DriverRoute $driverRoute, bool $useLambda = false)
    {
        $result = $this->botService->sendPlivoBreakReminder($data, $driverRoute, $useLambda);
        file_put_contents("/tmp/plivo_break_paycom.txt", "\n\n" . json_encode($result) . "\n\n", FILE_APPEND);
    }

    public function sendMessagesToStationManagersToWorker(array $data, bool $useLambda = false)
    {
        try {
            $result = $this->botService->sendMessagesToStationManagers($data, $useLambda);
            file_put_contents("/tmp/twilio_message_managers.txt", "\n\n" . json_encode($result) . "\n\n", FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents("/tmp/twilio_message_managers.txt", "\n\n" . json_encode($e->getTrace()) . "\n\n", FILE_APPEND);
        }
    }

    public function createEventToWorker(array $data)
    {
        $result = $this->botService->createEvent($data);
        file_put_contents("/tmp/event_result.txt", "\n\n" . json_encode($result) . "\n\n", FILE_APPEND);
    }

    public function createTaskToWorker(array $data)
    {
        try {
            $result = $this->botService->createTask($data);
            file_put_contents("/tmp/task_result.txt", "\n\n" . json_encode($result) . "\n\n", FILE_APPEND);
        } catch (Exception $e) {
            file_put_contents("/tmp/task_result.txt", "\n\n" . json_encode($e->getTrace()) . "\n\n", FILE_APPEND);
        }
    }

    public function later($delay = 0, $priority = null)
    {
        return parent::later($delay, $priority);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'bot_service_worker';
    }
}