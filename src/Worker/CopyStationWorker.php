<?php 

namespace App\Worker;

use App\Entity\Station;
use App\Entity\SkillRate;
use App\Entity\Rate;
use App\Entity\InvoiceType;
use App\Entity\Shift;
use App\Entity\Schedule;
use App\Entity\ScheduleDesign;
use App\Entity\BalanceGroup;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Dtc\QueueBundle\Model\Worker;
use Exception;

class CopyStationWorker extends Worker
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager )
    {
        $this->entityManager = $entityManager;
    }

    public function getName()
    {
        return 'CopyStationWorker';
    }

    public function copyStation(int $oldStation = null, int $newStation = null)
    {    
        if($oldStation &&  $newStation ){

            $oldStation = $this->entityManager->getRepository('App\Entity\Station')->find($oldStation);
            $newStation = $this->entityManager->getRepository('App\Entity\Station')->find($newStation);

            $newBalanceGroupArr = [];
            foreach ($oldStation->getBalanceGroups() as $balanceGroup) {
                $newBalanceGroup = new BalanceGroup();
                $newBalanceGroup->setName($balanceGroup->getName());
                $newBalanceGroup->setCompany($balanceGroup->getCompany());
                $newBalanceGroup->setIsArchive($balanceGroup->getIsArchive());
                $newBalanceGroup->setStation($newStation);
                $this->entityManager->persist($newBalanceGroup);
                $this->entityManager->flush($newBalanceGroup);
                $newBalanceGroupArr[$balanceGroup->getId()] = $newBalanceGroup;
            }

            foreach ($oldStation->getSkillRates() as $skillRate) {
                $newSkillRate = new SkillRate();
                $newSkillRate->setDefaultRate($skillRate->getDefaultRate());
                $newSkillRate->setSkill($skillRate->getSkill());
                $newSkillRate->setIsArchive($skillRate->getIsArchive());
                $newSkillRate->setStation($newStation);
                $this->entityManager->persist($newSkillRate);
                $this->entityManager->flush($newSkillRate);
            }

            $newRateArr = [];
            foreach ($oldStation->getRates() as $rate) {
                $newRate = new Rate();
                $newRate->setName($rate->getName());
                $newRate->setCriteria($rate->getCriteria());
                $newRate->setEffectiveRate($rate->getEffectiveRate());
                $newRate->setCumulativeRate($rate->getCumulativeRate());
                $newRate->setCompany($rate->getCompany());
                $newRate->setStation($newStation);
                $newRate->setIsArchive($rate->getIsArchive());
                $this->entityManager->persist($newRate);
                $this->entityManager->flush($newRate);
                $newRateArr[$rate->getId()] = $newRate;
            }

            $newInvoiceArr = [];
            foreach ($oldStation->getInvoiceTypes() as $invoice) {
                $newInvoice = new InvoiceType();
                $newInvoice->setName($invoice->getName());
                $newInvoice->setBillableHour($invoice->getBillableHour());
                if($invoice->getRateRule() && array_key_exists($invoice->getRateRule()->getId(),$newRateArr)){
                $newInvoice->setRateRule($newRateArr[$invoice->getRateRule()->getId()]);
                }else {
                $newInvoice->setRateRule(null);
                }
                $newInvoice->setInvoiceType($invoice->getInvoiceType());
                $newInvoice->setStation($newStation);
                $newInvoice->setIsArchive($invoice->getIsArchive());
                $this->entityManager->persist($newInvoice);
                $this->entityManager->flush($newInvoice);
                $newInvoiceArr[$invoice->getId()] = $newInvoice;
            }

            $newShiftArr = [];
            foreach ($oldStation->getShifts() as $shift) {
                $newShift = new Shift();
                $newShift->setName($shift->getName());
                $newShift->setColor($shift->getColor());
                $newShift->setStartTime($shift->getStartTime());
                $newShift->setEndTime($shift->getEndTime());
                $newShift->setUnpaidBreak($shift->getUnpaidBreak());
                $newShift->setNote($shift->getNote());
                $newShift->setStation($newStation);
                $newShift->setSkill(($shift->getSkill())?$shift->getSkill():null);
                $newShift->setIsArchive($shift->getIsArchive());
                $newShift->setCompany($shift->getCompany());
                $newShift->setCategory($shift->getCategory());
                $newShift->setTextColor($shift->getTextColor());
                $newShift->setShiftTemplate($shift->getShiftTemplate());

                if($shift->getBalanceGroup() && array_key_exists($shift->getBalanceGroup()->getId(),$newBalanceGroupArr)){
                $newShift->setBalanceGroup($newBalanceGroupArr[$shift->getBalanceGroup()->getId()]);
                }else {
                $newShift->setBalanceGroup(null);
                }

                if($shift->getInvoiceType() && array_key_exists($shift->getInvoiceType()->getId(),$newInvoiceArr)){
                $newShift->setInvoiceType($newInvoiceArr[$shift->getInvoiceType()->getId()]);
                }else {
                $newShift->setInvoiceType(null);
                }

                $this->entityManager->persist($newShift);
                $this->entityManager->flush($newShift);
                $newShiftArr[$shift->getId()] = $newShift;
            }

            foreach ($oldStation->getSchedules() as $schedule) {
                $newSchedule = new Schedule();
                $newSchedule->setName($schedule->getName());
                $newSchedule->setCompany($schedule->getCompany());
                $newSchedule->setCreatedDate($schedule->getCreatedDate());
                $newSchedule->setStartDate($schedule->getStartDate());
                $newSchedule->setEndDate($schedule->getEndDate());
                $newSchedule->setIsRecurring($schedule->getIsRecurring());
                $newSchedule->setStation($newStation);
                $newSchedule->setIsArchive($schedule->getIsArchive());
                $newSchedule->setStartWeek($schedule->getStartWeek());
                $newSchedule->setEndWeek($schedule->getEndWeek());
                $newSchedule->setYear($schedule->getYear());
                $newSchedule->setWeekEndYear($schedule->getWeekEndYear());
                $newSchedule->setOriginalWeekCount($schedule->getOriginalWeekCount());
                $newSchedule->setScheduleLastDate($schedule->getScheduleLastDate());
                $newSchedule->setPreviousAssignSchedule($schedule->getPreviousAssignSchedule());

                $this->entityManager->persist($newSchedule);
                $this->entityManager->flush($newSchedule);

                // $schedule->getDrivers();
                foreach ($schedule->getScheduleDesigns() as $scheduleDesign) {

                $newScheduleDesign= new ScheduleDesign();
                $newScheduleDesign->setSchedule($newSchedule);
                foreach ($scheduleDesign->getShifts() as $shift) {
                    if($shift && array_key_exists($shift->getId(),$newShiftArr)){
                    $newScheduleDesign->addShift($newShiftArr[$shift->getId()]);
                    }
                }
                $newScheduleDesign->setWeek($scheduleDesign->getWeek());
                $newScheduleDesign->setSunday($scheduleDesign->getSunday());
                $newScheduleDesign->setMonday($scheduleDesign->getMonday());
                $newScheduleDesign->setTuesday($scheduleDesign->getTuesday());
                $newScheduleDesign->setWednesday($scheduleDesign->getWednesday());
                $newScheduleDesign->setThursday($scheduleDesign->getThursday());
                $newScheduleDesign->setFriday($scheduleDesign->getFriday());
                $newScheduleDesign->setSaturday($scheduleDesign->getSaturday());
                $newScheduleDesign->setOrdering($scheduleDesign->getOrdering());
                $newScheduleDesign->setYear($scheduleDesign->getYear());
                $newScheduleDesign->setIsArchive($scheduleDesign->getIsArchive());
                $this->entityManager->persist($newScheduleDesign);
                $this->entityManager->flush($newScheduleDesign);
                }
            }
        }

        return self::RESULT_SUCCESS;  
    }

    //
    //bin/console dtc:queue:create CopyStationWorker copyStation  85 55
    //
    //THEN
    //bin/console dtc:queue:run CopyStationWorker copyStation
}