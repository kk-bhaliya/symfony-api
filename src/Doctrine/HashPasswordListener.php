<?php


namespace App\Doctrine;


use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashPasswordListener implements EventSubscriber
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {

        $this->encoder = $encoder;
    }

    /**
     * @param User $entity
     */
    private function encodePassword(User $entity)
    {

        $encoded = $this->encoder->encodePassword($entity, $entity->getPassword());
        $entity->setPassword($encoded);
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof User) {
            return;
        }
        $encoded = $this->encoder->encodePassword($entity, $entity->getPassword());
        $entity->setPassword($encoded);
    }
    
    public function preUpdate(LifecycleEventArgs $args)
    {
//        $entity = $args->getEntity();
//        if (!$entity instanceof User) {
//            return;
//        }
//        $this->encodePassword($entity);
        // necessary to force the update to see the change
//        $em = $args->getEntityManager();
//        $meta = $em->getClassMetadata(get_class($entity));
//        $em->getUnitOfWork()->recomputeSingleEntityChangeSet($meta, $entity);
    }
    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return string[]
     */
    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate'];
    }
}